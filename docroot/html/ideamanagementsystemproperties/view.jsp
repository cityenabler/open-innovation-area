<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.CLSCategoriesSetLocalServiceUtil"%>
<%@page import="com.liferay.calendar.model.Calendar"%>
<%@page import="com.liferay.calendar.service.CalendarLocalServiceUtil"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet"%>
<%@page import="it.eng.rspa.ideas.controlpanel.CategoryKeys"%>
<%@page import="it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties"%>
<%@page import="com.liferay.portlet.PortletPreferencesFactoryUtil"%>
<%@page import="javax.portlet.PortletPreferences"%>
<%@page import="com.liferay.portal.kernel.util.StringPool"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@page import="javax.portlet.ActionRequest"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portlet.asset.service.AssetVocabularyLocalServiceUtil"%>
<%@page import="com.liferay.portlet.asset.model.AssetVocabulary"%>
<%@page import="com.liferay.portlet.asset.model.AssetCategory"%>
<%@page import="com.liferay.util.portlet.PortletProps"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ include file="/html/challenges/init.jsp" %>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<%
IdeaManagementSystemProperties imsp = new IdeaManagementSystemProperties();

String rootUrl = PortalUtil.getHomeURL(request);

%>

<%
PortletURL savePreferenciesURL = renderResponse.createActionURL();
savePreferenciesURL.setParameter(ActionRequest.ACTION_NAME, "savePreferencies");
%>

<aui:form name="fm" method="POST" action="<%= savePreferenciesURL.toString() %>" >


<h2><b><liferay-ui:message key="categories"/></b></h2>
 
<br/>
<%
ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);

////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////// CATEGORIES ///////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

List<AssetVocabulary> vocabularies = AssetVocabularyLocalServiceUtil.getAssetVocabularies(com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS, com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS);
List<Long> mapCategoriesSet = imsp.getMapCategoriesSetIMS();
	
for (AssetVocabulary vocabulary:vocabularies){	
	
	if(!   (vocabulary.getName().equals ("Topic") || vocabulary.getName().equals ("Topic (2)") )){
		
		String inputSingleName = "choiceTypeForVocabulary_" + vocabulary.getVocabularyId();
		String inputMultiName = "choiceTypeForVocabulary_" + vocabulary.getVocabularyId();
		String inputIcon = "iconName_" + vocabulary.getVocabularyId();
		String inputColour = "colourName_" + vocabulary.getVocabularyId();
		String checkboxVocabularyName = "checkbox_vocabulary_#"+vocabulary.getVocabularyId()+"#Checkbox";
		
		String value = "false";
		String icon = "";
		String colour = "";
		boolean inputSingleChecked = false;
		boolean inputMultiChecked = true;
		if(mapCategoriesSet.contains(vocabulary.getVocabularyId()) ){
			value = "true";
			
			CLSCategoriesSet catSet = CLSCategoriesSetLocalServiceUtil.getCLSCategoriesSet(vocabulary.getVocabularyId());
			icon = catSet.getIconName();
			colour = catSet.getColourName();
			
		}
		
		%>
			<aui:input inlineField="true" type="checkbox" label="<%=vocabulary.getName()%>" name="<%=checkboxVocabularyName %>" value="<%=value%>"/>
			<aui:input type="text"  label="Icon name" name="<%=inputIcon%>" value="<%=icon%>"  helpMessage="e.g. icon-smile"/><br/>
			<aui:input type="text"  label="Colour name" name="<%=inputColour%>" value="<%=colour%>" helpMessage="e.g.one among: grey, blue-grey, teal, green, cyan, red, brown, purple, blue, orange "/><br/>
			
			<hr/>
			<br/>
		<%
	}// if (!topic )

}


////////////////////////////////////////////////////////////////////////////////////
////////////////////////////// END CATEGORIES //////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////




// ottengo i suffissi url
String suffixImsHome = IdeaManagementSystemProperties.getUrlSuffixImsHome();
String suffixIdeas = IdeaManagementSystemProperties.getFriendlyUrlSuffixIdeas();
String suffixNeeds = IdeaManagementSystemProperties.getFriendlyUrlSuffixNeeds();
String suffixChallenges = IdeaManagementSystemProperties.getFriendlyUrlSuffixChallenges();


boolean emailNotificationsEnabled = IdeaManagementSystemProperties.getEnabledProperty("emailNotificationsEnabled");
boolean dockbarNotificationsEnabled = IdeaManagementSystemProperties.getEnabledProperty("dockbarNotificationsEnabled");

%>
	<br/>
	
	<h2><b><liferay-ui:message key="suffix"/></b></h2>
	

	 
	 <aui:input name="sfxHomeIms" label="ims.imsHome-suffix" value="<%=suffixImsHome%>" helpMessage="e.g. innovation-area">
				  	 <aui:validator name="required"  errorMessage="ims.enter-imsHome-suffix"  />
	 </aui:input>
	 
	<aui:input name="sfxIdeas" label="ims.ideas-suffix" value="<%=suffixIdeas%>" helpMessage="e.g. ideas_explorer">
				  	 <aui:validator name="required"  errorMessage="ims.enter-ideas-suffix"  />
	 </aui:input>
	              
	 <aui:input name="sfxNeeds" label="ims.needs-suffix" value="<%=suffixNeeds%>" helpMessage="e.g. needs_explorer">
				  	 <aui:validator name="required"  errorMessage="ims.enter-needs-suffix"  />
	 </aui:input>
	
	<aui:input name="sfxChallenges" label="ims.challenges-suffix" value="<%=suffixChallenges%>" helpMessage="e.g. challenges_explorer">
				  	 <aui:validator name="required"  errorMessage="ims.enter-challenges-suffix"  />
	</aui:input>
	

	
	<br/>
	<br/>
	<br/>
	<h2><b>User notification</b></h2>
	<aui:input inlineField="true" type="checkbox" label="Email notifications Enabled" name="emailNotificationsEnabled" value="<%=emailNotificationsEnabled%>"/><br/><br/>	
	<aui:input inlineField="true" type="checkbox" label="Dockbar Notifications Enabled" name="dockbarNotificationsEnabled" value="<%=dockbarNotificationsEnabled%>"/><br/>	
	<br/><br/>
	<%
	String senderNotificheMailIdeario = IdeaManagementSystemProperties.getProperty("senderNotificheMailIdeario");
	String oggettoNotificheMailIdeario = IdeaManagementSystemProperties.getProperty("oggettoNotificheMailIdeario");
	String firmaNotificheMailIdeario = IdeaManagementSystemProperties.getProperty("firmaNotificheMailIdeario");
	String utenzaMail = IdeaManagementSystemProperties.getProperty("utenzaMail");
	
	 %>
	 <h2><b><liferay-ui:message key="default-email-notification"/></b></h2>
	
	<aui:input name="senderNotificheMailIdeario" label="sender" value="<%=senderNotificheMailIdeario%>" helpMessage="e.g. WeLive">
				  	 <aui:validator name="required"   />
	</aui:input>
	<aui:input name="oggettoNotificheMailIdeario" label="ims.email-object" value="<%=oggettoNotificheMailIdeario%>" helpMessage="e.g. Innovation Arena">
				  	 <aui:validator name="required"   />
	</aui:input>
	<aui:input name="firmaNotificheMailIdeario" label="signature" value="<%=firmaNotificheMailIdeario%>" helpMessage="e.g. admin">
				  	 <aui:validator name="required"   />
	</aui:input>
	<aui:input name="utenzaMail" label="suffix" value="<%=utenzaMail%>" helpMessage="e.g. openinnovationarea">
				  	 <aui:validator name="required"   />
	</aui:input>
	
	
	<br/>
	<br/>
	<br/>
	<h2><b><liferay-ui:message key="ims.map-parameters"/></b></h2>

	 
	<%
	String latitude = imsp.getMapCenterLatitude();
	String longitude = imsp.getMapCenterLongitude();
	
	boolean googleMapsAPIKeyEnabled = IdeaManagementSystemProperties.getEnabledProperty("googleMapsAPIKeyEnabled");
	String googleMapsAPIKey = IdeaManagementSystemProperties.getProperty("googleMapsAPIKey");
	%>
	
	
	<aui:input inlineField="true" type="checkbox" label="googleMapsAPIKeyEnabled" name="googleMapsAPIKeyEnabled" value="<%=googleMapsAPIKeyEnabled%>"/><br/>
	<br/>
	<aui:input name="googleMapsAPIKey" id="googleMapsAPIKey" label="Google Maps API Key Enabled" value="<%=googleMapsAPIKey%>" type="text" helpMessage="AIzaSyCNOca-zFX8gT4IBoU5fJwu8LB4w4-Ini8"/><br/>
	
	
	<aui:input name="Latitude"  id="Latitude"  label="ims.latitude"  value="<%=latitude%>" type="text" helpMessage="e.g. 40.353237">
				  	 <aui:validator name="required"  errorMessage="Enter the latitude"  />
	              </aui:input>
	<aui:input name="Longitude" id="Longitude" label="ims.longitude" value="<%=longitude%>" type="text" helpMessage="e.g. 18.172545">
				  	 <aui:validator name="required"  errorMessage="Enter the longitude"  />
	              </aui:input>

	<br/>
	<br/>
	<%	
	List<Calendar> calendars = CalendarLocalServiceUtil.getCalendars(com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS, com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS);
	long calendarId = imsp.getChallengesReferenceCalendarId();
	 %>
	
	<b><liferay-ui:message key="ims.challenges-calendar"/></b>
	 <br/>
	<aui:select label="" name="challengeCalendar" id="challengeCalendar" >
		<aui:option selected="<%=calendarId == -1 ? true : false %>" value="-1"><liferay-ui:message key="ims.nothing" /></aui:option>
		<%
		for (Calendar calendar : calendars) {
		%>
						<aui:option selected="<%=calendarId == calendar.getCalendarId() ? true : false %>" value="<%=calendar.getCalendarId()%>"><liferay-ui:message key="<%= calendar.getName() %>" /></aui:option>

		<%
		
		}
		%>
	</aui:select>
	<br/><br/>
	<%	
		boolean verboseEnabled = IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled");
		boolean pilotingEnabled = IdeaManagementSystemProperties.getEnabledProperty("pilotingEnabled");
		boolean needEnabled = IdeaManagementSystemProperties.getEnabledProperty("needEnabled");
		boolean publicIdeasEnabled = IdeaManagementSystemProperties.getEnabledProperty("publicIdeasEnabled");
		boolean reducedLifecycle = IdeaManagementSystemProperties.getEnabledProperty("reducedLifecycle");
		boolean emailOnNewChallengeEnabled = IdeaManagementSystemProperties.getEnabledProperty("emailOnNewChallengeEnabled");
	%>

	<h2><b>Verbosity</b></h2>
	<br/>
	<aui:input inlineField="true" type="checkbox" label="Catalina.out verbosity Enabled" name="verboseEnabled" value="<%=verboseEnabled%>"/><br/>
	<br/>
	<h2><b>Piloting</b></h2>
	
	<aui:input inlineField="true" type="checkbox" label="Partition in pilot" name="pilotingEnabled" value="<%=pilotingEnabled%>"/><br/>
	<br/>
	
	<h2><b>Need</b></h2>
	
	<aui:input inlineField="true" type="checkbox" label="Need enabled" name="needEnabled" value="<%=needEnabled%>"/><br/>
	<br/>
	
	<h2><b>Public Ideas</b></h2>
	
	<aui:input inlineField="true" type="checkbox" label="Public Ideas" name="publicIdeasEnabled" value="<%=publicIdeasEnabled%>"/><br/>
	<br/>
	
	<h2><b>Reduced Lifecycle</b></h2>
	
	<aui:input inlineField="true" type="checkbox" label="Reduced Lifecycle" name="reducedLifecycle" value="<%=reducedLifecycle%>"/><br/>
	<br/>
	<br/>
	
	<h2><b>Email On New Challenge</b></h2>
	
	<aui:input inlineField="true" type="checkbox" label="Email On New Challenge Enabled" name="emailOnNewChallengeEnabled" value="<%=emailOnNewChallengeEnabled%>"/><br/>
	<br/>
	<br/>
	
<h2><b><liferay-ui:message key="external-services"/></b></h2>


	<%
	boolean cdvEnabled = IdeaManagementSystemProperties.getEnabledProperty("cdvEnabled");
	String cdvAddress = IdeaManagementSystemProperties.getProperty("cdvAddress");
	
	boolean mockEnabled = IdeaManagementSystemProperties.getEnabledProperty("mockEnabled");
	boolean mktEnabled = IdeaManagementSystemProperties.getEnabledProperty("mktEnabled");
	
	boolean vcEnabled = IdeaManagementSystemProperties.getEnabledProperty("vcEnabled");
	String vcAddress = IdeaManagementSystemProperties.getProperty("vcAddress");
	String vcWSAddress = IdeaManagementSystemProperties.getProperty("vcWSAddress");
	boolean deEnabled = IdeaManagementSystemProperties.getEnabledProperty("deEnabled");
	String deAddress = IdeaManagementSystemProperties.getProperty("deAddress");
	boolean lbbEnabled = IdeaManagementSystemProperties.getEnabledProperty("lbbEnabled");
	String lbbAddress = IdeaManagementSystemProperties.getProperty("lbbAddress");
	String oiaAppId4lbb = IdeaManagementSystemProperties.getProperty("oiaAppId4lbb");
	boolean tweetingEnabled = IdeaManagementSystemProperties.getEnabledProperty("tweetingEnabled");
	String basicAuthUser = IdeaManagementSystemProperties.getProperty("basicAuthUser");
	String basicAuthPwd = IdeaManagementSystemProperties.getProperty("basicAuthPwd");
	
	boolean fiwareEnabled = IdeaManagementSystemProperties.getEnabledProperty("fiwareEnabled");
	boolean fiwareRemoteCatalogueEnabled = IdeaManagementSystemProperties.getEnabledProperty("fiwareRemoteCatalogueEnabled");
	String fiwareCatalogueAddress = IdeaManagementSystemProperties.getProperty("fiwareCatalogueAddress");
	
	boolean fundingBoxEnabled = IdeaManagementSystemProperties.getEnabledProperty("fundingBoxEnabled");

	
	boolean jmsEnabled = IdeaManagementSystemProperties.getEnabledProperty("jmsEnabled");
	String brokerJMSusername = IdeaManagementSystemProperties.getProperty("brokerJMSusername");
	String brokerJMSpassword = IdeaManagementSystemProperties.getProperty("brokerJMSpassword");
	String brokerJMSurl = IdeaManagementSystemProperties.getProperty("brokerJMSurl");
	String jmsTopic = IdeaManagementSystemProperties.getProperty("jmsTopic");
	String fundingBoxAddress = IdeaManagementSystemProperties.getProperty("fundingBoxAddress");
	String fundingBoxAPIAddress = IdeaManagementSystemProperties.getProperty("fundingBoxAPIAddress");
	
	boolean graylogEnabled = IdeaManagementSystemProperties.getEnabledProperty("graylogEnabled");
	String graylogAddress = IdeaManagementSystemProperties.getProperty("graylogAddress");
	
	
	boolean virtuosityPointsEnabled = IdeaManagementSystemProperties.getEnabledProperty("virtuosityPointsEnabled");
	String orionUrl = IdeaManagementSystemProperties.getProperty("orionUrl");
	
	
	 %>
	 

	<aui:input name="basicAuthUser" id="basicAuthUser" label="Basic Authentication User" value="<%=basicAuthUser%>" type="text" helpMessage="e.g. opsi@opsi.eu"/><br/>
	<aui:input name="basicAuthPwd" id="basicAuthPwd" label="Basic Authentication Password" value="<%=basicAuthPwd%>" type="text" helpMessage="e.g. 01h2h2h3o3"/><br/>	
<br/><br/>
	<aui:input inlineField="true" type="checkbox" label="Marketplace Enabled" name="mktEnabled" value="<%=mktEnabled%>"/><br/>	 
	
<br/><br/>
	<aui:input inlineField="true" type="checkbox" label="Tweeting Enabled" name="tweetingEnabled" value="<%=tweetingEnabled%>"/><br/>
<br/><br/>
	<aui:input inlineField="true" type="checkbox" label="Citizen Data Vault Enabled" name="cdvEnabled" value="<%=cdvEnabled%>"/><br/>
	<aui:input name="cdvAddress" id="cdvAddress" label="Citizen Data Vault Address" value="<%=cdvAddress%>" type="text" helpMessage="e.g. https://dev.welive.eu"/><br/>
<br/><br/>				  	
	<aui:input inlineField="true" type="checkbox" label="Visual Composer Enabled" name="vcEnabled" value="<%=vcEnabled%>"/><br/>
	<aui:input name="vcAddress" id="vcAddress" label="Visual Composer Address" value="<%=vcAddress%>" type="text" helpMessage="e.g. https://dev.welive.eu/visualcomposer/"/><br/>
	<aui:input name="vcWSAddress" id="vcWSAddress" label="Visual Composer WS Address" value="<%=vcWSAddress%>" type="text" helpMessage="e.g. https://test.welive.eu/dev/api/vc"/><br/>
<br/><br/>	
	<aui:input inlineField="true" type="checkbox" label="Decision Engine Enabled" name="deEnabled" value="<%=deEnabled%>"/><br/>
	<aui:input name="deAddress" id="deAddress" label="Decision Engine Address" value="<%=deAddress%>" type="text" helpMessage="e.g. https://dev.welive.eu/dev/api/de/"/><br/>
<br/><br/>
	<aui:input inlineField="true" type="checkbox" label="Logging BB Enabled" name="lbbEnabled" value="<%=lbbEnabled%>"/><br/>
	<aui:input name="lbbAddress" id="lbbAddress" label="Logging BB Address" value="<%=lbbAddress%>" type="text" helpMessage="e.g. https://dev.welive.eu/dev/api/log/"/><br/>
	<aui:input name="oiaAppId4lbb" id="oiaAppId4lbb" label="OIA appId Logging BB" value="<%=oiaAppId4lbb%>" type="text" helpMessage="oia"/><br/>
	<br/><br/>
	
	<h3><b>JSM</b></h3>
	<aui:input inlineField="true" type="checkbox" label="JMS Enabled" name="jmsEnabled" value="<%=jmsEnabled%>"/><br/>
	<aui:input name="brokerJMSusername" id="brokerJMSusername" label="broker JMS username" value="<%=brokerJMSusername%>" type="text" helpMessage="e.g. tesb"/><br/>
	<aui:input name="brokerJMSpassword" id="brokerJMSpassword" label="broker JMS password" value="<%=brokerJMSpassword%>" type="text" helpMessage="e.g. tesb"/><br/>
	<aui:input name="brokerJMSurl" id="brokerJMSurl" label="broker JMS URL" value="<%=brokerJMSurl%>" type="text" helpMessage="e.g. tcp://oia.eng.it:8080"/><br/>
	<aui:input name="jmsTopic" id="jmsTopic" label="JMS Topic" value="<%=jmsTopic%>" type="text" helpMessage="e.g. IdeaManagement"/><br/>
	<br/><br/>
	
	
	<h3><b>FIWARE</b></h3>
	<aui:input inlineField="true" type="checkbox" label="FIWARE Enabled" name="fiwareEnabled" value="<%=fiwareEnabled%>"/><br/><br/>	
	<aui:input inlineField="true" type="checkbox" label="FIWARE Remote Catalogue Enabled" name="fiwareRemoteCatalogueEnabled" value="<%=fiwareRemoteCatalogueEnabled%>"/><br/>	
	<aui:input name="fiwareCatalogueAddress" id="fiwareCatalogueAddress" label="FIWARE Remote Catalogue Address" value="<%=fiwareCatalogueAddress%>" type="text" helpMessage="https://catalogue.fiware.org/getallges/views/getallges/"/><br/>
	<br/>
	
	<h3><b>FundingBox</b></h3>
	<aui:input inlineField="true" type="checkbox" label="FundingBox Enabled" name="fundingBoxEnabled" value="<%=fundingBoxEnabled%>"/><br/>
	<aui:input name="fundingBoxAddress" id="fundingBoxAddress" label="Fundingbox Address" value="<%=fundingBoxAddress%>" type="text" helpMessage="http://fiware-gctc.fundingbox.com/"/><br/>	
	<aui:input name="fundingBoxAPIAddress" id="fundingBoxAPIAddress" label="Fundingbox API Address" value="<%=fundingBoxAPIAddress%>" type="text" helpMessage="http://api.fundingbox.com/v2/applications"/><br/><br/>
	<br/>
	
	<h2><b>Mock</b></h2>
	
	<aui:input inlineField="true" type="checkbox" label="Mock Enabled" name="mockEnabled" value="<%=mockEnabled%>"/><br/>	
	<br/>
	<br/>
	
	<h2><b>GrayLog</b></h2>
	
	<aui:input inlineField="true" type="checkbox" label="GrayLog Enabled" name="graylogEnabled" value="<%=graylogEnabled%>"/><br/>	
	<aui:input name="graylogAddress" id="graylogAddress" label="GrayLog Address" value="<%=graylogAddress%>" type="text" helpMessage="http://217.172.12.185:8081/gelf"/><br/>	
	<br/>
	<br/>
	
	<h2><b>Virtuosity points</b></h2>
	
	<aui:input inlineField="true" type="checkbox" label="Virtuosity Points Enabled" name="virtuosityPointsEnabled" value="<%=virtuosityPointsEnabled%>"/><br/>	
	<aui:input name="orionUrl" id="orionUrl" label="Orion Address" value="<%=orionUrl%>" type="text" helpMessage="http://217.172.12.145:1026"/><br/>	
	
	<br/>
	<br/>
	
	
	
	
	
	
	
<aui:button type="submit" value="save"/>


</aui:form>

