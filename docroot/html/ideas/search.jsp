<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.search.Field"%>
<%@page import="com.liferay.portal.kernel.util.GetterUtil"%>
<%@page import="com.liferay.portal.kernel.util.StringPool"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@page import="com.liferay.portal.kernel.search.Document"%>
<%@page import="com.liferay.portal.kernel.search.Hits"%>
<%@page import="com.liferay.portal.kernel.search.SearchContextFactory"%>
<%@page import="com.liferay.portal.kernel.search.SearchContext"%>
<%@page import="com.liferay.portal.kernel.search.IndexerRegistryUtil"%>
<%@page import="com.liferay.portal.kernel.search.Indexer"%>
<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@page import="com.liferay.portal.kernel.dao.search.SearchContainer"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@ include file="/html/challenges/init.jsp"%>
<%
	String redirect = ParamUtil.getString(request, "redirect");
	String keywords = ParamUtil.getString(request, "keywords");
%>
<portlet:renderURL var="searchURL">
	<portlet:param name="jspPage" value="/html/ideas/search.jsp" />
</portlet:renderURL>
<aui:form action="<%=searchURL%>" method="POST" name="fm">
	<liferay-portlet:renderURLParams varImpl="searchURL" />
	<aui:input name="redirect" type="hidden" value="<%=redirect%>" />
	<liferay-ui:header backURL="<%=redirect%>" title="search" />
	<%
		PortletURL portletURL = renderResponse.createRenderURL();
			portletURL.setParameter("jspPage", "/html/ideas/search.jsp");
			portletURL.setParameter("redirect", redirect);
			portletURL.setParameter("keywords", keywords);
			List<String> headerNames = new ArrayList<String>();
			headerNames.add("#");
			headerNames.add("slogan");
			//headerNames.add("score");
			SearchContainer searchContainer = new SearchContainer(
					renderRequest,
					null,
					null,
					SearchContainer.DEFAULT_CUR_PARAM,
					SearchContainer.DEFAULT_DELTA,
					portletURL,
					headerNames,
					LanguageUtil
							.format(pageContext,
									"no-entries-were-found-that-matched-the-keywords-x",
									"<b class='boldText'>" + HtmlUtil.escape(keywords)
											+ "</b>"));
			try {
				Indexer indexer = IndexerRegistryUtil.getIndexer(CLSIdea.class);
				SearchContext searchContext = SearchContextFactory.getInstance(request);
				searchContext.setEnd(searchContainer.getEnd());
				searchContext.setKeywords(keywords);
				searchContext.setStart(searchContainer.getStart());
				Hits results = indexer.search(searchContext);
				int total = results.getLength();
				searchContainer.setTotal(total);
				List resultRows = searchContainer.getResultRows();
				for (int i = 0; i < results.getDocs().length; i++) {
					Document doc = results.doc(i);
					ResultRow row = new ResultRow(doc, i, i);
					// Position
					row.addText(searchContainer.getStart() + i + 1
							+ StringPool.PERIOD);
					// Slogan
					long sloganId = GetterUtil.getLong(doc
							.get(Field.ENTRY_CLASS_PK));
					CLSIdea slogan = null;
					try {
						slogan = CLSIdeaLocalServiceUtil
								.getCLSIdea(sloganId);
						slogan = slogan.toEscapedModel();
					} catch (Exception e) {
						if (_log.isWarnEnabled()) {
							_log.warn("Slogan search index is stale and contains entry "
									+ sloganId);
						}
						continue;
					}
					PortletURL rowURL = renderResponse.createRenderURL();
					rowURL.setParameter("jspPage", "/html/ideas/view_idea.jsp");
					rowURL.setParameter("redirect", PortletURLUtil.getCurrent(renderRequest, renderResponse).toString());
					rowURL.setParameter("ideaId",String.valueOf(slogan.getChallengeId()));
					row.addText(slogan.getIdeaTitle(), rowURL);
					// Score
					//row.addScore(results.score(i));
					// Add result row
					resultRows.add(row);
				}
	%>
	<span class="aui-search-bar"> <aui:input
			inlineField="<%=true%>" label="" name="keywords" size="30"
			title="search-entries" type="text" value="<%=keywords%>" /> <aui:button
			type="submit" value="search" />
	</span>
	<br />
	<br />
	<liferay-ui:search-iterator searchContainer="<%=searchContainer%>" />
	<%
		} catch (Exception e) {
				_log.error(e.getMessage());
			}
	%>
</aui:form>
<%
	if (Validator.isNotNull(keywords)) {
		PortalUtil.addPortletBreadcrumbEntry(request,
				LanguageUtil.get(pageContext, "search") + ": "
						+ keywords,
				PortletURLUtil
						.getCurrent(renderRequest, renderResponse)
						.toString());
	}
%>
<%!private static Log _log = LogFactoryUtil
			.getLog("slogan_contest.docroot.html.search_jsp");%>
			
<br/><aui:button type="cancel" value="back" onClick="history.go(-1);"/>