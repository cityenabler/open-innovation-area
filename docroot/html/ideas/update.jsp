<%@page import="it.eng.rspa.ideas.utils.MyConstants"%>
<%@page import="it.eng.rspa.ideas.utils.IdeasListUtils"%>
<%@page import="com.liferay.portal.model.Organization"%>
<%@page import="it.eng.rspa.ideas.utils.UpdateIdeaUtils"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.CLSCategoriesSetForChallengeLocalServiceUtil"%>
<%@page import="com.liferay.portal.model.Role"%>
<%@page import="com.liferay.portal.service.RoleLocalServiceUtil"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaPoiLocalServiceUtil"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoi"%>
<%@page import="org.apache.commons.lang3.ArrayUtils"%>
<%@page import="it.eng.rspa.ideas.controlpanel.CategoryKeys"%>
<%@page import="com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil"%>
<%@page import="com.liferay.portlet.asset.service.AssetVocabularyLocalServiceUtil"%>
<%@page import="com.liferay.portlet.asset.model.AssetVocabulary"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet"%>
<%@page import="it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="com.liferay.portal.util.PortletKeys"%>
<%@page import="javax.portlet.PortletRequest"%>
<%@page import="com.liferay.portlet.PortletURLFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayPortletURL"%>
<%@page import="com.liferay.portal.kernel.util.StringPool"%>
<%@page import="com.liferay.portlet.documentlibrary.model.DLFileEntry"%>
<%@page import="com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.util.OrderByComparator"%>
<%@page import="com.liferay.portlet.documentlibrary.service.DLFolderLocalServiceUtil"%>
<%@page import="com.liferay.portlet.documentlibrary.model.DLFolder"%>
<%@page import="javax.portlet.PortletPreferences"%>
<%@page import="com.liferay.util.PwdGenerator"%>
<%@page import="java.io.File"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSCoworkerUtil"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.CLSCoworkerLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.dao.orm.QueryUtil"%>
<%@page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@page import="com.liferay.portal.service.RoleLocalServiceUtil"%>
<%@page import="com.liferay.portlet.asset.model.AssetTag"%>
<%@page import="com.liferay.portlet.asset.service.AssetTagServiceUtil"%>
<%@page import="com.liferay.portlet.asset.model.AssetCategory"%>
<%@page import="com.liferay.portlet.asset.service.AssetCategoryServiceUtil"%>
<%@page import="it.eng.rspa.ideas.utils.StringComparator"%>
<%@page import="com.liferay.portal.kernel.util.KeyValuePair"%>
<%@ include file="/html/challenges/init.jsp" %>

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script src="<%=request.getContextPath()%>/js/upload/jquery.uploadfile.js"></script>
		<script src="<%=request.getContextPath()%>/js/customckeditor/ckeditor.js"></script>
		
		<link href="<%=request.getContextPath()%>/css/upload/uploadfile.css" rel="stylesheet"/>
		<link href="<%=request.getContextPath()%>/css/creation.css" rel="stylesheet"/>
		
		<portlet:renderURL var="portletURL" windowState="normal">
			<portlet:param name="struts_action" value="/ext/reports/view_reports"/>
		</portlet:renderURL>
		
		<portlet:actionURL var="editCaseURL" name="uploadCase">
		    <portlet:param name="jspPage" value="/edit.jsp" />
		</portlet:actionURL>
		
		<portlet:resourceURL var="resourceURL" />
		
		<portlet:actionURL var="deleteFileURL" >
		    <portlet:param name="<%=ActionRequest.ACTION_NAME%>" value="deleteFile" />
		</portlet:actionURL>
		
		<portlet:actionURL var="updateIdeasURL">
			<portlet:param name="<%=ActionRequest.ACTION_NAME%>" value="updateIdeas" />
		</portlet:actionURL>
		
		<portlet:renderURL var="viewIdeasURL">
			<portlet:param name="jspPage" value="/html/ideasexplorermaterial/ideasdetails/view.jsp"/>
		</portlet:renderURL>
		
		<%@ include file="checkUser.jspf" %>
		
		<% 
		
		Locale locale = (Locale)request.getSession().getAttribute("org.apache.struts.action.LOCALE");
		
		String uuid = UUID.randomUUID().toString();
		
		User currentUser = PortalUtil.getUser(request);
		
		boolean needReport=false;
		
		String pathDefaultimg = "/img/ideasImg.png";
		
		String urlHomeIms = IdeaManagementSystemProperties.getUrlHomeIMS();
		
	    //per la gestione delle municipalita
	    //List<User> municipalitaI = UpdateIdeaUtils.getAllMunicipalities();
	    List<Organization> municipalitaI = UpdateIdeaUtils.getAllOrganizationMunicipalities();
	    
	   // List<User> municipalita = MyUtils.getListaUtenticonPilotUguali(currentUser, municipalitaI);//filtro su pilot
		 List<Organization> municipalita = MyUtils.getListaOrganizationsconPilotUgualeByUser(currentUser, municipalitaI); //filtro su pilot
		 
		 //se non ci sono municipalita mando alla pagina di errore
		 if (municipalita.size() ==0){
			 
			 String errorMessageKeyI18n = "ims.error-no-authority";
		 %>
 				<%@ include file="/html/home/pages/goToErrorPage.jspf" %>
			 <%return;
		 }
		 
			
		String pilota = IdeasListUtils.getPilot(renderRequest);
		String [] coordinateMap = 	MyUtils.getMapLatLongCenterbyPilot(pilota);
		
	    List<CLSChallenge> challenges = new ArrayList<CLSChallenge>();
	    
		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
		
		Long groupId = themeDisplay.getCompanyGroupId();	
		pageContext.setAttribute("themeDisplay", themeDisplay);
		
		boolean reducedLifecycle = IdeaManagementSystemProperties.getEnabledProperty("reducedLifecycle");
		
		//recupero i dati dell'idea se sono in modifica
		long id = -1;
		long challengeId = -1;
		long needId = -1;
		long municipalityId = 0;
		String authorId = "-1";
		String ideaId = "-1";
		String ideaTitle = "";
		String ideaHashTag="";
		String ideaeDescription = "";
		String tags = "";
		String linguaMod = "";
		String cityName = "";
				
		//Gestione immagine rappresentativa
		String ideaRepresentativeImgUrl = "";
		//Gestione immagine rappresentativa
		
		Iterator<DLFileEntry> iter =null;
		Map<String, String[]> paramMap = request.getParameterMap();
		String[] paramIdeaId = paramMap.get("ideaId");
		
		
		boolean isModifica = false;	
		boolean areDescriptionAndTagsEditable = true;
		boolean areIdeaFieldsEditable = true;
		boolean mockEnabled = IdeaManagementSystemProperties.getEnabledProperty("mockEnabled");
		boolean fiwareEnabled = IdeaManagementSystemProperties.getEnabledProperty("fiwareEnabled");
		boolean isAuthor = false;
		
		
		if(paramIdeaId!=null){
			if(paramIdeaId.length>0){
				String ideaIdText = paramMap.get("ideaId")[0];
				if(ideaIdText!=null){
					try{
						id = Long.parseLong(ideaIdText);
						//recupero i dati del challenge
						if(id>0){ //sono in modifica
							
							isModifica = true;
							isAuthor = IdeasListUtils.isAuthorByIdeaId(id, currentUser);
							CLSIdea idea = CLSIdeaLocalServiceUtil.getCLSIdea(id);
							ideaId = Long.toString(id);
							
							
							areDescriptionAndTagsEditable = UpdateIdeaUtils.isDescriptionIdeaEditable(idea);
							areIdeaFieldsEditable = UpdateIdeaUtils.areIdeaFieldsEditable(idea);
							tags =IdeasListUtils.getTagsVirgoleByIdeaId(id);
							
							authorId = Long.toString(idea.getUserId());
							ideaeDescription = idea.getIdeaDescription();
							ideaTitle = idea.getIdeaTitle();
							
							cityName = idea.getCityName();

							challengeId = idea.getChallengeId();
							needId = idea.getNeedId();
							municipalityId = idea.getMunicipalityId();
							linguaMod = idea.getLanguage();
							
							//recupero dei documenti associati
							DLFolder folder = DLFolderLocalServiceUtil.getDLFolder(idea.getIdFolder());
							Long folderId = idea.getIdFolder();
							Integer count = DLFileEntryLocalServiceUtil.getFileEntriesCount(groupId, folderId);
							OrderByComparator obc = OrderByComparatorFactoryUtil.create("DLFileEntry", "title", true);
							List<DLFileEntry> files = DLFileEntryLocalServiceUtil.getFileEntries(
									groupId.longValue(), folderId.longValue(), 0, count,obc);
							iter = files.iterator();
							
							
							
							// Gestione immagine rappresentativa
							if(idea.getRepresentativeImgUrl() != null){
								ideaRepresentativeImgUrl = idea.getRepresentativeImgUrl();
							}
							// Gestione immagine rappresentativa
							
						}//if(id>0){
					}catch(Exception e){
						
					}
				}
			}
		}
		
		//recupero il forChallengeId nel caso in cui l'idea � proposta automaticamente per una particolare gara
		long forChallengeId=new Long(0);		
		if(request.getParameter("forChallengeId")!=null){
		 forChallengeId = Long.valueOf(request.getParameter("forChallengeId")).longValue();		 
			if(forChallengeId > 0){
				challengeId = forChallengeId;
			}
		}
		%>
<div class="materialize">

	<%
	String hrefBack = urlHomeIms;
	
	if (reducedLifecycle)
		hrefBack="javascript:history.go(-1)";
	%>

	<div class="row">
		<div class="col s12"> 
			<a class="waves-effect btn-flat tooltipped" href="<%=hrefBack %>" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.go-back"/>">
				<i class="icon-arrow-left left">&nbsp;</i><liferay-ui:message key="back"/>
			</a>
		</div>
	</div>



<div class="css-class-wrapper-update-fake">
		
		<% //recupero la lista delle challenge disponibili
		if(challengeId > 0){
			CLSChallenge challenge = CLSChallengeLocalServiceUtil.getCLSChallenge(challengeId);
		%>
			<div id="parentRef">
				<h4>
				
				<% if (!reducedLifecycle){	%>
	  					<i class="material-icons icon-trophy small tooltipped" data-position="top" data-delay="40" data-tooltip="<liferay-ui:message key="ims.challenge"/>">&nbsp;</i>
	  				<%}else{	%>	
	  					<i class="fa <%=MyConstants.ICON_CLG_REDUCED %> small tooltipped" data-position="top" data-delay="40" data-tooltip="<liferay-ui:message key="ims.challenge"/>">&nbsp;</i>
	  				<%}%>	
				
				
				<a href="<%=IdeaManagementSystemProperties.getFriendlyUrlChallenges(challengeId) %>" target="_blank">  
					<%=challenge.getChallengeTitle() %>
				 </a>
				</h4>
				<br>
				
				<% if (!reducedLifecycle){	%>
				
					<h6 class="valign-wrapper valign-content-center">
					<i class="material-icons small tooltipped" data-position="top" data-delay="40" data-tooltip="<liferay-ui:message key="organization"/>">business_center</i>
					<%=ChallengesUtils.getAuthorOrganizationNameByChallenge(challenge)%>
					</h6>
					<br>
				<%}%>	
					
				
			</div>
		<%}else if (needId > 0){
					 CLSIdea need = CLSIdeaLocalServiceUtil.getCLSIdea(needId);
			 %>	
			 <div id="parentRef">
				<h4><i class="fa fa-commenting-o fa-2x small tooltipped" data-position="top" data-delay="40" data-tooltip="<liferay-ui:message key="ims.need-report"/>">&nbsp;</i>
					<a href="<%=IdeaManagementSystemProperties.getFriendlyUrlNeeds(needId) %>" target="_blank">  
						<%=need.getIdeaTitle() %>
					</a>
				</h4>
				<br>
				<h6 class="valign-wrapper valign-content-center">
				<i class="material-icons small tooltipped" data-position="top" data-delay="40" data-tooltip="<liferay-ui:message key="organization"/>">business_center</i>
					<%=MyUtils.getOrganizationNameByOrganizationId(need.getMunicipalityOrganizationId()) %>
				</h6>
				<br>
			</div>
			 
			 
		<%	 
		 } // if NeedId >0
		
		%>
		
		<aui:form name="fm" method="POST" action="<%= updateIdeasURL.toString() %>" >
		<!-- enctype="multipart/form-data" > -->
			
		<!-- Gestione immagine rappresentativa campi -->
		<aui:model-context  model="<%=CLSIdea.class%>"></aui:model-context>
		<aui:input name="ideaId" type="hidden" value="<%=ideaId %>"></aui:input>
		<aui:input name="authorId" type="hidden" value="<%=authorId %>"></aui:input>
		<aui:input name="groupid" type="hidden" value="<%=groupId.longValue()%>"/> <!-- questo campo deve essere nascosto -->
		<aui:input name="token" type="hidden" value="<%=uuid%>"/><!-- questo campo deve essere nascosto -->												
		<aui:input name="isWizard" type="hidden" value="false"/>
		<aui:input name="selGes" type="hidden" value=""/>										
								

<!-- INFORMAZIONI GENERALI -->
		<%
		String panelTitle;
		if(ideaId.equals("-1")){ panelTitle = "ims.create.idea";}
		else{panelTitle="ims.edit.idea";}%>
		
		<div id="introduction" class="section">
	    	<div class="row">
	    		<div class="col s12">
	    			<div class="row">
   						<div class="col s12 flow-text weight-4 center"><liferay-ui:message key="<%=panelTitle %>"/></div>
   						<br>
   						<div class="bottom-margin"></div>
						<div class="divider bottom-margin"></div>
		
						<blockquote>
						  	*&nbsp;<liferay-ui:message key="ims.starred-are-mandatory"/>
						</blockquote>	
						<%@ include file="idea_PanelTitleUpdate.jspf" %>
		
					</div>
				</div>
			</div>
		</div>
		

		
				
<!-- Categorie e keywords -->
				<div class="section" id="cat-tag-panel">
					<h5 class="header"><liferay-ui:message key="ims.categories-and-keywords"/></h5> 
					<div class="divider bottom-margin "></div>	
					<%@ include file="categoriesUpdate.jspf" %>
					<%@ include file="tagsUpdate.jspf" %>
				</div>
				
<!-- fine Categorie e keywords --> 


<% if (! reducedLifecycle){ %>

<!-- attachments -->
				<div class="section" id="attach-panel" >
					<h5 class="header"><liferay-ui:message key="attachments"/><liferay-ui:icon-help message="ims.tip-attachments"/></h5> 
					<div class="divider bottom-margin "></div>		
					<%@ include file="attachmentsUpdate.jspf" %>
				</div>
<!-- fine attachments -->
<% } %>

<%if(fiwareEnabled ) {  %>
			
<!-- GENERIC ENABLERS -->			
				<div class="section" id="ge-panel" >
					<h5 class="header">Generic Enablers <liferay-ui:icon-help message="ims.tip-generic-enablers-section"/></h5>
					<div class="divider bottom-margin "></div>	
						<%@ include file="/html/ideas/genericenablers_selection.jspf"%>
				</div>
<!-- end GENERIC ENABLERS -->
<%} %>
			
<!-- #######�import COWORKERS ####### -->
			<% 	if(id == -1 || CLSIdeaLocalServiceUtil.getCLSIdea(id).getUserId()==currentUser.getUserId()){ %>
				<div class="section" id="cw-panel" helpMessage="ims.tip-coworkers-add"> 
					<h5 class="header"><liferay-ui:message key="ims.coworkers"/> <liferay-ui:icon-help message="ims.tip-coworkers"/></h5> 
					<div class="divider bottom-margin "></div>	
						<%@ include file="/html/ideas/coworkers_selection.jspf"%>
				</div>
			<%}%>
<!-- #######�fine Collaborators ####### -->
<% if (isAuthor){ %>
	<%@ include file="/html/ideasexplorermaterial/ideasdetails/sections/inviteByEmail.jspf" %>
<% } %>

	<div class="divider bottom-margin "></div>	
	
				
			
			<!-- Pulsanti Salva e Cancella della pagina -->
		
			<div id="mainFormButtons" class="material-design-restyle btn--raised btn--colored single-btn-space" >
				<aui:button-row>
					<aui:button cssClass="waves-effect waves-light btn core-color color-1" type="button" value="save" id="submitForm"  />
					<aui:button cssClass="waves-effect waves-light btn core-color color-1" type="cancel" value="cancel" onClick="history.go(-1);"/>			
				</aui:button-row>
			</div>
		

		</aui:form>
			<div id="overlay">
				<aui:form name="newPOIForm" id="newPOIForm">
					<span id="POIDiplayCoordinates"></span>
					<aui:input name="POITitle" id="POITitle" label="title" value="" type="text" />
					<aui:input name="POIDescription" id="POIDescription" label="description" value="" type="textarea" />
					<aui:input name="POILatitude"  id="POILatitude"   value="" type="hidden" /> 
					<aui:input name="POILongitude" id="POILongitude"  value="" type="hidden" /> 
					<aui:button type="button" value="add" id="addPOI"/>
					<aui:button type="reset" value="cancel"  id="cancelAddPOI"/>
			  	</aui:form>
			</div>

		
		<form style="display: none;" method="POST" id="fm2" name="fm2" enctype="multipart/form-data">
		     <input type="file" name="myFile" id="myFile" onchange="imgsubmit()"/>
		     <input type="submit" id="btnsubmit" value="Save"/>
		</form>		

</div><!-- fine css-class-wrapper-update-fake -->


</div><!-- end Materialize -->
		
<script>
		
		var editor;
		
		function changeEnter() {
			// If we already have an editor, let's destroy it first.
			if ( editor )
				editor.destroy( true );
			
			var token = Liferay.authToken;
			var listUrl = "<%=themeDisplay.getPortalURL()%>/api/jsonws/Challenge62-portlet.clsidea/get-idea-images/idea-id/<%=ideaId%>?p_auth=";
			listUrl = listUrl+token;
		
			// Create the editor again, with the appropriate settings.
			editor = CKEDITOR.replace( 'ideaDesc');
			CKEDITOR.config.allowedContent = true; /*questa chiamata permette di caricare i contenuti di youtube a runtime */

		}
		
		window.onload = changeEnter;
		
		function submitFormData() {
			var editorText = CKEDITOR.instances.ideaDesc.getData();
			document.getElementById('<portlet:namespace/>ideaDescription').setAttribute('value', editorText);
			
			$("#<portlet:namespace />fm").submit();
		}
		

		
		
		
		function gestioneSelettori(){
			
			if ($("#radioChallengeMunicType1").is(":checked")){
												
				$("#municipalityIdSpan").css('display','none');
				$("#challengeSelectionIdSpan").css('display','block');
				$("#mapContainer").css('display','none');
				$("#mapContainer").parent().removeClass('l7');
				$(".contentUpload").parent().removeClass('l5');
				$("#linguaUtenteSenzaGara").css('display','none');
				$("#linguaUtenteConGara").css('display','inline-block');
				$("#linguaUtenteConGara-tip").css('display','inline-block');
				$("#suggestionCatWithoutChallenge").css('display','none');
				$("#suggestionCatWithChallenge").css('display','inline-block');
				
				updateCategories();		
				
			}else{
				$("#mapContainer").css('display','block');
				$("#mapContainer").parent().addClass('l7');
				$(".contentUpload").parent().addClass('l5');
				$("#municipalityIdSpan").css('display','block');
				$("#linguaUtenteSenzaGara").css('display','block');
				$("#linguaUtenteConGara").css('display','none');
				$("#linguaUtenteConGara-tip").css('display','none');
				$("#suggestionCatWithoutChallenge").css('display','inline-block');
				$("#suggestionCatWithChallenge").css('display','none');
				$("#challengeSelectionIdSpan").css('display','none');	
				
				
				
				document.getElementById("messaggioNoCategory").style.display="none";
            	$( ".contentCategory" ).show();
				
				 $( ".nav-tabs > li" ).each(function( index ) {
                	                 	  
            		$( this ).show();
                	    
                });
				
			}														
		}
		
		
		
		function  updateCategories(){
			
			RemoveAllCategories();/* Rimuovo le categorie gia selezionate */
			
			var idgara = document.getElementById("<portlet:namespace/>challengeSelectionId").value;
			
			//per tirare con Ajax, nelle idee, le categorie abilitate nelle gare
			 AUI().use('aui-io-request', function(A){
			    	
					
			 
			        A.io.request('<%=resourceURL.toString()%>', {
			               method: 'post',
			               data: {
			            	   <portlet:namespace />param: 'updateCategories',
			            	   <portlet:namespace />challengeChoosen: idgara,
			            	   <portlet:namespace />localePerAssetVoc: '<%=locale%>',
			            	   
			               },
			               dataType: 'json',
			               on: {
			                   success: function() {
			                    
			                	   
				                    ajRitornoAggiorno = this.get('responseData').vocabolari;
				                    				         
				                    var splita = ajRitornoAggiorno.split(',');

				                    if (splita.length == 1){ //se non ci sono categorie, 1 perch� una virgola c'e' sempre
				                    	
				                    	$( ".contentCategory" ).hide();//cancello tutto il tab delle categorie				                    
				                    	document.getElementById("messaggioNoCategory").style.display="inline";//mostro msg no category

				                    
				                    }else{
				                    	//se ci sono categorie
				                    	
				                    	document.getElementById("messaggioNoCategory").style.display="none";//nasconto msg no category
				                    	$( ".contentCategory" ).show();//mostro  il tab delle categorie	
				                    	
										//scorro le categorie selezionate
					                    $( ".nav-tabs > li" ).each(function( index ) {
					                    	 // console.log( index + ": " + $( this ).text() );
					                    	  
					                    	  trovato=false;
					                    	  for ( i=0; i <splita.length-1; i++){
							                    					                    		  					                    		  
					                    		  if ($( this ).text().trim() == splita[i].trim()){
					                    			  trovato=true;				                    			  
					                    		  }
					                    		 
							                    }
					                    	  
					                    	  if (trovato == false){
					                    		
					                    		  $( this ).hide();//nascondo tab
					                    		  
					                    	  }else{
					                    		  
					                    		  $( this ).show(); //mostro tab, se non lo metto li cancella tutti				                    	
					                    		  this.firstElementChild.click();//per prendere il focus e aggiornare sotto
					                    	  }				                    	  
					                    	});	
				                    
				                    }
				                    
			                   }//success
			              }//on					               
			        });//A.io.request					 
			    });//AUI().use	
			
			
		}
		
		

//devo poter selezionare solo da un tab,quindi appena clickko
// deseleziono tutti i checkbox sotto gli altri tab		
function RemoveAllCategories(){
	
	var ns=$( ".category-item.mdl-checkbox__input" );//tutti i checkbox per classe
	
	
	$.each(ns, function(i){
		
		$(this).closest('label').removeClass('is-checked'); //parte grafica
		$(this).prop('checked', false); //parte logica
		
	})
}
		
//controllo che ci sia almeno una categoria selezionata
function checkCategories(){
	
	catTrovata = false;
	var ns=$( ".category-item.mdl-checkbox__input" );//tutti i checkbox per classe
	
	$.each(ns, function(i){
		
		if ($(this).is(':checked')){
			catTrovata = true;
		}
	})
	
	return catTrovata;
}

//Only one main category (Tab) must be choosen
function checkOnlyOneMainCategory(){
	
	var ns=$( ".category-item.mdl-checkbox__input" );//tutti i checkbox per classe
	
	
	var catSelezionate = [];
	
	$.each(ns, function(i){
		
		if ($(this).is(':checked')){
			
			var cBoxId = $(this).attr('id').replace("checkbox-", ""); //was checkbox-1-2, now 1-2
			var cBoxMainCatId= cBoxId.substr(0, cBoxId.indexOf('-')); 
			catSelezionate.push (cBoxMainCatId);
			
		}
		
	})
	
	if (allTheSame(catSelezionate)){
		return false;
	}
		
	//else
	return true;
}

//check if all elements of an array are equal
function allTheSame(array) {
    var first = array[0];
    return array.every(function(element) {
        return element === first;
    });
}

$( document ).on("change", ".category-item.mdl-checkbox__input", function() {
	 
	suggestGEsByCategories();
});


$(document).ready(function(){
	suggestGEsByCategories();
});

/*Suggest GE basing on Categories mapping */
function suggestGEsByCategories(){
	
	var ns=$( ".category-item.mdl-checkbox__input" );//all checkboxes for class
	var selectedCat = [];
	var jsonCat;
	$.each(ns, function(i){
		
		if ($(this).is(':checked')){
			
			var cBoxId = $(this).attr('id');
			var labelCat = document.getElementById('label-'+cBoxId).textContent /*take label for category */
			selectedCat.push (labelCat);
			jsonCat = JSON.stringify(selectedCat);
			
		}
		
	});
	
	
	//GO to the server by Ajax
	 AUI().use('aui-io-request', function(A){
	    	
	 
	        A.io.request('<%=resourceURL.toString()%>', {
	               method: 'post',
	               data: {
	            	   <portlet:namespace />param: 'mapCategories',
	            	   <portlet:namespace />jsonCat: jsonCat,
	               },
	               dataType: 'json',
	               on: {
	                   success: function() {
	                    
	                	   var jsonCatgoriesArray = this.get('responseData');
	                	   
	                	   var ges=$( ".templateGE" );
	                	   
	                	   
	                	   $.each(ges, function(index, ge) {
	                		   
	                		   var catGE = $(ge).find('.field-name-field-api-chapter').text() ;
	                		   
	                		  var isInArray = $.inArray(catGE,jsonCatgoriesArray );
	                		  
	                		  if (isInArray> -1){
	                			  
	                			  $(ge).find('.favourite').removeClass("hide") ;
	                		  }else{
	                			  
	                			  $(ge).find('.favourite').addClass("hide") ;
	                		  }
	                		  
	                		  
	                	   });
	                	   
	                   }//success
	              }//on					               
	        });//A.io.request					 
	    });//AUI().use	
	
}



/* returns the selected categories, which are used with Ajax for the recommendation of the collaborator */
function getCategories(){
	
	
	var ns=$( ".category-item.mdl-checkbox__input" );//tutti i checkbox per classe
	
	var categories = new Array();
	y = 0;
	$.each(ns, function(i){
		
		if ($(this).is(':checked')){ 
			
			var checkboxCurrentId = $(this).attr('id');
			var catName = $("#label-"+checkboxCurrentId).text();
			
			if (catName!=""){
				categories[y] = catName;
				y++;
			}
		}
	})
	
	return categories;
}


/* returns the selected tags, which are used with Ajax for the recommendation of the collaborator */
function getTags(){
	
	
	var ns=$( ".textboxlistentry-text" );//tutti i tag per classe
	
	var tags = new Array();
	
	var k=0;
	$.each(ns, function(i){
			
			nomeTag = $(this).text();
			
			if (nomeTag!=""){
				tags[k] = nomeTag;
				k++;
			}
		
	})
	
	return tags;
}

function getTagsNoTaglib(){
	
	
	var ns=$( ".tagname" );//tutti i tag per classe
	
	var tags = new Array();
	
	var z = 0;
	$.each(ns, function(i){
			
		var hC = $(this).hasClass( "hide" ).toString();
		
			if ($(this).is(":visible")){//remove the chip template
				
				nomeTag = $(this).text();
				
				if (nomeTag!=""){
					
					tags[z] = nomeTag;
					z++;
				}
			}
	})
	
	return tags;
}			
		
		
		
$(document).ready(function(){
	
			//Iserisce una classe di comodo nella section 
			//ad insicare che si tratta di una nuova idea e non di una modifica
			<%if(ideaId.equals("-1")){%>
				$(".section").addClass("newCreation");
			<%}%>
			
			
			
			<% if(challengeId == -1){ %>
				//caricamento selettori alternativi gare/enti
				gestioneSelettori();
			<%}%>
			

			
			var uploadObj =$("#advancedUpload").uploadFile({
				formData: { token: '<%=uuid%>'},
				url:"<%= editCaseURL %>",
				autoSubmit:false,
				fileName:"myfile",
				showProgress:true,
				dragDropStr : '<em><liferay-ui:message key="ims.drag-e-drop-files"/></em>',
				uploadStr:'<liferay-ui:message key="ims.add-document"/>',
	            cancelStr: '<liferay-ui:message key="cancel"/>',
	            deletelStr: '<liferay-ui:message key="delete"/>',
	            extErrorStr: '<liferay-ui:message key="ims.is-not-allowed-allowed-extensions"/>',
	            duplicateErrorStr: '<liferay-ui:message key="ims.is-not-allowed-allowed-file-already-exists"/>',
	            sizeErrorStr: '<liferay-ui:message key="ims.is-not-allowed-allowed-max-size"/> ',
	            dragdropWidth: 'auto',
	            statusBarWidth: 'auto',
	            allowedTypes: "<%=PortletProps.get("upload-allowed-extension") %>",
	            maxFileSize:<%=PortletProps.get("upload-max-bytes-size") %>, /*20MB*/
				showStatusAfterSuccess:false,
				afterUploadAll:function(obj){
					submitFormData();
				}
				});
			
			
				$("#submitForm").click(function(){
					
					//chiamata Ajax per vedere se i titoli gia' esistono
					 AUI().use('aui-io-request', function(A){
					    	
						 var ideaTitle = document.getElementById('<portlet:namespace/>ideaTitle').value;
					 
					        A.io.request('<%=resourceURL.toString()%>', {
					               method: 'post',
					               data: {
					            	   <portlet:namespace />param: 'titoloIdea',
					            	   <portlet:namespace />titleChoosen: ideaTitle,
					            	   <portlet:namespace />modifica: '<%=ideaId%>'
					               },
					               dataType: 'json',
					               on: {
					                   success: function() {
					                    
						                    ajRitornoTitleExist = this.get('responseData').titleExist;
						                    
						                   ckCat = checkCategories();
						                   ckOnlyMainCat = checkOnlyOneMainCategory();
						                    
						                   if (ideaTitle.trim() == "") {
												alert('<liferay-ui:message key="ims.insert-title"/>');
						                   }else if(ajRitornoTitleExist == 'yes'){
					                			alert('<liferay-ui:message key="ims.existing-title"/>');	
																							
						                   }else if( ckCat == false ){
							                    alert('<liferay-ui:message key="please-select-a-category"/>');	
						                   }else if( ckOnlyMainCat == true ){
							                    alert('<liferay-ui:message key="ims.alert-more-main-categories"/>');     
						                   }	
						                    else if(CKEDITOR.instances.ideaDesc.getData().trim() == ""){
												alert('<liferay-ui:message key="ims.insert-description"/>');
											}else{

												if(uploadObj.getFileCount()>0){		 
													uploadObj.startUpload();
												 }else{
													 submitFormData();
												 }
											}//if else					                  	                   	                	
					                   }//success
					              }//on					               
					        });//A.io.request					 
					    });//AUI().use					
				});//$("#submitForm").click
				
				
				
				$('.next-button').click(function(e){
					e.preventDefault();
					var gotoId = $(this).data('nextid');
					scrollPage(gotoId);
				});
				
				/* Manage mouse hover sample image */
				/*
				$('#imgDiv').mouseenter(function(){
					$(this).find('span').stop().animate({opacity: '1'}, 'slow');
				}).mouseleave(function(){
					$(this).find('span').stop().animate({opacity: '0'}, "slow");
				})
				*/
				
		});
		
		function scrollPage(elementId){
			var position = $('#'+elementId).offset().top;
			$("html, body").animate({scrollTop:(position-45)}, 'slow');
			return;
		}
		
		function deleteFile(fileId) {
			if (confirm('<liferay-ui:message key="ims.delete-document-message"></liferay-ui:message>')) { 
				var url = "<%=deleteFileURL.toString()%>";
				
				Liferay.Service(
							'/Challenge62-portlet.clsidea/delete-idea-file',
						  {
						    fileEntryId: fileId
						  },
						  function(obj) {
// 						    console.log(obj);
						    $( "#div_"+fileId).remove();
						  }
						);
			}
		}
		</script>
		
		
		
		
		<!-- Gestione immagine rappresentativa script -->
		<script charset="utf-8" type="text/javascript">
		function <portlet:namespace/>selectRepresentativeImg(imgURL){
// 			console.log(imgURL);
			document.getElementById('<portlet:namespace/>representativeImgUrl').setAttribute('value', imgURL);
			var pic1 = document.getElementById("<portlet:namespace/>article-image");
			pic1.src = imgURL;
		}
		</script>
		
		<script>
		function imgsubmit() {
			 $("#btnsubmit").trigger("click");
		}
		
		function flb() {
			 $("#myFile").trigger("click");
		}
		</script>
		
		
		<script>
// 		function makeBreadCrumb() {
			
// 		var ns=$( ".breadcrumb" );//ritorna un UL	
			

		
// 		$('.breadcrumb li:last-child')
		
			
			
// 		}
		
		
		</script>
		
		
		
		
		<!-- Gestione immagine rappresentativa tramite Ajax -->
		<aui:script use="aui-io-request,aui-node" >
		
		YUI().use(
		  'aui-node',
		  'aui-io-request',
		  function(Y) {
		 
		  var node = Y.one('#btnsubmit');
		var img=Y.one('#<portlet:namespace/>representativeImgUrl');
		    
		    node.on(
		      'click',
		      function(){
		      
		      var A = AUI();
		  A.io.request('<%=resourceURL.toString()%>',{
		  method: 'POST',
		  form: { id: 'fm2', upload: true },
		  data: {
					  <portlet:namespace/>param:'caricamento',
						
					},
		  
		  on: {
		  complete: function(){
		   	
		  	
		  	display();
		  	
		   
		       }
		     }
		    });
		      }
		    );
		    
		    
		    function display(){
		   
		    
		       Y.io.request('<%=resourceURL.toString()%>',
		 			   
			            {
				  data: {
					  //qua mandiamo il parametro vista
					  
					  <portlet:namespace/>param:'view',
						
					},
			              dataType: 'json',
			              on: {
			                success: function() {
			                	
			                	var data = this.get('responseData');
			                	
			                	img.set("value", data.value);
		  						var pic1 = document.getElementById("<portlet:namespace/>article-image");
		  						
		  						pic1.src =data.value;
			                	
			                }
			              }
			            }
			          );
			  
		    }
		  }
		);
		</aui:script >
		
