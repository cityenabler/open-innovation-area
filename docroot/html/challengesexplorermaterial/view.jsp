<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.CLSCategoriesSetLocalServiceUtil"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="it.eng.rspa.ideas.utils.MyConstants"%>
<%@page import="it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties"%>
<%@page import="com.liferay.portal.model.User"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@page import="it.eng.rspa.ideas.utils.MyUtils"%>
<%@page import="it.eng.rspa.ideas.utils.IdeasListUtils"%>
<%@page import="javax.portlet.WindowState"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %> 
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>
<portlet:resourceURL var="resourceURL" />

<portlet:defineObjects />
<liferay-theme:defineObjects/>

<%@ include file="/html/home/sections/checkPilot.jspf" %>

<%
User currentUser = PortalUtil.getUser(request);
boolean isIMSSimpleUser =  MyUtils.isIMSSimpleUser(currentUser);
boolean isEnte = MyUtils.isAuthority(currentUser);

String pilot = IdeasListUtils.getPilot(renderRequest);
String pilotUI = MyUtils.getUIPilotByPilotId(pilot);
String urlHomeIms = IdeaManagementSystemProperties.getUrlHomeIMS();
boolean reducedLifecycle = IdeaManagementSystemProperties.getEnabledProperty("reducedLifecycle");

List<CLSCategoriesSet> categoriesList = CLSCategoriesSetLocalServiceUtil.getCLSCategoriesSets(0, CLSCategoriesSetLocalServiceUtil.getCLSCategoriesSetsCount());
	

%>

<div class="materialize">
		
	  	<div class="section row valign-wrapper" style="position: relative; height: 70px;">
	  	
	  		<div class="col s4 valign left">
	  			<a class="waves-effect btn-flat tooltipped" href="<%=urlHomeIms %>" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.go-back"/>">
	  				<i class="icon-arrow-left left">&nbsp;</i><liferay-ui:message key="back"/>
	  			</a>
	  		</div>
			<div class="col s4 valign center">
					
			<% if (isEnte){	%>
	  			<a class="waves-effect btn core-color color-2 tooltipped" href="<portlet:renderURL windowState='<%=WindowState.MAXIMIZED.toString() %>' ><portlet:param name='jspPage' value='/html/challenges/update.jsp' /> 
										</portlet:renderURL>" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.create.challenge"/>"> 
	  				
	  				<% if (reducedLifecycle){	%>
	  					<span class="icon-trophy">&nbsp;</span><span class="hide-on-small-only"><liferay-ui:message key="create"/></span>
	  				<%}else{	%>	
	  					<span class="fa <%=MyConstants.ICON_CLG_REDUCED %>">&nbsp;</span><span class="hide-on-small-only"><liferay-ui:message key="create"/></span>
	  				<%}%>	
	  			</a>
	  			
		  	<%	}else{%>
		  	
		  		<h3 class="flow-text strong"><liferay-ui:message key="ims.challenges"/></h3>
		  		<div class="divider"></div>
		  	
		  	<%} %>	
	  	
	  		</div>
			<div class="col s4 valign right">
				  <div class="fixed-action-btn horizontal" style="position: absolute; top:7px; z-index:300;">
				    <a class="btn-floating btn-large waves-effect btn-large core-color color-2 tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.tip-wrench-challenges-explorer"/>">
				      <span class="icon-wrench"></span>
				    </a>
				    <ul>
				    	<!-- Modal Trigger -->
				      <li><a class="modal-trigger-challenge btn-floating waves-effect core-color color-1 tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="filters"/>" href="#modal-filter">
				      	<i class="icon-filter"></i></a></li>
				      	<!-- Modal Trigger -->
				      <li><a class="modal-trigger-challenge btn-floating waves-effect core-color color-1 tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="sort-by"/>" href="#modal-sorting">
				      	<i class="material-icons">sort_by_alpha</i></a></li>
				    </ul>
				  </div>
			 </div>
			 
		</div>

      	<%@ include file="filter-panel.jspf" %>
      	<%@ include file="sorting-panel.jsp" %>
      	
      	<div class="row container-card">
      		<%@ include file="../loader.jspf" %>
      		<!-- qui attacco le gare create con Ajax -->
      			
 		</div> <!-- Fine Row Gare -->
 		
 		<%@ include file="challenge-card.jspf" %>
</div>

<script>
    
	(function($){
		  $(function(){
				
				//Attiva la modale
				$('.modal-trigger-challenge').leanModal({
				    dismissible: true, // Modal can be dismissed by clicking outside of the modal
				    opacity: .5, // Opacity of modal background
				    in_duration: 300, // Transition in duration
				    out_duration: 200, // Transition out duration
				  //  ready: function() { console.log('Ready'); }, // Callback for Modal open
				    complete: function() { //alla chiusura del pannello dei filtri
				    	
				    	listaGareAjax();
				    } // Callback for Modal close
				  }
				);
				
				//Attiva le select
				$('.aui .materialize select').material_select();
	
				//Attiva gli switch legati ad una checkbox
				$( ".activeSort" ).change(function() {
					  var $input = $( this );
					  var $lever = $input.parent().parent().find( ".switch input" );
					  
					  if($input.is( ":checked" )) {
						  $lever.prop( "disabled", false );
					  }else {
						  $lever.prop( "disabled", true );
					  }
					  
				}).change();
				
				listaGareAjax();
				
				$(document).ready(function(){
				    setTimeout(function(){ $('.tooltipped').tooltip({delay: 50});}, 3000);
				});
				
		  }); // end of document ready
		  
		  
		  function  listaGareAjax(){
				
				//chiamata Ajax per caricare la lista delle idee
				 AUI().use('aui-io-request', function(A){
					 
					 //if inline per settare i valori dei filtri sugli stati
					  (($('#state1').is(":checked"))? stateOpen = "true" : stateOpen = "false");
					  (($('#state2').is(":checked"))? stateEvaluation = "true" : stateEvaluation = "false");
					  (($('#state3').is(":checked"))? stateEvaluated = "true" : stateEvaluated = "false");
					 

	 					//if inline per settare i valori dei filtri sui vocabulary di Liferay-categorie nella GUI
	 					
	 	<%			for (int i = 0; i< categoriesList.size(); i++){	%>
	 	
	 	 				(($('#cat4<%=i %>').is(":checked"))? cat<%=i %> = "true" : cat<%=i %> = "false");
	 	
	 	<%			}	%>
	 	
	 
					  
					  (($('#pers1').is(":checked"))? pers1 = "true" : pers1 = "false");
					  (($('#pers2').is(":checked"))? pers2 = "true" : pers2 = "false");
					  (($('#pers3').is(":checked"))? pers3 = "true" : pers3 = "false");
					  (($('#pers4').is(":checked"))? pers4 = "true" : pers4 = "false"); 
					  (($('#pers5').is(":checked"))? pers5 = "true" : pers5 = "false"); 
					  
						//if inline per l'ordinamento delle liste
					   (($('#startDate').is(":checked"))? sortByStartDate = "true" : sortByStartDate = "false");
					   (($('#sortDateStart').is(":checked"))? sortStartDateAsc = "true" : sortStartDateAsc = "false");
					   (($('#endDate').is(":checked"))? sortByEndDate = "true" : sortByEndDate = "false");
					   (($('#sortDateEnd').is(":checked"))? sortEndDateAsc = "true" : sortEndDateAsc = "false");
					   (($('#ratingSort').is(":checked"))? sortByRating = "true" : sortByRating = "false");
					   (($('#rateSort').is(":checked"))? sortRatingLower = "true" : sortRatingLower = "false");
					 
					
				     A.io.request('<%=resourceURL.toString()%>', {
				               method: 'post',
				               data: {
				            	<portlet:namespace />param: 'ChallengesList',
				            	<portlet:namespace />localePerAssetVoc: '<%=locale%>',
				            	<portlet:namespace />pilot: '<%=pilot%>',
				            	<portlet:namespace />stateOpen: stateOpen,
					            <portlet:namespace />stateEvaluation: stateEvaluation,
					            <portlet:namespace />stateEvaluated: stateEvaluated,
				            	
					            
					    <%			for (int i = 0; i< categoriesList.size(); i++){	%>    
					            <portlet:namespace />cat<%=i %>: cat<%=i %>,
					            
					     <%	}%>        
					            
					            
					            <portlet:namespace />pers1: pers1,
					            <portlet:namespace />pers2: pers2,
					            <portlet:namespace />pers3: pers3,
					            <portlet:namespace />pers4: pers4,
					            <portlet:namespace />pers5: pers5,
					            <portlet:namespace />sortByStartDate: sortByStartDate,
					            <portlet:namespace />sortStartDateAsc: sortStartDateAsc,
					            <portlet:namespace />sortByEndDate: sortByEndDate,
					            <portlet:namespace />sortEndDateAsc: sortEndDateAsc,
					            <portlet:namespace />sortByRating: sortByRating,
					            <portlet:namespace />sortRatingLower: sortRatingLower
				            	   
				               },
				               dataType: 'json',
				               on: {
				                   success: function() {
				                    				                	 
				                	   var jsonChallengesArray = this.get('responseData');
					                   appendMoreChallenges(jsonChallengesArray);
					                   
					                   $('.tooltipped').tooltip({delay: 50}); /*initialize the tooltip */
					     				                  	                   	                	
				                   }//success
				              }//on					               
				        });//A.io.request					 
				 });//AUI().use
				 
			}//listaGareAjax
		  
		  
		  function appendMoreChallenges(jsonChallengesArray){
				
			  var contenitore = jQuery('.container-card');
			  
			  
			  var templateP = jQuery('.schedaGara').first();
				$(contenitore).empty();
			  
				jQuery.each(jsonChallengesArray, function(index, gara) {
					
			 		var template = templateP.clone();
			 		
			 		template.css('display','block');
			 		template.removeClass('schedaGara');//pulizia
			 		
			 		
			 		template.find('.card').attr("id","challenge"+gara.idChallenge);
			 		
			 		if (gara.Vocabulary != ""){
				 		
			 			template.find('.titoloVocabolario').text(gara.Vocabulary);
			 			template.find('.titoloVocabolario').parent().attr("data-tooltip",gara.Vocabulary);
			 		}else{
			 			
			 			template.find('.titoloVocabolario').text("<liferay-ui:message key='ims.no-category-associated'/>");
			 			template.find('.titoloVocabolario').parent().attr("data-tooltip","<liferay-ui:message key='ims.no-category-associated'/>");
			 		}
			 		
			 		template.find('.iconaVocabolario').addClass( gara.iconVocabulary );
			 		template.find('.coloreVocabolario').addClass( gara.colorVocabulary );
			 		
			 		template.find('.dropdown-buttonMore').attr("data-activates","dropdown-"+index);
			 		template.find('.garaLink').attr("href", gara.GaraFriendlyUrl);
			 		template.find('.numGaraDropdown').attr("id","dropdown-"+index);
			 		
			 		var titleLink = template.find('.titoloGara');
					template.find('.card-multi-title').text(gara.Title);
// 					template.find('.card-multi-title').append(titleLink);
			 		template.find('.titoloGara').attr('href',gara.GaraFriendlyUrl);
			 		
			 		template.find('.autoreGara').text(gara.AuthorOrganization);
			 		
			 		<% if (isEnte){ //nel caso di ente faccio vedere anche l'autore esplicito oltre l'organization %>
			 			template.find('.autoreGara').text(gara.Author + " - "+gara.AuthorOrganization);
			 		<% } %>
			 		
			 		
			 		template.find('.dataInizioGara').text(gara.StartDate);
			 		template.find('.dataFineGara').text(gara.EndDate);
			 		
			 		var numStelline = gara.RatingsStar;
					 
			 		var stellineS="";
				 	for (i = 0; i < numStelline; i++) {
						 stellineS+='<i class="icon-star"></i>';
				 	}
				 	for (i = 0; i < 5-numStelline; i++) {
						 stellineS+='<i class="icon-star-empty"></i>';
					}
								 
				 	template.find('.votoMedio').html(stellineS);
				 	
				 	template.find('.imgRappresentativa').attr('src',gara.ImageURL);
				
					if(gara.ImageURL.indexOf('-portlet/img/') > -1) {
						template.find('.sampleText').html("<liferay-ui:message key='ims.sample-text'/>");
						template.find('.sampleText').css({'display':'block','position':'relative'});
					}else{
						template.find('.sampleText').css({'display':'none'});
					}
					
// 					template.find('.descrizioneGara p.descrLink a.moreDescr').html(gara.Description);
// 					template.find('.descrizioneGara p.descrLink a.moreDescr').attr('href', gara.GaraFriendlyUrl);
// 					template.find('.descrizioneGara a.readmore').attr('href', gara.GaraFriendlyUrl);
					template.find('.descrizioneGara p.descrLink').html(gara.Description);
				 	
				 	template.find('.numIdeas').text(gara.NumIdeas);
				 	
				 	template.find('.iconaStato.'+gara.Status).removeClass("grey-text").addClass(" white-text");
				 	
				 	statoTradotto = getUilabelState(gara.Status);
				 	template.find('.statoChallenge.'+gara.Status).removeClass("grey-text").addClass("white-text").html(statoTradotto);
				 	
				 	if (gara.isAuthor){
				 		template.find('.garaLink').html('<liferay-ui:message key="ims.view-or-manage-challenge"/>');
				 		template.find('.viewChallenge').attr("data-tooltip",'<liferay-ui:message key="ims.view-or-manage-challenge"/>' );
				 	}
				 	
				 	if (gara.isActive){
				 		template.find('.statusChallengeClose').addClass("hidden");
				 	}else{
				 		template.find('.statusChallengeOpen').addClass("hidden");
				 	}
				 	
				 	jQuery(contenitore).append(template);
				 	
				 	//rende tutto il badge cliccabile
// 				 	$("#challenge"+gara.idChallenge).on("click", function(){
// 				 	     window.location.href=gara.GaraFriendlyUrl;
// 				 	 })

				 	
				});//foreach jsonArray
				
				if (jsonChallengesArray.length == 0 ){
					jQuery(contenitore).append("<p class='infoText bottomSpace'><liferay-ui:message key='ims.no-challenge'/></p>");
				}
				
				/*  Permette Ellipsis su multiriga */
				/*
				$('.multi-truncate').dotdotdot({
					ellipsis: '...',
					height: 120,
					watch: 'window',
					after: 'a.readmore'
				});
				*/
				
				/* Hide READMORE if description have an height under 120px (max limit) */
			 	$('.descrLink').each(function( index ) {
			 		$this = $( this );
// 			 		console.log(index+" "+$this.height());
			 		 if($this.height() < 100) {
			 			$this.parent().find("a.readmore").css("display", "none")
			 		};
			 	});
				
			 	$('.dropdown-buttonMore').dropdown({
			 	      inDuration: 300,
			 	      outDuration: 225,
			 	      constrain_width: false, // Does not change width of dropdown to that of the activator
			 	      hover: true, // Activate on hover
			 	      gutter: 0, // Spacing from edge
			 	      belowOrigin: false, // Displays dropdown below the button
			 	      alignment: 'left' // Displays dropdown with edge aligned to the left of button
			 	    }
			 	  );

			}//funzione	appendMoreChallenges

	})(jQuery); // end of jQuery name space
	
	
    function getUilabelState (state){
		
    	switch(state) {
        case "<%=MyConstants.CHALLENGE_STATE_OPEN%>": 
            return '<liferay-ui:message key="ims.open"/>';
        case "<%=MyConstants.CHALLENGE_STATE_EVALUATION%>": 
            return '<liferay-ui:message key="ims.evaluation"/>';
        case "<%=MyConstants.CHALLENGE_STATE_EVALUATED%>": 
            return '<liferay-ui:message key="ims.evaluated"/>';
        default:
            return '';
    	}	
	}
	
	

</script>

