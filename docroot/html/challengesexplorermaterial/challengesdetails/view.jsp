<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.VirtuosityPointsLocalServiceUtil"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.NeedLinkedChallengeLocalServiceUtil"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.impl.CLSFavouriteChallengesLocalServiceImpl"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges"%>
<%@page import="it.eng.rspa.ideas.utils.MyUtils"%>
<%@page import="it.eng.rspa.ideas.utils.ChallengesUtils"%>
<%@page import="java.util.ResourceBundle"%>

<%@page import="com.liferay.portal.kernel.util.StringPool"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts"%>
<%@page import="java.util.List"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.CLSArtifactsLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portal.model.User"%>
<%@page import="it.eng.rspa.ideas.utils.IdeasListUtils"%>
<%@page import="it.eng.rspa.ideas.utils.ViewIdeaUtils"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@page import="java.util.Locale" %>
<%@page import="it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties"%>
<%@page import="org.apache.commons.lang3.StringEscapeUtils"%>
<%@page import="com.liferay.portal.kernel.util.StringUtil"%>
<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteIdeasLocalServiceUtil"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas"%>

<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects"%>
<%@page import="com.liferay.portlet.documentlibrary.model.DLFileEntry"%>
<%@ include file="/html/challenges/init.jsp" %>
<portlet:defineObjects />

<!-- CSS -->
<link href="<%=request.getContextPath()%>/css/share-social.css" rel="stylesheet">
<link href="<%=request.getContextPath()%>/css/show.css" rel="stylesheet">

<!-- SCRIPT -->
<script src="<%=request.getContextPath()%>/js/social-share/jquery.prettySocial.js"></script>

<liferay-theme:defineObjects />


<%

String challengeIdS = request.getParameter("challengeId");
User currentUser = PortalUtil.getUser(request);
boolean isEnte = MyUtils.isAuthority(currentUser);

long challengeId = Long.valueOf(challengeIdS).longValue();

CLSChallenge challenge = null;
try{
	challenge =  CLSChallengeLocalServiceUtil.getCLSChallenge(challengeId);
		
}catch(Exception e){ 
	
	System.out.println(e.getMessage()); 
	String errorMessageKeyI18n = "ims.missing-resource";
%>
<%@ include file="/html/home/pages/goToErrorPage.jspf" %>
<%return;

}//try-catch

%>
<%@ include file="/html/challengesexplorermaterial/challengesdetails/sections/checkPilotOnChallengeDetail.jspf" %>

<%

String coloreVocabolario = ChallengesUtils.getColorForVocabularyByChallengeId(challengeId);
String urlImg = ChallengesUtils.getRepresentativeImgUrlByChallenge(challenge, request.getContextPath());
String vocabolario = ChallengesUtils.getVocabularyNameByChallengeId(challengeId, locale);
String iconaVocabolario = ChallengesUtils.getIconForVocabularyByChallengeId(challengeId);
String tags =ChallengesUtils.getTagsVirgoleByChallengeId(challengeId);
String titolo = challenge.getChallengeTitle();
String viewAuthorName = ChallengesUtils.getAuthorNameByChallengeAndUser(challenge, currentUser);

List<DLFileEntry> files = ChallengesUtils.getFilesByChallenge(challenge);

boolean isGaraWithPoi = ChallengesUtils.isGaraWithPoi(challenge);
boolean isFavoriteChallenge = ChallengesUtils.isFavoriteChallenge(challenge, currentUser);
boolean isAuthor = ChallengesUtils.isAuthor(challenge, currentUser);

boolean isChallengeWhichCanProposeIdeas = ChallengesUtils.isChallengeWhichCanProposeIdeas(challenge, renderRequest);
boolean isChallengeWhichCanDelete = ChallengesUtils.isChallengeWhichCanDelete (challengeId);
boolean isEvaluationClosable = ChallengesUtils.isEvaluationClosable (challenge);
boolean isEvaluationOpenable = ChallengesUtils.isEvaluationOpenable (challengeId);
boolean publicIdeasEnabled = IdeaManagementSystemProperties.getEnabledProperty("publicIdeasEnabled");
boolean reducedLifecycle = IdeaManagementSystemProperties.getEnabledProperty("reducedLifecycle");
		
//Proposed Ideas to this challenge		
List<CLSIdea> ideas = CLSIdeaLocalServiceUtil.getIdeasByChallengeId(challenge.getChallengeId());
		
List<VirtuosityPoints> virtuosityPoints = VirtuosityPointsLocalServiceUtil.getVirtuosityPointByChallengeId(challenge.getChallengeId());
		
		
String urlcreateIdeaWizardForChallenge = IdeaManagementSystemProperties.getUrlHomeIMS() +"?c="+challengeId;
		
PortletURL criteria=renderResponse.createRenderURL();
	criteria.setParameter("jspPage", "/html/challengesexplorermaterial/challengesdetails/pages/manageCriteria.jsp");
	criteria.setParameter("challengeId", String.valueOf(challengeId)); 
		
PortletURL scoreboard=renderResponse.createRenderURL();
	scoreboard.setParameter("jspPage", "/html/challengesexplorermaterial/challengesdetails/pages/ideaScoreboard.jsp");
	scoreboard.setParameter("challengeId", String.valueOf(challengeId));
		
PortletURL documentation=renderResponse.createRenderURL();
	documentation.setParameter("jspPage", "/html/challengesexplorermaterial/challengesdetails/pages/documentationPage.jsp");
	documentation.setParameter("challengeId", String.valueOf(challengeId));
		
%>




<portlet:resourceURL var="resourceURL" />


<!-- Per associare una idea direttamente ad una gara -->
<portlet:renderURL var="updateIdeasURL" windowState="<%=WindowState.MAXIMIZED.toString() %>">
							<portlet:param name="jspPage" value="/html/ideas/update.jsp"/>
							<portlet:param name="forChallengeId" value="<%= Long.toString(challengeId) %>"/>
</portlet:renderURL>


<!-- Social Share -->	
<%						
String shareUrl = IdeaManagementSystemProperties.getFriendlyUrlChallengesCheckHttps(challengeId, themeDisplay.isSecure());					
String imgurlshare = request.getScheme()+ "://" + request.getServerName() + ":" + request.getServerPort() + urlImg;
String summary = ChallengesUtils.getSocialSummaryByChallenge(challenge);
%>
<!-- fine Social Share -->

<div class="materialize">

		<!-- background row -->
		<div class="row row-background  <%=coloreVocabolario %> lighten-1 bottom-margin ">
			<!-- back col -->
			<div class="col s5 m3 l2 center-align">
	  			<br> 
	  			<% if (!reducedLifecycle){ %>
		  			<a class="waves-effect btn-flat tooltipped white-text" href="<portlet:renderURL>
										<portlet:param name="jspPage" value="/html/challengesexplorermaterial/view.jsp" />
									 </portlet:renderURL>" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.go-back"/>">
		  				<i class="material-icons icon-arrow-left small">&nbsp;</i><liferay-ui:message key="back"/>
		  			</a>
	  			<% }else{ %>
	  				<a class="waves-effect btn-flat tooltipped white-text" href="javascript:history.go(-1)" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.go-back"/>">
		  				<i class="material-icons icon-arrow-left small">&nbsp;</i><liferay-ui:message key="back"/>
		  			</a>
	  			<% } %>
	  			
	  			
	  		</div>
	  		<!-- category col -->
	  		<div class="col s2 m6 l8">
		  		
			         <% if (!reducedLifecycle){ %>
			         <div class="header-card-details valign-wrapper white-text tooltipped" data-position="top" data-delay="50" data-tooltip="<%=vocabolario %>"> 
			          	<i class="material-icons <%=iconaVocabolario %> circle small"></i>
				        <span class="truncate hide-on-small-only">
				             &nbsp;&nbsp;<%=vocabolario %>
				        </span>
			         <% }else{%> 
			         	<div class="header-card-details valign-wrapper white-text"> 
			          <% }%> 
				</div>
			</div>
			
			<!-- management col -->
			<div class="col s5 m3 l2 center-align ">
			
				 <!-- bottoni vari di gestione idea (stati, preferiti, modifica, cancella...) -->
				<%@ include file="sections/manageButton.jspf" %>
			</div>
		</div>
		
		<div class="row">
<!-- 			Colonna sinistra -->
			<!-- left-column per la visualizazione per Desktop e oltre -->
			<div class="col s12 m12 l2 hide-on-med-and-down" style="height:1px">
				<div class="row toc-wrapper">
					<%@ include file="left-column.jspf" %>
				</div>
			</div><!-- fine Colonna sinistra -->
			 
<!-- 			 Colonna centrale -->
		     <div class="col s12 m12 l8">
		       <div class="card breaking">
		         <div class="card-content">
	         		<h6 class="center text-details">
	      <%
	      String iconChallenge =MyConstants.ICON_CLG;
	      if(reducedLifecycle){ 
	    	  iconChallenge =MyConstants.ICON_CLG_REDUCED;
	      }
	      %>
		         		<i class="fa <%=iconChallenge %> fa-2x" aria-hidden="true">&nbsp;</i><liferay-ui:message key="ims.challenge.details"/></h6>
		         		
	           		<div class="divider bottom-margin"></div>
		         
<!-- Introduction -->
		           	<div id="introduction" class="section scrollspy">
				    	<div class="row">
							
							<!-- left-column per la visualizazione per Tablet e al di sotto -->
				    		<div class="col s12 m4 l12 hide-on-large-only">
				    			<%@ include file="left-column.jspf" %>
				    		</div>
				    		
				    		<div class="col s12 m8 l12">
				    			<div class="row">
<!-- 				    				Titolo -->
			   						<div class="col s10 card-multi-title flow-text weight-4"><%=titolo%></div><!-- 60 char -->
<!-- 			   						Share -->
			   						<div class="col s2">
			   							<a class="dropdown-button  tooltipped right " href="javascript:void(0);" data-activates="dropdown_share" data-constrainwidth="false" data-alignment="right" data-position="top" data-delay="50" data-tooltip='<liferay-ui:message key="ims.share"/>'> 
			   								<i class="material-icons fa fa-share-alt"></i> 
			   							</a>
			  						</div>
<!-- 			  						Autore -->
			  						<div class="col s12  top-margin center">
			  						  	<label class=""><liferay-ui:message key="ims.submitted-by"/></label>
							         	<%=viewAuthorName%>
							      	</div>

  								
									<div class="">
									 	<!-- Social Share -->		
										
												<ul id="dropdown_share" class="dropdown-content social-container">
													<li class="links">
															<a href="#" data-type="twitter" data-url="<%=shareUrl%>" data-description="<%=challenge.getChallengeTitle()%>" data-via="<liferay-ui:message key="ims.social.twitter"/>" class="prettySocial core-color-text text-color-4">
															<i class="fa fa-twitter-square fa-lg"></i>
															<liferay-ui:message key="ims.social.twitter.label"/>
														</a>
													</li>
													<li class="links">
														<a href="#" data-type="facebook" data-url="<%=shareUrl%>"  
																	data-description="<%=summary%>" 
																	data-title="<%=challenge.getChallengeTitle()%>" 
																	data-media="<%=imgurlshare%>" 
															
															class="prettySocial core-color-text text-color-4">
															
															<i class="fa fa-facebook-square fa-lg"></i>
															<liferay-ui:message key="ims.social.facebook.label"/>
														</a>
													</li>
													<li class="links">		
														<a href="#" data-type="googleplus" data-url="<%=shareUrl%>" 
																	data-title="<%=challenge.getChallengeTitle()%>" 
																	data-description="<%=summary%>" 
																	class="prettySocial core-color-text text-color-4">
															<i class="fa fa-google-plus-square fa-lg"></i>
															<liferay-ui:message key="ims.social.googleplus.label"/>
														</a>
													</li>
												</ul>	
													<liferay-util:html-top>
													    <meta content="<%=shareUrl%>" property="og:url">
													    <meta content="<%=summary%>" property="og:description">
													    <meta content="<%=challenge.getChallengeTitle()%>" property="og:title">
													    <meta content="<%=imgurlshare%>" property="og:image">
													</liferay-util:html-top>
													
													<script type="text/javascript">
														$( document ).ready(function() {
															$('head').append("<meta property=\"og:title\" content=\"<%=challenge.getChallengeTitle()%>\" ></meta>");
															$('head').append("<meta property=\"og:url\" content=\"<%=shareUrl%>\" ></meta>");
															$('head').append("<meta property=\"og:image\" content=\"<%=imgurlshare%>\" ></meta>");
															$('head').append("<meta property=\"og:description\" content=\"<%=summary%>\" ></meta>");
															$('.prettySocial').prettySocial();
														});
													</script>
						
										<!-- fine Social Share -->
									</div>
								</div>
								
					      	</div>
					      	
					      	
					      	<!-- right-column per la visualizazione per Tablet e al di sotto -->
					      	<div class="col s12 m12 l12 hide-on-large-only reset-width">
					      		<div class="bottom-margin"></div>
				    			<%@ include file="right-column.jspf" %>
				    		</div>
						</div>
						<!-- <div class="divider"></div> -->
						
						<% if (!tags.equalsIgnoreCase("")){ %>
						<div class="row">
							<div class="col s12">
								<label><liferay-ui:message key="keywords"/></label> 
								<span ><liferay-ui:asset-tags-summary className="<%= CLSChallenge.class.getName()%>" classPK="<%= challenge.getChallengeId() %>"/></span>
							</div>
						</div>
						
						<%} %>
						
						
						
						<!-- Date Start/Close -->
						<div class="divider"></div>
					    <div class="section row valign-wrapper">
					    	<div class="col s12">

						      	<div class="col s12 m6 center">
						      		<h6 class="weight-5"><liferay-ui:message key="start-date"/> <liferay-ui:icon-help message="ims.tip-challenge-start-date"/></h6>
						      		<%=MyUtils.getDateDDMMYYYYByDate(challenge.getDateStart()) %>  
						      	</div>
						      	<div class="col s12 m6 center">
						      		<h6 class="weight-5"><liferay-ui:message key="end-date"/> <liferay-ui:icon-help message="ims.tip-challenge-end-date"/></h6>
						      		<%=MyUtils.getDateDDMMYYYYByDate(challenge.getDateEnd()) %> 
						      	</div>
						      	
						      	<div class="bottom-margin hide-on-med-and-up"><br><br></div>
 							</div>
					     </div>
					     
 <%	if (!reducedLifecycle)	{	%>		     
						
<!--  		STATE		 -->
						<%@ include file="sections/challengeState.jspf"   %>
<!--  		STATE		 -->				     
					     
 <%}	%>				     
					     
					     
					    <c:if test="<%= ChallengesUtils.isChallengeLinkedWithOnlyOneNeed(challengeId) %>">
					     	<div class="divider bottom-margin"></div>
						     <!-- eventuale needReport di provenienza -->
						   <%
							CLSIdea needReportProvenienza= NeedLinkedChallengeLocalServiceUtil.getNeedByChallengeId(challengeId).get(0);
							String needUrl = IdeaManagementSystemProperties.getFriendlyUrlNeeds(needReportProvenienza.getIdeaID());
							%>
							
						     
							<div class="row">
							   	<div class="col s12 ">
							   		<label><liferay-ui:message key="ims.based-on-need-report"/></label>
									<span>
										<i class="fa fa-commenting-o fa-fw fa-lg" aria-hidden="true"></i>
										<a href="<%=needUrl %>" class="urlGara" target="_blank">
											<span class="titoloGara truncate"><%=needReportProvenienza.getIdeaTitle() %></span>&nbsp;&nbsp;
											<i class="fa fa-external-link fa-fw fa-lg" aria-hidden="true"></i>
										</a>
									</span>
								</div>
							</div>  <!-- Need di provenienza-->
					     
					    </c:if>
					    
					     
					     
				     	<div class="divider"></div>
						<div class="section row">
						   	<div class="col s12">
						   		<label><liferay-ui:message key="description"/></label>
								<div class="textEditorView">
									<%=challenge.getChallengeDescription() %>
								</div>
							</div>
						</div>
						
						
						<!--Premio -->
						
						<%if(challenge.getChallengeWithReward()  ){%>
						
						<div class="divider"></div>
						<div class="section row">
						   	<div class="col s12">
						   		<label><liferay-ui:message key="ims.reward"/> <liferay-ui:icon-help message="ims.tip-reward-and-incentives"/></label>
								<div class="textEditorView">
									<%=challenge.getChallengeReward() %>
								</div>
								<br/>
								<label><liferay-ui:message key="ims.number-of-selectable-ideas"/> <liferay-ui:icon-help message="ims.tip-number-of-selectable-ideas"/></label>
								<p class="">
									<%=challenge.getNumberOfSelectableIdeas() %>
								</p>
								
								<%  if( virtuosityPoints.size() > 0){ %>
								<br/><label><liferay-ui:message key="ims.virtuosity-points"/> <liferay-ui:icon-help message="ims.tip-virtuosity-point"/></label>
								
								<%  for(int w=0; w< virtuosityPoints.size();w++){ %>
								<br/>
								<label><liferay-ui:message key="ims.number-of-virtuosity-points-for-the-position"/>: <%= virtuosityPoints.get(w).getPosition() %> </label>
								<p class="">
									<%=virtuosityPoints.get(w).getPoints() %>
								</p>
								<%  }
								}%>
								
								
								
								
							</div>
						</div>
						
						<% }%>
						
						
						
						
	          		</div> <!-- fine Introduction -->




<!-- Mappa -->

<% if(isGaraWithPoi){%>

					<div class="section scrollspy" id="sezgmaps">
						<h5 class="header"><liferay-ui:message key="ims.points-of-interest"/></h5> 
						<div class="divider"></div>	           
						
<!-- 							mappa legata alla gara -->
							<%@ include file="sections/googleMaps.jspf" %>
							
					</div>
					
					
			<%} %>			
<!-- fine Mappa -->
					
					
<!-- Allegati -->
				<%if(files.size() > 0){ %>
					<div class="section scrollspy" id="sezallegati">
						<h5 class="header"><liferay-ui:message key="ims.attached-documents"/></h5> 
						<div class="divider"></div>	           
						
							<%@ include file="sections/attachmentsSection.jspf" %>
							
					</div>
			   <%} %>	
<!-- fine Allegati -->		

<!-- Idee proposte -->
				
					<div class="section scrollspy" id="sezproposedideas">
						<h5 class="header"><liferay-ui:message key="ims.ideas-proposed"/> <liferay-ui:icon-help message="ims.tip-ideas-proposed-section"/></h5> 
						<div class="divider"></div>	           
						
							<%@ include file="sections/proposedideas.jspf" %>
							
					</div>
			   	
<!-- fine Idee proposte -->

<!-- Linked Needs -->
				<c:if test="<%= ChallengesUtils.isChallengeLinkedWithManyNeeds(challengeId)  %>">
					<div class="section scrollspy" id="sezLinkedNeeds">
						<h5 class="header"><liferay-ui:message key="ims.challenge-based-on-need-reports"/> <liferay-ui:icon-help message="ims.tip-challenge-based-on-need-reports"/></h5> 
						<div class="divider"></div>	           
						
							<%@ include file="sections/linkedNeeds.jspf" %>
							
					</div>
			   	</c:if>
<!-- END Linked Needs -->


					
<!-- Commenti -->
 					<div class="section scrollspy" id="sezcommenti"> 
						<h5 class="header"><liferay-ui:message key="comments"/></h5>
 						<div class="divider"></div>		     
					
	
						<liferay-ui:panel-container extended="true" id="challengeCommentsPanelContainer" persistState="true">
							<liferay-ui:panel collapsible="false" 
											  extended="false"
											  id="challengeCommentsPanel"
											  persistState="<%= true %>"
										      title="">
															
							<portlet:actionURL name="invokeTaglibDiscussion" var="discussionURL" />


				<% String currentURL = PortalUtil.getCurrentURL(renderRequest);
						  currentURL = currentURL + "?challengeId="+challenge.getChallengeId(); %>
			
							<liferay-ui:discussion className="<%= CLSChallenge.class.getName() %>"
						classPK="<%= challenge.getChallengeId() %>"
				 		formAction="<%= discussionURL %>"
				 		formName="fm2"
						ratingsEnabled="<%= true %>" 
						redirect="<%= currentURL %>"
						subject="<%= challenge.getChallengeTitle() %>"
						userId="<%= themeDisplay.getUserId()  %>" />
							
						
							</liferay-ui:panel>
						</liferay-ui:panel-container>

					</div> 
<!-- fine commenti -->
  
		         </div><!-- fine card-breaking -->
		       </div><!-- fine card-content -->
		     </div><!-- fine colonna centrale -->
		     
<!-- 		     Colonna destra -->
			<!-- right-column per la visualizazione per Desktop e oltre -->
		    <div class="col s12 m12 l2 center-align hide-on-med-and-down" style="height:1px">
		    	<div class="row toc-wrapper">
		     		<%@ include file="right-column.jspf" %>
		     	</div>
	      	</div><!-- fine Colonna destra -->
	      	
		</div>
	
</div>



<script>

$(document).ready(function(){
	//Workaround perche' quando spunti il msg di operazione avvenuta con successo, dopo un commento, 
	//si agghiaccia tutto JS, non funzionano ad esempio i bottoni, quindi trovo questa classe ricarico
	//la pagina e tutto si sistema
	if ($('.alert-success').length > 0) {
		
			location.href="<%=shareUrl %>";
	}
	
	<%
	if (ideas.size()> 0){ //initialize the carousel only if it has ideas to show, becasue in any case I show the section
	%>
	
// 		$('#tableIdeas').carousel();
		$('#tableIdeas.carousel-slider').carousel({full_width: true, indicators: true, dist:0});
	
	<% } //I don't need to do the same control for the needReport, because if it has not element to show, I don't show the entire section %>
	
		$('#tableNeeds.carousel-slider').carousel({full_width: true, indicators: true, dist:0});
});




	(function($){
		  $(function(){
			  
				var resizeTimer;
				$(window).on('resize', function(e) {

				  clearTimeout(resizeTimer);
				  resizeTimer = setTimeout(function() {tocScrollSpy()}, 250);
				});
				
// 				var scrollTimer2;
// 				$(window).on('scroll', function(e){
					
// 					clearTimeout(scrollTimer2);
// // 					scrollTimer2 = setTimeout(function() {
// 							tocScrollSpy();
// // 					}, 200);
						
// 				});

					
					
				    $('.mini-card .card-title').dotdotdot({height: 60});
				    
				    var startFixed = $('.toc-wrapper').parent().parent().offset().top;
					$('.toc-wrapper').pushpin({ top: startFixed});
					
					tocScrollSpy();
					
		  }); // end of document ready
	})(jQuery); // end of jQuery name space


	function tocScrollSpy(){
		
		if($(window).width() >= 992){
			
			$('.scrollspy').scrollSpy();
			
			var columnWidth = $('.toc-wrapper').parent().width();
			$('.image img').width(columnWidth);
			
			$('.pin-top').width(columnWidth);
			$('.pinned').width(columnWidth);
			
		}
	}
	
</script>



<script>
	
function deleteChallenge(garaId){
    var r=confirm('<liferay-ui:message key="ims.delete-challenge-message"></liferay-ui:message>');

    var formName = "#<portlet:namespace/>fmch2"+garaId;
    if (r==true) {
        $( formName ).submit();
    }
}


function openEvaluationConfirm(){
    var r=confirm('<liferay-ui:message key="ims.alert-open-evaluation"/>');

    if (r==true) {
        $( '#<portlet:namespace/>fm_openEvaluation' ).submit();
    }
}


function closeEvaluationConfirm(){
    var r=confirm('<liferay-ui:message key="ims.alert-close-evaluation"/>');

    if (r==true) {
        $( '#<portlet:namespace/>fm_closeEvaluation' ).submit();
    }
}




<% //se l'utente � loggato verifico se la gara � preferita o no ed aggiungo le funzioni per la sua gestione
	if( currentUser != null){
		CLSFavouriteChallengesLocalServiceImpl clsFavChallLocServImpl = new CLSFavouriteChallengesLocalServiceImpl();
		//verifico se la gara � preferita o no
		List<CLSFavouriteChallenges> favChall = clsFavChallLocServImpl.getFavouriteChallenges(challengeId, currentUser.getUserId());
		if (favChall.size() == 1 ){
			%>
			var isFavourite = true;
			<%
		}else{
			%>
			var isFavourite = false;
			<%
		}
		
		//aggiungo le funzioni per la gestione delle gare preferite
		%>
		$("#addToFavourite").unbind("click").click(function(){				
			if(!isFavourite){
				console.log("add");
				console.log(isFavourite);
				
				Liferay.Service(
						  '/Challenge62-portlet.clsfavouritechallenges/add-favourite-challenge',
						  {
						    favChallengeId: <%=challengeId%>,
						    userId: <%= currentUser.getUserId() %>
						  },
						  function(obj) {
							isFavourite = true;							
							$("#addToFavourite").html('<i class="icon-bookmark"></i>\n<liferay-ui:message key="ims.remove-from-favourites"/>');
						    console.log(obj);
						  }
						);		
			}else{
				console.log("remove");
				console.log(isFavourite);
				
				Liferay.Service(
						  '/Challenge62-portlet.clsfavouritechallenges/remove-favourite-challenge',
						  {
						    favChallengeId: <%=challengeId%>,
						    userId: <%= currentUser.getUserId() %>
						  },
						  function(obj) {
							  isFavourite = false;
							  $("#addToFavourite").html('<i class="icon-bookmark-empty"></i>\n<liferay-ui:message key="ims.add-to-favourites"/>');
						      console.log(obj);
						  }
						);
			}
		});
		
		
		function isInFavouriteChallenges(){
			Liferay.Service('/ChallengeGA3-portlet.clsfavouritechallenges/get-favourite-challenges',{
						favChallengeId:  <%=challengeId%>,
			   			userId: <%= currentUser.getUserId() %>
						},
						function(obj) {
							if(obj.length>0){
			            		isFavourite = true;
			            		$("#addToFavourite").html('<i class="icon-bookmark"></i>\n<liferay-ui:message key="ims.remove-from-favourites"/>');
			            	}else{
			            		isFavourite = false;
			            		 $("#addToFavourite").html('<i class="icon-bookmark-empty"></i>\n<liferay-ui:message key="ims.add-to-favourites"/>');
			            	}    
						});
		}
		<%
	}
	%>

	</script>

