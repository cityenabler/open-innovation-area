<%@page import="it.eng.rspa.ideas.utils.ChallengesUtils"%>
<%@page import="it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.EvaluationCriteriaLocalServiceUtil"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria"%>

<%@ include file="/html/challenges/init.jsp" %>

<%

long challengeId = Long.parseLong(request.getParameter("challengeId"));

String challURL = IdeaManagementSystemProperties.getFriendlyUrlChallenges(challengeId);
String challengeName = ChallengesUtils.getChallengeNameByChallengeId(challengeId);

%>

<div class="materialize">


	<div class="row  valign-wrapper">
		<div class="col s2 left-align">
		 	<a class="waves-effect btn-flat tooltipped" href="<%=challURL %>"  data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.go-back"/>">
	  			<i class="icon-arrow-left left">&nbsp;</i><liferay-ui:message key="back"/>
	  		</a>
		</div>

		<div id="parentRef" class="col s8 center">
					<h4><i class="material-icons icon-trophy small tooltipped" data-position="top" data-delay="40" data-tooltip="<liferay-ui:message key="ims.challenge"/>">&nbsp;</i>
					<a href="<%=challURL%>" target="_blank">  
						<%=challengeName %>
					 </a>
					</h4>
		</div>

		<div class="col s2 right-align">

		</div>
		
	</div>


<!-- This jspf is included in other pages as tab -->
<%@ include file="sections/evaluationCriteria.jspf" %>

</div>