<%@page import="it.eng.rspa.ideas.utils.DecisionEngine"%>
<%@page import="java.util.ResourceBundle"%>

<%@page import="com.liferay.portal.kernel.util.StringPool"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts"%>
<%@page import="java.util.List"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.CLSArtifactsLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portal.model.User"%>
<%@page import="it.eng.rspa.ideas.utils.IdeasListUtils"%>
<%@page import="it.eng.rspa.ideas.utils.MyUtils"%>
<%@page import="it.eng.rspa.ideas.utils.ViewIdeaUtils"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@page import="java.util.Locale" %>
<%@page import="it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties"%>
<%@page import="org.apache.commons.lang3.StringEscapeUtils"%>
<%@page import="com.liferay.portal.kernel.util.StringUtil"%>
<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteIdeasLocalServiceUtil"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas"%>

<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects"%>
<%@page import="com.liferay.portlet.documentlibrary.model.DLFileEntry"%>
<%@ include file="/html/challenges/init.jsp" %>
<portlet:defineObjects />

<!-- CSS -->
<link href="<%=request.getContextPath()%>/css/share-social.css" rel="stylesheet">
<link href="<%=request.getContextPath()%>/css/show.css" rel="stylesheet">

<!-- SCRIPT -->
<script src="<%=request.getContextPath()%>/js/social-share/jquery.prettySocial.js"></script>

<liferay-theme:defineObjects />

<%
String ideaIdS = request.getParameter("ideaId");
long ideaId = Long.valueOf(ideaIdS).longValue();

CLSIdea idea = null;
try{
	idea =  CLSIdeaLocalServiceUtil.getCLSIdea(ideaId);
		
}catch(Exception e){ 
	
	System.out.println(e.getMessage()); 
	String errorMessageKeyI18n = "ims.missing-resource";
%>
<%@ include file="/html/home/pages/goToErrorPage.jspf" %>
<%return;

}//try-catch
		
		
		
boolean isNeed = idea.getIsNeed();

//Per la ricerca idea e need sono la stessa cosa, quindi passa da qui e reindirizziamo a Need
if(isNeed){
	
	String urlNeed = IdeaManagementSystemProperties.getFriendlyUrlNeeds(ideaId);
%>
	<script>
	location.href="<%=urlNeed %>";
	</script>
<%return;} %>


<%@ include file="/html/ideasexplorermaterial/ideasdetails/sections/checkPilotOnIdeaDetail.jspf" %>


<%
User currentUser = PortalUtil.getUser(request);

boolean publicIdeasEnabled = IdeaManagementSystemProperties.getEnabledProperty("publicIdeasEnabled");
boolean isAuthorOrCoworkerOrAdminByIdeaId = IdeasListUtils.isAuthorOrCoworkerOrAdminByIdeaId(ideaId, currentUser);


if (  !publicIdeasEnabled){
	if (  !isAuthorOrCoworkerOrAdminByIdeaId){
%>
<script> 
	location.href="<%=themeDisplay.getURLSignIn() %>"; /*it redirects to login */
</script> 
<%
	}
}
String language = locale.getLanguage();
String country = locale.getCountry();
ResourceBundle res = ResourceBundle.getBundle("Language", new Locale(language, country));


//ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
Long groupId = themeDisplay.getCompanyGroupId();

String vocabolario = IdeasListUtils.getVocabularyNameByIdeaId(ideaId, locale);
long vocabolarioId = IdeasListUtils.getVocabularyIdByIdeaId(ideaId);
String iconaVocabolario = IdeasListUtils.getIconForVocabularyByIdeaId(ideaId);
String coloreVocabolario = IdeasListUtils.getColorForVocabularyByIdeaId(ideaId);

String titolo = idea.getIdeaTitle();
String authorName = IdeasListUtils.getAuthorNameByIdeaId(ideaId);
String authorProfileUrl = IdeasListUtils.getAuthorProfileUrlByIdea(idea);
String garaName = IdeasListUtils.getChallengeNameByIdeaId(ideaId);
String garaUrl = IdeasListUtils.getUrlChallengeByIdeaId(ideaId);
String enteName = IdeasListUtils.getMunicipalityNameByIdeaAndUser(idea, currentUser);
String categorie =IdeasListUtils.getCategoriesVirgoleByIdeaId(ideaId, locale);
String tags =IdeasListUtils.getTagsVirgoleByIdeaId(ideaId);
String dataCreazione = IdeasListUtils.getDateDDMMYYYYByIdeaId(ideaId);
String urlImg = IdeasListUtils.getRepresentativeImgUrlByIdeaId(ideaId, request.getContextPath());
String cityName = idea.getCityName();

List<DLFileEntry> files = IdeasListUtils.getFilesByIdeaId(ideaId, groupId);

boolean isEnteRiferimento = IdeasListUtils.isEntityReferencebyIdea(idea, currentUser) ;
boolean isAuthor = IdeasListUtils.isAuthorByIdeaId(ideaId, currentUser);
boolean isCoworker = IdeasListUtils.isCoworkerByIdeaId(ideaId, currentUser);
boolean isAuthorOrCoworker = IdeasListUtils.isAuthorOrCoworkerByIdeaId(ideaId, currentUser);
boolean isFavoriteIdea = IdeasListUtils.isFavoriteIdeas(ideaId, currentUser);
boolean isIdeaWithPoi = IdeasListUtils.isIdeaWithPoi(idea);
boolean isIdeaConnectableWithChallengeStartedByNeed =IdeasListUtils.isIdeaConnectableWithChallengeStartedByNeed(idea); 
boolean isIdeaEditable = UpdateIdeaUtils.isIdeaEditable(idea);
boolean isCompanyLeader = MyUtils.isCompanyLeader( currentUser);
boolean isTakenUp = idea.isTakenUp();
boolean isIdeaTakeable = IdeasListUtils.isIdeaTakeable(ideaId);

double evaluationScore = IdeasListUtils.getTotalScoreIdeaByIdeaId(ideaId);
int  rankingPosition = IdeasListUtils.getEvaluationRankingPositionByIdea(idea);


List<CLSIdea> suggestedIdeas = DecisionEngine.deGetIdeasVsIdeasRecommendations(ideaId);
//List<CLSIdea> suggestedIdeas = CLSIdeaLocalServiceUtil.getCLSIdeas(0, 10);

long challengeId = idea.getChallengeId();
long linkedNeedId = idea.getNeedId();

long screenFolderId = IdeasListUtils.getScreenshotFolderIdByIdea(idea);


List<CLSVmeProjects> mashups =  ViewIdeaUtils.getMashups(idea.getIdeaID());

boolean mktEnabled = IdeaManagementSystemProperties.getEnabledProperty("mktEnabled");
boolean fiwareEnabled = IdeaManagementSystemProperties.getEnabledProperty("fiwareEnabled");
boolean reducedLifecycle = IdeaManagementSystemProperties.getEnabledProperty("reducedLifecycle");
boolean fundingBoxEnabled = IdeaManagementSystemProperties.getEnabledProperty("fundingBoxEnabled");


String marketURL=themeDisplay.getPortalURL()+MyConstants.MKT_DETAIL_LINK;

String pathDocumentLibrary = themeDisplay.getPortalURL() + themeDisplay.getPathContext() + "/documents/" + themeDisplay.getScopeGroupId() + StringPool.SLASH;


PortletURL screenshots=renderResponse.createRenderURL();
screenshots.setParameter("jspPage", "/html/ideasexplorermaterial/ideasdetails/pages/screenshots.jsp");
screenshots.setParameter("ideaId", String.valueOf(idea.getIdeaID())); 

%>

<portlet:actionURL var="downloadFileURL" name="serveResource"/>
<portlet:resourceURL id="getFavouriteIdeas" var="getFavouriteIdeasURL"/>
<portlet:resourceURL id="addFavouriteIdea" var="addFavouriteIdea"/>
<portlet:resourceURL id="removeFavouriteIdea" var="removeFavouriteIdea"/>
<portlet:resourceURL var="resourceURL"/>


<!-- Social Share -->	
<%						
String shareUrl = IdeaManagementSystemProperties.getFriendlyUrlIdeasCheckHttps(ideaId, themeDisplay.isSecure());				
String imgurlshare = request.getScheme()+ "://" + request.getServerName() + ":" + request.getServerPort() + urlImg;
String summary = IdeasListUtils.getSocialSummaryByIdea(idea);

%>
<!-- fine Social Share -->

<div class="materialize">

		<!-- background row -->
		<div class="row row-background  <%=coloreVocabolario %> lighten-1 bottom-margin ">
			<!-- back col -->
			<div class="col s5 m3 l2 center-align">
	  			<br> 
	  			<a class="waves-effect btn-flat tooltipped white-text" href="<portlet:renderURL>
									<portlet:param name="jspPage" value="/html/ideasexplorermaterial/view.jsp" />
								 </portlet:renderURL>" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.go-back"/>">
	  				<i class="material-icons icon-arrow-left small">&nbsp;</i><liferay-ui:message key="back"/>
	  			</a>
	  		</div>
	  		
	  		<!-- category col -->
	  		<div class="col s2 m6 l8 center-align">
	  		  <% if (!reducedLifecycle){ %>
		  		<div class="header-card-details valign-wrapper white-text tooltipped" data-position="top" data-delay="50" data-tooltip="<%=vocabolario %>">
			          <i class="material-icons <%=iconaVocabolario %> circle small"></i>
			          <span class="truncate hide-on-small-only">
			            &nbsp;&nbsp;<%=vocabolario %>
			          </span>
			 <% }else{%> 
			         	<div class="header-card-details valign-wrapper white-text"> 
			 <% }%> 
				</div>
			</div>
			
			<!-- management col -->
			<div class="col s5 m3 l2 center-align ">
			
				 <!-- bottoni vari di gestione idea (stati, preferiti, modifica, cancella...) -->
				<%@ include file="sections/manageButton.jspf" %>
			</div>
		</div>
		
		<div class="row">
<!-- 			Colonna sinistra -->
			<!-- left-column per la visualizazione per Desktop e oltre -->
			<div class="col s12 m12 l2 hide-on-med-and-down" style="height:1px">
				<div class="row toc-wrapper">
					<%@ include file="left-column.jspf" %>
				</div>
			</div><!-- fine Colonna sinistra -->
			 
<!-- 			 Colonna centrale -->
		     <div class="col s12 m12 l8">
		       <div class="card breaking">
		         <div class="card-content">
		         	<h6 class="center text-details">
		         		<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true">&nbsp;</i><liferay-ui:message key="ims.idea.details"/>
		         	</h6>
	           		<div class="divider bottom-margin"></div>
<!-- Introduction -->
		           	<div id="introduction" class="section scrollspy">
				    	<div class="row">
							
							<!-- left-column per la visualizazione per Tablet e al di sotto -->
				    		<div class="col s12 m4 l12 hide-on-large-only">
				    			<%@ include file="left-column.jspf" %>
				    		</div>
				    		
				    		<div class="col s12 m8 l12">
				    			<div class="row">
<!-- 				    				Titolo -->
			   						<div class="col s10 card-multi-title flow-text weight-4"><%=titolo%></div><!-- 60 char -->
<!-- 			   						Share -->
			   						<div class="col s2">
			   							<a class="dropdown-button  tooltipped right " href="javascript:void(0);" data-activates="dropdown_share" data-constrainwidth="false" data-alignment="right" data-position="top" data-delay="50" data-tooltip='<liferay-ui:message key="ims.share"/>'> 
			   								<i class="material-icons fa fa-share-alt"></i> 
			   							</a>
			  						</div>
<!-- 			  						Autore -->
			  						<div class="col s12 top-margin center">
			  						  	<label class=""><liferay-ui:message key="ims.submitted-by"/></label>
							         	<a href="<%= authorProfileUrl %>" target="_blank"><%=authorName%></a>
							      	</div>

<!--   								Linked Challenge -->
								<c:if test="<%= challengeId>-1 %>">

							      	<div class="col s12 top-margin">
										<label class="">
										<% if(isTakenUp){%>
											<liferay-ui:message key="ims.formerly-associated-to-challenge"/>
										<% }else{%>
											<liferay-ui:message key="ims.for-the-challenge"/>
										<% }%>	
											
										</label> 
										<span>
										<%
										String iconChallenge =MyConstants.ICON_CLG;
										if(reducedLifecycle){ 
											  iconChallenge =MyConstants.ICON_CLG_REDUCED;
										}
										%>
											<i class="fa <%=iconChallenge%> fa-fw fa-lg" aria-hidden="true">&nbsp;</i>
											<a href="<%=garaUrl%>" class="urlGara tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.tip-link-idea-to-challenge"/>" target="_blank">
												<span class="titoloGara truncate"><%=garaName%></span>&nbsp;&nbsp;
												<i class="fa fa-external-link fa-fw fa-lg" aria-hidden="true"></i>
											</a>
										</span>
									</div>
								</c:if>	
								
<!--   								Linked Need -->
  								<% if(linkedNeedId>0){
	  								String	linkedNeedUrl = IdeasListUtils.getUrlNeedByIdea(idea);
	  								String	linkedNeedName = IdeasListUtils.getNeedNameByIdea(idea);
  								%>
							      	<div class="col s12 top-margin">
										<label class=""><liferay-ui:message key="ims.addressed-to-need"/></label> 
										<span>
											<i class="fa fa-commenting-o fa-fw fa-lg" aria-hidden="true">&nbsp;</i>
											<a href="<%=linkedNeedUrl%>" class="urlGara tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.tip-link-idea-to-need"/>" target="_blank">
												<span class="titoloGara truncate"><%=linkedNeedName%></span>&nbsp;
												<i class="fa fa-external-link fa-fw fa-lg" aria-hidden="true"></i>
											</a>
										</span>
									</div>
								<%}%>
									
									
									<div class="">
									 	<!-- Social Share -->		
										
												<ul id="dropdown_share" class="dropdown-content social-container">
													<li class="links">
														<a href="#" data-type="twitter" data-url="<%=shareUrl%>" data-description="<%=idea.getIdeaTitle()%>" data-via="<liferay-ui:message key="ims.social.twitter"/>" class="prettySocial core-color-text text-color-4">
															<i class="fa fa-twitter-square fa-lg"></i>
															<liferay-ui:message key="ims.social.twitter.label"/>
														</a>
													</li>
													<li class="links">	
														<a href="#" data-type="facebook" data-url="<%=shareUrl%>" 
																	data-description="<%=summary%>" 
																	data-title="<%=idea.getIdeaTitle()%>" 
																	data-media="<%=imgurlshare%>" 
																	class="prettySocial core-color-text text-color-4">
																<i class="fa fa-facebook-square fa-lg"></i>
																<liferay-ui:message key="ims.social.facebook.label"/>
														</a>
													</li>
													<li class="links">		
														<a href="#" data-type="googleplus" data-url="<%=shareUrl%>" 
																	data-title="<%=idea.getIdeaTitle()%>" 
																	data-description="<%=summary%>" 
																	class="prettySocial core-color-text text-color-4">
															<i class="fa fa-google-plus-square fa-lg"></i>
															<liferay-ui:message key="ims.social.googleplus.label"/>
														</a>
													</li>
												</ul>	
													<liferay-util:html-top>
													    <meta content="<%=shareUrl%>" property="og:url">
													    <meta content="<%=summary%>" property="og:description">
													    <meta content="<%=idea.getIdeaTitle()%>" property="og:title">
													    <meta content="<%=imgurlshare%>" property="og:image">
													</liferay-util:html-top>
													
													<script type="text/javascript">
														$( document ).ready(function() {
															$('head').append("<meta property=\"og:title\" content=\"<%=idea.getIdeaTitle()%>\" ></meta>");
															$('head').append("<meta property=\"og:url\" content=\"<%=shareUrl%>\" ></meta>");
															$('head').append("<meta property=\"og:image\" content=\"<%=imgurlshare%>\" ></meta>");
															$('head').append("<meta property=\"og:description\" content=\"<%=summary%>\" ></meta>");
															$('.prettySocial').prettySocial();
														});
													</script>
						
										<!-- fine Social Share -->
									</div>
								</div>
								<div class="divider bottom-margin"></div>
					      	</div>
					      	

						
					      	<!-- Stato avanzamento -->
					      	<div class="col s12">
								<div class="">
									<!-- Stato di avanzamento in cui si trova l'idea -->
										<%@ include file="sections/imsState.jspf" %>
								</div>
								<div class="divider bottom-margin"></div>
							</div>
							
	<% if (!(idea.getFinalMotivation().isEmpty() ||
		  idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_ELABORATION) || 	
		  idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_EVALUATION) ||
		  isTakenUp
		 )
	   ){%>						
							<!-- Evaluation -->
					      	<div class="col s12">
								<div class="">
									
										<%@ include file="sections/imsEvaluation.jspf" %>
								</div>
								
							</div>					      	
	<% }%>			      	
					      	<!-- right-column per la visualizazione per Tablet e al di sotto -->
					      	<div class="col s12 m12 l12 hide-on-large-only reset-width">
					      		<div class="bottom-margin"></div>
				    			<%@ include file="right-column.jspf" %>
				    		</div>
						</div>
						
						<!-- SubCategories -->
						<div class="section row">
							<div class="col s12">
								
								<label>
							<%if (!reducedLifecycle){ %>	
								<liferay-ui:message key="subcategories"/>
							<%}else{ %>	
								<liferay-ui:message key="ims.categories"/>
							<%} %>		
								
								</label> 
								<span class="truncate"><%=categorie %></span>
							</div>
						</div>
						
						
						
						
						
						<!-- Tags -->
						<% if (!tags.equalsIgnoreCase("")){ %>
						<div class="section row">
							<div class="col s12">
								<label><liferay-ui:message key="keywords"/></label> 
								<span ><liferay-ui:asset-tags-summary className="<%= CLSIdea.class.getName()%>" classPK="<%= idea.getIdeaID() %>"/></span>
							</div>
						</div>
						
						<%} %>
						
							
						
						<div class="divider bottom-margin"></div>
						
						<!-- Date e Rating -->
					    <div class="row">
					      	<div class="col s12 m6 left">
					      		<label><liferay-ui:message key="ims.creation-date"/></label> 
					      		<%=dataCreazione %> 
					      	</div>
					      	<div class="bottom-margin hide-on-med-and-up"><br><br></div>
						    <div class="col s12 m6">
						    
						    <% if (isTakenUp) {%> 
						   		<label><liferay-ui:message key="ims.taken-up-by-the-company"/></label> 
								<h6 class="valign-wrapper tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.company"/>">
									<i class="material-icons small">business_center</i>&nbsp; <%=enteName %> 
								</h6>
							<%}else if (fundingBoxEnabled ){ 
										if (!cityName.isEmpty()){
							%>		
										<h6 class="valign-wrapper tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="city"/>">
											<i class="material-icons small">location_city</i>&nbsp; <%=cityName %> 
										</h6>
								
							<% 			}
							}else{ %>	
								<h6 class="valign-wrapper tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="organization"/>">
									<i class="material-icons small">business_center</i>&nbsp; <%=enteName %> 
								</h6>
							<%} %>
							
				            </div>
					     </div>
				     	<div class="divider"></div>
						<div class="section row">
						   	<div class="col s12">
						   		<label><liferay-ui:message key="description"/></label>
								<div class="textEditorView">
									<%=idea.getIdeaDescription() %>
								</div>
							</div>
						</div>
	          		</div> <!-- fine Introduction -->

<!-- Autori -->
					<div class="section scrollspy" id="structure">
						<h5 class="header"><liferay-ui:message key="ims.authors"/></h5> 
						<div class="divider"></div>	 
							<!-- dettagli Autore e collaboratori dell'idea -->
							<%@ include file="sections/authorsSection.jspf" %>
							
					</div>
<!-- fine Autori -->


<!-- Modifica Autori -->
<% if((isTakenUp && isEnteRiferimento) || isAuthor ){%>
 
					<div class="section scrollspy" id="addCollaborators"> 
						<h5 class="header"><liferay-ui:message key="ims.select-coworkers"/></h5> 
						<div class="divider"></div>	           

							<%@ include file="sections/coworkers_selection.jspf" %>
							
					</div>
<%} %>
<!-- fine Mofica Autori -->


<!-- Mappa (solo idee senza gara) -->

<% if(challengeId==-1 && isIdeaWithPoi){%>

					<div class="section scrollspy" id="sezgmaps">
						<h5 class="header"><liferay-ui:message key="ims.points-of-interest"/></h5> 
						<div class="divider"></div>	           
						
							<!-- mappa legata all'idea -->
							<%@ include file="sections/googleMaps.jspf" %>
							
					</div>
					
					
			<%} %>			
<!-- fine Mappa -->

<!-- Requirements - Refinements-->
<%

		if (ViewIdeaUtils.isVisibleRequirementSectionByIdea(idea)) {
%>
		
		<div class="section scrollspy" id="sezrequirements">
				<h5 class="header"><liferay-ui:message key="ims.refinement"/> <liferay-ui:icon-help message="ims.tip-requirements-section"/></h5>
				<div class="divider"></div>    
				<%@ include file="sections/requirements/viewRequirements.jspf" %>
		</div><!-- fine Requirements -->
<%
		}
%>
<!--Fine Requirements - Refinements-->
					
					
<!-- Allegati -->
				<%if(files.size() > 0){ %>
					<div class="section scrollspy" id="sezallegati">
						<h5 class="header"><liferay-ui:message key="ims.attached-documents"/></h5> 
						<div class="divider"></div>	           
						
							<%@ include file="sections/attachmentsSection.jspf" %>
							
					</div><!-- fine Allegati -->
			   <%} %>	
<!-- fine Allegati -->		

<!-- SCREENSHOT Contribute-->
 <%

 	if((idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_IMPLEMENTATION)|| (idea.getIdeaStatus().equals(MyConstants.IDEA_STATE_MONITORING))) 
		&& 
				
		(screenFolderId>0 || (mashups.size()>0  &&  isAuthorOrCoworker))
		) {
	 %>
					<div class="section scrollspy" id="sezscreenshot">
						<h5 class="header"><liferay-ui:message key="ims.contribute"/> <liferay-ui:icon-help message="ims.tip-contrinute-screenshot"/></h5> 
						<div class="divider"></div>	 
							
							<%@ include file="sections/screenshot.jspf" %>

					</div>
		
<% }%>
<!-- fine SCREENSHOT -->

<!-- Artefatti Associati -->

<%if(mktEnabled  && (idea.getIdeaStatus().equals(MyConstants.IDEA_STATE_IMPLEMENTATION) || idea.getIdeaStatus().equals(MyConstants.IDEA_STATE_MONITORING) )) {  %>

      <div class="section scrollspy" id="artefatti"> 
            <h5 class="header"><liferay-ui:message key="ims.artifacts"/> <liferay-ui:icon-help message="ims.tip-artefacts-section"/></h5>
            <div class="divider"></div>       

            <%

			String display="none";
			String display_row="block";
	
            //Caricamento Artefatti associati
            List<CLSArtifacts> idArtefatti = new ArrayList<CLSArtifacts>();
            try{ idArtefatti = CLSArtifactsLocalServiceUtil.getArtifactsByIdeaId(idea.getIdeaID()); }
            catch(Exception e){ System.out.println(e.getMessage()); }
            
                  PortletURL rowURL=renderResponse.createRenderURL();
                  rowURL.setParameter("jspPage", "/html/marketplacestore/view_artefatto.jsp");
            %>
                  
                  <!-- Sezione Artefatti associati all'idea -->
                  <%@ include file="sections/artefactsAssociatedSection.jspf" %>
                  <!-- fine Sezione Artefatti associati all'idea -->

		
      </div>
                              
<%}%>
                              
<!-- fine Artefatti Associati -->
					
<!-- Associa Artefatti -->

<%
		//Artefacts search
		if((idea.getIdeaStatus().equals(MyConstants.IDEA_STATE_IMPLEMENTATION) || idea.getIdeaStatus().equals(MyConstants.IDEA_STATE_MONITORING) ) && isEnteRiferimento && mktEnabled ){
%>

					<div class="section scrollspy" id="associa_arte">
						<h5 class="header"><liferay-ui:message key="ims.associate-artifacts"/> <liferay-ui:icon-help message="ims.tip-associate-artefacts-section"/></h5>
						<div class="divider"></div>    
						


					
						<div class="row top-margin">
						    <div class="col s12">
						      <div class="row">
						      	<div class="searchArtefactDiv valign-wrapper">
							        <div class="input-field col s8">
							          <input id="key-search" name="key-search" type="text" class="validate">
							          <label for="key-search"><liferay-ui:message key="ims.search-artifact"></liferay-ui:message></label>
							        </div>
							        <div class="col s4">
							            <button id="searchKey" class="btn waves-effect" onclick="search()">
									   	<i class="icon-search">&nbsp;</i><liferay-ui:message key="search"/>
									  </button>
							        </div>
						        </div>
						      </div>
						      <div class="row" id="resultsArtefactDiv">
								<p class="hide noarts"><liferay-ui:message key="ims.no-artifacts"/></p>
								<ul class="collection">
									<li class="template hide collection-item avatar">
								    	<i class="bblock core-color color-1 hide fa fa-cog fa-3x circle"></i>
					      				<i class="psa core-color color-3 green hide fa fa-mobile fa-3x circle"></i>
										<i class="dataset core-color color-2 blue hide fa fa-database fa-3x circle"></i>
								      	<a href="#" target="_blank"><span class="title truncate"><liferay-ui:message key="title"/></span></a>
										<p>
											<span class="author truncate"><liferay-ui:message key="author"/></span>
											<span class="type truncate"><liferay-ui:message key="type"/></span>
										</p>
								      	<a href="#!" class="hide-on-med-and-down assoc secondary-content btn-flat"><liferay-ui:message key="associate"/></a>
								      	<a href="#!" class="hide-on-large-only assoc core-color-text text-color-2 btn-flat"><liferay-ui:message key="associate"/></a>
								    </li>
						      	</ul>
							  </div>
						    </div>
						  </div>
					</div>
				
	<%} %>
<!-- fine Associa Artefatti -->
	
<!-- Generic Enablers -->	
	<%
	if(fiwareEnabled  ) { 
			boolean isIdeaWithSelectedGEs = FiwareUtils.isIdeaWithSelectedGEs(ideaId);
			if (isIdeaWithSelectedGEs){
		%>

					<div class="section scrollspy" id="structure">
						<h5 class="header">Generic Enablers <liferay-ui:icon-help message="ims.tip-generic-enablers-section"/></h5> 
						
						
						<div class="divider"></div>	 
							
							<%@ include file="sections/genericenablers_selected.jspf" %>
							
					</div>
<!-- fine Generic Enablers -->
	<%		}
	}%>			
<!-- Commenti -->
					<div class="section scrollspy" id="sezcommenti">
						<h5 class="header"><liferay-ui:message key="comments"/></h5>
						<div class="divider"></div>		    
					
	
						<liferay-ui:panel-container extended="true" id="ideaCommentsPanelContainer" persistState="true">
							<liferay-ui:panel collapsible="false" 
											  extended="false"
											  id="ideaCommentsPanel"
											  persistState="<%= true %>"
										      title="">
															
							<portlet:actionURL name="invokeTaglibDiscussion" var="discussionURL" />
							<% 
						String currentURL = PortalUtil.getCurrentURL(request);
						currentURL = currentURL + "?ideaID="+idea.getIdeaID();
						%>
							<liferay-ui:discussion className="<%= CLSIdea.class.getName() %>"
									classPK="<%= idea.getIdeaID() %>"
							 		formAction="<%= discussionURL %>"
							 		formName="fm2"
									ratingsEnabled="<%= true %>" 
									redirect="<%= currentURL %>"
									subject="<%= idea.getIdeaTitle() %>"
									userId="<%= themeDisplay.getUserId()  %>" />
							
						
							</liferay-ui:panel>
						</liferay-ui:panel-container>

					</div><!-- fine commenti -->
  
  <!-- Suggested Ideas -->
<%
	
		if (suggestedIdeas.size() > 0) {
%>

	<%@ include file="sections/suggestedIdeas.jspf" %>
<%
		
	}
%>

<!-- fine Suggested Ideas -->
  
  
  
  
  
		         </div><!-- fine card-breaking -->
		       </div><!-- fine card-content -->
		     </div><!-- fine colonna centrale -->
		     
<!-- 		     Colonna destra -->
			<!-- right-column per la visualizazione per Desktop e oltre -->
		    <div class="col s12 m12 l2 center-align hide-on-med-and-down" style="height:1px">
		    	<div class="row toc-wrapper">
		     		<%@ include file="right-column.jspf" %>
		     	</div>
	      	</div><!-- fine Colonna destra -->
	      	
		</div>
	
</div>



<script>

	(function($){
		  $(function(){
			  	window.scrollTo(0,0);
			  	
				var resizeTimer;
				$(window).on('resize', function(e) {

				  clearTimeout(resizeTimer);
				  resizeTimer = setTimeout(function() {tocScrollSpy()}, 250);
				});
				/*
				var resizeTimer2;
				$(window).on('scroll', function(e){
					clearTimeout(resizeTimer2);
					resizeTimer2 = setTimeout(function() {tocScrollSpy();}, 1000);
				});
				*/
				

				$(document).ready(function(){
					
					if($('.artefact-carousel .carousel-item').length > 0){
				    	$('.artefact-carousel').carousel({full_width: true, indicators: true, dist:0});
					}
					$('.carousel').not('.artefact-carousel').carousel({full_width: true, indicators: true, dist:0});
				    
				    $('.carousel-item-title').dotdotdot({height: 34});
				    $('.carousel-item-provider').dotdotdot({height: 20});

				  });

				var startFixed = $('.toc-wrapper').parent().parent().offset().top;
				$('.toc-wrapper').pushpin({ top: startFixed});

				tocScrollSpy();
		  }); // end of document ready
	})(jQuery); // end of jQuery name space


	function tocScrollSpy(){
		
		if($(window).width() >= 992){
			
			$('.scrollspy').scrollSpy();
			
			var columnWidth = $('.toc-wrapper').parent().width();
			$('.image img').width(columnWidth);

			$('.pin-top').width(columnWidth);
			$('.pinned').width(columnWidth);			
		}
	}
	

function search(){
	YUI().use(
	  'aui-node',
	  'aui-io-request',
	  function(Y) {
	  
		   var keyword=Y.one("#key-search");
		   var keywordValue=keyword.get("value");
		   
		   /*per far partire la ricerca ci vogliono almeno 4 chars, altrimenti i risultati sono troppissimi*/
		   if (keywordValue.length <4){
			   alert('<liferay-ui:message arguments="4" key="please-enter-at-list-x-characters"/>');
		   }else{
		   
		   
		   var divRes=Y.one("#resultsArtefactDiv");
		   
	       Y.io.request('<%=resourceURL.toString()%>',
	 		   
		      {
				  data: {
					  <portlet:namespace/>param:'search',
					  <portlet:namespace/>key:keyword.get("value"),
					  <portlet:namespace/>idIdea:"<%= idea.getIdeaID()%>",
					},
			      dataType: 'json',
			      on: {
			      		success: function() {
			                	
			                	var data = this.get('responseData');
			                	$('#resultsArtefactDiv .collection').find('li').not('.template').remove();
			                	
			                	if(data.value.length==0){
			                		$('#resultsArtefactDiv').find('.noarts').removeClass('hide');
			                	}
			                	else{
			                		$('#resultsArtefactDiv').find('.noarts').addClass('hide');
				                	var template = $('#resultsArtefactDiv .template').first();
				                	for (i=0; i<data.value.length; i++) { 

				                		 var li = template.clone();
				                		 
				                		 li.find('.'+data.value[i].category).removeClass('hide');
				                		 li.find('.title').parent().attr('href', '<%=marketURL%>'+data.value[i].id);
           		 						 li.find('.title').text(data.value[i].titolo);
				                		 li.find('.author').text(data.value[i].author);
				                		 li.find('.type').text(data.value[i].type);
				                		 var anchor = li.find('.assoc');
				                		 if(data.value[i].associato=="no"){
				                			 anchor.attr('onclick', 'javascript:associa('+data.value[i].id+')');
				                			 anchor.attr('id', data.value[i].id);
				                			 anchor.text('<liferay-ui:message key="ims.associate"/>');
				                		 }
				                		 else{
				                			 anchor.attr('onclick', 'javascript:disassocia('+data.value[i].id+')');
				                			 anchor.attr('id', data.value[i].id);
				                			 anchor.text('<liferay-ui:message key="ims.disassociate"/>');
				                		 }
				                		 
				                		 li.removeClass('template');
				                		 li.removeClass('hide');
				                		 $('#resultsArtefactDiv .collection').append(li);
				                	}
			                	}
			            	}
			           }
		       });
	    
		   } 
	  }
	  
	);
}


function associa(id){
		YUI().use('aui-node', 'aui-io-request', function(Y) {
			   
		       Y.io.request(' <%=resourceURL.toString()%> ', {
				  data: {
						  <portlet:namespace/>param:'associa',
						  <portlet:namespace/>idArtefatto:id,
						  <portlet:namespace/>idIdea:"<%= idea.getIdeaID()%>",
						},
			      dataType: 'json',
			      on: {
			           success: function() {

		                		$("#noArtefact").css('display','none');
		                		$('#artefatti .row').css('display','block');
		                		
			                	var data = this.get('responseData');
			                	
			                	$('.artefact-carousel').removeClass('initialized');
			                	$('.artefact-carousel .indicators').remove();
			                	
			                	<% 
			                	String ptlmktUrl = themeDisplay.getPortalURL()+MyConstants.MKT_DETAIL_LINK;
			                	%>
			                	
			                	var link = "<%=ptlmktUrl%>"+id;

							   li00 = '<div class="carousel-item" id="item'+id+'" data-link="'+link+'">'
 							   		  +'<div class="card mini-card">'
					           		  +'<div class="card-content" style="background-image: url('+data.imgURl+');">';
					           
				            	<%
								/*il bottone con la x lo pu� vedere solo l'ente */
								if((idea.getIdeaStatus().equals(MyConstants.IDEA_STATE_IMPLEMENTATION) || idea.getIdeaStatus().equals(MyConstants.IDEA_STATE_MONITORING) ) && isEnteRiferimento ){
								%>
									li01 = '<i class="material-icons btn_close_artifact tooltipped" data-position="bottom" data-delay="50" data-tooltip="<liferay-ui:message key="ims.disassociate"/>" onClick="disassocia('+id+')">close</i>';
								<%}else{ %>
									li01 = '';
								<%}%>
					           	
								li02 = '<a class="shadow-layer white-text tooltipped" data-position="bottom" data-delay="50" data-tooltip="<liferay-ui:message key="view"/>" href="'+link+'" target="_blank">'
								        +'<span class="card-title weight-4 truncate">'+data.titolo+'</span>'
								        + '<div class="row"> <div class="col s12">'
								        + '<label><liferay-ui:message key="ims.provider"/></label>'
								        + ' <p title="'+data.providerName+'">'+data.providerName+'</p>'
								        + '</div>'
								        + '<div class="col s12">'
								        + '<label>&nbsp;</label>'
								        + '<p title="">&nbsp;</p>'
								        + '</div></div></a></div>'
								        + '<div class="card-action center">'
								        + '<a class="core-color-text text-color-2" type="submit" href="'+link+'" target="_blank">'
								        + '<liferay-ui:message key="ims.details"/>'
								        + '</a></div></div></div>';

								var li = li00+li01+li02;
			                	
			                	var divRes=$('#artefatti .artefact-carousel').append(li);
			                	
			                	$("#"+id).click(function(){
			                		disassocia(id);
			                	});
			                	$("#"+id).text('<liferay-ui:message key="ims.disassociate"/>');
			                	
			                	/*Add left and right arrows to navigate Artifacts if there are more than one associated. */
								if ($('#artefatti .artefact-carousel .carousel-item').length > 1){
					            	div = '<div class="carousel-fixed-item left">'
								     +'<a class="tooltipped" data-position="right" data-delay="50" data-tooltip="<liferay-ui:message key="previous"/>" href="javascript:$(\'#tableArtefatti\').carousel(\'prev\');"><i class="material-icons medium left">navigate_before</i></a>' 
								    +'</div>'
									+'<div class="carousel-fixed-item right">'
								      +'<a class="tooltipped" data-position="right" data-delay="50" data-tooltip="<liferay-ui:message key="next"/>" href="javascript:$(\'#tableArtefatti\').carousel(\'next\');"><i class="material-icons medium right">navigate_next</i></a>'
								    +'</div>';
								    
								    $('#artefatti .artefact-carousel').append(div);
						    	}

			                	$('.artefact-carousel').delay(800).carousel({full_width: true, indicators: true, dist:0});
			                }
			           }
			   });
		  }
	);
		
}


function disassocia(id){
	
		YUI().use('aui-node', 'aui-io-request', function(Y) {
			   
		       Y.io.request('<%=resourceURL.toString()%>', {
				  data: {
					  <portlet:namespace/>param:'disassocia',
					  <portlet:namespace/>idArtefatto:id,
					  <portlet:namespace/>idIdea:"<%= idea.getIdeaID()%>",
					},
	              dataType: 'json',
	              on: {
	                success: function() {
	                	
	                	if($(".artefact-carousel .carousel-item").length >= 1){
		                	$('.artefact-carousel').removeClass('initialized');
		                	$('.tooltipped').tooltip('remove');
		                	$('.artefact-carousel .indicators').remove();
		                
		                	 var child= $("#item"+id);
		                	 
		                	 if(document.getElementById(id)!=null){
		                		 
			                	 $("#"+id).click(function(){
			                		 associa(id);
			                	 });
				                 $("#"+id).text('<liferay-ui:message key="ims.associate"/>');
	
		                	 }
		                	 var divRes=$('#artefatti .artefact-carousel');
			                	
			                divRes.find("#item"+id).remove();
      	
							if ($('#artefatti .artefact-carousel .carousel-item').length == 1){
								/*Remove left and right arrows to navigate Artifacts if there is only one associated.*/
							    $('#artefatti .artefact-carousel .carousel-fixed-item.left').remove();
							    $('#artefatti .artefact-carousel .carousel-fixed-item.right').remove();
					    	}
			                
			                if($(".artefact-carousel .carousel-item").length < 1){
	                			$("#noArtefact").css('display','block');
	                			$('#artefatti .row').css('display','none');
	                		}else{
	                			$('.artefact-carousel').delay(800).carousel({full_width: true, indicators: true, dist:0});
	                			$('.tooltipped').tooltip({delay: 50});
	                		}
	                	}
	                }
	              }
			   }
			 );
		  }
		  
		);
		
	
}

</script >

<script type="text/javascript">

/*	delete the task 
	associated with jspf viewRequirements
*/
function deleteTask(id){
	
	var r=confirm("<liferay-ui:message key='are-you-sure-you-want-to-delete-this'/>");
	
	if (r==true){
	
			YUI().use(
			  'aui-node',
			  'aui-io-request',
			  function(Y) {
				   
			       Y.io.request('<%=resourceURL.toString()%>',
				   {
						data: {
						  <portlet:namespace/>param:'deleteTask',
						  <portlet:namespace/>taskId:id
						  
						},
				     	dataType: 'json',
				     	on: {
				                success: function() {
				                	window.location.reload(true);
				                }
				     	}
				    });
			  });
	}
	
}
</script>

<script type="text/javascript">

// 	cancella il requisito
// 	� associato alla jspf viewRequirements

function deleteReq(id){
	
	var r=confirm("<liferay-ui:message key='are-you-sure-you-want-to-delete-this'/>");
	
	if (r==true){
			YUI().use(
			  'aui-node',
			  'aui-io-request',
			  function(Y) {
			  
				   
			       Y.io.request('<%=resourceURL.toString()%>',
			 		   
				            {
							  data: {
							
								  <portlet:namespace/>param:'deleteReq',
								  <portlet:namespace/>reqId:id,
								},
				              dataType: 'json',
				              on: {
				                success: function() {
				                	window.location.reload(true);
				                }
				              }
				            }
				          );
			  }
			  
			);
	}
}

	
	function deleteIdea(ideaId){
	    var r=confirm("<liferay-ui:message key='ims.delete-idea-message'/>");
	    
	    var formName = "#<portlet:namespace/>fm2"+ideaId;
	    if (r==true){
	        $( formName ).submit();
	    }
	}
	
	
	function rejectIdea(){
	    var r=confirm('<liferay-ui:message key="ims.are-you-sure-you-want-to-reject-this-idea"/>');

	    if (r==true) {
	        $( '#<portlet:namespace/>fm_reject' ).submit();
	    }
	}
	
	function takeCharge(){
	    var r=confirm('<liferay-ui:message key="ims.are-you-sure-you-want-to-take-charge-of-this-idea"/>');

	    if (r==true) {
	        $( '#<portlet:namespace/>fm_takecharge' ).submit();
	    }
	}
	
	
	
</script>

<script type="text/javascript">

//var pageInitialized = false;
jQuery(document).ready(function() {
	
			
			<% //se l'utente � loggato verifico se la gara � preferita o no ed aggiungo le funzioni per la sua gestione
			if( currentUser != null){
				
				//verifico se l'idea � preferita o no
				if (isFavoriteIdea ){
					%>
					var isFavourite = true;
					<%
				}else{
					%>
					var isFavourite = false;
					<%
				}
				
				//aggiungo le funzioni per la gestione delle idee preferite
				%>
				$("#addToFavourite").unbind("click").click(function() {	
					if(!isFavourite){
						
						Liferay.Service(
								  '/Challenge62-portlet.clsfavouriteideas/add-favourite-idea',
								  {
								    ideaId: <%= ideaId %>,
								    userId: <%= currentUser.getUserId() %>
								  },
								  function(obj) {
									isFavourite = true;
									$("#addToFavourite").html('<i class="icon-bookmark"></i>\n<liferay-ui:message key="ims.remove-from-favourites"/>');
// 								    console.log(obj);
								  }
								);				
					}else{
						
						Liferay.Service(
								  '/Challenge62-portlet.clsfavouriteideas/remove-favourite-idea',
								  {
									  favIdeaId: <%= ideaId %>,
									  userId: <%= currentUser.getUserId() %>
								  },
								  function(obj) {
									  isFavourite = false;
									  $("#addToFavourite").html('<i class="icon-bookmark-empty"></i>\n<liferay-ui:message key="ims.add-to-favourites"/>');
// 								      console.log(obj);
								  }
								);
					}
				});
				
				function isInFavouriteIdeas(){
					Liferay.Service('/Challenge62-portlet.clsfavouriteideas/get-favourite-ideas',{
								ideaId: <%=ideaId%>,
					   			userId: <%= currentUser.getUserId() %>
								},
								function(obj) {
									if(obj.length>0){
					            		isFavourite = true;
					            		$("#addToFavourite").html('<i class="icon-bookmark"></i>\n<liferay-ui:message key="ims.remove-from-favourites"/>');
					            	}else{
					            		isFavourite = false;
					            		 $("#addToFavourite").html('<i class="icon-bookmark-empty"></i>\n<liferay-ui:message key="ims.add-to-favourites"/>');
					            	}    
								});
				}
				<%
			}
			%>
});

</script>

                  
<script>

	$(document).ready(function(){
		
		if($(".artefact-carousel .carousel-item").length > 0){
			
			$('#artefatti .row').css("display","block");
			
			
			$(document).on('click','.carousel-item a', function(){
				   var item = $(this).parent();
				   
				   if(item.css('z-index') == 0){
					    var link = item.attr('data-link');
					    window.open(link, '_blank');
					    window.focus();
				   }
			});
			$('.artefact-carousel').delay(1000).carousel({full_width: true, indicators: true, dist:0});
			
		}else{
			$('#artefatti .row').css("display","none");
			$('.carousel').not('.artefact-carousel').delay(1000).carousel({full_width: true, indicators: true, dist:0});
		}
	
		
		//Workaround perche' quando spunti il msg di operazione avvenuta con successo, dopo un commento, 
		//si agghiaccia tutto JS, non funzionano ad esempio i bottoni, quindi trovo questa classe ricarico
		//la pagina e tutto si sistema
		if ($('.alert-success').length > 0) {
			location.href="<%=shareUrl %>";
		}
		
	});
	
    $(document).ready(function(){
        $('.collapsible').collapsible({
          accordion : true // A setting that changes the collapsible behavior to expandable instead of the default accordion style
        });
    });
</script>


<!-- Documentation TOUR  -->
<script type="text/javascript">
	 $(window).load(function() {
// 	 $("body").append('<div class="tour-modal-bg"></div>');
	 });
</script>
<%-- <%@ include file="tour.jspf" %> --%>