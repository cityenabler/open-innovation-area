<%@page import="it.eng.rspa.ideas.utils.MyConstants"%>
<%@page import="it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties"%>
<%@page import="it.eng.rspa.ideas.utils.IdeasListUtils"%>
<%@ include file="/html/challenges/init.jsp" %>
<%@page import="java.util.Locale" %>
<%String ideaeDescription = ""; %>

<link href="<%=request.getContextPath()%>/css/requirements-popup.css" rel="stylesheet">

				
<%
	boolean isTakenUp = Boolean.valueOf(request.getParameter("takenUp"));
	boolean reducedLifecycle = IdeaManagementSystemProperties.getEnabledProperty("reducedLifecycle");
	boolean fundingBoxEnabled = IdeaManagementSystemProperties.getEnabledProperty("fundingBoxEnabled");

	PortletURL seleionaIdea = renderResponse.createActionURL();
	seleionaIdea.setParameter(ActionRequest.ACTION_NAME,"completeTask"); 
	seleionaIdea.setParameter("companyId",request.getParameter("companyId") );
	seleionaIdea.setParameter("userId",request.getParameter("userId") );
	seleionaIdea.setParameter("transictionName", request.getParameter("transictionName"));
	seleionaIdea.setParameter("ideaId", request.getParameter("ideaId"));
	seleionaIdea.setParameter("takenUp", "false" );
	
	PortletURL takeChargeIdea = renderResponse.createActionURL();
	takeChargeIdea.setParameter("userId", request.getParameter("userId") );
	takeChargeIdea.setParameter("transictionName", "takeCharge");
	takeChargeIdea.setParameter("ideaId", request.getParameter("ideaId"));
	takeChargeIdea.setParameter("takenUp", "true");
	takeChargeIdea.setParameter(ActionRequest.ACTION_NAME,"takeCharge");
	
	PortletURL fbRefinement = renderResponse.createActionURL();
	fbRefinement.setParameter("userId", request.getParameter("userId") );
	fbRefinement.setParameter("transictionName", "fbRefinement");
	fbRefinement.setParameter("ideaId", request.getParameter("ideaId"));
	fbRefinement.setParameter("takenUp", "false");
	fbRefinement.setParameter(ActionRequest.ACTION_NAME,"fbRefinement");
	
	
	CLSIdea idea =  CLSIdeaLocalServiceUtil.getCLSIdea( Long.valueOf(request.getParameter("ideaId")));
	String titoloGara = IdeasListUtils.getChallengeNameByIdeaId(idea.getIdeaID());
	String cityName = idea.getCityName();
	
	String actionToSend = seleionaIdea.toString();
		if (isTakenUp)
			actionToSend = takeChargeIdea.toString();
		
		if (fundingBoxEnabled)
			actionToSend = fbRefinement.toString();
		
	
						
%>

<div class="materialize">
	<div class="row">
		<div class="col s12">
			<a class="waves-effect btn-flat tooltipped" href="#" onClick="history.go(-1);" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.go-back"/>">
				<i class="icon-arrow-left left">&nbsp;</i><liferay-ui:message key="back"/>
			</a>
			<br/>
		</div>
		
		
		<div class="col s12">
		
				<% if (!(titoloGara.equals("") || isTakenUp)   ){ %>
				
				<h4 class="valign-wrapper valign-content-center">
				
				<% if (!reducedLifecycle){	%>
	  					<i class="material-icons icon-trophy small tooltipped" data-position="top" data-delay="40" data-tooltip="<liferay-ui:message key="ims.challenge"/>">&nbsp;</i>
	  				<%}else{	%>	
	  					<i class="fa <%=MyConstants.ICON_CLG_REDUCED %> small tooltipped" data-position="top" data-delay="40" data-tooltip="<liferay-ui:message key="ims.challenge"/>">&nbsp;</i>
	  				<%}%>	
				
				
					<%=titoloGara %>
				</h4>
				<br/>
				
				<% } %>
				<h5 class="valign-wrapper valign-content-center">
				<i class="material-icons icon-lightbulb small tooltipped" data-position="top" data-delay="40" data-tooltip="<liferay-ui:message key="ims.idea"/>">&nbsp;</i>
				<%=idea.getIdeaTitle() %>
				</h5>
				
				
				<% if (!idea.getCityName().isEmpty()){ %>
				<br/>
				<h6 class="valign-wrapper valign-content-center">
					
					<i class="material-icons small tooltipped" data-position="top" data-delay="40" data-tooltip="<liferay-ui:message key="city"/>">location_city</i>&nbsp;<%=idea.getCityName() %>
				</h6>
				<% } %>
				<br>
				<% if (!fundingBoxEnabled){ %>
					<div id="refinement-tip">
						<blockquote>
					      	<liferay-ui:message key="ims.tip-refinement-request-section"/>
					   </blockquote>
					</div>
				<% } %>
		</div>
		
	</div>


	<div class="addRequirementContent">
		<form method="POST" id="fm" action="<%= actionToSend %>">
		
			<div class="row">
			
				<div class="col s12">	
				<% if (!fundingBoxEnabled){ %>
					<div  id="requisitifunzionali" class="reqTypeTitle">
					    
						<b><liferay-ui:message key="ims.refinement-request"/></b> 
						<input hidden="true" id="numFunzionali" name="<portlet:namespace/>numFunzionali" value="0"></input>
						&nbsp;
					</div>
					<button class="btn btn_add_requirement" type="button" value="Add" onClick="addInputFunctional('requisitifunzionali');"><liferay-ui:message key="add"/></button>
					
					<button type="submit" class="btnSave btn btn_add_requirement" ><liferay-ui:message key="ims.save"/></button>
				<% }else{ %>	
				</div>
			
				<div class="col s12">	
					<h5><liferay-ui:message key="ims.the-refinement-requests-will-be"/></h5> 
		
					<ul class="browser-default">
					  	<li>Project information
							  <ul>
							    <li>Challenges and solutions</li>
								<li>Setup and configuration</li>
								<li>Technical feasibility analysis</li>
								<li>Hardware and network diagrams  <i>(a file with the selected Generic Enablers will be  created automatically)</i></li>
							  </ul>
						</li>
						<li>Project approach
						  	<ul>
							    <li>Major requirements</li>
								<li>Kpi measurement methods</li>
								<li>Standars interoperability</li>
								<li>Replicability scalability and sustainability</li>
								<li>Demostration deployment phases</li>
								<li>Test plan and results</li>
							</ul>
						</li>
						<li>Project impact
						    <ul>
							    <li>Socio economic and societal impact</li>
							    <li>Differentation</li>
							    <li>Others</li>
							    <li>Benefits</li>
						    </ul>
			     	   </li>
					   <li>Project team
						  	<ul>
							    <li>partners</li>
							</ul>
					   </li>
					</ul>
				</div>
		</div>	
		
		<div class="row">	
		<% if (cityName.isEmpty()){ %>
				
			<div class="col s12">
					<blockquote>
					    <b>You have not yet entered the City name for this idea!</b>
					</blockquote>
				
					<br>

					<div class="input-field ">
						<input name="<portlet:namespace/>cityName"
							type="text"
							class="general-info"
							maxlength="60"
							id="<portlet:namespace/>cityName"
							value="" />
							<label for="<portlet:namespace/>cityName"> <liferay-ui:message key="ims.city-name-placeholder"/>&nbsp;*</label>
					</div>
			</div>
		</div>
		</form>
		<button id="buttonSave" onclick="checkAndGoForm();" class="btnSave btn btn_add_requirement" ><liferay-ui:message key="proceed"/></button>
			
		<% 	}else{ %>
		<div class="row">
			<div class="col s12">
				<button type="submit" class="btnSave btn btn_add_requirement" ><liferay-ui:message key="proceed"/></button>
			</div>
		</div>
		</form>
		<% 	}
		}%>
		
	</div>

</div>
		
<script type="text/javascript">

function checkAndGoForm(){
	
	 var ideaCityName = document.getElementById('<portlet:namespace/>cityName').value;
	 
	 if (ideaCityName.trim() == "") {
			alert('<liferay-ui:message key="ims.insert-cityname"/>');
    }else{
    	$("#fm").submit();
    }
}


	$(document).ready(function(){
    
		<% if (!fundingBoxEnabled){ %>
			addInputFunctional('requisitifunzionali');
		<% }%>
 });

function addInputFunctional(divName){
   		
		 var counter=document.getElementById("numFunzionali").getAttribute("value");
		 counter++;
         var newdiv = document.createElement('div');
         newdiv.innerHTML = "<span>"+counter+"</span> <textarea id=\"testorequisitifunzionali"+(counter)+"\" name=\"<portlet:namespace/>testorequisitifunzionali"+(counter)+"\" ></textarea>" ;
         document.getElementById(divName).appendChild(newdiv);
        
         document.getElementById("numFunzionali").setAttribute("value", counter);  
}



</script>			
