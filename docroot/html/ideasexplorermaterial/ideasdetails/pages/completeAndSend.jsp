<%@page import="com.liferay.portal.kernel.json.JSONArray"%>
<%@page import="com.liferay.portal.kernel.json.JSONObject"%>
<%@page import="it.eng.rspa.ideas.utils.FundingboxUtils"%>
<%@page import="it.eng.rspa.ideas.utils.FiwareUtils"%>
<%@page import="com.liferay.tasks.model.TasksDetails"%>
<%@page import="it.eng.rspa.ideas.utils.TaskUtils"%>
<%@page import="it.eng.rspa.ideas.utils.MyConstants"%>
<%@page import="it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties"%>
<%@page import="it.eng.rspa.ideas.utils.IdeasListUtils"%>
<%@ include file="/html/challenges/init.jsp" %>
<%@page import="java.util.Locale" %>
<%String ideaeDescription = ""; %>


				
<%
	boolean reducedLifecycle = IdeaManagementSystemProperties.getEnabledProperty("reducedLifecycle");
	boolean fundingBoxEnabled = IdeaManagementSystemProperties.getEnabledProperty("fundingBoxEnabled");

	
	
	CLSIdea idea =  CLSIdeaLocalServiceUtil.getCLSIdea( Long.valueOf(request.getParameter("ideaId")));
	String clgTitle = IdeasListUtils.getChallengeNameByIdeaId(idea.getIdeaID());
	String cityName = idea.getCityName();

		
	
						
%>

<div class="materialize">
	<div class="row">
		<div class="col s12">
			<a class="waves-effect btn-flat tooltipped" href="#" onClick="history.go(-1);" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.go-back"/>">
				<i class="icon-arrow-left left">&nbsp;</i><liferay-ui:message key="back"/>
			</a>
			<br/>
		</div>
		
		
		<div class="col s12">	
		
	
				<h4 class="valign-wrapper valign-content-center">
				
	  				<i class="fa <%=MyConstants.ICON_CLG_REDUCED %> small tooltipped" data-position="top" data-delay="40" data-tooltip="<liferay-ui:message key="ims.challenge"/>">&nbsp;</i>
				
					<%=clgTitle %>
				</h4>
				<br/>
				

				<h5 class="valign-wrapper valign-content-center">
					<i class="material-icons icon-lightbulb small tooltipped" data-position="top" data-delay="40" data-tooltip="<liferay-ui:message key="ims.idea"/>">&nbsp;</i>
					<%=idea.getIdeaTitle() %>
				</h5>
				
				<h4 class="valign-wrapper valign-content-center">
					<liferay-ui:message key="ims.refinement-resume"/> 
				</h4>

			<blockquote>
				Send your refinement outputs to Fundingbox. After the sending, you will be redirected to Fundingbox, where the idea will be in "Draft status". There, you'll be still able to modify your contributes and confirm it definitively
			</blockquote>
			
			
			

<!-- rob		 -->
		<ul class="collection">
    		<li class="collection-item avatar">
   				<i class="material-icons circle cyan lighten-3">folder</i>
		      	<h5>Project information</h5>
				<DL>
		         	<DT>Title
					<DD><%=idea.getIdeaTitle() %>
				</DL>
				<DL>
		         	<DT>Description
					<DD><%=IdeasListUtils.getSocialSummaryByIdeaNotLimited(idea) %>
				</DL>


		      	<DL>
		      		<DT>Challenges and solutions
		      		<DD>
						  	<% List<TasksDetails> tasksDetails1 = TaskUtils.getSolutionsByIdeaReq(idea, "Challenges and solutions");
						    		for (TasksDetails td:tasksDetails1){			    
						    %> 
								Challenge: <%=td.getDescription() %> <br> Solution: <i><%=td.getSolution() %></i>
								<br><br>
						    <%}%>
				</DL>
				
				<DL>
		         	<DT>Setup and configuration
					<DD><%=TaskUtils.getSolutionByIdeaReq(idea, "Setup and configuration") %>
				</DL>

				<DL>
					<DT>Technical feasibility analysis
					<DD><%=TaskUtils.getSolutionByIdeaReq(idea, "Technical feasibility analysis") %> 
				</DL>

				<DL>		
					<DT>Hardware and network diagrams
					<DD>
						<% List<TasksDetails> tasksDetails2 = TaskUtils.getSolutionsByIdeaReq(idea, "Hardware and network diagrams");
				    		for (TasksDetails td:tasksDetails2){			    
				   		 %> 
							<a href="<%=TaskUtils.getFileOutcomeLinkByTaskDetails(td) %>"   target="_blank"><%=td.getDescription() %>  </a>
							<br>
				    	<%}	 
				    		
				    		boolean isIdeaWithSelectedGEs = FiwareUtils.isIdeaWithSelectedGEs(idea.getIdeaID());
							if (isIdeaWithSelectedGEs){
								
								FiwareUtils.createSavePdfByIdeaId(idea.getIdeaID(), renderRequest);
								
						%>	
							<a href="<%=FiwareUtils.getUrlFilePdfGE(idea.getIdeaID()) %>"   target="_blank">Selected Generic Enablers</a>
							<br>	
						<%	}				    		
				    		
				   		 %> 
		      	</DL>
			</li>
			<li class="collection-item avatar">
   				<i class="material-icons circle cyan lighten-3">folder</i>
		      	<h5>Project approach</h5>
		      	<DL>
	      			<DT>Major requirements
				    <DD><%=TaskUtils.getSolutionByIdeaReq(idea, "Major requirements") %> 
				</DL>
				<DL>    
					<DT>KPI measurement methods
					<DD><% List<TasksDetails> tasksDetails3 = TaskUtils.getSolutionsByIdeaReq(idea, "KPI measurement methods");
				    		for (TasksDetails td:tasksDetails3){			    
				   		 %> 
							KPI: <%=td.getDescription() %> <br> Measurement: <i><%=td.getSolution() %></i>
							<br><br>
				    	<%}	    
				   		 %>
				</DL>
				<DL> 	
					<DT>Standars interoperability
					<DD><%=TaskUtils.getSolutionByIdeaReq(idea, "Standars interoperability") %> 
				</DL>
				<DL>	
					<DT>Replicability scalability and sustainability
					<DD><%=TaskUtils.getSolutionByIdeaReq(idea, "Replicability scalability and sustainability") %> 
				</DL>
				<DL>					
					<DT>Demostration deployment phases
					<DD><%=TaskUtils.getSolutionByIdeaReq(idea, "Demostration deployment phases") %> 
				</DL>
				<DL>					
					<DT>Test plan and results
					<DD><%=TaskUtils.getSolutionByIdeaReq(idea, "Test plan and results") %>
		      	</DL>
			</li>
			
			<li class="collection-item avatar">
   				<i class="material-icons circle cyan lighten-3">folder</i>
		      	<h5>Project impact</h5>

				<DL>	
					<DT>Socio economic and societal impact
				    <DD><%=TaskUtils.getSolutionByIdeaReq(idea, "Socio economic and societal impact") %> 
				</DL>
				<DL>				    
				    <DT>Differentation
				   	<DD><%=TaskUtils.getSolutionByIdeaReq(idea, "Differentation") %> 
				</DL>
				<DL>				    
				   <DT>Others
				   <DD><%=TaskUtils.getSolutionByIdeaReq(idea, "Others") %> 
				</DL>
				<DL>				    
				    <DT>Benefits
				    <DD><%=TaskUtils.getSolutionByIdeaReq(idea, "Benefits") %> 
				</DL>
			</li>
			
			<li class="collection-item avatar">
   				<i class="material-icons circle cyan lighten-3">folder</i>
		      	<h5>Project team</h5>
		      	<DL>				    
				    <DT>Team lead
				    <DD><%=IdeasListUtils.getAuthorNameByIdea(idea) %> - <%=IdeasListUtils.getAuthorEmailByIdea(idea) %>
				</DL>
				
				<%JSONArray coworker = IdeasListUtils.getCoworkewsByIdea(idea);
				
				if (coworker.length()>0){
				%>
			      	<DL>	
						<DT>Members
					    <DD><% 
					    		for(int i=0; i<coworker.length(); i++){	
					    			
					    			 JSONObject jsObj = coworker.getJSONObject(i);
					    	%> 
								<i><%=jsObj.getString("member_name") %> - <%=jsObj.getString("member_email") %></i> <br>
					    	<%} %>
			      	</DL>
		      	<%} %>
		      	<DL>				    
				    <DT>City
				    <DD><%=idea.getCityName() %> 
				</DL>
		      	<DL>	
					<DT>Partners
				    <DD><% List<TasksDetails> tasksDetails = TaskUtils.getSolutionsByIdeaReq(idea, "Partners");
				    		for (TasksDetails td:tasksDetails){			    
				    	%> 
							<i><%=td.getDescription() %></i> <br>
				    	<%} %>
		      	</DL>
		      	
			</li>
		</ul>
<!-- fine rob		 -->


		</div>
							
							
			<%	
				PortletURL sendIdeaURL = renderResponse.createActionURL();
			sendIdeaURL.setParameter(ActionRequest.ACTION_NAME,"sendIdeaToFundingBox");
			%>	
				<aui:form method="POST" action="<%=sendIdeaURL.toString()%>" name="fmSend" cssClass="btnAction">

					<aui:input name="transictionName" type="hidden" value="<%=MyConstants.IDEA_STATE_COMPLETED %>"></aui:input>
					<aui:input name="ideaId" type="hidden" value="<%=idea.getIdeaID()%>"></aui:input>
					
				</aui:form>
				
				<button class="btn" type="button"  onClick='areYouSure();' >
						<i class="material-icons right">send</i>
							Send idea to Fundingbox
				</button>
		
		
	</div>
</div>

<script>
function areYouSure(){
	
	
	var r=confirm("Are you sure to send this idea to Fundingbox?");

	if (r==true){
		
		var formName = "#<portlet:namespace/>fmSend";
		$( formName ).submit();
	}
}
</script>
		
		



		
		
