<!-- ATTENZIONE! Questa pagina � legata alla pagina /html/ideasexplorermaterial/ideasdetails/pages/screenshots.jsp -->

<%@ include file="/html/challenges/init.jsp" %>
<%
String ratingTitle = request.getParameter("title");
String className = request.getParameter("classname");

long classPK = Long.parseLong(request.getParameter("classPK"));

%>
<label><liferay-ui:message key="ratings"/></label>
<liferay-ui:ratings className="<%= className %>" classPK="<%=classPK %>"/>