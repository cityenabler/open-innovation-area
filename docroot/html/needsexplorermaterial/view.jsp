<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.CLSCategoriesSetLocalServiceUtil"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties"%>
<%@page import="it.eng.rspa.ideas.utils.MyUtils"%>
<%@page import="javax.portlet.PortletRequest"%>
<%@page import="com.liferay.portlet.PortletURLFactoryUtil"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@page import="javax.portlet.WindowState"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="javax.portlet.PortletSession"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ include file="/html/challenges/init.jsp" %>

<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %> 
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>
<portlet:resourceURL var="resourceURL" />

<portlet:defineObjects />
<liferay-theme:defineObjects/>

<%@ include file="/html/home/sections/checkPilot.jspf" %>

<%
User currentUser = PortalUtil.getUser(request);
boolean isIMSSimpleUser =  MyUtils.isIMSSimpleUser(currentUser);
boolean isEnte = MyUtils.isAuthority(currentUser);

String pilot = IdeasListUtils.getPilot(renderRequest);
String urlHomeIms = IdeaManagementSystemProperties.getUrlHomeIMS();
String urlcreateNeedWizard = IdeaManagementSystemProperties.getFriendyUrlCreateNeedWizard();

List<CLSCategoriesSet> categoriesList = CLSCategoriesSetLocalServiceUtil.getCLSCategoriesSets(0, CLSCategoriesSetLocalServiceUtil.getCLSCategoriesSetsCount());
%>




<div class="materialize">
		
	  	<div class="section row valign-wrapper" style="position: relative; height: 70px;">
	  	
	  		<div class="col s4 valign left">
	  			<a class="waves-effect btn-flat tooltipped" href="<%=urlHomeIms %>" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.go-back"/>">
	  				<i class="icon-arrow-left left">&nbsp;</i><liferay-ui:message key="back"/>
	  			</a>
	  		</div>
			<div class="col s4 valign center">
			
			
		<% if (!isEnte){	%>
	  			<a class="waves-effect btn core-color color-2 tooltipped
	  			
	  			<% if ( Validator.isNull(currentUser)) {	%>
	  				
	  				disabled" data-tooltip="<liferay-ui:message key="ims.login-to-create-need"/>"
	  			<%	}else{	%>
	  				  			
	  			" href="<%=urlcreateNeedWizard %>" data-tooltip="<liferay-ui:message key="ims.create-new-need"/>"
	  			
	  			<%	}%>
	  			
	  			data-position="top" data-delay="50" >
	  			
	  			
	  			
	  				<span class="fa fa-commenting-o">&nbsp;</span><span class="hide-on-small-only"><liferay-ui:message key="create"/></span>
	  			</a>
	  	<%	}else{%>
		  	
		  		<h3 class="flow-text strong"><liferay-ui:message key="ims.needs"/></h3>
		  		<div class="divider"></div>
		  	
		<%} %>
	  			
	  		</div>
			<div class="col s4 valign right">
				  <div class="fixed-action-btn horizontal" style="position: absolute; top:7px; z-index:300;">
				    <a class="btn-floating btn-large waves-effect btn-large core-color color-2 tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.tip-wrench-needs-explorer"/>">
				      <span class="icon-wrench" ></span>
				    </a>
				    <ul>
				    	<!-- Modal Trigger -->
				      <li><a class="modal-trigger-need btn-floating waves-effect core-color color-1 tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="filters"/>" href="#modal-filter">
				      	<i class="icon-filter"></i></a></li>
				      	<!-- Modal Trigger -->
				      <li><a class="modal-trigger-need btn-floating waves-effect core-color color-1 tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="sort-by"/>" href="#modal-sorting">
				      	<i class="material-icons">sort_by_alpha</i></a></li>
				    </ul>
				  </div>
			 </div>
			 
		</div>

      	<%@ include file="filter-panel.jspf" %>
      	<%@ include file="sorting-panel.jsp" %>
      	
      	<div class="row container-card">
      		<%@ include file="../loader.jspf" %>
			<!-- qui attacco le idee create con Ajax -->
 		</div> 
 		<!-- Fine Row Idee -->
 		<%@ include file="need-card.jsp" %>
 		

</div>

<script>
    
(function($){
	
		  $(function(){
				
				//Attiva la modale
				$('.modal-trigger-need').leanModal({
				    dismissible: true, // Modal can be dismissed by clicking outside of the modal
				    opacity: .5, // Opacity of modal background
				    in_duration: 300, // Transition in duration
				    out_duration: 200, // Transition out duration
				  //  ready: function() { console.log('Ready'); }, // Callback for Modal open
				    complete: function() { //alla chiusura del pannello dei filtri
				    	listaIdeeAjax();
				    } // Callback for Modal close
				  }
				);
				
				//Attiva gli switch legati ad una checkbox
				$( ".activeSort" ).change(function() {
					  var $input = $( this );
					  var $lever = $input.parent().parent().find( ".switch input" );
					  
					  if($input.is( ":checked" )) {
						  $lever.prop( "disabled", false );
					  }else {
						  $lever.prop( "disabled", true );
					  } 
					  
				}).change();
				
				
				listaIdeeAjax();
				
				$(document).ready(function(){
				    setTimeout(function(){ $('.tooltipped').tooltip({delay: 50});}, 3000);
				    
				    <% if (isIMSSimpleUser){%>
				    	/*only myNeed by the parameter my*/
					    var myNeeds = getUrlParam('my');
					    if (myNeeds){
					    	 $('#pers1').prop('checked', true);
					    }
			    
			    	<% }%>
				});
				
		  }); // end of document ready
		  
		  
		  
		  function  listaIdeeAjax(){
				
				//chiamata Ajax per caricare la lista delle idee
				 AUI().use('aui-io-request', function(A){
					
					  
					//if inline per settare i valori dei filtri sui vocabulary di Liferay-categorie nella GUI
		<%			for (int i = 0; i< categoriesList.size(); i++){	%>
					 	
	 	 				(($('#cat4<%=i %>').is(":checked"))? cat<%=i %> = "true" : cat<%=i %> = "false");
	 	
	 	<%			}	%>
					  
					  (($('#pers1').is(":checked"))? pers1 = "true" : pers1 = "false");
					  (($('#pers2').is(":checked"))? pers2 = "true" : pers2 = "false");
					  (($('#pers3').is(":checked"))? pers3 = "true" : pers3 = "false");
					  
					//if inline per l'ordinamento delle liste
					   (($('#creationDate').is(":checked"))? sortByDate = "true" : sortByDate = "false");
					   (($('#sortDate').is(":checked"))? sortDateAsc = "true" : sortDateAsc = "false");
					   (($('#ratingSort').is(":checked"))? sortByRating = "true" : sortByRating = "false");
					   (($('#rateSort').is(":checked"))? sortRatingLower = "true" : sortRatingLower = "false");
					   
					
				     A.io.request('<%=resourceURL.toString()%>', {
				               method: 'post',
				               data: {
				            	   <portlet:namespace />param: 'IdeasList',
				            	   <portlet:namespace />localePerAssetVoc: '<%=locale%>',
				            	   <portlet:namespace />pilot: '<%=pilot%>',
				            	   <portlet:namespace />isNeed: "true",
				            	   <portlet:namespace />stateSelection: "false",
				            	   <portlet:namespace />stateRefinement: "false",
				            	   <portlet:namespace />stateImplementation: "false",
				            	   <portlet:namespace />stateMonitoring: "false",
	
			<%			for (int i = 0; i< categoriesList.size(); i++){	%>    
						           <portlet:namespace />cat<%=i %>: cat<%=i %>,
						            
			<%	}%> 	   
				            	   
				            	   
				            	   
				            	   <portlet:namespace />pers1: pers1,
				            	   <portlet:namespace />pers2: pers2,
				            	   <portlet:namespace />pers3: pers3,
				            	   <portlet:namespace />sortByDate: sortByDate,
				            	   <portlet:namespace />sortDateAsc: sortDateAsc,
				            	   <portlet:namespace />sortByRating: sortByRating,
				            	   <portlet:namespace />sortRatingLower: sortRatingLower
				            	   
				               },
				               dataType: 'json',
				               on: {
				                   success: function() {
				                    				                	 
				                	   var jsonIdeasArray = this.get('responseData');
					                   appendMoreIdeas(jsonIdeasArray);
					                   
					                   $('.tooltipped').tooltip({delay: 50}); /*initialize the tooltip */
					     				                  	                   	                	
				                   }//success
				              }//on					               
				        });//A.io.request					 
				 });//AUI().use
				 
			}//listaIdeeAjax
		  
		  
		  
		  
	function appendMoreIdeas(jsonIdeasArray){
		    	
		var contenitore = jQuery('.container-card');
		
		var templateP = jQuery('.schedaIdea').first();
		$(contenitore).empty();
			    
	 	jQuery.each(jsonIdeasArray, function(index, idea) {
					
	 		var template = templateP.clone();
	 		
	 		template.css('display','block');
	 		template.removeClass('schedaIdea');//pulizia
	 		
	 		template.find('.card').attr("id","idea"+idea.idIdea);
	 		
	 		if (idea.Vocabulary != ""){
	 		
	 			template.find('.titoloVocabolario').text(idea.Vocabulary);
	 			template.find('.titoloVocabolario').parent().attr("data-tooltip",idea.Vocabulary);
	 		}else{
	 			
	 			template.find('.titoloVocabolario').text("<liferay-ui:message key='ims.no-category-associated'/>");
	 			template.find('.titoloVocabolario').parent().attr("data-tooltip","<liferay-ui:message key='ims.no-category-associated'/>");
	 		}
	 		
	 		template.find('.iconaVocabolario').addClass( idea.iconVocabulary );
	 		template.find('.coloreVocabolario').addClass( idea.colorVocabulary );
	 		
	 		template.find('.dropdown-buttonMore').attr("data-activates","dropdown-"+index);
	 		template.find('.needLink').attr("href", idea.IdeaFriendlyUrl);
	 		template.find('.numNeedDropdown').attr("id","dropdown-"+index);
	 		
	 		var titleLink = template.find('.titoloIdea');
			template.find('.card-multi-title').text(idea.Title);
// 			template.find('.card-multi-title').append(titleLink);
			template.find('.titoloIdea').attr('href',idea.IdeaFriendlyUrl)
	 		template.find('.autoreIdea').text(idea.Author);
					 
			var ideaCategories = idea.Categories;
					 						 
			var categoriesAll="";
			if(ideaCategories.length>0){
		 		jQuery.each(ideaCategories, function(index, category) {
							 
				 	if (index!=ideaCategories.length-1){
							 categoriesAll+="<small>"+category+"</small>,&nbsp;";
					}else{
							 categoriesAll+="<small>"+category+"</small>";
				 	}
	 			});
			}else{
				categoriesAll = '<small><liferay-ui:message key="there-are-no-categories"/></small>';
			}
			template.find('.categorieIdea').html(categoriesAll);
	 		template.find('.dataCreazioneIdea').html(idea.CreationDate);
			 
		 	var numStelline = idea.RatingsStar;
					 
	 		var stellineS="";
		 	for (i = 0; i < numStelline; i++) {
				 stellineS+='<i class="icon-star"></i>';
		 	}
		 	for (i = 0; i < 5-numStelline; i++) {
				 stellineS+='<i class="icon-star-empty"></i>';
			}
						 
		 	template.find('.votoMedio').html(stellineS);
						 
		 	template.find('.enteIdea').text(idea.Authority);
		 	template.find('.imgRappresentativa').attr('src',idea.ImageURL);
		 	
			if(idea.ImageURL.indexOf('-portlet/img/') > -1) {
				template.find('.sampleText').html("<liferay-ui:message key='ims.sample-text'/>");
				template.find('.sampleText').css({'display':'block','position':'relative'});
			}else{
				template.find('.sampleText').css({'display':'none'});
			}
		 	
// 			template.find('.descrizioneIdea p.descrLink a.moreDescr').html(idea.Description);
// 			template.find('.descrizioneIdea p.descrLink a.moreDescr').attr('href', idea.IdeaFriendlyUrl); 	
// 			template.find('.descrizioneIdea a.readmore').attr('href', idea.IdeaFriendlyUrl);
			template.find('.descrizioneIdea p.descrLink').html(idea.Description);
			
			template.find('.numGare').text(idea.NumGare);
			template.find('.numIdeas').text(idea.numIdeas);
			
			if (idea.isAuthorOrCoworker){
		 		template.find('.needLink').html('<liferay-ui:message key="ims.view-or-manage-need"/>');
		 		template.find('.viewNeed').attr("data-tooltip",'<liferay-ui:message key="ims.view-or-manage-need"/>' );
		 		template.find('.owner_label').removeClass("hide");
		 	}
			if(idea.isAuthor){
		 		template.find('.author').removeClass("hide").attr("data-tooltip",'<liferay-ui:message key="ims.you-are-author"/>' );
		 	}
			
			
			jQuery(contenitore).append(template);
		 	
		 	//rende tutto il badge cliccabile
// 		 	$("#idea"+idea.idIdea).on("click", function(){
// 		 	     window.location.href=idea.IdeaFriendlyUrl;
// 		 	 })
		 	
		});//foreach jsonArray
		
		if (jsonIdeasArray.length == 0 ){
			
			jQuery(contenitore).append("<p class='infoText bottomSpace'><liferay-ui:message key='ims.no-need'/></p>");
			
		}
		
		/*  Permette Ellipsis su multiriga */
		/*
	 	$('.multi-truncate').dotdotdot({
			ellipsis: '...',
			height: 120,
			watch: 'window',
			after: 'a.readmore'
		});
		*/
		
		/* Hide READMORE if description have an height under 120px (max limit) */
	 	$('.descrLink').each(function( index ) {
	 		$this = $( this );
// 	 		console.log(index+" "+$this.height());
	 		 if($this.height() < 100) {
	 			$this.parent().find("a.readmore").css("display", "none")
	 		};
	 	});
		
	 	$('.dropdown-buttonMore').dropdown({
	 	      inDuration: 300,
	 	      outDuration: 225,
	 	      constrain_width: false, // Does not change width of dropdown to that of the activator
	 	      hover: true, // Activate on hover
	 	      gutter: 0, // Spacing from edge
	 	      belowOrigin: false, // Displays dropdown below the button
	 	      alignment: 'left' // Displays dropdown with edge aligned to the left of button
	 	    }
	 	  );
		
	}//funzione	appendMoreIdeas	  
		  
		  
})(jQuery); // end of jQuery name space
	
</script>
