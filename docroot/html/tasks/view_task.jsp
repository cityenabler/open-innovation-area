<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This file is part of Liferay Social Office. Liferay Social Office is free
 * software: you can redistribute it and/or modify it under the terms of the GNU
 * Affero General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * Liferay Social Office is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Liferay Social Office. If not, see http://www.gnu.org/licenses/agpl-3.0.html.
 */
--%>

<%@ include file="/html/tasks/init.jsp" %>

<%
long tasksEntryId = ParamUtil.getLong(request, "tasksEntryId");

TasksEntry tasksEntry = TasksEntryLocalServiceUtil.fetchTasksEntry(tasksEntryId);
%>

<div class="materialize">

<c:choose>
	<c:when test="<%= tasksEntry == null %>">
		<span class="alert alert-error"><liferay-ui:message key="task-could-not-be-found" /></span>
	</c:when>
	<c:otherwise>

		<%
		tasksEntry = tasksEntry.toEscapedModel();

		Calendar dueDate = CalendarFactoryUtil.getCalendar(timeZone, locale);
		%>

		
		<h5><%= HtmlUtil.unescape(tasksEntry.getTitle()) %></h5>
		<br>

		<div class="task-data-container">
			<div class="task-data assignee">
				<c:choose>
					<c:when test="<%= tasksEntry.getAssigneeUserId() > 0 %>">

						<%
						String assigneeDisplayURL = StringPool.BLANK;
						String taglibAssigneeDisplayURL = LanguageUtil.get(pageContext, "unknown-user");

						User assigneeUser = UserLocalServiceUtil.fetchUser(tasksEntry.getAssigneeUserId());

						if (assigneeUser != null) {
							assigneeDisplayURL = assigneeUser.getDisplayURL(themeDisplay);

							taglibAssigneeDisplayURL = "<a href=\"" + assigneeDisplayURL + "\">" + HtmlUtil.escape(tasksEntry.getAssigneeFullName()) + "</a>";
						}
						%>

						<b><liferay-ui:message arguments="<%= taglibAssigneeDisplayURL %>" key="assigned-to-x" translateArguments="<%= false %>" /></b>
					</c:when>
					<c:otherwise>
						<liferay-ui:message key="unassigned" />
					</c:otherwise>
				</c:choose>
			</div>
			<br>
			<div class="task-data reporter">

				<%
				String reporterDisplayURL = StringPool.BLANK;
				String taglibReporterDisplayURL = LanguageUtil.get(pageContext, "unknown-user");

				User reporterUser = UserLocalServiceUtil.fetchUser(tasksEntry.getUserId());

				if (reporterUser != null) {
					reporterDisplayURL = reporterUser.getDisplayURL(themeDisplay);

					taglibReporterDisplayURL = "<a href=\"" + reporterDisplayURL + "\">" + HtmlUtil.escape(tasksEntry.getReporterFullName()) + "</a>";
				}
				%>

				<b><liferay-ui:message arguments="<%= taglibReporterDisplayURL %>" key="created-by-x" translateArguments="<%= false %>" /></b>
			</div>
			<br>
			<div class="task-data last modified-date">
				<b><liferay-ui:message arguments="<%= dateFormatDateTime.format(tasksEntry.getModifiedDate()) %>" key="modified-on-x" translateArguments="<%= false %>" /></b>
			</div>
		</div>
		<br>
		<div class="task-data-table lfr-table">
			<br>
			<b class="lfr-label"><liferay-ui:message key="status" />:</b>
			<span class="task-data status">
				<liferay-ui:message key="<%= tasksEntry.getStatusLabel() %>" />
			</span>
			<br>
			<br>
			<b class="lfr-label"><liferay-ui:message key="priority" />:</b>
			<span class="task-data <%= tasksEntry.getPriorityLabel() %>">
				<liferay-ui:message key="<%= tasksEntry.getPriorityLabel() %>" />
			</span>
			<br>
		<c:if test="<%= tasksEntry.getDueDate() != null %>">
				<br>
				<b class="lfr-label"><liferay-ui:message key="due-date" />:</b>
				<span class="task-data due-date">
						<%= dateFormatDateTime.format(tasksEntry.getDueDate()) %>
				</span>
				<br>
		</c:if>

				<br />
				

<!-- 			<b class="lfr-label"> -->
<%-- 				<liferay-ui:message key="tags" /> --%>
<!-- 			</b> -->
<!-- 			<span> -->
<%-- 				<liferay-ui:asset-tags-summary --%>
<%-- 					className="<%= TasksEntry.class.getName() %>" --%>
<%-- 					classPK="<%= tasksEntry.getTasksEntryId() %>" --%>
<%-- 				/> --%>
<!-- 			</span> -->

		</div>
		
	</c:otherwise>
</c:choose>

<br>
<br>
	<a class="btn" href="<portlet:renderURL windowState="<%=WindowState.MAXIMIZED.toString()%>">
								<portlet:param name="jspPage" value="/html/ideasexplorermaterial/ideasdetails/view.jsp" />
			    				<portlet:param name="ideaId" value="<%=new Long(tasksEntry.getIdeaId()).toString()%>"/>
			 				 </portlet:renderURL>"> <liferay-ui:message key="ims.return-to-idea" />
	</a>
			 				 
</div>			 				 