<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>
<%@ page isELIgnored ="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@page import="com.liferay.util.portlet.PortletProps"%>
<%@page import="java.io.InputStream" %>

<%@page import="java.util.*" %>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="javax.portlet.PortletMode"%>
<%@page import="javax.portlet.WindowState"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="javax.portlet.ActionRequest"%>

<%@page import="com.liferay.portal.model.User"%>
<%@page import="com.liferay.portal.model.Image"%>
<%@page import="com.liferay.portal.model.Website"%>
<%@page import="com.liferay.portlet.PortletURLUtil"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@page import="com.liferay.portal.util.PortalUtil" %>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@page import="com.liferay.portal.kernel.util.HttpUtil"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@page import="com.liferay.portlet.asset.model.AssetEntry"%>
<%@page import="com.liferay.portlet.calendar.model.CalEvent"%>
<%@page import="com.liferay.portlet.ratings.model.RatingsEntry"%>
<%@page import="com.liferay.portlet.ratings.model.RatingsStats"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="com.liferay.portal.service.ImageLocalServiceUtil"%>
<%@page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.CalendarFactoryUtil"%>
<%@page import="com.liferay.portlet.ratings.service.RatingsEntryServiceUtil"%>
<%@page import="com.liferay.portlet.asset.service.AssetEntryLocalServiceUtil"%>
<%@page import="com.liferay.portlet.ratings.service.persistence.RatingsStatsUtil"%>
<%@page import="com.liferay.portlet.ratings.service.persistence.RatingsEntryUtil"%>
<%@page import="com.liferay.portlet.ratings.service.RatingsEntryLocalServiceUtil"%>
<%@page import="com.liferay.portlet.ratings.service.RatingsStatsLocalServiceUtil"%>
<%@page import="com.liferay.portal.security.permission.PermissionCheckerFactoryUtil"%>
<%@page import="com.liferay.portal.security.permission.PermissionChecker"%>

<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeLocalServiceUtil"%>

<portlet:defineObjects />