<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.VirtuosityPointsLocalServiceUtil"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.impl.VirtuosityPointsLocalServiceImpl"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.NeedLinkedChallengeLocalServiceUtil"%>
<%@page import="it.eng.rspa.ideas.utils.IdeasListUtils"%>
<%@page import="it.eng.rspa.ideas.utils.ChallengesUtils"%>
<%@page import="it.eng.rspa.ideas.utils.MyUtils"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.CLSCategoriesSetForChallengeLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.PortalClassLoaderUtil"%>
<%@page import="com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.dao.orm.DynamicQuery"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSCategoriesSetForChallengeUtil"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.CLSCategoriesSetForChallengeServiceUtil"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge"%>
<%@page import="it.eng.rspa.ideas.utils.URLEncoderDecoder"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengePoiLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@page import="com.liferay.portlet.documentlibrary.model.DLFileVersion"%>
<%@page import="com.liferay.portlet.documentlibrary.model.DLFileShortcut"%>
<%@page import="com.liferay.portlet.documentlibrary.util.DLUtil"%>
<%@page import="it.eng.rspa.ideas.controlpanel.CategoryKeys"%>
<%@page import="it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet"%>
<%@page import="com.liferay.portal.util.PortletKeys"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="javax.portlet.PortletRequest"%>
<%@page import="com.liferay.portlet.PortletURLFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayPortletURL"%>
<%@page import="com.liferay.portal.kernel.util.StringPool"%>
<%@page import="com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.util.OrderByComparator"%>
<%@page import="com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil"%>
<%@page import="com.liferay.portlet.documentlibrary.service.DLFolderLocalServiceUtil"%>
<%@page import="com.liferay.portlet.documentlibrary.model.DLFolder"%>
<%@page import="com.liferay.portlet.documentlibrary.model.DLFileEntry"%>
<%@page import="org.apache.commons.lang3.ArrayUtils"%>
<%@page import="it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties"%>
<%@page import="com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil"%>
<%@page import="com.liferay.portlet.asset.service.AssetVocabularyLocalServiceUtil"%>
<%@page import="com.liferay.portlet.asset.model.AssetVocabulary"%>
<%@page import="com.liferay.portlet.asset.model.AssetTag"%>
<%@page import="com.liferay.portlet.asset.service.AssetTagServiceUtil"%>
<%@page import="com.liferay.portlet.asset.model.AssetCategory"%>
<%@page import="com.liferay.portlet.asset.service.AssetCategoryServiceUtil"%>
<%@page import="com.liferay.portal.service.RoleLocalServiceUtil" %>
<%@page import="com.liferay.portal.service.UserLocalServiceUtil" %>
<%@page import="com.liferay.portal.model.Role"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.search.Field"%>
<%@page import="com.liferay.portal.kernel.util.GetterUtil"%>
<%@page import="com.liferay.portal.kernel.util.StringPool"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@page import="com.liferay.portal.kernel.search.Document"%>
<%@page import="com.liferay.portal.kernel.search.Hits"%>
<%@page import="com.liferay.portal.kernel.search.SearchContextFactory"%>
<%@page import="com.liferay.portal.kernel.search.SearchContext"%>
<%@page import="com.liferay.portal.kernel.search.IndexerRegistryUtil"%>
<%@page import="com.liferay.portal.kernel.search.Indexer"%>
<%@page import="com.liferay.portal.kernel.dao.search.SearchContainer"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="it.eng.rspa.ideas.utils.ChallengeComparator" %>
<%@ include file="/html/challenges/init.jsp" %>

<% 
ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
User challengeUser = PortalUtil.getUser(request);

boolean isEnte=MyUtils.isAuthority(challengeUser);

if(!isEnte ){
 %>
			<script>
				location.href="/";
			</script>
<%return;
}

Locale locale = (Locale)request.getSession().getAttribute("org.apache.struts.action.LOCALE");
%>
				
				
<portlet:resourceURL var="resourceURL" />
<%
PortletURL deleteFileURL = renderResponse.createActionURL();
deleteFileURL.setParameter(ActionRequest.ACTION_NAME, "deleteChallengeFile");

String urlHomeIms = IdeaManagementSystemProperties.getUrlHomeIMS(); 
String uuid = UUID.randomUUID().toString();
User currentUser = PortalUtil.getUser(request);

Long groupId = themeDisplay.getCompanyGroupId();

pageContext.setAttribute("themeDisplay", themeDisplay);

boolean pilotingEnabled = IdeaManagementSystemProperties.getEnabledProperty("pilotingEnabled");
boolean needEnabled = IdeaManagementSystemProperties.getEnabledProperty("needEnabled");
boolean virtuosityPointsEnabled = IdeaManagementSystemProperties.getEnabledProperty("virtuosityPointsEnabled");

long id = -1;
String challengeId = "-1";
String challengeTitle = "";
String challengeDescription = "";
boolean hasReward = true;
String challengeReward = "";
int numOfSelectableIdeas = 0;


String linguaMod="";
long needReport=0;
String challengeHashTag="";

//Gestione immagine rappresentativa
String challengeRepresentativeImgUrl = "";
//Gestione immagine rappresentativa

//gestione data apertura e chiususa della gara - inizio
Calendar dateEnd = CalendarFactoryUtil.getCalendar();

dateEnd.setTimeInMillis(dateEnd.getTimeInMillis()+ 3*new Long(900000000)); // circa un mese dopo
Calendar dateStart = CalendarFactoryUtil.getCalendar( themeDisplay.getTimeZone(), themeDisplay.getLocale());
dateStart.set(Calendar.HOUR, 0);	//imposto l'ora per evitare problemi con getActiveChallenges che potrebbe far disorientare l'utente
dateStart.set(Calendar.MINUTE, 1);// cosi' l'orario di apertura (in partenza) e' 00:01 oppure 12:01 di oggi (cmq si puo' sempre cambiare a mano)

//gestione data apertura e chiususa della gara - fine


String pilota = IdeasListUtils.getPilot(renderRequest);
String [] coordinateMap = 	MyUtils.getMapLatLongCenterbyPilot(pilota);

//String categoryIds = "";
String tags = "";
Iterator<DLFileEntry> iter =null;
Map<String, String[]> paramMap = request.getParameterMap();
String[] paramChallengeId = paramMap.get("challengeId");

String pilot = IdeasListUtils.getPilot(renderRequest);

//Get available needs
List <CLSIdea> needsI = 	CLSIdeaLocalServiceUtil.getNeeds();
List<CLSIdea> filteredNeeds =IdeasListUtils.getIdeasListFilterByPilot(needsI, pilot);
List<VirtuosityPoints> virtuosityPoints = new ArrayList<VirtuosityPoints>();

needReport=ParamUtil.getLong(renderRequest, "needReport");
long vocabularyFromNeed=ParamUtil.getLong(renderRequest, "vocabularyFromNeed"); //se apro una gara a partire da un need

boolean isModifica = false;

if(paramChallengeId!=null){
	if(paramChallengeId.length>0){
		String challengeIdText = paramMap.get("challengeId")[0];
		if(challengeIdText!=null){
			try{
				id = Long.parseLong(challengeIdText);
				//recupero i dati del challenge
				if(id>0){
					
					isModifica = true;
					
					CLSChallenge challenge = CLSChallengeLocalServiceUtil.getCLSChallenge(id);
					challengeId = Long.toString(id);
					challengeDescription = challenge.getChallengeDescription();
					challengeTitle = challenge.getChallengeTitle();
					
					hasReward = challenge.getChallengeWithReward();
					
					challengeReward=challenge.getChallengeReward();
					numOfSelectableIdeas = challenge.getNumberOfSelectableIdeas();
					//gestione data apertura e chiususa della gara - inizio
					dateStart.setTime(challenge.getDateStart());
					dateEnd.setTime(challenge.getDateEnd());
					//gestione data apertura e chiususa della gara - fine
					
					linguaMod=challenge.getLanguage();
					
					DLFolder  folder = DLFolderLocalServiceUtil.getDLFolder(challenge.getIdFolder());
					Long folderId = challenge.getIdFolder();
					Integer count = DLFileEntryLocalServiceUtil.getFileEntriesCount(groupId, folderId);
					OrderByComparator obc = OrderByComparatorFactoryUtil.create("DLFileEntry", "title", true);
					List<DLFileEntry> files = DLFileEntryLocalServiceUtil.getFileEntries(
							groupId.longValue(), folderId.longValue(), 0, count,obc
					);
					iter = files.iterator();
					
					// Gestione immagine rappresentativa
					if(challenge.getRepresentativeImgUrl() != null){
						challengeRepresentativeImgUrl = challenge.getRepresentativeImgUrl();
					}
					// Gestione immagine rappresentativa
					
					virtuosityPoints = VirtuosityPointsLocalServiceUtil.getVirtuosityPointByChallengeId(id);
					
					
				}	
			}catch(Exception e){
				
			}
		}
	}
}
%>


<link href="<%=request.getContextPath()%>/css/upload/uploadfile.css" rel="stylesheet">
<script src="<%=request.getContextPath()%>/js/jquery-1.9.1.min.js"></script>
<script src="<%=request.getContextPath()%>/js/upload/jquery.uploadfile.js"></script>
<script src="<%=request.getContextPath()%>/js/customckeditor/ckeditor.js"></script>
<link href="<%=request.getContextPath()%>/css/creation.css" rel="stylesheet"/>
<%
PortletURL updateChallengesURL = renderResponse.createActionURL();
updateChallengesURL.setParameter(ActionRequest.ACTION_NAME, "updateChallenges");
%>

<portlet:actionURL var="editCaseURL" name="uploadCase">
    <portlet:param name="jspPage" value="/edit.jsp" />
</portlet:actionURL>


<%
String portalURL = URLEncoderDecoder.encodeURIComponent(themeDisplay.getPortalURL());
String pathContext = themeDisplay.getPathContext();
if(pathContext.trim().equals("")){
	pathContext = " ";
}
pathContext = URLEncoderDecoder.encodeURIComponent(pathContext);
%>


<script>

var editor;
var editor2;
var markers = {};

function markerHash(marker){
	  var position = marker.getPosition();
	  return position.lat() + "_" + position.lng();
};

function changeEnter() {
	// If we already have an editor, let's destroy it first.
	if ( editor )
		editor.destroy( true );

	if ( editor2 )
		editor2.destroy( true );
	var token = Liferay.authToken;
	var listUrl = "<%=themeDisplay.getPortalURL()%>/api/jsonws/Challenge62-portlet.clschallenge/get-challenge-images/challenge-id/<%=challengeId%>?p_auth=";
	listUrl = listUrl+token;
	
	// Create the editor again, with the appropriate settings.
	editor = CKEDITOR.replace( 'challengeDesc');
	editor2=CKEDITOR.replace( 'challengeRew');
	
	CKEDITOR.config.allowedContent = true; /*questa chiamata permette di caricare i contenuti di youtube a runtime */
}

window.onload = changeEnter;

function submitFormData() {
	var editorText = CKEDITOR.instances.challengeDesc.getData();
	var editorText2 = CKEDITOR.instances.challengeRew.getData();
	document.getElementById('<portlet:namespace/>challengeDescription').setAttribute('value', editorText);
	document.getElementById('<portlet:namespace/>challengeReward').setAttribute('value', editorText2);
	$("#<portlet:namespace />fm").submit();
}

function deletePOI(markerHashIdentifier){
	var marker = markers[markerHashIdentifier];
	marker.setMap(null);
	markers[markerHashIdentifier] = null;
	
	var poisList = document.getElementById("POIsList");
	poisList.removeChild(document.getElementById("<portlet:namespace/>newPoiInputTitle_#"+markerHashIdentifier));
	poisList.removeChild(document.getElementById("<portlet:namespace/>newPoiInputDescription_#"+markerHashIdentifier));
	poisList.removeChild(document.getElementById("<portlet:namespace/>newPoiInputLatidute_#"+markerHashIdentifier));
	poisList.removeChild(document.getElementById("<portlet:namespace/>newPoiInputLongitude_#"+markerHashIdentifier));
}

$(document).ready(function()
{
	
	var uploadObj =$("#advancedUpload").uploadFile({
		formData: { token: '<%=uuid%>'},
		url:"<%= editCaseURL %>",
		autoSubmit:false,
		fileName:"myfile",
		showProgress:true,
		dragDropStr : '<em><liferay-ui:message key="ims.drag-e-drop-files"/></em>',
		uploadStr:'<liferay-ui:message key="ims.add-document"/>',
        cancelStr: '<liferay-ui:message key="cancel"/>',
        deletelStr: '<liferay-ui:message key="delete"/>',
        extErrorStr: '<liferay-ui:message key="ims.is-not-allowed-allowed-extensions"/>',
        duplicateErrorStr: '<liferay-ui:message key="ims.is-not-allowed-allowed-file-already-exists"/>',
        sizeErrorStr: '<liferay-ui:message key="ims.is-not-allowed-allowed-max-size"/> ',
        dragdropWidth: 'auto',
        statusBarWidth: 'auto',
        allowedTypes: "<%=PortletProps.get("upload-allowed-extension") %>",
        maxFileSize:<%=PortletProps.get("upload-max-bytes-size") %>, /*20MB*/
		showStatusAfterSuccess:false,
		afterUploadAll:function(obj){
			submitFormData();
		}
		});
	
	
	$("#submitForm").click(function(){
				
		//chiamata Ajax per vedere se i titoli gia' esistono
		 AUI().use('aui-io-request', function(A){
		    	
		    	var titleGara = document.getElementById('<portlet:namespace/>challengeTitle').value; 
		    	var hasReward = document.getElementById('<portlet:namespace/>challengeWithReward').value;
		    	var numSelectableIdeas = document.getElementById('<portlet:namespace/>numberOfSelectableIdeas').value;
		    
		 
		        A.io.request('<%=resourceURL.toString()%>', {
		               method: 'post',
		               data: {
		            	   <portlet:namespace />param: 'titoloGara',
		            	   <portlet:namespace />titleChoosen: titleGara,   
		            	   <portlet:namespace />modifica: '<%=challengeId%>'   
		               },
		               dataType: 'json',
		               on: {
		                   success: function() {
		                    
		                    ajRitornoTitleExist = this.get('responseData').titleExist;
		                    
		                		if( (titleGara.trim() == "") ){
		                			document.getElementById('<portlet:namespace/>challengeTitle').focus();
		                			alert('<liferay-ui:message key="ims.insert-title"/>');		                			
		                		}else if(ajRitornoTitleExist == 'yes'){
		                			document.getElementById('<portlet:namespace/>challengeTitle').focus();
		                			alert('<liferay-ui:message key="ims.existing-title"/>');
		                		}else if(CKEDITOR.instances.challengeDesc.getData().trim() == ""){
		                			document.getElementById('descriptionDiv').scrollIntoView();
		                			alert('<liferay-ui:message key="ims.insert-description"/>');	     
		                			
		                		}else if( (hasReward == "true") && CKEDITOR.instances.challengeRew.getData().trim() == ""){	
		                			document.getElementById('reward-panel').scrollIntoView();
		                			alert('<liferay-ui:message key="ims.alert-reward-description"/>');	     
		                			
		                		}else if( (hasReward == "true") &&  !( numSelectableIdeas > 0 )){	
		                			 document.getElementById('<portlet:namespace/>numberOfSelectableIdeas').focus();
		                			alert('<liferay-ui:message key="ims.alert-number-of-selectable-ideas"/>');	     
		                			
		                		}else if (!$("input[name='<portlet:namespace/>radio_vocabulary']").is(':checked')) {//if there aren't selected categories		
		                			document.getElementById('cat-tag-panel').scrollIntoView();
		                			alert('<liferay-ui:message key="please-select-a-category"/>');
		                		}else{
		                			if(uploadObj.getFileCount()>0){			 
		                				uploadObj.startUpload();
		                			 }else{
		                				 submitFormData();
		                			 }
		                		}// else if 		                	                   	                   	                   	                	   
		                   }//success
		              }//on		               
		        });//A.io.request	 
		    });// AUI().use
	});//submitForm
	
	$("#cancelAddPOI").click(function(){
		
		/* clean the fields */
		document.getElementById('<portlet:namespace/>POITitle').value = "";
		document.getElementById('<portlet:namespace/>POIDescription').value = "";
		document.getElementById('<portlet:namespace/>POILatitude').setAttribute('value', "");
		document.getElementById('<portlet:namespace/>POILongitude').setAttribute('value', "");
		
		/* hide the popup */
		document.getElementById('map_canvas').style.display='block';
		document.getElementById('mainFormButtons').style.display='block';
     	document.getElementById('overlay').style.display='none';
	});
	
	$("#addPOI").click(function(){
		
		var poiTitle       = document.getElementById('<portlet:namespace/>POITitle').value; //getAttribute('value');
		var poiDescription = document.getElementById('<portlet:namespace/>POIDescription').value; //getAttribute('value');
		
		poiTitle = poiTitle.split('\"').join('\'');
		poiDescription = poiDescription.split('\"').join('\'');
		
		var poiLatitude    = document.getElementById('<portlet:namespace/>POILatitude').getAttribute('value');
		var poiLongitude   = document.getElementById('<portlet:namespace/>POILongitude').getAttribute('value');
  	
		if((poiTitle.trim() == "") || (poiDescription.trim() == "")){
			alert('<liferay-ui:message key="ims.insert-poi-title-description"></liferay-ui:message>');
		}else{
			// poiLatitude+"_"+poiLongitude � uguale al markerHashIdentifier utilizzato per la cancellazione in function deletePOI(markerHashIdentifier)
			
		  	var newPoiInputTitle = document.createElement("input");
	  		newPoiInputTitle.setAttribute("type", "hidden");
	  		newPoiInputTitle.setAttribute("value", poiTitle);
	  		newPoiInputTitle.setAttribute("name", "<portlet:namespace/>newPoiInputTitle_#"+poiLatitude+"_"+poiLongitude);
	  		newPoiInputTitle.setAttribute("id", "<portlet:namespace/>newPoiInputTitle_#"+poiLatitude+"_"+poiLongitude);
	  		newPoiInputTitle.setAttribute("class", "field");
		  		
		  	var newPoiInputDescription = document.createElement("input");
		  	newPoiInputDescription.setAttribute("type", "hidden");
		  	newPoiInputDescription.setAttribute("value", poiDescription);
		  	newPoiInputDescription.setAttribute("name", "<portlet:namespace/>newPoiInputDescription_#"+poiLatitude+"_"+poiLongitude);
		  	newPoiInputDescription.setAttribute("id", "<portlet:namespace/>newPoiInputDescription_#"+poiLatitude+"_"+poiLongitude);
		  	newPoiInputDescription.setAttribute("class", "field");
				
		  	var newPoiInputLatidute = document.createElement("input");
		  	newPoiInputLatidute.setAttribute("type", "hidden");
		  	newPoiInputLatidute.setAttribute("value", poiLatitude);
		  	newPoiInputLatidute.setAttribute("name", "<portlet:namespace/>newPoiInputLatidute_#"+poiLatitude+"_"+poiLongitude);
		  	newPoiInputLatidute.setAttribute("id", "<portlet:namespace/>newPoiInputLatidute_#"+poiLatitude+"_"+poiLongitude);
		  	newPoiInputLatidute.setAttribute("class", "field");
				
		  	var newPoiInputLongitude = document.createElement("input");
		  	newPoiInputLongitude.setAttribute("type", "hidden");
		  	newPoiInputLongitude.setAttribute("value", poiLongitude);
		  	newPoiInputLongitude.setAttribute("name", "<portlet:namespace/>newPoiInputLongitude_#"+poiLatitude+"_"+poiLongitude);
		  	newPoiInputLongitude.setAttribute("id", "<portlet:namespace/>newPoiInputLongitude_#"+poiLatitude+"_"+poiLongitude);
		  	newPoiInputLongitude.setAttribute("class", "field");
			
			var poisList = document.getElementById("POIsList");
			poisList.appendChild(newPoiInputTitle);
			poisList.appendChild(newPoiInputDescription);
			poisList.appendChild(newPoiInputLatidute);
			poisList.appendChild(newPoiInputLongitude);
			
			document.getElementById('<portlet:namespace/>POITitle').value = "";
			document.getElementById('<portlet:namespace/>POIDescription').value = "";
			document.getElementById('<portlet:namespace/>POILatitude').setAttribute('value', "");
			document.getElementById('<portlet:namespace/>POILongitude').setAttribute('value', "");
			
			
			var latlng = new google.maps.LatLng(poiLatitude, poiLongitude);
			
	    	var marker = new google.maps.Marker({
				position: latlng,
				map: map, 
	            title: poiTitle,
	            html: ""
	        });

			var markerHashIdentifier = markerHash(marker);

			var infoWindowContent = '<label><liferay-ui:message key="ims.title"></liferay-ui:message></label><br/>'+
									poiTitle+
									'<br/><label><liferay-ui:message key="ims.description"></liferay-ui:message></label><br/>'+
									poiDescription+
									'<br/><button type=\"button\" onclick=\"deletePOI(\''+markerHashIdentifier+'\');\" class=\"btn\"><liferay-ui:message key="ims.delete"></liferay-ui:message></button>';
									
			marker.html = infoWindowContent;
			
			markers[markerHashIdentifier] = marker;

			google.maps.event.addListener(marker, 'click', function() {
				infowindow.setContent(this.html);
				infowindow.open(map,this);
			});
			
			document.getElementById('map_canvas').style.display='block';
			document.getElementById('mainFormButtons').style.display='block';
	     	document.getElementById('overlay').style.display='none';
		}
	});
});

function deleteFile(fileId) {
	if (confirm('<liferay-ui:message key="ims.delete-document-message"></liferay-ui:message>')) { 
		var url = "<%=deleteFileURL.toString()%>";
		
		Liferay.Service(
				  '/Challenge62-portlet.clschallenge/delete-challenge-file',
				  {
				    fileEntryId: fileId
				  },
				  function(obj) {
				    $( "#div_"+fileId).remove();
				  }
				);
	}
}
</script>

<portlet:renderURL var="viewChallengesURL">
	<portlet:param name="jspPage" value="/html/challenges/view.jsp"/>
</portlet:renderURL>

<%
	PortletURL actionURL = renderResponse.createActionURL();
	actionURL.setParameter("myActions", "uploadMultipleFile");
%>

<%-- la parte di recupero del challenge era qui --%>

<!-- Gestione immagine rappresentativa script -->
<script charset="utf-8" type="text/javascript">
function <portlet:namespace/>selectRepresentativeImg(imgURL){
	document.getElementById('<portlet:namespace/>representativeImgUrl').setAttribute('value', imgURL);
	var pic1 = document.getElementById("<portlet:namespace/>article-image");
	pic1.src = imgURL;
}
</script>
<!-- Gestione immagine rappresentativa script -->

<form  method="POST" id="fm2" name="fm2" enctype="multipart/form-data">
	<input type="file" name="myFile" id="myFile" onchange="imgsubmit()" hidden></input>
	<button id="btnsubmit" value="Save" hidden ></button>
</form>

<script>

function imgsubmit() {
	 $("#btnsubmit").trigger("click");
}

function flb() {
	 $("#myFile").trigger("click");
}


</script>

<!-- Gestione immagine rappresentativa tramite Ajax -->
<aui:script use="aui-io-request,aui-node" >

YUI().use(
  'aui-node',
  'aui-io-request',
  function(Y) {
  
  	var node = Y.one('#btnsubmit');
	var img=Y.one('#<portlet:namespace/>representativeImgUrl');
    
    node.on('click', function(){
      var A = AUI();
		A.io.request('<%=resourceURL.toString()%>',{method: 'POST', form: { id: 'fm2', upload: true }, data: {<portlet:namespace/>param:'caricamento',}, 
  		on: {complete: function(){display();}}});
      });
    
    
    function display(){
       Y.io.request('<%=resourceURL.toString()%>',{
		  data: {
			  //qua mandiamo il parametro vista
			  
			  <portlet:namespace/>param:'view',
				
			},
	              dataType: 'json',
	              on: {
	                success: function() {
	                	
	                	var data = this.get('responseData');
	                	
	                	img.set("value", data.value);
  						var pic1 = document.getElementById("<portlet:namespace/>article-image");
  						pic1.src =data.value;
	                	
	                }
	              }
	            }
	          );
    }
  }
);



</aui:script >

<div class="materialize">

	<div class="row">
		<div class="col s12">
			<a class="waves-effect btn-flat tooltipped" href="<%=urlHomeIms %>" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="ims.go-back"/>">
				<i class="icon-arrow-left left">&nbsp;</i><liferay-ui:message key="back"/>
			</a>
		</div>
	</div>

	<div class="css-class-wrapper-update-fake">
	
		<aui:form name="fm" method="POST" action="<%= updateChallengesURL.toString() %>" id="formChallenge"> <!-- enctype="multipart/form-data" > -->
		
			<aui:model-context  model="<%=CLSChallenge.class%>"></aui:model-context>
			<aui:input name="challengeId" type="hidden" value="<%=challengeId %>"></aui:input>
			<aui:input name="groupid" type="hidden" value="<%=groupId.longValue()%>"/> <!-- questo campo deve essere nascosto -->
			<aui:input name="token" type="hidden" value="<%=uuid%>"/><!-- questo campo deve essere nascosto -->
			<aui:input name="needSelection" id="needSelection" type="hidden" value=""/> 			
			<%
			String acronimo = MyUtils.getLanguageAcronimByAuthoridyIdPilot(currentUser.getUserId());
			String linguaScelta = MyUtils.getFullLanguageNameByAcronym(acronimo);
			String linguaSceltaCase = linguaScelta.substring(0, 1).toUpperCase() + linguaScelta.substring(1);
			
			String panelTitle;
			if(challengeId.equals("-1")){ panelTitle = "ims.create.challenge";}
			else{panelTitle="ims.edit.challenge";}%>

		<div id="introduction" class="">

	    			<div class="row">
   						<div class="col s12 flow-text weight-4 center"><b><liferay-ui:message key="<%=panelTitle %>"/></b></div>
   						<br>
   						<div class="bottom-margin"></div>
   		

		
		<blockquote>
			*&nbsp;<liferay-ui:message key="ims.starred-are-mandatory"/>
		</blockquote>
		<br>
		
		<c:if test="<%= (needEnabled && filteredNeeds.size() >0)  %>">
		
			<!-- Needs di riferimento -->
			<div class="" id="linkNeeds-panel">
				<h5 class="header"><liferay-ui:message key="ims.linked-needs"/>&nbsp;<liferay-ui:icon-help message="ims.tip-linked-needs"/></h5> 
				<div class="divider bottom-margin "></div>
				<%@ include file="needsCarousel.jspf" %>
			</div>
		</c:if>
		
		<!-- Info Challlenge -->
		<div class="" id="linkNeeds-panel"> 
			<h5 class="header"><liferay-ui:message key="ims.challenge-info"/></h5> 
			<div class="divider bottom-margin "></div>
				
				<c:if test="<%= pilotingEnabled  %>">	
				
					<div id="linguaUtenteSenzaGara"	>
						<%	
							boolean englishSelected = false;
							boolean altraLinguaSelected = false;
							
							
							if (!isModifica){
								altraLinguaSelected=true;
							}else{
								
								if (linguaMod.equalsIgnoreCase("en"))
									englishSelected=true;
								else
									altraLinguaSelected=true;
							}
						%>
							<div class="input-field col s12">
								<div class="select-wrapper">
									<aui:select  name="languageChosen" id="languageChosen" label="" disabled="<%=isModifica %>">
										 <option value="" disabled selected><liferay-ui:message key="ims.choose-your-option"/></option> 
										 <aui:option  value="en"  selected="<%=englishSelected %>"   ><liferay-ui:message key="english"/></aui:option>
										 <aui:option  value="<%=acronimo %>"  selected="<%=altraLinguaSelected %>" ><%=linguaSceltaCase %></aui:option>	
									</aui:select>
								</div>
								<label><liferay-ui:message key="choose-a-language"/>&nbsp;*&nbsp;<liferay-ui:icon-help message="ims.tip-idea-language"/></label>
				           </div>
					</div>
					
				</c:if>	
					
			
			<!-- Immagine -->
					<div class="section row" id="general-section">
						<div class="col s12 m3 l3" id="imgDiv">
							<aui:input type="hidden" name="representativeImgUrl" id="representativeImgUrl" value="<%=challengeRepresentativeImgUrl %>"/>
					        <div class="profile-image">
					        	<% if(challengeRepresentativeImgUrl.equals("")){ %>
					        		<img class="journal-image" hspace="0" id="<portlet:namespace/>article-image" src="<%=request.getContextPath()%>/img/challengesImg.png" vspace="0" onclick="flb()" />
					        	<% }else{ %>
					        		<img class="journal-image" hspace="0" id="<portlet:namespace/>article-image" src="<%=challengeRepresentativeImgUrl %>" vspace="0" onclick="flb()" />
					        	<% } %>
					        </div>
					        <span onclick="flb();"><i class="infoImg"><liferay-ui:message key="ims.load.img"/></i></span>
						</div>
						
						<div class="col s12 m9 l9" id="generalInfoDiv">
							
							<div class="" id="titleDiv">
								<!--Se la gara � gi� stata creata non permetto la modifica del titolo in modo da preservare l'associazione con la directory nel document repository  -->
								<%if(challengeId.equals("-1")){
									String language = locale.getLanguage();
								%>
								<!-- Title -->	
								<div class="input-field">
									<input name="<portlet:namespace/>challengeTitle"
											type="text"
											class="general-info"
											maxlength="60"
											id="<portlet:namespace/>challengeTitle"
											value="<%=challengeTitle %>" />
									 <label for="<portlet:namespace/>challengeTitle"> <liferay-ui:message key="ims.title.placeholder.challange"/>&nbsp;*</label>
								</div>	
								<!-- fine Title -->	
								<!-- Hashtag -->
								<% if (IdeaManagementSystemProperties.getEnabledProperty("tweetingEnabled")){ %>	
								<div class="input-field">	
									<input name="<portlet:namespace/>challengeHashTag"
											type="text"
											id="<portlet:namespace/>challengeHashTag"
											value="<%=challengeHashTag %>"/>
									<label for="<portlet:namespace/>challengeHashTag"> <liferay-ui:message key="ims.hashtag"/></label>		
								</div>				
								<%} %>			
								<!-- fine Hashtag -->		
												
								<%}else{%>
									<h4><%=challengeTitle %></h4>
									<aui:input name="challengeTitle" class="general-info" type="hidden" value="<%=challengeTitle %>"/>
								<%}%>
							</div>
							
							<!-- Apertura e chiusura della gara-->
							<div class="row openCloseDate">
								<div class="col s12 m6">
									<aui:input inlineField="true" label="ims.challenge-opening-date" name="dateStart" model="<%=CalEvent.class%>" field="startDate" value="<%=dateStart%>" />
								</div>
								<div class="col s12 m6">
									<aui:input inlineField="true" label="ims.challenge-closing-date" name="dateEnd"   model="<%=CalEvent.class%>" field="endDate"   value="<%=dateEnd%>" />
					        	</div>
					       
								<!-- Description -->
								<div class="assetInfo inline left col s12" id="descriptionDiv">
									<label for="challengeDesc" style="font-size: 1rem;"><liferay-ui:message key="ims.description"/>*</label>
									<aui:input name="challengeDescription" id="challengeDescription" type="hidden" value=""/>
									<!-- questa text area � il ckeditor customizzato per youtube -->
									<textarea cols="80" id="challengeDesc" name="challengeDesc" rows="30">
										<%=challengeDescription.replaceAll("[\\r\\n]", "") %>
									</textarea> 
									
									<script type="text/javascript">
							            function <portlet:namespace />initEditorChallengeDescription() {
							                        return '<%=challengeDescription.replaceAll("[\\r\\n]", "") %>';
							            }
									</script>
								</div>
						 	</div>
	
						</div>
						<div class="block next-button-wrapper">
								<button class="right next-button btn-flat" id="next-general" data-nextid="reward-panel">
								  <span class="icon-arrow-down"></span>
								</button>
						</div>
					</div>
					

				
		</div><!-- fine Info Challlenge -->
		
					<!-- Reward -->
					<div class="" id="reward-panel">
						<h5 class="header"><liferay-ui:message key="ims.reward"/>&nbsp;<liferay-ui:icon-help message="ims.tip-challenge-reward-insert"/></h5> 
						<div class="divider bottom-margin "></div>
						
						<div class="row">
  							<div class="col s12 center">
						      <div class="switch top-margin">
						      	<aui:input name="challengeWithReward" id="challengeWithReward" type="hidden" value="true"/>
							    <label for="weHaveReward">
							      <liferay-ui:message key="ims.off"/>
							      <input type="checkbox" id="weHaveReward"   onclick="rewardChange()"   
							      <% if (hasReward) {%>
							    	  checked
							    	
							       <% }  %>
							      >
							      <span class="lever"></span>
							      <liferay-ui:message key="ims.on"/>	
							    </label>
							  </div>
						   </div>
						 </div>
						
				 		<div id="rewardDetailsDiv">
							<div class="assetInfo bottom-margin" id="rewardDiv">
								<label for="challengeReward" style="font-size: 1rem;"><liferay-ui:message key="ims.description"/></label>						
								<aui:input name="challengeReward" id="challengeReward" type="hidden" value=""/>
								<!-- questa text area � il ckeditor customizzato per youtube -->
								<textarea cols="80" id="challengeRew" name="challengeRew" rows="30" placeholder="Placeholder">
									<%=challengeReward.replaceAll("[\\r\\n]", "") %>
								</textarea> 
								<script type="text/javascript">
						            function <portlet:namespace />initEditorChallengeReward() {
						                 return '<%=challengeReward.replaceAll("[\\r\\n]", "") %>';
						            }
								</script>
							</div>
						</div>
													
						<div class="row" id="numberIdeaDiv">
							<label><liferay-ui:message key="ims.number-of-selectable-ideas"/>&nbsp;<liferay-ui:icon-help message="ims.tip-number-of-selectable-ideas"/></label> 
							<div class="col s2">
								<aui:input name="numberOfSelectableIdeas" label="" id="numberOfSelectableIdeas" type="number" min="1" value="<%=String.valueOf(numOfSelectableIdeas)%>"  />
							</div>
						</div>
					
					<% if (virtuosityPointsEnabled){%>
						
						<div  id="virtuosityPointsNumber">
							<%  for (int i = 0; i< virtuosityPoints.size(); i++){
							int posInd = i+1;
							
							%>							
									<div class="row" id="virtuosityPointsDiv<%=i%>" >
										<label><liferay-ui:message key="ims.number-of-virtuosity-points-for-the-position"/>: <%=posInd %> &nbsp;<liferay-ui:icon-help message="ims.tip-virtuosity-point"/></label> 
											<div class="col s2">
												<input type="number"  name='<portlet:namespace/>virtuosityPoint<%=i%>' id='<portlet:namespace/>virtuosityPoint<%=i%>' min="1" value="<%=virtuosityPoints.get(i).getPoints()  %>"  />
											</div>
									</div>
							<%  }%>		
						</div>
						<input type ='hidden' id="oldVirtuosityPoints" value="<%=virtuosityPoints.size() %>" />
						
					<% }%>
						
						
						<div class="block next-button-wrapper">
							<button class="right next-button btn-flat" id="next-general" data-nextid="cat-tag-panel">
							  <span class="icon-arrow-down"></span>
							</button>
						</div>
					</div>
					<!-- fine Reward -->
					
					
			</div>
	</div>	<!-- fine Introdution -->
		
						
			<div class="section" id="cat-tag-panel">
				<h5 class="header"><liferay-ui:message key="ims.categories-and-keywords"/></h5> 
				<div class="divider bottom-margin "></div>	
				<label class="lblSection"><span class="icon-folder-close"></span><liferay-ui:message key="categories"/>* <liferay-ui:icon-help message="ims.tip-challenge-categories-insert"/></label>
							
						<div class="categoryGroup row">
							<%
		
								HashMap<Long,Integer> clsCategoriesSetsForChallengeMap = CLSCategoriesSetForChallengeLocalServiceUtil.getMapCategoriesSetForChallenge(Long.valueOf(challengeId));
															
								//recupero i set di categorie selezionabili
								IdeaManagementSystemProperties imsp = new IdeaManagementSystemProperties();
								List<CLSCategoriesSet> clsCategoriesSets = imsp.getCategoriesSetIMS();
								Iterator<CLSCategoriesSet> clsCategoriesSetsIt = clsCategoriesSets.iterator();
								
								int num=0;
								while(clsCategoriesSetsIt.hasNext()){
									CLSCategoriesSet clsCategoriesSet = (CLSCategoriesSet)clsCategoriesSetsIt.next();
										
										String inputSingleName = "choiceTypeForVocabulary_" + clsCategoriesSet.getCategoriesSetID();
										String inputMultiName = "choiceTypeForVocabulary_" + clsCategoriesSet.getCategoriesSetID();
										
										String checkboxVocabularyName = "radio_vocabulary";	
										
										String value = Long.toString(clsCategoriesSet.getCategoriesSetID());
										boolean inputSingleChecked = false;
										boolean inputMultiChecked = true;
										String checcato = "";
										if(clsCategoriesSetsForChallengeMap.containsKey(clsCategoriesSet.getCategoriesSetID()) 
												
											|| 
										vocabularyFromNeed == clsCategoriesSet.getCategoriesSetID()
												
												){
											checcato = "checked";
											
											value = Long.toString(clsCategoriesSet.getCategoriesSetID());
										//	if(clsCategoriesSetsForChallengeMap.get(clsCategoriesSet.getCategoriesSetID()) == CategoryKeys.SINGLE_CHOICE){
												inputSingleChecked = false;
												inputMultiChecked = true;
										//	} 
										}
										AssetVocabulary vocabulary = AssetVocabularyLocalServiceUtil.fetchAssetVocabulary(clsCategoriesSet.getCategoriesSetID());
										
										String catsVoc = ChallengesUtils.getCategoriesByVocabulary(vocabulary, locale);
										
							%>
							<p class="categoryRow col s6 m4 l3">
								<input id="cat_<%=num%>" class="with-gap category-item" type="radio" name="<portlet:namespace/><%=checkboxVocabularyName %>" value="<%=value%>"  <%=checcato%> />
								<label for="cat_<%=num%>" class="categoryVacabolary tooltipped" data-position="top" data-delay="50" data-tooltip="<liferay-ui:message key="subcategories"/>: <%= catsVoc%>"><%= vocabulary.getTitle(locale)%></label>
					  		</p>
					  			<span class="categoryChoiceType" style="display:none;">
									<aui:input inlineLabel="right" name="<%=inputSingleName%>" type="checkbox" value="<%=CategoryKeys.SINGLE_CHOICE%>" label="ims.single-choice"  checked="<%=inputSingleChecked %>"/>
									<aui:input inlineLabel="right" name="<%=inputMultiName%>"  type="checkbox" value="<%=CategoryKeys.MULTI_CHOICE%>"  label="ims.multiple-choice" checked="<%=inputMultiChecked %>" />
								</span>
							
							<% 
							num++; 
							} %>
						</div>
				
				<!-- Tags -->
				<div id="tagsSection" class="divSection">
					<label class="lblSection"><span class="icon-tags"></span><liferay-ui:message key="keywords"/><liferay-ui:icon-help message="ims.tip-challenges-tags"/></label>
					<div class="material-design-restyle btn--raised multiple-btn-space">
						<liferay-ui:asset-tags-selector className="<%=CLSChallenge.class.getName() %>" classPK="<%=id%>"/>
					</div>
				</div><!-- gestione dei tag per l'idea - fine -->
			
				<div class="block next-button-wrapper">
					<button class="right next-button btn-flat" id="next-general"  data-nextid="attach-panel">
					  <span class="icon-arrow-down"></span>
					</button>
				</div>
			</div><!-- gestione delle categorie per l'idea - fine -->
			
				
			<div class="section" id="attach-panel" >
				<h5 class="header"><liferay-ui:message key="attachments"/><liferay-ui:icon-help message="ims.tip-attachments"/></h5> 
				<div class="divider bottom-margin "></div>	
					<%if (iter!=null){
							int count=0;
							while (iter.hasNext()){
								count+=1;
								DLFileEntry fileCor = iter.next();
								String value = fileCor.getFileEntryId()+"";
								String fileCorURL = themeDisplay.getPortalURL() + 
							 			 themeDisplay.getPathContext() + 
							 			 "/documents/" + 
							 			 //themeDisplay.getScopeGroupId() + 
							 			 themeDisplay.getCompanyGroupId() +
							 			 StringPool.SLASH + 
							 			 fileCor.getUuid();
								String fileEntryId = Long.toString(fileCor.getFileEntryId());
								String javaScriptDeleteFileFunctionCall = "deleteFile("+fileEntryId+");";
								String idString = "cancellafile_"+fileCor.getFileEntryId();
							%>
									
									<div id="div_<%=fileCor.getFileEntryId() %>" class="uploadedFiles">
					   					<div>
						   					<% String icon = "../file_system/small/" + fileCor.getIcon();%>
											<liferay-ui:icon target="_blank" label="<%= true %>" message="<%=fileCor.getTitle() %>" url="<%= fileCorURL %>"/>
											<br/>
											<aui:button type="button" value="ims.delete-file" id="<%= idString %>" onClick="<%=javaScriptDeleteFileFunctionCall%>"/>
										</div>
				   					</div>
				   					
						<%	}
						}%>
				<div class="sectionAttachment">
					<div id="overlay">
						<aui:form name="newPOIForm" id="newPOIForm">
							<spam id="POIDiplayCoordinates"></spam>
							<aui:input name="POITitle" id="POITitle" label="ims.title" value="" type="text" />
							<aui:input name="POIDescription" id="POIDescription" label="ims.description" value="" type="textarea" />
							<aui:input name="POILatitude"  id="POILatitude"   value="" type="hidden" /> 
							<aui:input name="POILongitude" id="POILongitude"  value="" type="hidden" /> 
							<aui:button type="button" value="add" id="addPOI"/>
							<aui:button type="cancel" value="cancel" id="cancelAddPOI"/>
					  	</aui:form>
					</div>
					<div class="contentUpload contentFlexSpace">
						<div id="advancedUpload"><!-- <button class="waves-effect waves-light btn" style="font-size:15.4px;"><liferay-ui:message key="ims.add-document"/></button> --></div><br/>
					</div>			
					
					<div class="contentMap contentFlexSpace">
						<!-- Google maps -->
						<% 
						boolean googleMapsAPIKeyEnabled = IdeaManagementSystemProperties.getEnabledProperty("googleMapsAPIKeyEnabled");
						String googleMapsAPIKey = IdeaManagementSystemProperties.getProperty("googleMapsAPIKey");
						
						if (googleMapsAPIKeyEnabled){%>	
							<script src="https://maps.googleapis.com/maps/api/js?key=<%=googleMapsAPIKey %>&v=3.30&libraries=places"></script>
						<% }else{%>	
							<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>
						<% }%>	
					 	<script type="text/javascript">
					       var map;
					       
					       var infowindow = new google.maps.InfoWindow({
								content: ""
							});
					       
					      function initialize() {
					            var centroMappa = new google.maps.LatLng(<%=coordinateMap[0] %> , <%=coordinateMap[1] %>);
					
					            var mapOptions = {
					              zoom: 6,
					              center: centroMappa,
					              scrollwheel: true,
					              mapTypeId: google.maps.MapTypeId.ROADMAP
					            };
					               
					            map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
					            
					            var input = /** @type {HTMLInputElement} */(
					           	document.getElementById('pac-input'));
					            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
					            
					            var searchBox = new google.maps.places.SearchBox(
					            	    /** @type {HTMLInputElement} */(input));
					            
					            google.maps.event.addListener(searchBox, 'places_changed', function() {
					                var places = searchBox.getPlaces();
			
					                if (places.length == 0) {
					                  return;
					                }
					                for (var i = 0, marker; marker = markers[i]; i++) {
					                  marker.setMap(null);
					                }
			
					                // For each place, get the icon, place name, and location.
					                markers = [];
					                var bounds = new google.maps.LatLngBounds();
					                for (var i = 0, place; place = places[i]; i++) {
					                  /* var image = {
					                    url: place.icon,
					                    size: new google.maps.Size(71, 71),
					                    origin: new google.maps.Point(0, 0),
					                    anchor: new google.maps.Point(17, 34),
					                    scaledSize: new google.maps.Size(25, 25)
					                  };
			
					                  // Create a marker for each place.
					                  var marker = new google.maps.Marker({
					                    map: map,
					                    icon: image,
					                    title: place.name,
					                    position: place.geometry.location
					                  });
		
					                  markers.push(marker); */
					                  bounds.extend(place.geometry.location);
									}
				
									map.fitBounds(bounds);
					            });
					            
					            google.maps.event.addListener(map, 'bounds_changed', function() {
					                var bounds = map.getBounds();
					                searchBox.setBounds(bounds);
					            });
					            
					            google.maps.event.addListener(map, 'click', function(event) {
					            	 callPopup(event.latLng);
					            });
					            
					            var oldPois=[
					            <%
					    		List<CLSChallengePoi> poisList2 = CLSChallengePoiLocalServiceUtil.getChallengePOIByChallengeId(id);
					    		Iterator<CLSChallengePoi> poisListIt2 = poisList2.iterator();
					    		while(poisListIt2.hasNext()){
					    			CLSChallengePoi poi = poisListIt2.next();
					    			%>
					    			{title:"<%=poi.getTitle()%>",description:"<%=poi.getDescription()%>",latitude:"<%=poi.getLatitude()%>",longitude:"<%=poi.getLongitude()%>"},
					    			<%
					    		}
					    		%>
					    		]; 
					            
					            for (index = 0; index < oldPois.length; ++index) {
						                
					            	var poi = oldPois[index];
					                
					            	var latlng = new google.maps.LatLng(poi.latitude, poi.longitude);
					        		
					            	var marker = new google.maps.Marker({
					        			position: latlng,
					        			map: map, 
					                    title: poi.title,
					                    html: ""
					                });
							
									var markerHashIdentifier = markerHash(marker);
							
									var infoWindowContent = '<label><liferay-ui:message key="ims.title"></liferay-ui:message></label><br/>'+
															poi.title+
															'<br/><label><liferay-ui:message key="ims.description"></liferay-ui:message></label><br/>'+
															poi.description+
															'<br/><button type=\"button\" onclick=\"deletePOI(\''+markerHashIdentifier+'\');\" class=\"btn\"><liferay-ui:message key="ims.delete"></liferay-ui:message></button>';
															
									marker.html = infoWindowContent;
									
									markers[markerHashIdentifier] = marker;
					
									google.maps.event.addListener(marker, 'click', function() {
										infowindow.setContent(this.html);
										infowindow.open(map,this);
									});
									
									//setto il centro all'ultimo poi elaborato
									map.setCenter(latlng);
					            }
					      }
					
					      google.maps.event.addDomListener(window, 'load', initialize);
					
						function callPopup(location){
							$("#POIDiplayCoordinates").text('<liferay-ui:message key="ims.coordinate"></liferay-ui:message>: <liferay-ui:message key="ims.latitude"></liferay-ui:message> ' + location.lat() + ' <liferay-ui:message key="ims.longitude"></liferay-ui:message>' + location.lng());
							document.getElementById('<portlet:namespace/>POILatitude').setAttribute('value', location.lat());
							document.getElementById('<portlet:namespace/>POILongitude').setAttribute('value', location.lng());
							document.getElementById('map_canvas').style.display='none';
							document.getElementById('mainFormButtons').style.display='none';
							document.getElementById('overlay').style.display='block';
						}	
						</script >
					
		<%-- 			<label class="lblSection"><liferay-ui:message key="ims.points-of-interest"></liferay-ui:message></label> --%>
						<i><liferay-ui:message key="ims.click-on-map"></liferay-ui:message></i>
						<input id="pac-input" class="controls" type="text" placeholder="Search Box"/>
						<div id="map_canvas" style="height:500px"></div>
						<div id="POIsList">
							<%
							List<CLSChallengePoi> poisList = CLSChallengePoiLocalServiceUtil.getChallengePOIByChallengeId(id);
							Iterator<CLSChallengePoi> poisListIt = poisList.iterator();
							while(poisListIt.hasNext()){
								CLSChallengePoi poi = poisListIt.next();
								String poiIdentifier = poi.getLatitude() + "_" + poi.getLongitude();
								%>
								<input type="hidden" 
									   value="<%=poi.getTitle() %>" 
									   name="<portlet:namespace/>newPoiInputTitle_#<%=poiIdentifier %>" 
									   id="<portlet:namespace/>newPoiInputTitle_#<%=poiIdentifier %>" 
									   class="field">
								<input type="hidden" 
									   value="<%=poi.getDescription() %>" 
									   name="<portlet:namespace/>newPoiInputDescription_#<%=poiIdentifier %>" 
									   id="<portlet:namespace/>newPoiInputDescription_#<%=poiIdentifier %>" 
									   class="field">
								<input type="hidden" 
									   value="<%=poi.getLatitude() %>" 
									   name="<portlet:namespace/>newPoiInputLatidute_#<%=poiIdentifier %>" 
									   id="<portlet:namespace/>newPoiInputLatidute_#<%=poiIdentifier %>" 
									   class="field">
								<input type="hidden" 
									   value="<%=poi.getLongitude() %>" 
									   name="<portlet:namespace/>newPoiInputLongitude_#<%=poiIdentifier %>" 
									   id="<portlet:namespace/>newPoiInputLongitude_#<%=poiIdentifier %>" 
									   class="field">
								<% } %>
						</div>
					</div>
				</div>
			</div>
			
			<!-- Pulsanti Salva e Cancella della pagina -->
			<div id="mainFormButtons" class="" >
				<div class="section">
					<button class="waves-effect waves-light btn core-color color-1"  id="submitForm"><liferay-ui:message key='publish'/></button>
					<button class="waves-effect waves-light btn core-color color-1"  onClick="history.go(-1);"><liferay-ui:message key='cancel'/></button>			
				</div>
			</div>
			
		</aui:form>
	</div>

</div>

<script>
function scrollPage(elementId){
	var position = $('#'+elementId).offset().top;
	$("html, body").animate({scrollTop:(position-45)}, 'slow');
	return;
}



var namespaceP = '<portlet:namespace/>';
var tipVirtuosityPoint ='<liferay-ui:icon-help message="ims.tip-virtuosity-point"/>';
var virtuosityPointsLabel = '<liferay-ui:message key="ims.number-of-virtuosity-points-for-the-position"/>'; 

$(document).ready(function(){

	
/*	Insert a comfortable class in the section to indicate that this is a new idea and not a modification */
	<%if(challengeId.equals("-1")){%>
		$(".section").addClass("newCreation");
	<%}else{
		//manage the linked NEED
		List<CLSIdea> needs = NeedLinkedChallengeLocalServiceUtil.getNeedByChallengeId(Long.valueOf(challengeId))	;
			for (CLSIdea need:needs){
	%>
				$('#<%=need.getIdeaID()%>').addClass('linked');
	<%		}
	
		if (needs.size() >0){
	%>
				enableLinkButton();
				cyclingNeeds();
				checkSelectedCard();
	<%	}
	}
	
	if (needReport>0 ) {
	%>
			$('#<%=needReport%>').addClass('linked');
			enableLinkButton();
			cyclingNeeds();
			checkSelectedCard();
	<%}%>
	
	$('.next-button').click(function(e){
		e.preventDefault();
		var gotoId = $(this).data('nextid');
		scrollPage(gotoId);
	});
	
	
	//initialize the sections of Reward
	rewardChange();
	

    // Inizializza la modale con le info del Need
    $('.modal-trigger').leanModal();
    
   <% if (virtuosityPointsEnabled){%>  
		 /*management of div virtuositypoint on change */
		 $("#<portlet:namespace/>numberOfSelectableIdeas").change(function() {
			 
			 manageVirtuosityPoints();
		});
	<%}%>
    

})

//hide-show the Reward details section (Details, number of selectable ideas)
function rewardChange(){
	  
	  if ($('#weHaveReward').is(":checked")){
		  
		  $('#rewardDetailsDiv, #numberIdeaDiv, #virtuosityPointsNumber').show();
		  $('#<portlet:namespace/>challengeWithReward').val("true");
		  
		  
	  }else{
		  $('#rewardDetailsDiv, #numberIdeaDiv, #virtuosityPointsNumber').hide();
		  $('#<portlet:namespace/>challengeWithReward').val("false");
		  
	  }
}

function manageVirtuosityPoints(){
	
	 var numIdea =  $('#<portlet:namespace/>numberOfSelectableIdeas').val();
	 var oldVirtuosityPoints =$('#oldVirtuosityPoints').val();
	 
	 if ( parseInt(numIdea)> parseInt(oldVirtuosityPoints)){		 		 
		 for (var i =  parseInt(oldVirtuosityPoints); i <  parseInt(numIdea); i++) { 
			 
			 var currPos = i+1;
			 
			 
			 $("#virtuosityPointsNumber").append("<div class=\"row\" id=\"virtuosityPointsDiv"+i+"\" >"+
					 "<label>"+virtuosityPointsLabel+": "+currPos+" &nbsp;"+tipVirtuosityPoint+"</label>"+
					 "<div class=\"col s2\">"+
					 	"<input type=\"number\" name=\""+namespaceP+"virtuosityPoint"+i+"\" min=\"1\" value=\"100\" >"+
					"</div>"+ 
					 "</div>"); 
		 };
		 
	 }else{
		 
		 $("#virtuosityPointsNumber > div").last().remove();
	 }
	
	 $('#oldVirtuosityPoints').val(numIdea);
	
}



</script>

