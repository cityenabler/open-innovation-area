/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CLSFavouriteChallengesLocalService}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSFavouriteChallengesLocalService
 * @generated
 */
public class CLSFavouriteChallengesLocalServiceWrapper
	implements CLSFavouriteChallengesLocalService,
		ServiceWrapper<CLSFavouriteChallengesLocalService> {
	public CLSFavouriteChallengesLocalServiceWrapper(
		CLSFavouriteChallengesLocalService clsFavouriteChallengesLocalService) {
		_clsFavouriteChallengesLocalService = clsFavouriteChallengesLocalService;
	}

	/**
	* Adds the c l s favourite challenges to the database. Also notifies the appropriate model listeners.
	*
	* @param clsFavouriteChallenges the c l s favourite challenges
	* @return the c l s favourite challenges that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges addCLSFavouriteChallenges(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges clsFavouriteChallenges)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteChallengesLocalService.addCLSFavouriteChallenges(clsFavouriteChallenges);
	}

	/**
	* Creates a new c l s favourite challenges with the primary key. Does not add the c l s favourite challenges to the database.
	*
	* @param clsFavouriteChallengesPK the primary key for the new c l s favourite challenges
	* @return the new c l s favourite challenges
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges createCLSFavouriteChallenges(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteChallengesPK clsFavouriteChallengesPK) {
		return _clsFavouriteChallengesLocalService.createCLSFavouriteChallenges(clsFavouriteChallengesPK);
	}

	/**
	* Deletes the c l s favourite challenges with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param clsFavouriteChallengesPK the primary key of the c l s favourite challenges
	* @return the c l s favourite challenges that was removed
	* @throws PortalException if a c l s favourite challenges with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges deleteCLSFavouriteChallenges(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteChallengesPK clsFavouriteChallengesPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteChallengesLocalService.deleteCLSFavouriteChallenges(clsFavouriteChallengesPK);
	}

	/**
	* Deletes the c l s favourite challenges from the database. Also notifies the appropriate model listeners.
	*
	* @param clsFavouriteChallenges the c l s favourite challenges
	* @return the c l s favourite challenges that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges deleteCLSFavouriteChallenges(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges clsFavouriteChallenges)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteChallengesLocalService.deleteCLSFavouriteChallenges(clsFavouriteChallenges);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _clsFavouriteChallengesLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteChallengesLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteChallengesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteChallengesLocalService.dynamicQuery(dynamicQuery,
			start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteChallengesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteChallengesLocalService.dynamicQuery(dynamicQuery,
			start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteChallengesLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteChallengesLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges fetchCLSFavouriteChallenges(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteChallengesPK clsFavouriteChallengesPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteChallengesLocalService.fetchCLSFavouriteChallenges(clsFavouriteChallengesPK);
	}

	/**
	* Returns the c l s favourite challenges with the primary key.
	*
	* @param clsFavouriteChallengesPK the primary key of the c l s favourite challenges
	* @return the c l s favourite challenges
	* @throws PortalException if a c l s favourite challenges with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges getCLSFavouriteChallenges(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteChallengesPK clsFavouriteChallengesPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteChallengesLocalService.getCLSFavouriteChallenges(clsFavouriteChallengesPK);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteChallengesLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the c l s favourite challengeses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteChallengesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s favourite challengeses
	* @param end the upper bound of the range of c l s favourite challengeses (not inclusive)
	* @return the range of c l s favourite challengeses
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges> getCLSFavouriteChallengeses(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteChallengesLocalService.getCLSFavouriteChallengeses(start,
			end);
	}

	/**
	* Returns the number of c l s favourite challengeses.
	*
	* @return the number of c l s favourite challengeses
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getCLSFavouriteChallengesesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteChallengesLocalService.getCLSFavouriteChallengesesCount();
	}

	/**
	* Updates the c l s favourite challenges in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param clsFavouriteChallenges the c l s favourite challenges
	* @return the c l s favourite challenges that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges updateCLSFavouriteChallenges(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges clsFavouriteChallenges)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteChallengesLocalService.updateCLSFavouriteChallenges(clsFavouriteChallenges);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _clsFavouriteChallengesLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_clsFavouriteChallengesLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _clsFavouriteChallengesLocalService.invokeMethod(name,
			parameterTypes, arguments);
	}

	@Override
	public void removeFavouriteChallengesOnChallengeDelete(long challengeId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException {
		_clsFavouriteChallengesLocalService.removeFavouriteChallengesOnChallengeDelete(challengeId);
	}

	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges> getFavouriteChallengesEntriesByChallengeId(
		long challengeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteChallengesLocalService.getFavouriteChallengesEntriesByChallengeId(challengeId);
	}

	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge> getFavouriteChallengesByChallengeId(
		long challengeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteChallengesLocalService.getFavouriteChallengesByChallengeId(challengeId);
	}

	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge> getFavouriteChallengesByUserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteChallengesLocalService.getFavouriteChallengesByUserId(userId);
	}

	@Override
	public boolean addFavouriteChallenge(java.lang.Long favChallengeId,
		java.lang.Long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteChallengesLocalService.addFavouriteChallenge(favChallengeId,
			userId);
	}

	@Override
	public boolean removeFavouriteChallenge(java.lang.Long favChallengeId,
		java.lang.Long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteChallengesLocalService.removeFavouriteChallenge(favChallengeId,
			userId);
	}

	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges> getFavouriteChallenges(
		java.lang.Long favChallengeId, java.lang.Long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteChallengesLocalService.getFavouriteChallenges(favChallengeId,
			userId);
	}

	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges> getFavouriteChallengesEntriesByUserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteChallengesLocalService.getFavouriteChallengesEntriesByUserId(userId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public CLSFavouriteChallengesLocalService getWrappedCLSFavouriteChallengesLocalService() {
		return _clsFavouriteChallengesLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedCLSFavouriteChallengesLocalService(
		CLSFavouriteChallengesLocalService clsFavouriteChallengesLocalService) {
		_clsFavouriteChallengesLocalService = clsFavouriteChallengesLocalService;
	}

	@Override
	public CLSFavouriteChallengesLocalService getWrappedService() {
		return _clsFavouriteChallengesLocalService;
	}

	@Override
	public void setWrappedService(
		CLSFavouriteChallengesLocalService clsFavouriteChallengesLocalService) {
		_clsFavouriteChallengesLocalService = clsFavouriteChallengesLocalService;
	}

	private CLSFavouriteChallengesLocalService _clsFavouriteChallengesLocalService;
}