/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer;
import it.eng.rspa.ideas.challenges.servicelayer.service.InvitedMailLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.InvitedMailPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 */
public class InvitedMailClp extends BaseModelImpl<InvitedMail>
	implements InvitedMail {
	public InvitedMailClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return InvitedMail.class;
	}

	@Override
	public String getModelClassName() {
		return InvitedMail.class.getName();
	}

	@Override
	public InvitedMailPK getPrimaryKey() {
		return new InvitedMailPK(_ideaId, _mail);
	}

	@Override
	public void setPrimaryKey(InvitedMailPK primaryKey) {
		setIdeaId(primaryKey.ideaId);
		setMail(primaryKey.mail);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new InvitedMailPK(_ideaId, _mail);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((InvitedMailPK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ideaId", getIdeaId());
		attributes.put("mail", getMail());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ideaId = (Long)attributes.get("ideaId");

		if (ideaId != null) {
			setIdeaId(ideaId);
		}

		String mail = (String)attributes.get("mail");

		if (mail != null) {
			setMail(mail);
		}
	}

	@Override
	public long getIdeaId() {
		return _ideaId;
	}

	@Override
	public void setIdeaId(long ideaId) {
		_ideaId = ideaId;

		if (_invitedMailRemoteModel != null) {
			try {
				Class<?> clazz = _invitedMailRemoteModel.getClass();

				Method method = clazz.getMethod("setIdeaId", long.class);

				method.invoke(_invitedMailRemoteModel, ideaId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getMail() {
		return _mail;
	}

	@Override
	public void setMail(String mail) {
		_mail = mail;

		if (_invitedMailRemoteModel != null) {
			try {
				Class<?> clazz = _invitedMailRemoteModel.getClass();

				Method method = clazz.getMethod("setMail", String.class);

				method.invoke(_invitedMailRemoteModel, mail);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getInvitedMailRemoteModel() {
		return _invitedMailRemoteModel;
	}

	public void setInvitedMailRemoteModel(BaseModel<?> invitedMailRemoteModel) {
		_invitedMailRemoteModel = invitedMailRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _invitedMailRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_invitedMailRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			InvitedMailLocalServiceUtil.addInvitedMail(this);
		}
		else {
			InvitedMailLocalServiceUtil.updateInvitedMail(this);
		}
	}

	@Override
	public InvitedMail toEscapedModel() {
		return (InvitedMail)ProxyUtil.newProxyInstance(InvitedMail.class.getClassLoader(),
			new Class[] { InvitedMail.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		InvitedMailClp clone = new InvitedMailClp();

		clone.setIdeaId(getIdeaId());
		clone.setMail(getMail());

		return clone;
	}

	@Override
	public int compareTo(InvitedMail invitedMail) {
		InvitedMailPK primaryKey = invitedMail.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof InvitedMailClp)) {
			return false;
		}

		InvitedMailClp invitedMail = (InvitedMailClp)obj;

		InvitedMailPK primaryKey = invitedMail.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{ideaId=");
		sb.append(getIdeaId());
		sb.append(", mail=");
		sb.append(getMail());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(10);

		sb.append("<model><model-name>");
		sb.append("it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>ideaId</column-name><column-value><![CDATA[");
		sb.append(getIdeaId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>mail</column-name><column-value><![CDATA[");
		sb.append(getMail());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _ideaId;
	private String _mail;
	private BaseModel<?> _invitedMailRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.class;
}