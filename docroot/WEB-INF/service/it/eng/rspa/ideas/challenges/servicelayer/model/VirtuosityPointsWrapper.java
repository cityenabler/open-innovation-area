/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link VirtuosityPoints}.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see VirtuosityPoints
 * @generated
 */
public class VirtuosityPointsWrapper implements VirtuosityPoints,
	ModelWrapper<VirtuosityPoints> {
	public VirtuosityPointsWrapper(VirtuosityPoints virtuosityPoints) {
		_virtuosityPoints = virtuosityPoints;
	}

	@Override
	public Class<?> getModelClass() {
		return VirtuosityPoints.class;
	}

	@Override
	public String getModelClassName() {
		return VirtuosityPoints.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("challengeId", getChallengeId());
		attributes.put("position", getPosition());
		attributes.put("points", getPoints());
		attributes.put("dateAdded", getDateAdded());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long challengeId = (Long)attributes.get("challengeId");

		if (challengeId != null) {
			setChallengeId(challengeId);
		}

		Long position = (Long)attributes.get("position");

		if (position != null) {
			setPosition(position);
		}

		Long points = (Long)attributes.get("points");

		if (points != null) {
			setPoints(points);
		}

		Date dateAdded = (Date)attributes.get("dateAdded");

		if (dateAdded != null) {
			setDateAdded(dateAdded);
		}
	}

	/**
	* Returns the primary key of this virtuosity points.
	*
	* @return the primary key of this virtuosity points
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.service.persistence.VirtuosityPointsPK getPrimaryKey() {
		return _virtuosityPoints.getPrimaryKey();
	}

	/**
	* Sets the primary key of this virtuosity points.
	*
	* @param primaryKey the primary key of this virtuosity points
	*/
	@Override
	public void setPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.VirtuosityPointsPK primaryKey) {
		_virtuosityPoints.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the challenge ID of this virtuosity points.
	*
	* @return the challenge ID of this virtuosity points
	*/
	@Override
	public long getChallengeId() {
		return _virtuosityPoints.getChallengeId();
	}

	/**
	* Sets the challenge ID of this virtuosity points.
	*
	* @param challengeId the challenge ID of this virtuosity points
	*/
	@Override
	public void setChallengeId(long challengeId) {
		_virtuosityPoints.setChallengeId(challengeId);
	}

	/**
	* Returns the position of this virtuosity points.
	*
	* @return the position of this virtuosity points
	*/
	@Override
	public long getPosition() {
		return _virtuosityPoints.getPosition();
	}

	/**
	* Sets the position of this virtuosity points.
	*
	* @param position the position of this virtuosity points
	*/
	@Override
	public void setPosition(long position) {
		_virtuosityPoints.setPosition(position);
	}

	/**
	* Returns the points of this virtuosity points.
	*
	* @return the points of this virtuosity points
	*/
	@Override
	public long getPoints() {
		return _virtuosityPoints.getPoints();
	}

	/**
	* Sets the points of this virtuosity points.
	*
	* @param points the points of this virtuosity points
	*/
	@Override
	public void setPoints(long points) {
		_virtuosityPoints.setPoints(points);
	}

	/**
	* Returns the date added of this virtuosity points.
	*
	* @return the date added of this virtuosity points
	*/
	@Override
	public java.util.Date getDateAdded() {
		return _virtuosityPoints.getDateAdded();
	}

	/**
	* Sets the date added of this virtuosity points.
	*
	* @param dateAdded the date added of this virtuosity points
	*/
	@Override
	public void setDateAdded(java.util.Date dateAdded) {
		_virtuosityPoints.setDateAdded(dateAdded);
	}

	@Override
	public boolean isNew() {
		return _virtuosityPoints.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_virtuosityPoints.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _virtuosityPoints.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_virtuosityPoints.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _virtuosityPoints.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _virtuosityPoints.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_virtuosityPoints.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _virtuosityPoints.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_virtuosityPoints.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_virtuosityPoints.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_virtuosityPoints.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new VirtuosityPointsWrapper((VirtuosityPoints)_virtuosityPoints.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints virtuosityPoints) {
		return _virtuosityPoints.compareTo(virtuosityPoints);
	}

	@Override
	public int hashCode() {
		return _virtuosityPoints.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints> toCacheModel() {
		return _virtuosityPoints.toCacheModel();
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints toEscapedModel() {
		return new VirtuosityPointsWrapper(_virtuosityPoints.toEscapedModel());
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints toUnescapedModel() {
		return new VirtuosityPointsWrapper(_virtuosityPoints.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _virtuosityPoints.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _virtuosityPoints.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_virtuosityPoints.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof VirtuosityPointsWrapper)) {
			return false;
		}

		VirtuosityPointsWrapper virtuosityPointsWrapper = (VirtuosityPointsWrapper)obj;

		if (Validator.equals(_virtuosityPoints,
					virtuosityPointsWrapper._virtuosityPoints)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public VirtuosityPoints getWrappedVirtuosityPoints() {
		return _virtuosityPoints;
	}

	@Override
	public VirtuosityPoints getWrappedModel() {
		return _virtuosityPoints;
	}

	@Override
	public void resetOriginalValues() {
		_virtuosityPoints.resetOriginalValues();
	}

	private VirtuosityPoints _virtuosityPoints;
}