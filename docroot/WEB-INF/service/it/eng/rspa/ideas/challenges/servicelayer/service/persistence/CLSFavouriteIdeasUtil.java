/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas;

import java.util.List;

/**
 * The persistence utility for the c l s favourite ideas service. This utility wraps {@link CLSFavouriteIdeasPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSFavouriteIdeasPersistence
 * @see CLSFavouriteIdeasPersistenceImpl
 * @generated
 */
public class CLSFavouriteIdeasUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(CLSFavouriteIdeas clsFavouriteIdeas) {
		getPersistence().clearCache(clsFavouriteIdeas);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<CLSFavouriteIdeas> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<CLSFavouriteIdeas> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<CLSFavouriteIdeas> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static CLSFavouriteIdeas update(CLSFavouriteIdeas clsFavouriteIdeas)
		throws SystemException {
		return getPersistence().update(clsFavouriteIdeas);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static CLSFavouriteIdeas update(
		CLSFavouriteIdeas clsFavouriteIdeas, ServiceContext serviceContext)
		throws SystemException {
		return getPersistence().update(clsFavouriteIdeas, serviceContext);
	}

	/**
	* Returns all the c l s favourite ideases where ideaID = &#63;.
	*
	* @param ideaID the idea i d
	* @return the matching c l s favourite ideases
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas> findByIdeaID(
		long ideaID) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByIdeaID(ideaID);
	}

	/**
	* Returns a range of all the c l s favourite ideases where ideaID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteIdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaID the idea i d
	* @param start the lower bound of the range of c l s favourite ideases
	* @param end the upper bound of the range of c l s favourite ideases (not inclusive)
	* @return the range of matching c l s favourite ideases
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas> findByIdeaID(
		long ideaID, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByIdeaID(ideaID, start, end);
	}

	/**
	* Returns an ordered range of all the c l s favourite ideases where ideaID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteIdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaID the idea i d
	* @param start the lower bound of the range of c l s favourite ideases
	* @param end the upper bound of the range of c l s favourite ideases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching c l s favourite ideases
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas> findByIdeaID(
		long ideaID, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByIdeaID(ideaID, start, end, orderByComparator);
	}

	/**
	* Returns the first c l s favourite ideas in the ordered set where ideaID = &#63;.
	*
	* @param ideaID the idea i d
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s favourite ideas
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException if a matching c l s favourite ideas could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas findByIdeaID_First(
		long ideaID,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException {
		return getPersistence().findByIdeaID_First(ideaID, orderByComparator);
	}

	/**
	* Returns the first c l s favourite ideas in the ordered set where ideaID = &#63;.
	*
	* @param ideaID the idea i d
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s favourite ideas, or <code>null</code> if a matching c l s favourite ideas could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas fetchByIdeaID_First(
		long ideaID,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByIdeaID_First(ideaID, orderByComparator);
	}

	/**
	* Returns the last c l s favourite ideas in the ordered set where ideaID = &#63;.
	*
	* @param ideaID the idea i d
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s favourite ideas
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException if a matching c l s favourite ideas could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas findByIdeaID_Last(
		long ideaID,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException {
		return getPersistence().findByIdeaID_Last(ideaID, orderByComparator);
	}

	/**
	* Returns the last c l s favourite ideas in the ordered set where ideaID = &#63;.
	*
	* @param ideaID the idea i d
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s favourite ideas, or <code>null</code> if a matching c l s favourite ideas could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas fetchByIdeaID_Last(
		long ideaID,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByIdeaID_Last(ideaID, orderByComparator);
	}

	/**
	* Returns the c l s favourite ideases before and after the current c l s favourite ideas in the ordered set where ideaID = &#63;.
	*
	* @param clsFavouriteIdeasPK the primary key of the current c l s favourite ideas
	* @param ideaID the idea i d
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next c l s favourite ideas
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException if a c l s favourite ideas with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas[] findByIdeaID_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteIdeasPK clsFavouriteIdeasPK,
		long ideaID,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException {
		return getPersistence()
				   .findByIdeaID_PrevAndNext(clsFavouriteIdeasPK, ideaID,
			orderByComparator);
	}

	/**
	* Removes all the c l s favourite ideases where ideaID = &#63; from the database.
	*
	* @param ideaID the idea i d
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByIdeaID(long ideaID)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByIdeaID(ideaID);
	}

	/**
	* Returns the number of c l s favourite ideases where ideaID = &#63;.
	*
	* @param ideaID the idea i d
	* @return the number of matching c l s favourite ideases
	* @throws SystemException if a system exception occurred
	*/
	public static int countByIdeaID(long ideaID)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByIdeaID(ideaID);
	}

	/**
	* Returns all the c l s favourite ideases where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching c l s favourite ideases
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas> findByUserID(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUserID(userId);
	}

	/**
	* Returns a range of all the c l s favourite ideases where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteIdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of c l s favourite ideases
	* @param end the upper bound of the range of c l s favourite ideases (not inclusive)
	* @return the range of matching c l s favourite ideases
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas> findByUserID(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUserID(userId, start, end);
	}

	/**
	* Returns an ordered range of all the c l s favourite ideases where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteIdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of c l s favourite ideases
	* @param end the upper bound of the range of c l s favourite ideases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching c l s favourite ideases
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas> findByUserID(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUserID(userId, start, end, orderByComparator);
	}

	/**
	* Returns the first c l s favourite ideas in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s favourite ideas
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException if a matching c l s favourite ideas could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas findByUserID_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException {
		return getPersistence().findByUserID_First(userId, orderByComparator);
	}

	/**
	* Returns the first c l s favourite ideas in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s favourite ideas, or <code>null</code> if a matching c l s favourite ideas could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas fetchByUserID_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByUserID_First(userId, orderByComparator);
	}

	/**
	* Returns the last c l s favourite ideas in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s favourite ideas
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException if a matching c l s favourite ideas could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas findByUserID_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException {
		return getPersistence().findByUserID_Last(userId, orderByComparator);
	}

	/**
	* Returns the last c l s favourite ideas in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s favourite ideas, or <code>null</code> if a matching c l s favourite ideas could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas fetchByUserID_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByUserID_Last(userId, orderByComparator);
	}

	/**
	* Returns the c l s favourite ideases before and after the current c l s favourite ideas in the ordered set where userId = &#63;.
	*
	* @param clsFavouriteIdeasPK the primary key of the current c l s favourite ideas
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next c l s favourite ideas
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException if a c l s favourite ideas with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas[] findByUserID_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteIdeasPK clsFavouriteIdeasPK,
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException {
		return getPersistence()
				   .findByUserID_PrevAndNext(clsFavouriteIdeasPK, userId,
			orderByComparator);
	}

	/**
	* Removes all the c l s favourite ideases where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByUserID(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByUserID(userId);
	}

	/**
	* Returns the number of c l s favourite ideases where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching c l s favourite ideases
	* @throws SystemException if a system exception occurred
	*/
	public static int countByUserID(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByUserID(userId);
	}

	/**
	* Returns the c l s favourite ideas where ideaID = &#63; and userId = &#63; or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException} if it could not be found.
	*
	* @param ideaID the idea i d
	* @param userId the user ID
	* @return the matching c l s favourite ideas
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException if a matching c l s favourite ideas could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas findByIdeaIDAndUserID(
		long ideaID, long userId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException {
		return getPersistence().findByIdeaIDAndUserID(ideaID, userId);
	}

	/**
	* Returns the c l s favourite ideas where ideaID = &#63; and userId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param ideaID the idea i d
	* @param userId the user ID
	* @return the matching c l s favourite ideas, or <code>null</code> if a matching c l s favourite ideas could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas fetchByIdeaIDAndUserID(
		long ideaID, long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByIdeaIDAndUserID(ideaID, userId);
	}

	/**
	* Returns the c l s favourite ideas where ideaID = &#63; and userId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param ideaID the idea i d
	* @param userId the user ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching c l s favourite ideas, or <code>null</code> if a matching c l s favourite ideas could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas fetchByIdeaIDAndUserID(
		long ideaID, long userId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByIdeaIDAndUserID(ideaID, userId, retrieveFromCache);
	}

	/**
	* Removes the c l s favourite ideas where ideaID = &#63; and userId = &#63; from the database.
	*
	* @param ideaID the idea i d
	* @param userId the user ID
	* @return the c l s favourite ideas that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas removeByIdeaIDAndUserID(
		long ideaID, long userId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException {
		return getPersistence().removeByIdeaIDAndUserID(ideaID, userId);
	}

	/**
	* Returns the number of c l s favourite ideases where ideaID = &#63; and userId = &#63;.
	*
	* @param ideaID the idea i d
	* @param userId the user ID
	* @return the number of matching c l s favourite ideases
	* @throws SystemException if a system exception occurred
	*/
	public static int countByIdeaIDAndUserID(long ideaID, long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByIdeaIDAndUserID(ideaID, userId);
	}

	/**
	* Caches the c l s favourite ideas in the entity cache if it is enabled.
	*
	* @param clsFavouriteIdeas the c l s favourite ideas
	*/
	public static void cacheResult(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas clsFavouriteIdeas) {
		getPersistence().cacheResult(clsFavouriteIdeas);
	}

	/**
	* Caches the c l s favourite ideases in the entity cache if it is enabled.
	*
	* @param clsFavouriteIdeases the c l s favourite ideases
	*/
	public static void cacheResult(
		java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas> clsFavouriteIdeases) {
		getPersistence().cacheResult(clsFavouriteIdeases);
	}

	/**
	* Creates a new c l s favourite ideas with the primary key. Does not add the c l s favourite ideas to the database.
	*
	* @param clsFavouriteIdeasPK the primary key for the new c l s favourite ideas
	* @return the new c l s favourite ideas
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas create(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteIdeasPK clsFavouriteIdeasPK) {
		return getPersistence().create(clsFavouriteIdeasPK);
	}

	/**
	* Removes the c l s favourite ideas with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param clsFavouriteIdeasPK the primary key of the c l s favourite ideas
	* @return the c l s favourite ideas that was removed
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException if a c l s favourite ideas with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas remove(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteIdeasPK clsFavouriteIdeasPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException {
		return getPersistence().remove(clsFavouriteIdeasPK);
	}

	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas clsFavouriteIdeas)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(clsFavouriteIdeas);
	}

	/**
	* Returns the c l s favourite ideas with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException} if it could not be found.
	*
	* @param clsFavouriteIdeasPK the primary key of the c l s favourite ideas
	* @return the c l s favourite ideas
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException if a c l s favourite ideas with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas findByPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteIdeasPK clsFavouriteIdeasPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException {
		return getPersistence().findByPrimaryKey(clsFavouriteIdeasPK);
	}

	/**
	* Returns the c l s favourite ideas with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param clsFavouriteIdeasPK the primary key of the c l s favourite ideas
	* @return the c l s favourite ideas, or <code>null</code> if a c l s favourite ideas with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas fetchByPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteIdeasPK clsFavouriteIdeasPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(clsFavouriteIdeasPK);
	}

	/**
	* Returns all the c l s favourite ideases.
	*
	* @return the c l s favourite ideases
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the c l s favourite ideases.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteIdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s favourite ideases
	* @param end the upper bound of the range of c l s favourite ideases (not inclusive)
	* @return the range of c l s favourite ideases
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the c l s favourite ideases.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteIdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s favourite ideases
	* @param end the upper bound of the range of c l s favourite ideases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of c l s favourite ideases
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the c l s favourite ideases from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of c l s favourite ideases.
	*
	* @return the number of c l s favourite ideases
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static CLSFavouriteIdeasPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (CLSFavouriteIdeasPersistence)PortletBeanLocatorUtil.locate(it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.getServletContextName(),
					CLSFavouriteIdeasPersistence.class.getName());

			ReferenceRegistry.registerReference(CLSFavouriteIdeasUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(CLSFavouriteIdeasPersistence persistence) {
	}

	private static CLSFavouriteIdeasPersistence _persistence;
}