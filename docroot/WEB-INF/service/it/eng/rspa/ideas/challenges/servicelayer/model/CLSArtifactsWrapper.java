/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CLSArtifacts}.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSArtifacts
 * @generated
 */
public class CLSArtifactsWrapper implements CLSArtifacts,
	ModelWrapper<CLSArtifacts> {
	public CLSArtifactsWrapper(CLSArtifacts clsArtifacts) {
		_clsArtifacts = clsArtifacts;
	}

	@Override
	public Class<?> getModelClass() {
		return CLSArtifacts.class;
	}

	@Override
	public String getModelClassName() {
		return CLSArtifacts.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ideaId", getIdeaId());
		attributes.put("artifactId", getArtifactId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ideaId = (Long)attributes.get("ideaId");

		if (ideaId != null) {
			setIdeaId(ideaId);
		}

		Long artifactId = (Long)attributes.get("artifactId");

		if (artifactId != null) {
			setArtifactId(artifactId);
		}
	}

	/**
	* Returns the primary key of this c l s artifacts.
	*
	* @return the primary key of this c l s artifacts
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSArtifactsPK getPrimaryKey() {
		return _clsArtifacts.getPrimaryKey();
	}

	/**
	* Sets the primary key of this c l s artifacts.
	*
	* @param primaryKey the primary key of this c l s artifacts
	*/
	@Override
	public void setPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSArtifactsPK primaryKey) {
		_clsArtifacts.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the idea ID of this c l s artifacts.
	*
	* @return the idea ID of this c l s artifacts
	*/
	@Override
	public long getIdeaId() {
		return _clsArtifacts.getIdeaId();
	}

	/**
	* Sets the idea ID of this c l s artifacts.
	*
	* @param ideaId the idea ID of this c l s artifacts
	*/
	@Override
	public void setIdeaId(long ideaId) {
		_clsArtifacts.setIdeaId(ideaId);
	}

	/**
	* Returns the artifact ID of this c l s artifacts.
	*
	* @return the artifact ID of this c l s artifacts
	*/
	@Override
	public long getArtifactId() {
		return _clsArtifacts.getArtifactId();
	}

	/**
	* Sets the artifact ID of this c l s artifacts.
	*
	* @param artifactId the artifact ID of this c l s artifacts
	*/
	@Override
	public void setArtifactId(long artifactId) {
		_clsArtifacts.setArtifactId(artifactId);
	}

	@Override
	public boolean isNew() {
		return _clsArtifacts.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_clsArtifacts.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _clsArtifacts.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_clsArtifacts.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _clsArtifacts.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _clsArtifacts.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_clsArtifacts.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _clsArtifacts.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_clsArtifacts.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_clsArtifacts.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_clsArtifacts.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CLSArtifactsWrapper((CLSArtifacts)_clsArtifacts.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts clsArtifacts) {
		return _clsArtifacts.compareTo(clsArtifacts);
	}

	@Override
	public int hashCode() {
		return _clsArtifacts.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> toCacheModel() {
		return _clsArtifacts.toCacheModel();
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts toEscapedModel() {
		return new CLSArtifactsWrapper(_clsArtifacts.toEscapedModel());
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts toUnescapedModel() {
		return new CLSArtifactsWrapper(_clsArtifacts.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _clsArtifacts.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _clsArtifacts.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_clsArtifacts.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSArtifactsWrapper)) {
			return false;
		}

		CLSArtifactsWrapper clsArtifactsWrapper = (CLSArtifactsWrapper)obj;

		if (Validator.equals(_clsArtifacts, clsArtifactsWrapper._clsArtifacts)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public CLSArtifacts getWrappedCLSArtifacts() {
		return _clsArtifacts;
	}

	@Override
	public CLSArtifacts getWrappedModel() {
		return _clsArtifacts;
	}

	@Override
	public void resetOriginalValues() {
		_clsArtifacts.resetOriginalValues();
	}

	private CLSArtifacts _clsArtifacts;
}