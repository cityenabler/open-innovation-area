/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.messaging;

import com.liferay.portal.kernel.messaging.BaseMessageListener;
import com.liferay.portal.kernel.messaging.Message;

import it.eng.rspa.ideas.challenges.servicelayer.service.CLSArtifactsLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSArtifactsServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSCategoriesSetForChallengeLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSCategoriesSetForChallengeServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSCategoriesSetLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSCategoriesSetServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengePoiLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengePoiServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengesCalendarLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengesCalendarServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSCoworkerLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSCoworkerServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteChallengesLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteChallengesServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteIdeasLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteIdeasServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSFriendlyUrlSuffixLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSFriendlyUrlSuffixServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaPoiLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaPoiServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSMapPropertiesLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSMapPropertiesServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSOpLogIdeaLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSOpLogIdeaServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSRequisitiLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSRequisitiServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSVmeProjectsLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSVmeProjectsServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer;
import it.eng.rspa.ideas.challenges.servicelayer.service.EvaluationCriteriaLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.EvaluationCriteriaServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.GESelectedLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.GESelectedServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.IdeaEvaluationLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.IdeaEvaluationServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.InvitedMailLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.InvitedMailServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.NeedLinkedChallengeLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.NeedLinkedChallengeServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.RestAPIsServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.VirtuosityPointsLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.VirtuosityPointsServiceUtil;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 */
public class ClpMessageListener extends BaseMessageListener {
	public static String getServletContextName() {
		return ClpSerializer.getServletContextName();
	}

	@Override
	protected void doReceive(Message message) throws Exception {
		String command = message.getString("command");
		String servletContextName = message.getString("servletContextName");

		if (command.equals("undeploy") &&
				servletContextName.equals(getServletContextName())) {
			CLSArtifactsLocalServiceUtil.clearService();

			CLSArtifactsServiceUtil.clearService();
			CLSCategoriesSetLocalServiceUtil.clearService();

			CLSCategoriesSetServiceUtil.clearService();
			CLSCategoriesSetForChallengeLocalServiceUtil.clearService();

			CLSCategoriesSetForChallengeServiceUtil.clearService();
			CLSChallengeLocalServiceUtil.clearService();

			CLSChallengeServiceUtil.clearService();
			CLSChallengePoiLocalServiceUtil.clearService();

			CLSChallengePoiServiceUtil.clearService();
			CLSChallengesCalendarLocalServiceUtil.clearService();

			CLSChallengesCalendarServiceUtil.clearService();
			CLSCoworkerLocalServiceUtil.clearService();

			CLSCoworkerServiceUtil.clearService();
			CLSFavouriteChallengesLocalServiceUtil.clearService();

			CLSFavouriteChallengesServiceUtil.clearService();
			CLSFavouriteIdeasLocalServiceUtil.clearService();

			CLSFavouriteIdeasServiceUtil.clearService();
			CLSFriendlyUrlSuffixLocalServiceUtil.clearService();

			CLSFriendlyUrlSuffixServiceUtil.clearService();
			CLSIdeaLocalServiceUtil.clearService();

			CLSIdeaServiceUtil.clearService();
			CLSIdeaPoiLocalServiceUtil.clearService();

			CLSIdeaPoiServiceUtil.clearService();
			CLSMapPropertiesLocalServiceUtil.clearService();

			CLSMapPropertiesServiceUtil.clearService();
			CLSOpLogIdeaLocalServiceUtil.clearService();

			CLSOpLogIdeaServiceUtil.clearService();
			CLSRequisitiLocalServiceUtil.clearService();

			CLSRequisitiServiceUtil.clearService();
			CLSVmeProjectsLocalServiceUtil.clearService();

			CLSVmeProjectsServiceUtil.clearService();
			EvaluationCriteriaLocalServiceUtil.clearService();

			EvaluationCriteriaServiceUtil.clearService();
			GESelectedLocalServiceUtil.clearService();

			GESelectedServiceUtil.clearService();
			IdeaEvaluationLocalServiceUtil.clearService();

			IdeaEvaluationServiceUtil.clearService();
			InvitedMailLocalServiceUtil.clearService();

			InvitedMailServiceUtil.clearService();
			NeedLinkedChallengeLocalServiceUtil.clearService();

			NeedLinkedChallengeServiceUtil.clearService();

			RestAPIsServiceUtil.clearService();
			VirtuosityPointsLocalServiceUtil.clearService();

			VirtuosityPointsServiceUtil.clearService();
		}
	}
}