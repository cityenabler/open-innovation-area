/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.eng.rspa.ideas.challenges.servicelayer.service.http.CLSCoworkerServiceSoap}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.http.CLSCoworkerServiceSoap
 * @generated
 */
public class CLSCoworkerSoap implements Serializable {
	public static CLSCoworkerSoap toSoapModel(CLSCoworker model) {
		CLSCoworkerSoap soapModel = new CLSCoworkerSoap();

		soapModel.setIdeaID(model.getIdeaID());
		soapModel.setCoworkerId(model.getCoworkerId());
		soapModel.setUserId(model.getUserId());

		return soapModel;
	}

	public static CLSCoworkerSoap[] toSoapModels(CLSCoworker[] models) {
		CLSCoworkerSoap[] soapModels = new CLSCoworkerSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CLSCoworkerSoap[][] toSoapModels(CLSCoworker[][] models) {
		CLSCoworkerSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CLSCoworkerSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CLSCoworkerSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CLSCoworkerSoap[] toSoapModels(List<CLSCoworker> models) {
		List<CLSCoworkerSoap> soapModels = new ArrayList<CLSCoworkerSoap>(models.size());

		for (CLSCoworker model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CLSCoworkerSoap[soapModels.size()]);
	}

	public CLSCoworkerSoap() {
	}

	public long getPrimaryKey() {
		return _coworkerId;
	}

	public void setPrimaryKey(long pk) {
		setCoworkerId(pk);
	}

	public long getIdeaID() {
		return _ideaID;
	}

	public void setIdeaID(long ideaID) {
		_ideaID = ideaID;
	}

	public long getCoworkerId() {
		return _coworkerId;
	}

	public void setCoworkerId(long coworkerId) {
		_coworkerId = coworkerId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	private long _ideaID;
	private long _coworkerId;
	private long _userId;
}