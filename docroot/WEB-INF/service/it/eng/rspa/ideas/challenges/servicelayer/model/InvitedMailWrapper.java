/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link InvitedMail}.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see InvitedMail
 * @generated
 */
public class InvitedMailWrapper implements InvitedMail,
	ModelWrapper<InvitedMail> {
	public InvitedMailWrapper(InvitedMail invitedMail) {
		_invitedMail = invitedMail;
	}

	@Override
	public Class<?> getModelClass() {
		return InvitedMail.class;
	}

	@Override
	public String getModelClassName() {
		return InvitedMail.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ideaId", getIdeaId());
		attributes.put("mail", getMail());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ideaId = (Long)attributes.get("ideaId");

		if (ideaId != null) {
			setIdeaId(ideaId);
		}

		String mail = (String)attributes.get("mail");

		if (mail != null) {
			setMail(mail);
		}
	}

	/**
	* Returns the primary key of this invited mail.
	*
	* @return the primary key of this invited mail
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.service.persistence.InvitedMailPK getPrimaryKey() {
		return _invitedMail.getPrimaryKey();
	}

	/**
	* Sets the primary key of this invited mail.
	*
	* @param primaryKey the primary key of this invited mail
	*/
	@Override
	public void setPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.InvitedMailPK primaryKey) {
		_invitedMail.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the idea ID of this invited mail.
	*
	* @return the idea ID of this invited mail
	*/
	@Override
	public long getIdeaId() {
		return _invitedMail.getIdeaId();
	}

	/**
	* Sets the idea ID of this invited mail.
	*
	* @param ideaId the idea ID of this invited mail
	*/
	@Override
	public void setIdeaId(long ideaId) {
		_invitedMail.setIdeaId(ideaId);
	}

	/**
	* Returns the mail of this invited mail.
	*
	* @return the mail of this invited mail
	*/
	@Override
	public java.lang.String getMail() {
		return _invitedMail.getMail();
	}

	/**
	* Sets the mail of this invited mail.
	*
	* @param mail the mail of this invited mail
	*/
	@Override
	public void setMail(java.lang.String mail) {
		_invitedMail.setMail(mail);
	}

	@Override
	public boolean isNew() {
		return _invitedMail.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_invitedMail.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _invitedMail.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_invitedMail.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _invitedMail.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _invitedMail.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_invitedMail.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _invitedMail.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_invitedMail.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_invitedMail.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_invitedMail.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new InvitedMailWrapper((InvitedMail)_invitedMail.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail invitedMail) {
		return _invitedMail.compareTo(invitedMail);
	}

	@Override
	public int hashCode() {
		return _invitedMail.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail> toCacheModel() {
		return _invitedMail.toCacheModel();
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail toEscapedModel() {
		return new InvitedMailWrapper(_invitedMail.toEscapedModel());
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail toUnescapedModel() {
		return new InvitedMailWrapper(_invitedMail.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _invitedMail.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _invitedMail.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_invitedMail.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof InvitedMailWrapper)) {
			return false;
		}

		InvitedMailWrapper invitedMailWrapper = (InvitedMailWrapper)obj;

		if (Validator.equals(_invitedMail, invitedMailWrapper._invitedMail)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public InvitedMail getWrappedInvitedMail() {
		return _invitedMail;
	}

	@Override
	public InvitedMail getWrappedModel() {
		return _invitedMail;
	}

	@Override
	public void resetOriginalValues() {
		_invitedMail.resetOriginalValues();
	}

	private InvitedMail _invitedMail;
}