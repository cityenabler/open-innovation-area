/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for CLSChallenge. This utility wraps
 * {@link it.eng.rspa.ideas.challenges.servicelayer.service.impl.CLSChallengeLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSChallengeLocalService
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.base.CLSChallengeLocalServiceBaseImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.impl.CLSChallengeLocalServiceImpl
 * @generated
 */
public class CLSChallengeLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.eng.rspa.ideas.challenges.servicelayer.service.impl.CLSChallengeLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the c l s challenge to the database. Also notifies the appropriate model listeners.
	*
	* @param clsChallenge the c l s challenge
	* @return the c l s challenge that was added
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge addCLSChallenge(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge clsChallenge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addCLSChallenge(clsChallenge);
	}

	/**
	* Creates a new c l s challenge with the primary key. Does not add the c l s challenge to the database.
	*
	* @param challengeId the primary key for the new c l s challenge
	* @return the new c l s challenge
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge createCLSChallenge(
		long challengeId) {
		return getService().createCLSChallenge(challengeId);
	}

	/**
	* Deletes the c l s challenge with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param challengeId the primary key of the c l s challenge
	* @return the c l s challenge that was removed
	* @throws PortalException if a c l s challenge with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge deleteCLSChallenge(
		long challengeId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteCLSChallenge(challengeId);
	}

	/**
	* Deletes the c l s challenge from the database. Also notifies the appropriate model listeners.
	*
	* @param clsChallenge the c l s challenge
	* @return the c l s challenge that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge deleteCLSChallenge(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge clsChallenge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteCLSChallenge(clsChallenge);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge fetchCLSChallenge(
		long challengeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchCLSChallenge(challengeId);
	}

	/**
	* Returns the c l s challenge with the matching UUID and company.
	*
	* @param uuid the c l s challenge's UUID
	* @param companyId the primary key of the company
	* @return the matching c l s challenge, or <code>null</code> if a matching c l s challenge could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge fetchCLSChallengeByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchCLSChallengeByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Returns the c l s challenge matching the UUID and group.
	*
	* @param uuid the c l s challenge's UUID
	* @param groupId the primary key of the group
	* @return the matching c l s challenge, or <code>null</code> if a matching c l s challenge could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge fetchCLSChallengeByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchCLSChallengeByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Returns the c l s challenge with the primary key.
	*
	* @param challengeId the primary key of the c l s challenge
	* @return the c l s challenge
	* @throws PortalException if a c l s challenge with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge getCLSChallenge(
		long challengeId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getCLSChallenge(challengeId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the c l s challenge with the matching UUID and company.
	*
	* @param uuid the c l s challenge's UUID
	* @param companyId the primary key of the company
	* @return the matching c l s challenge
	* @throws PortalException if a matching c l s challenge could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge getCLSChallengeByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getCLSChallengeByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Returns the c l s challenge matching the UUID and group.
	*
	* @param uuid the c l s challenge's UUID
	* @param groupId the primary key of the group
	* @return the matching c l s challenge
	* @throws PortalException if a matching c l s challenge could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge getCLSChallengeByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getCLSChallengeByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Returns a range of all the c l s challenges.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s challenges
	* @param end the upper bound of the range of c l s challenges (not inclusive)
	* @return the range of c l s challenges
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge> getCLSChallenges(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getCLSChallenges(start, end);
	}

	/**
	* Returns the number of c l s challenges.
	*
	* @return the number of c l s challenges
	* @throws SystemException if a system exception occurred
	*/
	public static int getCLSChallengesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getCLSChallengesCount();
	}

	/**
	* Updates the c l s challenge in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param clsChallenge the c l s challenge
	* @return the c l s challenge that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge updateCLSChallenge(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge clsChallenge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateCLSChallenge(clsChallenge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static void sendNotification(java.lang.String textMessage,
		long destinationUderId, java.lang.String senderUserName,
		long challengeId, javax.portlet.ActionRequest actionRequest,
		java.lang.String azione)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		getService()
			.sendNotification(textMessage, destinationUderId, senderUserName,
			challengeId, actionRequest, azione);
	}

	public static void sendNotification(java.lang.String textMessage,
		long destinationUderId, java.lang.String senderUserName)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		getService()
			.sendNotification(textMessage, destinationUderId, senderUserName);
	}

	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getIdeasForChallenge(
		long challengeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getIdeasForChallenge(challengeId);
	}

	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge> getChallengesByUserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getChallengesByUserId(userId);
	}

	/**
	* @param userId
	* @param maxNumber
	* @param locale
	* @return
	*/
	public static com.liferay.portal.kernel.json.JSONArray getChallengesForMeByUserId(
		long userId, int maxNumber, java.util.Locale locale) {
		return getService().getChallengesForMeByUserId(userId, maxNumber, locale);
	}

	/**
	* @param pilot
	* @param maxNumber
	* @param locale
	* @return
	*/
	public static com.liferay.portal.kernel.json.JSONArray getTopRatedChallengesByPilot(
		java.lang.String pilot, int maxNumber, java.util.Locale locale) {
		return getService()
				   .getTopRatedChallengesByPilot(pilot, maxNumber, locale);
	}

	/**
	* @param language
	* @param maxNumber
	* @param locale
	* @return
	*/
	public static com.liferay.portal.kernel.json.JSONArray getTopRatedChallengesByLanguage(
		java.lang.String language, int maxNumber, java.util.Locale locale) {
		return getService()
				   .getTopRatedChallengesByLanguage(language, maxNumber, locale);
	}

	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge> getActiveChallenges()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getActiveChallenges();
	}

	public static void clearService() {
		_service = null;
	}

	public static CLSChallengeLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					CLSChallengeLocalService.class.getName());

			if (invokableLocalService instanceof CLSChallengeLocalService) {
				_service = (CLSChallengeLocalService)invokableLocalService;
			}
			else {
				_service = new CLSChallengeLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(CLSChallengeLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(CLSChallengeLocalService service) {
	}

	private static CLSChallengeLocalService _service;
}