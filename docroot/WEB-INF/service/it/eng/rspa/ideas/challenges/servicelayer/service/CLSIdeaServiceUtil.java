/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableService;

/**
 * Provides the remote service utility for CLSIdea. This utility wraps
 * {@link it.eng.rspa.ideas.challenges.servicelayer.service.impl.CLSIdeaServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSIdeaService
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.base.CLSIdeaServiceBaseImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.impl.CLSIdeaServiceImpl
 * @generated
 */
public class CLSIdeaServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.eng.rspa.ideas.challenges.servicelayer.service.impl.CLSIdeaServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static com.liferay.portal.kernel.json.JSONObject publishScreenShot(
		java.lang.String body) {
		return getService().publishScreenShot(body);
	}

	public static com.liferay.portal.kernel.json.JSONObject vcProjectUpdate(
		java.lang.String vmeprojectid, java.lang.String vmeprojectname,
		boolean ismockup, java.lang.String ideaid) {
		return getService()
				   .vcProjectUpdate(vmeprojectid, vmeprojectname, ismockup,
			ideaid);
	}

	/**
	* @param fileEntryId
	
	questo metodo viene utilizzato per eliminare gli allegati con Ajax
	*/
	public static void deleteIdeaFile(long fileEntryId) {
		getService().deleteIdeaFile(fileEntryId);
	}

	public static void clearService() {
		_service = null;
	}

	public static CLSIdeaService getService() {
		if (_service == null) {
			InvokableService invokableService = (InvokableService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					CLSIdeaService.class.getName());

			if (invokableService instanceof CLSIdeaService) {
				_service = (CLSIdeaService)invokableService;
			}
			else {
				_service = new CLSIdeaServiceClp(invokableService);
			}

			ReferenceRegistry.registerReference(CLSIdeaServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(CLSIdeaService service) {
	}

	private static CLSIdeaService _service;
}