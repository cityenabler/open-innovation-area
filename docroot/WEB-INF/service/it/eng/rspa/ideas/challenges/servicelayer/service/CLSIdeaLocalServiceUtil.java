/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for CLSIdea. This utility wraps
 * {@link it.eng.rspa.ideas.challenges.servicelayer.service.impl.CLSIdeaLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSIdeaLocalService
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.base.CLSIdeaLocalServiceBaseImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.impl.CLSIdeaLocalServiceImpl
 * @generated
 */
public class CLSIdeaLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.eng.rspa.ideas.challenges.servicelayer.service.impl.CLSIdeaLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the c l s idea to the database. Also notifies the appropriate model listeners.
	*
	* @param clsIdea the c l s idea
	* @return the c l s idea that was added
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea addCLSIdea(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea clsIdea)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addCLSIdea(clsIdea);
	}

	/**
	* Creates a new c l s idea with the primary key. Does not add the c l s idea to the database.
	*
	* @param ideaID the primary key for the new c l s idea
	* @return the new c l s idea
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea createCLSIdea(
		long ideaID) {
		return getService().createCLSIdea(ideaID);
	}

	/**
	* Deletes the c l s idea with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ideaID the primary key of the c l s idea
	* @return the c l s idea that was removed
	* @throws PortalException if a c l s idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea deleteCLSIdea(
		long ideaID)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteCLSIdea(ideaID);
	}

	/**
	* Deletes the c l s idea from the database. Also notifies the appropriate model listeners.
	*
	* @param clsIdea the c l s idea
	* @return the c l s idea that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea deleteCLSIdea(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea clsIdea)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteCLSIdea(clsIdea);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea fetchCLSIdea(
		long ideaID) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchCLSIdea(ideaID);
	}

	/**
	* Returns the c l s idea with the matching UUID and company.
	*
	* @param uuid the c l s idea's UUID
	* @param companyId the primary key of the company
	* @return the matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea fetchCLSIdeaByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchCLSIdeaByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Returns the c l s idea matching the UUID and group.
	*
	* @param uuid the c l s idea's UUID
	* @param groupId the primary key of the group
	* @return the matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea fetchCLSIdeaByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchCLSIdeaByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Returns the c l s idea with the primary key.
	*
	* @param ideaID the primary key of the c l s idea
	* @return the c l s idea
	* @throws PortalException if a c l s idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea getCLSIdea(
		long ideaID)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getCLSIdea(ideaID);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the c l s idea with the matching UUID and company.
	*
	* @param uuid the c l s idea's UUID
	* @param companyId the primary key of the company
	* @return the matching c l s idea
	* @throws PortalException if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea getCLSIdeaByUuidAndCompanyId(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getCLSIdeaByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Returns the c l s idea matching the UUID and group.
	*
	* @param uuid the c l s idea's UUID
	* @param groupId the primary key of the group
	* @return the matching c l s idea
	* @throws PortalException if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea getCLSIdeaByUuidAndGroupId(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getCLSIdeaByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Returns a range of all the c l s ideas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s ideas
	* @param end the upper bound of the range of c l s ideas (not inclusive)
	* @return the range of c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getCLSIdeas(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getCLSIdeas(start, end);
	}

	/**
	* Returns the number of c l s ideas.
	*
	* @return the number of c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static int getCLSIdeasCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getCLSIdeasCount();
	}

	/**
	* Updates the c l s idea in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param clsIdea the c l s idea
	* @return the c l s idea that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea updateCLSIdea(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea clsIdea)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateCLSIdea(clsIdea);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static void sendNotificationForTransition(
		java.util.ResourceBundle res, long ideaId,
		java.lang.String transictionName,
		javax.portlet.ActionRequest actionRequest) {
		getService()
			.sendNotificationForTransition(res, ideaId, transictionName,
			actionRequest);
	}

	/**
	* Send notifications in dockbar and emails
	*
	* @param azione Fase in cui avviene la notifica (puo' assumere: "nuovaIdea", "delete", "passaggioDiStato", "aggiuntaCollaboratore", "modificata" )
	*/
	public static void sendNotification(java.util.ResourceBundle res,
		java.lang.String textMessage, long destinationUderId,
		java.lang.String senderUserName, long ideaId,
		javax.portlet.ActionRequest actionRequest, java.lang.String azione)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		getService()
			.sendNotification(res, textMessage, destinationUderId,
			senderUserName, ideaId, actionRequest, azione);
	}

	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getIdeasByUserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getIdeasByUserId(userId);
	}

	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea getIdeasByIdeaId(
		long ideaId) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getIdeasByIdeaId(ideaId);
	}

	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getIdeasByChallengeId(
		long challengeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getIdeasByChallengeId(challengeId);
	}

	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getIdeasByMunicipalityId(
		long municipalityId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getIdeasByMunicipalityId(municipalityId);
	}

	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getIdeasByMunicipalityOrganizationId(
		long municipalityOrganizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .getIdeasByMunicipalityOrganizationId(municipalityOrganizationId);
	}

	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getIdeasByNeedId(
		long needId) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getIdeasByNeedId(needId);
	}

	/**
	* Return a list of all Needs
	*
	* @return
	* @throws SystemException
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getNeeds()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getNeeds();
	}

	/**
	* Return a list of all Needs of the Municipality Organization
	*
	* @param userId
	* @return
	* @throws SystemException
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getNeedsByMunicipalityOrganizationId(
		long municipalityOrganizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .getNeedsByMunicipalityOrganizationId(municipalityOrganizationId);
	}

	/**
	* Return a list of all Ideas of the Municipality Organization
	*
	* @param userId
	* @return
	* @throws SystemException
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getOnlyIdeasByMunicipalityOrganizationId(
		long municipalityOrganizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .getOnlyIdeasByMunicipalityOrganizationId(municipalityOrganizationId);
	}

	/**
	* Return a list of all Needs of the Municipality
	*
	* @param userId
	* @return
	* @throws SystemException
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getNeedsByMunicipalityId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getNeedsByMunicipalityId(userId);
	}

	/**
	* Return a list of all Ideas of the Municipality
	*
	* @param userId
	* @return
	* @throws SystemException
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getOnlyIdeasByMunicipalityId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getOnlyIdeasByMunicipalityId(userId);
	}

	/**
	* Return a list of all Needs of the user
	*
	* @param userId
	* @return
	* @throws SystemException
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getNeedsByUserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getNeedsByUserId(userId);
	}

	/**
	* Return a list of all Ideas, without need
	*
	* @return
	* @throws SystemException
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getOnlyIdeas()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getOnlyIdeas();
	}

	/**
	* Return a list of all Ideas (without need) of the user
	*
	* @return
	* @throws SystemException
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getOnlyIdeasByUserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getOnlyIdeasByUserId(userId);
	}

	public static void addCoworker(long indeaId,
		com.liferay.portal.model.User coworker) {
		getService().addCoworker(indeaId, coworker);
	}

	/**
	* @return
	*/
	public static com.liferay.portal.kernel.json.JSONObject doRightToBeForgottenIMS(
		long liferayUserId, boolean deleteAllContent,
		javax.portlet.ActionRequest actionRequest) {
		return getService()
				   .doRightToBeForgottenIMS(liferayUserId, deleteAllContent,
			actionRequest);
	}

	/**
	* @param pilot
	* @param isNeed
	* @param maxNumber
	* @return
	*/
	public static com.liferay.portal.kernel.json.JSONArray getTopRatedIdeasOrNeedsByPilot(
		java.lang.String pilot, boolean isNeed, int maxNumber,
		java.util.Locale locale) {
		return getService()
				   .getTopRatedIdeasOrNeedsByPilot(pilot, isNeed, maxNumber,
			locale);
	}

	/**
	* @param language_acronim (en,es, it, sr, fi)
	* @param isNeed
	* @param maxNumber
	* @return
	*/
	public static com.liferay.portal.kernel.json.JSONArray getTopRatedIdeasOrNeedsByLanguage(
		java.lang.String language, boolean isNeed, int maxNumber,
		java.util.Locale locale) {
		return getService()
				   .getTopRatedIdeasOrNeedsByLanguage(language, isNeed,
			maxNumber, locale);
	}

	/**
	* @param liferayUserId
	* @param isNeed
	* @param maxNumber
	* @return
	*/
	public static com.liferay.portal.kernel.json.JSONArray getIdeasOrNeedsByUserId(
		long liferayUserId, boolean isNeed, int maxNumber,
		java.util.Locale locale) {
		return getService()
				   .getIdeasOrNeedsByUserId(liferayUserId, isNeed, maxNumber,
			locale);
	}

	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea updateStatus(
		long userId, long resourcePrimKey, int wStatus,
		com.liferay.portal.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .updateStatus(userId, resourcePrimKey, wStatus,
			serviceContext);
	}

	/**
	* Access from other plugin
	*
	* @param properties
	* @return
	*/
	public static java.lang.String getIMSProperties(java.lang.String properties) {
		return getService().getIMSProperties(properties);
	}

	/**
	* Access from other plugin
	*
	* @param property
	* @return
	*/
	public static boolean getIMSEnabledProperties(java.lang.String property) {
		return getService().getIMSEnabledProperties(property);
	}

	public static void clearService() {
		_service = null;
	}

	public static CLSIdeaLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					CLSIdeaLocalService.class.getName());

			if (invokableLocalService instanceof CLSIdeaLocalService) {
				_service = (CLSIdeaLocalService)invokableLocalService;
			}
			else {
				_service = new CLSIdeaLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(CLSIdeaLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(CLSIdeaLocalService service) {
	}

	private static CLSIdeaLocalService _service;
}