/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 * @generated
 */
public class CLSCategoriesSetForChallengePK implements Comparable<CLSCategoriesSetForChallengePK>,
	Serializable {
	public long categoriesSetID;
	public long challengeId;

	public CLSCategoriesSetForChallengePK() {
	}

	public CLSCategoriesSetForChallengePK(long categoriesSetID, long challengeId) {
		this.categoriesSetID = categoriesSetID;
		this.challengeId = challengeId;
	}

	public long getCategoriesSetID() {
		return categoriesSetID;
	}

	public void setCategoriesSetID(long categoriesSetID) {
		this.categoriesSetID = categoriesSetID;
	}

	public long getChallengeId() {
		return challengeId;
	}

	public void setChallengeId(long challengeId) {
		this.challengeId = challengeId;
	}

	@Override
	public int compareTo(CLSCategoriesSetForChallengePK pk) {
		if (pk == null) {
			return -1;
		}

		int value = 0;

		if (categoriesSetID < pk.categoriesSetID) {
			value = -1;
		}
		else if (categoriesSetID > pk.categoriesSetID) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		if (challengeId < pk.challengeId) {
			value = -1;
		}
		else if (challengeId > pk.challengeId) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSCategoriesSetForChallengePK)) {
			return false;
		}

		CLSCategoriesSetForChallengePK pk = (CLSCategoriesSetForChallengePK)obj;

		if ((categoriesSetID == pk.categoriesSetID) &&
				(challengeId == pk.challengeId)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (String.valueOf(categoriesSetID) + String.valueOf(challengeId)).hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(10);

		sb.append(StringPool.OPEN_CURLY_BRACE);

		sb.append("categoriesSetID");
		sb.append(StringPool.EQUAL);
		sb.append(categoriesSetID);

		sb.append(StringPool.COMMA);
		sb.append(StringPool.SPACE);
		sb.append("challengeId");
		sb.append(StringPool.EQUAL);
		sb.append(challengeId);

		sb.append(StringPool.CLOSE_CURLY_BRACE);

		return sb.toString();
	}
}