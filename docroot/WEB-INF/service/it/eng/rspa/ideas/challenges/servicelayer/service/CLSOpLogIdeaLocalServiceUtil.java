/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for CLSOpLogIdea. This utility wraps
 * {@link it.eng.rspa.ideas.challenges.servicelayer.service.impl.CLSOpLogIdeaLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSOpLogIdeaLocalService
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.base.CLSOpLogIdeaLocalServiceBaseImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.impl.CLSOpLogIdeaLocalServiceImpl
 * @generated
 */
public class CLSOpLogIdeaLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.eng.rspa.ideas.challenges.servicelayer.service.impl.CLSOpLogIdeaLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the c l s op log idea to the database. Also notifies the appropriate model listeners.
	*
	* @param clsOpLogIdea the c l s op log idea
	* @return the c l s op log idea that was added
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea addCLSOpLogIdea(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea clsOpLogIdea)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addCLSOpLogIdea(clsOpLogIdea);
	}

	/**
	* Creates a new c l s op log idea with the primary key. Does not add the c l s op log idea to the database.
	*
	* @param opLogId the primary key for the new c l s op log idea
	* @return the new c l s op log idea
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea createCLSOpLogIdea(
		long opLogId) {
		return getService().createCLSOpLogIdea(opLogId);
	}

	/**
	* Deletes the c l s op log idea with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param opLogId the primary key of the c l s op log idea
	* @return the c l s op log idea that was removed
	* @throws PortalException if a c l s op log idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea deleteCLSOpLogIdea(
		long opLogId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteCLSOpLogIdea(opLogId);
	}

	/**
	* Deletes the c l s op log idea from the database. Also notifies the appropriate model listeners.
	*
	* @param clsOpLogIdea the c l s op log idea
	* @return the c l s op log idea that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea deleteCLSOpLogIdea(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea clsOpLogIdea)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteCLSOpLogIdea(clsOpLogIdea);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSOpLogIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSOpLogIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea fetchCLSOpLogIdea(
		long opLogId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchCLSOpLogIdea(opLogId);
	}

	/**
	* Returns the c l s op log idea with the primary key.
	*
	* @param opLogId the primary key of the c l s op log idea
	* @return the c l s op log idea
	* @throws PortalException if a c l s op log idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea getCLSOpLogIdea(
		long opLogId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getCLSOpLogIdea(opLogId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the c l s op log ideas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSOpLogIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s op log ideas
	* @param end the upper bound of the range of c l s op log ideas (not inclusive)
	* @return the range of c l s op log ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea> getCLSOpLogIdeas(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getCLSOpLogIdeas(start, end);
	}

	/**
	* Returns the number of c l s op log ideas.
	*
	* @return the number of c l s op log ideas
	* @throws SystemException if a system exception occurred
	*/
	public static int getCLSOpLogIdeasCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getCLSOpLogIdeasCount();
	}

	/**
	* Updates the c l s op log idea in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param clsOpLogIdea the c l s op log idea
	* @return the c l s op log idea that was updated
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea updateCLSOpLogIdea(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea clsOpLogIdea)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateCLSOpLogIdea(clsOpLogIdea);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea> getOpLogIdeaByIdeaId(
		long ideaId) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getOpLogIdeaByIdeaId(ideaId);
	}

	public static void clearService() {
		_service = null;
	}

	public static CLSOpLogIdeaLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					CLSOpLogIdeaLocalService.class.getName());

			if (invokableLocalService instanceof CLSOpLogIdeaLocalService) {
				_service = (CLSOpLogIdeaLocalService)invokableLocalService;
			}
			else {
				_service = new CLSOpLogIdeaLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(CLSOpLogIdeaLocalServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(CLSOpLogIdeaLocalService service) {
	}

	private static CLSOpLogIdeaLocalService _service;
}