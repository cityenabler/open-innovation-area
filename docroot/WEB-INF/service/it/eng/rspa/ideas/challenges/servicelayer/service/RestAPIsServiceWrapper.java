/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link RestAPIsService}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see RestAPIsService
 * @generated
 */
public class RestAPIsServiceWrapper implements RestAPIsService,
	ServiceWrapper<RestAPIsService> {
	public RestAPIsServiceWrapper(RestAPIsService restAPIsService) {
		_restAPIsService = restAPIsService;
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _restAPIsService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_restAPIsService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _restAPIsService.invokeMethod(name, parameterTypes, arguments);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONArray challenges() {
		return _restAPIsService.challenges();
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject onechallenge(long id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _restAPIsService.onechallenge(id);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONArray ideas() {
		return _restAPIsService.ideas();
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject oneidea(long id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _restAPIsService.oneidea(id);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONArray allNeeds() {
		return _restAPIsService.allNeeds();
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject oneneed(long id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _restAPIsService.oneneed(id);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONArray allNeedsOpen311() {
		return _restAPIsService.allNeedsOpen311();
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject oneneedOpen311(long id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _restAPIsService.oneneedOpen311(id);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONArray allPoisOpen311() {
		return _restAPIsService.allPoisOpen311();
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject onepoiOpen311(long id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _restAPIsService.onepoiOpen311(id);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONArray allPoisOpen311_V2() {
		return _restAPIsService.allPoisOpen311_V2();
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject onepoiOpen311_V2(long id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _restAPIsService.onepoiOpen311_V2(id);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public RestAPIsService getWrappedRestAPIsService() {
		return _restAPIsService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedRestAPIsService(RestAPIsService restAPIsService) {
		_restAPIsService = restAPIsService;
	}

	@Override
	public RestAPIsService getWrappedService() {
		return _restAPIsService;
	}

	@Override
	public void setWrappedService(RestAPIsService restAPIsService) {
		_restAPIsService = restAPIsService;
	}

	private RestAPIsService _restAPIsService;
}