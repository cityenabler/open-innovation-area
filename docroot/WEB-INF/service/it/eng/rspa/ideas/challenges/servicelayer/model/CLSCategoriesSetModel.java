/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import java.io.Serializable;

/**
 * The base model interface for the CLSCategoriesSet service. Represents a row in the &quot;CSL_CLSCategoriesSet&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCategoriesSetModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCategoriesSetImpl}.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSCategoriesSet
 * @see it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCategoriesSetImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCategoriesSetModelImpl
 * @generated
 */
public interface CLSCategoriesSetModel extends BaseModel<CLSCategoriesSet> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a c l s categories set model instance should use the {@link CLSCategoriesSet} interface instead.
	 */

	/**
	 * Returns the primary key of this c l s categories set.
	 *
	 * @return the primary key of this c l s categories set
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this c l s categories set.
	 *
	 * @param primaryKey the primary key of this c l s categories set
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the categories set i d of this c l s categories set.
	 *
	 * @return the categories set i d of this c l s categories set
	 */
	public long getCategoriesSetID();

	/**
	 * Sets the categories set i d of this c l s categories set.
	 *
	 * @param categoriesSetID the categories set i d of this c l s categories set
	 */
	public void setCategoriesSetID(long categoriesSetID);

	/**
	 * Returns the icon name of this c l s categories set.
	 *
	 * @return the icon name of this c l s categories set
	 */
	@AutoEscape
	public String getIconName();

	/**
	 * Sets the icon name of this c l s categories set.
	 *
	 * @param iconName the icon name of this c l s categories set
	 */
	public void setIconName(String iconName);

	/**
	 * Returns the colour name of this c l s categories set.
	 *
	 * @return the colour name of this c l s categories set
	 */
	@AutoEscape
	public String getColourName();

	/**
	 * Sets the colour name of this c l s categories set.
	 *
	 * @param colourName the colour name of this c l s categories set
	 */
	public void setColourName(String colourName);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet clsCategoriesSet);

	@Override
	public int hashCode();

	@Override
	public CacheModel<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet> toCacheModel();

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet toEscapedModel();

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}