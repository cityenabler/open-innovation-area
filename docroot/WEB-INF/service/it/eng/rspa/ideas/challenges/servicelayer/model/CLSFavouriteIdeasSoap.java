/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteIdeasPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.eng.rspa.ideas.challenges.servicelayer.service.http.CLSFavouriteIdeasServiceSoap}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.http.CLSFavouriteIdeasServiceSoap
 * @generated
 */
public class CLSFavouriteIdeasSoap implements Serializable {
	public static CLSFavouriteIdeasSoap toSoapModel(CLSFavouriteIdeas model) {
		CLSFavouriteIdeasSoap soapModel = new CLSFavouriteIdeasSoap();

		soapModel.setIdeaID(model.getIdeaID());
		soapModel.setUserId(model.getUserId());

		return soapModel;
	}

	public static CLSFavouriteIdeasSoap[] toSoapModels(
		CLSFavouriteIdeas[] models) {
		CLSFavouriteIdeasSoap[] soapModels = new CLSFavouriteIdeasSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CLSFavouriteIdeasSoap[][] toSoapModels(
		CLSFavouriteIdeas[][] models) {
		CLSFavouriteIdeasSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CLSFavouriteIdeasSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CLSFavouriteIdeasSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CLSFavouriteIdeasSoap[] toSoapModels(
		List<CLSFavouriteIdeas> models) {
		List<CLSFavouriteIdeasSoap> soapModels = new ArrayList<CLSFavouriteIdeasSoap>(models.size());

		for (CLSFavouriteIdeas model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CLSFavouriteIdeasSoap[soapModels.size()]);
	}

	public CLSFavouriteIdeasSoap() {
	}

	public CLSFavouriteIdeasPK getPrimaryKey() {
		return new CLSFavouriteIdeasPK(_ideaID, _userId);
	}

	public void setPrimaryKey(CLSFavouriteIdeasPK pk) {
		setIdeaID(pk.ideaID);
		setUserId(pk.userId);
	}

	public long getIdeaID() {
		return _ideaID;
	}

	public void setIdeaID(long ideaID) {
		_ideaID = ideaID;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	private long _ideaID;
	private long _userId;
}