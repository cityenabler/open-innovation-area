/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;

import java.util.List;

/**
 * The persistence utility for the c l s idea service. This utility wraps {@link CLSIdeaPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSIdeaPersistence
 * @see CLSIdeaPersistenceImpl
 * @generated
 */
public class CLSIdeaUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(CLSIdea clsIdea) {
		getPersistence().clearCache(clsIdea);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<CLSIdea> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<CLSIdea> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<CLSIdea> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static CLSIdea update(CLSIdea clsIdea) throws SystemException {
		return getPersistence().update(clsIdea);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static CLSIdea update(CLSIdea clsIdea, ServiceContext serviceContext)
		throws SystemException {
		return getPersistence().update(clsIdea, serviceContext);
	}

	/**
	* Returns all the c l s ideas where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> findByUuid(
		java.lang.String uuid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the c l s ideas where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of c l s ideas
	* @param end the upper bound of the range of c l s ideas (not inclusive)
	* @return the range of matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> findByUuid(
		java.lang.String uuid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the c l s ideas where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of c l s ideas
	* @param end the upper bound of the range of c l s ideas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> findByUuid(
		java.lang.String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns the first c l s idea in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea findByUuid_First(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first c l s idea in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea fetchByUuid_First(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last c l s idea in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea findByUuid_Last(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last c l s idea in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea fetchByUuid_Last(
		java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the c l s ideas before and after the current c l s idea in the ordered set where uuid = &#63;.
	*
	* @param ideaID the primary key of the current c l s idea
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next c l s idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a c l s idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea[] findByUuid_PrevAndNext(
		long ideaID, java.lang.String uuid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException {
		return getPersistence()
				   .findByUuid_PrevAndNext(ideaID, uuid, orderByComparator);
	}

	/**
	* Removes all the c l s ideas where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByUuid(java.lang.String uuid)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of c l s ideas where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static int countByUuid(java.lang.String uuid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns the c l s idea where uuid = &#63; and groupId = &#63; or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching c l s idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea findByUUID_G(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException {
		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	* Returns the c l s idea where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea fetchByUUID_G(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	* Returns the c l s idea where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea fetchByUUID_G(
		java.lang.String uuid, long groupId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByUUID_G(uuid, groupId, retrieveFromCache);
	}

	/**
	* Removes the c l s idea where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the c l s idea that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea removeByUUID_G(
		java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException {
		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	* Returns the number of c l s ideas where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static int countByUUID_G(java.lang.String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	* Returns all the c l s ideas where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> findByUuid_C(
		java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the c l s ideas where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of c l s ideas
	* @param end the upper bound of the range of c l s ideas (not inclusive)
	* @return the range of matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the c l s ideas where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of c l s ideas
	* @param end the upper bound of the range of c l s ideas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> findByUuid_C(
		java.lang.String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns the first c l s idea in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea findByUuid_C_First(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first c l s idea in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea fetchByUuid_C_First(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last c l s idea in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea findByUuid_C_Last(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last c l s idea in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea fetchByUuid_C_Last(
		java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the c l s ideas before and after the current c l s idea in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param ideaID the primary key of the current c l s idea
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next c l s idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a c l s idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea[] findByUuid_C_PrevAndNext(
		long ideaID, java.lang.String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(ideaID, uuid, companyId,
			orderByComparator);
	}

	/**
	* Removes all the c l s ideas where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByUuid_C(java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of c l s ideas where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static int countByUuid_C(java.lang.String uuid, long companyId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Returns all the c l s ideas where ideaTitle = &#63;.
	*
	* @param ideaTitle the idea title
	* @return the matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> findByIdeaTitle(
		java.lang.String ideaTitle)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByIdeaTitle(ideaTitle);
	}

	/**
	* Returns a range of all the c l s ideas where ideaTitle = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaTitle the idea title
	* @param start the lower bound of the range of c l s ideas
	* @param end the upper bound of the range of c l s ideas (not inclusive)
	* @return the range of matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> findByIdeaTitle(
		java.lang.String ideaTitle, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByIdeaTitle(ideaTitle, start, end);
	}

	/**
	* Returns an ordered range of all the c l s ideas where ideaTitle = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaTitle the idea title
	* @param start the lower bound of the range of c l s ideas
	* @param end the upper bound of the range of c l s ideas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> findByIdeaTitle(
		java.lang.String ideaTitle, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByIdeaTitle(ideaTitle, start, end, orderByComparator);
	}

	/**
	* Returns the first c l s idea in the ordered set where ideaTitle = &#63;.
	*
	* @param ideaTitle the idea title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea findByIdeaTitle_First(
		java.lang.String ideaTitle,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException {
		return getPersistence()
				   .findByIdeaTitle_First(ideaTitle, orderByComparator);
	}

	/**
	* Returns the first c l s idea in the ordered set where ideaTitle = &#63;.
	*
	* @param ideaTitle the idea title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea fetchByIdeaTitle_First(
		java.lang.String ideaTitle,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByIdeaTitle_First(ideaTitle, orderByComparator);
	}

	/**
	* Returns the last c l s idea in the ordered set where ideaTitle = &#63;.
	*
	* @param ideaTitle the idea title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea findByIdeaTitle_Last(
		java.lang.String ideaTitle,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException {
		return getPersistence()
				   .findByIdeaTitle_Last(ideaTitle, orderByComparator);
	}

	/**
	* Returns the last c l s idea in the ordered set where ideaTitle = &#63;.
	*
	* @param ideaTitle the idea title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea fetchByIdeaTitle_Last(
		java.lang.String ideaTitle,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByIdeaTitle_Last(ideaTitle, orderByComparator);
	}

	/**
	* Returns the c l s ideas before and after the current c l s idea in the ordered set where ideaTitle = &#63;.
	*
	* @param ideaID the primary key of the current c l s idea
	* @param ideaTitle the idea title
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next c l s idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a c l s idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea[] findByIdeaTitle_PrevAndNext(
		long ideaID, java.lang.String ideaTitle,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException {
		return getPersistence()
				   .findByIdeaTitle_PrevAndNext(ideaID, ideaTitle,
			orderByComparator);
	}

	/**
	* Removes all the c l s ideas where ideaTitle = &#63; from the database.
	*
	* @param ideaTitle the idea title
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByIdeaTitle(java.lang.String ideaTitle)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByIdeaTitle(ideaTitle);
	}

	/**
	* Returns the number of c l s ideas where ideaTitle = &#63;.
	*
	* @param ideaTitle the idea title
	* @return the number of matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static int countByIdeaTitle(java.lang.String ideaTitle)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByIdeaTitle(ideaTitle);
	}

	/**
	* Returns all the c l s ideas where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> findByIdeaUserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByIdeaUserId(userId);
	}

	/**
	* Returns a range of all the c l s ideas where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of c l s ideas
	* @param end the upper bound of the range of c l s ideas (not inclusive)
	* @return the range of matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> findByIdeaUserId(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByIdeaUserId(userId, start, end);
	}

	/**
	* Returns an ordered range of all the c l s ideas where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of c l s ideas
	* @param end the upper bound of the range of c l s ideas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> findByIdeaUserId(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByIdeaUserId(userId, start, end, orderByComparator);
	}

	/**
	* Returns the first c l s idea in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea findByIdeaUserId_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException {
		return getPersistence().findByIdeaUserId_First(userId, orderByComparator);
	}

	/**
	* Returns the first c l s idea in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea fetchByIdeaUserId_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByIdeaUserId_First(userId, orderByComparator);
	}

	/**
	* Returns the last c l s idea in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea findByIdeaUserId_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException {
		return getPersistence().findByIdeaUserId_Last(userId, orderByComparator);
	}

	/**
	* Returns the last c l s idea in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea fetchByIdeaUserId_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByIdeaUserId_Last(userId, orderByComparator);
	}

	/**
	* Returns the c l s ideas before and after the current c l s idea in the ordered set where userId = &#63;.
	*
	* @param ideaID the primary key of the current c l s idea
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next c l s idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a c l s idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea[] findByIdeaUserId_PrevAndNext(
		long ideaID, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException {
		return getPersistence()
				   .findByIdeaUserId_PrevAndNext(ideaID, userId,
			orderByComparator);
	}

	/**
	* Removes all the c l s ideas where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByIdeaUserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByIdeaUserId(userId);
	}

	/**
	* Returns the number of c l s ideas where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static int countByIdeaUserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByIdeaUserId(userId);
	}

	/**
	* Returns all the c l s ideas where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @return the matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> findByIdeaChallengeId(
		long challengeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByIdeaChallengeId(challengeId);
	}

	/**
	* Returns a range of all the c l s ideas where challengeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param challengeId the challenge ID
	* @param start the lower bound of the range of c l s ideas
	* @param end the upper bound of the range of c l s ideas (not inclusive)
	* @return the range of matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> findByIdeaChallengeId(
		long challengeId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByIdeaChallengeId(challengeId, start, end);
	}

	/**
	* Returns an ordered range of all the c l s ideas where challengeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param challengeId the challenge ID
	* @param start the lower bound of the range of c l s ideas
	* @param end the upper bound of the range of c l s ideas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> findByIdeaChallengeId(
		long challengeId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByIdeaChallengeId(challengeId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first c l s idea in the ordered set where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea findByIdeaChallengeId_First(
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException {
		return getPersistence()
				   .findByIdeaChallengeId_First(challengeId, orderByComparator);
	}

	/**
	* Returns the first c l s idea in the ordered set where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea fetchByIdeaChallengeId_First(
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByIdeaChallengeId_First(challengeId, orderByComparator);
	}

	/**
	* Returns the last c l s idea in the ordered set where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea findByIdeaChallengeId_Last(
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException {
		return getPersistence()
				   .findByIdeaChallengeId_Last(challengeId, orderByComparator);
	}

	/**
	* Returns the last c l s idea in the ordered set where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea fetchByIdeaChallengeId_Last(
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByIdeaChallengeId_Last(challengeId, orderByComparator);
	}

	/**
	* Returns the c l s ideas before and after the current c l s idea in the ordered set where challengeId = &#63;.
	*
	* @param ideaID the primary key of the current c l s idea
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next c l s idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a c l s idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea[] findByIdeaChallengeId_PrevAndNext(
		long ideaID, long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException {
		return getPersistence()
				   .findByIdeaChallengeId_PrevAndNext(ideaID, challengeId,
			orderByComparator);
	}

	/**
	* Removes all the c l s ideas where challengeId = &#63; from the database.
	*
	* @param challengeId the challenge ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByIdeaChallengeId(long challengeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByIdeaChallengeId(challengeId);
	}

	/**
	* Returns the number of c l s ideas where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @return the number of matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static int countByIdeaChallengeId(long challengeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByIdeaChallengeId(challengeId);
	}

	/**
	* Returns all the c l s ideas where needId = &#63;.
	*
	* @param needId the need ID
	* @return the matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> findByIdeaNeedId(
		long needId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByIdeaNeedId(needId);
	}

	/**
	* Returns a range of all the c l s ideas where needId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param needId the need ID
	* @param start the lower bound of the range of c l s ideas
	* @param end the upper bound of the range of c l s ideas (not inclusive)
	* @return the range of matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> findByIdeaNeedId(
		long needId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByIdeaNeedId(needId, start, end);
	}

	/**
	* Returns an ordered range of all the c l s ideas where needId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param needId the need ID
	* @param start the lower bound of the range of c l s ideas
	* @param end the upper bound of the range of c l s ideas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> findByIdeaNeedId(
		long needId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByIdeaNeedId(needId, start, end, orderByComparator);
	}

	/**
	* Returns the first c l s idea in the ordered set where needId = &#63;.
	*
	* @param needId the need ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea findByIdeaNeedId_First(
		long needId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException {
		return getPersistence().findByIdeaNeedId_First(needId, orderByComparator);
	}

	/**
	* Returns the first c l s idea in the ordered set where needId = &#63;.
	*
	* @param needId the need ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea fetchByIdeaNeedId_First(
		long needId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByIdeaNeedId_First(needId, orderByComparator);
	}

	/**
	* Returns the last c l s idea in the ordered set where needId = &#63;.
	*
	* @param needId the need ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea findByIdeaNeedId_Last(
		long needId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException {
		return getPersistence().findByIdeaNeedId_Last(needId, orderByComparator);
	}

	/**
	* Returns the last c l s idea in the ordered set where needId = &#63;.
	*
	* @param needId the need ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea fetchByIdeaNeedId_Last(
		long needId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByIdeaNeedId_Last(needId, orderByComparator);
	}

	/**
	* Returns the c l s ideas before and after the current c l s idea in the ordered set where needId = &#63;.
	*
	* @param ideaID the primary key of the current c l s idea
	* @param needId the need ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next c l s idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a c l s idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea[] findByIdeaNeedId_PrevAndNext(
		long ideaID, long needId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException {
		return getPersistence()
				   .findByIdeaNeedId_PrevAndNext(ideaID, needId,
			orderByComparator);
	}

	/**
	* Removes all the c l s ideas where needId = &#63; from the database.
	*
	* @param needId the need ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByIdeaNeedId(long needId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByIdeaNeedId(needId);
	}

	/**
	* Returns the number of c l s ideas where needId = &#63;.
	*
	* @param needId the need ID
	* @return the number of matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static int countByIdeaNeedId(long needId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByIdeaNeedId(needId);
	}

	/**
	* Returns all the c l s ideas where municipalityId = &#63;.
	*
	* @param municipalityId the municipality ID
	* @return the matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> findByIdeaMunicipalityId(
		long municipalityId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByIdeaMunicipalityId(municipalityId);
	}

	/**
	* Returns a range of all the c l s ideas where municipalityId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param municipalityId the municipality ID
	* @param start the lower bound of the range of c l s ideas
	* @param end the upper bound of the range of c l s ideas (not inclusive)
	* @return the range of matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> findByIdeaMunicipalityId(
		long municipalityId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByIdeaMunicipalityId(municipalityId, start, end);
	}

	/**
	* Returns an ordered range of all the c l s ideas where municipalityId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param municipalityId the municipality ID
	* @param start the lower bound of the range of c l s ideas
	* @param end the upper bound of the range of c l s ideas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> findByIdeaMunicipalityId(
		long municipalityId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByIdeaMunicipalityId(municipalityId, start, end,
			orderByComparator);
	}

	/**
	* Returns the first c l s idea in the ordered set where municipalityId = &#63;.
	*
	* @param municipalityId the municipality ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea findByIdeaMunicipalityId_First(
		long municipalityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException {
		return getPersistence()
				   .findByIdeaMunicipalityId_First(municipalityId,
			orderByComparator);
	}

	/**
	* Returns the first c l s idea in the ordered set where municipalityId = &#63;.
	*
	* @param municipalityId the municipality ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea fetchByIdeaMunicipalityId_First(
		long municipalityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByIdeaMunicipalityId_First(municipalityId,
			orderByComparator);
	}

	/**
	* Returns the last c l s idea in the ordered set where municipalityId = &#63;.
	*
	* @param municipalityId the municipality ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea findByIdeaMunicipalityId_Last(
		long municipalityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException {
		return getPersistence()
				   .findByIdeaMunicipalityId_Last(municipalityId,
			orderByComparator);
	}

	/**
	* Returns the last c l s idea in the ordered set where municipalityId = &#63;.
	*
	* @param municipalityId the municipality ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea fetchByIdeaMunicipalityId_Last(
		long municipalityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByIdeaMunicipalityId_Last(municipalityId,
			orderByComparator);
	}

	/**
	* Returns the c l s ideas before and after the current c l s idea in the ordered set where municipalityId = &#63;.
	*
	* @param ideaID the primary key of the current c l s idea
	* @param municipalityId the municipality ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next c l s idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a c l s idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea[] findByIdeaMunicipalityId_PrevAndNext(
		long ideaID, long municipalityId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException {
		return getPersistence()
				   .findByIdeaMunicipalityId_PrevAndNext(ideaID,
			municipalityId, orderByComparator);
	}

	/**
	* Removes all the c l s ideas where municipalityId = &#63; from the database.
	*
	* @param municipalityId the municipality ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByIdeaMunicipalityId(long municipalityId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByIdeaMunicipalityId(municipalityId);
	}

	/**
	* Returns the number of c l s ideas where municipalityId = &#63;.
	*
	* @param municipalityId the municipality ID
	* @return the number of matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static int countByIdeaMunicipalityId(long municipalityId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByIdeaMunicipalityId(municipalityId);
	}

	/**
	* Returns all the c l s ideas where municipalityOrganizationId = &#63;.
	*
	* @param municipalityOrganizationId the municipality organization ID
	* @return the matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> findByIdeaMunicipalityOrganizationId(
		long municipalityOrganizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByIdeaMunicipalityOrganizationId(municipalityOrganizationId);
	}

	/**
	* Returns a range of all the c l s ideas where municipalityOrganizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param municipalityOrganizationId the municipality organization ID
	* @param start the lower bound of the range of c l s ideas
	* @param end the upper bound of the range of c l s ideas (not inclusive)
	* @return the range of matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> findByIdeaMunicipalityOrganizationId(
		long municipalityOrganizationId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByIdeaMunicipalityOrganizationId(municipalityOrganizationId,
			start, end);
	}

	/**
	* Returns an ordered range of all the c l s ideas where municipalityOrganizationId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param municipalityOrganizationId the municipality organization ID
	* @param start the lower bound of the range of c l s ideas
	* @param end the upper bound of the range of c l s ideas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> findByIdeaMunicipalityOrganizationId(
		long municipalityOrganizationId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByIdeaMunicipalityOrganizationId(municipalityOrganizationId,
			start, end, orderByComparator);
	}

	/**
	* Returns the first c l s idea in the ordered set where municipalityOrganizationId = &#63;.
	*
	* @param municipalityOrganizationId the municipality organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea findByIdeaMunicipalityOrganizationId_First(
		long municipalityOrganizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException {
		return getPersistence()
				   .findByIdeaMunicipalityOrganizationId_First(municipalityOrganizationId,
			orderByComparator);
	}

	/**
	* Returns the first c l s idea in the ordered set where municipalityOrganizationId = &#63;.
	*
	* @param municipalityOrganizationId the municipality organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea fetchByIdeaMunicipalityOrganizationId_First(
		long municipalityOrganizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByIdeaMunicipalityOrganizationId_First(municipalityOrganizationId,
			orderByComparator);
	}

	/**
	* Returns the last c l s idea in the ordered set where municipalityOrganizationId = &#63;.
	*
	* @param municipalityOrganizationId the municipality organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea findByIdeaMunicipalityOrganizationId_Last(
		long municipalityOrganizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException {
		return getPersistence()
				   .findByIdeaMunicipalityOrganizationId_Last(municipalityOrganizationId,
			orderByComparator);
	}

	/**
	* Returns the last c l s idea in the ordered set where municipalityOrganizationId = &#63;.
	*
	* @param municipalityOrganizationId the municipality organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea fetchByIdeaMunicipalityOrganizationId_Last(
		long municipalityOrganizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByIdeaMunicipalityOrganizationId_Last(municipalityOrganizationId,
			orderByComparator);
	}

	/**
	* Returns the c l s ideas before and after the current c l s idea in the ordered set where municipalityOrganizationId = &#63;.
	*
	* @param ideaID the primary key of the current c l s idea
	* @param municipalityOrganizationId the municipality organization ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next c l s idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a c l s idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea[] findByIdeaMunicipalityOrganizationId_PrevAndNext(
		long ideaID, long municipalityOrganizationId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException {
		return getPersistence()
				   .findByIdeaMunicipalityOrganizationId_PrevAndNext(ideaID,
			municipalityOrganizationId, orderByComparator);
	}

	/**
	* Removes all the c l s ideas where municipalityOrganizationId = &#63; from the database.
	*
	* @param municipalityOrganizationId the municipality organization ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByIdeaMunicipalityOrganizationId(
		long municipalityOrganizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence()
			.removeByIdeaMunicipalityOrganizationId(municipalityOrganizationId);
	}

	/**
	* Returns the number of c l s ideas where municipalityOrganizationId = &#63;.
	*
	* @param municipalityOrganizationId the municipality organization ID
	* @return the number of matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static int countByIdeaMunicipalityOrganizationId(
		long municipalityOrganizationId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .countByIdeaMunicipalityOrganizationId(municipalityOrganizationId);
	}

	/**
	* Returns the c l s idea where ideaID = &#63; or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException} if it could not be found.
	*
	* @param ideaID the idea i d
	* @return the matching c l s idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea findByIdeaId(
		long ideaID)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException {
		return getPersistence().findByIdeaId(ideaID);
	}

	/**
	* Returns the c l s idea where ideaID = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param ideaID the idea i d
	* @return the matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea fetchByIdeaId(
		long ideaID) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByIdeaId(ideaID);
	}

	/**
	* Returns the c l s idea where ideaID = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param ideaID the idea i d
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea fetchByIdeaId(
		long ideaID, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByIdeaId(ideaID, retrieveFromCache);
	}

	/**
	* Removes the c l s idea where ideaID = &#63; from the database.
	*
	* @param ideaID the idea i d
	* @return the c l s idea that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea removeByIdeaId(
		long ideaID)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException {
		return getPersistence().removeByIdeaId(ideaID);
	}

	/**
	* Returns the number of c l s ideas where ideaID = &#63;.
	*
	* @param ideaID the idea i d
	* @return the number of matching c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static int countByIdeaId(long ideaID)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByIdeaId(ideaID);
	}

	/**
	* Caches the c l s idea in the entity cache if it is enabled.
	*
	* @param clsIdea the c l s idea
	*/
	public static void cacheResult(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea clsIdea) {
		getPersistence().cacheResult(clsIdea);
	}

	/**
	* Caches the c l s ideas in the entity cache if it is enabled.
	*
	* @param clsIdeas the c l s ideas
	*/
	public static void cacheResult(
		java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> clsIdeas) {
		getPersistence().cacheResult(clsIdeas);
	}

	/**
	* Creates a new c l s idea with the primary key. Does not add the c l s idea to the database.
	*
	* @param ideaID the primary key for the new c l s idea
	* @return the new c l s idea
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea create(
		long ideaID) {
		return getPersistence().create(ideaID);
	}

	/**
	* Removes the c l s idea with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ideaID the primary key of the c l s idea
	* @return the c l s idea that was removed
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a c l s idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea remove(
		long ideaID)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException {
		return getPersistence().remove(ideaID);
	}

	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea clsIdea)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(clsIdea);
	}

	/**
	* Returns the c l s idea with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException} if it could not be found.
	*
	* @param ideaID the primary key of the c l s idea
	* @return the c l s idea
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a c l s idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea findByPrimaryKey(
		long ideaID)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException {
		return getPersistence().findByPrimaryKey(ideaID);
	}

	/**
	* Returns the c l s idea with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ideaID the primary key of the c l s idea
	* @return the c l s idea, or <code>null</code> if a c l s idea with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea fetchByPrimaryKey(
		long ideaID) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(ideaID);
	}

	/**
	* Returns all the c l s ideas.
	*
	* @return the c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the c l s ideas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s ideas
	* @param end the upper bound of the range of c l s ideas (not inclusive)
	* @return the range of c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the c l s ideas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s ideas
	* @param end the upper bound of the range of c l s ideas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the c l s ideas from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of c l s ideas.
	*
	* @return the number of c l s ideas
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static CLSIdeaPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (CLSIdeaPersistence)PortletBeanLocatorUtil.locate(it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.getServletContextName(),
					CLSIdeaPersistence.class.getName());

			ReferenceRegistry.registerReference(CLSIdeaUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(CLSIdeaPersistence persistence) {
	}

	private static CLSIdeaPersistence _persistence;
}