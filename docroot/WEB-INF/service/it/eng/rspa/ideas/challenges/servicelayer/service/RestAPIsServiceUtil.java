/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableService;

/**
 * Provides the remote service utility for RestAPIs. This utility wraps
 * {@link it.eng.rspa.ideas.challenges.servicelayer.service.impl.RestAPIsServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see RestAPIsService
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.base.RestAPIsServiceBaseImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.impl.RestAPIsServiceImpl
 * @generated
 */
public class RestAPIsServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link it.eng.rspa.ideas.challenges.servicelayer.service.impl.RestAPIsServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static com.liferay.portal.kernel.json.JSONArray challenges() {
		return getService().challenges();
	}

	public static com.liferay.portal.kernel.json.JSONObject onechallenge(
		long id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().onechallenge(id);
	}

	public static com.liferay.portal.kernel.json.JSONArray ideas() {
		return getService().ideas();
	}

	public static com.liferay.portal.kernel.json.JSONObject oneidea(long id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().oneidea(id);
	}

	public static com.liferay.portal.kernel.json.JSONArray allNeeds() {
		return getService().allNeeds();
	}

	public static com.liferay.portal.kernel.json.JSONObject oneneed(long id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().oneneed(id);
	}

	public static com.liferay.portal.kernel.json.JSONArray allNeedsOpen311() {
		return getService().allNeedsOpen311();
	}

	public static com.liferay.portal.kernel.json.JSONObject oneneedOpen311(
		long id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().oneneedOpen311(id);
	}

	public static com.liferay.portal.kernel.json.JSONArray allPoisOpen311() {
		return getService().allPoisOpen311();
	}

	public static com.liferay.portal.kernel.json.JSONObject onepoiOpen311(
		long id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().onepoiOpen311(id);
	}

	public static com.liferay.portal.kernel.json.JSONArray allPoisOpen311_V2() {
		return getService().allPoisOpen311_V2();
	}

	public static com.liferay.portal.kernel.json.JSONObject onepoiOpen311_V2(
		long id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().onepoiOpen311_V2(id);
	}

	public static void clearService() {
		_service = null;
	}

	public static RestAPIsService getService() {
		if (_service == null) {
			InvokableService invokableService = (InvokableService)PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					RestAPIsService.class.getName());

			if (invokableService instanceof RestAPIsService) {
				_service = (RestAPIsService)invokableService;
			}
			else {
				_service = new RestAPIsServiceClp(invokableService);
			}

			ReferenceRegistry.registerReference(RestAPIsServiceUtil.class,
				"_service");
		}

		return _service;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setService(RestAPIsService service) {
	}

	private static RestAPIsService _service;
}