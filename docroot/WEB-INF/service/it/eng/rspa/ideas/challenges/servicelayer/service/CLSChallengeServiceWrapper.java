/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CLSChallengeService}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSChallengeService
 * @generated
 */
public class CLSChallengeServiceWrapper implements CLSChallengeService,
	ServiceWrapper<CLSChallengeService> {
	public CLSChallengeServiceWrapper(CLSChallengeService clsChallengeService) {
		_clsChallengeService = clsChallengeService;
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _clsChallengeService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_clsChallengeService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _clsChallengeService.invokeMethod(name, parameterTypes, arguments);
	}

	/**
	* @param fileEntryId
	
	questo metodo viene utilizzato per eliminare gli allegati con Ajax
	*/
	@Override
	public void deleteChallengeFile(long fileEntryId) {
		_clsChallengeService.deleteChallengeFile(fileEntryId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public CLSChallengeService getWrappedCLSChallengeService() {
		return _clsChallengeService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedCLSChallengeService(
		CLSChallengeService clsChallengeService) {
		_clsChallengeService = clsChallengeService;
	}

	@Override
	public CLSChallengeService getWrappedService() {
		return _clsChallengeService;
	}

	@Override
	public void setWrappedService(CLSChallengeService clsChallengeService) {
		_clsChallengeService = clsChallengeService;
	}

	private CLSChallengeService _clsChallengeService;
}