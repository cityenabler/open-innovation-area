/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge;

import java.util.List;

/**
 * The persistence utility for the c l s categories set for challenge service. This utility wraps {@link CLSCategoriesSetForChallengePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSCategoriesSetForChallengePersistence
 * @see CLSCategoriesSetForChallengePersistenceImpl
 * @generated
 */
public class CLSCategoriesSetForChallengeUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(
		CLSCategoriesSetForChallenge clsCategoriesSetForChallenge) {
		getPersistence().clearCache(clsCategoriesSetForChallenge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<CLSCategoriesSetForChallenge> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<CLSCategoriesSetForChallenge> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<CLSCategoriesSetForChallenge> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static CLSCategoriesSetForChallenge update(
		CLSCategoriesSetForChallenge clsCategoriesSetForChallenge)
		throws SystemException {
		return getPersistence().update(clsCategoriesSetForChallenge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static CLSCategoriesSetForChallenge update(
		CLSCategoriesSetForChallenge clsCategoriesSetForChallenge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence()
				   .update(clsCategoriesSetForChallenge, serviceContext);
	}

	/**
	* Returns all the c l s categories set for challenges where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @return the matching c l s categories set for challenges
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge> findByChallengeId(
		long challengeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByChallengeId(challengeId);
	}

	/**
	* Returns a range of all the c l s categories set for challenges where challengeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCategoriesSetForChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param challengeId the challenge ID
	* @param start the lower bound of the range of c l s categories set for challenges
	* @param end the upper bound of the range of c l s categories set for challenges (not inclusive)
	* @return the range of matching c l s categories set for challenges
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge> findByChallengeId(
		long challengeId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByChallengeId(challengeId, start, end);
	}

	/**
	* Returns an ordered range of all the c l s categories set for challenges where challengeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCategoriesSetForChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param challengeId the challenge ID
	* @param start the lower bound of the range of c l s categories set for challenges
	* @param end the upper bound of the range of c l s categories set for challenges (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching c l s categories set for challenges
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge> findByChallengeId(
		long challengeId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByChallengeId(challengeId, start, end, orderByComparator);
	}

	/**
	* Returns the first c l s categories set for challenge in the ordered set where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s categories set for challenge
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException if a matching c l s categories set for challenge could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge findByChallengeId_First(
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException {
		return getPersistence()
				   .findByChallengeId_First(challengeId, orderByComparator);
	}

	/**
	* Returns the first c l s categories set for challenge in the ordered set where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s categories set for challenge, or <code>null</code> if a matching c l s categories set for challenge could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge fetchByChallengeId_First(
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByChallengeId_First(challengeId, orderByComparator);
	}

	/**
	* Returns the last c l s categories set for challenge in the ordered set where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s categories set for challenge
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException if a matching c l s categories set for challenge could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge findByChallengeId_Last(
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException {
		return getPersistence()
				   .findByChallengeId_Last(challengeId, orderByComparator);
	}

	/**
	* Returns the last c l s categories set for challenge in the ordered set where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s categories set for challenge, or <code>null</code> if a matching c l s categories set for challenge could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge fetchByChallengeId_Last(
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByChallengeId_Last(challengeId, orderByComparator);
	}

	/**
	* Returns the c l s categories set for challenges before and after the current c l s categories set for challenge in the ordered set where challengeId = &#63;.
	*
	* @param clsCategoriesSetForChallengePK the primary key of the current c l s categories set for challenge
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next c l s categories set for challenge
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException if a c l s categories set for challenge with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge[] findByChallengeId_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSCategoriesSetForChallengePK clsCategoriesSetForChallengePK,
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException {
		return getPersistence()
				   .findByChallengeId_PrevAndNext(clsCategoriesSetForChallengePK,
			challengeId, orderByComparator);
	}

	/**
	* Removes all the c l s categories set for challenges where challengeId = &#63; from the database.
	*
	* @param challengeId the challenge ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByChallengeId(long challengeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByChallengeId(challengeId);
	}

	/**
	* Returns the number of c l s categories set for challenges where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @return the number of matching c l s categories set for challenges
	* @throws SystemException if a system exception occurred
	*/
	public static int countByChallengeId(long challengeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByChallengeId(challengeId);
	}

	/**
	* Returns all the c l s categories set for challenges where categoriesSetID = &#63;.
	*
	* @param categoriesSetID the categories set i d
	* @return the matching c l s categories set for challenges
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge> findByCategorySetID(
		long categoriesSetID)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCategorySetID(categoriesSetID);
	}

	/**
	* Returns a range of all the c l s categories set for challenges where categoriesSetID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCategoriesSetForChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param categoriesSetID the categories set i d
	* @param start the lower bound of the range of c l s categories set for challenges
	* @param end the upper bound of the range of c l s categories set for challenges (not inclusive)
	* @return the range of matching c l s categories set for challenges
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge> findByCategorySetID(
		long categoriesSetID, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCategorySetID(categoriesSetID, start, end);
	}

	/**
	* Returns an ordered range of all the c l s categories set for challenges where categoriesSetID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCategoriesSetForChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param categoriesSetID the categories set i d
	* @param start the lower bound of the range of c l s categories set for challenges
	* @param end the upper bound of the range of c l s categories set for challenges (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching c l s categories set for challenges
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge> findByCategorySetID(
		long categoriesSetID, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCategorySetID(categoriesSetID, start, end,
			orderByComparator);
	}

	/**
	* Returns the first c l s categories set for challenge in the ordered set where categoriesSetID = &#63;.
	*
	* @param categoriesSetID the categories set i d
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s categories set for challenge
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException if a matching c l s categories set for challenge could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge findByCategorySetID_First(
		long categoriesSetID,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException {
		return getPersistence()
				   .findByCategorySetID_First(categoriesSetID, orderByComparator);
	}

	/**
	* Returns the first c l s categories set for challenge in the ordered set where categoriesSetID = &#63;.
	*
	* @param categoriesSetID the categories set i d
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s categories set for challenge, or <code>null</code> if a matching c l s categories set for challenge could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge fetchByCategorySetID_First(
		long categoriesSetID,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByCategorySetID_First(categoriesSetID,
			orderByComparator);
	}

	/**
	* Returns the last c l s categories set for challenge in the ordered set where categoriesSetID = &#63;.
	*
	* @param categoriesSetID the categories set i d
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s categories set for challenge
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException if a matching c l s categories set for challenge could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge findByCategorySetID_Last(
		long categoriesSetID,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException {
		return getPersistence()
				   .findByCategorySetID_Last(categoriesSetID, orderByComparator);
	}

	/**
	* Returns the last c l s categories set for challenge in the ordered set where categoriesSetID = &#63;.
	*
	* @param categoriesSetID the categories set i d
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s categories set for challenge, or <code>null</code> if a matching c l s categories set for challenge could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge fetchByCategorySetID_Last(
		long categoriesSetID,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByCategorySetID_Last(categoriesSetID, orderByComparator);
	}

	/**
	* Returns the c l s categories set for challenges before and after the current c l s categories set for challenge in the ordered set where categoriesSetID = &#63;.
	*
	* @param clsCategoriesSetForChallengePK the primary key of the current c l s categories set for challenge
	* @param categoriesSetID the categories set i d
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next c l s categories set for challenge
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException if a c l s categories set for challenge with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge[] findByCategorySetID_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSCategoriesSetForChallengePK clsCategoriesSetForChallengePK,
		long categoriesSetID,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException {
		return getPersistence()
				   .findByCategorySetID_PrevAndNext(clsCategoriesSetForChallengePK,
			categoriesSetID, orderByComparator);
	}

	/**
	* Removes all the c l s categories set for challenges where categoriesSetID = &#63; from the database.
	*
	* @param categoriesSetID the categories set i d
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByCategorySetID(long categoriesSetID)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByCategorySetID(categoriesSetID);
	}

	/**
	* Returns the number of c l s categories set for challenges where categoriesSetID = &#63;.
	*
	* @param categoriesSetID the categories set i d
	* @return the number of matching c l s categories set for challenges
	* @throws SystemException if a system exception occurred
	*/
	public static int countByCategorySetID(long categoriesSetID)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByCategorySetID(categoriesSetID);
	}

	/**
	* Caches the c l s categories set for challenge in the entity cache if it is enabled.
	*
	* @param clsCategoriesSetForChallenge the c l s categories set for challenge
	*/
	public static void cacheResult(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge clsCategoriesSetForChallenge) {
		getPersistence().cacheResult(clsCategoriesSetForChallenge);
	}

	/**
	* Caches the c l s categories set for challenges in the entity cache if it is enabled.
	*
	* @param clsCategoriesSetForChallenges the c l s categories set for challenges
	*/
	public static void cacheResult(
		java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge> clsCategoriesSetForChallenges) {
		getPersistence().cacheResult(clsCategoriesSetForChallenges);
	}

	/**
	* Creates a new c l s categories set for challenge with the primary key. Does not add the c l s categories set for challenge to the database.
	*
	* @param clsCategoriesSetForChallengePK the primary key for the new c l s categories set for challenge
	* @return the new c l s categories set for challenge
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge create(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSCategoriesSetForChallengePK clsCategoriesSetForChallengePK) {
		return getPersistence().create(clsCategoriesSetForChallengePK);
	}

	/**
	* Removes the c l s categories set for challenge with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param clsCategoriesSetForChallengePK the primary key of the c l s categories set for challenge
	* @return the c l s categories set for challenge that was removed
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException if a c l s categories set for challenge with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge remove(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSCategoriesSetForChallengePK clsCategoriesSetForChallengePK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException {
		return getPersistence().remove(clsCategoriesSetForChallengePK);
	}

	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge clsCategoriesSetForChallenge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(clsCategoriesSetForChallenge);
	}

	/**
	* Returns the c l s categories set for challenge with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException} if it could not be found.
	*
	* @param clsCategoriesSetForChallengePK the primary key of the c l s categories set for challenge
	* @return the c l s categories set for challenge
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException if a c l s categories set for challenge with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge findByPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSCategoriesSetForChallengePK clsCategoriesSetForChallengePK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException {
		return getPersistence().findByPrimaryKey(clsCategoriesSetForChallengePK);
	}

	/**
	* Returns the c l s categories set for challenge with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param clsCategoriesSetForChallengePK the primary key of the c l s categories set for challenge
	* @return the c l s categories set for challenge, or <code>null</code> if a c l s categories set for challenge with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge fetchByPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSCategoriesSetForChallengePK clsCategoriesSetForChallengePK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(clsCategoriesSetForChallengePK);
	}

	/**
	* Returns all the c l s categories set for challenges.
	*
	* @return the c l s categories set for challenges
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the c l s categories set for challenges.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCategoriesSetForChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s categories set for challenges
	* @param end the upper bound of the range of c l s categories set for challenges (not inclusive)
	* @return the range of c l s categories set for challenges
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the c l s categories set for challenges.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCategoriesSetForChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s categories set for challenges
	* @param end the upper bound of the range of c l s categories set for challenges (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of c l s categories set for challenges
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the c l s categories set for challenges from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of c l s categories set for challenges.
	*
	* @return the number of c l s categories set for challenges
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static CLSCategoriesSetForChallengePersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (CLSCategoriesSetForChallengePersistence)PortletBeanLocatorUtil.locate(it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.getServletContextName(),
					CLSCategoriesSetForChallengePersistence.class.getName());

			ReferenceRegistry.registerReference(CLSCategoriesSetForChallengeUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(
		CLSCategoriesSetForChallengePersistence persistence) {
	}

	private static CLSCategoriesSetForChallengePersistence _persistence;
}