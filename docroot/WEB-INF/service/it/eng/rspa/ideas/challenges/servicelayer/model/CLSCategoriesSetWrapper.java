/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CLSCategoriesSet}.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSCategoriesSet
 * @generated
 */
public class CLSCategoriesSetWrapper implements CLSCategoriesSet,
	ModelWrapper<CLSCategoriesSet> {
	public CLSCategoriesSetWrapper(CLSCategoriesSet clsCategoriesSet) {
		_clsCategoriesSet = clsCategoriesSet;
	}

	@Override
	public Class<?> getModelClass() {
		return CLSCategoriesSet.class;
	}

	@Override
	public String getModelClassName() {
		return CLSCategoriesSet.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("categoriesSetID", getCategoriesSetID());
		attributes.put("iconName", getIconName());
		attributes.put("colourName", getColourName());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long categoriesSetID = (Long)attributes.get("categoriesSetID");

		if (categoriesSetID != null) {
			setCategoriesSetID(categoriesSetID);
		}

		String iconName = (String)attributes.get("iconName");

		if (iconName != null) {
			setIconName(iconName);
		}

		String colourName = (String)attributes.get("colourName");

		if (colourName != null) {
			setColourName(colourName);
		}
	}

	/**
	* Returns the primary key of this c l s categories set.
	*
	* @return the primary key of this c l s categories set
	*/
	@Override
	public long getPrimaryKey() {
		return _clsCategoriesSet.getPrimaryKey();
	}

	/**
	* Sets the primary key of this c l s categories set.
	*
	* @param primaryKey the primary key of this c l s categories set
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_clsCategoriesSet.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the categories set i d of this c l s categories set.
	*
	* @return the categories set i d of this c l s categories set
	*/
	@Override
	public long getCategoriesSetID() {
		return _clsCategoriesSet.getCategoriesSetID();
	}

	/**
	* Sets the categories set i d of this c l s categories set.
	*
	* @param categoriesSetID the categories set i d of this c l s categories set
	*/
	@Override
	public void setCategoriesSetID(long categoriesSetID) {
		_clsCategoriesSet.setCategoriesSetID(categoriesSetID);
	}

	/**
	* Returns the icon name of this c l s categories set.
	*
	* @return the icon name of this c l s categories set
	*/
	@Override
	public java.lang.String getIconName() {
		return _clsCategoriesSet.getIconName();
	}

	/**
	* Sets the icon name of this c l s categories set.
	*
	* @param iconName the icon name of this c l s categories set
	*/
	@Override
	public void setIconName(java.lang.String iconName) {
		_clsCategoriesSet.setIconName(iconName);
	}

	/**
	* Returns the colour name of this c l s categories set.
	*
	* @return the colour name of this c l s categories set
	*/
	@Override
	public java.lang.String getColourName() {
		return _clsCategoriesSet.getColourName();
	}

	/**
	* Sets the colour name of this c l s categories set.
	*
	* @param colourName the colour name of this c l s categories set
	*/
	@Override
	public void setColourName(java.lang.String colourName) {
		_clsCategoriesSet.setColourName(colourName);
	}

	@Override
	public boolean isNew() {
		return _clsCategoriesSet.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_clsCategoriesSet.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _clsCategoriesSet.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_clsCategoriesSet.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _clsCategoriesSet.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _clsCategoriesSet.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_clsCategoriesSet.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _clsCategoriesSet.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_clsCategoriesSet.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_clsCategoriesSet.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_clsCategoriesSet.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CLSCategoriesSetWrapper((CLSCategoriesSet)_clsCategoriesSet.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet clsCategoriesSet) {
		return _clsCategoriesSet.compareTo(clsCategoriesSet);
	}

	@Override
	public int hashCode() {
		return _clsCategoriesSet.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet> toCacheModel() {
		return _clsCategoriesSet.toCacheModel();
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet toEscapedModel() {
		return new CLSCategoriesSetWrapper(_clsCategoriesSet.toEscapedModel());
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet toUnescapedModel() {
		return new CLSCategoriesSetWrapper(_clsCategoriesSet.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _clsCategoriesSet.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _clsCategoriesSet.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_clsCategoriesSet.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSCategoriesSetWrapper)) {
			return false;
		}

		CLSCategoriesSetWrapper clsCategoriesSetWrapper = (CLSCategoriesSetWrapper)obj;

		if (Validator.equals(_clsCategoriesSet,
					clsCategoriesSetWrapper._clsCategoriesSet)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public CLSCategoriesSet getWrappedCLSCategoriesSet() {
		return _clsCategoriesSet;
	}

	@Override
	public CLSCategoriesSet getWrappedModel() {
		return _clsCategoriesSet;
	}

	@Override
	public void resetOriginalValues() {
		_clsCategoriesSet.resetOriginalValues();
	}

	private CLSCategoriesSet _clsCategoriesSet;
}