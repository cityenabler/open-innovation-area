/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link VirtuosityPointsService}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see VirtuosityPointsService
 * @generated
 */
public class VirtuosityPointsServiceWrapper implements VirtuosityPointsService,
	ServiceWrapper<VirtuosityPointsService> {
	public VirtuosityPointsServiceWrapper(
		VirtuosityPointsService virtuosityPointsService) {
		_virtuosityPointsService = virtuosityPointsService;
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _virtuosityPointsService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_virtuosityPointsService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _virtuosityPointsService.invokeMethod(name, parameterTypes,
			arguments);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public VirtuosityPointsService getWrappedVirtuosityPointsService() {
		return _virtuosityPointsService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedVirtuosityPointsService(
		VirtuosityPointsService virtuosityPointsService) {
		_virtuosityPointsService = virtuosityPointsService;
	}

	@Override
	public VirtuosityPointsService getWrappedService() {
		return _virtuosityPointsService;
	}

	@Override
	public void setWrappedService(
		VirtuosityPointsService virtuosityPointsService) {
		_virtuosityPointsService = virtuosityPointsService;
	}

	private VirtuosityPointsService _virtuosityPointsService;
}