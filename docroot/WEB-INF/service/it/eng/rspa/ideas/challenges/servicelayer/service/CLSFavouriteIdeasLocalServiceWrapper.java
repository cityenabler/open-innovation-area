/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CLSFavouriteIdeasLocalService}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSFavouriteIdeasLocalService
 * @generated
 */
public class CLSFavouriteIdeasLocalServiceWrapper
	implements CLSFavouriteIdeasLocalService,
		ServiceWrapper<CLSFavouriteIdeasLocalService> {
	public CLSFavouriteIdeasLocalServiceWrapper(
		CLSFavouriteIdeasLocalService clsFavouriteIdeasLocalService) {
		_clsFavouriteIdeasLocalService = clsFavouriteIdeasLocalService;
	}

	/**
	* Adds the c l s favourite ideas to the database. Also notifies the appropriate model listeners.
	*
	* @param clsFavouriteIdeas the c l s favourite ideas
	* @return the c l s favourite ideas that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas addCLSFavouriteIdeas(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas clsFavouriteIdeas)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteIdeasLocalService.addCLSFavouriteIdeas(clsFavouriteIdeas);
	}

	/**
	* Creates a new c l s favourite ideas with the primary key. Does not add the c l s favourite ideas to the database.
	*
	* @param clsFavouriteIdeasPK the primary key for the new c l s favourite ideas
	* @return the new c l s favourite ideas
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas createCLSFavouriteIdeas(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteIdeasPK clsFavouriteIdeasPK) {
		return _clsFavouriteIdeasLocalService.createCLSFavouriteIdeas(clsFavouriteIdeasPK);
	}

	/**
	* Deletes the c l s favourite ideas with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param clsFavouriteIdeasPK the primary key of the c l s favourite ideas
	* @return the c l s favourite ideas that was removed
	* @throws PortalException if a c l s favourite ideas with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas deleteCLSFavouriteIdeas(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteIdeasPK clsFavouriteIdeasPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteIdeasLocalService.deleteCLSFavouriteIdeas(clsFavouriteIdeasPK);
	}

	/**
	* Deletes the c l s favourite ideas from the database. Also notifies the appropriate model listeners.
	*
	* @param clsFavouriteIdeas the c l s favourite ideas
	* @return the c l s favourite ideas that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas deleteCLSFavouriteIdeas(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas clsFavouriteIdeas)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteIdeasLocalService.deleteCLSFavouriteIdeas(clsFavouriteIdeas);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _clsFavouriteIdeasLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteIdeasLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteIdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteIdeasLocalService.dynamicQuery(dynamicQuery, start,
			end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteIdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteIdeasLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteIdeasLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteIdeasLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas fetchCLSFavouriteIdeas(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteIdeasPK clsFavouriteIdeasPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteIdeasLocalService.fetchCLSFavouriteIdeas(clsFavouriteIdeasPK);
	}

	/**
	* Returns the c l s favourite ideas with the primary key.
	*
	* @param clsFavouriteIdeasPK the primary key of the c l s favourite ideas
	* @return the c l s favourite ideas
	* @throws PortalException if a c l s favourite ideas with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas getCLSFavouriteIdeas(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteIdeasPK clsFavouriteIdeasPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteIdeasLocalService.getCLSFavouriteIdeas(clsFavouriteIdeasPK);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteIdeasLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the c l s favourite ideases.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteIdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s favourite ideases
	* @param end the upper bound of the range of c l s favourite ideases (not inclusive)
	* @return the range of c l s favourite ideases
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas> getCLSFavouriteIdeases(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteIdeasLocalService.getCLSFavouriteIdeases(start, end);
	}

	/**
	* Returns the number of c l s favourite ideases.
	*
	* @return the number of c l s favourite ideases
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getCLSFavouriteIdeasesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteIdeasLocalService.getCLSFavouriteIdeasesCount();
	}

	/**
	* Updates the c l s favourite ideas in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param clsFavouriteIdeas the c l s favourite ideas
	* @return the c l s favourite ideas that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas updateCLSFavouriteIdeas(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas clsFavouriteIdeas)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteIdeasLocalService.updateCLSFavouriteIdeas(clsFavouriteIdeas);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _clsFavouriteIdeasLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_clsFavouriteIdeasLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _clsFavouriteIdeasLocalService.invokeMethod(name,
			parameterTypes, arguments);
	}

	@Override
	public void removeFavouriteIdeasOnIdeaDelete(long ideaId) {
		_clsFavouriteIdeasLocalService.removeFavouriteIdeasOnIdeaDelete(ideaId);
	}

	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas> getFavouriteIdeasEntriesByIdeaId(
		long ideaId) throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteIdeasLocalService.getFavouriteIdeasEntriesByIdeaId(ideaId);
	}

	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea> getFavouriteIdeasByUserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteIdeasLocalService.getFavouriteIdeasByUserId(userId);
	}

	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas> getFavouriteIdeasEntriesByUserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteIdeasLocalService.getFavouriteIdeasEntriesByUserId(userId);
	}

	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas> getFavouriteIdeas(
		java.lang.Long ideaId, java.lang.Long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteIdeasLocalService.getFavouriteIdeas(ideaId, userId);
	}

	@Override
	public boolean addFavouriteIdea(java.lang.Long favIdeaId,
		java.lang.Long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteIdeasLocalService.addFavouriteIdea(favIdeaId, userId);
	}

	@Override
	public boolean removeFavouriteIdea(java.lang.Long favIdeaId,
		java.lang.Long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteIdeasLocalService.removeFavouriteIdea(favIdeaId,
			userId);
	}

	@Override
	public boolean removeFavouriteIdea(java.lang.Long favideaId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteIdeasLocalService.removeFavouriteIdea(favideaId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public CLSFavouriteIdeasLocalService getWrappedCLSFavouriteIdeasLocalService() {
		return _clsFavouriteIdeasLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedCLSFavouriteIdeasLocalService(
		CLSFavouriteIdeasLocalService clsFavouriteIdeasLocalService) {
		_clsFavouriteIdeasLocalService = clsFavouriteIdeasLocalService;
	}

	@Override
	public CLSFavouriteIdeasLocalService getWrappedService() {
		return _clsFavouriteIdeasLocalService;
	}

	@Override
	public void setWrappedService(
		CLSFavouriteIdeasLocalService clsFavouriteIdeasLocalService) {
		_clsFavouriteIdeasLocalService = clsFavouriteIdeasLocalService;
	}

	private CLSFavouriteIdeasLocalService _clsFavouriteIdeasLocalService;
}