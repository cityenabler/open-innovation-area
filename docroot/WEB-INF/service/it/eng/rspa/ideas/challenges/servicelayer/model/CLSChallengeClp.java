/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 */
public class CLSChallengeClp extends BaseModelImpl<CLSChallenge>
	implements CLSChallenge {
	public CLSChallengeClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return CLSChallenge.class;
	}

	@Override
	public String getModelClassName() {
		return CLSChallenge.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _challengeId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setChallengeId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _challengeId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("challengeId", getChallengeId());
		attributes.put("challengeTitle", getChallengeTitle());
		attributes.put("challengeDescription", getChallengeDescription());
		attributes.put("challengeWithReward", getChallengeWithReward());
		attributes.put("challengeReward", getChallengeReward());
		attributes.put("numberOfSelectableIdeas", getNumberOfSelectableIdeas());
		attributes.put("dateAdded", getDateAdded());
		attributes.put("challengeStatus", getChallengeStatus());
		attributes.put("status", getStatus());
		attributes.put("statusByUserId", getStatusByUserId());
		attributes.put("statusByUserName", getStatusByUserName());
		attributes.put("statusDate", getStatusDate());
		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("userId", getUserId());
		attributes.put("dateEnd", getDateEnd());
		attributes.put("dateStart", getDateStart());
		attributes.put("language", getLanguage());
		attributes.put("challengeHashTag", getChallengeHashTag());
		attributes.put("dmFolderName", getDmFolderName());
		attributes.put("idFolder", getIdFolder());
		attributes.put("representativeImgUrl", getRepresentativeImgUrl());
		attributes.put("calendarBooking", getCalendarBooking());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long challengeId = (Long)attributes.get("challengeId");

		if (challengeId != null) {
			setChallengeId(challengeId);
		}

		String challengeTitle = (String)attributes.get("challengeTitle");

		if (challengeTitle != null) {
			setChallengeTitle(challengeTitle);
		}

		String challengeDescription = (String)attributes.get(
				"challengeDescription");

		if (challengeDescription != null) {
			setChallengeDescription(challengeDescription);
		}

		Boolean challengeWithReward = (Boolean)attributes.get(
				"challengeWithReward");

		if (challengeWithReward != null) {
			setChallengeWithReward(challengeWithReward);
		}

		String challengeReward = (String)attributes.get("challengeReward");

		if (challengeReward != null) {
			setChallengeReward(challengeReward);
		}

		Integer numberOfSelectableIdeas = (Integer)attributes.get(
				"numberOfSelectableIdeas");

		if (numberOfSelectableIdeas != null) {
			setNumberOfSelectableIdeas(numberOfSelectableIdeas);
		}

		Date dateAdded = (Date)attributes.get("dateAdded");

		if (dateAdded != null) {
			setDateAdded(dateAdded);
		}

		String challengeStatus = (String)attributes.get("challengeStatus");

		if (challengeStatus != null) {
			setChallengeStatus(challengeStatus);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		Long statusByUserId = (Long)attributes.get("statusByUserId");

		if (statusByUserId != null) {
			setStatusByUserId(statusByUserId);
		}

		String statusByUserName = (String)attributes.get("statusByUserName");

		if (statusByUserName != null) {
			setStatusByUserName(statusByUserName);
		}

		Date statusDate = (Date)attributes.get("statusDate");

		if (statusDate != null) {
			setStatusDate(statusDate);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Date dateEnd = (Date)attributes.get("dateEnd");

		if (dateEnd != null) {
			setDateEnd(dateEnd);
		}

		Date dateStart = (Date)attributes.get("dateStart");

		if (dateStart != null) {
			setDateStart(dateStart);
		}

		String language = (String)attributes.get("language");

		if (language != null) {
			setLanguage(language);
		}

		String challengeHashTag = (String)attributes.get("challengeHashTag");

		if (challengeHashTag != null) {
			setChallengeHashTag(challengeHashTag);
		}

		String dmFolderName = (String)attributes.get("dmFolderName");

		if (dmFolderName != null) {
			setDmFolderName(dmFolderName);
		}

		Long idFolder = (Long)attributes.get("idFolder");

		if (idFolder != null) {
			setIdFolder(idFolder);
		}

		String representativeImgUrl = (String)attributes.get(
				"representativeImgUrl");

		if (representativeImgUrl != null) {
			setRepresentativeImgUrl(representativeImgUrl);
		}

		Long calendarBooking = (Long)attributes.get("calendarBooking");

		if (calendarBooking != null) {
			setCalendarBooking(calendarBooking);
		}
	}

	@Override
	public String getUuid() {
		return _uuid;
	}

	@Override
	public void setUuid(String uuid) {
		_uuid = uuid;

		if (_clsChallengeRemoteModel != null) {
			try {
				Class<?> clazz = _clsChallengeRemoteModel.getClass();

				Method method = clazz.getMethod("setUuid", String.class);

				method.invoke(_clsChallengeRemoteModel, uuid);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getChallengeId() {
		return _challengeId;
	}

	@Override
	public void setChallengeId(long challengeId) {
		_challengeId = challengeId;

		if (_clsChallengeRemoteModel != null) {
			try {
				Class<?> clazz = _clsChallengeRemoteModel.getClass();

				Method method = clazz.getMethod("setChallengeId", long.class);

				method.invoke(_clsChallengeRemoteModel, challengeId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getChallengeTitle() {
		return _challengeTitle;
	}

	@Override
	public void setChallengeTitle(String challengeTitle) {
		_challengeTitle = challengeTitle;

		if (_clsChallengeRemoteModel != null) {
			try {
				Class<?> clazz = _clsChallengeRemoteModel.getClass();

				Method method = clazz.getMethod("setChallengeTitle",
						String.class);

				method.invoke(_clsChallengeRemoteModel, challengeTitle);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getChallengeDescription() {
		return _challengeDescription;
	}

	@Override
	public void setChallengeDescription(String challengeDescription) {
		_challengeDescription = challengeDescription;

		if (_clsChallengeRemoteModel != null) {
			try {
				Class<?> clazz = _clsChallengeRemoteModel.getClass();

				Method method = clazz.getMethod("setChallengeDescription",
						String.class);

				method.invoke(_clsChallengeRemoteModel, challengeDescription);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getChallengeWithReward() {
		return _challengeWithReward;
	}

	@Override
	public boolean isChallengeWithReward() {
		return _challengeWithReward;
	}

	@Override
	public void setChallengeWithReward(boolean challengeWithReward) {
		_challengeWithReward = challengeWithReward;

		if (_clsChallengeRemoteModel != null) {
			try {
				Class<?> clazz = _clsChallengeRemoteModel.getClass();

				Method method = clazz.getMethod("setChallengeWithReward",
						boolean.class);

				method.invoke(_clsChallengeRemoteModel, challengeWithReward);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getChallengeReward() {
		return _challengeReward;
	}

	@Override
	public void setChallengeReward(String challengeReward) {
		_challengeReward = challengeReward;

		if (_clsChallengeRemoteModel != null) {
			try {
				Class<?> clazz = _clsChallengeRemoteModel.getClass();

				Method method = clazz.getMethod("setChallengeReward",
						String.class);

				method.invoke(_clsChallengeRemoteModel, challengeReward);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getNumberOfSelectableIdeas() {
		return _numberOfSelectableIdeas;
	}

	@Override
	public void setNumberOfSelectableIdeas(int numberOfSelectableIdeas) {
		_numberOfSelectableIdeas = numberOfSelectableIdeas;

		if (_clsChallengeRemoteModel != null) {
			try {
				Class<?> clazz = _clsChallengeRemoteModel.getClass();

				Method method = clazz.getMethod("setNumberOfSelectableIdeas",
						int.class);

				method.invoke(_clsChallengeRemoteModel, numberOfSelectableIdeas);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDateAdded() {
		return _dateAdded;
	}

	@Override
	public void setDateAdded(Date dateAdded) {
		_dateAdded = dateAdded;

		if (_clsChallengeRemoteModel != null) {
			try {
				Class<?> clazz = _clsChallengeRemoteModel.getClass();

				Method method = clazz.getMethod("setDateAdded", Date.class);

				method.invoke(_clsChallengeRemoteModel, dateAdded);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getChallengeStatus() {
		return _challengeStatus;
	}

	@Override
	public void setChallengeStatus(String challengeStatus) {
		_challengeStatus = challengeStatus;

		if (_clsChallengeRemoteModel != null) {
			try {
				Class<?> clazz = _clsChallengeRemoteModel.getClass();

				Method method = clazz.getMethod("setChallengeStatus",
						String.class);

				method.invoke(_clsChallengeRemoteModel, challengeStatus);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getStatus() {
		return _status;
	}

	@Override
	public void setStatus(int status) {
		_status = status;

		if (_clsChallengeRemoteModel != null) {
			try {
				Class<?> clazz = _clsChallengeRemoteModel.getClass();

				Method method = clazz.getMethod("setStatus", int.class);

				method.invoke(_clsChallengeRemoteModel, status);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getStatusByUserId() {
		return _statusByUserId;
	}

	@Override
	public void setStatusByUserId(long statusByUserId) {
		_statusByUserId = statusByUserId;

		if (_clsChallengeRemoteModel != null) {
			try {
				Class<?> clazz = _clsChallengeRemoteModel.getClass();

				Method method = clazz.getMethod("setStatusByUserId", long.class);

				method.invoke(_clsChallengeRemoteModel, statusByUserId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getStatusByUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getStatusByUserId(), "uuid",
			_statusByUserUuid);
	}

	@Override
	public void setStatusByUserUuid(String statusByUserUuid) {
		_statusByUserUuid = statusByUserUuid;
	}

	@Override
	public String getStatusByUserName() {
		return _statusByUserName;
	}

	@Override
	public void setStatusByUserName(String statusByUserName) {
		_statusByUserName = statusByUserName;

		if (_clsChallengeRemoteModel != null) {
			try {
				Class<?> clazz = _clsChallengeRemoteModel.getClass();

				Method method = clazz.getMethod("setStatusByUserName",
						String.class);

				method.invoke(_clsChallengeRemoteModel, statusByUserName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getStatusDate() {
		return _statusDate;
	}

	@Override
	public void setStatusDate(Date statusDate) {
		_statusDate = statusDate;

		if (_clsChallengeRemoteModel != null) {
			try {
				Class<?> clazz = _clsChallengeRemoteModel.getClass();

				Method method = clazz.getMethod("setStatusDate", Date.class);

				method.invoke(_clsChallengeRemoteModel, statusDate);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getCompanyId() {
		return _companyId;
	}

	@Override
	public void setCompanyId(long companyId) {
		_companyId = companyId;

		if (_clsChallengeRemoteModel != null) {
			try {
				Class<?> clazz = _clsChallengeRemoteModel.getClass();

				Method method = clazz.getMethod("setCompanyId", long.class);

				method.invoke(_clsChallengeRemoteModel, companyId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getGroupId() {
		return _groupId;
	}

	@Override
	public void setGroupId(long groupId) {
		_groupId = groupId;

		if (_clsChallengeRemoteModel != null) {
			try {
				Class<?> clazz = _clsChallengeRemoteModel.getClass();

				Method method = clazz.getMethod("setGroupId", long.class);

				method.invoke(_clsChallengeRemoteModel, groupId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_userId = userId;

		if (_clsChallengeRemoteModel != null) {
			try {
				Class<?> clazz = _clsChallengeRemoteModel.getClass();

				Method method = clazz.getMethod("setUserId", long.class);

				method.invoke(_clsChallengeRemoteModel, userId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	@Override
	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	@Override
	public Date getDateEnd() {
		return _dateEnd;
	}

	@Override
	public void setDateEnd(Date dateEnd) {
		_dateEnd = dateEnd;

		if (_clsChallengeRemoteModel != null) {
			try {
				Class<?> clazz = _clsChallengeRemoteModel.getClass();

				Method method = clazz.getMethod("setDateEnd", Date.class);

				method.invoke(_clsChallengeRemoteModel, dateEnd);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDateStart() {
		return _dateStart;
	}

	@Override
	public void setDateStart(Date dateStart) {
		_dateStart = dateStart;

		if (_clsChallengeRemoteModel != null) {
			try {
				Class<?> clazz = _clsChallengeRemoteModel.getClass();

				Method method = clazz.getMethod("setDateStart", Date.class);

				method.invoke(_clsChallengeRemoteModel, dateStart);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLanguage() {
		return _language;
	}

	@Override
	public void setLanguage(String language) {
		_language = language;

		if (_clsChallengeRemoteModel != null) {
			try {
				Class<?> clazz = _clsChallengeRemoteModel.getClass();

				Method method = clazz.getMethod("setLanguage", String.class);

				method.invoke(_clsChallengeRemoteModel, language);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getChallengeHashTag() {
		return _challengeHashTag;
	}

	@Override
	public void setChallengeHashTag(String challengeHashTag) {
		_challengeHashTag = challengeHashTag;

		if (_clsChallengeRemoteModel != null) {
			try {
				Class<?> clazz = _clsChallengeRemoteModel.getClass();

				Method method = clazz.getMethod("setChallengeHashTag",
						String.class);

				method.invoke(_clsChallengeRemoteModel, challengeHashTag);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDmFolderName() {
		return _dmFolderName;
	}

	@Override
	public void setDmFolderName(String dmFolderName) {
		_dmFolderName = dmFolderName;

		if (_clsChallengeRemoteModel != null) {
			try {
				Class<?> clazz = _clsChallengeRemoteModel.getClass();

				Method method = clazz.getMethod("setDmFolderName", String.class);

				method.invoke(_clsChallengeRemoteModel, dmFolderName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdFolder() {
		return _idFolder;
	}

	@Override
	public void setIdFolder(long idFolder) {
		_idFolder = idFolder;

		if (_clsChallengeRemoteModel != null) {
			try {
				Class<?> clazz = _clsChallengeRemoteModel.getClass();

				Method method = clazz.getMethod("setIdFolder", long.class);

				method.invoke(_clsChallengeRemoteModel, idFolder);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getRepresentativeImgUrl() {
		return _representativeImgUrl;
	}

	@Override
	public void setRepresentativeImgUrl(String representativeImgUrl) {
		_representativeImgUrl = representativeImgUrl;

		if (_clsChallengeRemoteModel != null) {
			try {
				Class<?> clazz = _clsChallengeRemoteModel.getClass();

				Method method = clazz.getMethod("setRepresentativeImgUrl",
						String.class);

				method.invoke(_clsChallengeRemoteModel, representativeImgUrl);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getCalendarBooking() {
		return _calendarBooking;
	}

	@Override
	public void setCalendarBooking(long calendarBooking) {
		_calendarBooking = calendarBooking;

		if (_clsChallengeRemoteModel != null) {
			try {
				Class<?> clazz = _clsChallengeRemoteModel.getClass();

				Method method = clazz.getMethod("setCalendarBooking", long.class);

				method.invoke(_clsChallengeRemoteModel, calendarBooking);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #isApproved}
	 */
	@Override
	public boolean getApproved() {
		return isApproved();
	}

	@Override
	public boolean isApproved() {
		if (getStatus() == WorkflowConstants.STATUS_APPROVED) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean isDenied() {
		if (getStatus() == WorkflowConstants.STATUS_DENIED) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean isDraft() {
		if (getStatus() == WorkflowConstants.STATUS_DRAFT) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean isExpired() {
		if (getStatus() == WorkflowConstants.STATUS_EXPIRED) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean isInactive() {
		if (getStatus() == WorkflowConstants.STATUS_INACTIVE) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean isIncomplete() {
		if (getStatus() == WorkflowConstants.STATUS_INCOMPLETE) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean isPending() {
		if (getStatus() == WorkflowConstants.STATUS_PENDING) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean isScheduled() {
		if (getStatus() == WorkflowConstants.STATUS_SCHEDULED) {
			return true;
		}
		else {
			return false;
		}
	}

	public BaseModel<?> getCLSChallengeRemoteModel() {
		return _clsChallengeRemoteModel;
	}

	public void setCLSChallengeRemoteModel(BaseModel<?> clsChallengeRemoteModel) {
		_clsChallengeRemoteModel = clsChallengeRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _clsChallengeRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_clsChallengeRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			CLSChallengeLocalServiceUtil.addCLSChallenge(this);
		}
		else {
			CLSChallengeLocalServiceUtil.updateCLSChallenge(this);
		}
	}

	@Override
	public CLSChallenge toEscapedModel() {
		return (CLSChallenge)ProxyUtil.newProxyInstance(CLSChallenge.class.getClassLoader(),
			new Class[] { CLSChallenge.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		CLSChallengeClp clone = new CLSChallengeClp();

		clone.setUuid(getUuid());
		clone.setChallengeId(getChallengeId());
		clone.setChallengeTitle(getChallengeTitle());
		clone.setChallengeDescription(getChallengeDescription());
		clone.setChallengeWithReward(getChallengeWithReward());
		clone.setChallengeReward(getChallengeReward());
		clone.setNumberOfSelectableIdeas(getNumberOfSelectableIdeas());
		clone.setDateAdded(getDateAdded());
		clone.setChallengeStatus(getChallengeStatus());
		clone.setStatus(getStatus());
		clone.setStatusByUserId(getStatusByUserId());
		clone.setStatusByUserName(getStatusByUserName());
		clone.setStatusDate(getStatusDate());
		clone.setCompanyId(getCompanyId());
		clone.setGroupId(getGroupId());
		clone.setUserId(getUserId());
		clone.setDateEnd(getDateEnd());
		clone.setDateStart(getDateStart());
		clone.setLanguage(getLanguage());
		clone.setChallengeHashTag(getChallengeHashTag());
		clone.setDmFolderName(getDmFolderName());
		clone.setIdFolder(getIdFolder());
		clone.setRepresentativeImgUrl(getRepresentativeImgUrl());
		clone.setCalendarBooking(getCalendarBooking());

		return clone;
	}

	@Override
	public int compareTo(CLSChallenge clsChallenge) {
		int value = 0;

		if (getChallengeId() < clsChallenge.getChallengeId()) {
			value = -1;
		}
		else if (getChallengeId() > clsChallenge.getChallengeId()) {
			value = 1;
		}
		else {
			value = 0;
		}

		value = value * -1;

		if (value != 0) {
			return value;
		}

		value = DateUtil.compareTo(getDateAdded(), clsChallenge.getDateAdded());

		value = value * -1;

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSChallengeClp)) {
			return false;
		}

		CLSChallengeClp clsChallenge = (CLSChallengeClp)obj;

		long primaryKey = clsChallenge.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(49);

		sb.append("{uuid=");
		sb.append(getUuid());
		sb.append(", challengeId=");
		sb.append(getChallengeId());
		sb.append(", challengeTitle=");
		sb.append(getChallengeTitle());
		sb.append(", challengeDescription=");
		sb.append(getChallengeDescription());
		sb.append(", challengeWithReward=");
		sb.append(getChallengeWithReward());
		sb.append(", challengeReward=");
		sb.append(getChallengeReward());
		sb.append(", numberOfSelectableIdeas=");
		sb.append(getNumberOfSelectableIdeas());
		sb.append(", dateAdded=");
		sb.append(getDateAdded());
		sb.append(", challengeStatus=");
		sb.append(getChallengeStatus());
		sb.append(", status=");
		sb.append(getStatus());
		sb.append(", statusByUserId=");
		sb.append(getStatusByUserId());
		sb.append(", statusByUserName=");
		sb.append(getStatusByUserName());
		sb.append(", statusDate=");
		sb.append(getStatusDate());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", groupId=");
		sb.append(getGroupId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", dateEnd=");
		sb.append(getDateEnd());
		sb.append(", dateStart=");
		sb.append(getDateStart());
		sb.append(", language=");
		sb.append(getLanguage());
		sb.append(", challengeHashTag=");
		sb.append(getChallengeHashTag());
		sb.append(", dmFolderName=");
		sb.append(getDmFolderName());
		sb.append(", idFolder=");
		sb.append(getIdFolder());
		sb.append(", representativeImgUrl=");
		sb.append(getRepresentativeImgUrl());
		sb.append(", calendarBooking=");
		sb.append(getCalendarBooking());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(76);

		sb.append("<model><model-name>");
		sb.append(
			"it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>uuid</column-name><column-value><![CDATA[");
		sb.append(getUuid());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>challengeId</column-name><column-value><![CDATA[");
		sb.append(getChallengeId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>challengeTitle</column-name><column-value><![CDATA[");
		sb.append(getChallengeTitle());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>challengeDescription</column-name><column-value><![CDATA[");
		sb.append(getChallengeDescription());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>challengeWithReward</column-name><column-value><![CDATA[");
		sb.append(getChallengeWithReward());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>challengeReward</column-name><column-value><![CDATA[");
		sb.append(getChallengeReward());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numberOfSelectableIdeas</column-name><column-value><![CDATA[");
		sb.append(getNumberOfSelectableIdeas());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dateAdded</column-name><column-value><![CDATA[");
		sb.append(getDateAdded());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>challengeStatus</column-name><column-value><![CDATA[");
		sb.append(getChallengeStatus());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>status</column-name><column-value><![CDATA[");
		sb.append(getStatus());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>statusByUserId</column-name><column-value><![CDATA[");
		sb.append(getStatusByUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>statusByUserName</column-name><column-value><![CDATA[");
		sb.append(getStatusByUserName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>statusDate</column-name><column-value><![CDATA[");
		sb.append(getStatusDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>groupId</column-name><column-value><![CDATA[");
		sb.append(getGroupId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dateEnd</column-name><column-value><![CDATA[");
		sb.append(getDateEnd());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dateStart</column-name><column-value><![CDATA[");
		sb.append(getDateStart());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>language</column-name><column-value><![CDATA[");
		sb.append(getLanguage());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>challengeHashTag</column-name><column-value><![CDATA[");
		sb.append(getChallengeHashTag());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dmFolderName</column-name><column-value><![CDATA[");
		sb.append(getDmFolderName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idFolder</column-name><column-value><![CDATA[");
		sb.append(getIdFolder());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>representativeImgUrl</column-name><column-value><![CDATA[");
		sb.append(getRepresentativeImgUrl());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>calendarBooking</column-name><column-value><![CDATA[");
		sb.append(getCalendarBooking());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private String _uuid;
	private long _challengeId;
	private String _challengeTitle;
	private String _challengeDescription;
	private boolean _challengeWithReward;
	private String _challengeReward;
	private int _numberOfSelectableIdeas;
	private Date _dateAdded;
	private String _challengeStatus;
	private int _status;
	private long _statusByUserId;
	private String _statusByUserUuid;
	private String _statusByUserName;
	private Date _statusDate;
	private long _companyId;
	private long _groupId;
	private long _userId;
	private String _userUuid;
	private Date _dateEnd;
	private Date _dateStart;
	private String _language;
	private String _challengeHashTag;
	private String _dmFolderName;
	private long _idFolder;
	private String _representativeImgUrl;
	private long _calendarBooking;
	private BaseModel<?> _clsChallengeRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.class;
}