/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 */
public class CLSIdeaClp extends BaseModelImpl<CLSIdea> implements CLSIdea {
	public CLSIdeaClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return CLSIdea.class;
	}

	@Override
	public String getModelClassName() {
		return CLSIdea.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _ideaID;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setIdeaID(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _ideaID;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("ideaID", getIdeaID());
		attributes.put("ideaTitle", getIdeaTitle());
		attributes.put("ideaDescription", getIdeaDescription());
		attributes.put("dateAdded", getDateAdded());
		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("userId", getUserId());
		attributes.put("municipalityId", getMunicipalityId());
		attributes.put("municipalityOrganizationId",
			getMunicipalityOrganizationId());
		attributes.put("challengeId", getChallengeId());
		attributes.put("takenUp", getTakenUp());
		attributes.put("needId", getNeedId());
		attributes.put("dmFolderName", getDmFolderName());
		attributes.put("idFolder", getIdFolder());
		attributes.put("representativeImgUrl", getRepresentativeImgUrl());
		attributes.put("isNeed", getIsNeed());
		attributes.put("status", getStatus());
		attributes.put("statusByUserId", getStatusByUserId());
		attributes.put("statusByUserName", getStatusByUserName());
		attributes.put("statusDate", getStatusDate());
		attributes.put("chatGroup", getChatGroup());
		attributes.put("ideaHashTag", getIdeaHashTag());
		attributes.put("ideaStatus", getIdeaStatus());
		attributes.put("language", getLanguage());
		attributes.put("finalMotivation", getFinalMotivation());
		attributes.put("evaluationPassed", getEvaluationPassed());
		attributes.put("cityName", getCityName());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long ideaID = (Long)attributes.get("ideaID");

		if (ideaID != null) {
			setIdeaID(ideaID);
		}

		String ideaTitle = (String)attributes.get("ideaTitle");

		if (ideaTitle != null) {
			setIdeaTitle(ideaTitle);
		}

		String ideaDescription = (String)attributes.get("ideaDescription");

		if (ideaDescription != null) {
			setIdeaDescription(ideaDescription);
		}

		Date dateAdded = (Date)attributes.get("dateAdded");

		if (dateAdded != null) {
			setDateAdded(dateAdded);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Long municipalityId = (Long)attributes.get("municipalityId");

		if (municipalityId != null) {
			setMunicipalityId(municipalityId);
		}

		Long municipalityOrganizationId = (Long)attributes.get(
				"municipalityOrganizationId");

		if (municipalityOrganizationId != null) {
			setMunicipalityOrganizationId(municipalityOrganizationId);
		}

		Long challengeId = (Long)attributes.get("challengeId");

		if (challengeId != null) {
			setChallengeId(challengeId);
		}

		Boolean takenUp = (Boolean)attributes.get("takenUp");

		if (takenUp != null) {
			setTakenUp(takenUp);
		}

		Long needId = (Long)attributes.get("needId");

		if (needId != null) {
			setNeedId(needId);
		}

		String dmFolderName = (String)attributes.get("dmFolderName");

		if (dmFolderName != null) {
			setDmFolderName(dmFolderName);
		}

		Long idFolder = (Long)attributes.get("idFolder");

		if (idFolder != null) {
			setIdFolder(idFolder);
		}

		String representativeImgUrl = (String)attributes.get(
				"representativeImgUrl");

		if (representativeImgUrl != null) {
			setRepresentativeImgUrl(representativeImgUrl);
		}

		Boolean isNeed = (Boolean)attributes.get("isNeed");

		if (isNeed != null) {
			setIsNeed(isNeed);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		Long statusByUserId = (Long)attributes.get("statusByUserId");

		if (statusByUserId != null) {
			setStatusByUserId(statusByUserId);
		}

		String statusByUserName = (String)attributes.get("statusByUserName");

		if (statusByUserName != null) {
			setStatusByUserName(statusByUserName);
		}

		Date statusDate = (Date)attributes.get("statusDate");

		if (statusDate != null) {
			setStatusDate(statusDate);
		}

		Long chatGroup = (Long)attributes.get("chatGroup");

		if (chatGroup != null) {
			setChatGroup(chatGroup);
		}

		String ideaHashTag = (String)attributes.get("ideaHashTag");

		if (ideaHashTag != null) {
			setIdeaHashTag(ideaHashTag);
		}

		String ideaStatus = (String)attributes.get("ideaStatus");

		if (ideaStatus != null) {
			setIdeaStatus(ideaStatus);
		}

		String language = (String)attributes.get("language");

		if (language != null) {
			setLanguage(language);
		}

		String finalMotivation = (String)attributes.get("finalMotivation");

		if (finalMotivation != null) {
			setFinalMotivation(finalMotivation);
		}

		Boolean evaluationPassed = (Boolean)attributes.get("evaluationPassed");

		if (evaluationPassed != null) {
			setEvaluationPassed(evaluationPassed);
		}

		String cityName = (String)attributes.get("cityName");

		if (cityName != null) {
			setCityName(cityName);
		}
	}

	@Override
	public String getUuid() {
		return _uuid;
	}

	@Override
	public void setUuid(String uuid) {
		_uuid = uuid;

		if (_clsIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setUuid", String.class);

				method.invoke(_clsIdeaRemoteModel, uuid);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdeaID() {
		return _ideaID;
	}

	@Override
	public void setIdeaID(long ideaID) {
		_ideaID = ideaID;

		if (_clsIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setIdeaID", long.class);

				method.invoke(_clsIdeaRemoteModel, ideaID);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getIdeaTitle() {
		return _ideaTitle;
	}

	@Override
	public void setIdeaTitle(String ideaTitle) {
		_ideaTitle = ideaTitle;

		if (_clsIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setIdeaTitle", String.class);

				method.invoke(_clsIdeaRemoteModel, ideaTitle);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getIdeaDescription() {
		return _ideaDescription;
	}

	@Override
	public void setIdeaDescription(String ideaDescription) {
		_ideaDescription = ideaDescription;

		if (_clsIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setIdeaDescription",
						String.class);

				method.invoke(_clsIdeaRemoteModel, ideaDescription);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDateAdded() {
		return _dateAdded;
	}

	@Override
	public void setDateAdded(Date dateAdded) {
		_dateAdded = dateAdded;

		if (_clsIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setDateAdded", Date.class);

				method.invoke(_clsIdeaRemoteModel, dateAdded);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getCompanyId() {
		return _companyId;
	}

	@Override
	public void setCompanyId(long companyId) {
		_companyId = companyId;

		if (_clsIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setCompanyId", long.class);

				method.invoke(_clsIdeaRemoteModel, companyId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getGroupId() {
		return _groupId;
	}

	@Override
	public void setGroupId(long groupId) {
		_groupId = groupId;

		if (_clsIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setGroupId", long.class);

				method.invoke(_clsIdeaRemoteModel, groupId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_userId = userId;

		if (_clsIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setUserId", long.class);

				method.invoke(_clsIdeaRemoteModel, userId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	@Override
	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	@Override
	public long getMunicipalityId() {
		return _municipalityId;
	}

	@Override
	public void setMunicipalityId(long municipalityId) {
		_municipalityId = municipalityId;

		if (_clsIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setMunicipalityId", long.class);

				method.invoke(_clsIdeaRemoteModel, municipalityId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getMunicipalityOrganizationId() {
		return _municipalityOrganizationId;
	}

	@Override
	public void setMunicipalityOrganizationId(long municipalityOrganizationId) {
		_municipalityOrganizationId = municipalityOrganizationId;

		if (_clsIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setMunicipalityOrganizationId",
						long.class);

				method.invoke(_clsIdeaRemoteModel, municipalityOrganizationId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getChallengeId() {
		return _challengeId;
	}

	@Override
	public void setChallengeId(long challengeId) {
		_challengeId = challengeId;

		if (_clsIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setChallengeId", long.class);

				method.invoke(_clsIdeaRemoteModel, challengeId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getTakenUp() {
		return _takenUp;
	}

	@Override
	public boolean isTakenUp() {
		return _takenUp;
	}

	@Override
	public void setTakenUp(boolean takenUp) {
		_takenUp = takenUp;

		if (_clsIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setTakenUp", boolean.class);

				method.invoke(_clsIdeaRemoteModel, takenUp);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getNeedId() {
		return _needId;
	}

	@Override
	public void setNeedId(long needId) {
		_needId = needId;

		if (_clsIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setNeedId", long.class);

				method.invoke(_clsIdeaRemoteModel, needId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getDmFolderName() {
		return _dmFolderName;
	}

	@Override
	public void setDmFolderName(String dmFolderName) {
		_dmFolderName = dmFolderName;

		if (_clsIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setDmFolderName", String.class);

				method.invoke(_clsIdeaRemoteModel, dmFolderName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdFolder() {
		return _idFolder;
	}

	@Override
	public void setIdFolder(long idFolder) {
		_idFolder = idFolder;

		if (_clsIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setIdFolder", long.class);

				method.invoke(_clsIdeaRemoteModel, idFolder);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getRepresentativeImgUrl() {
		return _representativeImgUrl;
	}

	@Override
	public void setRepresentativeImgUrl(String representativeImgUrl) {
		_representativeImgUrl = representativeImgUrl;

		if (_clsIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setRepresentativeImgUrl",
						String.class);

				method.invoke(_clsIdeaRemoteModel, representativeImgUrl);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getIsNeed() {
		return _isNeed;
	}

	@Override
	public boolean isIsNeed() {
		return _isNeed;
	}

	@Override
	public void setIsNeed(boolean isNeed) {
		_isNeed = isNeed;

		if (_clsIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setIsNeed", boolean.class);

				method.invoke(_clsIdeaRemoteModel, isNeed);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getStatus() {
		return _status;
	}

	@Override
	public void setStatus(int status) {
		_status = status;

		if (_clsIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setStatus", int.class);

				method.invoke(_clsIdeaRemoteModel, status);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getStatusByUserId() {
		return _statusByUserId;
	}

	@Override
	public void setStatusByUserId(long statusByUserId) {
		_statusByUserId = statusByUserId;

		if (_clsIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setStatusByUserId", long.class);

				method.invoke(_clsIdeaRemoteModel, statusByUserId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getStatusByUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getStatusByUserId(), "uuid",
			_statusByUserUuid);
	}

	@Override
	public void setStatusByUserUuid(String statusByUserUuid) {
		_statusByUserUuid = statusByUserUuid;
	}

	@Override
	public String getStatusByUserName() {
		return _statusByUserName;
	}

	@Override
	public void setStatusByUserName(String statusByUserName) {
		_statusByUserName = statusByUserName;

		if (_clsIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setStatusByUserName",
						String.class);

				method.invoke(_clsIdeaRemoteModel, statusByUserName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getStatusDate() {
		return _statusDate;
	}

	@Override
	public void setStatusDate(Date statusDate) {
		_statusDate = statusDate;

		if (_clsIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setStatusDate", Date.class);

				method.invoke(_clsIdeaRemoteModel, statusDate);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getChatGroup() {
		return _chatGroup;
	}

	@Override
	public void setChatGroup(long chatGroup) {
		_chatGroup = chatGroup;

		if (_clsIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setChatGroup", long.class);

				method.invoke(_clsIdeaRemoteModel, chatGroup);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getIdeaHashTag() {
		return _ideaHashTag;
	}

	@Override
	public void setIdeaHashTag(String ideaHashTag) {
		_ideaHashTag = ideaHashTag;

		if (_clsIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setIdeaHashTag", String.class);

				method.invoke(_clsIdeaRemoteModel, ideaHashTag);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getIdeaStatus() {
		return _ideaStatus;
	}

	@Override
	public void setIdeaStatus(String ideaStatus) {
		_ideaStatus = ideaStatus;

		if (_clsIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setIdeaStatus", String.class);

				method.invoke(_clsIdeaRemoteModel, ideaStatus);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLanguage() {
		return _language;
	}

	@Override
	public void setLanguage(String language) {
		_language = language;

		if (_clsIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setLanguage", String.class);

				method.invoke(_clsIdeaRemoteModel, language);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getFinalMotivation() {
		return _finalMotivation;
	}

	@Override
	public void setFinalMotivation(String finalMotivation) {
		_finalMotivation = finalMotivation;

		if (_clsIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setFinalMotivation",
						String.class);

				method.invoke(_clsIdeaRemoteModel, finalMotivation);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getEvaluationPassed() {
		return _evaluationPassed;
	}

	@Override
	public boolean isEvaluationPassed() {
		return _evaluationPassed;
	}

	@Override
	public void setEvaluationPassed(boolean evaluationPassed) {
		_evaluationPassed = evaluationPassed;

		if (_clsIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setEvaluationPassed",
						boolean.class);

				method.invoke(_clsIdeaRemoteModel, evaluationPassed);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getCityName() {
		return _cityName;
	}

	@Override
	public void setCityName(String cityName) {
		_cityName = cityName;

		if (_clsIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setCityName", String.class);

				method.invoke(_clsIdeaRemoteModel, cityName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #isApproved}
	 */
	@Override
	public boolean getApproved() {
		return isApproved();
	}

	@Override
	public boolean isApproved() {
		if (getStatus() == WorkflowConstants.STATUS_APPROVED) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean isDenied() {
		if (getStatus() == WorkflowConstants.STATUS_DENIED) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean isDraft() {
		if (getStatus() == WorkflowConstants.STATUS_DRAFT) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean isExpired() {
		if (getStatus() == WorkflowConstants.STATUS_EXPIRED) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean isInactive() {
		if (getStatus() == WorkflowConstants.STATUS_INACTIVE) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean isIncomplete() {
		if (getStatus() == WorkflowConstants.STATUS_INCOMPLETE) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean isPending() {
		if (getStatus() == WorkflowConstants.STATUS_PENDING) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean isScheduled() {
		if (getStatus() == WorkflowConstants.STATUS_SCHEDULED) {
			return true;
		}
		else {
			return false;
		}
	}

	public BaseModel<?> getCLSIdeaRemoteModel() {
		return _clsIdeaRemoteModel;
	}

	public void setCLSIdeaRemoteModel(BaseModel<?> clsIdeaRemoteModel) {
		_clsIdeaRemoteModel = clsIdeaRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _clsIdeaRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_clsIdeaRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			CLSIdeaLocalServiceUtil.addCLSIdea(this);
		}
		else {
			CLSIdeaLocalServiceUtil.updateCLSIdea(this);
		}
	}

	@Override
	public CLSIdea toEscapedModel() {
		return (CLSIdea)ProxyUtil.newProxyInstance(CLSIdea.class.getClassLoader(),
			new Class[] { CLSIdea.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		CLSIdeaClp clone = new CLSIdeaClp();

		clone.setUuid(getUuid());
		clone.setIdeaID(getIdeaID());
		clone.setIdeaTitle(getIdeaTitle());
		clone.setIdeaDescription(getIdeaDescription());
		clone.setDateAdded(getDateAdded());
		clone.setCompanyId(getCompanyId());
		clone.setGroupId(getGroupId());
		clone.setUserId(getUserId());
		clone.setMunicipalityId(getMunicipalityId());
		clone.setMunicipalityOrganizationId(getMunicipalityOrganizationId());
		clone.setChallengeId(getChallengeId());
		clone.setTakenUp(getTakenUp());
		clone.setNeedId(getNeedId());
		clone.setDmFolderName(getDmFolderName());
		clone.setIdFolder(getIdFolder());
		clone.setRepresentativeImgUrl(getRepresentativeImgUrl());
		clone.setIsNeed(getIsNeed());
		clone.setStatus(getStatus());
		clone.setStatusByUserId(getStatusByUserId());
		clone.setStatusByUserName(getStatusByUserName());
		clone.setStatusDate(getStatusDate());
		clone.setChatGroup(getChatGroup());
		clone.setIdeaHashTag(getIdeaHashTag());
		clone.setIdeaStatus(getIdeaStatus());
		clone.setLanguage(getLanguage());
		clone.setFinalMotivation(getFinalMotivation());
		clone.setEvaluationPassed(getEvaluationPassed());
		clone.setCityName(getCityName());

		return clone;
	}

	@Override
	public int compareTo(CLSIdea clsIdea) {
		int value = 0;

		if (getIdeaID() < clsIdea.getIdeaID()) {
			value = -1;
		}
		else if (getIdeaID() > clsIdea.getIdeaID()) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		value = DateUtil.compareTo(getDateAdded(), clsIdea.getDateAdded());

		value = value * -1;

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSIdeaClp)) {
			return false;
		}

		CLSIdeaClp clsIdea = (CLSIdeaClp)obj;

		long primaryKey = clsIdea.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(57);

		sb.append("{uuid=");
		sb.append(getUuid());
		sb.append(", ideaID=");
		sb.append(getIdeaID());
		sb.append(", ideaTitle=");
		sb.append(getIdeaTitle());
		sb.append(", ideaDescription=");
		sb.append(getIdeaDescription());
		sb.append(", dateAdded=");
		sb.append(getDateAdded());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", groupId=");
		sb.append(getGroupId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", municipalityId=");
		sb.append(getMunicipalityId());
		sb.append(", municipalityOrganizationId=");
		sb.append(getMunicipalityOrganizationId());
		sb.append(", challengeId=");
		sb.append(getChallengeId());
		sb.append(", takenUp=");
		sb.append(getTakenUp());
		sb.append(", needId=");
		sb.append(getNeedId());
		sb.append(", dmFolderName=");
		sb.append(getDmFolderName());
		sb.append(", idFolder=");
		sb.append(getIdFolder());
		sb.append(", representativeImgUrl=");
		sb.append(getRepresentativeImgUrl());
		sb.append(", isNeed=");
		sb.append(getIsNeed());
		sb.append(", status=");
		sb.append(getStatus());
		sb.append(", statusByUserId=");
		sb.append(getStatusByUserId());
		sb.append(", statusByUserName=");
		sb.append(getStatusByUserName());
		sb.append(", statusDate=");
		sb.append(getStatusDate());
		sb.append(", chatGroup=");
		sb.append(getChatGroup());
		sb.append(", ideaHashTag=");
		sb.append(getIdeaHashTag());
		sb.append(", ideaStatus=");
		sb.append(getIdeaStatus());
		sb.append(", language=");
		sb.append(getLanguage());
		sb.append(", finalMotivation=");
		sb.append(getFinalMotivation());
		sb.append(", evaluationPassed=");
		sb.append(getEvaluationPassed());
		sb.append(", cityName=");
		sb.append(getCityName());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(88);

		sb.append("<model><model-name>");
		sb.append("it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>uuid</column-name><column-value><![CDATA[");
		sb.append(getUuid());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ideaID</column-name><column-value><![CDATA[");
		sb.append(getIdeaID());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ideaTitle</column-name><column-value><![CDATA[");
		sb.append(getIdeaTitle());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ideaDescription</column-name><column-value><![CDATA[");
		sb.append(getIdeaDescription());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dateAdded</column-name><column-value><![CDATA[");
		sb.append(getDateAdded());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>groupId</column-name><column-value><![CDATA[");
		sb.append(getGroupId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>municipalityId</column-name><column-value><![CDATA[");
		sb.append(getMunicipalityId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>municipalityOrganizationId</column-name><column-value><![CDATA[");
		sb.append(getMunicipalityOrganizationId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>challengeId</column-name><column-value><![CDATA[");
		sb.append(getChallengeId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>takenUp</column-name><column-value><![CDATA[");
		sb.append(getTakenUp());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>needId</column-name><column-value><![CDATA[");
		sb.append(getNeedId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dmFolderName</column-name><column-value><![CDATA[");
		sb.append(getDmFolderName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idFolder</column-name><column-value><![CDATA[");
		sb.append(getIdFolder());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>representativeImgUrl</column-name><column-value><![CDATA[");
		sb.append(getRepresentativeImgUrl());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>isNeed</column-name><column-value><![CDATA[");
		sb.append(getIsNeed());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>status</column-name><column-value><![CDATA[");
		sb.append(getStatus());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>statusByUserId</column-name><column-value><![CDATA[");
		sb.append(getStatusByUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>statusByUserName</column-name><column-value><![CDATA[");
		sb.append(getStatusByUserName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>statusDate</column-name><column-value><![CDATA[");
		sb.append(getStatusDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>chatGroup</column-name><column-value><![CDATA[");
		sb.append(getChatGroup());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ideaHashTag</column-name><column-value><![CDATA[");
		sb.append(getIdeaHashTag());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ideaStatus</column-name><column-value><![CDATA[");
		sb.append(getIdeaStatus());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>language</column-name><column-value><![CDATA[");
		sb.append(getLanguage());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>finalMotivation</column-name><column-value><![CDATA[");
		sb.append(getFinalMotivation());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>evaluationPassed</column-name><column-value><![CDATA[");
		sb.append(getEvaluationPassed());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>cityName</column-name><column-value><![CDATA[");
		sb.append(getCityName());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private String _uuid;
	private long _ideaID;
	private String _ideaTitle;
	private String _ideaDescription;
	private Date _dateAdded;
	private long _companyId;
	private long _groupId;
	private long _userId;
	private String _userUuid;
	private long _municipalityId;
	private long _municipalityOrganizationId;
	private long _challengeId;
	private boolean _takenUp;
	private long _needId;
	private String _dmFolderName;
	private long _idFolder;
	private String _representativeImgUrl;
	private boolean _isNeed;
	private int _status;
	private long _statusByUserId;
	private String _statusByUserUuid;
	private String _statusByUserName;
	private Date _statusDate;
	private long _chatGroup;
	private String _ideaHashTag;
	private String _ideaStatus;
	private String _language;
	private String _finalMotivation;
	private boolean _evaluationPassed;
	private String _cityName;
	private BaseModel<?> _clsIdeaRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.class;
}