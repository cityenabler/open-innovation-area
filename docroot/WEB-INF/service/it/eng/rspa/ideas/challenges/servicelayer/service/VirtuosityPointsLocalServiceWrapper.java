/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link VirtuosityPointsLocalService}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see VirtuosityPointsLocalService
 * @generated
 */
public class VirtuosityPointsLocalServiceWrapper
	implements VirtuosityPointsLocalService,
		ServiceWrapper<VirtuosityPointsLocalService> {
	public VirtuosityPointsLocalServiceWrapper(
		VirtuosityPointsLocalService virtuosityPointsLocalService) {
		_virtuosityPointsLocalService = virtuosityPointsLocalService;
	}

	/**
	* Adds the virtuosity points to the database. Also notifies the appropriate model listeners.
	*
	* @param virtuosityPoints the virtuosity points
	* @return the virtuosity points that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints addVirtuosityPoints(
		it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints virtuosityPoints)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _virtuosityPointsLocalService.addVirtuosityPoints(virtuosityPoints);
	}

	/**
	* Creates a new virtuosity points with the primary key. Does not add the virtuosity points to the database.
	*
	* @param virtuosityPointsPK the primary key for the new virtuosity points
	* @return the new virtuosity points
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints createVirtuosityPoints(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.VirtuosityPointsPK virtuosityPointsPK) {
		return _virtuosityPointsLocalService.createVirtuosityPoints(virtuosityPointsPK);
	}

	/**
	* Deletes the virtuosity points with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param virtuosityPointsPK the primary key of the virtuosity points
	* @return the virtuosity points that was removed
	* @throws PortalException if a virtuosity points with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints deleteVirtuosityPoints(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.VirtuosityPointsPK virtuosityPointsPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _virtuosityPointsLocalService.deleteVirtuosityPoints(virtuosityPointsPK);
	}

	/**
	* Deletes the virtuosity points from the database. Also notifies the appropriate model listeners.
	*
	* @param virtuosityPoints the virtuosity points
	* @return the virtuosity points that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints deleteVirtuosityPoints(
		it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints virtuosityPoints)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _virtuosityPointsLocalService.deleteVirtuosityPoints(virtuosityPoints);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _virtuosityPointsLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _virtuosityPointsLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.VirtuosityPointsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _virtuosityPointsLocalService.dynamicQuery(dynamicQuery, start,
			end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.VirtuosityPointsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _virtuosityPointsLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _virtuosityPointsLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _virtuosityPointsLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints fetchVirtuosityPoints(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.VirtuosityPointsPK virtuosityPointsPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _virtuosityPointsLocalService.fetchVirtuosityPoints(virtuosityPointsPK);
	}

	/**
	* Returns the virtuosity points with the primary key.
	*
	* @param virtuosityPointsPK the primary key of the virtuosity points
	* @return the virtuosity points
	* @throws PortalException if a virtuosity points with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints getVirtuosityPoints(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.VirtuosityPointsPK virtuosityPointsPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _virtuosityPointsLocalService.getVirtuosityPoints(virtuosityPointsPK);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _virtuosityPointsLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the virtuosity pointses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.VirtuosityPointsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of virtuosity pointses
	* @param end the upper bound of the range of virtuosity pointses (not inclusive)
	* @return the range of virtuosity pointses
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints> getVirtuosityPointses(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _virtuosityPointsLocalService.getVirtuosityPointses(start, end);
	}

	/**
	* Returns the number of virtuosity pointses.
	*
	* @return the number of virtuosity pointses
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getVirtuosityPointsesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _virtuosityPointsLocalService.getVirtuosityPointsesCount();
	}

	/**
	* Updates the virtuosity points in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param virtuosityPoints the virtuosity points
	* @return the virtuosity points that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints updateVirtuosityPoints(
		it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints virtuosityPoints)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _virtuosityPointsLocalService.updateVirtuosityPoints(virtuosityPoints);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _virtuosityPointsLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_virtuosityPointsLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _virtuosityPointsLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints> getVirtuosityPointByChallengeId(
		long challengeId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _virtuosityPointsLocalService.getVirtuosityPointByChallengeId(challengeId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public VirtuosityPointsLocalService getWrappedVirtuosityPointsLocalService() {
		return _virtuosityPointsLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedVirtuosityPointsLocalService(
		VirtuosityPointsLocalService virtuosityPointsLocalService) {
		_virtuosityPointsLocalService = virtuosityPointsLocalService;
	}

	@Override
	public VirtuosityPointsLocalService getWrappedService() {
		return _virtuosityPointsLocalService;
	}

	@Override
	public void setWrappedService(
		VirtuosityPointsLocalService virtuosityPointsLocalService) {
		_virtuosityPointsLocalService = virtuosityPointsLocalService;
	}

	private VirtuosityPointsLocalService _virtuosityPointsLocalService;
}