/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.GESelectedPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.eng.rspa.ideas.challenges.servicelayer.service.http.GESelectedServiceSoap}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.http.GESelectedServiceSoap
 * @generated
 */
public class GESelectedSoap implements Serializable {
	public static GESelectedSoap toSoapModel(GESelected model) {
		GESelectedSoap soapModel = new GESelectedSoap();

		soapModel.setIdeaId(model.getIdeaId());
		soapModel.setNid(model.getNid());
		soapModel.setUrl(model.getUrl());
		soapModel.setIcon(model.getIcon());
		soapModel.setTitle(model.getTitle());
		soapModel.setShortDescription(model.getShortDescription());
		soapModel.setLabel(model.getLabel());
		soapModel.setRank(model.getRank());
		soapModel.setChapter(model.getChapter());

		return soapModel;
	}

	public static GESelectedSoap[] toSoapModels(GESelected[] models) {
		GESelectedSoap[] soapModels = new GESelectedSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static GESelectedSoap[][] toSoapModels(GESelected[][] models) {
		GESelectedSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new GESelectedSoap[models.length][models[0].length];
		}
		else {
			soapModels = new GESelectedSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static GESelectedSoap[] toSoapModels(List<GESelected> models) {
		List<GESelectedSoap> soapModels = new ArrayList<GESelectedSoap>(models.size());

		for (GESelected model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new GESelectedSoap[soapModels.size()]);
	}

	public GESelectedSoap() {
	}

	public GESelectedPK getPrimaryKey() {
		return new GESelectedPK(_ideaId, _nid);
	}

	public void setPrimaryKey(GESelectedPK pk) {
		setIdeaId(pk.ideaId);
		setNid(pk.nid);
	}

	public long getIdeaId() {
		return _ideaId;
	}

	public void setIdeaId(long ideaId) {
		_ideaId = ideaId;
	}

	public String getNid() {
		return _nid;
	}

	public void setNid(String nid) {
		_nid = nid;
	}

	public String getUrl() {
		return _url;
	}

	public void setUrl(String url) {
		_url = url;
	}

	public String getIcon() {
		return _icon;
	}

	public void setIcon(String icon) {
		_icon = icon;
	}

	public String getTitle() {
		return _title;
	}

	public void setTitle(String title) {
		_title = title;
	}

	public String getShortDescription() {
		return _shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		_shortDescription = shortDescription;
	}

	public String getLabel() {
		return _label;
	}

	public void setLabel(String label) {
		_label = label;
	}

	public String getRank() {
		return _rank;
	}

	public void setRank(String rank) {
		_rank = rank;
	}

	public String getChapter() {
		return _chapter;
	}

	public void setChapter(String chapter) {
		_chapter = chapter;
	}

	private long _ideaId;
	private String _nid;
	private String _url;
	private String _icon;
	private String _title;
	private String _shortDescription;
	private String _label;
	private String _rank;
	private String _chapter;
}