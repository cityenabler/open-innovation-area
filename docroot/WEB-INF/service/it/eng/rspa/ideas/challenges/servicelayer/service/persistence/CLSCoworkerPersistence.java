/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker;

/**
 * The persistence interface for the c l s coworker service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSCoworkerPersistenceImpl
 * @see CLSCoworkerUtil
 * @generated
 */
public interface CLSCoworkerPersistence extends BasePersistence<CLSCoworker> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CLSCoworkerUtil} to access the c l s coworker persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the c l s coworkers where ideaID = &#63;.
	*
	* @param ideaID the idea i d
	* @return the matching c l s coworkers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker> findByIdeaID(
		long ideaID) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the c l s coworkers where ideaID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCoworkerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaID the idea i d
	* @param start the lower bound of the range of c l s coworkers
	* @param end the upper bound of the range of c l s coworkers (not inclusive)
	* @return the range of matching c l s coworkers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker> findByIdeaID(
		long ideaID, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the c l s coworkers where ideaID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCoworkerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaID the idea i d
	* @param start the lower bound of the range of c l s coworkers
	* @param end the upper bound of the range of c l s coworkers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching c l s coworkers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker> findByIdeaID(
		long ideaID, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first c l s coworker in the ordered set where ideaID = &#63;.
	*
	* @param ideaID the idea i d
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s coworker
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException if a matching c l s coworker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker findByIdeaID_First(
		long ideaID,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException;

	/**
	* Returns the first c l s coworker in the ordered set where ideaID = &#63;.
	*
	* @param ideaID the idea i d
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s coworker, or <code>null</code> if a matching c l s coworker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker fetchByIdeaID_First(
		long ideaID,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last c l s coworker in the ordered set where ideaID = &#63;.
	*
	* @param ideaID the idea i d
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s coworker
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException if a matching c l s coworker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker findByIdeaID_Last(
		long ideaID,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException;

	/**
	* Returns the last c l s coworker in the ordered set where ideaID = &#63;.
	*
	* @param ideaID the idea i d
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s coworker, or <code>null</code> if a matching c l s coworker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker fetchByIdeaID_Last(
		long ideaID,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the c l s coworkers before and after the current c l s coworker in the ordered set where ideaID = &#63;.
	*
	* @param coworkerId the primary key of the current c l s coworker
	* @param ideaID the idea i d
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next c l s coworker
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException if a c l s coworker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker[] findByIdeaID_PrevAndNext(
		long coworkerId, long ideaID,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException;

	/**
	* Removes all the c l s coworkers where ideaID = &#63; from the database.
	*
	* @param ideaID the idea i d
	* @throws SystemException if a system exception occurred
	*/
	public void removeByIdeaID(long ideaID)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of c l s coworkers where ideaID = &#63;.
	*
	* @param ideaID the idea i d
	* @return the number of matching c l s coworkers
	* @throws SystemException if a system exception occurred
	*/
	public int countByIdeaID(long ideaID)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the c l s coworkers where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching c l s coworkers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker> findByUserID(
		long userId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the c l s coworkers where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCoworkerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of c l s coworkers
	* @param end the upper bound of the range of c l s coworkers (not inclusive)
	* @return the range of matching c l s coworkers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker> findByUserID(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the c l s coworkers where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCoworkerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of c l s coworkers
	* @param end the upper bound of the range of c l s coworkers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching c l s coworkers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker> findByUserID(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first c l s coworker in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s coworker
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException if a matching c l s coworker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker findByUserID_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException;

	/**
	* Returns the first c l s coworker in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s coworker, or <code>null</code> if a matching c l s coworker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker fetchByUserID_First(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last c l s coworker in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s coworker
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException if a matching c l s coworker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker findByUserID_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException;

	/**
	* Returns the last c l s coworker in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s coworker, or <code>null</code> if a matching c l s coworker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker fetchByUserID_Last(
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the c l s coworkers before and after the current c l s coworker in the ordered set where userId = &#63;.
	*
	* @param coworkerId the primary key of the current c l s coworker
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next c l s coworker
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException if a c l s coworker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker[] findByUserID_PrevAndNext(
		long coworkerId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException;

	/**
	* Removes all the c l s coworkers where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByUserID(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of c l s coworkers where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching c l s coworkers
	* @throws SystemException if a system exception occurred
	*/
	public int countByUserID(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the c l s coworker where ideaID = &#63; and userId = &#63; or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException} if it could not be found.
	*
	* @param ideaID the idea i d
	* @param userId the user ID
	* @return the matching c l s coworker
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException if a matching c l s coworker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker findByIdeaIDAndUserID(
		long ideaID, long userId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException;

	/**
	* Returns the c l s coworker where ideaID = &#63; and userId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param ideaID the idea i d
	* @param userId the user ID
	* @return the matching c l s coworker, or <code>null</code> if a matching c l s coworker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker fetchByIdeaIDAndUserID(
		long ideaID, long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the c l s coworker where ideaID = &#63; and userId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param ideaID the idea i d
	* @param userId the user ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching c l s coworker, or <code>null</code> if a matching c l s coworker could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker fetchByIdeaIDAndUserID(
		long ideaID, long userId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the c l s coworker where ideaID = &#63; and userId = &#63; from the database.
	*
	* @param ideaID the idea i d
	* @param userId the user ID
	* @return the c l s coworker that was removed
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker removeByIdeaIDAndUserID(
		long ideaID, long userId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException;

	/**
	* Returns the number of c l s coworkers where ideaID = &#63; and userId = &#63;.
	*
	* @param ideaID the idea i d
	* @param userId the user ID
	* @return the number of matching c l s coworkers
	* @throws SystemException if a system exception occurred
	*/
	public int countByIdeaIDAndUserID(long ideaID, long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the c l s coworker in the entity cache if it is enabled.
	*
	* @param clsCoworker the c l s coworker
	*/
	public void cacheResult(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker clsCoworker);

	/**
	* Caches the c l s coworkers in the entity cache if it is enabled.
	*
	* @param clsCoworkers the c l s coworkers
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker> clsCoworkers);

	/**
	* Creates a new c l s coworker with the primary key. Does not add the c l s coworker to the database.
	*
	* @param coworkerId the primary key for the new c l s coworker
	* @return the new c l s coworker
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker create(
		long coworkerId);

	/**
	* Removes the c l s coworker with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param coworkerId the primary key of the c l s coworker
	* @return the c l s coworker that was removed
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException if a c l s coworker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker remove(
		long coworkerId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException;

	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker clsCoworker)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the c l s coworker with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException} if it could not be found.
	*
	* @param coworkerId the primary key of the c l s coworker
	* @return the c l s coworker
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException if a c l s coworker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker findByPrimaryKey(
		long coworkerId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException;

	/**
	* Returns the c l s coworker with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param coworkerId the primary key of the c l s coworker
	* @return the c l s coworker, or <code>null</code> if a c l s coworker with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker fetchByPrimaryKey(
		long coworkerId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the c l s coworkers.
	*
	* @return the c l s coworkers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the c l s coworkers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCoworkerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s coworkers
	* @param end the upper bound of the range of c l s coworkers (not inclusive)
	* @return the range of c l s coworkers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the c l s coworkers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCoworkerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s coworkers
	* @param end the upper bound of the range of c l s coworkers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of c l s coworkers
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the c l s coworkers from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of c l s coworkers.
	*
	* @return the number of c l s coworkers
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}