/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.ideas.challenges.servicelayer.service.CLSArtifactsLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSArtifactsPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 */
public class CLSArtifactsClp extends BaseModelImpl<CLSArtifacts>
	implements CLSArtifacts {
	public CLSArtifactsClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return CLSArtifacts.class;
	}

	@Override
	public String getModelClassName() {
		return CLSArtifacts.class.getName();
	}

	@Override
	public CLSArtifactsPK getPrimaryKey() {
		return new CLSArtifactsPK(_ideaId, _artifactId);
	}

	@Override
	public void setPrimaryKey(CLSArtifactsPK primaryKey) {
		setIdeaId(primaryKey.ideaId);
		setArtifactId(primaryKey.artifactId);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new CLSArtifactsPK(_ideaId, _artifactId);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((CLSArtifactsPK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ideaId", getIdeaId());
		attributes.put("artifactId", getArtifactId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ideaId = (Long)attributes.get("ideaId");

		if (ideaId != null) {
			setIdeaId(ideaId);
		}

		Long artifactId = (Long)attributes.get("artifactId");

		if (artifactId != null) {
			setArtifactId(artifactId);
		}
	}

	@Override
	public long getIdeaId() {
		return _ideaId;
	}

	@Override
	public void setIdeaId(long ideaId) {
		_ideaId = ideaId;

		if (_clsArtifactsRemoteModel != null) {
			try {
				Class<?> clazz = _clsArtifactsRemoteModel.getClass();

				Method method = clazz.getMethod("setIdeaId", long.class);

				method.invoke(_clsArtifactsRemoteModel, ideaId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getArtifactId() {
		return _artifactId;
	}

	@Override
	public void setArtifactId(long artifactId) {
		_artifactId = artifactId;

		if (_clsArtifactsRemoteModel != null) {
			try {
				Class<?> clazz = _clsArtifactsRemoteModel.getClass();

				Method method = clazz.getMethod("setArtifactId", long.class);

				method.invoke(_clsArtifactsRemoteModel, artifactId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getCLSArtifactsRemoteModel() {
		return _clsArtifactsRemoteModel;
	}

	public void setCLSArtifactsRemoteModel(BaseModel<?> clsArtifactsRemoteModel) {
		_clsArtifactsRemoteModel = clsArtifactsRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _clsArtifactsRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_clsArtifactsRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			CLSArtifactsLocalServiceUtil.addCLSArtifacts(this);
		}
		else {
			CLSArtifactsLocalServiceUtil.updateCLSArtifacts(this);
		}
	}

	@Override
	public CLSArtifacts toEscapedModel() {
		return (CLSArtifacts)ProxyUtil.newProxyInstance(CLSArtifacts.class.getClassLoader(),
			new Class[] { CLSArtifacts.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		CLSArtifactsClp clone = new CLSArtifactsClp();

		clone.setIdeaId(getIdeaId());
		clone.setArtifactId(getArtifactId());

		return clone;
	}

	@Override
	public int compareTo(CLSArtifacts clsArtifacts) {
		CLSArtifactsPK primaryKey = clsArtifacts.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSArtifactsClp)) {
			return false;
		}

		CLSArtifactsClp clsArtifacts = (CLSArtifactsClp)obj;

		CLSArtifactsPK primaryKey = clsArtifacts.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{ideaId=");
		sb.append(getIdeaId());
		sb.append(", artifactId=");
		sb.append(getArtifactId());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(10);

		sb.append("<model><model-name>");
		sb.append(
			"it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>ideaId</column-name><column-value><![CDATA[");
		sb.append(getIdeaId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>artifactId</column-name><column-value><![CDATA[");
		sb.append(getArtifactId());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _ideaId;
	private long _artifactId;
	private BaseModel<?> _clsArtifactsRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.class;
}