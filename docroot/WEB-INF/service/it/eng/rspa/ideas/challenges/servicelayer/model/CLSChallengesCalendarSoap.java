/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.eng.rspa.ideas.challenges.servicelayer.service.http.CLSChallengesCalendarServiceSoap}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.http.CLSChallengesCalendarServiceSoap
 * @generated
 */
public class CLSChallengesCalendarSoap implements Serializable {
	public static CLSChallengesCalendarSoap toSoapModel(
		CLSChallengesCalendar model) {
		CLSChallengesCalendarSoap soapModel = new CLSChallengesCalendarSoap();

		soapModel.setChallengesCalendarId(model.getChallengesCalendarId());
		soapModel.setReferenceCalendarId(model.getReferenceCalendarId());

		return soapModel;
	}

	public static CLSChallengesCalendarSoap[] toSoapModels(
		CLSChallengesCalendar[] models) {
		CLSChallengesCalendarSoap[] soapModels = new CLSChallengesCalendarSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CLSChallengesCalendarSoap[][] toSoapModels(
		CLSChallengesCalendar[][] models) {
		CLSChallengesCalendarSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CLSChallengesCalendarSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CLSChallengesCalendarSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CLSChallengesCalendarSoap[] toSoapModels(
		List<CLSChallengesCalendar> models) {
		List<CLSChallengesCalendarSoap> soapModels = new ArrayList<CLSChallengesCalendarSoap>(models.size());

		for (CLSChallengesCalendar model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CLSChallengesCalendarSoap[soapModels.size()]);
	}

	public CLSChallengesCalendarSoap() {
	}

	public long getPrimaryKey() {
		return _challengesCalendarId;
	}

	public void setPrimaryKey(long pk) {
		setChallengesCalendarId(pk);
	}

	public long getChallengesCalendarId() {
		return _challengesCalendarId;
	}

	public void setChallengesCalendarId(long challengesCalendarId) {
		_challengesCalendarId = challengesCalendarId;
	}

	public long getReferenceCalendarId() {
		return _referenceCalendarId;
	}

	public void setReferenceCalendarId(long referenceCalendarId) {
		_referenceCalendarId = referenceCalendarId;
	}

	private long _challengesCalendarId;
	private long _referenceCalendarId;
}