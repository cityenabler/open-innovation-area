/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.eng.rspa.ideas.challenges.servicelayer.model.GESelected;

import java.util.List;

/**
 * The persistence utility for the g e selected service. This utility wraps {@link GESelectedPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see GESelectedPersistence
 * @see GESelectedPersistenceImpl
 * @generated
 */
public class GESelectedUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(GESelected geSelected) {
		getPersistence().clearCache(geSelected);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<GESelected> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<GESelected> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<GESelected> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static GESelected update(GESelected geSelected)
		throws SystemException {
		return getPersistence().update(geSelected);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static GESelected update(GESelected geSelected,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(geSelected, serviceContext);
	}

	/**
	* Returns all the g e selecteds where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @return the matching g e selecteds
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.GESelected> findBygesByIdeaId(
		long ideaId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBygesByIdeaId(ideaId);
	}

	/**
	* Returns a range of all the g e selecteds where ideaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.GESelectedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param start the lower bound of the range of g e selecteds
	* @param end the upper bound of the range of g e selecteds (not inclusive)
	* @return the range of matching g e selecteds
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.GESelected> findBygesByIdeaId(
		long ideaId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBygesByIdeaId(ideaId, start, end);
	}

	/**
	* Returns an ordered range of all the g e selecteds where ideaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.GESelectedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param start the lower bound of the range of g e selecteds
	* @param end the upper bound of the range of g e selecteds (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching g e selecteds
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.GESelected> findBygesByIdeaId(
		long ideaId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBygesByIdeaId(ideaId, start, end, orderByComparator);
	}

	/**
	* Returns the first g e selected in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching g e selected
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException if a matching g e selected could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.GESelected findBygesByIdeaId_First(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException {
		return getPersistence()
				   .findBygesByIdeaId_First(ideaId, orderByComparator);
	}

	/**
	* Returns the first g e selected in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching g e selected, or <code>null</code> if a matching g e selected could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.GESelected fetchBygesByIdeaId_First(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBygesByIdeaId_First(ideaId, orderByComparator);
	}

	/**
	* Returns the last g e selected in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching g e selected
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException if a matching g e selected could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.GESelected findBygesByIdeaId_Last(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException {
		return getPersistence().findBygesByIdeaId_Last(ideaId, orderByComparator);
	}

	/**
	* Returns the last g e selected in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching g e selected, or <code>null</code> if a matching g e selected could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.GESelected fetchBygesByIdeaId_Last(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBygesByIdeaId_Last(ideaId, orderByComparator);
	}

	/**
	* Returns the g e selecteds before and after the current g e selected in the ordered set where ideaId = &#63;.
	*
	* @param geSelectedPK the primary key of the current g e selected
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next g e selected
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException if a g e selected with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.GESelected[] findBygesByIdeaId_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.GESelectedPK geSelectedPK,
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException {
		return getPersistence()
				   .findBygesByIdeaId_PrevAndNext(geSelectedPK, ideaId,
			orderByComparator);
	}

	/**
	* Removes all the g e selecteds where ideaId = &#63; from the database.
	*
	* @param ideaId the idea ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBygesByIdeaId(long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBygesByIdeaId(ideaId);
	}

	/**
	* Returns the number of g e selecteds where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @return the number of matching g e selecteds
	* @throws SystemException if a system exception occurred
	*/
	public static int countBygesByIdeaId(long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBygesByIdeaId(ideaId);
	}

	/**
	* Returns all the g e selecteds where nid = &#63;.
	*
	* @param nid the nid
	* @return the matching g e selecteds
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.GESelected> findBygesByNid(
		java.lang.String nid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBygesByNid(nid);
	}

	/**
	* Returns a range of all the g e selecteds where nid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.GESelectedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nid the nid
	* @param start the lower bound of the range of g e selecteds
	* @param end the upper bound of the range of g e selecteds (not inclusive)
	* @return the range of matching g e selecteds
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.GESelected> findBygesByNid(
		java.lang.String nid, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBygesByNid(nid, start, end);
	}

	/**
	* Returns an ordered range of all the g e selecteds where nid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.GESelectedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nid the nid
	* @param start the lower bound of the range of g e selecteds
	* @param end the upper bound of the range of g e selecteds (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching g e selecteds
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.GESelected> findBygesByNid(
		java.lang.String nid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBygesByNid(nid, start, end, orderByComparator);
	}

	/**
	* Returns the first g e selected in the ordered set where nid = &#63;.
	*
	* @param nid the nid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching g e selected
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException if a matching g e selected could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.GESelected findBygesByNid_First(
		java.lang.String nid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException {
		return getPersistence().findBygesByNid_First(nid, orderByComparator);
	}

	/**
	* Returns the first g e selected in the ordered set where nid = &#63;.
	*
	* @param nid the nid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching g e selected, or <code>null</code> if a matching g e selected could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.GESelected fetchBygesByNid_First(
		java.lang.String nid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBygesByNid_First(nid, orderByComparator);
	}

	/**
	* Returns the last g e selected in the ordered set where nid = &#63;.
	*
	* @param nid the nid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching g e selected
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException if a matching g e selected could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.GESelected findBygesByNid_Last(
		java.lang.String nid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException {
		return getPersistence().findBygesByNid_Last(nid, orderByComparator);
	}

	/**
	* Returns the last g e selected in the ordered set where nid = &#63;.
	*
	* @param nid the nid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching g e selected, or <code>null</code> if a matching g e selected could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.GESelected fetchBygesByNid_Last(
		java.lang.String nid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBygesByNid_Last(nid, orderByComparator);
	}

	/**
	* Returns the g e selecteds before and after the current g e selected in the ordered set where nid = &#63;.
	*
	* @param geSelectedPK the primary key of the current g e selected
	* @param nid the nid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next g e selected
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException if a g e selected with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.GESelected[] findBygesByNid_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.GESelectedPK geSelectedPK,
		java.lang.String nid,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException {
		return getPersistence()
				   .findBygesByNid_PrevAndNext(geSelectedPK, nid,
			orderByComparator);
	}

	/**
	* Removes all the g e selecteds where nid = &#63; from the database.
	*
	* @param nid the nid
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBygesByNid(java.lang.String nid)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBygesByNid(nid);
	}

	/**
	* Returns the number of g e selecteds where nid = &#63;.
	*
	* @param nid the nid
	* @return the number of matching g e selecteds
	* @throws SystemException if a system exception occurred
	*/
	public static int countBygesByNid(java.lang.String nid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBygesByNid(nid);
	}

	/**
	* Returns the g e selected where ideaId = &#63; and nid = &#63; or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException} if it could not be found.
	*
	* @param ideaId the idea ID
	* @param nid the nid
	* @return the matching g e selected
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException if a matching g e selected could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.GESelected findByideaIdAndNid(
		long ideaId, java.lang.String nid)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException {
		return getPersistence().findByideaIdAndNid(ideaId, nid);
	}

	/**
	* Returns the g e selected where ideaId = &#63; and nid = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param ideaId the idea ID
	* @param nid the nid
	* @return the matching g e selected, or <code>null</code> if a matching g e selected could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.GESelected fetchByideaIdAndNid(
		long ideaId, java.lang.String nid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByideaIdAndNid(ideaId, nid);
	}

	/**
	* Returns the g e selected where ideaId = &#63; and nid = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param ideaId the idea ID
	* @param nid the nid
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching g e selected, or <code>null</code> if a matching g e selected could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.GESelected fetchByideaIdAndNid(
		long ideaId, java.lang.String nid, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByideaIdAndNid(ideaId, nid, retrieveFromCache);
	}

	/**
	* Removes the g e selected where ideaId = &#63; and nid = &#63; from the database.
	*
	* @param ideaId the idea ID
	* @param nid the nid
	* @return the g e selected that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.GESelected removeByideaIdAndNid(
		long ideaId, java.lang.String nid)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException {
		return getPersistence().removeByideaIdAndNid(ideaId, nid);
	}

	/**
	* Returns the number of g e selecteds where ideaId = &#63; and nid = &#63;.
	*
	* @param ideaId the idea ID
	* @param nid the nid
	* @return the number of matching g e selecteds
	* @throws SystemException if a system exception occurred
	*/
	public static int countByideaIdAndNid(long ideaId, java.lang.String nid)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByideaIdAndNid(ideaId, nid);
	}

	/**
	* Caches the g e selected in the entity cache if it is enabled.
	*
	* @param geSelected the g e selected
	*/
	public static void cacheResult(
		it.eng.rspa.ideas.challenges.servicelayer.model.GESelected geSelected) {
		getPersistence().cacheResult(geSelected);
	}

	/**
	* Caches the g e selecteds in the entity cache if it is enabled.
	*
	* @param geSelecteds the g e selecteds
	*/
	public static void cacheResult(
		java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.GESelected> geSelecteds) {
		getPersistence().cacheResult(geSelecteds);
	}

	/**
	* Creates a new g e selected with the primary key. Does not add the g e selected to the database.
	*
	* @param geSelectedPK the primary key for the new g e selected
	* @return the new g e selected
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.GESelected create(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.GESelectedPK geSelectedPK) {
		return getPersistence().create(geSelectedPK);
	}

	/**
	* Removes the g e selected with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param geSelectedPK the primary key of the g e selected
	* @return the g e selected that was removed
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException if a g e selected with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.GESelected remove(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.GESelectedPK geSelectedPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException {
		return getPersistence().remove(geSelectedPK);
	}

	public static it.eng.rspa.ideas.challenges.servicelayer.model.GESelected updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.GESelected geSelected)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(geSelected);
	}

	/**
	* Returns the g e selected with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException} if it could not be found.
	*
	* @param geSelectedPK the primary key of the g e selected
	* @return the g e selected
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException if a g e selected with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.GESelected findByPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.GESelectedPK geSelectedPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException {
		return getPersistence().findByPrimaryKey(geSelectedPK);
	}

	/**
	* Returns the g e selected with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param geSelectedPK the primary key of the g e selected
	* @return the g e selected, or <code>null</code> if a g e selected with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.GESelected fetchByPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.GESelectedPK geSelectedPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(geSelectedPK);
	}

	/**
	* Returns all the g e selecteds.
	*
	* @return the g e selecteds
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.GESelected> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the g e selecteds.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.GESelectedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of g e selecteds
	* @param end the upper bound of the range of g e selecteds (not inclusive)
	* @return the range of g e selecteds
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.GESelected> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the g e selecteds.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.GESelectedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of g e selecteds
	* @param end the upper bound of the range of g e selecteds (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of g e selecteds
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.GESelected> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the g e selecteds from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of g e selecteds.
	*
	* @return the number of g e selecteds
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static GESelectedPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (GESelectedPersistence)PortletBeanLocatorUtil.locate(it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.getServletContextName(),
					GESelectedPersistence.class.getName());

			ReferenceRegistry.registerReference(GESelectedUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(GESelectedPersistence persistence) {
	}

	private static GESelectedPersistence _persistence;
}