/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer;
import it.eng.rspa.ideas.challenges.servicelayer.service.IdeaEvaluationLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.IdeaEvaluationPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 */
public class IdeaEvaluationClp extends BaseModelImpl<IdeaEvaluation>
	implements IdeaEvaluation {
	public IdeaEvaluationClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return IdeaEvaluation.class;
	}

	@Override
	public String getModelClassName() {
		return IdeaEvaluation.class.getName();
	}

	@Override
	public IdeaEvaluationPK getPrimaryKey() {
		return new IdeaEvaluationPK(_criteriaId, _ideaId);
	}

	@Override
	public void setPrimaryKey(IdeaEvaluationPK primaryKey) {
		setCriteriaId(primaryKey.criteriaId);
		setIdeaId(primaryKey.ideaId);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new IdeaEvaluationPK(_criteriaId, _ideaId);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((IdeaEvaluationPK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("criteriaId", getCriteriaId());
		attributes.put("ideaId", getIdeaId());
		attributes.put("motivation", getMotivation());
		attributes.put("passed", getPassed());
		attributes.put("score", getScore());
		attributes.put("date", getDate());
		attributes.put("authorId", getAuthorId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long criteriaId = (Long)attributes.get("criteriaId");

		if (criteriaId != null) {
			setCriteriaId(criteriaId);
		}

		Long ideaId = (Long)attributes.get("ideaId");

		if (ideaId != null) {
			setIdeaId(ideaId);
		}

		String motivation = (String)attributes.get("motivation");

		if (motivation != null) {
			setMotivation(motivation);
		}

		Boolean passed = (Boolean)attributes.get("passed");

		if (passed != null) {
			setPassed(passed);
		}

		Double score = (Double)attributes.get("score");

		if (score != null) {
			setScore(score);
		}

		Date date = (Date)attributes.get("date");

		if (date != null) {
			setDate(date);
		}

		Long authorId = (Long)attributes.get("authorId");

		if (authorId != null) {
			setAuthorId(authorId);
		}
	}

	@Override
	public long getCriteriaId() {
		return _criteriaId;
	}

	@Override
	public void setCriteriaId(long criteriaId) {
		_criteriaId = criteriaId;

		if (_ideaEvaluationRemoteModel != null) {
			try {
				Class<?> clazz = _ideaEvaluationRemoteModel.getClass();

				Method method = clazz.getMethod("setCriteriaId", long.class);

				method.invoke(_ideaEvaluationRemoteModel, criteriaId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdeaId() {
		return _ideaId;
	}

	@Override
	public void setIdeaId(long ideaId) {
		_ideaId = ideaId;

		if (_ideaEvaluationRemoteModel != null) {
			try {
				Class<?> clazz = _ideaEvaluationRemoteModel.getClass();

				Method method = clazz.getMethod("setIdeaId", long.class);

				method.invoke(_ideaEvaluationRemoteModel, ideaId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getMotivation() {
		return _motivation;
	}

	@Override
	public void setMotivation(String motivation) {
		_motivation = motivation;

		if (_ideaEvaluationRemoteModel != null) {
			try {
				Class<?> clazz = _ideaEvaluationRemoteModel.getClass();

				Method method = clazz.getMethod("setMotivation", String.class);

				method.invoke(_ideaEvaluationRemoteModel, motivation);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getPassed() {
		return _passed;
	}

	@Override
	public boolean isPassed() {
		return _passed;
	}

	@Override
	public void setPassed(boolean passed) {
		_passed = passed;

		if (_ideaEvaluationRemoteModel != null) {
			try {
				Class<?> clazz = _ideaEvaluationRemoteModel.getClass();

				Method method = clazz.getMethod("setPassed", boolean.class);

				method.invoke(_ideaEvaluationRemoteModel, passed);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public double getScore() {
		return _score;
	}

	@Override
	public void setScore(double score) {
		_score = score;

		if (_ideaEvaluationRemoteModel != null) {
			try {
				Class<?> clazz = _ideaEvaluationRemoteModel.getClass();

				Method method = clazz.getMethod("setScore", double.class);

				method.invoke(_ideaEvaluationRemoteModel, score);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDate() {
		return _date;
	}

	@Override
	public void setDate(Date date) {
		_date = date;

		if (_ideaEvaluationRemoteModel != null) {
			try {
				Class<?> clazz = _ideaEvaluationRemoteModel.getClass();

				Method method = clazz.getMethod("setDate", Date.class);

				method.invoke(_ideaEvaluationRemoteModel, date);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getAuthorId() {
		return _authorId;
	}

	@Override
	public void setAuthorId(long authorId) {
		_authorId = authorId;

		if (_ideaEvaluationRemoteModel != null) {
			try {
				Class<?> clazz = _ideaEvaluationRemoteModel.getClass();

				Method method = clazz.getMethod("setAuthorId", long.class);

				method.invoke(_ideaEvaluationRemoteModel, authorId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getIdeaEvaluationRemoteModel() {
		return _ideaEvaluationRemoteModel;
	}

	public void setIdeaEvaluationRemoteModel(
		BaseModel<?> ideaEvaluationRemoteModel) {
		_ideaEvaluationRemoteModel = ideaEvaluationRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _ideaEvaluationRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_ideaEvaluationRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			IdeaEvaluationLocalServiceUtil.addIdeaEvaluation(this);
		}
		else {
			IdeaEvaluationLocalServiceUtil.updateIdeaEvaluation(this);
		}
	}

	@Override
	public IdeaEvaluation toEscapedModel() {
		return (IdeaEvaluation)ProxyUtil.newProxyInstance(IdeaEvaluation.class.getClassLoader(),
			new Class[] { IdeaEvaluation.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		IdeaEvaluationClp clone = new IdeaEvaluationClp();

		clone.setCriteriaId(getCriteriaId());
		clone.setIdeaId(getIdeaId());
		clone.setMotivation(getMotivation());
		clone.setPassed(getPassed());
		clone.setScore(getScore());
		clone.setDate(getDate());
		clone.setAuthorId(getAuthorId());

		return clone;
	}

	@Override
	public int compareTo(IdeaEvaluation ideaEvaluation) {
		IdeaEvaluationPK primaryKey = ideaEvaluation.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof IdeaEvaluationClp)) {
			return false;
		}

		IdeaEvaluationClp ideaEvaluation = (IdeaEvaluationClp)obj;

		IdeaEvaluationPK primaryKey = ideaEvaluation.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{criteriaId=");
		sb.append(getCriteriaId());
		sb.append(", ideaId=");
		sb.append(getIdeaId());
		sb.append(", motivation=");
		sb.append(getMotivation());
		sb.append(", passed=");
		sb.append(getPassed());
		sb.append(", score=");
		sb.append(getScore());
		sb.append(", date=");
		sb.append(getDate());
		sb.append(", authorId=");
		sb.append(getAuthorId());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(25);

		sb.append("<model><model-name>");
		sb.append(
			"it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>criteriaId</column-name><column-value><![CDATA[");
		sb.append(getCriteriaId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ideaId</column-name><column-value><![CDATA[");
		sb.append(getIdeaId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>motivation</column-name><column-value><![CDATA[");
		sb.append(getMotivation());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>passed</column-name><column-value><![CDATA[");
		sb.append(getPassed());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>score</column-name><column-value><![CDATA[");
		sb.append(getScore());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>date</column-name><column-value><![CDATA[");
		sb.append(getDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>authorId</column-name><column-value><![CDATA[");
		sb.append(getAuthorId());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _criteriaId;
	private long _ideaId;
	private String _motivation;
	private boolean _passed;
	private double _score;
	private Date _date;
	private long _authorId;
	private BaseModel<?> _ideaEvaluationRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.class;
}