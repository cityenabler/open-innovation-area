/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CLSIdeaPoiLocalService}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSIdeaPoiLocalService
 * @generated
 */
public class CLSIdeaPoiLocalServiceWrapper implements CLSIdeaPoiLocalService,
	ServiceWrapper<CLSIdeaPoiLocalService> {
	public CLSIdeaPoiLocalServiceWrapper(
		CLSIdeaPoiLocalService clsIdeaPoiLocalService) {
		_clsIdeaPoiLocalService = clsIdeaPoiLocalService;
	}

	/**
	* Adds the c l s idea poi to the database. Also notifies the appropriate model listeners.
	*
	* @param clsIdeaPoi the c l s idea poi
	* @return the c l s idea poi that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoi addCLSIdeaPoi(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoi clsIdeaPoi)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaPoiLocalService.addCLSIdeaPoi(clsIdeaPoi);
	}

	/**
	* Creates a new c l s idea poi with the primary key. Does not add the c l s idea poi to the database.
	*
	* @param poiId the primary key for the new c l s idea poi
	* @return the new c l s idea poi
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoi createCLSIdeaPoi(
		long poiId) {
		return _clsIdeaPoiLocalService.createCLSIdeaPoi(poiId);
	}

	/**
	* Deletes the c l s idea poi with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param poiId the primary key of the c l s idea poi
	* @return the c l s idea poi that was removed
	* @throws PortalException if a c l s idea poi with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoi deleteCLSIdeaPoi(
		long poiId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaPoiLocalService.deleteCLSIdeaPoi(poiId);
	}

	/**
	* Deletes the c l s idea poi from the database. Also notifies the appropriate model listeners.
	*
	* @param clsIdeaPoi the c l s idea poi
	* @return the c l s idea poi that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoi deleteCLSIdeaPoi(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoi clsIdeaPoi)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaPoiLocalService.deleteCLSIdeaPoi(clsIdeaPoi);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _clsIdeaPoiLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaPoiLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaPoiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaPoiLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaPoiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaPoiLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaPoiLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaPoiLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoi fetchCLSIdeaPoi(
		long poiId) throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaPoiLocalService.fetchCLSIdeaPoi(poiId);
	}

	/**
	* Returns the c l s idea poi with the primary key.
	*
	* @param poiId the primary key of the c l s idea poi
	* @return the c l s idea poi
	* @throws PortalException if a c l s idea poi with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoi getCLSIdeaPoi(
		long poiId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaPoiLocalService.getCLSIdeaPoi(poiId);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaPoiLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the c l s idea pois.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaPoiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s idea pois
	* @param end the upper bound of the range of c l s idea pois (not inclusive)
	* @return the range of c l s idea pois
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoi> getCLSIdeaPois(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaPoiLocalService.getCLSIdeaPois(start, end);
	}

	/**
	* Returns the number of c l s idea pois.
	*
	* @return the number of c l s idea pois
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getCLSIdeaPoisCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaPoiLocalService.getCLSIdeaPoisCount();
	}

	/**
	* Updates the c l s idea poi in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param clsIdeaPoi the c l s idea poi
	* @return the c l s idea poi that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoi updateCLSIdeaPoi(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoi clsIdeaPoi)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaPoiLocalService.updateCLSIdeaPoi(clsIdeaPoi);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _clsIdeaPoiLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_clsIdeaPoiLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _clsIdeaPoiLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoi> getIdeaPOIByIdeaId(
		long ideaId) throws com.liferay.portal.kernel.exception.SystemException {
		return _clsIdeaPoiLocalService.getIdeaPOIByIdeaId(ideaId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public CLSIdeaPoiLocalService getWrappedCLSIdeaPoiLocalService() {
		return _clsIdeaPoiLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedCLSIdeaPoiLocalService(
		CLSIdeaPoiLocalService clsIdeaPoiLocalService) {
		_clsIdeaPoiLocalService = clsIdeaPoiLocalService;
	}

	@Override
	public CLSIdeaPoiLocalService getWrappedService() {
		return _clsIdeaPoiLocalService;
	}

	@Override
	public void setWrappedService(CLSIdeaPoiLocalService clsIdeaPoiLocalService) {
		_clsIdeaPoiLocalService = clsIdeaPoiLocalService;
	}

	private CLSIdeaPoiLocalService _clsIdeaPoiLocalService;
}