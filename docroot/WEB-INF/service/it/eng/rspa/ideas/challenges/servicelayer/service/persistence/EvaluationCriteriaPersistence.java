/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria;

/**
 * The persistence interface for the evaluation criteria service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see EvaluationCriteriaPersistenceImpl
 * @see EvaluationCriteriaUtil
 * @generated
 */
public interface EvaluationCriteriaPersistence extends BasePersistence<EvaluationCriteria> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link EvaluationCriteriaUtil} to access the evaluation criteria persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the evaluation criterias where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @return the matching evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria> findBycriteriaByChallenge(
		long challengeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the evaluation criterias where challengeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param challengeId the challenge ID
	* @param start the lower bound of the range of evaluation criterias
	* @param end the upper bound of the range of evaluation criterias (not inclusive)
	* @return the range of matching evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria> findBycriteriaByChallenge(
		long challengeId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the evaluation criterias where challengeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param challengeId the challenge ID
	* @param start the lower bound of the range of evaluation criterias
	* @param end the upper bound of the range of evaluation criterias (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria> findBycriteriaByChallenge(
		long challengeId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first evaluation criteria in the ordered set where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching evaluation criteria
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria findBycriteriaByChallenge_First(
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException;

	/**
	* Returns the first evaluation criteria in the ordered set where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria fetchBycriteriaByChallenge_First(
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last evaluation criteria in the ordered set where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching evaluation criteria
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria findBycriteriaByChallenge_Last(
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException;

	/**
	* Returns the last evaluation criteria in the ordered set where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria fetchBycriteriaByChallenge_Last(
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the evaluation criterias before and after the current evaluation criteria in the ordered set where challengeId = &#63;.
	*
	* @param evaluationCriteriaPK the primary key of the current evaluation criteria
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next evaluation criteria
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a evaluation criteria with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria[] findBycriteriaByChallenge_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.EvaluationCriteriaPK evaluationCriteriaPK,
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException;

	/**
	* Removes all the evaluation criterias where challengeId = &#63; from the database.
	*
	* @param challengeId the challenge ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeBycriteriaByChallenge(long challengeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of evaluation criterias where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @return the number of matching evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public int countBycriteriaByChallenge(long challengeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the evaluation criterias where challengeId = &#63; and enabled = &#63;.
	*
	* @param challengeId the challenge ID
	* @param enabled the enabled
	* @return the matching evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria> findBycriteriaByChallengeAndEnabled(
		long challengeId, boolean enabled)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the evaluation criterias where challengeId = &#63; and enabled = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param challengeId the challenge ID
	* @param enabled the enabled
	* @param start the lower bound of the range of evaluation criterias
	* @param end the upper bound of the range of evaluation criterias (not inclusive)
	* @return the range of matching evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria> findBycriteriaByChallengeAndEnabled(
		long challengeId, boolean enabled, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the evaluation criterias where challengeId = &#63; and enabled = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param challengeId the challenge ID
	* @param enabled the enabled
	* @param start the lower bound of the range of evaluation criterias
	* @param end the upper bound of the range of evaluation criterias (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria> findBycriteriaByChallengeAndEnabled(
		long challengeId, boolean enabled, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first evaluation criteria in the ordered set where challengeId = &#63; and enabled = &#63;.
	*
	* @param challengeId the challenge ID
	* @param enabled the enabled
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching evaluation criteria
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria findBycriteriaByChallengeAndEnabled_First(
		long challengeId, boolean enabled,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException;

	/**
	* Returns the first evaluation criteria in the ordered set where challengeId = &#63; and enabled = &#63;.
	*
	* @param challengeId the challenge ID
	* @param enabled the enabled
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria fetchBycriteriaByChallengeAndEnabled_First(
		long challengeId, boolean enabled,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last evaluation criteria in the ordered set where challengeId = &#63; and enabled = &#63;.
	*
	* @param challengeId the challenge ID
	* @param enabled the enabled
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching evaluation criteria
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria findBycriteriaByChallengeAndEnabled_Last(
		long challengeId, boolean enabled,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException;

	/**
	* Returns the last evaluation criteria in the ordered set where challengeId = &#63; and enabled = &#63;.
	*
	* @param challengeId the challenge ID
	* @param enabled the enabled
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria fetchBycriteriaByChallengeAndEnabled_Last(
		long challengeId, boolean enabled,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the evaluation criterias before and after the current evaluation criteria in the ordered set where challengeId = &#63; and enabled = &#63;.
	*
	* @param evaluationCriteriaPK the primary key of the current evaluation criteria
	* @param challengeId the challenge ID
	* @param enabled the enabled
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next evaluation criteria
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a evaluation criteria with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria[] findBycriteriaByChallengeAndEnabled_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.EvaluationCriteriaPK evaluationCriteriaPK,
		long challengeId, boolean enabled,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException;

	/**
	* Removes all the evaluation criterias where challengeId = &#63; and enabled = &#63; from the database.
	*
	* @param challengeId the challenge ID
	* @param enabled the enabled
	* @throws SystemException if a system exception occurred
	*/
	public void removeBycriteriaByChallengeAndEnabled(long challengeId,
		boolean enabled)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of evaluation criterias where challengeId = &#63; and enabled = &#63;.
	*
	* @param challengeId the challenge ID
	* @param enabled the enabled
	* @return the number of matching evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public int countBycriteriaByChallengeAndEnabled(long challengeId,
		boolean enabled)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the evaluation criteria where criteriaId = &#63; or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException} if it could not be found.
	*
	* @param criteriaId the criteria ID
	* @return the matching evaluation criteria
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria findBycriteriaId(
		long criteriaId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException;

	/**
	* Returns the evaluation criteria where criteriaId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param criteriaId the criteria ID
	* @return the matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria fetchBycriteriaId(
		long criteriaId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the evaluation criteria where criteriaId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param criteriaId the criteria ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria fetchBycriteriaId(
		long criteriaId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the evaluation criteria where criteriaId = &#63; from the database.
	*
	* @param criteriaId the criteria ID
	* @return the evaluation criteria that was removed
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria removeBycriteriaId(
		long criteriaId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException;

	/**
	* Returns the number of evaluation criterias where criteriaId = &#63;.
	*
	* @param criteriaId the criteria ID
	* @return the number of matching evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public int countBycriteriaId(long criteriaId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the evaluation criterias where challengeId = &#63; and isBarriera = &#63;.
	*
	* @param challengeId the challenge ID
	* @param isBarriera the is barriera
	* @return the matching evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria> findByChallengeAndIsBarriera(
		long challengeId, boolean isBarriera)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the evaluation criterias where challengeId = &#63; and isBarriera = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param challengeId the challenge ID
	* @param isBarriera the is barriera
	* @param start the lower bound of the range of evaluation criterias
	* @param end the upper bound of the range of evaluation criterias (not inclusive)
	* @return the range of matching evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria> findByChallengeAndIsBarriera(
		long challengeId, boolean isBarriera, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the evaluation criterias where challengeId = &#63; and isBarriera = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param challengeId the challenge ID
	* @param isBarriera the is barriera
	* @param start the lower bound of the range of evaluation criterias
	* @param end the upper bound of the range of evaluation criterias (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria> findByChallengeAndIsBarriera(
		long challengeId, boolean isBarriera, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first evaluation criteria in the ordered set where challengeId = &#63; and isBarriera = &#63;.
	*
	* @param challengeId the challenge ID
	* @param isBarriera the is barriera
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching evaluation criteria
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria findByChallengeAndIsBarriera_First(
		long challengeId, boolean isBarriera,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException;

	/**
	* Returns the first evaluation criteria in the ordered set where challengeId = &#63; and isBarriera = &#63;.
	*
	* @param challengeId the challenge ID
	* @param isBarriera the is barriera
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria fetchByChallengeAndIsBarriera_First(
		long challengeId, boolean isBarriera,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last evaluation criteria in the ordered set where challengeId = &#63; and isBarriera = &#63;.
	*
	* @param challengeId the challenge ID
	* @param isBarriera the is barriera
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching evaluation criteria
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria findByChallengeAndIsBarriera_Last(
		long challengeId, boolean isBarriera,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException;

	/**
	* Returns the last evaluation criteria in the ordered set where challengeId = &#63; and isBarriera = &#63;.
	*
	* @param challengeId the challenge ID
	* @param isBarriera the is barriera
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria fetchByChallengeAndIsBarriera_Last(
		long challengeId, boolean isBarriera,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the evaluation criterias before and after the current evaluation criteria in the ordered set where challengeId = &#63; and isBarriera = &#63;.
	*
	* @param evaluationCriteriaPK the primary key of the current evaluation criteria
	* @param challengeId the challenge ID
	* @param isBarriera the is barriera
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next evaluation criteria
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a evaluation criteria with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria[] findByChallengeAndIsBarriera_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.EvaluationCriteriaPK evaluationCriteriaPK,
		long challengeId, boolean isBarriera,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException;

	/**
	* Removes all the evaluation criterias where challengeId = &#63; and isBarriera = &#63; from the database.
	*
	* @param challengeId the challenge ID
	* @param isBarriera the is barriera
	* @throws SystemException if a system exception occurred
	*/
	public void removeByChallengeAndIsBarriera(long challengeId,
		boolean isBarriera)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of evaluation criterias where challengeId = &#63; and isBarriera = &#63;.
	*
	* @param challengeId the challenge ID
	* @param isBarriera the is barriera
	* @return the number of matching evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public int countByChallengeAndIsBarriera(long challengeId,
		boolean isBarriera)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the evaluation criterias where challengeId = &#63; and isBarriera = &#63; and enabled = &#63;.
	*
	* @param challengeId the challenge ID
	* @param isBarriera the is barriera
	* @param enabled the enabled
	* @return the matching evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria> findByChallengeAndIsBarrieraAndEnabled(
		long challengeId, boolean isBarriera, boolean enabled)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the evaluation criterias where challengeId = &#63; and isBarriera = &#63; and enabled = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param challengeId the challenge ID
	* @param isBarriera the is barriera
	* @param enabled the enabled
	* @param start the lower bound of the range of evaluation criterias
	* @param end the upper bound of the range of evaluation criterias (not inclusive)
	* @return the range of matching evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria> findByChallengeAndIsBarrieraAndEnabled(
		long challengeId, boolean isBarriera, boolean enabled, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the evaluation criterias where challengeId = &#63; and isBarriera = &#63; and enabled = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param challengeId the challenge ID
	* @param isBarriera the is barriera
	* @param enabled the enabled
	* @param start the lower bound of the range of evaluation criterias
	* @param end the upper bound of the range of evaluation criterias (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria> findByChallengeAndIsBarrieraAndEnabled(
		long challengeId, boolean isBarriera, boolean enabled, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first evaluation criteria in the ordered set where challengeId = &#63; and isBarriera = &#63; and enabled = &#63;.
	*
	* @param challengeId the challenge ID
	* @param isBarriera the is barriera
	* @param enabled the enabled
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching evaluation criteria
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria findByChallengeAndIsBarrieraAndEnabled_First(
		long challengeId, boolean isBarriera, boolean enabled,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException;

	/**
	* Returns the first evaluation criteria in the ordered set where challengeId = &#63; and isBarriera = &#63; and enabled = &#63;.
	*
	* @param challengeId the challenge ID
	* @param isBarriera the is barriera
	* @param enabled the enabled
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria fetchByChallengeAndIsBarrieraAndEnabled_First(
		long challengeId, boolean isBarriera, boolean enabled,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last evaluation criteria in the ordered set where challengeId = &#63; and isBarriera = &#63; and enabled = &#63;.
	*
	* @param challengeId the challenge ID
	* @param isBarriera the is barriera
	* @param enabled the enabled
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching evaluation criteria
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria findByChallengeAndIsBarrieraAndEnabled_Last(
		long challengeId, boolean isBarriera, boolean enabled,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException;

	/**
	* Returns the last evaluation criteria in the ordered set where challengeId = &#63; and isBarriera = &#63; and enabled = &#63;.
	*
	* @param challengeId the challenge ID
	* @param isBarriera the is barriera
	* @param enabled the enabled
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria fetchByChallengeAndIsBarrieraAndEnabled_Last(
		long challengeId, boolean isBarriera, boolean enabled,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the evaluation criterias before and after the current evaluation criteria in the ordered set where challengeId = &#63; and isBarriera = &#63; and enabled = &#63;.
	*
	* @param evaluationCriteriaPK the primary key of the current evaluation criteria
	* @param challengeId the challenge ID
	* @param isBarriera the is barriera
	* @param enabled the enabled
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next evaluation criteria
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a evaluation criteria with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria[] findByChallengeAndIsBarrieraAndEnabled_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.EvaluationCriteriaPK evaluationCriteriaPK,
		long challengeId, boolean isBarriera, boolean enabled,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException;

	/**
	* Removes all the evaluation criterias where challengeId = &#63; and isBarriera = &#63; and enabled = &#63; from the database.
	*
	* @param challengeId the challenge ID
	* @param isBarriera the is barriera
	* @param enabled the enabled
	* @throws SystemException if a system exception occurred
	*/
	public void removeByChallengeAndIsBarrieraAndEnabled(long challengeId,
		boolean isBarriera, boolean enabled)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of evaluation criterias where challengeId = &#63; and isBarriera = &#63; and enabled = &#63;.
	*
	* @param challengeId the challenge ID
	* @param isBarriera the is barriera
	* @param enabled the enabled
	* @return the number of matching evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public int countByChallengeAndIsBarrieraAndEnabled(long challengeId,
		boolean isBarriera, boolean enabled)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the evaluation criterias where challengeId = &#63; and isBarriera = &#63; and isCustom = &#63;.
	*
	* @param challengeId the challenge ID
	* @param isBarriera the is barriera
	* @param isCustom the is custom
	* @return the matching evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria> findByChallengeAndBarrieraAndCustom(
		long challengeId, boolean isBarriera, boolean isCustom)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the evaluation criterias where challengeId = &#63; and isBarriera = &#63; and isCustom = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param challengeId the challenge ID
	* @param isBarriera the is barriera
	* @param isCustom the is custom
	* @param start the lower bound of the range of evaluation criterias
	* @param end the upper bound of the range of evaluation criterias (not inclusive)
	* @return the range of matching evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria> findByChallengeAndBarrieraAndCustom(
		long challengeId, boolean isBarriera, boolean isCustom, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the evaluation criterias where challengeId = &#63; and isBarriera = &#63; and isCustom = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param challengeId the challenge ID
	* @param isBarriera the is barriera
	* @param isCustom the is custom
	* @param start the lower bound of the range of evaluation criterias
	* @param end the upper bound of the range of evaluation criterias (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria> findByChallengeAndBarrieraAndCustom(
		long challengeId, boolean isBarriera, boolean isCustom, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first evaluation criteria in the ordered set where challengeId = &#63; and isBarriera = &#63; and isCustom = &#63;.
	*
	* @param challengeId the challenge ID
	* @param isBarriera the is barriera
	* @param isCustom the is custom
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching evaluation criteria
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria findByChallengeAndBarrieraAndCustom_First(
		long challengeId, boolean isBarriera, boolean isCustom,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException;

	/**
	* Returns the first evaluation criteria in the ordered set where challengeId = &#63; and isBarriera = &#63; and isCustom = &#63;.
	*
	* @param challengeId the challenge ID
	* @param isBarriera the is barriera
	* @param isCustom the is custom
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria fetchByChallengeAndBarrieraAndCustom_First(
		long challengeId, boolean isBarriera, boolean isCustom,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last evaluation criteria in the ordered set where challengeId = &#63; and isBarriera = &#63; and isCustom = &#63;.
	*
	* @param challengeId the challenge ID
	* @param isBarriera the is barriera
	* @param isCustom the is custom
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching evaluation criteria
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria findByChallengeAndBarrieraAndCustom_Last(
		long challengeId, boolean isBarriera, boolean isCustom,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException;

	/**
	* Returns the last evaluation criteria in the ordered set where challengeId = &#63; and isBarriera = &#63; and isCustom = &#63;.
	*
	* @param challengeId the challenge ID
	* @param isBarriera the is barriera
	* @param isCustom the is custom
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria fetchByChallengeAndBarrieraAndCustom_Last(
		long challengeId, boolean isBarriera, boolean isCustom,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the evaluation criterias before and after the current evaluation criteria in the ordered set where challengeId = &#63; and isBarriera = &#63; and isCustom = &#63;.
	*
	* @param evaluationCriteriaPK the primary key of the current evaluation criteria
	* @param challengeId the challenge ID
	* @param isBarriera the is barriera
	* @param isCustom the is custom
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next evaluation criteria
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a evaluation criteria with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria[] findByChallengeAndBarrieraAndCustom_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.EvaluationCriteriaPK evaluationCriteriaPK,
		long challengeId, boolean isBarriera, boolean isCustom,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException;

	/**
	* Removes all the evaluation criterias where challengeId = &#63; and isBarriera = &#63; and isCustom = &#63; from the database.
	*
	* @param challengeId the challenge ID
	* @param isBarriera the is barriera
	* @param isCustom the is custom
	* @throws SystemException if a system exception occurred
	*/
	public void removeByChallengeAndBarrieraAndCustom(long challengeId,
		boolean isBarriera, boolean isCustom)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of evaluation criterias where challengeId = &#63; and isBarriera = &#63; and isCustom = &#63;.
	*
	* @param challengeId the challenge ID
	* @param isBarriera the is barriera
	* @param isCustom the is custom
	* @return the number of matching evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public int countByChallengeAndBarrieraAndCustom(long challengeId,
		boolean isBarriera, boolean isCustom)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the evaluation criterias where challengeId = &#63; and inputId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param inputId the input ID
	* @return the matching evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria> findBychallengeIdAndInputId(
		long challengeId, java.lang.String inputId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the evaluation criterias where challengeId = &#63; and inputId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param challengeId the challenge ID
	* @param inputId the input ID
	* @param start the lower bound of the range of evaluation criterias
	* @param end the upper bound of the range of evaluation criterias (not inclusive)
	* @return the range of matching evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria> findBychallengeIdAndInputId(
		long challengeId, java.lang.String inputId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the evaluation criterias where challengeId = &#63; and inputId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param challengeId the challenge ID
	* @param inputId the input ID
	* @param start the lower bound of the range of evaluation criterias
	* @param end the upper bound of the range of evaluation criterias (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria> findBychallengeIdAndInputId(
		long challengeId, java.lang.String inputId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first evaluation criteria in the ordered set where challengeId = &#63; and inputId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param inputId the input ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching evaluation criteria
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria findBychallengeIdAndInputId_First(
		long challengeId, java.lang.String inputId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException;

	/**
	* Returns the first evaluation criteria in the ordered set where challengeId = &#63; and inputId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param inputId the input ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria fetchBychallengeIdAndInputId_First(
		long challengeId, java.lang.String inputId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last evaluation criteria in the ordered set where challengeId = &#63; and inputId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param inputId the input ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching evaluation criteria
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria findBychallengeIdAndInputId_Last(
		long challengeId, java.lang.String inputId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException;

	/**
	* Returns the last evaluation criteria in the ordered set where challengeId = &#63; and inputId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param inputId the input ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria fetchBychallengeIdAndInputId_Last(
		long challengeId, java.lang.String inputId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the evaluation criterias before and after the current evaluation criteria in the ordered set where challengeId = &#63; and inputId = &#63;.
	*
	* @param evaluationCriteriaPK the primary key of the current evaluation criteria
	* @param challengeId the challenge ID
	* @param inputId the input ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next evaluation criteria
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a evaluation criteria with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria[] findBychallengeIdAndInputId_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.EvaluationCriteriaPK evaluationCriteriaPK,
		long challengeId, java.lang.String inputId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException;

	/**
	* Removes all the evaluation criterias where challengeId = &#63; and inputId = &#63; from the database.
	*
	* @param challengeId the challenge ID
	* @param inputId the input ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeBychallengeIdAndInputId(long challengeId,
		java.lang.String inputId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of evaluation criterias where challengeId = &#63; and inputId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param inputId the input ID
	* @return the number of matching evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public int countBychallengeIdAndInputId(long challengeId,
		java.lang.String inputId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the evaluation criterias where isCustom = &#63; and language = &#63;.
	*
	* @param isCustom the is custom
	* @param language the language
	* @return the matching evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria> findByisCustomAndLanguage(
		boolean isCustom, java.lang.String language)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the evaluation criterias where isCustom = &#63; and language = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param isCustom the is custom
	* @param language the language
	* @param start the lower bound of the range of evaluation criterias
	* @param end the upper bound of the range of evaluation criterias (not inclusive)
	* @return the range of matching evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria> findByisCustomAndLanguage(
		boolean isCustom, java.lang.String language, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the evaluation criterias where isCustom = &#63; and language = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param isCustom the is custom
	* @param language the language
	* @param start the lower bound of the range of evaluation criterias
	* @param end the upper bound of the range of evaluation criterias (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria> findByisCustomAndLanguage(
		boolean isCustom, java.lang.String language, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first evaluation criteria in the ordered set where isCustom = &#63; and language = &#63;.
	*
	* @param isCustom the is custom
	* @param language the language
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching evaluation criteria
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria findByisCustomAndLanguage_First(
		boolean isCustom, java.lang.String language,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException;

	/**
	* Returns the first evaluation criteria in the ordered set where isCustom = &#63; and language = &#63;.
	*
	* @param isCustom the is custom
	* @param language the language
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria fetchByisCustomAndLanguage_First(
		boolean isCustom, java.lang.String language,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last evaluation criteria in the ordered set where isCustom = &#63; and language = &#63;.
	*
	* @param isCustom the is custom
	* @param language the language
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching evaluation criteria
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria findByisCustomAndLanguage_Last(
		boolean isCustom, java.lang.String language,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException;

	/**
	* Returns the last evaluation criteria in the ordered set where isCustom = &#63; and language = &#63;.
	*
	* @param isCustom the is custom
	* @param language the language
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria fetchByisCustomAndLanguage_Last(
		boolean isCustom, java.lang.String language,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the evaluation criterias before and after the current evaluation criteria in the ordered set where isCustom = &#63; and language = &#63;.
	*
	* @param evaluationCriteriaPK the primary key of the current evaluation criteria
	* @param isCustom the is custom
	* @param language the language
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next evaluation criteria
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a evaluation criteria with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria[] findByisCustomAndLanguage_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.EvaluationCriteriaPK evaluationCriteriaPK,
		boolean isCustom, java.lang.String language,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException;

	/**
	* Removes all the evaluation criterias where isCustom = &#63; and language = &#63; from the database.
	*
	* @param isCustom the is custom
	* @param language the language
	* @throws SystemException if a system exception occurred
	*/
	public void removeByisCustomAndLanguage(boolean isCustom,
		java.lang.String language)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of evaluation criterias where isCustom = &#63; and language = &#63;.
	*
	* @param isCustom the is custom
	* @param language the language
	* @return the number of matching evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public int countByisCustomAndLanguage(boolean isCustom,
		java.lang.String language)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the evaluation criteria in the entity cache if it is enabled.
	*
	* @param evaluationCriteria the evaluation criteria
	*/
	public void cacheResult(
		it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria evaluationCriteria);

	/**
	* Caches the evaluation criterias in the entity cache if it is enabled.
	*
	* @param evaluationCriterias the evaluation criterias
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria> evaluationCriterias);

	/**
	* Creates a new evaluation criteria with the primary key. Does not add the evaluation criteria to the database.
	*
	* @param evaluationCriteriaPK the primary key for the new evaluation criteria
	* @return the new evaluation criteria
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria create(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.EvaluationCriteriaPK evaluationCriteriaPK);

	/**
	* Removes the evaluation criteria with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param evaluationCriteriaPK the primary key of the evaluation criteria
	* @return the evaluation criteria that was removed
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a evaluation criteria with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria remove(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.EvaluationCriteriaPK evaluationCriteriaPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException;

	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria evaluationCriteria)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the evaluation criteria with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException} if it could not be found.
	*
	* @param evaluationCriteriaPK the primary key of the evaluation criteria
	* @return the evaluation criteria
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a evaluation criteria with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria findByPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.EvaluationCriteriaPK evaluationCriteriaPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException;

	/**
	* Returns the evaluation criteria with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param evaluationCriteriaPK the primary key of the evaluation criteria
	* @return the evaluation criteria, or <code>null</code> if a evaluation criteria with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria fetchByPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.EvaluationCriteriaPK evaluationCriteriaPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the evaluation criterias.
	*
	* @return the evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the evaluation criterias.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of evaluation criterias
	* @param end the upper bound of the range of evaluation criterias (not inclusive)
	* @return the range of evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the evaluation criterias.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of evaluation criterias
	* @param end the upper bound of the range of evaluation criterias (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the evaluation criterias from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of evaluation criterias.
	*
	* @return the number of evaluation criterias
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}