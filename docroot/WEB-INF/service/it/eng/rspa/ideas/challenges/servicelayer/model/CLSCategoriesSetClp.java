/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.ideas.challenges.servicelayer.service.CLSCategoriesSetLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 */
public class CLSCategoriesSetClp extends BaseModelImpl<CLSCategoriesSet>
	implements CLSCategoriesSet {
	public CLSCategoriesSetClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return CLSCategoriesSet.class;
	}

	@Override
	public String getModelClassName() {
		return CLSCategoriesSet.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _categoriesSetID;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setCategoriesSetID(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _categoriesSetID;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("categoriesSetID", getCategoriesSetID());
		attributes.put("iconName", getIconName());
		attributes.put("colourName", getColourName());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long categoriesSetID = (Long)attributes.get("categoriesSetID");

		if (categoriesSetID != null) {
			setCategoriesSetID(categoriesSetID);
		}

		String iconName = (String)attributes.get("iconName");

		if (iconName != null) {
			setIconName(iconName);
		}

		String colourName = (String)attributes.get("colourName");

		if (colourName != null) {
			setColourName(colourName);
		}
	}

	@Override
	public long getCategoriesSetID() {
		return _categoriesSetID;
	}

	@Override
	public void setCategoriesSetID(long categoriesSetID) {
		_categoriesSetID = categoriesSetID;

		if (_clsCategoriesSetRemoteModel != null) {
			try {
				Class<?> clazz = _clsCategoriesSetRemoteModel.getClass();

				Method method = clazz.getMethod("setCategoriesSetID", long.class);

				method.invoke(_clsCategoriesSetRemoteModel, categoriesSetID);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getIconName() {
		return _iconName;
	}

	@Override
	public void setIconName(String iconName) {
		_iconName = iconName;

		if (_clsCategoriesSetRemoteModel != null) {
			try {
				Class<?> clazz = _clsCategoriesSetRemoteModel.getClass();

				Method method = clazz.getMethod("setIconName", String.class);

				method.invoke(_clsCategoriesSetRemoteModel, iconName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getColourName() {
		return _colourName;
	}

	@Override
	public void setColourName(String colourName) {
		_colourName = colourName;

		if (_clsCategoriesSetRemoteModel != null) {
			try {
				Class<?> clazz = _clsCategoriesSetRemoteModel.getClass();

				Method method = clazz.getMethod("setColourName", String.class);

				method.invoke(_clsCategoriesSetRemoteModel, colourName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getCLSCategoriesSetRemoteModel() {
		return _clsCategoriesSetRemoteModel;
	}

	public void setCLSCategoriesSetRemoteModel(
		BaseModel<?> clsCategoriesSetRemoteModel) {
		_clsCategoriesSetRemoteModel = clsCategoriesSetRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _clsCategoriesSetRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_clsCategoriesSetRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			CLSCategoriesSetLocalServiceUtil.addCLSCategoriesSet(this);
		}
		else {
			CLSCategoriesSetLocalServiceUtil.updateCLSCategoriesSet(this);
		}
	}

	@Override
	public CLSCategoriesSet toEscapedModel() {
		return (CLSCategoriesSet)ProxyUtil.newProxyInstance(CLSCategoriesSet.class.getClassLoader(),
			new Class[] { CLSCategoriesSet.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		CLSCategoriesSetClp clone = new CLSCategoriesSetClp();

		clone.setCategoriesSetID(getCategoriesSetID());
		clone.setIconName(getIconName());
		clone.setColourName(getColourName());

		return clone;
	}

	@Override
	public int compareTo(CLSCategoriesSet clsCategoriesSet) {
		long primaryKey = clsCategoriesSet.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSCategoriesSetClp)) {
			return false;
		}

		CLSCategoriesSetClp clsCategoriesSet = (CLSCategoriesSetClp)obj;

		long primaryKey = clsCategoriesSet.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{categoriesSetID=");
		sb.append(getCategoriesSetID());
		sb.append(", iconName=");
		sb.append(getIconName());
		sb.append(", colourName=");
		sb.append(getColourName());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(13);

		sb.append("<model><model-name>");
		sb.append(
			"it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>categoriesSetID</column-name><column-value><![CDATA[");
		sb.append(getCategoriesSetID());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>iconName</column-name><column-value><![CDATA[");
		sb.append(getIconName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>colourName</column-name><column-value><![CDATA[");
		sb.append(getColourName());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _categoriesSetID;
	private String _iconName;
	private String _colourName;
	private BaseModel<?> _clsCategoriesSetRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.class;
}