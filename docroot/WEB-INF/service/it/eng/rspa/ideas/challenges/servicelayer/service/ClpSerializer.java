/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayInputStream;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayOutputStream;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ClassLoaderObjectInputStream;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.BaseModel;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifactsClp;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetClp;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallengeClp;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengeClp;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoiClp;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengesCalendarClp;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworkerClp;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallengesClp;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeasClp;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSFriendlyUrlSuffixClp;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaClp;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoiClp;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapPropertiesClp;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdeaClp;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisitiClp;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjectsClp;
import it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteriaClp;
import it.eng.rspa.ideas.challenges.servicelayer.model.GESelectedClp;
import it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluationClp;
import it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMailClp;
import it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallengeClp;
import it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPointsClp;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 */
public class ClpSerializer {
	public static String getServletContextName() {
		if (Validator.isNotNull(_servletContextName)) {
			return _servletContextName;
		}

		synchronized (ClpSerializer.class) {
			if (Validator.isNotNull(_servletContextName)) {
				return _servletContextName;
			}

			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Class<?> portletPropsClass = classLoader.loadClass(
						"com.liferay.util.portlet.PortletProps");

				Method getMethod = portletPropsClass.getMethod("get",
						new Class<?>[] { String.class });

				String portletPropsServletContextName = (String)getMethod.invoke(null,
						"Challenge62-portlet-deployment-context");

				if (Validator.isNotNull(portletPropsServletContextName)) {
					_servletContextName = portletPropsServletContextName;
				}
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info(
						"Unable to locate deployment context from portlet properties");
				}
			}

			if (Validator.isNull(_servletContextName)) {
				try {
					String propsUtilServletContextName = PropsUtil.get(
							"Challenge62-portlet-deployment-context");

					if (Validator.isNotNull(propsUtilServletContextName)) {
						_servletContextName = propsUtilServletContextName;
					}
				}
				catch (Throwable t) {
					if (_log.isInfoEnabled()) {
						_log.info(
							"Unable to locate deployment context from portal properties");
					}
				}
			}

			if (Validator.isNull(_servletContextName)) {
				_servletContextName = "Challenge62-portlet";
			}

			return _servletContextName;
		}
	}

	public static Object translateInput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals(CLSArtifactsClp.class.getName())) {
			return translateInputCLSArtifacts(oldModel);
		}

		if (oldModelClassName.equals(CLSCategoriesSetClp.class.getName())) {
			return translateInputCLSCategoriesSet(oldModel);
		}

		if (oldModelClassName.equals(
					CLSCategoriesSetForChallengeClp.class.getName())) {
			return translateInputCLSCategoriesSetForChallenge(oldModel);
		}

		if (oldModelClassName.equals(CLSChallengeClp.class.getName())) {
			return translateInputCLSChallenge(oldModel);
		}

		if (oldModelClassName.equals(CLSChallengePoiClp.class.getName())) {
			return translateInputCLSChallengePoi(oldModel);
		}

		if (oldModelClassName.equals(CLSChallengesCalendarClp.class.getName())) {
			return translateInputCLSChallengesCalendar(oldModel);
		}

		if (oldModelClassName.equals(CLSCoworkerClp.class.getName())) {
			return translateInputCLSCoworker(oldModel);
		}

		if (oldModelClassName.equals(CLSFavouriteChallengesClp.class.getName())) {
			return translateInputCLSFavouriteChallenges(oldModel);
		}

		if (oldModelClassName.equals(CLSFavouriteIdeasClp.class.getName())) {
			return translateInputCLSFavouriteIdeas(oldModel);
		}

		if (oldModelClassName.equals(CLSFriendlyUrlSuffixClp.class.getName())) {
			return translateInputCLSFriendlyUrlSuffix(oldModel);
		}

		if (oldModelClassName.equals(CLSIdeaClp.class.getName())) {
			return translateInputCLSIdea(oldModel);
		}

		if (oldModelClassName.equals(CLSIdeaPoiClp.class.getName())) {
			return translateInputCLSIdeaPoi(oldModel);
		}

		if (oldModelClassName.equals(CLSMapPropertiesClp.class.getName())) {
			return translateInputCLSMapProperties(oldModel);
		}

		if (oldModelClassName.equals(CLSOpLogIdeaClp.class.getName())) {
			return translateInputCLSOpLogIdea(oldModel);
		}

		if (oldModelClassName.equals(CLSRequisitiClp.class.getName())) {
			return translateInputCLSRequisiti(oldModel);
		}

		if (oldModelClassName.equals(CLSVmeProjectsClp.class.getName())) {
			return translateInputCLSVmeProjects(oldModel);
		}

		if (oldModelClassName.equals(EvaluationCriteriaClp.class.getName())) {
			return translateInputEvaluationCriteria(oldModel);
		}

		if (oldModelClassName.equals(GESelectedClp.class.getName())) {
			return translateInputGESelected(oldModel);
		}

		if (oldModelClassName.equals(IdeaEvaluationClp.class.getName())) {
			return translateInputIdeaEvaluation(oldModel);
		}

		if (oldModelClassName.equals(InvitedMailClp.class.getName())) {
			return translateInputInvitedMail(oldModel);
		}

		if (oldModelClassName.equals(NeedLinkedChallengeClp.class.getName())) {
			return translateInputNeedLinkedChallenge(oldModel);
		}

		if (oldModelClassName.equals(VirtuosityPointsClp.class.getName())) {
			return translateInputVirtuosityPoints(oldModel);
		}

		return oldModel;
	}

	public static Object translateInput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateInput(curObj));
		}

		return newList;
	}

	public static Object translateInputCLSArtifacts(BaseModel<?> oldModel) {
		CLSArtifactsClp oldClpModel = (CLSArtifactsClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCLSArtifactsRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCLSCategoriesSet(BaseModel<?> oldModel) {
		CLSCategoriesSetClp oldClpModel = (CLSCategoriesSetClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCLSCategoriesSetRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCLSCategoriesSetForChallenge(
		BaseModel<?> oldModel) {
		CLSCategoriesSetForChallengeClp oldClpModel = (CLSCategoriesSetForChallengeClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCLSCategoriesSetForChallengeRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCLSChallenge(BaseModel<?> oldModel) {
		CLSChallengeClp oldClpModel = (CLSChallengeClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCLSChallengeRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCLSChallengePoi(BaseModel<?> oldModel) {
		CLSChallengePoiClp oldClpModel = (CLSChallengePoiClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCLSChallengePoiRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCLSChallengesCalendar(
		BaseModel<?> oldModel) {
		CLSChallengesCalendarClp oldClpModel = (CLSChallengesCalendarClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCLSChallengesCalendarRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCLSCoworker(BaseModel<?> oldModel) {
		CLSCoworkerClp oldClpModel = (CLSCoworkerClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCLSCoworkerRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCLSFavouriteChallenges(
		BaseModel<?> oldModel) {
		CLSFavouriteChallengesClp oldClpModel = (CLSFavouriteChallengesClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCLSFavouriteChallengesRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCLSFavouriteIdeas(BaseModel<?> oldModel) {
		CLSFavouriteIdeasClp oldClpModel = (CLSFavouriteIdeasClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCLSFavouriteIdeasRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCLSFriendlyUrlSuffix(
		BaseModel<?> oldModel) {
		CLSFriendlyUrlSuffixClp oldClpModel = (CLSFriendlyUrlSuffixClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCLSFriendlyUrlSuffixRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCLSIdea(BaseModel<?> oldModel) {
		CLSIdeaClp oldClpModel = (CLSIdeaClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCLSIdeaRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCLSIdeaPoi(BaseModel<?> oldModel) {
		CLSIdeaPoiClp oldClpModel = (CLSIdeaPoiClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCLSIdeaPoiRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCLSMapProperties(BaseModel<?> oldModel) {
		CLSMapPropertiesClp oldClpModel = (CLSMapPropertiesClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCLSMapPropertiesRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCLSOpLogIdea(BaseModel<?> oldModel) {
		CLSOpLogIdeaClp oldClpModel = (CLSOpLogIdeaClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCLSOpLogIdeaRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCLSRequisiti(BaseModel<?> oldModel) {
		CLSRequisitiClp oldClpModel = (CLSRequisitiClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCLSRequisitiRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputCLSVmeProjects(BaseModel<?> oldModel) {
		CLSVmeProjectsClp oldClpModel = (CLSVmeProjectsClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCLSVmeProjectsRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputEvaluationCriteria(BaseModel<?> oldModel) {
		EvaluationCriteriaClp oldClpModel = (EvaluationCriteriaClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getEvaluationCriteriaRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputGESelected(BaseModel<?> oldModel) {
		GESelectedClp oldClpModel = (GESelectedClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getGESelectedRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputIdeaEvaluation(BaseModel<?> oldModel) {
		IdeaEvaluationClp oldClpModel = (IdeaEvaluationClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getIdeaEvaluationRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputInvitedMail(BaseModel<?> oldModel) {
		InvitedMailClp oldClpModel = (InvitedMailClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getInvitedMailRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputNeedLinkedChallenge(
		BaseModel<?> oldModel) {
		NeedLinkedChallengeClp oldClpModel = (NeedLinkedChallengeClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getNeedLinkedChallengeRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInputVirtuosityPoints(BaseModel<?> oldModel) {
		VirtuosityPointsClp oldClpModel = (VirtuosityPointsClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getVirtuosityPointsRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}

	public static Object translateInput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateInput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateInput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Object translateOutput(BaseModel<?> oldModel) {
		Class<?> oldModelClass = oldModel.getClass();

		String oldModelClassName = oldModelClass.getName();

		if (oldModelClassName.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsImpl")) {
			return translateOutputCLSArtifacts(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCategoriesSetImpl")) {
			return translateOutputCLSCategoriesSet(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCategoriesSetForChallengeImpl")) {
			return translateOutputCLSCategoriesSetForChallenge(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengeImpl")) {
			return translateOutputCLSChallenge(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengePoiImpl")) {
			return translateOutputCLSChallengePoi(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengesCalendarImpl")) {
			return translateOutputCLSChallengesCalendar(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCoworkerImpl")) {
			return translateOutputCLSCoworker(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteChallengesImpl")) {
			return translateOutputCLSFavouriteChallenges(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteIdeasImpl")) {
			return translateOutputCLSFavouriteIdeas(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFriendlyUrlSuffixImpl")) {
			return translateOutputCLSFriendlyUrlSuffix(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaImpl")) {
			return translateOutputCLSIdea(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaPoiImpl")) {
			return translateOutputCLSIdeaPoi(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSMapPropertiesImpl")) {
			return translateOutputCLSMapProperties(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSOpLogIdeaImpl")) {
			return translateOutputCLSOpLogIdea(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSRequisitiImpl")) {
			return translateOutputCLSRequisiti(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSVmeProjectsImpl")) {
			return translateOutputCLSVmeProjects(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaImpl")) {
			return translateOutputEvaluationCriteria(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.model.impl.GESelectedImpl")) {
			return translateOutputGESelected(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationImpl")) {
			return translateOutputIdeaEvaluation(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.model.impl.InvitedMailImpl")) {
			return translateOutputInvitedMail(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.model.impl.NeedLinkedChallengeImpl")) {
			return translateOutputNeedLinkedChallenge(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		if (oldModelClassName.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.model.impl.VirtuosityPointsImpl")) {
			return translateOutputVirtuosityPoints(oldModel);
		}
		else if (oldModelClassName.endsWith("Clp")) {
			try {
				ClassLoader classLoader = ClpSerializer.class.getClassLoader();

				Method getClpSerializerClassMethod = oldModelClass.getMethod(
						"getClpSerializerClass");

				Class<?> oldClpSerializerClass = (Class<?>)getClpSerializerClassMethod.invoke(oldModel);

				Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

				Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
						BaseModel.class);

				Class<?> oldModelModelClass = oldModel.getModelClass();

				Method getRemoteModelMethod = oldModelClass.getMethod("get" +
						oldModelModelClass.getSimpleName() + "RemoteModel");

				Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

				BaseModel<?> newModel = (BaseModel<?>)translateOutputMethod.invoke(null,
						oldRemoteModel);

				return newModel;
			}
			catch (Throwable t) {
				if (_log.isInfoEnabled()) {
					_log.info("Unable to translate " + oldModelClassName, t);
				}
			}
		}

		return oldModel;
	}

	public static Object translateOutput(List<Object> oldList) {
		List<Object> newList = new ArrayList<Object>(oldList.size());

		for (int i = 0; i < oldList.size(); i++) {
			Object curObj = oldList.get(i);

			newList.add(translateOutput(curObj));
		}

		return newList;
	}

	public static Object translateOutput(Object obj) {
		if (obj instanceof BaseModel<?>) {
			return translateOutput((BaseModel<?>)obj);
		}
		else if (obj instanceof List<?>) {
			return translateOutput((List<Object>)obj);
		}
		else {
			return obj;
		}
	}

	public static Throwable translateThrowable(Throwable throwable) {
		if (_useReflectionToTranslateThrowable) {
			try {
				UnsyncByteArrayOutputStream unsyncByteArrayOutputStream = new UnsyncByteArrayOutputStream();
				ObjectOutputStream objectOutputStream = new ObjectOutputStream(unsyncByteArrayOutputStream);

				objectOutputStream.writeObject(throwable);

				objectOutputStream.flush();
				objectOutputStream.close();

				UnsyncByteArrayInputStream unsyncByteArrayInputStream = new UnsyncByteArrayInputStream(unsyncByteArrayOutputStream.unsafeGetByteArray(),
						0, unsyncByteArrayOutputStream.size());

				Thread currentThread = Thread.currentThread();

				ClassLoader contextClassLoader = currentThread.getContextClassLoader();

				ObjectInputStream objectInputStream = new ClassLoaderObjectInputStream(unsyncByteArrayInputStream,
						contextClassLoader);

				throwable = (Throwable)objectInputStream.readObject();

				objectInputStream.close();

				return throwable;
			}
			catch (SecurityException se) {
				if (_log.isInfoEnabled()) {
					_log.info("Do not use reflection to translate throwable");
				}

				_useReflectionToTranslateThrowable = false;
			}
			catch (Throwable throwable2) {
				_log.error(throwable2, throwable2);

				return throwable2;
			}
		}

		Class<?> clazz = throwable.getClass();

		String className = clazz.getName();

		if (className.equals(PortalException.class.getName())) {
			return new PortalException();
		}

		if (className.equals(SystemException.class.getName())) {
			return new SystemException();
		}

		if (className.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException")) {
			return new it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException();
		}

		if (className.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetException")) {
			return new it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetException();
		}

		if (className.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException")) {
			return new it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException();
		}

		if (className.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengeException")) {
			return new it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengeException();
		}

		if (className.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengePoiException")) {
			return new it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengePoiException();
		}

		if (className.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengesCalendarException")) {
			return new it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengesCalendarException();
		}

		if (className.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException")) {
			return new it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCoworkerException();
		}

		if (className.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException")) {
			return new it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteChallengesException();
		}

		if (className.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException")) {
			return new it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException();
		}

		if (className.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFriendlyUrlSuffixException")) {
			return new it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFriendlyUrlSuffixException();
		}

		if (className.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException")) {
			return new it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException();
		}

		if (className.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaPoiException")) {
			return new it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaPoiException();
		}

		if (className.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSMapPropertiesException")) {
			return new it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSMapPropertiesException();
		}

		if (className.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSOpLogIdeaException")) {
			return new it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSOpLogIdeaException();
		}

		if (className.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException")) {
			return new it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException();
		}

		if (className.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException")) {
			return new it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException();
		}

		if (className.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException")) {
			return new it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException();
		}

		if (className.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException")) {
			return new it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException();
		}

		if (className.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException")) {
			return new it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException();
		}

		if (className.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException")) {
			return new it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException();
		}

		if (className.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException")) {
			return new it.eng.rspa.ideas.challenges.servicelayer.NoSuchNeedLinkedChallengeException();
		}

		if (className.equals(
					"it.eng.rspa.ideas.challenges.servicelayer.NoSuchVirtuosityPointsException")) {
			return new it.eng.rspa.ideas.challenges.servicelayer.NoSuchVirtuosityPointsException();
		}

		return throwable;
	}

	public static Object translateOutputCLSArtifacts(BaseModel<?> oldModel) {
		CLSArtifactsClp newModel = new CLSArtifactsClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCLSArtifactsRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCLSCategoriesSet(BaseModel<?> oldModel) {
		CLSCategoriesSetClp newModel = new CLSCategoriesSetClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCLSCategoriesSetRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCLSCategoriesSetForChallenge(
		BaseModel<?> oldModel) {
		CLSCategoriesSetForChallengeClp newModel = new CLSCategoriesSetForChallengeClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCLSCategoriesSetForChallengeRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCLSChallenge(BaseModel<?> oldModel) {
		CLSChallengeClp newModel = new CLSChallengeClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCLSChallengeRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCLSChallengePoi(BaseModel<?> oldModel) {
		CLSChallengePoiClp newModel = new CLSChallengePoiClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCLSChallengePoiRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCLSChallengesCalendar(
		BaseModel<?> oldModel) {
		CLSChallengesCalendarClp newModel = new CLSChallengesCalendarClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCLSChallengesCalendarRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCLSCoworker(BaseModel<?> oldModel) {
		CLSCoworkerClp newModel = new CLSCoworkerClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCLSCoworkerRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCLSFavouriteChallenges(
		BaseModel<?> oldModel) {
		CLSFavouriteChallengesClp newModel = new CLSFavouriteChallengesClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCLSFavouriteChallengesRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCLSFavouriteIdeas(BaseModel<?> oldModel) {
		CLSFavouriteIdeasClp newModel = new CLSFavouriteIdeasClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCLSFavouriteIdeasRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCLSFriendlyUrlSuffix(
		BaseModel<?> oldModel) {
		CLSFriendlyUrlSuffixClp newModel = new CLSFriendlyUrlSuffixClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCLSFriendlyUrlSuffixRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCLSIdea(BaseModel<?> oldModel) {
		CLSIdeaClp newModel = new CLSIdeaClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCLSIdeaRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCLSIdeaPoi(BaseModel<?> oldModel) {
		CLSIdeaPoiClp newModel = new CLSIdeaPoiClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCLSIdeaPoiRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCLSMapProperties(BaseModel<?> oldModel) {
		CLSMapPropertiesClp newModel = new CLSMapPropertiesClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCLSMapPropertiesRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCLSOpLogIdea(BaseModel<?> oldModel) {
		CLSOpLogIdeaClp newModel = new CLSOpLogIdeaClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCLSOpLogIdeaRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCLSRequisiti(BaseModel<?> oldModel) {
		CLSRequisitiClp newModel = new CLSRequisitiClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCLSRequisitiRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputCLSVmeProjects(BaseModel<?> oldModel) {
		CLSVmeProjectsClp newModel = new CLSVmeProjectsClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCLSVmeProjectsRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputEvaluationCriteria(
		BaseModel<?> oldModel) {
		EvaluationCriteriaClp newModel = new EvaluationCriteriaClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setEvaluationCriteriaRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputGESelected(BaseModel<?> oldModel) {
		GESelectedClp newModel = new GESelectedClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setGESelectedRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputIdeaEvaluation(BaseModel<?> oldModel) {
		IdeaEvaluationClp newModel = new IdeaEvaluationClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setIdeaEvaluationRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputInvitedMail(BaseModel<?> oldModel) {
		InvitedMailClp newModel = new InvitedMailClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setInvitedMailRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputNeedLinkedChallenge(
		BaseModel<?> oldModel) {
		NeedLinkedChallengeClp newModel = new NeedLinkedChallengeClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setNeedLinkedChallengeRemoteModel(oldModel);

		return newModel;
	}

	public static Object translateOutputVirtuosityPoints(BaseModel<?> oldModel) {
		VirtuosityPointsClp newModel = new VirtuosityPointsClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setVirtuosityPointsRemoteModel(oldModel);

		return newModel;
	}

	private static Log _log = LogFactoryUtil.getLog(ClpSerializer.class);
	private static String _servletContextName;
	private static boolean _useReflectionToTranslateThrowable = true;
}