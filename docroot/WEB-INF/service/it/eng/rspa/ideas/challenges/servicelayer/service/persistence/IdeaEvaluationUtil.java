/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation;

import java.util.List;

/**
 * The persistence utility for the idea evaluation service. This utility wraps {@link IdeaEvaluationPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see IdeaEvaluationPersistence
 * @see IdeaEvaluationPersistenceImpl
 * @generated
 */
public class IdeaEvaluationUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(IdeaEvaluation ideaEvaluation) {
		getPersistence().clearCache(ideaEvaluation);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<IdeaEvaluation> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<IdeaEvaluation> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<IdeaEvaluation> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static IdeaEvaluation update(IdeaEvaluation ideaEvaluation)
		throws SystemException {
		return getPersistence().update(ideaEvaluation);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static IdeaEvaluation update(IdeaEvaluation ideaEvaluation,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(ideaEvaluation, serviceContext);
	}

	/**
	* Returns the idea evaluation where criteriaId = &#63; and ideaId = &#63; or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException} if it could not be found.
	*
	* @param criteriaId the criteria ID
	* @param ideaId the idea ID
	* @return the matching idea evaluation
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException if a matching idea evaluation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation findBycriteriaAndIdeaId(
		long criteriaId, long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException {
		return getPersistence().findBycriteriaAndIdeaId(criteriaId, ideaId);
	}

	/**
	* Returns the idea evaluation where criteriaId = &#63; and ideaId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param criteriaId the criteria ID
	* @param ideaId the idea ID
	* @return the matching idea evaluation, or <code>null</code> if a matching idea evaluation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation fetchBycriteriaAndIdeaId(
		long criteriaId, long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBycriteriaAndIdeaId(criteriaId, ideaId);
	}

	/**
	* Returns the idea evaluation where criteriaId = &#63; and ideaId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param criteriaId the criteria ID
	* @param ideaId the idea ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching idea evaluation, or <code>null</code> if a matching idea evaluation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation fetchBycriteriaAndIdeaId(
		long criteriaId, long ideaId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBycriteriaAndIdeaId(criteriaId, ideaId,
			retrieveFromCache);
	}

	/**
	* Removes the idea evaluation where criteriaId = &#63; and ideaId = &#63; from the database.
	*
	* @param criteriaId the criteria ID
	* @param ideaId the idea ID
	* @return the idea evaluation that was removed
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation removeBycriteriaAndIdeaId(
		long criteriaId, long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException {
		return getPersistence().removeBycriteriaAndIdeaId(criteriaId, ideaId);
	}

	/**
	* Returns the number of idea evaluations where criteriaId = &#63; and ideaId = &#63;.
	*
	* @param criteriaId the criteria ID
	* @param ideaId the idea ID
	* @return the number of matching idea evaluations
	* @throws SystemException if a system exception occurred
	*/
	public static int countBycriteriaAndIdeaId(long criteriaId, long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBycriteriaAndIdeaId(criteriaId, ideaId);
	}

	/**
	* Returns all the idea evaluations where criteriaId = &#63;.
	*
	* @param criteriaId the criteria ID
	* @return the matching idea evaluations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation> findBycriteriaId(
		long criteriaId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBycriteriaId(criteriaId);
	}

	/**
	* Returns a range of all the idea evaluations where criteriaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param criteriaId the criteria ID
	* @param start the lower bound of the range of idea evaluations
	* @param end the upper bound of the range of idea evaluations (not inclusive)
	* @return the range of matching idea evaluations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation> findBycriteriaId(
		long criteriaId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBycriteriaId(criteriaId, start, end);
	}

	/**
	* Returns an ordered range of all the idea evaluations where criteriaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param criteriaId the criteria ID
	* @param start the lower bound of the range of idea evaluations
	* @param end the upper bound of the range of idea evaluations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching idea evaluations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation> findBycriteriaId(
		long criteriaId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBycriteriaId(criteriaId, start, end, orderByComparator);
	}

	/**
	* Returns the first idea evaluation in the ordered set where criteriaId = &#63;.
	*
	* @param criteriaId the criteria ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching idea evaluation
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException if a matching idea evaluation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation findBycriteriaId_First(
		long criteriaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException {
		return getPersistence()
				   .findBycriteriaId_First(criteriaId, orderByComparator);
	}

	/**
	* Returns the first idea evaluation in the ordered set where criteriaId = &#63;.
	*
	* @param criteriaId the criteria ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching idea evaluation, or <code>null</code> if a matching idea evaluation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation fetchBycriteriaId_First(
		long criteriaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBycriteriaId_First(criteriaId, orderByComparator);
	}

	/**
	* Returns the last idea evaluation in the ordered set where criteriaId = &#63;.
	*
	* @param criteriaId the criteria ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching idea evaluation
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException if a matching idea evaluation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation findBycriteriaId_Last(
		long criteriaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException {
		return getPersistence()
				   .findBycriteriaId_Last(criteriaId, orderByComparator);
	}

	/**
	* Returns the last idea evaluation in the ordered set where criteriaId = &#63;.
	*
	* @param criteriaId the criteria ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching idea evaluation, or <code>null</code> if a matching idea evaluation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation fetchBycriteriaId_Last(
		long criteriaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchBycriteriaId_Last(criteriaId, orderByComparator);
	}

	/**
	* Returns the idea evaluations before and after the current idea evaluation in the ordered set where criteriaId = &#63;.
	*
	* @param ideaEvaluationPK the primary key of the current idea evaluation
	* @param criteriaId the criteria ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next idea evaluation
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException if a idea evaluation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation[] findBycriteriaId_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.IdeaEvaluationPK ideaEvaluationPK,
		long criteriaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException {
		return getPersistence()
				   .findBycriteriaId_PrevAndNext(ideaEvaluationPK, criteriaId,
			orderByComparator);
	}

	/**
	* Removes all the idea evaluations where criteriaId = &#63; from the database.
	*
	* @param criteriaId the criteria ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBycriteriaId(long criteriaId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBycriteriaId(criteriaId);
	}

	/**
	* Returns the number of idea evaluations where criteriaId = &#63;.
	*
	* @param criteriaId the criteria ID
	* @return the number of matching idea evaluations
	* @throws SystemException if a system exception occurred
	*/
	public static int countBycriteriaId(long criteriaId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBycriteriaId(criteriaId);
	}

	/**
	* Returns all the idea evaluations where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @return the matching idea evaluations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation> findByideaId(
		long ideaId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByideaId(ideaId);
	}

	/**
	* Returns a range of all the idea evaluations where ideaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param start the lower bound of the range of idea evaluations
	* @param end the upper bound of the range of idea evaluations (not inclusive)
	* @return the range of matching idea evaluations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation> findByideaId(
		long ideaId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByideaId(ideaId, start, end);
	}

	/**
	* Returns an ordered range of all the idea evaluations where ideaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param start the lower bound of the range of idea evaluations
	* @param end the upper bound of the range of idea evaluations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching idea evaluations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation> findByideaId(
		long ideaId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByideaId(ideaId, start, end, orderByComparator);
	}

	/**
	* Returns the first idea evaluation in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching idea evaluation
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException if a matching idea evaluation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation findByideaId_First(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException {
		return getPersistence().findByideaId_First(ideaId, orderByComparator);
	}

	/**
	* Returns the first idea evaluation in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching idea evaluation, or <code>null</code> if a matching idea evaluation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation fetchByideaId_First(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByideaId_First(ideaId, orderByComparator);
	}

	/**
	* Returns the last idea evaluation in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching idea evaluation
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException if a matching idea evaluation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation findByideaId_Last(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException {
		return getPersistence().findByideaId_Last(ideaId, orderByComparator);
	}

	/**
	* Returns the last idea evaluation in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching idea evaluation, or <code>null</code> if a matching idea evaluation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation fetchByideaId_Last(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByideaId_Last(ideaId, orderByComparator);
	}

	/**
	* Returns the idea evaluations before and after the current idea evaluation in the ordered set where ideaId = &#63;.
	*
	* @param ideaEvaluationPK the primary key of the current idea evaluation
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next idea evaluation
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException if a idea evaluation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation[] findByideaId_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.IdeaEvaluationPK ideaEvaluationPK,
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException {
		return getPersistence()
				   .findByideaId_PrevAndNext(ideaEvaluationPK, ideaId,
			orderByComparator);
	}

	/**
	* Removes all the idea evaluations where ideaId = &#63; from the database.
	*
	* @param ideaId the idea ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByideaId(long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByideaId(ideaId);
	}

	/**
	* Returns the number of idea evaluations where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @return the number of matching idea evaluations
	* @throws SystemException if a system exception occurred
	*/
	public static int countByideaId(long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByideaId(ideaId);
	}

	/**
	* Caches the idea evaluation in the entity cache if it is enabled.
	*
	* @param ideaEvaluation the idea evaluation
	*/
	public static void cacheResult(
		it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation ideaEvaluation) {
		getPersistence().cacheResult(ideaEvaluation);
	}

	/**
	* Caches the idea evaluations in the entity cache if it is enabled.
	*
	* @param ideaEvaluations the idea evaluations
	*/
	public static void cacheResult(
		java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation> ideaEvaluations) {
		getPersistence().cacheResult(ideaEvaluations);
	}

	/**
	* Creates a new idea evaluation with the primary key. Does not add the idea evaluation to the database.
	*
	* @param ideaEvaluationPK the primary key for the new idea evaluation
	* @return the new idea evaluation
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation create(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.IdeaEvaluationPK ideaEvaluationPK) {
		return getPersistence().create(ideaEvaluationPK);
	}

	/**
	* Removes the idea evaluation with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ideaEvaluationPK the primary key of the idea evaluation
	* @return the idea evaluation that was removed
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException if a idea evaluation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation remove(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.IdeaEvaluationPK ideaEvaluationPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException {
		return getPersistence().remove(ideaEvaluationPK);
	}

	public static it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation ideaEvaluation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(ideaEvaluation);
	}

	/**
	* Returns the idea evaluation with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException} if it could not be found.
	*
	* @param ideaEvaluationPK the primary key of the idea evaluation
	* @return the idea evaluation
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException if a idea evaluation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation findByPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.IdeaEvaluationPK ideaEvaluationPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException {
		return getPersistence().findByPrimaryKey(ideaEvaluationPK);
	}

	/**
	* Returns the idea evaluation with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ideaEvaluationPK the primary key of the idea evaluation
	* @return the idea evaluation, or <code>null</code> if a idea evaluation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation fetchByPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.IdeaEvaluationPK ideaEvaluationPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(ideaEvaluationPK);
	}

	/**
	* Returns all the idea evaluations.
	*
	* @return the idea evaluations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the idea evaluations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of idea evaluations
	* @param end the upper bound of the range of idea evaluations (not inclusive)
	* @return the range of idea evaluations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the idea evaluations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of idea evaluations
	* @param end the upper bound of the range of idea evaluations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of idea evaluations
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the idea evaluations from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of idea evaluations.
	*
	* @return the number of idea evaluations
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static IdeaEvaluationPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (IdeaEvaluationPersistence)PortletBeanLocatorUtil.locate(it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.getServletContextName(),
					IdeaEvaluationPersistence.class.getName());

			ReferenceRegistry.registerReference(IdeaEvaluationUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(IdeaEvaluationPersistence persistence) {
	}

	private static IdeaEvaluationPersistence _persistence;
}