/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link EvaluationCriteria}.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see EvaluationCriteria
 * @generated
 */
public class EvaluationCriteriaWrapper implements EvaluationCriteria,
	ModelWrapper<EvaluationCriteria> {
	public EvaluationCriteriaWrapper(EvaluationCriteria evaluationCriteria) {
		_evaluationCriteria = evaluationCriteria;
	}

	@Override
	public Class<?> getModelClass() {
		return EvaluationCriteria.class;
	}

	@Override
	public String getModelClassName() {
		return EvaluationCriteria.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("criteriaId", getCriteriaId());
		attributes.put("challengeId", getChallengeId());
		attributes.put("enabled", getEnabled());
		attributes.put("description", getDescription());
		attributes.put("isBarriera", getIsBarriera());
		attributes.put("isCustom", getIsCustom());
		attributes.put("language", getLanguage());
		attributes.put("inputId", getInputId());
		attributes.put("weight", getWeight());
		attributes.put("threshold", getThreshold());
		attributes.put("date", getDate());
		attributes.put("authorId", getAuthorId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long criteriaId = (Long)attributes.get("criteriaId");

		if (criteriaId != null) {
			setCriteriaId(criteriaId);
		}

		Long challengeId = (Long)attributes.get("challengeId");

		if (challengeId != null) {
			setChallengeId(challengeId);
		}

		Boolean enabled = (Boolean)attributes.get("enabled");

		if (enabled != null) {
			setEnabled(enabled);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		Boolean isBarriera = (Boolean)attributes.get("isBarriera");

		if (isBarriera != null) {
			setIsBarriera(isBarriera);
		}

		Boolean isCustom = (Boolean)attributes.get("isCustom");

		if (isCustom != null) {
			setIsCustom(isCustom);
		}

		String language = (String)attributes.get("language");

		if (language != null) {
			setLanguage(language);
		}

		String inputId = (String)attributes.get("inputId");

		if (inputId != null) {
			setInputId(inputId);
		}

		Double weight = (Double)attributes.get("weight");

		if (weight != null) {
			setWeight(weight);
		}

		Integer threshold = (Integer)attributes.get("threshold");

		if (threshold != null) {
			setThreshold(threshold);
		}

		Date date = (Date)attributes.get("date");

		if (date != null) {
			setDate(date);
		}

		Long authorId = (Long)attributes.get("authorId");

		if (authorId != null) {
			setAuthorId(authorId);
		}
	}

	/**
	* Returns the primary key of this evaluation criteria.
	*
	* @return the primary key of this evaluation criteria
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.service.persistence.EvaluationCriteriaPK getPrimaryKey() {
		return _evaluationCriteria.getPrimaryKey();
	}

	/**
	* Sets the primary key of this evaluation criteria.
	*
	* @param primaryKey the primary key of this evaluation criteria
	*/
	@Override
	public void setPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.EvaluationCriteriaPK primaryKey) {
		_evaluationCriteria.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the criteria ID of this evaluation criteria.
	*
	* @return the criteria ID of this evaluation criteria
	*/
	@Override
	public long getCriteriaId() {
		return _evaluationCriteria.getCriteriaId();
	}

	/**
	* Sets the criteria ID of this evaluation criteria.
	*
	* @param criteriaId the criteria ID of this evaluation criteria
	*/
	@Override
	public void setCriteriaId(long criteriaId) {
		_evaluationCriteria.setCriteriaId(criteriaId);
	}

	/**
	* Returns the challenge ID of this evaluation criteria.
	*
	* @return the challenge ID of this evaluation criteria
	*/
	@Override
	public long getChallengeId() {
		return _evaluationCriteria.getChallengeId();
	}

	/**
	* Sets the challenge ID of this evaluation criteria.
	*
	* @param challengeId the challenge ID of this evaluation criteria
	*/
	@Override
	public void setChallengeId(long challengeId) {
		_evaluationCriteria.setChallengeId(challengeId);
	}

	/**
	* Returns the enabled of this evaluation criteria.
	*
	* @return the enabled of this evaluation criteria
	*/
	@Override
	public boolean getEnabled() {
		return _evaluationCriteria.getEnabled();
	}

	/**
	* Returns <code>true</code> if this evaluation criteria is enabled.
	*
	* @return <code>true</code> if this evaluation criteria is enabled; <code>false</code> otherwise
	*/
	@Override
	public boolean isEnabled() {
		return _evaluationCriteria.isEnabled();
	}

	/**
	* Sets whether this evaluation criteria is enabled.
	*
	* @param enabled the enabled of this evaluation criteria
	*/
	@Override
	public void setEnabled(boolean enabled) {
		_evaluationCriteria.setEnabled(enabled);
	}

	/**
	* Returns the description of this evaluation criteria.
	*
	* @return the description of this evaluation criteria
	*/
	@Override
	public java.lang.String getDescription() {
		return _evaluationCriteria.getDescription();
	}

	/**
	* Sets the description of this evaluation criteria.
	*
	* @param description the description of this evaluation criteria
	*/
	@Override
	public void setDescription(java.lang.String description) {
		_evaluationCriteria.setDescription(description);
	}

	/**
	* Returns the is barriera of this evaluation criteria.
	*
	* @return the is barriera of this evaluation criteria
	*/
	@Override
	public boolean getIsBarriera() {
		return _evaluationCriteria.getIsBarriera();
	}

	/**
	* Returns <code>true</code> if this evaluation criteria is is barriera.
	*
	* @return <code>true</code> if this evaluation criteria is is barriera; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsBarriera() {
		return _evaluationCriteria.isIsBarriera();
	}

	/**
	* Sets whether this evaluation criteria is is barriera.
	*
	* @param isBarriera the is barriera of this evaluation criteria
	*/
	@Override
	public void setIsBarriera(boolean isBarriera) {
		_evaluationCriteria.setIsBarriera(isBarriera);
	}

	/**
	* Returns the is custom of this evaluation criteria.
	*
	* @return the is custom of this evaluation criteria
	*/
	@Override
	public boolean getIsCustom() {
		return _evaluationCriteria.getIsCustom();
	}

	/**
	* Returns <code>true</code> if this evaluation criteria is is custom.
	*
	* @return <code>true</code> if this evaluation criteria is is custom; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsCustom() {
		return _evaluationCriteria.isIsCustom();
	}

	/**
	* Sets whether this evaluation criteria is is custom.
	*
	* @param isCustom the is custom of this evaluation criteria
	*/
	@Override
	public void setIsCustom(boolean isCustom) {
		_evaluationCriteria.setIsCustom(isCustom);
	}

	/**
	* Returns the language of this evaluation criteria.
	*
	* @return the language of this evaluation criteria
	*/
	@Override
	public java.lang.String getLanguage() {
		return _evaluationCriteria.getLanguage();
	}

	/**
	* Sets the language of this evaluation criteria.
	*
	* @param language the language of this evaluation criteria
	*/
	@Override
	public void setLanguage(java.lang.String language) {
		_evaluationCriteria.setLanguage(language);
	}

	/**
	* Returns the input ID of this evaluation criteria.
	*
	* @return the input ID of this evaluation criteria
	*/
	@Override
	public java.lang.String getInputId() {
		return _evaluationCriteria.getInputId();
	}

	/**
	* Sets the input ID of this evaluation criteria.
	*
	* @param inputId the input ID of this evaluation criteria
	*/
	@Override
	public void setInputId(java.lang.String inputId) {
		_evaluationCriteria.setInputId(inputId);
	}

	/**
	* Returns the weight of this evaluation criteria.
	*
	* @return the weight of this evaluation criteria
	*/
	@Override
	public double getWeight() {
		return _evaluationCriteria.getWeight();
	}

	/**
	* Sets the weight of this evaluation criteria.
	*
	* @param weight the weight of this evaluation criteria
	*/
	@Override
	public void setWeight(double weight) {
		_evaluationCriteria.setWeight(weight);
	}

	/**
	* Returns the threshold of this evaluation criteria.
	*
	* @return the threshold of this evaluation criteria
	*/
	@Override
	public int getThreshold() {
		return _evaluationCriteria.getThreshold();
	}

	/**
	* Sets the threshold of this evaluation criteria.
	*
	* @param threshold the threshold of this evaluation criteria
	*/
	@Override
	public void setThreshold(int threshold) {
		_evaluationCriteria.setThreshold(threshold);
	}

	/**
	* Returns the date of this evaluation criteria.
	*
	* @return the date of this evaluation criteria
	*/
	@Override
	public java.util.Date getDate() {
		return _evaluationCriteria.getDate();
	}

	/**
	* Sets the date of this evaluation criteria.
	*
	* @param date the date of this evaluation criteria
	*/
	@Override
	public void setDate(java.util.Date date) {
		_evaluationCriteria.setDate(date);
	}

	/**
	* Returns the author ID of this evaluation criteria.
	*
	* @return the author ID of this evaluation criteria
	*/
	@Override
	public long getAuthorId() {
		return _evaluationCriteria.getAuthorId();
	}

	/**
	* Sets the author ID of this evaluation criteria.
	*
	* @param authorId the author ID of this evaluation criteria
	*/
	@Override
	public void setAuthorId(long authorId) {
		_evaluationCriteria.setAuthorId(authorId);
	}

	@Override
	public boolean isNew() {
		return _evaluationCriteria.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_evaluationCriteria.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _evaluationCriteria.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_evaluationCriteria.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _evaluationCriteria.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _evaluationCriteria.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_evaluationCriteria.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _evaluationCriteria.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_evaluationCriteria.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_evaluationCriteria.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_evaluationCriteria.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new EvaluationCriteriaWrapper((EvaluationCriteria)_evaluationCriteria.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria evaluationCriteria) {
		return _evaluationCriteria.compareTo(evaluationCriteria);
	}

	@Override
	public int hashCode() {
		return _evaluationCriteria.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria> toCacheModel() {
		return _evaluationCriteria.toCacheModel();
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria toEscapedModel() {
		return new EvaluationCriteriaWrapper(_evaluationCriteria.toEscapedModel());
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria toUnescapedModel() {
		return new EvaluationCriteriaWrapper(_evaluationCriteria.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _evaluationCriteria.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _evaluationCriteria.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_evaluationCriteria.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof EvaluationCriteriaWrapper)) {
			return false;
		}

		EvaluationCriteriaWrapper evaluationCriteriaWrapper = (EvaluationCriteriaWrapper)obj;

		if (Validator.equals(_evaluationCriteria,
					evaluationCriteriaWrapper._evaluationCriteria)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public EvaluationCriteria getWrappedEvaluationCriteria() {
		return _evaluationCriteria;
	}

	@Override
	public EvaluationCriteria getWrappedModel() {
		return _evaluationCriteria;
	}

	@Override
	public void resetOriginalValues() {
		_evaluationCriteria.resetOriginalValues();
	}

	private EvaluationCriteria _evaluationCriteria;
}