/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link GESelected}.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see GESelected
 * @generated
 */
public class GESelectedWrapper implements GESelected, ModelWrapper<GESelected> {
	public GESelectedWrapper(GESelected geSelected) {
		_geSelected = geSelected;
	}

	@Override
	public Class<?> getModelClass() {
		return GESelected.class;
	}

	@Override
	public String getModelClassName() {
		return GESelected.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ideaId", getIdeaId());
		attributes.put("nid", getNid());
		attributes.put("url", getUrl());
		attributes.put("icon", getIcon());
		attributes.put("title", getTitle());
		attributes.put("shortDescription", getShortDescription());
		attributes.put("label", getLabel());
		attributes.put("rank", getRank());
		attributes.put("chapter", getChapter());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ideaId = (Long)attributes.get("ideaId");

		if (ideaId != null) {
			setIdeaId(ideaId);
		}

		String nid = (String)attributes.get("nid");

		if (nid != null) {
			setNid(nid);
		}

		String url = (String)attributes.get("url");

		if (url != null) {
			setUrl(url);
		}

		String icon = (String)attributes.get("icon");

		if (icon != null) {
			setIcon(icon);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}

		String shortDescription = (String)attributes.get("shortDescription");

		if (shortDescription != null) {
			setShortDescription(shortDescription);
		}

		String label = (String)attributes.get("label");

		if (label != null) {
			setLabel(label);
		}

		String rank = (String)attributes.get("rank");

		if (rank != null) {
			setRank(rank);
		}

		String chapter = (String)attributes.get("chapter");

		if (chapter != null) {
			setChapter(chapter);
		}
	}

	/**
	* Returns the primary key of this g e selected.
	*
	* @return the primary key of this g e selected
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.service.persistence.GESelectedPK getPrimaryKey() {
		return _geSelected.getPrimaryKey();
	}

	/**
	* Sets the primary key of this g e selected.
	*
	* @param primaryKey the primary key of this g e selected
	*/
	@Override
	public void setPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.GESelectedPK primaryKey) {
		_geSelected.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the idea ID of this g e selected.
	*
	* @return the idea ID of this g e selected
	*/
	@Override
	public long getIdeaId() {
		return _geSelected.getIdeaId();
	}

	/**
	* Sets the idea ID of this g e selected.
	*
	* @param ideaId the idea ID of this g e selected
	*/
	@Override
	public void setIdeaId(long ideaId) {
		_geSelected.setIdeaId(ideaId);
	}

	/**
	* Returns the nid of this g e selected.
	*
	* @return the nid of this g e selected
	*/
	@Override
	public java.lang.String getNid() {
		return _geSelected.getNid();
	}

	/**
	* Sets the nid of this g e selected.
	*
	* @param nid the nid of this g e selected
	*/
	@Override
	public void setNid(java.lang.String nid) {
		_geSelected.setNid(nid);
	}

	/**
	* Returns the url of this g e selected.
	*
	* @return the url of this g e selected
	*/
	@Override
	public java.lang.String getUrl() {
		return _geSelected.getUrl();
	}

	/**
	* Sets the url of this g e selected.
	*
	* @param url the url of this g e selected
	*/
	@Override
	public void setUrl(java.lang.String url) {
		_geSelected.setUrl(url);
	}

	/**
	* Returns the icon of this g e selected.
	*
	* @return the icon of this g e selected
	*/
	@Override
	public java.lang.String getIcon() {
		return _geSelected.getIcon();
	}

	/**
	* Sets the icon of this g e selected.
	*
	* @param icon the icon of this g e selected
	*/
	@Override
	public void setIcon(java.lang.String icon) {
		_geSelected.setIcon(icon);
	}

	/**
	* Returns the title of this g e selected.
	*
	* @return the title of this g e selected
	*/
	@Override
	public java.lang.String getTitle() {
		return _geSelected.getTitle();
	}

	/**
	* Sets the title of this g e selected.
	*
	* @param title the title of this g e selected
	*/
	@Override
	public void setTitle(java.lang.String title) {
		_geSelected.setTitle(title);
	}

	/**
	* Returns the short description of this g e selected.
	*
	* @return the short description of this g e selected
	*/
	@Override
	public java.lang.String getShortDescription() {
		return _geSelected.getShortDescription();
	}

	/**
	* Sets the short description of this g e selected.
	*
	* @param shortDescription the short description of this g e selected
	*/
	@Override
	public void setShortDescription(java.lang.String shortDescription) {
		_geSelected.setShortDescription(shortDescription);
	}

	/**
	* Returns the label of this g e selected.
	*
	* @return the label of this g e selected
	*/
	@Override
	public java.lang.String getLabel() {
		return _geSelected.getLabel();
	}

	/**
	* Sets the label of this g e selected.
	*
	* @param label the label of this g e selected
	*/
	@Override
	public void setLabel(java.lang.String label) {
		_geSelected.setLabel(label);
	}

	/**
	* Returns the rank of this g e selected.
	*
	* @return the rank of this g e selected
	*/
	@Override
	public java.lang.String getRank() {
		return _geSelected.getRank();
	}

	/**
	* Sets the rank of this g e selected.
	*
	* @param rank the rank of this g e selected
	*/
	@Override
	public void setRank(java.lang.String rank) {
		_geSelected.setRank(rank);
	}

	/**
	* Returns the chapter of this g e selected.
	*
	* @return the chapter of this g e selected
	*/
	@Override
	public java.lang.String getChapter() {
		return _geSelected.getChapter();
	}

	/**
	* Sets the chapter of this g e selected.
	*
	* @param chapter the chapter of this g e selected
	*/
	@Override
	public void setChapter(java.lang.String chapter) {
		_geSelected.setChapter(chapter);
	}

	@Override
	public boolean isNew() {
		return _geSelected.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_geSelected.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _geSelected.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_geSelected.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _geSelected.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _geSelected.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_geSelected.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _geSelected.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_geSelected.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_geSelected.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_geSelected.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new GESelectedWrapper((GESelected)_geSelected.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.ideas.challenges.servicelayer.model.GESelected geSelected) {
		return _geSelected.compareTo(geSelected);
	}

	@Override
	public int hashCode() {
		return _geSelected.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.ideas.challenges.servicelayer.model.GESelected> toCacheModel() {
		return _geSelected.toCacheModel();
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.GESelected toEscapedModel() {
		return new GESelectedWrapper(_geSelected.toEscapedModel());
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.GESelected toUnescapedModel() {
		return new GESelectedWrapper(_geSelected.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _geSelected.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _geSelected.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_geSelected.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof GESelectedWrapper)) {
			return false;
		}

		GESelectedWrapper geSelectedWrapper = (GESelectedWrapper)obj;

		if (Validator.equals(_geSelected, geSelectedWrapper._geSelected)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public GESelected getWrappedGESelected() {
		return _geSelected;
	}

	@Override
	public GESelected getWrappedModel() {
		return _geSelected;
	}

	@Override
	public void resetOriginalValues() {
		_geSelected.resetOriginalValues();
	}

	private GESelected _geSelected;
}