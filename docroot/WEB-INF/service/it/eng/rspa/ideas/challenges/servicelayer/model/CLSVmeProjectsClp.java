/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.ideas.challenges.servicelayer.service.CLSVmeProjectsLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 */
public class CLSVmeProjectsClp extends BaseModelImpl<CLSVmeProjects>
	implements CLSVmeProjects {
	public CLSVmeProjectsClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return CLSVmeProjects.class;
	}

	@Override
	public String getModelClassName() {
		return CLSVmeProjects.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _recordId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setRecordId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _recordId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("recordId", getRecordId());
		attributes.put("vmeProjectId", getVmeProjectId());
		attributes.put("VmeProjectName", getVmeProjectName());
		attributes.put("isMockup", getIsMockup());
		attributes.put("ideaId", getIdeaId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long recordId = (Long)attributes.get("recordId");

		if (recordId != null) {
			setRecordId(recordId);
		}

		Long vmeProjectId = (Long)attributes.get("vmeProjectId");

		if (vmeProjectId != null) {
			setVmeProjectId(vmeProjectId);
		}

		String VmeProjectName = (String)attributes.get("VmeProjectName");

		if (VmeProjectName != null) {
			setVmeProjectName(VmeProjectName);
		}

		Boolean isMockup = (Boolean)attributes.get("isMockup");

		if (isMockup != null) {
			setIsMockup(isMockup);
		}

		Long ideaId = (Long)attributes.get("ideaId");

		if (ideaId != null) {
			setIdeaId(ideaId);
		}
	}

	@Override
	public long getRecordId() {
		return _recordId;
	}

	@Override
	public void setRecordId(long recordId) {
		_recordId = recordId;

		if (_clsVmeProjectsRemoteModel != null) {
			try {
				Class<?> clazz = _clsVmeProjectsRemoteModel.getClass();

				Method method = clazz.getMethod("setRecordId", long.class);

				method.invoke(_clsVmeProjectsRemoteModel, recordId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getVmeProjectId() {
		return _vmeProjectId;
	}

	@Override
	public void setVmeProjectId(long vmeProjectId) {
		_vmeProjectId = vmeProjectId;

		if (_clsVmeProjectsRemoteModel != null) {
			try {
				Class<?> clazz = _clsVmeProjectsRemoteModel.getClass();

				Method method = clazz.getMethod("setVmeProjectId", long.class);

				method.invoke(_clsVmeProjectsRemoteModel, vmeProjectId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getVmeProjectName() {
		return _VmeProjectName;
	}

	@Override
	public void setVmeProjectName(String VmeProjectName) {
		_VmeProjectName = VmeProjectName;

		if (_clsVmeProjectsRemoteModel != null) {
			try {
				Class<?> clazz = _clsVmeProjectsRemoteModel.getClass();

				Method method = clazz.getMethod("setVmeProjectName",
						String.class);

				method.invoke(_clsVmeProjectsRemoteModel, VmeProjectName);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public boolean getIsMockup() {
		return _isMockup;
	}

	@Override
	public boolean isIsMockup() {
		return _isMockup;
	}

	@Override
	public void setIsMockup(boolean isMockup) {
		_isMockup = isMockup;

		if (_clsVmeProjectsRemoteModel != null) {
			try {
				Class<?> clazz = _clsVmeProjectsRemoteModel.getClass();

				Method method = clazz.getMethod("setIsMockup", boolean.class);

				method.invoke(_clsVmeProjectsRemoteModel, isMockup);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdeaId() {
		return _ideaId;
	}

	@Override
	public void setIdeaId(long ideaId) {
		_ideaId = ideaId;

		if (_clsVmeProjectsRemoteModel != null) {
			try {
				Class<?> clazz = _clsVmeProjectsRemoteModel.getClass();

				Method method = clazz.getMethod("setIdeaId", long.class);

				method.invoke(_clsVmeProjectsRemoteModel, ideaId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getCLSVmeProjectsRemoteModel() {
		return _clsVmeProjectsRemoteModel;
	}

	public void setCLSVmeProjectsRemoteModel(
		BaseModel<?> clsVmeProjectsRemoteModel) {
		_clsVmeProjectsRemoteModel = clsVmeProjectsRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _clsVmeProjectsRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_clsVmeProjectsRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			CLSVmeProjectsLocalServiceUtil.addCLSVmeProjects(this);
		}
		else {
			CLSVmeProjectsLocalServiceUtil.updateCLSVmeProjects(this);
		}
	}

	@Override
	public CLSVmeProjects toEscapedModel() {
		return (CLSVmeProjects)ProxyUtil.newProxyInstance(CLSVmeProjects.class.getClassLoader(),
			new Class[] { CLSVmeProjects.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		CLSVmeProjectsClp clone = new CLSVmeProjectsClp();

		clone.setRecordId(getRecordId());
		clone.setVmeProjectId(getVmeProjectId());
		clone.setVmeProjectName(getVmeProjectName());
		clone.setIsMockup(getIsMockup());
		clone.setIdeaId(getIdeaId());

		return clone;
	}

	@Override
	public int compareTo(CLSVmeProjects clsVmeProjects) {
		long primaryKey = clsVmeProjects.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSVmeProjectsClp)) {
			return false;
		}

		CLSVmeProjectsClp clsVmeProjects = (CLSVmeProjectsClp)obj;

		long primaryKey = clsVmeProjects.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{recordId=");
		sb.append(getRecordId());
		sb.append(", vmeProjectId=");
		sb.append(getVmeProjectId());
		sb.append(", VmeProjectName=");
		sb.append(getVmeProjectName());
		sb.append(", isMockup=");
		sb.append(getIsMockup());
		sb.append(", ideaId=");
		sb.append(getIdeaId());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(19);

		sb.append("<model><model-name>");
		sb.append(
			"it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>recordId</column-name><column-value><![CDATA[");
		sb.append(getRecordId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>vmeProjectId</column-name><column-value><![CDATA[");
		sb.append(getVmeProjectId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>VmeProjectName</column-name><column-value><![CDATA[");
		sb.append(getVmeProjectName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>isMockup</column-name><column-value><![CDATA[");
		sb.append(getIsMockup());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ideaId</column-name><column-value><![CDATA[");
		sb.append(getIdeaId());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _recordId;
	private long _vmeProjectId;
	private String _VmeProjectName;
	private boolean _isMockup;
	private long _ideaId;
	private BaseModel<?> _clsVmeProjectsRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.class;
}