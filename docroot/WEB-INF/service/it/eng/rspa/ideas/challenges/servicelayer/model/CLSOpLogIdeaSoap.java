/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.eng.rspa.ideas.challenges.servicelayer.service.http.CLSOpLogIdeaServiceSoap}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.http.CLSOpLogIdeaServiceSoap
 * @generated
 */
public class CLSOpLogIdeaSoap implements Serializable {
	public static CLSOpLogIdeaSoap toSoapModel(CLSOpLogIdea model) {
		CLSOpLogIdeaSoap soapModel = new CLSOpLogIdeaSoap();

		soapModel.setOpLogId(model.getOpLogId());
		soapModel.setIdeaId(model.getIdeaId());
		soapModel.setUserId(model.getUserId());
		soapModel.setDate(model.getDate());
		soapModel.setExtraData(model.getExtraData());

		return soapModel;
	}

	public static CLSOpLogIdeaSoap[] toSoapModels(CLSOpLogIdea[] models) {
		CLSOpLogIdeaSoap[] soapModels = new CLSOpLogIdeaSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CLSOpLogIdeaSoap[][] toSoapModels(CLSOpLogIdea[][] models) {
		CLSOpLogIdeaSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CLSOpLogIdeaSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CLSOpLogIdeaSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CLSOpLogIdeaSoap[] toSoapModels(List<CLSOpLogIdea> models) {
		List<CLSOpLogIdeaSoap> soapModels = new ArrayList<CLSOpLogIdeaSoap>(models.size());

		for (CLSOpLogIdea model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CLSOpLogIdeaSoap[soapModels.size()]);
	}

	public CLSOpLogIdeaSoap() {
	}

	public long getPrimaryKey() {
		return _opLogId;
	}

	public void setPrimaryKey(long pk) {
		setOpLogId(pk);
	}

	public long getOpLogId() {
		return _opLogId;
	}

	public void setOpLogId(long opLogId) {
		_opLogId = opLogId;
	}

	public long getIdeaId() {
		return _ideaId;
	}

	public void setIdeaId(long ideaId) {
		_ideaId = ideaId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public Date getDate() {
		return _date;
	}

	public void setDate(Date date) {
		_date = date;
	}

	public String getExtraData() {
		return _extraData;
	}

	public void setExtraData(String extraData) {
		_extraData = extraData;
	}

	private long _opLogId;
	private long _ideaId;
	private long _userId;
	private Date _date;
	private String _extraData;
}