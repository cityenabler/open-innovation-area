/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link CLSIdeaService}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSIdeaService
 * @generated
 */
public class CLSIdeaServiceWrapper implements CLSIdeaService,
	ServiceWrapper<CLSIdeaService> {
	public CLSIdeaServiceWrapper(CLSIdeaService clsIdeaService) {
		_clsIdeaService = clsIdeaService;
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _clsIdeaService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_clsIdeaService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _clsIdeaService.invokeMethod(name, parameterTypes, arguments);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject publishScreenShot(
		java.lang.String body) {
		return _clsIdeaService.publishScreenShot(body);
	}

	@Override
	public com.liferay.portal.kernel.json.JSONObject vcProjectUpdate(
		java.lang.String vmeprojectid, java.lang.String vmeprojectname,
		boolean ismockup, java.lang.String ideaid) {
		return _clsIdeaService.vcProjectUpdate(vmeprojectid, vmeprojectname,
			ismockup, ideaid);
	}

	/**
	* @param fileEntryId
	
	questo metodo viene utilizzato per eliminare gli allegati con Ajax
	*/
	@Override
	public void deleteIdeaFile(long fileEntryId) {
		_clsIdeaService.deleteIdeaFile(fileEntryId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public CLSIdeaService getWrappedCLSIdeaService() {
		return _clsIdeaService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedCLSIdeaService(CLSIdeaService clsIdeaService) {
		_clsIdeaService = clsIdeaService;
	}

	@Override
	public CLSIdeaService getWrappedService() {
		return _clsIdeaService;
	}

	@Override
	public void setWrappedService(CLSIdeaService clsIdeaService) {
		_clsIdeaService = clsIdeaService;
	}

	private CLSIdeaService _clsIdeaService;
}