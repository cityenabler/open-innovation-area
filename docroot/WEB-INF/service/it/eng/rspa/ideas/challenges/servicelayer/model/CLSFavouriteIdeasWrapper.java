/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CLSFavouriteIdeas}.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSFavouriteIdeas
 * @generated
 */
public class CLSFavouriteIdeasWrapper implements CLSFavouriteIdeas,
	ModelWrapper<CLSFavouriteIdeas> {
	public CLSFavouriteIdeasWrapper(CLSFavouriteIdeas clsFavouriteIdeas) {
		_clsFavouriteIdeas = clsFavouriteIdeas;
	}

	@Override
	public Class<?> getModelClass() {
		return CLSFavouriteIdeas.class;
	}

	@Override
	public String getModelClassName() {
		return CLSFavouriteIdeas.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ideaID", getIdeaID());
		attributes.put("userId", getUserId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ideaID = (Long)attributes.get("ideaID");

		if (ideaID != null) {
			setIdeaID(ideaID);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}
	}

	/**
	* Returns the primary key of this c l s favourite ideas.
	*
	* @return the primary key of this c l s favourite ideas
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteIdeasPK getPrimaryKey() {
		return _clsFavouriteIdeas.getPrimaryKey();
	}

	/**
	* Sets the primary key of this c l s favourite ideas.
	*
	* @param primaryKey the primary key of this c l s favourite ideas
	*/
	@Override
	public void setPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteIdeasPK primaryKey) {
		_clsFavouriteIdeas.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the idea i d of this c l s favourite ideas.
	*
	* @return the idea i d of this c l s favourite ideas
	*/
	@Override
	public long getIdeaID() {
		return _clsFavouriteIdeas.getIdeaID();
	}

	/**
	* Sets the idea i d of this c l s favourite ideas.
	*
	* @param ideaID the idea i d of this c l s favourite ideas
	*/
	@Override
	public void setIdeaID(long ideaID) {
		_clsFavouriteIdeas.setIdeaID(ideaID);
	}

	/**
	* Returns the user ID of this c l s favourite ideas.
	*
	* @return the user ID of this c l s favourite ideas
	*/
	@Override
	public long getUserId() {
		return _clsFavouriteIdeas.getUserId();
	}

	/**
	* Sets the user ID of this c l s favourite ideas.
	*
	* @param userId the user ID of this c l s favourite ideas
	*/
	@Override
	public void setUserId(long userId) {
		_clsFavouriteIdeas.setUserId(userId);
	}

	/**
	* Returns the user uuid of this c l s favourite ideas.
	*
	* @return the user uuid of this c l s favourite ideas
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteIdeas.getUserUuid();
	}

	/**
	* Sets the user uuid of this c l s favourite ideas.
	*
	* @param userUuid the user uuid of this c l s favourite ideas
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_clsFavouriteIdeas.setUserUuid(userUuid);
	}

	@Override
	public boolean isNew() {
		return _clsFavouriteIdeas.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_clsFavouriteIdeas.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _clsFavouriteIdeas.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_clsFavouriteIdeas.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _clsFavouriteIdeas.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _clsFavouriteIdeas.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_clsFavouriteIdeas.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _clsFavouriteIdeas.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_clsFavouriteIdeas.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_clsFavouriteIdeas.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_clsFavouriteIdeas.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CLSFavouriteIdeasWrapper((CLSFavouriteIdeas)_clsFavouriteIdeas.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas clsFavouriteIdeas) {
		return _clsFavouriteIdeas.compareTo(clsFavouriteIdeas);
	}

	@Override
	public int hashCode() {
		return _clsFavouriteIdeas.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas> toCacheModel() {
		return _clsFavouriteIdeas.toCacheModel();
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas toEscapedModel() {
		return new CLSFavouriteIdeasWrapper(_clsFavouriteIdeas.toEscapedModel());
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas toUnescapedModel() {
		return new CLSFavouriteIdeasWrapper(_clsFavouriteIdeas.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _clsFavouriteIdeas.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _clsFavouriteIdeas.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_clsFavouriteIdeas.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSFavouriteIdeasWrapper)) {
			return false;
		}

		CLSFavouriteIdeasWrapper clsFavouriteIdeasWrapper = (CLSFavouriteIdeasWrapper)obj;

		if (Validator.equals(_clsFavouriteIdeas,
					clsFavouriteIdeasWrapper._clsFavouriteIdeas)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public CLSFavouriteIdeas getWrappedCLSFavouriteIdeas() {
		return _clsFavouriteIdeas;
	}

	@Override
	public CLSFavouriteIdeas getWrappedModel() {
		return _clsFavouriteIdeas;
	}

	@Override
	public void resetOriginalValues() {
		_clsFavouriteIdeas.resetOriginalValues();
	}

	private CLSFavouriteIdeas _clsFavouriteIdeas;
}