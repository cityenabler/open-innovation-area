/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.eng.rspa.ideas.challenges.servicelayer.service.http.CLSChallengeServiceSoap}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.http.CLSChallengeServiceSoap
 * @generated
 */
public class CLSChallengeSoap implements Serializable {
	public static CLSChallengeSoap toSoapModel(CLSChallenge model) {
		CLSChallengeSoap soapModel = new CLSChallengeSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setChallengeId(model.getChallengeId());
		soapModel.setChallengeTitle(model.getChallengeTitle());
		soapModel.setChallengeDescription(model.getChallengeDescription());
		soapModel.setChallengeWithReward(model.getChallengeWithReward());
		soapModel.setChallengeReward(model.getChallengeReward());
		soapModel.setNumberOfSelectableIdeas(model.getNumberOfSelectableIdeas());
		soapModel.setDateAdded(model.getDateAdded());
		soapModel.setChallengeStatus(model.getChallengeStatus());
		soapModel.setStatus(model.getStatus());
		soapModel.setStatusByUserId(model.getStatusByUserId());
		soapModel.setStatusByUserName(model.getStatusByUserName());
		soapModel.setStatusDate(model.getStatusDate());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setUserId(model.getUserId());
		soapModel.setDateEnd(model.getDateEnd());
		soapModel.setDateStart(model.getDateStart());
		soapModel.setLanguage(model.getLanguage());
		soapModel.setChallengeHashTag(model.getChallengeHashTag());
		soapModel.setDmFolderName(model.getDmFolderName());
		soapModel.setIdFolder(model.getIdFolder());
		soapModel.setRepresentativeImgUrl(model.getRepresentativeImgUrl());
		soapModel.setCalendarBooking(model.getCalendarBooking());

		return soapModel;
	}

	public static CLSChallengeSoap[] toSoapModels(CLSChallenge[] models) {
		CLSChallengeSoap[] soapModels = new CLSChallengeSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CLSChallengeSoap[][] toSoapModels(CLSChallenge[][] models) {
		CLSChallengeSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CLSChallengeSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CLSChallengeSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CLSChallengeSoap[] toSoapModels(List<CLSChallenge> models) {
		List<CLSChallengeSoap> soapModels = new ArrayList<CLSChallengeSoap>(models.size());

		for (CLSChallenge model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CLSChallengeSoap[soapModels.size()]);
	}

	public CLSChallengeSoap() {
	}

	public long getPrimaryKey() {
		return _challengeId;
	}

	public void setPrimaryKey(long pk) {
		setChallengeId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getChallengeId() {
		return _challengeId;
	}

	public void setChallengeId(long challengeId) {
		_challengeId = challengeId;
	}

	public String getChallengeTitle() {
		return _challengeTitle;
	}

	public void setChallengeTitle(String challengeTitle) {
		_challengeTitle = challengeTitle;
	}

	public String getChallengeDescription() {
		return _challengeDescription;
	}

	public void setChallengeDescription(String challengeDescription) {
		_challengeDescription = challengeDescription;
	}

	public boolean getChallengeWithReward() {
		return _challengeWithReward;
	}

	public boolean isChallengeWithReward() {
		return _challengeWithReward;
	}

	public void setChallengeWithReward(boolean challengeWithReward) {
		_challengeWithReward = challengeWithReward;
	}

	public String getChallengeReward() {
		return _challengeReward;
	}

	public void setChallengeReward(String challengeReward) {
		_challengeReward = challengeReward;
	}

	public int getNumberOfSelectableIdeas() {
		return _numberOfSelectableIdeas;
	}

	public void setNumberOfSelectableIdeas(int numberOfSelectableIdeas) {
		_numberOfSelectableIdeas = numberOfSelectableIdeas;
	}

	public Date getDateAdded() {
		return _dateAdded;
	}

	public void setDateAdded(Date dateAdded) {
		_dateAdded = dateAdded;
	}

	public String getChallengeStatus() {
		return _challengeStatus;
	}

	public void setChallengeStatus(String challengeStatus) {
		_challengeStatus = challengeStatus;
	}

	public int getStatus() {
		return _status;
	}

	public void setStatus(int status) {
		_status = status;
	}

	public long getStatusByUserId() {
		return _statusByUserId;
	}

	public void setStatusByUserId(long statusByUserId) {
		_statusByUserId = statusByUserId;
	}

	public String getStatusByUserName() {
		return _statusByUserName;
	}

	public void setStatusByUserName(String statusByUserName) {
		_statusByUserName = statusByUserName;
	}

	public Date getStatusDate() {
		return _statusDate;
	}

	public void setStatusDate(Date statusDate) {
		_statusDate = statusDate;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public Date getDateEnd() {
		return _dateEnd;
	}

	public void setDateEnd(Date dateEnd) {
		_dateEnd = dateEnd;
	}

	public Date getDateStart() {
		return _dateStart;
	}

	public void setDateStart(Date dateStart) {
		_dateStart = dateStart;
	}

	public String getLanguage() {
		return _language;
	}

	public void setLanguage(String language) {
		_language = language;
	}

	public String getChallengeHashTag() {
		return _challengeHashTag;
	}

	public void setChallengeHashTag(String challengeHashTag) {
		_challengeHashTag = challengeHashTag;
	}

	public String getDmFolderName() {
		return _dmFolderName;
	}

	public void setDmFolderName(String dmFolderName) {
		_dmFolderName = dmFolderName;
	}

	public long getIdFolder() {
		return _idFolder;
	}

	public void setIdFolder(long idFolder) {
		_idFolder = idFolder;
	}

	public String getRepresentativeImgUrl() {
		return _representativeImgUrl;
	}

	public void setRepresentativeImgUrl(String representativeImgUrl) {
		_representativeImgUrl = representativeImgUrl;
	}

	public long getCalendarBooking() {
		return _calendarBooking;
	}

	public void setCalendarBooking(long calendarBooking) {
		_calendarBooking = calendarBooking;
	}

	private String _uuid;
	private long _challengeId;
	private String _challengeTitle;
	private String _challengeDescription;
	private boolean _challengeWithReward;
	private String _challengeReward;
	private int _numberOfSelectableIdeas;
	private Date _dateAdded;
	private String _challengeStatus;
	private int _status;
	private long _statusByUserId;
	private String _statusByUserName;
	private Date _statusDate;
	private long _companyId;
	private long _groupId;
	private long _userId;
	private Date _dateEnd;
	private Date _dateStart;
	private String _language;
	private String _challengeHashTag;
	private String _dmFolderName;
	private long _idFolder;
	private String _representativeImgUrl;
	private long _calendarBooking;
}