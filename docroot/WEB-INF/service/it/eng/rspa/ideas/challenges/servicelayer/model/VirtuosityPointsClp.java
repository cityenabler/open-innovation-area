/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer;
import it.eng.rspa.ideas.challenges.servicelayer.service.VirtuosityPointsLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.VirtuosityPointsPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 */
public class VirtuosityPointsClp extends BaseModelImpl<VirtuosityPoints>
	implements VirtuosityPoints {
	public VirtuosityPointsClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return VirtuosityPoints.class;
	}

	@Override
	public String getModelClassName() {
		return VirtuosityPoints.class.getName();
	}

	@Override
	public VirtuosityPointsPK getPrimaryKey() {
		return new VirtuosityPointsPK(_challengeId, _position);
	}

	@Override
	public void setPrimaryKey(VirtuosityPointsPK primaryKey) {
		setChallengeId(primaryKey.challengeId);
		setPosition(primaryKey.position);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new VirtuosityPointsPK(_challengeId, _position);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((VirtuosityPointsPK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("challengeId", getChallengeId());
		attributes.put("position", getPosition());
		attributes.put("points", getPoints());
		attributes.put("dateAdded", getDateAdded());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long challengeId = (Long)attributes.get("challengeId");

		if (challengeId != null) {
			setChallengeId(challengeId);
		}

		Long position = (Long)attributes.get("position");

		if (position != null) {
			setPosition(position);
		}

		Long points = (Long)attributes.get("points");

		if (points != null) {
			setPoints(points);
		}

		Date dateAdded = (Date)attributes.get("dateAdded");

		if (dateAdded != null) {
			setDateAdded(dateAdded);
		}
	}

	@Override
	public long getChallengeId() {
		return _challengeId;
	}

	@Override
	public void setChallengeId(long challengeId) {
		_challengeId = challengeId;

		if (_virtuosityPointsRemoteModel != null) {
			try {
				Class<?> clazz = _virtuosityPointsRemoteModel.getClass();

				Method method = clazz.getMethod("setChallengeId", long.class);

				method.invoke(_virtuosityPointsRemoteModel, challengeId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getPosition() {
		return _position;
	}

	@Override
	public void setPosition(long position) {
		_position = position;

		if (_virtuosityPointsRemoteModel != null) {
			try {
				Class<?> clazz = _virtuosityPointsRemoteModel.getClass();

				Method method = clazz.getMethod("setPosition", long.class);

				method.invoke(_virtuosityPointsRemoteModel, position);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getPoints() {
		return _points;
	}

	@Override
	public void setPoints(long points) {
		_points = points;

		if (_virtuosityPointsRemoteModel != null) {
			try {
				Class<?> clazz = _virtuosityPointsRemoteModel.getClass();

				Method method = clazz.getMethod("setPoints", long.class);

				method.invoke(_virtuosityPointsRemoteModel, points);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public Date getDateAdded() {
		return _dateAdded;
	}

	@Override
	public void setDateAdded(Date dateAdded) {
		_dateAdded = dateAdded;

		if (_virtuosityPointsRemoteModel != null) {
			try {
				Class<?> clazz = _virtuosityPointsRemoteModel.getClass();

				Method method = clazz.getMethod("setDateAdded", Date.class);

				method.invoke(_virtuosityPointsRemoteModel, dateAdded);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getVirtuosityPointsRemoteModel() {
		return _virtuosityPointsRemoteModel;
	}

	public void setVirtuosityPointsRemoteModel(
		BaseModel<?> virtuosityPointsRemoteModel) {
		_virtuosityPointsRemoteModel = virtuosityPointsRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _virtuosityPointsRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_virtuosityPointsRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			VirtuosityPointsLocalServiceUtil.addVirtuosityPoints(this);
		}
		else {
			VirtuosityPointsLocalServiceUtil.updateVirtuosityPoints(this);
		}
	}

	@Override
	public VirtuosityPoints toEscapedModel() {
		return (VirtuosityPoints)ProxyUtil.newProxyInstance(VirtuosityPoints.class.getClassLoader(),
			new Class[] { VirtuosityPoints.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		VirtuosityPointsClp clone = new VirtuosityPointsClp();

		clone.setChallengeId(getChallengeId());
		clone.setPosition(getPosition());
		clone.setPoints(getPoints());
		clone.setDateAdded(getDateAdded());

		return clone;
	}

	@Override
	public int compareTo(VirtuosityPoints virtuosityPoints) {
		int value = 0;

		if (getChallengeId() < virtuosityPoints.getChallengeId()) {
			value = -1;
		}
		else if (getChallengeId() > virtuosityPoints.getChallengeId()) {
			value = 1;
		}
		else {
			value = 0;
		}

		value = value * -1;

		if (value != 0) {
			return value;
		}

		if (getPosition() < virtuosityPoints.getPosition()) {
			value = -1;
		}
		else if (getPosition() > virtuosityPoints.getPosition()) {
			value = 1;
		}
		else {
			value = 0;
		}

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof VirtuosityPointsClp)) {
			return false;
		}

		VirtuosityPointsClp virtuosityPoints = (VirtuosityPointsClp)obj;

		VirtuosityPointsPK primaryKey = virtuosityPoints.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{challengeId=");
		sb.append(getChallengeId());
		sb.append(", position=");
		sb.append(getPosition());
		sb.append(", points=");
		sb.append(getPoints());
		sb.append(", dateAdded=");
		sb.append(getDateAdded());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(16);

		sb.append("<model><model-name>");
		sb.append(
			"it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>challengeId</column-name><column-value><![CDATA[");
		sb.append(getChallengeId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>position</column-name><column-value><![CDATA[");
		sb.append(getPosition());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>points</column-name><column-value><![CDATA[");
		sb.append(getPoints());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dateAdded</column-name><column-value><![CDATA[");
		sb.append(getDateAdded());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _challengeId;
	private long _position;
	private long _points;
	private Date _dateAdded;
	private BaseModel<?> _virtuosityPointsRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.class;
}