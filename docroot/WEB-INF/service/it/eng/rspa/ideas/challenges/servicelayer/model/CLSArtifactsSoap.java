/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSArtifactsPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.eng.rspa.ideas.challenges.servicelayer.service.http.CLSArtifactsServiceSoap}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.http.CLSArtifactsServiceSoap
 * @generated
 */
public class CLSArtifactsSoap implements Serializable {
	public static CLSArtifactsSoap toSoapModel(CLSArtifacts model) {
		CLSArtifactsSoap soapModel = new CLSArtifactsSoap();

		soapModel.setIdeaId(model.getIdeaId());
		soapModel.setArtifactId(model.getArtifactId());

		return soapModel;
	}

	public static CLSArtifactsSoap[] toSoapModels(CLSArtifacts[] models) {
		CLSArtifactsSoap[] soapModels = new CLSArtifactsSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CLSArtifactsSoap[][] toSoapModels(CLSArtifacts[][] models) {
		CLSArtifactsSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CLSArtifactsSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CLSArtifactsSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CLSArtifactsSoap[] toSoapModels(List<CLSArtifacts> models) {
		List<CLSArtifactsSoap> soapModels = new ArrayList<CLSArtifactsSoap>(models.size());

		for (CLSArtifacts model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CLSArtifactsSoap[soapModels.size()]);
	}

	public CLSArtifactsSoap() {
	}

	public CLSArtifactsPK getPrimaryKey() {
		return new CLSArtifactsPK(_ideaId, _artifactId);
	}

	public void setPrimaryKey(CLSArtifactsPK pk) {
		setIdeaId(pk.ideaId);
		setArtifactId(pk.artifactId);
	}

	public long getIdeaId() {
		return _ideaId;
	}

	public void setIdeaId(long ideaId) {
		_ideaId = ideaId;
	}

	public long getArtifactId() {
		return _artifactId;
	}

	public void setArtifactId(long artifactId) {
		_artifactId = artifactId;
	}

	private long _ideaId;
	private long _artifactId;
}