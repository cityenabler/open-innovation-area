/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.eng.rspa.ideas.challenges.servicelayer.service.http.CLSRequisitiServiceSoap}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.http.CLSRequisitiServiceSoap
 * @generated
 */
public class CLSRequisitiSoap implements Serializable {
	public static CLSRequisitiSoap toSoapModel(CLSRequisiti model) {
		CLSRequisitiSoap soapModel = new CLSRequisitiSoap();

		soapModel.setRequisitoId(model.getRequisitoId());
		soapModel.setIdeaId(model.getIdeaId());
		soapModel.setTaskId(model.getTaskId());
		soapModel.setTipo(model.getTipo());
		soapModel.setDescrizione(model.getDescrizione());
		soapModel.setHelp(model.getHelp());
		soapModel.setAuthorUser(model.getAuthorUser());
		soapModel.setCategory(model.getCategory());
		soapModel.setMultiTask(model.getMultiTask());
		soapModel.setOutcomeFile(model.getOutcomeFile());
		soapModel.setTaskDoubleField(model.getTaskDoubleField());

		return soapModel;
	}

	public static CLSRequisitiSoap[] toSoapModels(CLSRequisiti[] models) {
		CLSRequisitiSoap[] soapModels = new CLSRequisitiSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CLSRequisitiSoap[][] toSoapModels(CLSRequisiti[][] models) {
		CLSRequisitiSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CLSRequisitiSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CLSRequisitiSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CLSRequisitiSoap[] toSoapModels(List<CLSRequisiti> models) {
		List<CLSRequisitiSoap> soapModels = new ArrayList<CLSRequisitiSoap>(models.size());

		for (CLSRequisiti model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CLSRequisitiSoap[soapModels.size()]);
	}

	public CLSRequisitiSoap() {
	}

	public long getPrimaryKey() {
		return _requisitoId;
	}

	public void setPrimaryKey(long pk) {
		setRequisitoId(pk);
	}

	public long getRequisitoId() {
		return _requisitoId;
	}

	public void setRequisitoId(long requisitoId) {
		_requisitoId = requisitoId;
	}

	public long getIdeaId() {
		return _ideaId;
	}

	public void setIdeaId(long ideaId) {
		_ideaId = ideaId;
	}

	public long getTaskId() {
		return _taskId;
	}

	public void setTaskId(long taskId) {
		_taskId = taskId;
	}

	public String getTipo() {
		return _tipo;
	}

	public void setTipo(String tipo) {
		_tipo = tipo;
	}

	public String getDescrizione() {
		return _descrizione;
	}

	public void setDescrizione(String descrizione) {
		_descrizione = descrizione;
	}

	public String getHelp() {
		return _help;
	}

	public void setHelp(String help) {
		_help = help;
	}

	public long getAuthorUser() {
		return _authorUser;
	}

	public void setAuthorUser(long authorUser) {
		_authorUser = authorUser;
	}

	public String getCategory() {
		return _category;
	}

	public void setCategory(String category) {
		_category = category;
	}

	public boolean getMultiTask() {
		return _multiTask;
	}

	public boolean isMultiTask() {
		return _multiTask;
	}

	public void setMultiTask(boolean multiTask) {
		_multiTask = multiTask;
	}

	public boolean getOutcomeFile() {
		return _outcomeFile;
	}

	public boolean isOutcomeFile() {
		return _outcomeFile;
	}

	public void setOutcomeFile(boolean outcomeFile) {
		_outcomeFile = outcomeFile;
	}

	public boolean getTaskDoubleField() {
		return _taskDoubleField;
	}

	public boolean isTaskDoubleField() {
		return _taskDoubleField;
	}

	public void setTaskDoubleField(boolean taskDoubleField) {
		_taskDoubleField = taskDoubleField;
	}

	private long _requisitoId;
	private long _ideaId;
	private long _taskId;
	private String _tipo;
	private String _descrizione;
	private String _help;
	private long _authorUser;
	private String _category;
	private boolean _multiTask;
	private boolean _outcomeFile;
	private boolean _taskDoubleField;
}