/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail;

/**
 * The persistence interface for the invited mail service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see InvitedMailPersistenceImpl
 * @see InvitedMailUtil
 * @generated
 */
public interface InvitedMailPersistence extends BasePersistence<InvitedMail> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link InvitedMailUtil} to access the invited mail persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the invited mails where mail = &#63;.
	*
	* @param mail the mail
	* @return the matching invited mails
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail> findBybyMail(
		java.lang.String mail)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the invited mails where mail = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.InvitedMailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param mail the mail
	* @param start the lower bound of the range of invited mails
	* @param end the upper bound of the range of invited mails (not inclusive)
	* @return the range of matching invited mails
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail> findBybyMail(
		java.lang.String mail, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the invited mails where mail = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.InvitedMailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param mail the mail
	* @param start the lower bound of the range of invited mails
	* @param end the upper bound of the range of invited mails (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching invited mails
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail> findBybyMail(
		java.lang.String mail, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first invited mail in the ordered set where mail = &#63;.
	*
	* @param mail the mail
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching invited mail
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException if a matching invited mail could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail findBybyMail_First(
		java.lang.String mail,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException;

	/**
	* Returns the first invited mail in the ordered set where mail = &#63;.
	*
	* @param mail the mail
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching invited mail, or <code>null</code> if a matching invited mail could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail fetchBybyMail_First(
		java.lang.String mail,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last invited mail in the ordered set where mail = &#63;.
	*
	* @param mail the mail
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching invited mail
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException if a matching invited mail could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail findBybyMail_Last(
		java.lang.String mail,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException;

	/**
	* Returns the last invited mail in the ordered set where mail = &#63;.
	*
	* @param mail the mail
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching invited mail, or <code>null</code> if a matching invited mail could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail fetchBybyMail_Last(
		java.lang.String mail,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the invited mails before and after the current invited mail in the ordered set where mail = &#63;.
	*
	* @param invitedMailPK the primary key of the current invited mail
	* @param mail the mail
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next invited mail
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException if a invited mail with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail[] findBybyMail_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.InvitedMailPK invitedMailPK,
		java.lang.String mail,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException;

	/**
	* Removes all the invited mails where mail = &#63; from the database.
	*
	* @param mail the mail
	* @throws SystemException if a system exception occurred
	*/
	public void removeBybyMail(java.lang.String mail)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of invited mails where mail = &#63;.
	*
	* @param mail the mail
	* @return the number of matching invited mails
	* @throws SystemException if a system exception occurred
	*/
	public int countBybyMail(java.lang.String mail)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the invited mails where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @return the matching invited mails
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail> findBybyIdeaId(
		long ideaId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the invited mails where ideaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.InvitedMailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param start the lower bound of the range of invited mails
	* @param end the upper bound of the range of invited mails (not inclusive)
	* @return the range of matching invited mails
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail> findBybyIdeaId(
		long ideaId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the invited mails where ideaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.InvitedMailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param start the lower bound of the range of invited mails
	* @param end the upper bound of the range of invited mails (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching invited mails
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail> findBybyIdeaId(
		long ideaId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first invited mail in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching invited mail
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException if a matching invited mail could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail findBybyIdeaId_First(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException;

	/**
	* Returns the first invited mail in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching invited mail, or <code>null</code> if a matching invited mail could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail fetchBybyIdeaId_First(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last invited mail in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching invited mail
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException if a matching invited mail could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail findBybyIdeaId_Last(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException;

	/**
	* Returns the last invited mail in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching invited mail, or <code>null</code> if a matching invited mail could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail fetchBybyIdeaId_Last(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the invited mails before and after the current invited mail in the ordered set where ideaId = &#63;.
	*
	* @param invitedMailPK the primary key of the current invited mail
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next invited mail
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException if a invited mail with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail[] findBybyIdeaId_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.InvitedMailPK invitedMailPK,
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException;

	/**
	* Removes all the invited mails where ideaId = &#63; from the database.
	*
	* @param ideaId the idea ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeBybyIdeaId(long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of invited mails where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @return the number of matching invited mails
	* @throws SystemException if a system exception occurred
	*/
	public int countBybyIdeaId(long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the invited mail in the entity cache if it is enabled.
	*
	* @param invitedMail the invited mail
	*/
	public void cacheResult(
		it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail invitedMail);

	/**
	* Caches the invited mails in the entity cache if it is enabled.
	*
	* @param invitedMails the invited mails
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail> invitedMails);

	/**
	* Creates a new invited mail with the primary key. Does not add the invited mail to the database.
	*
	* @param invitedMailPK the primary key for the new invited mail
	* @return the new invited mail
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail create(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.InvitedMailPK invitedMailPK);

	/**
	* Removes the invited mail with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param invitedMailPK the primary key of the invited mail
	* @return the invited mail that was removed
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException if a invited mail with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail remove(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.InvitedMailPK invitedMailPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException;

	public it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail invitedMail)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the invited mail with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException} if it could not be found.
	*
	* @param invitedMailPK the primary key of the invited mail
	* @return the invited mail
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException if a invited mail with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail findByPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.InvitedMailPK invitedMailPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException;

	/**
	* Returns the invited mail with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param invitedMailPK the primary key of the invited mail
	* @return the invited mail, or <code>null</code> if a invited mail with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail fetchByPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.InvitedMailPK invitedMailPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the invited mails.
	*
	* @return the invited mails
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the invited mails.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.InvitedMailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of invited mails
	* @param end the upper bound of the range of invited mails (not inclusive)
	* @return the range of invited mails
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the invited mails.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.InvitedMailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of invited mails
	* @param end the upper bound of the range of invited mails (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of invited mails
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the invited mails from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of invited mails.
	*
	* @return the number of invited mails
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}