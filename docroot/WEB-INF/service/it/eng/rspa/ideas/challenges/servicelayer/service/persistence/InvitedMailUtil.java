/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail;

import java.util.List;

/**
 * The persistence utility for the invited mail service. This utility wraps {@link InvitedMailPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see InvitedMailPersistence
 * @see InvitedMailPersistenceImpl
 * @generated
 */
public class InvitedMailUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(InvitedMail invitedMail) {
		getPersistence().clearCache(invitedMail);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<InvitedMail> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<InvitedMail> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<InvitedMail> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static InvitedMail update(InvitedMail invitedMail)
		throws SystemException {
		return getPersistence().update(invitedMail);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static InvitedMail update(InvitedMail invitedMail,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(invitedMail, serviceContext);
	}

	/**
	* Returns all the invited mails where mail = &#63;.
	*
	* @param mail the mail
	* @return the matching invited mails
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail> findBybyMail(
		java.lang.String mail)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBybyMail(mail);
	}

	/**
	* Returns a range of all the invited mails where mail = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.InvitedMailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param mail the mail
	* @param start the lower bound of the range of invited mails
	* @param end the upper bound of the range of invited mails (not inclusive)
	* @return the range of matching invited mails
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail> findBybyMail(
		java.lang.String mail, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBybyMail(mail, start, end);
	}

	/**
	* Returns an ordered range of all the invited mails where mail = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.InvitedMailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param mail the mail
	* @param start the lower bound of the range of invited mails
	* @param end the upper bound of the range of invited mails (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching invited mails
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail> findBybyMail(
		java.lang.String mail, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBybyMail(mail, start, end, orderByComparator);
	}

	/**
	* Returns the first invited mail in the ordered set where mail = &#63;.
	*
	* @param mail the mail
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching invited mail
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException if a matching invited mail could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail findBybyMail_First(
		java.lang.String mail,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException {
		return getPersistence().findBybyMail_First(mail, orderByComparator);
	}

	/**
	* Returns the first invited mail in the ordered set where mail = &#63;.
	*
	* @param mail the mail
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching invited mail, or <code>null</code> if a matching invited mail could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail fetchBybyMail_First(
		java.lang.String mail,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBybyMail_First(mail, orderByComparator);
	}

	/**
	* Returns the last invited mail in the ordered set where mail = &#63;.
	*
	* @param mail the mail
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching invited mail
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException if a matching invited mail could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail findBybyMail_Last(
		java.lang.String mail,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException {
		return getPersistence().findBybyMail_Last(mail, orderByComparator);
	}

	/**
	* Returns the last invited mail in the ordered set where mail = &#63;.
	*
	* @param mail the mail
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching invited mail, or <code>null</code> if a matching invited mail could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail fetchBybyMail_Last(
		java.lang.String mail,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBybyMail_Last(mail, orderByComparator);
	}

	/**
	* Returns the invited mails before and after the current invited mail in the ordered set where mail = &#63;.
	*
	* @param invitedMailPK the primary key of the current invited mail
	* @param mail the mail
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next invited mail
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException if a invited mail with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail[] findBybyMail_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.InvitedMailPK invitedMailPK,
		java.lang.String mail,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException {
		return getPersistence()
				   .findBybyMail_PrevAndNext(invitedMailPK, mail,
			orderByComparator);
	}

	/**
	* Removes all the invited mails where mail = &#63; from the database.
	*
	* @param mail the mail
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBybyMail(java.lang.String mail)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBybyMail(mail);
	}

	/**
	* Returns the number of invited mails where mail = &#63;.
	*
	* @param mail the mail
	* @return the number of matching invited mails
	* @throws SystemException if a system exception occurred
	*/
	public static int countBybyMail(java.lang.String mail)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBybyMail(mail);
	}

	/**
	* Returns all the invited mails where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @return the matching invited mails
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail> findBybyIdeaId(
		long ideaId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBybyIdeaId(ideaId);
	}

	/**
	* Returns a range of all the invited mails where ideaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.InvitedMailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param start the lower bound of the range of invited mails
	* @param end the upper bound of the range of invited mails (not inclusive)
	* @return the range of matching invited mails
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail> findBybyIdeaId(
		long ideaId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findBybyIdeaId(ideaId, start, end);
	}

	/**
	* Returns an ordered range of all the invited mails where ideaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.InvitedMailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param start the lower bound of the range of invited mails
	* @param end the upper bound of the range of invited mails (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching invited mails
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail> findBybyIdeaId(
		long ideaId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findBybyIdeaId(ideaId, start, end, orderByComparator);
	}

	/**
	* Returns the first invited mail in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching invited mail
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException if a matching invited mail could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail findBybyIdeaId_First(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException {
		return getPersistence().findBybyIdeaId_First(ideaId, orderByComparator);
	}

	/**
	* Returns the first invited mail in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching invited mail, or <code>null</code> if a matching invited mail could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail fetchBybyIdeaId_First(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBybyIdeaId_First(ideaId, orderByComparator);
	}

	/**
	* Returns the last invited mail in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching invited mail
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException if a matching invited mail could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail findBybyIdeaId_Last(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException {
		return getPersistence().findBybyIdeaId_Last(ideaId, orderByComparator);
	}

	/**
	* Returns the last invited mail in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching invited mail, or <code>null</code> if a matching invited mail could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail fetchBybyIdeaId_Last(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchBybyIdeaId_Last(ideaId, orderByComparator);
	}

	/**
	* Returns the invited mails before and after the current invited mail in the ordered set where ideaId = &#63;.
	*
	* @param invitedMailPK the primary key of the current invited mail
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next invited mail
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException if a invited mail with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail[] findBybyIdeaId_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.InvitedMailPK invitedMailPK,
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException {
		return getPersistence()
				   .findBybyIdeaId_PrevAndNext(invitedMailPK, ideaId,
			orderByComparator);
	}

	/**
	* Removes all the invited mails where ideaId = &#63; from the database.
	*
	* @param ideaId the idea ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeBybyIdeaId(long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeBybyIdeaId(ideaId);
	}

	/**
	* Returns the number of invited mails where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @return the number of matching invited mails
	* @throws SystemException if a system exception occurred
	*/
	public static int countBybyIdeaId(long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countBybyIdeaId(ideaId);
	}

	/**
	* Caches the invited mail in the entity cache if it is enabled.
	*
	* @param invitedMail the invited mail
	*/
	public static void cacheResult(
		it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail invitedMail) {
		getPersistence().cacheResult(invitedMail);
	}

	/**
	* Caches the invited mails in the entity cache if it is enabled.
	*
	* @param invitedMails the invited mails
	*/
	public static void cacheResult(
		java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail> invitedMails) {
		getPersistence().cacheResult(invitedMails);
	}

	/**
	* Creates a new invited mail with the primary key. Does not add the invited mail to the database.
	*
	* @param invitedMailPK the primary key for the new invited mail
	* @return the new invited mail
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail create(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.InvitedMailPK invitedMailPK) {
		return getPersistence().create(invitedMailPK);
	}

	/**
	* Removes the invited mail with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param invitedMailPK the primary key of the invited mail
	* @return the invited mail that was removed
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException if a invited mail with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail remove(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.InvitedMailPK invitedMailPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException {
		return getPersistence().remove(invitedMailPK);
	}

	public static it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail invitedMail)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(invitedMail);
	}

	/**
	* Returns the invited mail with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException} if it could not be found.
	*
	* @param invitedMailPK the primary key of the invited mail
	* @return the invited mail
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException if a invited mail with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail findByPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.InvitedMailPK invitedMailPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException {
		return getPersistence().findByPrimaryKey(invitedMailPK);
	}

	/**
	* Returns the invited mail with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param invitedMailPK the primary key of the invited mail
	* @return the invited mail, or <code>null</code> if a invited mail with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail fetchByPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.InvitedMailPK invitedMailPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(invitedMailPK);
	}

	/**
	* Returns all the invited mails.
	*
	* @return the invited mails
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the invited mails.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.InvitedMailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of invited mails
	* @param end the upper bound of the range of invited mails (not inclusive)
	* @return the range of invited mails
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the invited mails.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.InvitedMailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of invited mails
	* @param end the upper bound of the range of invited mails (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of invited mails
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the invited mails from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of invited mails.
	*
	* @return the number of invited mails
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static InvitedMailPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (InvitedMailPersistence)PortletBeanLocatorUtil.locate(it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.getServletContextName(),
					InvitedMailPersistence.class.getName());

			ReferenceRegistry.registerReference(InvitedMailUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(InvitedMailPersistence persistence) {
	}

	private static InvitedMailPersistence _persistence;
}