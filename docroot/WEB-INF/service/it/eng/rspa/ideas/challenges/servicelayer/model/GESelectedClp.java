/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer;
import it.eng.rspa.ideas.challenges.servicelayer.service.GESelectedLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.GESelectedPK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 */
public class GESelectedClp extends BaseModelImpl<GESelected>
	implements GESelected {
	public GESelectedClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return GESelected.class;
	}

	@Override
	public String getModelClassName() {
		return GESelected.class.getName();
	}

	@Override
	public GESelectedPK getPrimaryKey() {
		return new GESelectedPK(_ideaId, _nid);
	}

	@Override
	public void setPrimaryKey(GESelectedPK primaryKey) {
		setIdeaId(primaryKey.ideaId);
		setNid(primaryKey.nid);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new GESelectedPK(_ideaId, _nid);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((GESelectedPK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ideaId", getIdeaId());
		attributes.put("nid", getNid());
		attributes.put("url", getUrl());
		attributes.put("icon", getIcon());
		attributes.put("title", getTitle());
		attributes.put("shortDescription", getShortDescription());
		attributes.put("label", getLabel());
		attributes.put("rank", getRank());
		attributes.put("chapter", getChapter());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ideaId = (Long)attributes.get("ideaId");

		if (ideaId != null) {
			setIdeaId(ideaId);
		}

		String nid = (String)attributes.get("nid");

		if (nid != null) {
			setNid(nid);
		}

		String url = (String)attributes.get("url");

		if (url != null) {
			setUrl(url);
		}

		String icon = (String)attributes.get("icon");

		if (icon != null) {
			setIcon(icon);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}

		String shortDescription = (String)attributes.get("shortDescription");

		if (shortDescription != null) {
			setShortDescription(shortDescription);
		}

		String label = (String)attributes.get("label");

		if (label != null) {
			setLabel(label);
		}

		String rank = (String)attributes.get("rank");

		if (rank != null) {
			setRank(rank);
		}

		String chapter = (String)attributes.get("chapter");

		if (chapter != null) {
			setChapter(chapter);
		}
	}

	@Override
	public long getIdeaId() {
		return _ideaId;
	}

	@Override
	public void setIdeaId(long ideaId) {
		_ideaId = ideaId;

		if (_geSelectedRemoteModel != null) {
			try {
				Class<?> clazz = _geSelectedRemoteModel.getClass();

				Method method = clazz.getMethod("setIdeaId", long.class);

				method.invoke(_geSelectedRemoteModel, ideaId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getNid() {
		return _nid;
	}

	@Override
	public void setNid(String nid) {
		_nid = nid;

		if (_geSelectedRemoteModel != null) {
			try {
				Class<?> clazz = _geSelectedRemoteModel.getClass();

				Method method = clazz.getMethod("setNid", String.class);

				method.invoke(_geSelectedRemoteModel, nid);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUrl() {
		return _url;
	}

	@Override
	public void setUrl(String url) {
		_url = url;

		if (_geSelectedRemoteModel != null) {
			try {
				Class<?> clazz = _geSelectedRemoteModel.getClass();

				Method method = clazz.getMethod("setUrl", String.class);

				method.invoke(_geSelectedRemoteModel, url);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getIcon() {
		return _icon;
	}

	@Override
	public void setIcon(String icon) {
		_icon = icon;

		if (_geSelectedRemoteModel != null) {
			try {
				Class<?> clazz = _geSelectedRemoteModel.getClass();

				Method method = clazz.getMethod("setIcon", String.class);

				method.invoke(_geSelectedRemoteModel, icon);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getTitle() {
		return _title;
	}

	@Override
	public void setTitle(String title) {
		_title = title;

		if (_geSelectedRemoteModel != null) {
			try {
				Class<?> clazz = _geSelectedRemoteModel.getClass();

				Method method = clazz.getMethod("setTitle", String.class);

				method.invoke(_geSelectedRemoteModel, title);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getShortDescription() {
		return _shortDescription;
	}

	@Override
	public void setShortDescription(String shortDescription) {
		_shortDescription = shortDescription;

		if (_geSelectedRemoteModel != null) {
			try {
				Class<?> clazz = _geSelectedRemoteModel.getClass();

				Method method = clazz.getMethod("setShortDescription",
						String.class);

				method.invoke(_geSelectedRemoteModel, shortDescription);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getLabel() {
		return _label;
	}

	@Override
	public void setLabel(String label) {
		_label = label;

		if (_geSelectedRemoteModel != null) {
			try {
				Class<?> clazz = _geSelectedRemoteModel.getClass();

				Method method = clazz.getMethod("setLabel", String.class);

				method.invoke(_geSelectedRemoteModel, label);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getRank() {
		return _rank;
	}

	@Override
	public void setRank(String rank) {
		_rank = rank;

		if (_geSelectedRemoteModel != null) {
			try {
				Class<?> clazz = _geSelectedRemoteModel.getClass();

				Method method = clazz.getMethod("setRank", String.class);

				method.invoke(_geSelectedRemoteModel, rank);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getChapter() {
		return _chapter;
	}

	@Override
	public void setChapter(String chapter) {
		_chapter = chapter;

		if (_geSelectedRemoteModel != null) {
			try {
				Class<?> clazz = _geSelectedRemoteModel.getClass();

				Method method = clazz.getMethod("setChapter", String.class);

				method.invoke(_geSelectedRemoteModel, chapter);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getGESelectedRemoteModel() {
		return _geSelectedRemoteModel;
	}

	public void setGESelectedRemoteModel(BaseModel<?> geSelectedRemoteModel) {
		_geSelectedRemoteModel = geSelectedRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _geSelectedRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_geSelectedRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			GESelectedLocalServiceUtil.addGESelected(this);
		}
		else {
			GESelectedLocalServiceUtil.updateGESelected(this);
		}
	}

	@Override
	public GESelected toEscapedModel() {
		return (GESelected)ProxyUtil.newProxyInstance(GESelected.class.getClassLoader(),
			new Class[] { GESelected.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		GESelectedClp clone = new GESelectedClp();

		clone.setIdeaId(getIdeaId());
		clone.setNid(getNid());
		clone.setUrl(getUrl());
		clone.setIcon(getIcon());
		clone.setTitle(getTitle());
		clone.setShortDescription(getShortDescription());
		clone.setLabel(getLabel());
		clone.setRank(getRank());
		clone.setChapter(getChapter());

		return clone;
	}

	@Override
	public int compareTo(GESelected geSelected) {
		int value = 0;

		if (getIdeaId() < geSelected.getIdeaId()) {
			value = -1;
		}
		else if (getIdeaId() > geSelected.getIdeaId()) {
			value = 1;
		}
		else {
			value = 0;
		}

		value = value * -1;

		if (value != 0) {
			return value;
		}

		value = getTitle().compareTo(geSelected.getTitle());

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof GESelectedClp)) {
			return false;
		}

		GESelectedClp geSelected = (GESelectedClp)obj;

		GESelectedPK primaryKey = geSelected.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{ideaId=");
		sb.append(getIdeaId());
		sb.append(", nid=");
		sb.append(getNid());
		sb.append(", url=");
		sb.append(getUrl());
		sb.append(", icon=");
		sb.append(getIcon());
		sb.append(", title=");
		sb.append(getTitle());
		sb.append(", shortDescription=");
		sb.append(getShortDescription());
		sb.append(", label=");
		sb.append(getLabel());
		sb.append(", rank=");
		sb.append(getRank());
		sb.append(", chapter=");
		sb.append(getChapter());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(31);

		sb.append("<model><model-name>");
		sb.append("it.eng.rspa.ideas.challenges.servicelayer.model.GESelected");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>ideaId</column-name><column-value><![CDATA[");
		sb.append(getIdeaId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>nid</column-name><column-value><![CDATA[");
		sb.append(getNid());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>url</column-name><column-value><![CDATA[");
		sb.append(getUrl());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>icon</column-name><column-value><![CDATA[");
		sb.append(getIcon());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>title</column-name><column-value><![CDATA[");
		sb.append(getTitle());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>shortDescription</column-name><column-value><![CDATA[");
		sb.append(getShortDescription());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>label</column-name><column-value><![CDATA[");
		sb.append(getLabel());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>rank</column-name><column-value><![CDATA[");
		sb.append(getRank());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>chapter</column-name><column-value><![CDATA[");
		sb.append(getChapter());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _ideaId;
	private String _nid;
	private String _url;
	private String _icon;
	private String _title;
	private String _shortDescription;
	private String _label;
	private String _rank;
	private String _chapter;
	private BaseModel<?> _geSelectedRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.class;
}