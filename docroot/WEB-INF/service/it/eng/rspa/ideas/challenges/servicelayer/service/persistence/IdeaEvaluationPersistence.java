/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation;

/**
 * The persistence interface for the idea evaluation service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see IdeaEvaluationPersistenceImpl
 * @see IdeaEvaluationUtil
 * @generated
 */
public interface IdeaEvaluationPersistence extends BasePersistence<IdeaEvaluation> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link IdeaEvaluationUtil} to access the idea evaluation persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns the idea evaluation where criteriaId = &#63; and ideaId = &#63; or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException} if it could not be found.
	*
	* @param criteriaId the criteria ID
	* @param ideaId the idea ID
	* @return the matching idea evaluation
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException if a matching idea evaluation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation findBycriteriaAndIdeaId(
		long criteriaId, long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException;

	/**
	* Returns the idea evaluation where criteriaId = &#63; and ideaId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param criteriaId the criteria ID
	* @param ideaId the idea ID
	* @return the matching idea evaluation, or <code>null</code> if a matching idea evaluation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation fetchBycriteriaAndIdeaId(
		long criteriaId, long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the idea evaluation where criteriaId = &#63; and ideaId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param criteriaId the criteria ID
	* @param ideaId the idea ID
	* @param retrieveFromCache whether to use the finder cache
	* @return the matching idea evaluation, or <code>null</code> if a matching idea evaluation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation fetchBycriteriaAndIdeaId(
		long criteriaId, long ideaId, boolean retrieveFromCache)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes the idea evaluation where criteriaId = &#63; and ideaId = &#63; from the database.
	*
	* @param criteriaId the criteria ID
	* @param ideaId the idea ID
	* @return the idea evaluation that was removed
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation removeBycriteriaAndIdeaId(
		long criteriaId, long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException;

	/**
	* Returns the number of idea evaluations where criteriaId = &#63; and ideaId = &#63;.
	*
	* @param criteriaId the criteria ID
	* @param ideaId the idea ID
	* @return the number of matching idea evaluations
	* @throws SystemException if a system exception occurred
	*/
	public int countBycriteriaAndIdeaId(long criteriaId, long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the idea evaluations where criteriaId = &#63;.
	*
	* @param criteriaId the criteria ID
	* @return the matching idea evaluations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation> findBycriteriaId(
		long criteriaId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the idea evaluations where criteriaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param criteriaId the criteria ID
	* @param start the lower bound of the range of idea evaluations
	* @param end the upper bound of the range of idea evaluations (not inclusive)
	* @return the range of matching idea evaluations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation> findBycriteriaId(
		long criteriaId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the idea evaluations where criteriaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param criteriaId the criteria ID
	* @param start the lower bound of the range of idea evaluations
	* @param end the upper bound of the range of idea evaluations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching idea evaluations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation> findBycriteriaId(
		long criteriaId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first idea evaluation in the ordered set where criteriaId = &#63;.
	*
	* @param criteriaId the criteria ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching idea evaluation
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException if a matching idea evaluation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation findBycriteriaId_First(
		long criteriaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException;

	/**
	* Returns the first idea evaluation in the ordered set where criteriaId = &#63;.
	*
	* @param criteriaId the criteria ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching idea evaluation, or <code>null</code> if a matching idea evaluation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation fetchBycriteriaId_First(
		long criteriaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last idea evaluation in the ordered set where criteriaId = &#63;.
	*
	* @param criteriaId the criteria ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching idea evaluation
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException if a matching idea evaluation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation findBycriteriaId_Last(
		long criteriaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException;

	/**
	* Returns the last idea evaluation in the ordered set where criteriaId = &#63;.
	*
	* @param criteriaId the criteria ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching idea evaluation, or <code>null</code> if a matching idea evaluation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation fetchBycriteriaId_Last(
		long criteriaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the idea evaluations before and after the current idea evaluation in the ordered set where criteriaId = &#63;.
	*
	* @param ideaEvaluationPK the primary key of the current idea evaluation
	* @param criteriaId the criteria ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next idea evaluation
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException if a idea evaluation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation[] findBycriteriaId_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.IdeaEvaluationPK ideaEvaluationPK,
		long criteriaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException;

	/**
	* Removes all the idea evaluations where criteriaId = &#63; from the database.
	*
	* @param criteriaId the criteria ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeBycriteriaId(long criteriaId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of idea evaluations where criteriaId = &#63;.
	*
	* @param criteriaId the criteria ID
	* @return the number of matching idea evaluations
	* @throws SystemException if a system exception occurred
	*/
	public int countBycriteriaId(long criteriaId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the idea evaluations where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @return the matching idea evaluations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation> findByideaId(
		long ideaId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the idea evaluations where ideaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param start the lower bound of the range of idea evaluations
	* @param end the upper bound of the range of idea evaluations (not inclusive)
	* @return the range of matching idea evaluations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation> findByideaId(
		long ideaId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the idea evaluations where ideaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param start the lower bound of the range of idea evaluations
	* @param end the upper bound of the range of idea evaluations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching idea evaluations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation> findByideaId(
		long ideaId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first idea evaluation in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching idea evaluation
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException if a matching idea evaluation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation findByideaId_First(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException;

	/**
	* Returns the first idea evaluation in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching idea evaluation, or <code>null</code> if a matching idea evaluation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation fetchByideaId_First(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last idea evaluation in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching idea evaluation
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException if a matching idea evaluation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation findByideaId_Last(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException;

	/**
	* Returns the last idea evaluation in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching idea evaluation, or <code>null</code> if a matching idea evaluation could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation fetchByideaId_Last(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the idea evaluations before and after the current idea evaluation in the ordered set where ideaId = &#63;.
	*
	* @param ideaEvaluationPK the primary key of the current idea evaluation
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next idea evaluation
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException if a idea evaluation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation[] findByideaId_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.IdeaEvaluationPK ideaEvaluationPK,
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException;

	/**
	* Removes all the idea evaluations where ideaId = &#63; from the database.
	*
	* @param ideaId the idea ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByideaId(long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of idea evaluations where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @return the number of matching idea evaluations
	* @throws SystemException if a system exception occurred
	*/
	public int countByideaId(long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the idea evaluation in the entity cache if it is enabled.
	*
	* @param ideaEvaluation the idea evaluation
	*/
	public void cacheResult(
		it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation ideaEvaluation);

	/**
	* Caches the idea evaluations in the entity cache if it is enabled.
	*
	* @param ideaEvaluations the idea evaluations
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation> ideaEvaluations);

	/**
	* Creates a new idea evaluation with the primary key. Does not add the idea evaluation to the database.
	*
	* @param ideaEvaluationPK the primary key for the new idea evaluation
	* @return the new idea evaluation
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation create(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.IdeaEvaluationPK ideaEvaluationPK);

	/**
	* Removes the idea evaluation with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ideaEvaluationPK the primary key of the idea evaluation
	* @return the idea evaluation that was removed
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException if a idea evaluation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation remove(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.IdeaEvaluationPK ideaEvaluationPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException;

	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation ideaEvaluation)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the idea evaluation with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException} if it could not be found.
	*
	* @param ideaEvaluationPK the primary key of the idea evaluation
	* @return the idea evaluation
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException if a idea evaluation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation findByPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.IdeaEvaluationPK ideaEvaluationPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchIdeaEvaluationException;

	/**
	* Returns the idea evaluation with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ideaEvaluationPK the primary key of the idea evaluation
	* @return the idea evaluation, or <code>null</code> if a idea evaluation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation fetchByPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.IdeaEvaluationPK ideaEvaluationPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the idea evaluations.
	*
	* @return the idea evaluations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the idea evaluations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of idea evaluations
	* @param end the upper bound of the range of idea evaluations (not inclusive)
	* @return the range of idea evaluations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the idea evaluations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of idea evaluations
	* @param end the upper bound of the range of idea evaluations (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of idea evaluations
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the idea evaluations from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of idea evaluations.
	*
	* @return the number of idea evaluations
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}