/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet;

/**
 * The persistence interface for the c l s categories set service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSCategoriesSetPersistenceImpl
 * @see CLSCategoriesSetUtil
 * @generated
 */
public interface CLSCategoriesSetPersistence extends BasePersistence<CLSCategoriesSet> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CLSCategoriesSetUtil} to access the c l s categories set persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the c l s categories set in the entity cache if it is enabled.
	*
	* @param clsCategoriesSet the c l s categories set
	*/
	public void cacheResult(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet clsCategoriesSet);

	/**
	* Caches the c l s categories sets in the entity cache if it is enabled.
	*
	* @param clsCategoriesSets the c l s categories sets
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet> clsCategoriesSets);

	/**
	* Creates a new c l s categories set with the primary key. Does not add the c l s categories set to the database.
	*
	* @param categoriesSetID the primary key for the new c l s categories set
	* @return the new c l s categories set
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet create(
		long categoriesSetID);

	/**
	* Removes the c l s categories set with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param categoriesSetID the primary key of the c l s categories set
	* @return the c l s categories set that was removed
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetException if a c l s categories set with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet remove(
		long categoriesSetID)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetException;

	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet clsCategoriesSet)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the c l s categories set with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetException} if it could not be found.
	*
	* @param categoriesSetID the primary key of the c l s categories set
	* @return the c l s categories set
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetException if a c l s categories set with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet findByPrimaryKey(
		long categoriesSetID)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetException;

	/**
	* Returns the c l s categories set with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param categoriesSetID the primary key of the c l s categories set
	* @return the c l s categories set, or <code>null</code> if a c l s categories set with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet fetchByPrimaryKey(
		long categoriesSetID)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the c l s categories sets.
	*
	* @return the c l s categories sets
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the c l s categories sets.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCategoriesSetModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s categories sets
	* @param end the upper bound of the range of c l s categories sets (not inclusive)
	* @return the range of c l s categories sets
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the c l s categories sets.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCategoriesSetModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s categories sets
	* @param end the upper bound of the range of c l s categories sets (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of c l s categories sets
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the c l s categories sets from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of c l s categories sets.
	*
	* @return the number of c l s categories sets
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}