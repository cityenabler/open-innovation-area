/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.ideas.challenges.servicelayer.service.CLSCategoriesSetForChallengeLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSCategoriesSetForChallengePK;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 */
public class CLSCategoriesSetForChallengeClp extends BaseModelImpl<CLSCategoriesSetForChallenge>
	implements CLSCategoriesSetForChallenge {
	public CLSCategoriesSetForChallengeClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return CLSCategoriesSetForChallenge.class;
	}

	@Override
	public String getModelClassName() {
		return CLSCategoriesSetForChallenge.class.getName();
	}

	@Override
	public CLSCategoriesSetForChallengePK getPrimaryKey() {
		return new CLSCategoriesSetForChallengePK(_categoriesSetID, _challengeId);
	}

	@Override
	public void setPrimaryKey(CLSCategoriesSetForChallengePK primaryKey) {
		setCategoriesSetID(primaryKey.categoriesSetID);
		setChallengeId(primaryKey.challengeId);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new CLSCategoriesSetForChallengePK(_categoriesSetID, _challengeId);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((CLSCategoriesSetForChallengePK)primaryKeyObj);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("categoriesSetID", getCategoriesSetID());
		attributes.put("challengeId", getChallengeId());
		attributes.put("type", getType());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long categoriesSetID = (Long)attributes.get("categoriesSetID");

		if (categoriesSetID != null) {
			setCategoriesSetID(categoriesSetID);
		}

		Long challengeId = (Long)attributes.get("challengeId");

		if (challengeId != null) {
			setChallengeId(challengeId);
		}

		Integer type = (Integer)attributes.get("type");

		if (type != null) {
			setType(type);
		}
	}

	@Override
	public long getCategoriesSetID() {
		return _categoriesSetID;
	}

	@Override
	public void setCategoriesSetID(long categoriesSetID) {
		_categoriesSetID = categoriesSetID;

		if (_clsCategoriesSetForChallengeRemoteModel != null) {
			try {
				Class<?> clazz = _clsCategoriesSetForChallengeRemoteModel.getClass();

				Method method = clazz.getMethod("setCategoriesSetID", long.class);

				method.invoke(_clsCategoriesSetForChallengeRemoteModel,
					categoriesSetID);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getChallengeId() {
		return _challengeId;
	}

	@Override
	public void setChallengeId(long challengeId) {
		_challengeId = challengeId;

		if (_clsCategoriesSetForChallengeRemoteModel != null) {
			try {
				Class<?> clazz = _clsCategoriesSetForChallengeRemoteModel.getClass();

				Method method = clazz.getMethod("setChallengeId", long.class);

				method.invoke(_clsCategoriesSetForChallengeRemoteModel,
					challengeId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public int getType() {
		return _type;
	}

	@Override
	public void setType(int type) {
		_type = type;

		if (_clsCategoriesSetForChallengeRemoteModel != null) {
			try {
				Class<?> clazz = _clsCategoriesSetForChallengeRemoteModel.getClass();

				Method method = clazz.getMethod("setType", int.class);

				method.invoke(_clsCategoriesSetForChallengeRemoteModel, type);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getCLSCategoriesSetForChallengeRemoteModel() {
		return _clsCategoriesSetForChallengeRemoteModel;
	}

	public void setCLSCategoriesSetForChallengeRemoteModel(
		BaseModel<?> clsCategoriesSetForChallengeRemoteModel) {
		_clsCategoriesSetForChallengeRemoteModel = clsCategoriesSetForChallengeRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _clsCategoriesSetForChallengeRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_clsCategoriesSetForChallengeRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			CLSCategoriesSetForChallengeLocalServiceUtil.addCLSCategoriesSetForChallenge(this);
		}
		else {
			CLSCategoriesSetForChallengeLocalServiceUtil.updateCLSCategoriesSetForChallenge(this);
		}
	}

	@Override
	public CLSCategoriesSetForChallenge toEscapedModel() {
		return (CLSCategoriesSetForChallenge)ProxyUtil.newProxyInstance(CLSCategoriesSetForChallenge.class.getClassLoader(),
			new Class[] { CLSCategoriesSetForChallenge.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		CLSCategoriesSetForChallengeClp clone = new CLSCategoriesSetForChallengeClp();

		clone.setCategoriesSetID(getCategoriesSetID());
		clone.setChallengeId(getChallengeId());
		clone.setType(getType());

		return clone;
	}

	@Override
	public int compareTo(
		CLSCategoriesSetForChallenge clsCategoriesSetForChallenge) {
		CLSCategoriesSetForChallengePK primaryKey = clsCategoriesSetForChallenge.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSCategoriesSetForChallengeClp)) {
			return false;
		}

		CLSCategoriesSetForChallengeClp clsCategoriesSetForChallenge = (CLSCategoriesSetForChallengeClp)obj;

		CLSCategoriesSetForChallengePK primaryKey = clsCategoriesSetForChallenge.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{categoriesSetID=");
		sb.append(getCategoriesSetID());
		sb.append(", challengeId=");
		sb.append(getChallengeId());
		sb.append(", type=");
		sb.append(getType());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(13);

		sb.append("<model><model-name>");
		sb.append(
			"it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>categoriesSetID</column-name><column-value><![CDATA[");
		sb.append(getCategoriesSetID());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>challengeId</column-name><column-value><![CDATA[");
		sb.append(getChallengeId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>type</column-name><column-value><![CDATA[");
		sb.append(getType());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _categoriesSetID;
	private long _challengeId;
	private int _type;
	private BaseModel<?> _clsCategoriesSetForChallengeRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.class;
}