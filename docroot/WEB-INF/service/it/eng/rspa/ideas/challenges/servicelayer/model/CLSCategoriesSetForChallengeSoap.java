/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSCategoriesSetForChallengePK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.eng.rspa.ideas.challenges.servicelayer.service.http.CLSCategoriesSetForChallengeServiceSoap}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.http.CLSCategoriesSetForChallengeServiceSoap
 * @generated
 */
public class CLSCategoriesSetForChallengeSoap implements Serializable {
	public static CLSCategoriesSetForChallengeSoap toSoapModel(
		CLSCategoriesSetForChallenge model) {
		CLSCategoriesSetForChallengeSoap soapModel = new CLSCategoriesSetForChallengeSoap();

		soapModel.setCategoriesSetID(model.getCategoriesSetID());
		soapModel.setChallengeId(model.getChallengeId());
		soapModel.setType(model.getType());

		return soapModel;
	}

	public static CLSCategoriesSetForChallengeSoap[] toSoapModels(
		CLSCategoriesSetForChallenge[] models) {
		CLSCategoriesSetForChallengeSoap[] soapModels = new CLSCategoriesSetForChallengeSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CLSCategoriesSetForChallengeSoap[][] toSoapModels(
		CLSCategoriesSetForChallenge[][] models) {
		CLSCategoriesSetForChallengeSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CLSCategoriesSetForChallengeSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CLSCategoriesSetForChallengeSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CLSCategoriesSetForChallengeSoap[] toSoapModels(
		List<CLSCategoriesSetForChallenge> models) {
		List<CLSCategoriesSetForChallengeSoap> soapModels = new ArrayList<CLSCategoriesSetForChallengeSoap>(models.size());

		for (CLSCategoriesSetForChallenge model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CLSCategoriesSetForChallengeSoap[soapModels.size()]);
	}

	public CLSCategoriesSetForChallengeSoap() {
	}

	public CLSCategoriesSetForChallengePK getPrimaryKey() {
		return new CLSCategoriesSetForChallengePK(_categoriesSetID, _challengeId);
	}

	public void setPrimaryKey(CLSCategoriesSetForChallengePK pk) {
		setCategoriesSetID(pk.categoriesSetID);
		setChallengeId(pk.challengeId);
	}

	public long getCategoriesSetID() {
		return _categoriesSetID;
	}

	public void setCategoriesSetID(long categoriesSetID) {
		_categoriesSetID = categoriesSetID;
	}

	public long getChallengeId() {
		return _challengeId;
	}

	public void setChallengeId(long challengeId) {
		_challengeId = challengeId;
	}

	public int getType() {
		return _type;
	}

	public void setType(int type) {
		_type = type;
	}

	private long _categoriesSetID;
	private long _challengeId;
	private int _type;
}