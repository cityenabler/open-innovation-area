/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.ideas.challenges.servicelayer.service.CLSMapPropertiesLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 */
public class CLSMapPropertiesClp extends BaseModelImpl<CLSMapProperties>
	implements CLSMapProperties {
	public CLSMapPropertiesClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return CLSMapProperties.class;
	}

	@Override
	public String getModelClassName() {
		return CLSMapProperties.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _mapPropertiesId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setMapPropertiesId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _mapPropertiesId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("mapPropertiesId", getMapPropertiesId());
		attributes.put("mapCenterLatitude", getMapCenterLatitude());
		attributes.put("mapCenterLongitude", getMapCenterLongitude());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long mapPropertiesId = (Long)attributes.get("mapPropertiesId");

		if (mapPropertiesId != null) {
			setMapPropertiesId(mapPropertiesId);
		}

		String mapCenterLatitude = (String)attributes.get("mapCenterLatitude");

		if (mapCenterLatitude != null) {
			setMapCenterLatitude(mapCenterLatitude);
		}

		String mapCenterLongitude = (String)attributes.get("mapCenterLongitude");

		if (mapCenterLongitude != null) {
			setMapCenterLongitude(mapCenterLongitude);
		}
	}

	@Override
	public long getMapPropertiesId() {
		return _mapPropertiesId;
	}

	@Override
	public void setMapPropertiesId(long mapPropertiesId) {
		_mapPropertiesId = mapPropertiesId;

		if (_clsMapPropertiesRemoteModel != null) {
			try {
				Class<?> clazz = _clsMapPropertiesRemoteModel.getClass();

				Method method = clazz.getMethod("setMapPropertiesId", long.class);

				method.invoke(_clsMapPropertiesRemoteModel, mapPropertiesId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getMapCenterLatitude() {
		return _mapCenterLatitude;
	}

	@Override
	public void setMapCenterLatitude(String mapCenterLatitude) {
		_mapCenterLatitude = mapCenterLatitude;

		if (_clsMapPropertiesRemoteModel != null) {
			try {
				Class<?> clazz = _clsMapPropertiesRemoteModel.getClass();

				Method method = clazz.getMethod("setMapCenterLatitude",
						String.class);

				method.invoke(_clsMapPropertiesRemoteModel, mapCenterLatitude);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getMapCenterLongitude() {
		return _mapCenterLongitude;
	}

	@Override
	public void setMapCenterLongitude(String mapCenterLongitude) {
		_mapCenterLongitude = mapCenterLongitude;

		if (_clsMapPropertiesRemoteModel != null) {
			try {
				Class<?> clazz = _clsMapPropertiesRemoteModel.getClass();

				Method method = clazz.getMethod("setMapCenterLongitude",
						String.class);

				method.invoke(_clsMapPropertiesRemoteModel, mapCenterLongitude);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getCLSMapPropertiesRemoteModel() {
		return _clsMapPropertiesRemoteModel;
	}

	public void setCLSMapPropertiesRemoteModel(
		BaseModel<?> clsMapPropertiesRemoteModel) {
		_clsMapPropertiesRemoteModel = clsMapPropertiesRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _clsMapPropertiesRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_clsMapPropertiesRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			CLSMapPropertiesLocalServiceUtil.addCLSMapProperties(this);
		}
		else {
			CLSMapPropertiesLocalServiceUtil.updateCLSMapProperties(this);
		}
	}

	@Override
	public CLSMapProperties toEscapedModel() {
		return (CLSMapProperties)ProxyUtil.newProxyInstance(CLSMapProperties.class.getClassLoader(),
			new Class[] { CLSMapProperties.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		CLSMapPropertiesClp clone = new CLSMapPropertiesClp();

		clone.setMapPropertiesId(getMapPropertiesId());
		clone.setMapCenterLatitude(getMapCenterLatitude());
		clone.setMapCenterLongitude(getMapCenterLongitude());

		return clone;
	}

	@Override
	public int compareTo(CLSMapProperties clsMapProperties) {
		long primaryKey = clsMapProperties.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSMapPropertiesClp)) {
			return false;
		}

		CLSMapPropertiesClp clsMapProperties = (CLSMapPropertiesClp)obj;

		long primaryKey = clsMapProperties.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{mapPropertiesId=");
		sb.append(getMapPropertiesId());
		sb.append(", mapCenterLatitude=");
		sb.append(getMapCenterLatitude());
		sb.append(", mapCenterLongitude=");
		sb.append(getMapCenterLongitude());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(13);

		sb.append("<model><model-name>");
		sb.append(
			"it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>mapPropertiesId</column-name><column-value><![CDATA[");
		sb.append(getMapPropertiesId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>mapCenterLatitude</column-name><column-value><![CDATA[");
		sb.append(getMapCenterLatitude());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>mapCenterLongitude</column-name><column-value><![CDATA[");
		sb.append(getMapCenterLongitude());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _mapPropertiesId;
	private String _mapCenterLatitude;
	private String _mapCenterLongitude;
	private BaseModel<?> _clsMapPropertiesRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.class;
}