/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.IdeaEvaluationPK;

import java.io.Serializable;

import java.util.Date;

/**
 * The base model interface for the IdeaEvaluation service. Represents a row in the &quot;CSL_IdeaEvaluation&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationImpl}.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see IdeaEvaluation
 * @see it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationModelImpl
 * @generated
 */
public interface IdeaEvaluationModel extends BaseModel<IdeaEvaluation> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a idea evaluation model instance should use the {@link IdeaEvaluation} interface instead.
	 */

	/**
	 * Returns the primary key of this idea evaluation.
	 *
	 * @return the primary key of this idea evaluation
	 */
	public IdeaEvaluationPK getPrimaryKey();

	/**
	 * Sets the primary key of this idea evaluation.
	 *
	 * @param primaryKey the primary key of this idea evaluation
	 */
	public void setPrimaryKey(IdeaEvaluationPK primaryKey);

	/**
	 * Returns the criteria ID of this idea evaluation.
	 *
	 * @return the criteria ID of this idea evaluation
	 */
	public long getCriteriaId();

	/**
	 * Sets the criteria ID of this idea evaluation.
	 *
	 * @param criteriaId the criteria ID of this idea evaluation
	 */
	public void setCriteriaId(long criteriaId);

	/**
	 * Returns the idea ID of this idea evaluation.
	 *
	 * @return the idea ID of this idea evaluation
	 */
	public long getIdeaId();

	/**
	 * Sets the idea ID of this idea evaluation.
	 *
	 * @param ideaId the idea ID of this idea evaluation
	 */
	public void setIdeaId(long ideaId);

	/**
	 * Returns the motivation of this idea evaluation.
	 *
	 * @return the motivation of this idea evaluation
	 */
	@AutoEscape
	public String getMotivation();

	/**
	 * Sets the motivation of this idea evaluation.
	 *
	 * @param motivation the motivation of this idea evaluation
	 */
	public void setMotivation(String motivation);

	/**
	 * Returns the passed of this idea evaluation.
	 *
	 * @return the passed of this idea evaluation
	 */
	public boolean getPassed();

	/**
	 * Returns <code>true</code> if this idea evaluation is passed.
	 *
	 * @return <code>true</code> if this idea evaluation is passed; <code>false</code> otherwise
	 */
	public boolean isPassed();

	/**
	 * Sets whether this idea evaluation is passed.
	 *
	 * @param passed the passed of this idea evaluation
	 */
	public void setPassed(boolean passed);

	/**
	 * Returns the score of this idea evaluation.
	 *
	 * @return the score of this idea evaluation
	 */
	public double getScore();

	/**
	 * Sets the score of this idea evaluation.
	 *
	 * @param score the score of this idea evaluation
	 */
	public void setScore(double score);

	/**
	 * Returns the date of this idea evaluation.
	 *
	 * @return the date of this idea evaluation
	 */
	public Date getDate();

	/**
	 * Sets the date of this idea evaluation.
	 *
	 * @param date the date of this idea evaluation
	 */
	public void setDate(Date date);

	/**
	 * Returns the author ID of this idea evaluation.
	 *
	 * @return the author ID of this idea evaluation
	 */
	public long getAuthorId();

	/**
	 * Sets the author ID of this idea evaluation.
	 *
	 * @param authorId the author ID of this idea evaluation
	 */
	public void setAuthorId(long authorId);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(
		it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation ideaEvaluation);

	@Override
	public int hashCode();

	@Override
	public CacheModel<it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation> toCacheModel();

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation toEscapedModel();

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}