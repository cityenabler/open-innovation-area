/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link IdeaEvaluationLocalService}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see IdeaEvaluationLocalService
 * @generated
 */
public class IdeaEvaluationLocalServiceWrapper
	implements IdeaEvaluationLocalService,
		ServiceWrapper<IdeaEvaluationLocalService> {
	public IdeaEvaluationLocalServiceWrapper(
		IdeaEvaluationLocalService ideaEvaluationLocalService) {
		_ideaEvaluationLocalService = ideaEvaluationLocalService;
	}

	/**
	* Adds the idea evaluation to the database. Also notifies the appropriate model listeners.
	*
	* @param ideaEvaluation the idea evaluation
	* @return the idea evaluation that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation addIdeaEvaluation(
		it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation ideaEvaluation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ideaEvaluationLocalService.addIdeaEvaluation(ideaEvaluation);
	}

	/**
	* Creates a new idea evaluation with the primary key. Does not add the idea evaluation to the database.
	*
	* @param ideaEvaluationPK the primary key for the new idea evaluation
	* @return the new idea evaluation
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation createIdeaEvaluation(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.IdeaEvaluationPK ideaEvaluationPK) {
		return _ideaEvaluationLocalService.createIdeaEvaluation(ideaEvaluationPK);
	}

	/**
	* Deletes the idea evaluation with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ideaEvaluationPK the primary key of the idea evaluation
	* @return the idea evaluation that was removed
	* @throws PortalException if a idea evaluation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation deleteIdeaEvaluation(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.IdeaEvaluationPK ideaEvaluationPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _ideaEvaluationLocalService.deleteIdeaEvaluation(ideaEvaluationPK);
	}

	/**
	* Deletes the idea evaluation from the database. Also notifies the appropriate model listeners.
	*
	* @param ideaEvaluation the idea evaluation
	* @return the idea evaluation that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation deleteIdeaEvaluation(
		it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation ideaEvaluation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ideaEvaluationLocalService.deleteIdeaEvaluation(ideaEvaluation);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _ideaEvaluationLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ideaEvaluationLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _ideaEvaluationLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ideaEvaluationLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ideaEvaluationLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ideaEvaluationLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation fetchIdeaEvaluation(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.IdeaEvaluationPK ideaEvaluationPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ideaEvaluationLocalService.fetchIdeaEvaluation(ideaEvaluationPK);
	}

	/**
	* Returns the idea evaluation with the primary key.
	*
	* @param ideaEvaluationPK the primary key of the idea evaluation
	* @return the idea evaluation
	* @throws PortalException if a idea evaluation with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation getIdeaEvaluation(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.IdeaEvaluationPK ideaEvaluationPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _ideaEvaluationLocalService.getIdeaEvaluation(ideaEvaluationPK);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _ideaEvaluationLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the idea evaluations.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.IdeaEvaluationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of idea evaluations
	* @param end the upper bound of the range of idea evaluations (not inclusive)
	* @return the range of idea evaluations
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation> getIdeaEvaluations(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ideaEvaluationLocalService.getIdeaEvaluations(start, end);
	}

	/**
	* Returns the number of idea evaluations.
	*
	* @return the number of idea evaluations
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getIdeaEvaluationsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ideaEvaluationLocalService.getIdeaEvaluationsCount();
	}

	/**
	* Updates the idea evaluation in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param ideaEvaluation the idea evaluation
	* @return the idea evaluation that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation updateIdeaEvaluation(
		it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation ideaEvaluation)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _ideaEvaluationLocalService.updateIdeaEvaluation(ideaEvaluation);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _ideaEvaluationLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_ideaEvaluationLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _ideaEvaluationLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	/**
	* @param ideaId
	* @return
	*/
	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation> getIdeaEvaluationsByIdeaId(
		long ideaId) {
		return _ideaEvaluationLocalService.getIdeaEvaluationsByIdeaId(ideaId);
	}

	/**
	* @param criteriaId
	* @param ideaId
	* @return
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation getIdeaEvaluationByCriteriaIdAndIdeaId(
		long criteriaId, long ideaId) {
		return _ideaEvaluationLocalService.getIdeaEvaluationByCriteriaIdAndIdeaId(criteriaId,
			ideaId);
	}

	/**
	* @param criteriaId
	* @return
	*/
	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation> getIdeaEvaluationsByCriteriaId(
		long criteriaId) {
		return _ideaEvaluationLocalService.getIdeaEvaluationsByCriteriaId(criteriaId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public IdeaEvaluationLocalService getWrappedIdeaEvaluationLocalService() {
		return _ideaEvaluationLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedIdeaEvaluationLocalService(
		IdeaEvaluationLocalService ideaEvaluationLocalService) {
		_ideaEvaluationLocalService = ideaEvaluationLocalService;
	}

	@Override
	public IdeaEvaluationLocalService getWrappedService() {
		return _ideaEvaluationLocalService;
	}

	@Override
	public void setWrappedService(
		IdeaEvaluationLocalService ideaEvaluationLocalService) {
		_ideaEvaluationLocalService = ideaEvaluationLocalService;
	}

	private IdeaEvaluationLocalService _ideaEvaluationLocalService;
}