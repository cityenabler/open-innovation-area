/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import java.io.Serializable;

/**
 * The base model interface for the CLSRequisiti service. Represents a row in the &quot;CSL_CLSRequisiti&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSRequisitiModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSRequisitiImpl}.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSRequisiti
 * @see it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSRequisitiImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSRequisitiModelImpl
 * @generated
 */
public interface CLSRequisitiModel extends BaseModel<CLSRequisiti> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a c l s requisiti model instance should use the {@link CLSRequisiti} interface instead.
	 */

	/**
	 * Returns the primary key of this c l s requisiti.
	 *
	 * @return the primary key of this c l s requisiti
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this c l s requisiti.
	 *
	 * @param primaryKey the primary key of this c l s requisiti
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the requisito ID of this c l s requisiti.
	 *
	 * @return the requisito ID of this c l s requisiti
	 */
	public long getRequisitoId();

	/**
	 * Sets the requisito ID of this c l s requisiti.
	 *
	 * @param requisitoId the requisito ID of this c l s requisiti
	 */
	public void setRequisitoId(long requisitoId);

	/**
	 * Returns the idea ID of this c l s requisiti.
	 *
	 * @return the idea ID of this c l s requisiti
	 */
	public long getIdeaId();

	/**
	 * Sets the idea ID of this c l s requisiti.
	 *
	 * @param ideaId the idea ID of this c l s requisiti
	 */
	public void setIdeaId(long ideaId);

	/**
	 * Returns the task ID of this c l s requisiti.
	 *
	 * @return the task ID of this c l s requisiti
	 */
	public long getTaskId();

	/**
	 * Sets the task ID of this c l s requisiti.
	 *
	 * @param taskId the task ID of this c l s requisiti
	 */
	public void setTaskId(long taskId);

	/**
	 * Returns the tipo of this c l s requisiti.
	 *
	 * @return the tipo of this c l s requisiti
	 */
	@AutoEscape
	public String getTipo();

	/**
	 * Sets the tipo of this c l s requisiti.
	 *
	 * @param tipo the tipo of this c l s requisiti
	 */
	public void setTipo(String tipo);

	/**
	 * Returns the descrizione of this c l s requisiti.
	 *
	 * @return the descrizione of this c l s requisiti
	 */
	@AutoEscape
	public String getDescrizione();

	/**
	 * Sets the descrizione of this c l s requisiti.
	 *
	 * @param descrizione the descrizione of this c l s requisiti
	 */
	public void setDescrizione(String descrizione);

	/**
	 * Returns the help of this c l s requisiti.
	 *
	 * @return the help of this c l s requisiti
	 */
	@AutoEscape
	public String getHelp();

	/**
	 * Sets the help of this c l s requisiti.
	 *
	 * @param help the help of this c l s requisiti
	 */
	public void setHelp(String help);

	/**
	 * Returns the author user of this c l s requisiti.
	 *
	 * @return the author user of this c l s requisiti
	 */
	public long getAuthorUser();

	/**
	 * Sets the author user of this c l s requisiti.
	 *
	 * @param authorUser the author user of this c l s requisiti
	 */
	public void setAuthorUser(long authorUser);

	/**
	 * Returns the category of this c l s requisiti.
	 *
	 * @return the category of this c l s requisiti
	 */
	@AutoEscape
	public String getCategory();

	/**
	 * Sets the category of this c l s requisiti.
	 *
	 * @param category the category of this c l s requisiti
	 */
	public void setCategory(String category);

	/**
	 * Returns the multi task of this c l s requisiti.
	 *
	 * @return the multi task of this c l s requisiti
	 */
	public boolean getMultiTask();

	/**
	 * Returns <code>true</code> if this c l s requisiti is multi task.
	 *
	 * @return <code>true</code> if this c l s requisiti is multi task; <code>false</code> otherwise
	 */
	public boolean isMultiTask();

	/**
	 * Sets whether this c l s requisiti is multi task.
	 *
	 * @param multiTask the multi task of this c l s requisiti
	 */
	public void setMultiTask(boolean multiTask);

	/**
	 * Returns the outcome file of this c l s requisiti.
	 *
	 * @return the outcome file of this c l s requisiti
	 */
	public boolean getOutcomeFile();

	/**
	 * Returns <code>true</code> if this c l s requisiti is outcome file.
	 *
	 * @return <code>true</code> if this c l s requisiti is outcome file; <code>false</code> otherwise
	 */
	public boolean isOutcomeFile();

	/**
	 * Sets whether this c l s requisiti is outcome file.
	 *
	 * @param outcomeFile the outcome file of this c l s requisiti
	 */
	public void setOutcomeFile(boolean outcomeFile);

	/**
	 * Returns the task double field of this c l s requisiti.
	 *
	 * @return the task double field of this c l s requisiti
	 */
	public boolean getTaskDoubleField();

	/**
	 * Returns <code>true</code> if this c l s requisiti is task double field.
	 *
	 * @return <code>true</code> if this c l s requisiti is task double field; <code>false</code> otherwise
	 */
	public boolean isTaskDoubleField();

	/**
	 * Sets whether this c l s requisiti is task double field.
	 *
	 * @param taskDoubleField the task double field of this c l s requisiti
	 */
	public void setTaskDoubleField(boolean taskDoubleField);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti clsRequisiti);

	@Override
	public int hashCode();

	@Override
	public CacheModel<it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti> toCacheModel();

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti toEscapedModel();

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}