/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.InvitedMailPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.eng.rspa.ideas.challenges.servicelayer.service.http.InvitedMailServiceSoap}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.http.InvitedMailServiceSoap
 * @generated
 */
public class InvitedMailSoap implements Serializable {
	public static InvitedMailSoap toSoapModel(InvitedMail model) {
		InvitedMailSoap soapModel = new InvitedMailSoap();

		soapModel.setIdeaId(model.getIdeaId());
		soapModel.setMail(model.getMail());

		return soapModel;
	}

	public static InvitedMailSoap[] toSoapModels(InvitedMail[] models) {
		InvitedMailSoap[] soapModels = new InvitedMailSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static InvitedMailSoap[][] toSoapModels(InvitedMail[][] models) {
		InvitedMailSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new InvitedMailSoap[models.length][models[0].length];
		}
		else {
			soapModels = new InvitedMailSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static InvitedMailSoap[] toSoapModels(List<InvitedMail> models) {
		List<InvitedMailSoap> soapModels = new ArrayList<InvitedMailSoap>(models.size());

		for (InvitedMail model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new InvitedMailSoap[soapModels.size()]);
	}

	public InvitedMailSoap() {
	}

	public InvitedMailPK getPrimaryKey() {
		return new InvitedMailPK(_ideaId, _mail);
	}

	public void setPrimaryKey(InvitedMailPK pk) {
		setIdeaId(pk.ideaId);
		setMail(pk.mail);
	}

	public long getIdeaId() {
		return _ideaId;
	}

	public void setIdeaId(long ideaId) {
		_ideaId = ideaId;
	}

	public String getMail() {
		return _mail;
	}

	public void setMail(String mail) {
		_mail = mail;
	}

	private long _ideaId;
	private String _mail;
}