/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CLSChallengesCalendar}.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSChallengesCalendar
 * @generated
 */
public class CLSChallengesCalendarWrapper implements CLSChallengesCalendar,
	ModelWrapper<CLSChallengesCalendar> {
	public CLSChallengesCalendarWrapper(
		CLSChallengesCalendar clsChallengesCalendar) {
		_clsChallengesCalendar = clsChallengesCalendar;
	}

	@Override
	public Class<?> getModelClass() {
		return CLSChallengesCalendar.class;
	}

	@Override
	public String getModelClassName() {
		return CLSChallengesCalendar.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("challengesCalendarId", getChallengesCalendarId());
		attributes.put("referenceCalendarId", getReferenceCalendarId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long challengesCalendarId = (Long)attributes.get("challengesCalendarId");

		if (challengesCalendarId != null) {
			setChallengesCalendarId(challengesCalendarId);
		}

		Long referenceCalendarId = (Long)attributes.get("referenceCalendarId");

		if (referenceCalendarId != null) {
			setReferenceCalendarId(referenceCalendarId);
		}
	}

	/**
	* Returns the primary key of this c l s challenges calendar.
	*
	* @return the primary key of this c l s challenges calendar
	*/
	@Override
	public long getPrimaryKey() {
		return _clsChallengesCalendar.getPrimaryKey();
	}

	/**
	* Sets the primary key of this c l s challenges calendar.
	*
	* @param primaryKey the primary key of this c l s challenges calendar
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_clsChallengesCalendar.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the challenges calendar ID of this c l s challenges calendar.
	*
	* @return the challenges calendar ID of this c l s challenges calendar
	*/
	@Override
	public long getChallengesCalendarId() {
		return _clsChallengesCalendar.getChallengesCalendarId();
	}

	/**
	* Sets the challenges calendar ID of this c l s challenges calendar.
	*
	* @param challengesCalendarId the challenges calendar ID of this c l s challenges calendar
	*/
	@Override
	public void setChallengesCalendarId(long challengesCalendarId) {
		_clsChallengesCalendar.setChallengesCalendarId(challengesCalendarId);
	}

	/**
	* Returns the reference calendar ID of this c l s challenges calendar.
	*
	* @return the reference calendar ID of this c l s challenges calendar
	*/
	@Override
	public long getReferenceCalendarId() {
		return _clsChallengesCalendar.getReferenceCalendarId();
	}

	/**
	* Sets the reference calendar ID of this c l s challenges calendar.
	*
	* @param referenceCalendarId the reference calendar ID of this c l s challenges calendar
	*/
	@Override
	public void setReferenceCalendarId(long referenceCalendarId) {
		_clsChallengesCalendar.setReferenceCalendarId(referenceCalendarId);
	}

	@Override
	public boolean isNew() {
		return _clsChallengesCalendar.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_clsChallengesCalendar.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _clsChallengesCalendar.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_clsChallengesCalendar.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _clsChallengesCalendar.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _clsChallengesCalendar.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_clsChallengesCalendar.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _clsChallengesCalendar.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_clsChallengesCalendar.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_clsChallengesCalendar.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_clsChallengesCalendar.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CLSChallengesCalendarWrapper((CLSChallengesCalendar)_clsChallengesCalendar.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengesCalendar clsChallengesCalendar) {
		return _clsChallengesCalendar.compareTo(clsChallengesCalendar);
	}

	@Override
	public int hashCode() {
		return _clsChallengesCalendar.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengesCalendar> toCacheModel() {
		return _clsChallengesCalendar.toCacheModel();
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengesCalendar toEscapedModel() {
		return new CLSChallengesCalendarWrapper(_clsChallengesCalendar.toEscapedModel());
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengesCalendar toUnescapedModel() {
		return new CLSChallengesCalendarWrapper(_clsChallengesCalendar.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _clsChallengesCalendar.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _clsChallengesCalendar.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_clsChallengesCalendar.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSChallengesCalendarWrapper)) {
			return false;
		}

		CLSChallengesCalendarWrapper clsChallengesCalendarWrapper = (CLSChallengesCalendarWrapper)obj;

		if (Validator.equals(_clsChallengesCalendar,
					clsChallengesCalendarWrapper._clsChallengesCalendar)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public CLSChallengesCalendar getWrappedCLSChallengesCalendar() {
		return _clsChallengesCalendar;
	}

	@Override
	public CLSChallengesCalendar getWrappedModel() {
		return _clsChallengesCalendar;
	}

	@Override
	public void resetOriginalValues() {
		_clsChallengesCalendar.resetOriginalValues();
	}

	private CLSChallengesCalendar _clsChallengesCalendar;
}