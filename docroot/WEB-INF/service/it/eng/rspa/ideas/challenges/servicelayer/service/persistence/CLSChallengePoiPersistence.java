/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi;

/**
 * The persistence interface for the c l s challenge poi service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSChallengePoiPersistenceImpl
 * @see CLSChallengePoiUtil
 * @generated
 */
public interface CLSChallengePoiPersistence extends BasePersistence<CLSChallengePoi> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CLSChallengePoiUtil} to access the c l s challenge poi persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the c l s challenge pois where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @return the matching c l s challenge pois
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi> findByChallengeId(
		long challengeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the c l s challenge pois where challengeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengePoiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param challengeId the challenge ID
	* @param start the lower bound of the range of c l s challenge pois
	* @param end the upper bound of the range of c l s challenge pois (not inclusive)
	* @return the range of matching c l s challenge pois
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi> findByChallengeId(
		long challengeId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the c l s challenge pois where challengeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengePoiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param challengeId the challenge ID
	* @param start the lower bound of the range of c l s challenge pois
	* @param end the upper bound of the range of c l s challenge pois (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching c l s challenge pois
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi> findByChallengeId(
		long challengeId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first c l s challenge poi in the ordered set where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s challenge poi
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengePoiException if a matching c l s challenge poi could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi findByChallengeId_First(
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengePoiException;

	/**
	* Returns the first c l s challenge poi in the ordered set where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s challenge poi, or <code>null</code> if a matching c l s challenge poi could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi fetchByChallengeId_First(
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last c l s challenge poi in the ordered set where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s challenge poi
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengePoiException if a matching c l s challenge poi could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi findByChallengeId_Last(
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengePoiException;

	/**
	* Returns the last c l s challenge poi in the ordered set where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s challenge poi, or <code>null</code> if a matching c l s challenge poi could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi fetchByChallengeId_Last(
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the c l s challenge pois before and after the current c l s challenge poi in the ordered set where challengeId = &#63;.
	*
	* @param poiId the primary key of the current c l s challenge poi
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next c l s challenge poi
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengePoiException if a c l s challenge poi with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi[] findByChallengeId_PrevAndNext(
		long poiId, long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengePoiException;

	/**
	* Removes all the c l s challenge pois where challengeId = &#63; from the database.
	*
	* @param challengeId the challenge ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByChallengeId(long challengeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of c l s challenge pois where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @return the number of matching c l s challenge pois
	* @throws SystemException if a system exception occurred
	*/
	public int countByChallengeId(long challengeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the c l s challenge poi in the entity cache if it is enabled.
	*
	* @param clsChallengePoi the c l s challenge poi
	*/
	public void cacheResult(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi clsChallengePoi);

	/**
	* Caches the c l s challenge pois in the entity cache if it is enabled.
	*
	* @param clsChallengePois the c l s challenge pois
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi> clsChallengePois);

	/**
	* Creates a new c l s challenge poi with the primary key. Does not add the c l s challenge poi to the database.
	*
	* @param poiId the primary key for the new c l s challenge poi
	* @return the new c l s challenge poi
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi create(
		long poiId);

	/**
	* Removes the c l s challenge poi with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param poiId the primary key of the c l s challenge poi
	* @return the c l s challenge poi that was removed
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengePoiException if a c l s challenge poi with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi remove(
		long poiId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengePoiException;

	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi clsChallengePoi)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the c l s challenge poi with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengePoiException} if it could not be found.
	*
	* @param poiId the primary key of the c l s challenge poi
	* @return the c l s challenge poi
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengePoiException if a c l s challenge poi with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi findByPrimaryKey(
		long poiId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengePoiException;

	/**
	* Returns the c l s challenge poi with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param poiId the primary key of the c l s challenge poi
	* @return the c l s challenge poi, or <code>null</code> if a c l s challenge poi with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi fetchByPrimaryKey(
		long poiId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the c l s challenge pois.
	*
	* @return the c l s challenge pois
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the c l s challenge pois.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengePoiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s challenge pois
	* @param end the upper bound of the range of c l s challenge pois (not inclusive)
	* @return the range of c l s challenge pois
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the c l s challenge pois.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengePoiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s challenge pois
	* @param end the upper bound of the range of c l s challenge pois (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of c l s challenge pois
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the c l s challenge pois from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of c l s challenge pois.
	*
	* @return the number of c l s challenge pois
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}