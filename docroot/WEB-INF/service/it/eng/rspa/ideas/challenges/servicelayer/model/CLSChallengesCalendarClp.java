/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengesCalendarLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 */
public class CLSChallengesCalendarClp extends BaseModelImpl<CLSChallengesCalendar>
	implements CLSChallengesCalendar {
	public CLSChallengesCalendarClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return CLSChallengesCalendar.class;
	}

	@Override
	public String getModelClassName() {
		return CLSChallengesCalendar.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _challengesCalendarId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setChallengesCalendarId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _challengesCalendarId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("challengesCalendarId", getChallengesCalendarId());
		attributes.put("referenceCalendarId", getReferenceCalendarId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long challengesCalendarId = (Long)attributes.get("challengesCalendarId");

		if (challengesCalendarId != null) {
			setChallengesCalendarId(challengesCalendarId);
		}

		Long referenceCalendarId = (Long)attributes.get("referenceCalendarId");

		if (referenceCalendarId != null) {
			setReferenceCalendarId(referenceCalendarId);
		}
	}

	@Override
	public long getChallengesCalendarId() {
		return _challengesCalendarId;
	}

	@Override
	public void setChallengesCalendarId(long challengesCalendarId) {
		_challengesCalendarId = challengesCalendarId;

		if (_clsChallengesCalendarRemoteModel != null) {
			try {
				Class<?> clazz = _clsChallengesCalendarRemoteModel.getClass();

				Method method = clazz.getMethod("setChallengesCalendarId",
						long.class);

				method.invoke(_clsChallengesCalendarRemoteModel,
					challengesCalendarId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getReferenceCalendarId() {
		return _referenceCalendarId;
	}

	@Override
	public void setReferenceCalendarId(long referenceCalendarId) {
		_referenceCalendarId = referenceCalendarId;

		if (_clsChallengesCalendarRemoteModel != null) {
			try {
				Class<?> clazz = _clsChallengesCalendarRemoteModel.getClass();

				Method method = clazz.getMethod("setReferenceCalendarId",
						long.class);

				method.invoke(_clsChallengesCalendarRemoteModel,
					referenceCalendarId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getCLSChallengesCalendarRemoteModel() {
		return _clsChallengesCalendarRemoteModel;
	}

	public void setCLSChallengesCalendarRemoteModel(
		BaseModel<?> clsChallengesCalendarRemoteModel) {
		_clsChallengesCalendarRemoteModel = clsChallengesCalendarRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _clsChallengesCalendarRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_clsChallengesCalendarRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			CLSChallengesCalendarLocalServiceUtil.addCLSChallengesCalendar(this);
		}
		else {
			CLSChallengesCalendarLocalServiceUtil.updateCLSChallengesCalendar(this);
		}
	}

	@Override
	public CLSChallengesCalendar toEscapedModel() {
		return (CLSChallengesCalendar)ProxyUtil.newProxyInstance(CLSChallengesCalendar.class.getClassLoader(),
			new Class[] { CLSChallengesCalendar.class },
			new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		CLSChallengesCalendarClp clone = new CLSChallengesCalendarClp();

		clone.setChallengesCalendarId(getChallengesCalendarId());
		clone.setReferenceCalendarId(getReferenceCalendarId());

		return clone;
	}

	@Override
	public int compareTo(CLSChallengesCalendar clsChallengesCalendar) {
		long primaryKey = clsChallengesCalendar.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSChallengesCalendarClp)) {
			return false;
		}

		CLSChallengesCalendarClp clsChallengesCalendar = (CLSChallengesCalendarClp)obj;

		long primaryKey = clsChallengesCalendar.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{challengesCalendarId=");
		sb.append(getChallengesCalendarId());
		sb.append(", referenceCalendarId=");
		sb.append(getReferenceCalendarId());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(10);

		sb.append("<model><model-name>");
		sb.append(
			"it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengesCalendar");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>challengesCalendarId</column-name><column-value><![CDATA[");
		sb.append(getChallengesCalendarId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>referenceCalendarId</column-name><column-value><![CDATA[");
		sb.append(getReferenceCalendarId());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _challengesCalendarId;
	private long _referenceCalendarId;
	private BaseModel<?> _clsChallengesCalendarRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.class;
}