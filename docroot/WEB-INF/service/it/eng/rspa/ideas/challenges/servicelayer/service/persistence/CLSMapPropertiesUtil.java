/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties;

import java.util.List;

/**
 * The persistence utility for the c l s map properties service. This utility wraps {@link CLSMapPropertiesPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSMapPropertiesPersistence
 * @see CLSMapPropertiesPersistenceImpl
 * @generated
 */
public class CLSMapPropertiesUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(CLSMapProperties clsMapProperties) {
		getPersistence().clearCache(clsMapProperties);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<CLSMapProperties> findWithDynamicQuery(
		DynamicQuery dynamicQuery) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<CLSMapProperties> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<CLSMapProperties> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
	 */
	public static CLSMapProperties update(CLSMapProperties clsMapProperties)
		throws SystemException {
		return getPersistence().update(clsMapProperties);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
	 */
	public static CLSMapProperties update(CLSMapProperties clsMapProperties,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(clsMapProperties, serviceContext);
	}

	/**
	* Caches the c l s map properties in the entity cache if it is enabled.
	*
	* @param clsMapProperties the c l s map properties
	*/
	public static void cacheResult(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties clsMapProperties) {
		getPersistence().cacheResult(clsMapProperties);
	}

	/**
	* Caches the c l s map propertieses in the entity cache if it is enabled.
	*
	* @param clsMapPropertieses the c l s map propertieses
	*/
	public static void cacheResult(
		java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties> clsMapPropertieses) {
		getPersistence().cacheResult(clsMapPropertieses);
	}

	/**
	* Creates a new c l s map properties with the primary key. Does not add the c l s map properties to the database.
	*
	* @param mapPropertiesId the primary key for the new c l s map properties
	* @return the new c l s map properties
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties create(
		long mapPropertiesId) {
		return getPersistence().create(mapPropertiesId);
	}

	/**
	* Removes the c l s map properties with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param mapPropertiesId the primary key of the c l s map properties
	* @return the c l s map properties that was removed
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSMapPropertiesException if a c l s map properties with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties remove(
		long mapPropertiesId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSMapPropertiesException {
		return getPersistence().remove(mapPropertiesId);
	}

	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties clsMapProperties)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(clsMapProperties);
	}

	/**
	* Returns the c l s map properties with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSMapPropertiesException} if it could not be found.
	*
	* @param mapPropertiesId the primary key of the c l s map properties
	* @return the c l s map properties
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSMapPropertiesException if a c l s map properties with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties findByPrimaryKey(
		long mapPropertiesId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSMapPropertiesException {
		return getPersistence().findByPrimaryKey(mapPropertiesId);
	}

	/**
	* Returns the c l s map properties with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param mapPropertiesId the primary key of the c l s map properties
	* @return the c l s map properties, or <code>null</code> if a c l s map properties with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties fetchByPrimaryKey(
		long mapPropertiesId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(mapPropertiesId);
	}

	/**
	* Returns all the c l s map propertieses.
	*
	* @return the c l s map propertieses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the c l s map propertieses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSMapPropertiesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s map propertieses
	* @param end the upper bound of the range of c l s map propertieses (not inclusive)
	* @return the range of c l s map propertieses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the c l s map propertieses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSMapPropertiesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s map propertieses
	* @param end the upper bound of the range of c l s map propertieses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of c l s map propertieses
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the c l s map propertieses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of c l s map propertieses.
	*
	* @return the number of c l s map propertieses
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static CLSMapPropertiesPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (CLSMapPropertiesPersistence)PortletBeanLocatorUtil.locate(it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.getServletContextName(),
					CLSMapPropertiesPersistence.class.getName());

			ReferenceRegistry.registerReference(CLSMapPropertiesUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated As of 6.2.0
	 */
	public void setPersistence(CLSMapPropertiesPersistence persistence) {
	}

	private static CLSMapPropertiesPersistence _persistence;
}