/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import it.eng.rspa.ideas.challenges.servicelayer.service.CLSOpLogIdeaLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 */
public class CLSOpLogIdeaClp extends BaseModelImpl<CLSOpLogIdea>
	implements CLSOpLogIdea {
	public CLSOpLogIdeaClp() {
	}

	@Override
	public Class<?> getModelClass() {
		return CLSOpLogIdea.class;
	}

	@Override
	public String getModelClassName() {
		return CLSOpLogIdea.class.getName();
	}

	@Override
	public long getPrimaryKey() {
		return _opLogId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setOpLogId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _opLogId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("opLogId", getOpLogId());
		attributes.put("ideaId", getIdeaId());
		attributes.put("userId", getUserId());
		attributes.put("date", getDate());
		attributes.put("extraData", getExtraData());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long opLogId = (Long)attributes.get("opLogId");

		if (opLogId != null) {
			setOpLogId(opLogId);
		}

		Long ideaId = (Long)attributes.get("ideaId");

		if (ideaId != null) {
			setIdeaId(ideaId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Date date = (Date)attributes.get("date");

		if (date != null) {
			setDate(date);
		}

		String extraData = (String)attributes.get("extraData");

		if (extraData != null) {
			setExtraData(extraData);
		}
	}

	@Override
	public long getOpLogId() {
		return _opLogId;
	}

	@Override
	public void setOpLogId(long opLogId) {
		_opLogId = opLogId;

		if (_clsOpLogIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsOpLogIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setOpLogId", long.class);

				method.invoke(_clsOpLogIdeaRemoteModel, opLogId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getIdeaId() {
		return _ideaId;
	}

	@Override
	public void setIdeaId(long ideaId) {
		_ideaId = ideaId;

		if (_clsOpLogIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsOpLogIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setIdeaId", long.class);

				method.invoke(_clsOpLogIdeaRemoteModel, ideaId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_userId = userId;

		if (_clsOpLogIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsOpLogIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setUserId", long.class);

				method.invoke(_clsOpLogIdeaRemoteModel, userId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	@Override
	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	@Override
	public Date getDate() {
		return _date;
	}

	@Override
	public void setDate(Date date) {
		_date = date;

		if (_clsOpLogIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsOpLogIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setDate", Date.class);

				method.invoke(_clsOpLogIdeaRemoteModel, date);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@Override
	public String getExtraData() {
		return _extraData;
	}

	@Override
	public void setExtraData(String extraData) {
		_extraData = extraData;

		if (_clsOpLogIdeaRemoteModel != null) {
			try {
				Class<?> clazz = _clsOpLogIdeaRemoteModel.getClass();

				Method method = clazz.getMethod("setExtraData", String.class);

				method.invoke(_clsOpLogIdeaRemoteModel, extraData);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getCLSOpLogIdeaRemoteModel() {
		return _clsOpLogIdeaRemoteModel;
	}

	public void setCLSOpLogIdeaRemoteModel(BaseModel<?> clsOpLogIdeaRemoteModel) {
		_clsOpLogIdeaRemoteModel = clsOpLogIdeaRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _clsOpLogIdeaRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_clsOpLogIdeaRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	@Override
	public void persist() throws SystemException {
		if (this.isNew()) {
			CLSOpLogIdeaLocalServiceUtil.addCLSOpLogIdea(this);
		}
		else {
			CLSOpLogIdeaLocalServiceUtil.updateCLSOpLogIdea(this);
		}
	}

	@Override
	public CLSOpLogIdea toEscapedModel() {
		return (CLSOpLogIdea)ProxyUtil.newProxyInstance(CLSOpLogIdea.class.getClassLoader(),
			new Class[] { CLSOpLogIdea.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		CLSOpLogIdeaClp clone = new CLSOpLogIdeaClp();

		clone.setOpLogId(getOpLogId());
		clone.setIdeaId(getIdeaId());
		clone.setUserId(getUserId());
		clone.setDate(getDate());
		clone.setExtraData(getExtraData());

		return clone;
	}

	@Override
	public int compareTo(CLSOpLogIdea clsOpLogIdea) {
		long primaryKey = clsOpLogIdea.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSOpLogIdeaClp)) {
			return false;
		}

		CLSOpLogIdeaClp clsOpLogIdea = (CLSOpLogIdeaClp)obj;

		long primaryKey = clsOpLogIdea.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	public Class<?> getClpSerializerClass() {
		return _clpSerializerClass;
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{opLogId=");
		sb.append(getOpLogId());
		sb.append(", ideaId=");
		sb.append(getIdeaId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", date=");
		sb.append(getDate());
		sb.append(", extraData=");
		sb.append(getExtraData());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(19);

		sb.append("<model><model-name>");
		sb.append(
			"it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>opLogId</column-name><column-value><![CDATA[");
		sb.append(getOpLogId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>ideaId</column-name><column-value><![CDATA[");
		sb.append(getIdeaId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>date</column-name><column-value><![CDATA[");
		sb.append(getDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>extraData</column-name><column-value><![CDATA[");
		sb.append(getExtraData());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _opLogId;
	private long _ideaId;
	private long _userId;
	private String _userUuid;
	private Date _date;
	private String _extraData;
	private BaseModel<?> _clsOpLogIdeaRemoteModel;
	private Class<?> _clpSerializerClass = it.eng.rspa.ideas.challenges.servicelayer.service.ClpSerializer.class;
}