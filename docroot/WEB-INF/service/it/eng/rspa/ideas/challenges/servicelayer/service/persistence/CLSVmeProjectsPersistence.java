/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects;

/**
 * The persistence interface for the c l s vme projects service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSVmeProjectsPersistenceImpl
 * @see CLSVmeProjectsUtil
 * @generated
 */
public interface CLSVmeProjectsPersistence extends BasePersistence<CLSVmeProjects> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CLSVmeProjectsUtil} to access the c l s vme projects persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the c l s vme projectses where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @return the matching c l s vme projectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects> findByprojectsByIdeaId(
		long ideaId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the c l s vme projectses where ideaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSVmeProjectsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param start the lower bound of the range of c l s vme projectses
	* @param end the upper bound of the range of c l s vme projectses (not inclusive)
	* @return the range of matching c l s vme projectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects> findByprojectsByIdeaId(
		long ideaId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the c l s vme projectses where ideaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSVmeProjectsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param start the lower bound of the range of c l s vme projectses
	* @param end the upper bound of the range of c l s vme projectses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching c l s vme projectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects> findByprojectsByIdeaId(
		long ideaId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first c l s vme projects in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s vme projects
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException if a matching c l s vme projects could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects findByprojectsByIdeaId_First(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException;

	/**
	* Returns the first c l s vme projects in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s vme projects, or <code>null</code> if a matching c l s vme projects could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects fetchByprojectsByIdeaId_First(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last c l s vme projects in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s vme projects
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException if a matching c l s vme projects could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects findByprojectsByIdeaId_Last(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException;

	/**
	* Returns the last c l s vme projects in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s vme projects, or <code>null</code> if a matching c l s vme projects could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects fetchByprojectsByIdeaId_Last(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the c l s vme projectses before and after the current c l s vme projects in the ordered set where ideaId = &#63;.
	*
	* @param recordId the primary key of the current c l s vme projects
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next c l s vme projects
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException if a c l s vme projects with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects[] findByprojectsByIdeaId_PrevAndNext(
		long recordId, long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException;

	/**
	* Removes all the c l s vme projectses where ideaId = &#63; from the database.
	*
	* @param ideaId the idea ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByprojectsByIdeaId(long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of c l s vme projectses where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @return the number of matching c l s vme projectses
	* @throws SystemException if a system exception occurred
	*/
	public int countByprojectsByIdeaId(long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the c l s vme projectses where ideaId = &#63; and isMockup = &#63;.
	*
	* @param ideaId the idea ID
	* @param isMockup the is mockup
	* @return the matching c l s vme projectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects> findByprojectsByIdeaIdAndType(
		long ideaId, boolean isMockup)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the c l s vme projectses where ideaId = &#63; and isMockup = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSVmeProjectsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param isMockup the is mockup
	* @param start the lower bound of the range of c l s vme projectses
	* @param end the upper bound of the range of c l s vme projectses (not inclusive)
	* @return the range of matching c l s vme projectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects> findByprojectsByIdeaIdAndType(
		long ideaId, boolean isMockup, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the c l s vme projectses where ideaId = &#63; and isMockup = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSVmeProjectsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param isMockup the is mockup
	* @param start the lower bound of the range of c l s vme projectses
	* @param end the upper bound of the range of c l s vme projectses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching c l s vme projectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects> findByprojectsByIdeaIdAndType(
		long ideaId, boolean isMockup, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first c l s vme projects in the ordered set where ideaId = &#63; and isMockup = &#63;.
	*
	* @param ideaId the idea ID
	* @param isMockup the is mockup
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s vme projects
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException if a matching c l s vme projects could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects findByprojectsByIdeaIdAndType_First(
		long ideaId, boolean isMockup,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException;

	/**
	* Returns the first c l s vme projects in the ordered set where ideaId = &#63; and isMockup = &#63;.
	*
	* @param ideaId the idea ID
	* @param isMockup the is mockup
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s vme projects, or <code>null</code> if a matching c l s vme projects could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects fetchByprojectsByIdeaIdAndType_First(
		long ideaId, boolean isMockup,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last c l s vme projects in the ordered set where ideaId = &#63; and isMockup = &#63;.
	*
	* @param ideaId the idea ID
	* @param isMockup the is mockup
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s vme projects
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException if a matching c l s vme projects could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects findByprojectsByIdeaIdAndType_Last(
		long ideaId, boolean isMockup,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException;

	/**
	* Returns the last c l s vme projects in the ordered set where ideaId = &#63; and isMockup = &#63;.
	*
	* @param ideaId the idea ID
	* @param isMockup the is mockup
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s vme projects, or <code>null</code> if a matching c l s vme projects could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects fetchByprojectsByIdeaIdAndType_Last(
		long ideaId, boolean isMockup,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the c l s vme projectses before and after the current c l s vme projects in the ordered set where ideaId = &#63; and isMockup = &#63;.
	*
	* @param recordId the primary key of the current c l s vme projects
	* @param ideaId the idea ID
	* @param isMockup the is mockup
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next c l s vme projects
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException if a c l s vme projects with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects[] findByprojectsByIdeaIdAndType_PrevAndNext(
		long recordId, long ideaId, boolean isMockup,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException;

	/**
	* Removes all the c l s vme projectses where ideaId = &#63; and isMockup = &#63; from the database.
	*
	* @param ideaId the idea ID
	* @param isMockup the is mockup
	* @throws SystemException if a system exception occurred
	*/
	public void removeByprojectsByIdeaIdAndType(long ideaId, boolean isMockup)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of c l s vme projectses where ideaId = &#63; and isMockup = &#63;.
	*
	* @param ideaId the idea ID
	* @param isMockup the is mockup
	* @return the number of matching c l s vme projectses
	* @throws SystemException if a system exception occurred
	*/
	public int countByprojectsByIdeaIdAndType(long ideaId, boolean isMockup)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the c l s vme projects in the entity cache if it is enabled.
	*
	* @param clsVmeProjects the c l s vme projects
	*/
	public void cacheResult(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects clsVmeProjects);

	/**
	* Caches the c l s vme projectses in the entity cache if it is enabled.
	*
	* @param clsVmeProjectses the c l s vme projectses
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects> clsVmeProjectses);

	/**
	* Creates a new c l s vme projects with the primary key. Does not add the c l s vme projects to the database.
	*
	* @param recordId the primary key for the new c l s vme projects
	* @return the new c l s vme projects
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects create(
		long recordId);

	/**
	* Removes the c l s vme projects with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param recordId the primary key of the c l s vme projects
	* @return the c l s vme projects that was removed
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException if a c l s vme projects with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects remove(
		long recordId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException;

	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects clsVmeProjects)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the c l s vme projects with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException} if it could not be found.
	*
	* @param recordId the primary key of the c l s vme projects
	* @return the c l s vme projects
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException if a c l s vme projects with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects findByPrimaryKey(
		long recordId)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSVmeProjectsException;

	/**
	* Returns the c l s vme projects with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param recordId the primary key of the c l s vme projects
	* @return the c l s vme projects, or <code>null</code> if a c l s vme projects with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects fetchByPrimaryKey(
		long recordId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the c l s vme projectses.
	*
	* @return the c l s vme projectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the c l s vme projectses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSVmeProjectsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s vme projectses
	* @param end the upper bound of the range of c l s vme projectses (not inclusive)
	* @return the range of c l s vme projectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the c l s vme projectses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSVmeProjectsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s vme projectses
	* @param end the upper bound of the range of c l s vme projectses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of c l s vme projectses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the c l s vme projectses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of c l s vme projectses.
	*
	* @return the number of c l s vme projectses
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}