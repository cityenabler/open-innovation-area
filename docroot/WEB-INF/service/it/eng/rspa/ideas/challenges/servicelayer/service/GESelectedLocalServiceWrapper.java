/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link GESelectedLocalService}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see GESelectedLocalService
 * @generated
 */
public class GESelectedLocalServiceWrapper implements GESelectedLocalService,
	ServiceWrapper<GESelectedLocalService> {
	public GESelectedLocalServiceWrapper(
		GESelectedLocalService geSelectedLocalService) {
		_geSelectedLocalService = geSelectedLocalService;
	}

	/**
	* Adds the g e selected to the database. Also notifies the appropriate model listeners.
	*
	* @param geSelected the g e selected
	* @return the g e selected that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.GESelected addGESelected(
		it.eng.rspa.ideas.challenges.servicelayer.model.GESelected geSelected)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _geSelectedLocalService.addGESelected(geSelected);
	}

	/**
	* Creates a new g e selected with the primary key. Does not add the g e selected to the database.
	*
	* @param geSelectedPK the primary key for the new g e selected
	* @return the new g e selected
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.GESelected createGESelected(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.GESelectedPK geSelectedPK) {
		return _geSelectedLocalService.createGESelected(geSelectedPK);
	}

	/**
	* Deletes the g e selected with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param geSelectedPK the primary key of the g e selected
	* @return the g e selected that was removed
	* @throws PortalException if a g e selected with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.GESelected deleteGESelected(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.GESelectedPK geSelectedPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _geSelectedLocalService.deleteGESelected(geSelectedPK);
	}

	/**
	* Deletes the g e selected from the database. Also notifies the appropriate model listeners.
	*
	* @param geSelected the g e selected
	* @return the g e selected that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.GESelected deleteGESelected(
		it.eng.rspa.ideas.challenges.servicelayer.model.GESelected geSelected)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _geSelectedLocalService.deleteGESelected(geSelected);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _geSelectedLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _geSelectedLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.GESelectedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _geSelectedLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.GESelectedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _geSelectedLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _geSelectedLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _geSelectedLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.GESelected fetchGESelected(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.GESelectedPK geSelectedPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _geSelectedLocalService.fetchGESelected(geSelectedPK);
	}

	/**
	* Returns the g e selected with the primary key.
	*
	* @param geSelectedPK the primary key of the g e selected
	* @return the g e selected
	* @throws PortalException if a g e selected with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.GESelected getGESelected(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.GESelectedPK geSelectedPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _geSelectedLocalService.getGESelected(geSelectedPK);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _geSelectedLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the g e selecteds.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.GESelectedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of g e selecteds
	* @param end the upper bound of the range of g e selecteds (not inclusive)
	* @return the range of g e selecteds
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.GESelected> getGESelecteds(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _geSelectedLocalService.getGESelecteds(start, end);
	}

	/**
	* Returns the number of g e selecteds.
	*
	* @return the number of g e selecteds
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getGESelectedsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _geSelectedLocalService.getGESelectedsCount();
	}

	/**
	* Updates the g e selected in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param geSelected the g e selected
	* @return the g e selected that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.GESelected updateGESelected(
		it.eng.rspa.ideas.challenges.servicelayer.model.GESelected geSelected)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _geSelectedLocalService.updateGESelected(geSelected);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _geSelectedLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_geSelectedLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _geSelectedLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	/**
	* @param ideaId
	* @return
	*/
	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.GESelected> getGeSelectedsByIdeaId(
		long ideaId) {
		return _geSelectedLocalService.getGeSelectedsByIdeaId(ideaId);
	}

	/**
	* @param nid
	* @return
	*/
	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.GESelected> getGeSelectedsByNid(
		java.lang.String nid) {
		return _geSelectedLocalService.getGeSelectedsByNid(nid);
	}

	/**
	* @param ideaId
	* @param nid
	* @return
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.GESelected getGeSelectedsByIdeaIdAndNid(
		long ideaId, java.lang.String nid) {
		return _geSelectedLocalService.getGeSelectedsByIdeaIdAndNid(ideaId, nid);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public GESelectedLocalService getWrappedGESelectedLocalService() {
		return _geSelectedLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedGESelectedLocalService(
		GESelectedLocalService geSelectedLocalService) {
		_geSelectedLocalService = geSelectedLocalService;
	}

	@Override
	public GESelectedLocalService getWrappedService() {
		return _geSelectedLocalService;
	}

	@Override
	public void setWrappedService(GESelectedLocalService geSelectedLocalService) {
		_geSelectedLocalService = geSelectedLocalService;
	}

	private GESelectedLocalService _geSelectedLocalService;
}