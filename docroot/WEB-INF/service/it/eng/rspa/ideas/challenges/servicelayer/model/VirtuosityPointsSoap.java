/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.VirtuosityPointsPK;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.eng.rspa.ideas.challenges.servicelayer.service.http.VirtuosityPointsServiceSoap}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.http.VirtuosityPointsServiceSoap
 * @generated
 */
public class VirtuosityPointsSoap implements Serializable {
	public static VirtuosityPointsSoap toSoapModel(VirtuosityPoints model) {
		VirtuosityPointsSoap soapModel = new VirtuosityPointsSoap();

		soapModel.setChallengeId(model.getChallengeId());
		soapModel.setPosition(model.getPosition());
		soapModel.setPoints(model.getPoints());
		soapModel.setDateAdded(model.getDateAdded());

		return soapModel;
	}

	public static VirtuosityPointsSoap[] toSoapModels(VirtuosityPoints[] models) {
		VirtuosityPointsSoap[] soapModels = new VirtuosityPointsSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static VirtuosityPointsSoap[][] toSoapModels(
		VirtuosityPoints[][] models) {
		VirtuosityPointsSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new VirtuosityPointsSoap[models.length][models[0].length];
		}
		else {
			soapModels = new VirtuosityPointsSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static VirtuosityPointsSoap[] toSoapModels(
		List<VirtuosityPoints> models) {
		List<VirtuosityPointsSoap> soapModels = new ArrayList<VirtuosityPointsSoap>(models.size());

		for (VirtuosityPoints model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new VirtuosityPointsSoap[soapModels.size()]);
	}

	public VirtuosityPointsSoap() {
	}

	public VirtuosityPointsPK getPrimaryKey() {
		return new VirtuosityPointsPK(_challengeId, _position);
	}

	public void setPrimaryKey(VirtuosityPointsPK pk) {
		setChallengeId(pk.challengeId);
		setPosition(pk.position);
	}

	public long getChallengeId() {
		return _challengeId;
	}

	public void setChallengeId(long challengeId) {
		_challengeId = challengeId;
	}

	public long getPosition() {
		return _position;
	}

	public void setPosition(long position) {
		_position = position;
	}

	public long getPoints() {
		return _points;
	}

	public void setPoints(long points) {
		_points = points;
	}

	public Date getDateAdded() {
		return _dateAdded;
	}

	public void setDateAdded(Date dateAdded) {
		_dateAdded = dateAdded;
	}

	private long _challengeId;
	private long _position;
	private long _points;
	private Date _dateAdded;
}