/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints;

/**
 * The persistence interface for the virtuosity points service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see VirtuosityPointsPersistenceImpl
 * @see VirtuosityPointsUtil
 * @generated
 */
public interface VirtuosityPointsPersistence extends BasePersistence<VirtuosityPoints> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link VirtuosityPointsUtil} to access the virtuosity points persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the virtuosity pointses where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @return the matching virtuosity pointses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints> findBybyChallengeId(
		long challengeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the virtuosity pointses where challengeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.VirtuosityPointsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param challengeId the challenge ID
	* @param start the lower bound of the range of virtuosity pointses
	* @param end the upper bound of the range of virtuosity pointses (not inclusive)
	* @return the range of matching virtuosity pointses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints> findBybyChallengeId(
		long challengeId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the virtuosity pointses where challengeId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.VirtuosityPointsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param challengeId the challenge ID
	* @param start the lower bound of the range of virtuosity pointses
	* @param end the upper bound of the range of virtuosity pointses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching virtuosity pointses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints> findBybyChallengeId(
		long challengeId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first virtuosity points in the ordered set where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching virtuosity points
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchVirtuosityPointsException if a matching virtuosity points could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints findBybyChallengeId_First(
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchVirtuosityPointsException;

	/**
	* Returns the first virtuosity points in the ordered set where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching virtuosity points, or <code>null</code> if a matching virtuosity points could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints fetchBybyChallengeId_First(
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last virtuosity points in the ordered set where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching virtuosity points
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchVirtuosityPointsException if a matching virtuosity points could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints findBybyChallengeId_Last(
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchVirtuosityPointsException;

	/**
	* Returns the last virtuosity points in the ordered set where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching virtuosity points, or <code>null</code> if a matching virtuosity points could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints fetchBybyChallengeId_Last(
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the virtuosity pointses before and after the current virtuosity points in the ordered set where challengeId = &#63;.
	*
	* @param virtuosityPointsPK the primary key of the current virtuosity points
	* @param challengeId the challenge ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next virtuosity points
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchVirtuosityPointsException if a virtuosity points with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints[] findBybyChallengeId_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.VirtuosityPointsPK virtuosityPointsPK,
		long challengeId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchVirtuosityPointsException;

	/**
	* Removes all the virtuosity pointses where challengeId = &#63; from the database.
	*
	* @param challengeId the challenge ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeBybyChallengeId(long challengeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of virtuosity pointses where challengeId = &#63;.
	*
	* @param challengeId the challenge ID
	* @return the number of matching virtuosity pointses
	* @throws SystemException if a system exception occurred
	*/
	public int countBybyChallengeId(long challengeId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the virtuosity points in the entity cache if it is enabled.
	*
	* @param virtuosityPoints the virtuosity points
	*/
	public void cacheResult(
		it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints virtuosityPoints);

	/**
	* Caches the virtuosity pointses in the entity cache if it is enabled.
	*
	* @param virtuosityPointses the virtuosity pointses
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints> virtuosityPointses);

	/**
	* Creates a new virtuosity points with the primary key. Does not add the virtuosity points to the database.
	*
	* @param virtuosityPointsPK the primary key for the new virtuosity points
	* @return the new virtuosity points
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints create(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.VirtuosityPointsPK virtuosityPointsPK);

	/**
	* Removes the virtuosity points with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param virtuosityPointsPK the primary key of the virtuosity points
	* @return the virtuosity points that was removed
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchVirtuosityPointsException if a virtuosity points with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints remove(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.VirtuosityPointsPK virtuosityPointsPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchVirtuosityPointsException;

	public it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints virtuosityPoints)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the virtuosity points with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchVirtuosityPointsException} if it could not be found.
	*
	* @param virtuosityPointsPK the primary key of the virtuosity points
	* @return the virtuosity points
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchVirtuosityPointsException if a virtuosity points with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints findByPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.VirtuosityPointsPK virtuosityPointsPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchVirtuosityPointsException;

	/**
	* Returns the virtuosity points with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param virtuosityPointsPK the primary key of the virtuosity points
	* @return the virtuosity points, or <code>null</code> if a virtuosity points with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints fetchByPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.VirtuosityPointsPK virtuosityPointsPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the virtuosity pointses.
	*
	* @return the virtuosity pointses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the virtuosity pointses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.VirtuosityPointsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of virtuosity pointses
	* @param end the upper bound of the range of virtuosity pointses (not inclusive)
	* @return the range of virtuosity pointses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the virtuosity pointses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.VirtuosityPointsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of virtuosity pointses
	* @param end the upper bound of the range of virtuosity pointses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of virtuosity pointses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the virtuosity pointses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of virtuosity pointses.
	*
	* @return the number of virtuosity pointses
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}