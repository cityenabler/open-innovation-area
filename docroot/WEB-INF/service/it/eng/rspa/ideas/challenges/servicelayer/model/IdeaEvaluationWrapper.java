/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link IdeaEvaluation}.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see IdeaEvaluation
 * @generated
 */
public class IdeaEvaluationWrapper implements IdeaEvaluation,
	ModelWrapper<IdeaEvaluation> {
	public IdeaEvaluationWrapper(IdeaEvaluation ideaEvaluation) {
		_ideaEvaluation = ideaEvaluation;
	}

	@Override
	public Class<?> getModelClass() {
		return IdeaEvaluation.class;
	}

	@Override
	public String getModelClassName() {
		return IdeaEvaluation.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("criteriaId", getCriteriaId());
		attributes.put("ideaId", getIdeaId());
		attributes.put("motivation", getMotivation());
		attributes.put("passed", getPassed());
		attributes.put("score", getScore());
		attributes.put("date", getDate());
		attributes.put("authorId", getAuthorId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long criteriaId = (Long)attributes.get("criteriaId");

		if (criteriaId != null) {
			setCriteriaId(criteriaId);
		}

		Long ideaId = (Long)attributes.get("ideaId");

		if (ideaId != null) {
			setIdeaId(ideaId);
		}

		String motivation = (String)attributes.get("motivation");

		if (motivation != null) {
			setMotivation(motivation);
		}

		Boolean passed = (Boolean)attributes.get("passed");

		if (passed != null) {
			setPassed(passed);
		}

		Double score = (Double)attributes.get("score");

		if (score != null) {
			setScore(score);
		}

		Date date = (Date)attributes.get("date");

		if (date != null) {
			setDate(date);
		}

		Long authorId = (Long)attributes.get("authorId");

		if (authorId != null) {
			setAuthorId(authorId);
		}
	}

	/**
	* Returns the primary key of this idea evaluation.
	*
	* @return the primary key of this idea evaluation
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.service.persistence.IdeaEvaluationPK getPrimaryKey() {
		return _ideaEvaluation.getPrimaryKey();
	}

	/**
	* Sets the primary key of this idea evaluation.
	*
	* @param primaryKey the primary key of this idea evaluation
	*/
	@Override
	public void setPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.IdeaEvaluationPK primaryKey) {
		_ideaEvaluation.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the criteria ID of this idea evaluation.
	*
	* @return the criteria ID of this idea evaluation
	*/
	@Override
	public long getCriteriaId() {
		return _ideaEvaluation.getCriteriaId();
	}

	/**
	* Sets the criteria ID of this idea evaluation.
	*
	* @param criteriaId the criteria ID of this idea evaluation
	*/
	@Override
	public void setCriteriaId(long criteriaId) {
		_ideaEvaluation.setCriteriaId(criteriaId);
	}

	/**
	* Returns the idea ID of this idea evaluation.
	*
	* @return the idea ID of this idea evaluation
	*/
	@Override
	public long getIdeaId() {
		return _ideaEvaluation.getIdeaId();
	}

	/**
	* Sets the idea ID of this idea evaluation.
	*
	* @param ideaId the idea ID of this idea evaluation
	*/
	@Override
	public void setIdeaId(long ideaId) {
		_ideaEvaluation.setIdeaId(ideaId);
	}

	/**
	* Returns the motivation of this idea evaluation.
	*
	* @return the motivation of this idea evaluation
	*/
	@Override
	public java.lang.String getMotivation() {
		return _ideaEvaluation.getMotivation();
	}

	/**
	* Sets the motivation of this idea evaluation.
	*
	* @param motivation the motivation of this idea evaluation
	*/
	@Override
	public void setMotivation(java.lang.String motivation) {
		_ideaEvaluation.setMotivation(motivation);
	}

	/**
	* Returns the passed of this idea evaluation.
	*
	* @return the passed of this idea evaluation
	*/
	@Override
	public boolean getPassed() {
		return _ideaEvaluation.getPassed();
	}

	/**
	* Returns <code>true</code> if this idea evaluation is passed.
	*
	* @return <code>true</code> if this idea evaluation is passed; <code>false</code> otherwise
	*/
	@Override
	public boolean isPassed() {
		return _ideaEvaluation.isPassed();
	}

	/**
	* Sets whether this idea evaluation is passed.
	*
	* @param passed the passed of this idea evaluation
	*/
	@Override
	public void setPassed(boolean passed) {
		_ideaEvaluation.setPassed(passed);
	}

	/**
	* Returns the score of this idea evaluation.
	*
	* @return the score of this idea evaluation
	*/
	@Override
	public double getScore() {
		return _ideaEvaluation.getScore();
	}

	/**
	* Sets the score of this idea evaluation.
	*
	* @param score the score of this idea evaluation
	*/
	@Override
	public void setScore(double score) {
		_ideaEvaluation.setScore(score);
	}

	/**
	* Returns the date of this idea evaluation.
	*
	* @return the date of this idea evaluation
	*/
	@Override
	public java.util.Date getDate() {
		return _ideaEvaluation.getDate();
	}

	/**
	* Sets the date of this idea evaluation.
	*
	* @param date the date of this idea evaluation
	*/
	@Override
	public void setDate(java.util.Date date) {
		_ideaEvaluation.setDate(date);
	}

	/**
	* Returns the author ID of this idea evaluation.
	*
	* @return the author ID of this idea evaluation
	*/
	@Override
	public long getAuthorId() {
		return _ideaEvaluation.getAuthorId();
	}

	/**
	* Sets the author ID of this idea evaluation.
	*
	* @param authorId the author ID of this idea evaluation
	*/
	@Override
	public void setAuthorId(long authorId) {
		_ideaEvaluation.setAuthorId(authorId);
	}

	@Override
	public boolean isNew() {
		return _ideaEvaluation.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_ideaEvaluation.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _ideaEvaluation.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_ideaEvaluation.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _ideaEvaluation.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _ideaEvaluation.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_ideaEvaluation.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _ideaEvaluation.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_ideaEvaluation.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_ideaEvaluation.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_ideaEvaluation.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new IdeaEvaluationWrapper((IdeaEvaluation)_ideaEvaluation.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation ideaEvaluation) {
		return _ideaEvaluation.compareTo(ideaEvaluation);
	}

	@Override
	public int hashCode() {
		return _ideaEvaluation.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation> toCacheModel() {
		return _ideaEvaluation.toCacheModel();
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation toEscapedModel() {
		return new IdeaEvaluationWrapper(_ideaEvaluation.toEscapedModel());
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation toUnescapedModel() {
		return new IdeaEvaluationWrapper(_ideaEvaluation.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _ideaEvaluation.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _ideaEvaluation.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_ideaEvaluation.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof IdeaEvaluationWrapper)) {
			return false;
		}

		IdeaEvaluationWrapper ideaEvaluationWrapper = (IdeaEvaluationWrapper)obj;

		if (Validator.equals(_ideaEvaluation,
					ideaEvaluationWrapper._ideaEvaluation)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public IdeaEvaluation getWrappedIdeaEvaluation() {
		return _ideaEvaluation;
	}

	@Override
	public IdeaEvaluation getWrappedModel() {
		return _ideaEvaluation;
	}

	@Override
	public void resetOriginalValues() {
		_ideaEvaluation.resetOriginalValues();
	}

	private IdeaEvaluation _ideaEvaluation;
}