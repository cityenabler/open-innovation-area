/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.util.Accessor;
import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the CLSIdea service. Represents a row in the &quot;CSL_CLSIdea&quot; database table, with each column mapped to a property of this class.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSIdeaModel
 * @see it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl
 * @generated
 */
public interface CLSIdea extends CLSIdeaModel, PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<CLSIdea, String> UUID_ACCESSOR = new Accessor<CLSIdea, String>() {
			@Override
			public String get(CLSIdea clsIdea) {
				return clsIdea.getUuid();
			}
		};
}