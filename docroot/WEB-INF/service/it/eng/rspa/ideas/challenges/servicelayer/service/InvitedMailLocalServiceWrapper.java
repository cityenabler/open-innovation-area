/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link InvitedMailLocalService}.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see InvitedMailLocalService
 * @generated
 */
public class InvitedMailLocalServiceWrapper implements InvitedMailLocalService,
	ServiceWrapper<InvitedMailLocalService> {
	public InvitedMailLocalServiceWrapper(
		InvitedMailLocalService invitedMailLocalService) {
		_invitedMailLocalService = invitedMailLocalService;
	}

	/**
	* Adds the invited mail to the database. Also notifies the appropriate model listeners.
	*
	* @param invitedMail the invited mail
	* @return the invited mail that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail addInvitedMail(
		it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail invitedMail)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _invitedMailLocalService.addInvitedMail(invitedMail);
	}

	/**
	* Creates a new invited mail with the primary key. Does not add the invited mail to the database.
	*
	* @param invitedMailPK the primary key for the new invited mail
	* @return the new invited mail
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail createInvitedMail(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.InvitedMailPK invitedMailPK) {
		return _invitedMailLocalService.createInvitedMail(invitedMailPK);
	}

	/**
	* Deletes the invited mail with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param invitedMailPK the primary key of the invited mail
	* @return the invited mail that was removed
	* @throws PortalException if a invited mail with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail deleteInvitedMail(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.InvitedMailPK invitedMailPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _invitedMailLocalService.deleteInvitedMail(invitedMailPK);
	}

	/**
	* Deletes the invited mail from the database. Also notifies the appropriate model listeners.
	*
	* @param invitedMail the invited mail
	* @return the invited mail that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail deleteInvitedMail(
		it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail invitedMail)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _invitedMailLocalService.deleteInvitedMail(invitedMail);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _invitedMailLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _invitedMailLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.InvitedMailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _invitedMailLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.InvitedMailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _invitedMailLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _invitedMailLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _invitedMailLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail fetchInvitedMail(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.InvitedMailPK invitedMailPK)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _invitedMailLocalService.fetchInvitedMail(invitedMailPK);
	}

	/**
	* Returns the invited mail with the primary key.
	*
	* @param invitedMailPK the primary key of the invited mail
	* @return the invited mail
	* @throws PortalException if a invited mail with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail getInvitedMail(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.InvitedMailPK invitedMailPK)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _invitedMailLocalService.getInvitedMail(invitedMailPK);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _invitedMailLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the invited mails.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.InvitedMailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of invited mails
	* @param end the upper bound of the range of invited mails (not inclusive)
	* @return the range of invited mails
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail> getInvitedMails(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _invitedMailLocalService.getInvitedMails(start, end);
	}

	/**
	* Returns the number of invited mails.
	*
	* @return the number of invited mails
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getInvitedMailsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _invitedMailLocalService.getInvitedMailsCount();
	}

	/**
	* Updates the invited mail in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param invitedMail the invited mail
	* @return the invited mail that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail updateInvitedMail(
		it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail invitedMail)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _invitedMailLocalService.updateInvitedMail(invitedMail);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _invitedMailLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_invitedMailLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _invitedMailLocalService.invokeMethod(name, parameterTypes,
			arguments);
	}

	/**
	* @param ideaId
	* @return
	*/
	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail> getInvitedMailByMail(
		java.lang.String mail) {
		return _invitedMailLocalService.getInvitedMailByMail(mail);
	}

	/**
	* @param ideaId
	* @return
	*/
	@Override
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail> getInvitedMailByIdeaId(
		long ideaId) {
		return _invitedMailLocalService.getInvitedMailByIdeaId(ideaId);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public InvitedMailLocalService getWrappedInvitedMailLocalService() {
		return _invitedMailLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedInvitedMailLocalService(
		InvitedMailLocalService invitedMailLocalService) {
		_invitedMailLocalService = invitedMailLocalService;
	}

	@Override
	public InvitedMailLocalService getWrappedService() {
		return _invitedMailLocalService;
	}

	@Override
	public void setWrappedService(
		InvitedMailLocalService invitedMailLocalService) {
		_invitedMailLocalService = invitedMailLocalService;
	}

	private InvitedMailLocalService _invitedMailLocalService;
}