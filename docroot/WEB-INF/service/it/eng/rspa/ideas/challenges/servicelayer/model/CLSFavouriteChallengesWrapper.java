/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CLSFavouriteChallenges}.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSFavouriteChallenges
 * @generated
 */
public class CLSFavouriteChallengesWrapper implements CLSFavouriteChallenges,
	ModelWrapper<CLSFavouriteChallenges> {
	public CLSFavouriteChallengesWrapper(
		CLSFavouriteChallenges clsFavouriteChallenges) {
		_clsFavouriteChallenges = clsFavouriteChallenges;
	}

	@Override
	public Class<?> getModelClass() {
		return CLSFavouriteChallenges.class;
	}

	@Override
	public String getModelClassName() {
		return CLSFavouriteChallenges.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("challengeId", getChallengeId());
		attributes.put("userId", getUserId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long challengeId = (Long)attributes.get("challengeId");

		if (challengeId != null) {
			setChallengeId(challengeId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}
	}

	/**
	* Returns the primary key of this c l s favourite challenges.
	*
	* @return the primary key of this c l s favourite challenges
	*/
	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteChallengesPK getPrimaryKey() {
		return _clsFavouriteChallenges.getPrimaryKey();
	}

	/**
	* Sets the primary key of this c l s favourite challenges.
	*
	* @param primaryKey the primary key of this c l s favourite challenges
	*/
	@Override
	public void setPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteChallengesPK primaryKey) {
		_clsFavouriteChallenges.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the challenge ID of this c l s favourite challenges.
	*
	* @return the challenge ID of this c l s favourite challenges
	*/
	@Override
	public long getChallengeId() {
		return _clsFavouriteChallenges.getChallengeId();
	}

	/**
	* Sets the challenge ID of this c l s favourite challenges.
	*
	* @param challengeId the challenge ID of this c l s favourite challenges
	*/
	@Override
	public void setChallengeId(long challengeId) {
		_clsFavouriteChallenges.setChallengeId(challengeId);
	}

	/**
	* Returns the user ID of this c l s favourite challenges.
	*
	* @return the user ID of this c l s favourite challenges
	*/
	@Override
	public long getUserId() {
		return _clsFavouriteChallenges.getUserId();
	}

	/**
	* Sets the user ID of this c l s favourite challenges.
	*
	* @param userId the user ID of this c l s favourite challenges
	*/
	@Override
	public void setUserId(long userId) {
		_clsFavouriteChallenges.setUserId(userId);
	}

	/**
	* Returns the user uuid of this c l s favourite challenges.
	*
	* @return the user uuid of this c l s favourite challenges
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsFavouriteChallenges.getUserUuid();
	}

	/**
	* Sets the user uuid of this c l s favourite challenges.
	*
	* @param userUuid the user uuid of this c l s favourite challenges
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_clsFavouriteChallenges.setUserUuid(userUuid);
	}

	@Override
	public boolean isNew() {
		return _clsFavouriteChallenges.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_clsFavouriteChallenges.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _clsFavouriteChallenges.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_clsFavouriteChallenges.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _clsFavouriteChallenges.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _clsFavouriteChallenges.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_clsFavouriteChallenges.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _clsFavouriteChallenges.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_clsFavouriteChallenges.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_clsFavouriteChallenges.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_clsFavouriteChallenges.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CLSFavouriteChallengesWrapper((CLSFavouriteChallenges)_clsFavouriteChallenges.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges clsFavouriteChallenges) {
		return _clsFavouriteChallenges.compareTo(clsFavouriteChallenges);
	}

	@Override
	public int hashCode() {
		return _clsFavouriteChallenges.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges> toCacheModel() {
		return _clsFavouriteChallenges.toCacheModel();
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges toEscapedModel() {
		return new CLSFavouriteChallengesWrapper(_clsFavouriteChallenges.toEscapedModel());
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges toUnescapedModel() {
		return new CLSFavouriteChallengesWrapper(_clsFavouriteChallenges.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _clsFavouriteChallenges.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _clsFavouriteChallenges.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_clsFavouriteChallenges.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSFavouriteChallengesWrapper)) {
			return false;
		}

		CLSFavouriteChallengesWrapper clsFavouriteChallengesWrapper = (CLSFavouriteChallengesWrapper)obj;

		if (Validator.equals(_clsFavouriteChallenges,
					clsFavouriteChallengesWrapper._clsFavouriteChallenges)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public CLSFavouriteChallenges getWrappedCLSFavouriteChallenges() {
		return _clsFavouriteChallenges;
	}

	@Override
	public CLSFavouriteChallenges getWrappedModel() {
		return _clsFavouriteChallenges;
	}

	@Override
	public void resetOriginalValues() {
		_clsFavouriteChallenges.resetOriginalValues();
	}

	private CLSFavouriteChallenges _clsFavouriteChallenges;
}