/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts;

/**
 * The persistence interface for the c l s artifacts service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSArtifactsPersistenceImpl
 * @see CLSArtifactsUtil
 * @generated
 */
public interface CLSArtifactsPersistence extends BasePersistence<CLSArtifacts> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CLSArtifactsUtil} to access the c l s artifacts persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the c l s artifactses where ideaId = &#63; and artifactId = &#63;.
	*
	* @param ideaId the idea ID
	* @param artifactId the artifact ID
	* @return the matching c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> findByartifactsByIdeaIdEArtifactId(
		long ideaId, long artifactId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the c l s artifactses where ideaId = &#63; and artifactId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param artifactId the artifact ID
	* @param start the lower bound of the range of c l s artifactses
	* @param end the upper bound of the range of c l s artifactses (not inclusive)
	* @return the range of matching c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> findByartifactsByIdeaIdEArtifactId(
		long ideaId, long artifactId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the c l s artifactses where ideaId = &#63; and artifactId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param artifactId the artifact ID
	* @param start the lower bound of the range of c l s artifactses
	* @param end the upper bound of the range of c l s artifactses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> findByartifactsByIdeaIdEArtifactId(
		long ideaId, long artifactId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first c l s artifacts in the ordered set where ideaId = &#63; and artifactId = &#63;.
	*
	* @param ideaId the idea ID
	* @param artifactId the artifact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s artifacts
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a matching c l s artifacts could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts findByartifactsByIdeaIdEArtifactId_First(
		long ideaId, long artifactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException;

	/**
	* Returns the first c l s artifacts in the ordered set where ideaId = &#63; and artifactId = &#63;.
	*
	* @param ideaId the idea ID
	* @param artifactId the artifact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s artifacts, or <code>null</code> if a matching c l s artifacts could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts fetchByartifactsByIdeaIdEArtifactId_First(
		long ideaId, long artifactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last c l s artifacts in the ordered set where ideaId = &#63; and artifactId = &#63;.
	*
	* @param ideaId the idea ID
	* @param artifactId the artifact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s artifacts
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a matching c l s artifacts could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts findByartifactsByIdeaIdEArtifactId_Last(
		long ideaId, long artifactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException;

	/**
	* Returns the last c l s artifacts in the ordered set where ideaId = &#63; and artifactId = &#63;.
	*
	* @param ideaId the idea ID
	* @param artifactId the artifact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s artifacts, or <code>null</code> if a matching c l s artifacts could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts fetchByartifactsByIdeaIdEArtifactId_Last(
		long ideaId, long artifactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the c l s artifactses before and after the current c l s artifacts in the ordered set where ideaId = &#63; and artifactId = &#63;.
	*
	* @param clsArtifactsPK the primary key of the current c l s artifacts
	* @param ideaId the idea ID
	* @param artifactId the artifact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next c l s artifacts
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a c l s artifacts with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts[] findByartifactsByIdeaIdEArtifactId_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSArtifactsPK clsArtifactsPK,
		long ideaId, long artifactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException;

	/**
	* Removes all the c l s artifactses where ideaId = &#63; and artifactId = &#63; from the database.
	*
	* @param ideaId the idea ID
	* @param artifactId the artifact ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByartifactsByIdeaIdEArtifactId(long ideaId,
		long artifactId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of c l s artifactses where ideaId = &#63; and artifactId = &#63;.
	*
	* @param ideaId the idea ID
	* @param artifactId the artifact ID
	* @return the number of matching c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public int countByartifactsByIdeaIdEArtifactId(long ideaId, long artifactId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the c l s artifactses where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @return the matching c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> findByartifactsByIdeaId(
		long ideaId) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the c l s artifactses where ideaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param start the lower bound of the range of c l s artifactses
	* @param end the upper bound of the range of c l s artifactses (not inclusive)
	* @return the range of matching c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> findByartifactsByIdeaId(
		long ideaId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the c l s artifactses where ideaId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param ideaId the idea ID
	* @param start the lower bound of the range of c l s artifactses
	* @param end the upper bound of the range of c l s artifactses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> findByartifactsByIdeaId(
		long ideaId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first c l s artifacts in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s artifacts
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a matching c l s artifacts could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts findByartifactsByIdeaId_First(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException;

	/**
	* Returns the first c l s artifacts in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s artifacts, or <code>null</code> if a matching c l s artifacts could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts fetchByartifactsByIdeaId_First(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last c l s artifacts in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s artifacts
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a matching c l s artifacts could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts findByartifactsByIdeaId_Last(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException;

	/**
	* Returns the last c l s artifacts in the ordered set where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s artifacts, or <code>null</code> if a matching c l s artifacts could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts fetchByartifactsByIdeaId_Last(
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the c l s artifactses before and after the current c l s artifacts in the ordered set where ideaId = &#63;.
	*
	* @param clsArtifactsPK the primary key of the current c l s artifacts
	* @param ideaId the idea ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next c l s artifacts
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a c l s artifacts with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts[] findByartifactsByIdeaId_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSArtifactsPK clsArtifactsPK,
		long ideaId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException;

	/**
	* Removes all the c l s artifactses where ideaId = &#63; from the database.
	*
	* @param ideaId the idea ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByartifactsByIdeaId(long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of c l s artifactses where ideaId = &#63;.
	*
	* @param ideaId the idea ID
	* @return the number of matching c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public int countByartifactsByIdeaId(long ideaId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the c l s artifactses where artifactId = &#63;.
	*
	* @param artifactId the artifact ID
	* @return the matching c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> findByartifactsByArtifactId(
		long artifactId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the c l s artifactses where artifactId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param artifactId the artifact ID
	* @param start the lower bound of the range of c l s artifactses
	* @param end the upper bound of the range of c l s artifactses (not inclusive)
	* @return the range of matching c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> findByartifactsByArtifactId(
		long artifactId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the c l s artifactses where artifactId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param artifactId the artifact ID
	* @param start the lower bound of the range of c l s artifactses
	* @param end the upper bound of the range of c l s artifactses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> findByartifactsByArtifactId(
		long artifactId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first c l s artifacts in the ordered set where artifactId = &#63;.
	*
	* @param artifactId the artifact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s artifacts
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a matching c l s artifacts could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts findByartifactsByArtifactId_First(
		long artifactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException;

	/**
	* Returns the first c l s artifacts in the ordered set where artifactId = &#63;.
	*
	* @param artifactId the artifact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching c l s artifacts, or <code>null</code> if a matching c l s artifacts could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts fetchByartifactsByArtifactId_First(
		long artifactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last c l s artifacts in the ordered set where artifactId = &#63;.
	*
	* @param artifactId the artifact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s artifacts
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a matching c l s artifacts could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts findByartifactsByArtifactId_Last(
		long artifactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException;

	/**
	* Returns the last c l s artifacts in the ordered set where artifactId = &#63;.
	*
	* @param artifactId the artifact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching c l s artifacts, or <code>null</code> if a matching c l s artifacts could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts fetchByartifactsByArtifactId_Last(
		long artifactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the c l s artifactses before and after the current c l s artifacts in the ordered set where artifactId = &#63;.
	*
	* @param clsArtifactsPK the primary key of the current c l s artifacts
	* @param artifactId the artifact ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next c l s artifacts
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a c l s artifacts with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts[] findByartifactsByArtifactId_PrevAndNext(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSArtifactsPK clsArtifactsPK,
		long artifactId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException;

	/**
	* Removes all the c l s artifactses where artifactId = &#63; from the database.
	*
	* @param artifactId the artifact ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByartifactsByArtifactId(long artifactId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of c l s artifactses where artifactId = &#63;.
	*
	* @param artifactId the artifact ID
	* @return the number of matching c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public int countByartifactsByArtifactId(long artifactId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Caches the c l s artifacts in the entity cache if it is enabled.
	*
	* @param clsArtifacts the c l s artifacts
	*/
	public void cacheResult(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts clsArtifacts);

	/**
	* Caches the c l s artifactses in the entity cache if it is enabled.
	*
	* @param clsArtifactses the c l s artifactses
	*/
	public void cacheResult(
		java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> clsArtifactses);

	/**
	* Creates a new c l s artifacts with the primary key. Does not add the c l s artifacts to the database.
	*
	* @param clsArtifactsPK the primary key for the new c l s artifacts
	* @return the new c l s artifacts
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts create(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSArtifactsPK clsArtifactsPK);

	/**
	* Removes the c l s artifacts with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param clsArtifactsPK the primary key of the c l s artifacts
	* @return the c l s artifacts that was removed
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a c l s artifacts with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts remove(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSArtifactsPK clsArtifactsPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException;

	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts clsArtifacts)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the c l s artifacts with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException} if it could not be found.
	*
	* @param clsArtifactsPK the primary key of the c l s artifacts
	* @return the c l s artifacts
	* @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException if a c l s artifacts with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts findByPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSArtifactsPK clsArtifactsPK)
		throws com.liferay.portal.kernel.exception.SystemException,
			it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSArtifactsException;

	/**
	* Returns the c l s artifacts with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param clsArtifactsPK the primary key of the c l s artifacts
	* @return the c l s artifacts, or <code>null</code> if a c l s artifacts with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts fetchByPrimaryKey(
		it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSArtifactsPK clsArtifactsPK)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the c l s artifactses.
	*
	* @return the c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the c l s artifactses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s artifactses
	* @param end the upper bound of the range of c l s artifactses (not inclusive)
	* @return the range of c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the c l s artifactses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSArtifactsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of c l s artifactses
	* @param end the upper bound of the range of c l s artifactses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the c l s artifactses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of c l s artifactses.
	*
	* @return the number of c l s artifactses
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}