/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CLSChallenge}.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSChallenge
 * @generated
 */
public class CLSChallengeWrapper implements CLSChallenge,
	ModelWrapper<CLSChallenge> {
	public CLSChallengeWrapper(CLSChallenge clsChallenge) {
		_clsChallenge = clsChallenge;
	}

	@Override
	public Class<?> getModelClass() {
		return CLSChallenge.class;
	}

	@Override
	public String getModelClassName() {
		return CLSChallenge.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("challengeId", getChallengeId());
		attributes.put("challengeTitle", getChallengeTitle());
		attributes.put("challengeDescription", getChallengeDescription());
		attributes.put("challengeWithReward", getChallengeWithReward());
		attributes.put("challengeReward", getChallengeReward());
		attributes.put("numberOfSelectableIdeas", getNumberOfSelectableIdeas());
		attributes.put("dateAdded", getDateAdded());
		attributes.put("challengeStatus", getChallengeStatus());
		attributes.put("status", getStatus());
		attributes.put("statusByUserId", getStatusByUserId());
		attributes.put("statusByUserName", getStatusByUserName());
		attributes.put("statusDate", getStatusDate());
		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("userId", getUserId());
		attributes.put("dateEnd", getDateEnd());
		attributes.put("dateStart", getDateStart());
		attributes.put("language", getLanguage());
		attributes.put("challengeHashTag", getChallengeHashTag());
		attributes.put("dmFolderName", getDmFolderName());
		attributes.put("idFolder", getIdFolder());
		attributes.put("representativeImgUrl", getRepresentativeImgUrl());
		attributes.put("calendarBooking", getCalendarBooking());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long challengeId = (Long)attributes.get("challengeId");

		if (challengeId != null) {
			setChallengeId(challengeId);
		}

		String challengeTitle = (String)attributes.get("challengeTitle");

		if (challengeTitle != null) {
			setChallengeTitle(challengeTitle);
		}

		String challengeDescription = (String)attributes.get(
				"challengeDescription");

		if (challengeDescription != null) {
			setChallengeDescription(challengeDescription);
		}

		Boolean challengeWithReward = (Boolean)attributes.get(
				"challengeWithReward");

		if (challengeWithReward != null) {
			setChallengeWithReward(challengeWithReward);
		}

		String challengeReward = (String)attributes.get("challengeReward");

		if (challengeReward != null) {
			setChallengeReward(challengeReward);
		}

		Integer numberOfSelectableIdeas = (Integer)attributes.get(
				"numberOfSelectableIdeas");

		if (numberOfSelectableIdeas != null) {
			setNumberOfSelectableIdeas(numberOfSelectableIdeas);
		}

		Date dateAdded = (Date)attributes.get("dateAdded");

		if (dateAdded != null) {
			setDateAdded(dateAdded);
		}

		String challengeStatus = (String)attributes.get("challengeStatus");

		if (challengeStatus != null) {
			setChallengeStatus(challengeStatus);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		Long statusByUserId = (Long)attributes.get("statusByUserId");

		if (statusByUserId != null) {
			setStatusByUserId(statusByUserId);
		}

		String statusByUserName = (String)attributes.get("statusByUserName");

		if (statusByUserName != null) {
			setStatusByUserName(statusByUserName);
		}

		Date statusDate = (Date)attributes.get("statusDate");

		if (statusDate != null) {
			setStatusDate(statusDate);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Date dateEnd = (Date)attributes.get("dateEnd");

		if (dateEnd != null) {
			setDateEnd(dateEnd);
		}

		Date dateStart = (Date)attributes.get("dateStart");

		if (dateStart != null) {
			setDateStart(dateStart);
		}

		String language = (String)attributes.get("language");

		if (language != null) {
			setLanguage(language);
		}

		String challengeHashTag = (String)attributes.get("challengeHashTag");

		if (challengeHashTag != null) {
			setChallengeHashTag(challengeHashTag);
		}

		String dmFolderName = (String)attributes.get("dmFolderName");

		if (dmFolderName != null) {
			setDmFolderName(dmFolderName);
		}

		Long idFolder = (Long)attributes.get("idFolder");

		if (idFolder != null) {
			setIdFolder(idFolder);
		}

		String representativeImgUrl = (String)attributes.get(
				"representativeImgUrl");

		if (representativeImgUrl != null) {
			setRepresentativeImgUrl(representativeImgUrl);
		}

		Long calendarBooking = (Long)attributes.get("calendarBooking");

		if (calendarBooking != null) {
			setCalendarBooking(calendarBooking);
		}
	}

	/**
	* Returns the primary key of this c l s challenge.
	*
	* @return the primary key of this c l s challenge
	*/
	@Override
	public long getPrimaryKey() {
		return _clsChallenge.getPrimaryKey();
	}

	/**
	* Sets the primary key of this c l s challenge.
	*
	* @param primaryKey the primary key of this c l s challenge
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_clsChallenge.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the uuid of this c l s challenge.
	*
	* @return the uuid of this c l s challenge
	*/
	@Override
	public java.lang.String getUuid() {
		return _clsChallenge.getUuid();
	}

	/**
	* Sets the uuid of this c l s challenge.
	*
	* @param uuid the uuid of this c l s challenge
	*/
	@Override
	public void setUuid(java.lang.String uuid) {
		_clsChallenge.setUuid(uuid);
	}

	/**
	* Returns the challenge ID of this c l s challenge.
	*
	* @return the challenge ID of this c l s challenge
	*/
	@Override
	public long getChallengeId() {
		return _clsChallenge.getChallengeId();
	}

	/**
	* Sets the challenge ID of this c l s challenge.
	*
	* @param challengeId the challenge ID of this c l s challenge
	*/
	@Override
	public void setChallengeId(long challengeId) {
		_clsChallenge.setChallengeId(challengeId);
	}

	/**
	* Returns the challenge title of this c l s challenge.
	*
	* @return the challenge title of this c l s challenge
	*/
	@Override
	public java.lang.String getChallengeTitle() {
		return _clsChallenge.getChallengeTitle();
	}

	/**
	* Sets the challenge title of this c l s challenge.
	*
	* @param challengeTitle the challenge title of this c l s challenge
	*/
	@Override
	public void setChallengeTitle(java.lang.String challengeTitle) {
		_clsChallenge.setChallengeTitle(challengeTitle);
	}

	/**
	* Returns the challenge description of this c l s challenge.
	*
	* @return the challenge description of this c l s challenge
	*/
	@Override
	public java.lang.String getChallengeDescription() {
		return _clsChallenge.getChallengeDescription();
	}

	/**
	* Sets the challenge description of this c l s challenge.
	*
	* @param challengeDescription the challenge description of this c l s challenge
	*/
	@Override
	public void setChallengeDescription(java.lang.String challengeDescription) {
		_clsChallenge.setChallengeDescription(challengeDescription);
	}

	/**
	* Returns the challenge with reward of this c l s challenge.
	*
	* @return the challenge with reward of this c l s challenge
	*/
	@Override
	public boolean getChallengeWithReward() {
		return _clsChallenge.getChallengeWithReward();
	}

	/**
	* Returns <code>true</code> if this c l s challenge is challenge with reward.
	*
	* @return <code>true</code> if this c l s challenge is challenge with reward; <code>false</code> otherwise
	*/
	@Override
	public boolean isChallengeWithReward() {
		return _clsChallenge.isChallengeWithReward();
	}

	/**
	* Sets whether this c l s challenge is challenge with reward.
	*
	* @param challengeWithReward the challenge with reward of this c l s challenge
	*/
	@Override
	public void setChallengeWithReward(boolean challengeWithReward) {
		_clsChallenge.setChallengeWithReward(challengeWithReward);
	}

	/**
	* Returns the challenge reward of this c l s challenge.
	*
	* @return the challenge reward of this c l s challenge
	*/
	@Override
	public java.lang.String getChallengeReward() {
		return _clsChallenge.getChallengeReward();
	}

	/**
	* Sets the challenge reward of this c l s challenge.
	*
	* @param challengeReward the challenge reward of this c l s challenge
	*/
	@Override
	public void setChallengeReward(java.lang.String challengeReward) {
		_clsChallenge.setChallengeReward(challengeReward);
	}

	/**
	* Returns the number of selectable ideas of this c l s challenge.
	*
	* @return the number of selectable ideas of this c l s challenge
	*/
	@Override
	public int getNumberOfSelectableIdeas() {
		return _clsChallenge.getNumberOfSelectableIdeas();
	}

	/**
	* Sets the number of selectable ideas of this c l s challenge.
	*
	* @param numberOfSelectableIdeas the number of selectable ideas of this c l s challenge
	*/
	@Override
	public void setNumberOfSelectableIdeas(int numberOfSelectableIdeas) {
		_clsChallenge.setNumberOfSelectableIdeas(numberOfSelectableIdeas);
	}

	/**
	* Returns the date added of this c l s challenge.
	*
	* @return the date added of this c l s challenge
	*/
	@Override
	public java.util.Date getDateAdded() {
		return _clsChallenge.getDateAdded();
	}

	/**
	* Sets the date added of this c l s challenge.
	*
	* @param dateAdded the date added of this c l s challenge
	*/
	@Override
	public void setDateAdded(java.util.Date dateAdded) {
		_clsChallenge.setDateAdded(dateAdded);
	}

	/**
	* Returns the challenge status of this c l s challenge.
	*
	* @return the challenge status of this c l s challenge
	*/
	@Override
	public java.lang.String getChallengeStatus() {
		return _clsChallenge.getChallengeStatus();
	}

	/**
	* Sets the challenge status of this c l s challenge.
	*
	* @param challengeStatus the challenge status of this c l s challenge
	*/
	@Override
	public void setChallengeStatus(java.lang.String challengeStatus) {
		_clsChallenge.setChallengeStatus(challengeStatus);
	}

	/**
	* Returns the status of this c l s challenge.
	*
	* @return the status of this c l s challenge
	*/
	@Override
	public int getStatus() {
		return _clsChallenge.getStatus();
	}

	/**
	* Sets the status of this c l s challenge.
	*
	* @param status the status of this c l s challenge
	*/
	@Override
	public void setStatus(int status) {
		_clsChallenge.setStatus(status);
	}

	/**
	* Returns the status by user ID of this c l s challenge.
	*
	* @return the status by user ID of this c l s challenge
	*/
	@Override
	public long getStatusByUserId() {
		return _clsChallenge.getStatusByUserId();
	}

	/**
	* Sets the status by user ID of this c l s challenge.
	*
	* @param statusByUserId the status by user ID of this c l s challenge
	*/
	@Override
	public void setStatusByUserId(long statusByUserId) {
		_clsChallenge.setStatusByUserId(statusByUserId);
	}

	/**
	* Returns the status by user uuid of this c l s challenge.
	*
	* @return the status by user uuid of this c l s challenge
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getStatusByUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsChallenge.getStatusByUserUuid();
	}

	/**
	* Sets the status by user uuid of this c l s challenge.
	*
	* @param statusByUserUuid the status by user uuid of this c l s challenge
	*/
	@Override
	public void setStatusByUserUuid(java.lang.String statusByUserUuid) {
		_clsChallenge.setStatusByUserUuid(statusByUserUuid);
	}

	/**
	* Returns the status by user name of this c l s challenge.
	*
	* @return the status by user name of this c l s challenge
	*/
	@Override
	public java.lang.String getStatusByUserName() {
		return _clsChallenge.getStatusByUserName();
	}

	/**
	* Sets the status by user name of this c l s challenge.
	*
	* @param statusByUserName the status by user name of this c l s challenge
	*/
	@Override
	public void setStatusByUserName(java.lang.String statusByUserName) {
		_clsChallenge.setStatusByUserName(statusByUserName);
	}

	/**
	* Returns the status date of this c l s challenge.
	*
	* @return the status date of this c l s challenge
	*/
	@Override
	public java.util.Date getStatusDate() {
		return _clsChallenge.getStatusDate();
	}

	/**
	* Sets the status date of this c l s challenge.
	*
	* @param statusDate the status date of this c l s challenge
	*/
	@Override
	public void setStatusDate(java.util.Date statusDate) {
		_clsChallenge.setStatusDate(statusDate);
	}

	/**
	* Returns the company ID of this c l s challenge.
	*
	* @return the company ID of this c l s challenge
	*/
	@Override
	public long getCompanyId() {
		return _clsChallenge.getCompanyId();
	}

	/**
	* Sets the company ID of this c l s challenge.
	*
	* @param companyId the company ID of this c l s challenge
	*/
	@Override
	public void setCompanyId(long companyId) {
		_clsChallenge.setCompanyId(companyId);
	}

	/**
	* Returns the group ID of this c l s challenge.
	*
	* @return the group ID of this c l s challenge
	*/
	@Override
	public long getGroupId() {
		return _clsChallenge.getGroupId();
	}

	/**
	* Sets the group ID of this c l s challenge.
	*
	* @param groupId the group ID of this c l s challenge
	*/
	@Override
	public void setGroupId(long groupId) {
		_clsChallenge.setGroupId(groupId);
	}

	/**
	* Returns the user ID of this c l s challenge.
	*
	* @return the user ID of this c l s challenge
	*/
	@Override
	public long getUserId() {
		return _clsChallenge.getUserId();
	}

	/**
	* Sets the user ID of this c l s challenge.
	*
	* @param userId the user ID of this c l s challenge
	*/
	@Override
	public void setUserId(long userId) {
		_clsChallenge.setUserId(userId);
	}

	/**
	* Returns the user uuid of this c l s challenge.
	*
	* @return the user uuid of this c l s challenge
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _clsChallenge.getUserUuid();
	}

	/**
	* Sets the user uuid of this c l s challenge.
	*
	* @param userUuid the user uuid of this c l s challenge
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_clsChallenge.setUserUuid(userUuid);
	}

	/**
	* Returns the date end of this c l s challenge.
	*
	* @return the date end of this c l s challenge
	*/
	@Override
	public java.util.Date getDateEnd() {
		return _clsChallenge.getDateEnd();
	}

	/**
	* Sets the date end of this c l s challenge.
	*
	* @param dateEnd the date end of this c l s challenge
	*/
	@Override
	public void setDateEnd(java.util.Date dateEnd) {
		_clsChallenge.setDateEnd(dateEnd);
	}

	/**
	* Returns the date start of this c l s challenge.
	*
	* @return the date start of this c l s challenge
	*/
	@Override
	public java.util.Date getDateStart() {
		return _clsChallenge.getDateStart();
	}

	/**
	* Sets the date start of this c l s challenge.
	*
	* @param dateStart the date start of this c l s challenge
	*/
	@Override
	public void setDateStart(java.util.Date dateStart) {
		_clsChallenge.setDateStart(dateStart);
	}

	/**
	* Returns the language of this c l s challenge.
	*
	* @return the language of this c l s challenge
	*/
	@Override
	public java.lang.String getLanguage() {
		return _clsChallenge.getLanguage();
	}

	/**
	* Sets the language of this c l s challenge.
	*
	* @param language the language of this c l s challenge
	*/
	@Override
	public void setLanguage(java.lang.String language) {
		_clsChallenge.setLanguage(language);
	}

	/**
	* Returns the challenge hash tag of this c l s challenge.
	*
	* @return the challenge hash tag of this c l s challenge
	*/
	@Override
	public java.lang.String getChallengeHashTag() {
		return _clsChallenge.getChallengeHashTag();
	}

	/**
	* Sets the challenge hash tag of this c l s challenge.
	*
	* @param challengeHashTag the challenge hash tag of this c l s challenge
	*/
	@Override
	public void setChallengeHashTag(java.lang.String challengeHashTag) {
		_clsChallenge.setChallengeHashTag(challengeHashTag);
	}

	/**
	* Returns the dm folder name of this c l s challenge.
	*
	* @return the dm folder name of this c l s challenge
	*/
	@Override
	public java.lang.String getDmFolderName() {
		return _clsChallenge.getDmFolderName();
	}

	/**
	* Sets the dm folder name of this c l s challenge.
	*
	* @param dmFolderName the dm folder name of this c l s challenge
	*/
	@Override
	public void setDmFolderName(java.lang.String dmFolderName) {
		_clsChallenge.setDmFolderName(dmFolderName);
	}

	/**
	* Returns the id folder of this c l s challenge.
	*
	* @return the id folder of this c l s challenge
	*/
	@Override
	public long getIdFolder() {
		return _clsChallenge.getIdFolder();
	}

	/**
	* Sets the id folder of this c l s challenge.
	*
	* @param idFolder the id folder of this c l s challenge
	*/
	@Override
	public void setIdFolder(long idFolder) {
		_clsChallenge.setIdFolder(idFolder);
	}

	/**
	* Returns the representative img url of this c l s challenge.
	*
	* @return the representative img url of this c l s challenge
	*/
	@Override
	public java.lang.String getRepresentativeImgUrl() {
		return _clsChallenge.getRepresentativeImgUrl();
	}

	/**
	* Sets the representative img url of this c l s challenge.
	*
	* @param representativeImgUrl the representative img url of this c l s challenge
	*/
	@Override
	public void setRepresentativeImgUrl(java.lang.String representativeImgUrl) {
		_clsChallenge.setRepresentativeImgUrl(representativeImgUrl);
	}

	/**
	* Returns the calendar booking of this c l s challenge.
	*
	* @return the calendar booking of this c l s challenge
	*/
	@Override
	public long getCalendarBooking() {
		return _clsChallenge.getCalendarBooking();
	}

	/**
	* Sets the calendar booking of this c l s challenge.
	*
	* @param calendarBooking the calendar booking of this c l s challenge
	*/
	@Override
	public void setCalendarBooking(long calendarBooking) {
		_clsChallenge.setCalendarBooking(calendarBooking);
	}

	/**
	* @deprecated As of 6.1.0, replaced by {@link #isApproved()}
	*/
	@Override
	public boolean getApproved() {
		return _clsChallenge.getApproved();
	}

	/**
	* Returns <code>true</code> if this c l s challenge is approved.
	*
	* @return <code>true</code> if this c l s challenge is approved; <code>false</code> otherwise
	*/
	@Override
	public boolean isApproved() {
		return _clsChallenge.isApproved();
	}

	/**
	* Returns <code>true</code> if this c l s challenge is denied.
	*
	* @return <code>true</code> if this c l s challenge is denied; <code>false</code> otherwise
	*/
	@Override
	public boolean isDenied() {
		return _clsChallenge.isDenied();
	}

	/**
	* Returns <code>true</code> if this c l s challenge is a draft.
	*
	* @return <code>true</code> if this c l s challenge is a draft; <code>false</code> otherwise
	*/
	@Override
	public boolean isDraft() {
		return _clsChallenge.isDraft();
	}

	/**
	* Returns <code>true</code> if this c l s challenge is expired.
	*
	* @return <code>true</code> if this c l s challenge is expired; <code>false</code> otherwise
	*/
	@Override
	public boolean isExpired() {
		return _clsChallenge.isExpired();
	}

	/**
	* Returns <code>true</code> if this c l s challenge is inactive.
	*
	* @return <code>true</code> if this c l s challenge is inactive; <code>false</code> otherwise
	*/
	@Override
	public boolean isInactive() {
		return _clsChallenge.isInactive();
	}

	/**
	* Returns <code>true</code> if this c l s challenge is incomplete.
	*
	* @return <code>true</code> if this c l s challenge is incomplete; <code>false</code> otherwise
	*/
	@Override
	public boolean isIncomplete() {
		return _clsChallenge.isIncomplete();
	}

	/**
	* Returns <code>true</code> if this c l s challenge is pending.
	*
	* @return <code>true</code> if this c l s challenge is pending; <code>false</code> otherwise
	*/
	@Override
	public boolean isPending() {
		return _clsChallenge.isPending();
	}

	/**
	* Returns <code>true</code> if this c l s challenge is scheduled.
	*
	* @return <code>true</code> if this c l s challenge is scheduled; <code>false</code> otherwise
	*/
	@Override
	public boolean isScheduled() {
		return _clsChallenge.isScheduled();
	}

	@Override
	public boolean isNew() {
		return _clsChallenge.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_clsChallenge.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _clsChallenge.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_clsChallenge.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _clsChallenge.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _clsChallenge.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_clsChallenge.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _clsChallenge.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_clsChallenge.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_clsChallenge.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_clsChallenge.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CLSChallengeWrapper((CLSChallenge)_clsChallenge.clone());
	}

	@Override
	public int compareTo(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge clsChallenge) {
		return _clsChallenge.compareTo(clsChallenge);
	}

	@Override
	public int hashCode() {
		return _clsChallenge.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge> toCacheModel() {
		return _clsChallenge.toCacheModel();
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge toEscapedModel() {
		return new CLSChallengeWrapper(_clsChallenge.toEscapedModel());
	}

	@Override
	public it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge toUnescapedModel() {
		return new CLSChallengeWrapper(_clsChallenge.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _clsChallenge.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _clsChallenge.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_clsChallenge.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSChallengeWrapper)) {
			return false;
		}

		CLSChallengeWrapper clsChallengeWrapper = (CLSChallengeWrapper)obj;

		if (Validator.equals(_clsChallenge, clsChallengeWrapper._clsChallenge)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public CLSChallenge getWrappedCLSChallenge() {
		return _clsChallenge;
	}

	@Override
	public CLSChallenge getWrappedModel() {
		return _clsChallenge;
	}

	@Override
	public void resetOriginalValues() {
		_clsChallenge.resetOriginalValues();
	}

	private CLSChallenge _clsChallenge;
}