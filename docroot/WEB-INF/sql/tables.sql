create table CSL_CLSArtifacts (
	ideaId LONG not null,
	artifactId LONG not null,
	primary key (ideaId, artifactId)
);

create table CSL_CLSBook (
	challengeId LONG not null primary key,
	challengeTitle VARCHAR(75) null,
	challengeDescription VARCHAR(75) null,
	dateAdded DATE null
);

create table CSL_CLSCategoriesChallenge (
	categoriesSetID LONG,
	challengeId LONG,
	type_ INTEGER,
	id_ LONG not null primary key
);

create table CSL_CLSCategoriesSet (
	categoriesSetID LONG not null primary key,
	iconName VARCHAR(75) null,
	colourName VARCHAR(75) null
);

create table CSL_CLSCategoriesSetForChallenge (
	categoriesSetID LONG not null,
	challengeId LONG not null,
	type_ INTEGER,
	primary key (categoriesSetID, challengeId)
);

create table CSL_CLSChallenge (
	uuid_ VARCHAR(75) null,
	challengeId LONG not null primary key,
	challengeTitle STRING null,
	challengeDescription TEXT null,
	challengeWithReward BOOLEAN,
	challengeReward TEXT null,
	numberOfSelectableIdeas INTEGER,
	dateAdded DATE null,
	challengeStatus VARCHAR(75) null,
	status INTEGER,
	statusByUserId LONG,
	statusByUserName VARCHAR(75) null,
	statusDate DATE null,
	companyId LONG,
	groupId LONG,
	userId LONG,
	dateEnd DATE null,
	dateStart DATE null,
	language STRING null,
	challengeHashTag VARCHAR(75) null,
	dmFolderName STRING null,
	idFolder LONG,
	representativeImgUrl STRING null,
	calendarBooking LONG
);

create table CSL_CLSChallengePoi (
	challengeId LONG,
	latitude STRING null,
	longitude STRING null,
	description TEXT null,
	poiId LONG not null primary key,
	title STRING null,
	dateAdded DATE null
);

create table CSL_CLSChallengesCalendar (
	challengesCalendarId LONG not null primary key,
	referenceCalendarId LONG
);

create table CSL_CLSCoworker (
	ideaID LONG,
	coworkerId LONG not null primary key,
	userId LONG
);

create table CSL_CLSFavouriteChallenges (
	challengeId LONG not null,
	userId LONG not null,
	primary key (challengeId, userId)
);

create table CSL_CLSFavouriteIdeas (
	ideaID LONG not null,
	userId LONG not null,
	primary key (ideaID, userId)
);

create table CSL_CLSFriendlyUrlSuffix (
	friendlyUrlSuffixID LONG not null primary key,
	UrlSuffixImsHome VARCHAR(75) null,
	UrlSuffixIdea STRING null,
	UrlSuffixNeed VARCHAR(75) null,
	UrlSuffixChallenge STRING null,
	senderNotificheMailIdeario VARCHAR(75) null,
	oggettoNotificheMailIdeario VARCHAR(75) null,
	firmaNotificheMailIdeario VARCHAR(75) null,
	utenzaMail VARCHAR(75) null,
	cdvEnabled BOOLEAN,
	cdvAddress VARCHAR(75) null,
	vcEnabled BOOLEAN,
	vcAddress VARCHAR(75) null,
	vcWSAddress VARCHAR(75) null,
	deEnabled BOOLEAN,
	deAddress VARCHAR(75) null,
	lbbEnabled BOOLEAN,
	lbbAddress VARCHAR(75) null,
	oiaAppId4lbb VARCHAR(75) null,
	tweetingEnabled BOOLEAN,
	basicAuthUser VARCHAR(75) null,
	basicAuthPwd VARCHAR(75) null,
	verboseEnabled BOOLEAN,
	mktEnabled BOOLEAN,
	emailNotificationsEnabled BOOLEAN,
	dockbarNotificationsEnabled BOOLEAN,
	jmsEnabled BOOLEAN,
	brokerJMSusername VARCHAR(75) null,
	brokerJMSpassword VARCHAR(75) null,
	brokerJMSurl VARCHAR(75) null,
	jmsTopic VARCHAR(75) null,
	pilotingEnabled BOOLEAN,
	mockEnabled BOOLEAN,
	fiwareEnabled BOOLEAN,
	fiwareRemoteCatalogueEnabled BOOLEAN,
	fiwareCatalogueAddress VARCHAR(75) null,
	needEnabled BOOLEAN,
	publicIdeasEnabled BOOLEAN,
	reducedLifecycle BOOLEAN,
	fundingBoxEnabled BOOLEAN,
	fundingBoxAddress VARCHAR(75) null,
	fundingBoxAPIAddress VARCHAR(75) null,
	googleMapsAPIKeyEnabled BOOLEAN,
	googleMapsAPIKey VARCHAR(75) null,
	graylogEnabled BOOLEAN,
	graylogAddress VARCHAR(75) null,
	virtuosityPointsEnabled BOOLEAN,
	emailOnNewChallengeEnabled BOOLEAN,
	orionUrl VARCHAR(75) null
);


create table CSL_CLSIdea (
	uuid_ STRING null,
	ideaID LONG not null primary key,
	ideaTitle STRING null,
	ideaDescription TEXT null,
	dateAdded DATE null,
	companyId LONG,
	groupId LONG,
	userId LONG,
	municipalityId LONG,
	municipalityOrganizationId LONG,
	challengeId LONG,
	takenUp BOOLEAN,
	needId LONG,
	dmFolderName STRING null,
	idFolder LONG,
	representativeImgUrl STRING null,
	isNeed BOOLEAN,
	status INTEGER,
	statusByUserId LONG,
	statusByUserName STRING null,
	statusDate DATE null,
	chatGroup LONG,
	ideaHashTag STRING null,
	ideaStatus STRING null,
	language VARCHAR(75) null,
	finalMotivation TEXT null,
	evaluationPassed BOOLEAN,
	cityName TEXT null
);

create table CSL_CLSIdeaPoi (
	ideaId LONG,
	latitude STRING null,
	longitude STRING null,
	description TEXT null,
	poiId LONG not null primary key,
	title STRING null,
	dateAdded DATE null
);

create table CSL_CLSIdeaProblemRelevant (
	uuid_ VARCHAR(75) null,
	ideaId LONG not null primary key
);

create table CSL_CLSIdeaSolutionRealistic (
	uuid_ VARCHAR(75) null,
	ideaId LONG not null primary key
);

create table CSL_CLSIdeaSolveProblem (
	uuid_ VARCHAR(75) null,
	ideaId LONG not null primary key
);

create table CSL_CLSMapProperties (
	mapPropertiesId LONG not null primary key,
	mapCenterLatitude STRING null,
	mapCenterLongitude STRING null
);

create table CSL_CLSOpLogIdea (
	opLogId LONG not null primary key,
	ideaId LONG,
	userId LONG,
	date_ DATE null,
	extraData TEXT null
);

create table CSL_CLSRequisiti (
	requisitoId LONG not null primary key,
	ideaId LONG,
	taskId LONG,
	tipo VARCHAR(75) null,
	descrizione TEXT null,
	help TEXT null,
	authorUser LONG,
	category VARCHAR(75) null,
	multiTask BOOLEAN,
	outcomeFile BOOLEAN,
	taskDoubleField BOOLEAN
);


create table CSL_CLSVmeProjects (
	recordId LONG not null primary key,
	vmeProjectId LONG,
	VmeProjectName TEXT null,
	isMockup BOOLEAN,
	ideaId LONG
);


create table CSL_EvaluationCriteria (
	criteriaId LONG not null,
	challengeId LONG not null,
	enabled BOOLEAN,
	description TEXT null,
	isBarriera BOOLEAN,
	isCustom BOOLEAN,
	language VARCHAR(75) null,
	inputId VARCHAR(75) null,
	weight DOUBLE,
	threshold INTEGER,
	date_ DATE null,
	authorId LONG,
	primary key (criteriaId, challengeId)
);

create table CSL_GESelected (
	ideaId LONG not null,
	nid VARCHAR(75) not null,
	url TEXT null,
	icon TEXT null,
	title TEXT null,
	shortDescription TEXT null,
	label TEXT null,
	rank TEXT null,
	chapter TEXT null,
	primary key (ideaId, nid)
);

create table CSL_IdeaEvaluation (
	criteriaId LONG not null,
	ideaId LONG not null,
	motivation TEXT null,
	passed BOOLEAN,
	score DOUBLE,
	date_ DATE null,
	authorId LONG,
	primary key (criteriaId, ideaId)
);

create table CSL_InvitedMail (
	ideaId LONG not null,
	mail VARCHAR(75) not null,
	primary key (ideaId, mail)
);

create table CSL_NeedLinkedChallenge (
	needId LONG not null,
	challengeId LONG not null,
	date_ DATE null,
	primary key (needId, challengeId)
);

create table CSL_VirtuosityPoints (
	challengeId LONG not null,
	position LONG not null,
	points LONG,
	dateAdded DATE null,
	primary key (challengeId, position)
);