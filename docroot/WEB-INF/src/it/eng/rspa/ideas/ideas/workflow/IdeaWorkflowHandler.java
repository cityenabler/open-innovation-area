package it.eng.rspa.ideas.ideas.workflow;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.service.impl.CLSIdeaLocalServiceImpl;

import java.io.Serializable;
import java.util.Locale;
import java.util.Map;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.workflow.BaseWorkflowHandler;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.security.permission.ResourceActionsUtil;
import com.liferay.portal.service.ServiceContext;

public class IdeaWorkflowHandler extends BaseWorkflowHandler{

	public String CLASS_NAME =CLSIdea.class.getName();
	
	@Override
	public String getClassName() {
		// TODO Auto-generated method stub
		return CLASS_NAME;
	}

	@Override
	public String getType(Locale locale) {
		// TODO Auto-generated method stub
		
		 return ResourceActionsUtil.getModelResource(locale, CLASS_NAME);
	}

	@Override
	public Object updateStatus(int status, Map<String, Serializable> workflowContext)
			throws PortalException, SystemException {

		 long userId = GetterUtil.getLong((String)workflowContext.get(WorkflowConstants.CONTEXT_USER_ID));
		 long resourcePrimKey = GetterUtil.getLong((String)workflowContext.get(WorkflowConstants.CONTEXT_ENTRY_CLASS_PK));
		 ServiceContext serviceContext = (ServiceContext)workflowContext.get("serviceContext");
		 CLSIdeaLocalServiceImpl idea = new CLSIdeaLocalServiceImpl();
		 return idea.updateStatus(userId, resourcePrimKey, status, serviceContext);
	}

}
