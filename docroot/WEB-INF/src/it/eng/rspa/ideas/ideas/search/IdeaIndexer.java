package it.eng.rspa.ideas.ideas.search;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;
import it.eng.rspa.ideas.ideas.PortletKeys;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.portlet.PortletURL;

import org.apache.commons.lang3.StringEscapeUtils;

import com.liferay.portal.kernel.search.BaseIndexer;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.DocumentImpl;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchEngineUtil;
import com.liferay.portal.kernel.search.Summary;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil;
import com.liferay.portlet.asset.service.AssetTagLocalServiceUtil;

public class IdeaIndexer extends BaseIndexer {
	
	public static final String[] CLASS_NAMES = {it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea.class.getName()};
	
	public static final String PORTLET_ID = PortletKeys.IDEAS;
	
	@Override
	public String[] getClassNames() {
		return CLASS_NAMES;
	}

	@Override
	public String getPortletId() {
		return PORTLET_ID;
	}

	@Override
	protected void doDelete(Object obj) throws Exception {		
		Set<String> seids = SearchEngineUtil.getSearchEngineIds();		
		CLSIdea idea = (CLSIdea)obj;
		Document document = new DocumentImpl();
		document.addUID(PORTLET_ID, idea.getPrimaryKey());
		SearchEngineUtil.deleteDocument(SearchEngineUtil.SYSTEM_ENGINE_ID,idea.getCompanyId(), document.get(Field.UID));    //(idea.getCompanyId(), document.get(Field.UID));
	}

	@Override
	protected Document doGetDocument(Object obj) throws Exception {
		
		CLSIdea idea = (CLSIdea)obj;
		//System.out.println("Getting idea "+idea.getIdeaTitle());
		long companyId = idea.getCompanyId();
		long groupId = getParentGroupId(idea.getGroupId());
		long scopeGroupId = idea.getGroupId();
		long userId = idea.getUserId();
		long resourcePrimKey = idea.getPrimaryKey();
		
		String title = idea.getIdeaTitle();
		
		String content = idea.getIdeaDescription();
		String regex ="\\<[^\\>]*\\>";
		String replacement = "";
		content = content.replaceAll(regex, replacement);
		String description = content;
		
		Date modifiedDate = idea.getDateAdded();
		
		long[] assetCategoryIds = AssetCategoryLocalServiceUtil.getCategoryIds(CLSIdea.class.getName(), resourcePrimKey);
		String[] assetCategoryNames = AssetCategoryLocalServiceUtil.getCategoryNames(CLSIdea.class.getName(), resourcePrimKey);
		String[] assetTagNames = AssetTagLocalServiceUtil.getTagNames(CLSIdea.class.getName(), resourcePrimKey);
		
		Document document = new DocumentImpl();
		
		document.addUID(PORTLET_ID, resourcePrimKey);
		document.addModifiedDate(modifiedDate);
		document.addKeyword(Field.COMPANY_ID, companyId);
		document.addKeyword(Field.PORTLET_ID, PORTLET_ID);
		document.addKeyword(Field.GROUP_ID, groupId);
		document.addKeyword(Field.SCOPE_GROUP_ID, scopeGroupId);
		document.addKeyword(Field.USER_ID, userId);
		document.addText(Field.TITLE, title);
		document.addText(Field.CONTENT, content);
		document.addText(Field.DESCRIPTION, description);
		document.addKeyword(Field.ASSET_CATEGORY_IDS, assetCategoryIds);
		document.addKeyword(Field.ASSET_CATEGORY_NAMES, assetCategoryNames);		
		document.addKeyword(Field.ASSET_TAG_NAMES, assetTagNames);
		document.addKeyword(Field.ENTRY_CLASS_NAME,CLSIdea.class.getName());
		document.addKeyword(Field.ENTRY_CLASS_PK, resourcePrimKey);
		
	//	System.out.println("Idea document: "+document);
		
		return document;
	}

	@Override
	protected Summary doGetSummary(Document document, Locale locale, String snippet, PortletURL portletURL) throws Exception {
		String title = document.get(Field.TITLE);
		String content = snippet;
		if (Validator.isNull(snippet)) {
			content = document.get(Field.DESCRIPTION);
			if (Validator.isNull(content)) {
				content = StringUtil.shorten(HtmlUtil.escape(document.get(Field.CONTENT)), 200); //StringUtil.shorten(document.get(Field.CONTENT), 200);
			}
			content = StringEscapeUtils.unescapeHtml4(content); //HtmlUtil.escape(content);
		}
		String resourcePrimKey = document.get(Field.ENTRY_CLASS_PK);
		portletURL.setParameter("jspPage", "html/ideas/view_idea.jsp");
		portletURL.setParameter("ideaId", resourcePrimKey);
		System.out.println(portletURL.toString());
		return new Summary(title, content, portletURL);
	}

	@Override
	protected void doReindex(Object obj) throws Exception {
		CLSIdea idea = (CLSIdea)obj;
		SearchEngineUtil.updateDocument(SearchEngineUtil.SYSTEM_ENGINE_ID, idea.getCompanyId(), getDocument(idea));
	}

	@Override
	protected void doReindex(String className, long classPK) throws Exception {
		if(className.equals(CLSIdea.class.getName())){
			CLSIdeaLocalServiceUtil.getCLSIdea(classPK);
		}

	}

	@Override
	protected void doReindex(String[] ids) throws Exception {

		
		List<CLSIdea> ideas = CLSIdeaLocalServiceUtil.getCLSIdeas(com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS, com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS);
		Iterator<CLSIdea> ideasIt = ideas.iterator();
		while (ideasIt.hasNext()) {
			CLSIdea clsIdea = (CLSIdea) ideasIt.next();
			doReindex(clsIdea);
		}

	}

	@Override
	protected String getPortletId(SearchContext searchContext) {
		return PORTLET_ID;
	}

}
