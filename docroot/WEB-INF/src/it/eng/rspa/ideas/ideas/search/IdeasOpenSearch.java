package it.eng.rspa.ideas.ideas.search;

import com.liferay.portal.kernel.search.HitsOpenSearchImpl;

public class IdeasOpenSearch extends HitsOpenSearchImpl {

	//public static final String PORTLET_ID = "Challenges_WAR_Challengesportlet";
	public static final String PORTLET_ID = "Ideas_WAR_Challenge62portlet";
	public static final String SEARCH_PATH = "/c/idea/open_search";
	public static final String TITLE = "Liferay Sample Search: ";	
	
	@Override
	public String getPortletId() {
		return PORTLET_ID;
	}

	@Override
	public String getSearchPath() {
		return SEARCH_PATH;
	}

	@Override
	public String getTitle(String keywords) {
		return TITLE + keywords;
	}

}
