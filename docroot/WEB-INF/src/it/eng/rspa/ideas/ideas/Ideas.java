package it.eng.rspa.ideas.ideas;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoi;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaImpl;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaPoiImpl;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSCoworkerLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteIdeasLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaPoiLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSOpLogIdeaLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSRequisitiLocalServiceUtil;
import it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties;
import it.eng.rspa.ideas.utils.Cdv;
import it.eng.rspa.ideas.utils.ChallengesUtils;
import it.eng.rspa.ideas.utils.DecisionEngine;
import it.eng.rspa.ideas.utils.DeleteIdeaUtils;
import it.eng.rspa.ideas.utils.FiwareUtils;
import it.eng.rspa.ideas.utils.FundingboxUtils;
import it.eng.rspa.ideas.utils.GrayLogUtils;
import it.eng.rspa.ideas.utils.IdeasListUtils;
import it.eng.rspa.ideas.utils.MyConstants;
import it.eng.rspa.ideas.utils.MyUtils;
import it.eng.rspa.ideas.utils.RestUtils;
import it.eng.rspa.ideas.utils.Tweeting;
import it.eng.rspa.ideas.utils.UpdateIdeaUtils;
import it.eng.rspa.ideas.utils.listener.IdeaLoggingNotifier;
import it.eng.rspa.ideas.utils.listener.iIdeaListener;
import it.eng.rspa.ideas.utils.model.Requirement;
import it.eng.rspa.jms.Producer;
import it.eng.rspa.jms.model.IdeaDataWrapper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.RoleConstants;
import com.liferay.portal.model.User;
import com.liferay.portal.model.UserGroup;
import com.liferay.portal.security.permission.ActionKeys;
import com.liferay.portal.service.ResourceLocalServiceUtil;
import com.liferay.portal.service.ResourcePermissionServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.service.UserGroupLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.asset.service.AssetEntryLocalServiceUtil;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLFolderLocalServiceUtil;
import com.liferay.portlet.social.service.SocialActivityLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;


public class Ideas extends MVCPortlet {
	String title="";
	
	Log logger = LogFactoryUtil.getLog(this.getClass());
	
	public static Set<iIdeaListener> ideaListeners = new HashSet<iIdeaListener>();
	
	static{
		if(IdeaManagementSystemProperties.getEnabledProperty("lbbEnabled")){
			ideaListeners.add(new IdeaLoggingNotifier());
		}
	}
	

	
///////////////////////////////////////////////////////////////////////////////////////////////////////////		
/////////////////////////////////		  	deleteIdea 			//////////////////////////////////////////	
///////////////////////////////////////////////////////////////////////////////////////////////////////////
@SuppressWarnings("unchecked")
public void deleteIdea(ActionRequest actionRequest, long ideaId) throws IOException, PortletException {

	
	if(IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")){
	
		System.out.println("Deleting...");
		
	}
	
		User user = (User) actionRequest.getAttribute(WebKeys.USER);
		ResourceBundle res = MyUtils.getResBundleViaPilotByAction(actionRequest);
		
		String lIdea = new String(res.getString("ims.idea") .getBytes("ISO-8859-1"), "UTF-8");
		String eStataCancellata = new String(res.getString("ims.deleted") .getBytes("ISO-8859-1"), "UTF-8");
		
		long id = ParamUtil.getLong(actionRequest, "ideaId");
		
		if (id <1){//if it is called in "Rigth to be forgotten"
			id = ideaId;
		}
	
		
		boolean isAuthor = IdeasListUtils.isAuthorByIdeaId(id, user);
		boolean isEntityReference = IdeasListUtils.isEntityReferencebyIdeaId(id, user);
		boolean isOmniadmin = PortalUtil.isOmniadmin(user.getUserId());
		
		boolean isEnabled = isAuthor ||  isEntityReference || isOmniadmin;
		
		if (!isEnabled){//Security issue - only the enabled user can do this action
			SessionErrors.add(actionRequest, "error");
			return;
		}
		

		try {			
			CLSIdea idea = CLSIdeaLocalServiceUtil.getCLSIdea(id);
			
			UserGroup chatGroup = UserGroupLocalServiceUtil.fetchUserGroup(idea.getChatGroup());
			
			try{
				//delete the associate files and the idea folder
				Long groupId = ParamUtil.getLong(actionRequest, "groupid");
				Long folderId = idea.getIdFolder();
				Integer count = DLFileEntryLocalServiceUtil.getFileEntriesCount(groupId, folderId);
				OrderByComparator obc = OrderByComparatorFactoryUtil.create("DLFileEntry", "title", true);
				List<DLFileEntry> files = DLFileEntryLocalServiceUtil.getFileEntries(groupId.longValue(), folderId.longValue(), 0, count, obc);
				Iterator<DLFileEntry> filesIt = files.iterator();
				while(filesIt.hasNext()){
					try{
						DLFileEntry file = filesIt.next();
						DLFileEntryLocalServiceUtil.deleteDLFileEntry(file);
					}
					catch(Exception e){ System.out.println(e.getMessage()); }
				}
			}
			catch(Exception e){ System.out.println(e.getMessage()); }
			
			try{
				//rimuovo la social activity della folder
				SocialActivityLocalServiceUtil.deleteActivities(DLFolder.class.getName(), idea.getIdFolder());
				if(IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")){
					
					System.out.println("deleteActivities completed");
				}
				
				////////////////////////rimuovo la cartella come asset
				try{ AssetEntryLocalServiceUtil.deleteEntry(DLFolder.class.getName(), idea.getIdFolder());
				}catch(Exception e){ System.out.println(e.getMessage()); }
				
				
				
				
				//rimuovo la folder associata all'idea
				DLFolderLocalServiceUtil.deleteDLFolder(idea.getIdFolder());
				
				if(IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")){
					
					System.out.println("deleteDLFolder completed");
				}
				
			}
			catch(Exception e){ System.out.println(e.getMessage()); }
			
			List<CLSCoworker> coworkers = new ArrayList<CLSCoworker>();
			
			try{
				//invio la notifica della cancellazione dell'idea a tutti gli utenti che ne sono collaboratori e li rimuovo come collaboratori			
				coworkers = CLSCoworkerLocalServiceUtil.getCoworkersByIdeaId(idea.getPrimaryKey());
				Iterator<CLSCoworker> coworkersIt = coworkers.iterator();
				while (coworkersIt.hasNext()) {
					try{
						CLSCoworker clsCoworker = (CLSCoworker) coworkersIt.next();
						
						String textMessage = lIdea +" "+ idea.getIdeaTitle() +" "+ eStataCancellata;

						long selectedUserId = clsCoworker.getUserId();
						String senderName = user.getFullName();
						
						CLSIdeaLocalServiceUtil.sendNotification(res,textMessage, selectedUserId, senderName, id, actionRequest,"delete");
						
						//rimuovo il collaboratore dalla chatGroup
						User chatUser = UserLocalServiceUtil.fetchUser(clsCoworker.getUserId());
						UserGroupLocalServiceUtil.deleteUserUserGroup(chatUser.getUserId(), chatGroup.getUserGroupId());
					}
					catch(Exception e){ System.out.println(e.getMessage()); }
				}
			}
			catch(Exception e){ System.out.println(e.getMessage()); }
			
			try{
				//rimuovo tutti i collaboratori dell'idea
				CLSCoworkerLocalServiceUtil.removeCoworkerOnIdeaDelete(id);
			}
			catch(Exception e){ System.out.println(e.getMessage()); }
			
			try{
				//invio la notifica della cancellazione dell'idea a tutti gli utenti che la hanno segnata come preferita
				List<CLSFavouriteIdeas> favouriteIdeasEntries = CLSFavouriteIdeasLocalServiceUtil.getFavouriteIdeasEntriesByIdeaId(id);
				Iterator<CLSFavouriteIdeas> favouriteIdeasEntriesIt = favouriteIdeasEntries.iterator();
				while (favouriteIdeasEntriesIt.hasNext()) {
					try{
						CLSFavouriteIdeas favouriteIdeasEntry = (CLSFavouriteIdeas) favouriteIdeasEntriesIt.next();
						
						String textMessage = lIdea +" "+ idea.getIdeaTitle() +" "+ eStataCancellata;
						long selectedUserId = favouriteIdeasEntry.getUserId();
						String senderName = user.getFullName();
						
						CLSIdeaLocalServiceUtil.sendNotification(res,textMessage, selectedUserId, senderName, id, actionRequest, "delete");
					}
					catch(Exception e){ System.out.println(e.getMessage()); }
				}
			}
			catch(Exception e){ System.out.println(e.getMessage()); }
			
			//Remove the idea as a favorite for all users that had marked as such. Then send a notification
			CLSFavouriteIdeasLocalServiceUtil.removeFavouriteIdeasOnIdeaDelete(id);
			

			DeleteIdeaUtils.deletePoiByIdeaId(id);
			
			DeleteIdeaUtils.deleteRequirementsAndTasksByIdeaId(id);
			
			DeleteIdeaUtils.deleteArtifactsAssociationByIdeaId(id);
			
			DeleteIdeaUtils.deleteVMEProjectsByIdeaId(id);
			
			if (idea.getIsNeed())
				DeleteIdeaUtils.deleteLinkWithChallengeByNeedId(id);
			
			DeleteIdeaUtils.deleteSubscriptionByIdea(idea);
			
			DeleteIdeaUtils.deleteIdeaAsAssetByIdea(idea);
			
			DeleteIdeaUtils.deleteSocialActivitiesByIdea(idea);
			
			DeleteIdeaUtils.deleteIdeaAsResourceByIdea(idea);
			
			DeleteIdeaUtils.deleteChatgroupByIdea(idea);
			
			DeleteIdeaUtils.deleteOpLogByIdeaId(id);
			
			DeleteIdeaUtils.deleteIdeaEvaluationByIdeaId(id);
			
			DeleteIdeaUtils.deleteGESelectedByIdeaId(id);
			
			DeleteIdeaUtils.deleteInvitedMailByIdeaId(id);
			
			DeleteIdeaUtils.outNotificationOnDeleteIdeaByIdeadId(idea);
			
			//////////////////////////////////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////    CDV NOTIFICATION - start
			//////////////////////////////////////////////////////////////////////////////////////////////////////
			
			
			if (IdeaManagementSystemProperties.getEnabledProperty("cdvEnabled") && !idea.isIsNeed()  ){
				
					Cdv.cdvEventNotification(idea, user.getUserId(), null, "cancellazione" ,coworkers );
			}
			
			
			//////////////////////////////////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////    CDV NOTIFICATION - end
			//////////////////////////////////////////////////////////////////////////////////////////////////////
			
			
			//////////////////////////////////////
			//destroy idea object
			//////////////////////////////////////
			try{ CLSIdeaLocalServiceUtil.deleteCLSIdea(idea); }
			catch(Exception e){ System.out.println(e.getMessage()); }
			//////////////////////////////////////
			// end - destroy idea object
			//////////////////////////////////////
			
			
			/////////////////////////////BONIFICO I TAG ////////////////////////////////
			MyUtils.bonificaTags();
			/////////////////////////////BONIFICO I TAG ////////////////////////////////
			

			
			for(iIdeaListener listener : ideaListeners){
				listener.onIdeaDelete(idea);
			}
			
			

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}





///////////////////////////////////////////////////////////////////////////////////////////////////////////		
/////////////////////////////////		  	associateChallenge 			//////////////////////////////////
///////////////////// 					associate idea to 	Challenge ///////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @param actionRequest
 * @param actionResponse
 */
public void associateChallenge (ActionRequest actionRequest,ActionResponse actionResponse) {
	
	String ideaIdS = ParamUtil.getString(actionRequest, "ideaId");
	long ideaId = Long.parseLong(ideaIdS);
	CLSIdea idea = null;
	try {
		idea = CLSIdeaLocalServiceUtil.getCLSIdea(ideaId);
	} catch (PortalException | SystemException e) {
		
		e.printStackTrace();
	}
	User user = (User) actionRequest.getAttribute(WebKeys.USER);
	boolean isEnteRiferimento = IdeasListUtils.isEntityReferencebyIdea(idea, user) ;
	boolean isAuthor = IdeasListUtils.isAuthorByIdeaId(ideaId, user);
	
	boolean isEnabled = isEnteRiferimento || isAuthor;
	
	if (!isEnabled){//Security issue - only the enabled user can do this action
		SessionErrors.add(actionRequest, "error");
		return;
	}
	
	long challengeId=ParamUtil.getLong(actionRequest, "challengeSelection");
	idea.setChallengeId(challengeId);
	idea.setIsNeed(false);
	
	//se non e' presa in carico allora l'assegno
	if (idea.getMunicipalityId() == 0){
		
		long enteId = ChallengesUtils.getAuthorIdByChallengeId(challengeId);
		idea.setMunicipalityId(enteId);
	}
	try {
		CLSIdeaLocalServiceUtil.updateCLSIdea(idea);//UPDATE the idea
	} catch (SystemException e) {
		
		e.printStackTrace();
	}
	
	ResourceBundle res = MyUtils.getResBundleViaPilotByAction(actionRequest);
	
	if (isAuthor)
		CLSIdeaLocalServiceUtil.sendNotificationForTransition(res,ideaId, "associateChallengeByAuthor", actionRequest);
	else
		CLSIdeaLocalServiceUtil.sendNotificationForTransition(res,ideaId, "associateChallengeByMunicipality", actionRequest);
	
	
	//////////////Logging BB //////////////////
	
	
	for(iIdeaListener listener : ideaListeners){
		listener.onIdeaUpdate(idea);
	}
	
	//////////////Logging BB //////////////////
	
	IdeasListUtils.redirectToIdea(actionRequest, actionResponse,ideaId);
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////		
/////////////////////////////////		  	updateIdeas 			///////////////////////////////////////	
///////////////////// 		 called both on creation and update of idea ///////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////
@SuppressWarnings("unchecked")
public long updateIdeas(ActionRequest actionRequest,ActionResponse actionResponse) 
		throws IOException,	PortletException, PortalException, com.liferay.portal.kernel.exception.SystemException {
	
		ResourceBundle res = MyUtils.getResBundleViaPilotByAction(actionRequest);
		
		User user = (User) actionRequest.getAttribute(WebKeys.USER);
		ThemeDisplay  themeDisplay= (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		
		String[] actionIdWiew =  {ActionKeys.VIEW};
		String[] actionIdRoot =  {ActionKeys.VIEW,ActionKeys.ACCESS, ActionKeys.ADD_DOCUMENT,ActionKeys.ADD_SHORTCUT,ActionKeys.ADD_SUBFOLDER,ActionKeys.UPDATE};
		final Role userRols = RoleLocalServiceUtil.getRole(user.getCompanyId(), RoleConstants.USER);		
		List<Role> allRoles = RoleLocalServiceUtil.getRoles(user.getCompanyId());
		List<Role> roles = new ArrayList<Role>(); // se voglio tutti i ruoli --> RoleLocalServiceUtil.getRoles(user.getCompanyId());				
		roles.add(userRols);
		
		ArrayList<Long> categoriesUserIdList = new ArrayList<Long>();
		long[] catsIdsLongArray = null;
		String[] tagsArrgay = null;
		
		long ideaId = ParamUtil.getLong(actionRequest, "ideaId");
		
		boolean isAuthor = false;
		if (ideaId > 0) {//only the author or coworker can edit the Idea - Security issue
			
			boolean isAuthorOrCoworker = IdeasListUtils.isAuthorOrCoworkerByIdeaId(ideaId, user);
			if (!isAuthorOrCoworker)//add error
				return-1;
			
			 isAuthor = IdeasListUtils.isAuthorByIdeaId(ideaId, user);
		}
		
		Long challengeSelection = ParamUtil.getLong(actionRequest,"challengeSelection");
		if (ideaId <0 && challengeSelection>0){//propose ideas only to enabled Challenges - Security issue
		
			boolean isChallengeWhichCanProposeIdeas = ChallengesUtils.isChallengeWhichCanProposeIdeasByChallengeIdAction(challengeSelection, actionRequest);
			if(!isChallengeWhichCanProposeIdeas)
				return-1;
		}


		boolean needReport=ParamUtil.getBoolean(actionRequest, "needReport");
		boolean isWizard=ParamUtil.getBoolean(actionRequest, "isWizard");
		
		
		
		//contiene i POI dell'idea
		HashMap<String, CLSIdeaPoi> pois = new HashMap<>();
		
		Map<String, String[]> paramMap = actionRequest.getParameterMap();
		Set<String> set = paramMap.keySet();
		Iterator<String> setIt = set.iterator();
		while (setIt.hasNext()) {
			String key = (String) setIt.next();

			
		///////////////////////////////////////////////////////////////////////////////////////////////////////////		
		///////////////////////////////Gestione dei POI - Inizio //////////////////////////////////////////////////	
		///////////////////////////////////////////////////////////////////////////////////////////////////////////
			if(key.startsWith("newPoiInput")){
				String poiIdentifier = key.substring(key.indexOf("#") + 1);
				String poiAttribute = key.substring("newPoiInput".length(),key.indexOf("_#"));
				if(pois.containsKey(poiIdentifier)){
					CLSIdeaPoi poi = pois.get(poiIdentifier);
					if(poiAttribute.equals("Description")){
						poi.setDescription(paramMap.get(key)[0]);
					}else if(poiAttribute.equals("Title")){
						poi.setTitle(paramMap.get(key)[0]);
					}else if(poiAttribute.equals("Longitude")){
						poi.setLongitude(paramMap.get(key)[0]);
					}else if(poiAttribute.equals("Latidute")){
						poi.setLatitude(paramMap.get(key)[0]);
					}
				}else{
					CLSIdeaPoi poi = new CLSIdeaPoiImpl();
					poi.setPoiId(CounterLocalServiceUtil.increment(CLSIdeaPoi.class.getName()));
					if(poiAttribute.equals("Description")){
						poi.setDescription(paramMap.get(key)[0]);
					}else if(poiAttribute.equals("Title")){
						poi.setTitle(paramMap.get(key)[0]);
					}else if(poiAttribute.equals("Longitude")){
						poi.setLongitude(paramMap.get(key)[0]);
					}else if(poiAttribute.equals("Latidute")){
						poi.setLatitude(paramMap.get(key)[0]);
					}
					pois.put(poiIdentifier, poi);
				}
			}
		///////////////////////////////////////////////////////////////////////////////////////////////////////////		
		///////////////////////////////Gestione dei POI - Fine ////////////////////////////////////////////////////	
		///////////////////////////////////////////////////////////////////////////////////////////////////////////

	
			
		///////////////////////////////////////////////////////////////////////////////////////////////////////////		
		/////////////////////////////////Gestione delle categorie - inizio ////////////////////////////////////////	
		///////////////////////////////////////////////////////////////////////////////////////////////////////////			
			
		
			
			if (needReport ){
				
				//select multiple Materialize
				
				String[] categs = ParamUtil.getParameterValues(actionRequest, "needSelectCategories");
				if(Validator.isNotNull(categs))	{
					
					for(int i=0;i<categs.length;i++){
						
						if (!categs[i].isEmpty()) 
							categoriesUserIdList.add(Long.parseLong(categs[i]));
						
					}
				}
				
				
			}else{
				
				//checkboxes AlloyUI
				
				if (key.startsWith("checkbox_category_#") && key.endsWith("#Checkbox")) {
					String categoryId = key.substring("checkbox_category_#".length());
					categoryId = categoryId.substring(0, categoryId.lastIndexOf("#Checkbox"));
					categoriesUserIdList.add(Long.parseLong(categoryId));
	
				}
				
				
			}
			
			
			
			
			//raccolgo i valori dei combobox per le categorie
			if (key.startsWith("singleChoiceTypeForCatgeroyOfSet_")) {
				String selectedCategoryIdComboBox = actionRequest.getParameter(key);
				if(!selectedCategoryIdComboBox.equalsIgnoreCase("-1")){
					categoriesUserIdList.add(Long.parseLong(selectedCategoryIdComboBox));
				}
			}
			//Gestione delle categorie - fine
		}

		// converto categoriesUserIdList (ArrayList<Long>) in catsIdsLongArray
		// (long[])
		if (!categoriesUserIdList.isEmpty()) {
			catsIdsLongArray = new long[categoriesUserIdList.size()];
			for (int count = 0; count < categoriesUserIdList.size(); count++) {
				catsIdsLongArray[count] = categoriesUserIdList.get(count).longValue();
			}
		}
        
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////////		
		/////////////////////////////////Gestione delle categorie - fine //////////////////////////////////////////	
		///////////////////////////////////////////////////////////////////////////////////////////////////////////			
		
		
		
		
		
		//long municipalityId = ParamUtil.getLong(actionRequest, "municipalityId");
		
		long municipalityOrganizationId = ParamUtil.getLong(actionRequest, "municipalityOrganizationId");

		String ideaTitle = ParamUtil.getString(actionRequest, "ideaTitle");
		String ideaDescription = ParamUtil.getString(actionRequest,"ideaDescription");
		
		
		if (needReport && isWizard){//it's the same page, I need to disambiguate with the names into the form
			
			 ideaTitle = ParamUtil.getString(actionRequest, "needTitle");
			 ideaDescription = ParamUtil.getString(actionRequest,"needDescription");	
		}
		
		String cityName = ParamUtil.getString(actionRequest, "cityName");
		
		String ideaHashTag = ParamUtil.getString(actionRequest, "ideaHashTag");
		
		Long needSelection = ParamUtil.getLong(actionRequest,"needSelection");
		String tags = ParamUtil.getString(actionRequest, "assetTagNames");		
		String challengeMunicType = ParamUtil.getString(actionRequest, "challengeMunicType");

		
		String linguaScelta = ParamUtil.getString(actionRequest, "languageChosen");
		
		String coworkersJsonString = ParamUtil.getString(actionRequest, "coworkersjson");
	
		ArrayList<Long> coworkersUserIdList = new ArrayList<Long>();
		coworkersUserIdList = UpdateIdeaUtils.coworkerStringToList(coworkersJsonString);
	
		
		//recupero l'immagine rappresentativa
		String representativeImgUrl = ParamUtil.getString(actionRequest,"representativeImgUrl");
		
						
		// converto tags in tagsArray
		if (tags.length() > 0) {
			tagsArrgay = tags.split(",");
		}
				
		CLSIdea idea = new CLSIdeaImpl();
		// set primary key

		
		boolean isNew = false;
		
		try {
			if (ideaId < 0) {
				isNew = true;
				//genero primary key
				ideaId = CounterLocalServiceUtil.increment(this.getClass().getName());
				idea.setIdeaID(ideaId);
				//idea.setLanguage(ideaLanguage);
			}else{
				// se invece e' vecchia allora recupero la idea dal db
				idea = CLSIdeaLocalServiceUtil.fetchCLSIdea(ideaId);
			}
					
			if (Validator.isNotNull(representativeImgUrl))
				idea.setRepresentativeImgUrl(representativeImgUrl);
			
			
			
			idea.setIdeaDescription(ideaDescription);	
			idea.setCityName(cityName);

			ServiceContext serviceContext = ServiceContextFactory.getInstance(actionRequest);
			Long groupId = ParamUtil.getLong(actionRequest, "groupid");

			// recupero la cartella che contiene tutte le sottocartelle ciascuna
			// per ogni idea partendo dalla root
		
			DLFolder ideaManagementFolderRoot;
			try{
				ideaManagementFolderRoot = DLFolderLocalServiceUtil.getFolder(groupId, 0, MyConstants.DL_FOLDER_ROOT);

			} catch (Exception e) {
				//questa folder non esiste la creo
				ideaManagementFolderRoot=DLFolderLocalServiceUtil.addFolder(user.getUserId(),// userId,
						ParamUtil.getLong(actionRequest, "groupid"),// groupId
						ParamUtil.getLong(actionRequest, "groupid"),// repositoryId,
						false,// mountPoint,
						0,// parentFolderId,
							// 0 e' la root. In questo caso 20406 e' la folder  ideaManagementRoot
							// che contiene le sottocartelle per ciascuna idea.
						MyConstants.DL_FOLDER_ROOT,// name,
						"",// description,
						false,
						serviceContext);// serviceContext)
				
				//List<Role> roles = RoleLocalServiceUtil.getRoles(user.getCompanyId());
				setPermission(actionRequest, Long.toString(ideaManagementFolderRoot.getFolderId()), DLFolder.class.getName(),actionIdRoot,roles, user);
				
				
			}
			// dichiaro la folder di questa idea
			DLFolder folder;
			//File folderUpload = new File(getInitParameter("uploadFolder")
			File dummyFile = File.createTempFile("dummy-file", ".dummy");
			String tempdir = dummyFile.getParent();
			dummyFile.delete();
			File folderUpload = new File(tempdir		
					+ File.separator
					+ ParamUtil.getString(actionRequest, "token"));
			
		///////////////////////////////////////////////////////////////////////////////////////////////////////////		
		/////////////////////////////// CREATION OF NEW IDEA //////////////////////////////////////////////////////		
		///////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			
			if (isNew) {
				
				idea.setGroupId(ParamUtil.getLong(actionRequest, "groupid"));
				idea.setCompanyId(user.getCompanyId());
				idea.setDateAdded(new Date());//Creation date
				idea.setIdeaTitle(ideaTitle);
				
				//If it's a new idea, set the state to Elaboration
				idea.setIdeaStatus(MyConstants.IDEA_STATE_ELABORATION);
				
				//Hashtag
				idea.setIdeaHashTag(ideaHashTag);
				
				//if it's a need
				idea.setIsNeed(needReport);
				
				// If it's a new idea, then the author is the logged-in user
				idea.setUserId(user.getUserId());
				
				String  suggests=new String(res.getString("ims.suggests") .getBytes("ISO-8859-1"), "UTF-8");
				
				//testo della mail in caso di segnalazione senza gara
				String textMessaggio = user.getFullName()  +" "+ suggests+": \""+ idea.getIdeaTitle()+"\"";
				
				String senderNome = user.getFullName();
				
				
				//se non vengo da una gara ed ho scelto la tipologia per ente
				if( challengeMunicType.equalsIgnoreCase("challengeMunicTypeMunic")){
					idea.setMunicipalityOrganizationId(municipalityOrganizationId);
					idea.setChallengeId(-1);
					
					idea.setLanguage(linguaScelta);
					
				}else{ //altrimenti in ogni caso devo scegliere la gara
					
					
					if (challengeSelection > 0){//Idea addressed to Challenge
						
						idea.setChallengeId(challengeSelection);				
						CLSChallenge challenge = CLSChallengeLocalServiceUtil.getCLSChallenge(challengeSelection);				
										
						idea.setMunicipalityId(challenge.getUserId());
						
						long orgId = MyUtils.getOrganizationIdByMunicipalityId(challenge.getUserId());
						idea.setMunicipalityOrganizationId(orgId);
						
						municipalityOrganizationId =orgId; //I use it below
						
						idea.setLanguage(challenge.getLanguage());
					}else{//Idea addressed to Need
						
						idea.setNeedId(needSelection);
						idea.setChallengeId(-1);
						CLSIdea needSel = CLSIdeaLocalServiceUtil.getIdeasByIdeaId(needSelection);
						
						long orgId=needSel.getMunicipalityOrganizationId();
						municipalityOrganizationId = orgId;
						
						idea.setMunicipalityOrganizationId(orgId);
						idea.setLanguage(needSel.getLanguage());
						
						
					}
					
				}
				
				
				
				String folderDescription = "Document for idea " + ideaTitle;
				long idFolder = 0;
				
			try{
				
				//imposto la folder
				String dmFolderName = ideaId
						+ "_"
						+ ParamUtil.getString(actionRequest, "ideaTitle")
								.replaceAll("[^\\dA-Za-z ]", "")
								.replaceAll("\\s+", "_");
				if(dmFolderName.length()>100){
					dmFolderName = dmFolderName.substring(0, 99);
				}
				
				idea.setDmFolderName(dmFolderName);
				
				
				
				// l'idea e' nuova creo la folder di questa idea in
				// ideaManagementFolderRoot
				
				
				folder = DLFolderLocalServiceUtil.addFolder(user.getUserId(),// userId,
						ParamUtil.getLong(actionRequest, "groupid"),// groupId
						ParamUtil.getLong(actionRequest, "groupid"),// repositoryId,
						false,// mountPoint,
						ideaManagementFolderRoot.getFolderId(),// parentFolderId, 0 e' la root.
																// In questo caso 20406 e' la folder ideaManagementRoot
																// che contiene le sottocartelle per ciascuna idea.
						dmFolderName,// name,
						folderDescription,// description,
						false,
						serviceContext);// serviceContext)
				
				//List<Role> roles = RoleLocalServiceUtil.getRoles(user.getCompanyId());
				setPermission(actionRequest, Long.toString(folder.getPrimaryKey()), DLFolder.class.getName(),actionIdWiew,roles, user);
		
				idea.setDmFolderName(dmFolderName);
				idea.setIdFolder(folder.getFolderId());	
				
				idFolder = folder.getFolderId();
				
			}catch(Exception e){ 
				System.out.println("updateIdeas folder exception: "+ e.getMessage()); 
			}
				
				
			try{	
				
					// dopo aver creato la folder di questa idea, faccio l'upload
					// dei files
					File[] listOfFiles = folderUpload.listFiles();
					if (listOfFiles!=null){
						for (int i = 0; i < listOfFiles.length; i++) {
							
						  try{
							
							if (listOfFiles[i].isFile()) {
								File f = listOfFiles[i];
								
								String mimeType = URLConnection.guessContentTypeFromName(f.getName());
								
								if(mimeType==null){
									mimeType = Files.probeContentType(f.toPath()); 
								}
								
								DLFileEntry dlFileEntry = DLFileEntryLocalServiceUtil.addFileEntry(
										user.getUserId(), // userId,
										ParamUtil.getLong(actionRequest, "groupid"),// groupId
										ParamUtil.getLong(actionRequest, "groupid"),// repositoryId,
										idFolder,// folderId
										f.getName(), // sourceFileName
										mimeType, // mimeType
										f.getName(), // title
										"Attached to idea " + ideaTitle, // descriptionideaDescription,// description
										"upload",// changeLog
										0,// entryTypeId
										null,
										f,// file
										new FileInputStream(f), f.length(),
										serviceContext);
		
								DLFileEntryLocalServiceUtil.updateFileEntry(user.getUserId(), // userId
										                                    dlFileEntry.getFileEntryId(), //fileEntryId,
										                                    f.getName(), // sourceFileName
										                                    mimeType, // mimeType
										                                    f.getName(), // title
										                                    "Attached to idea " + ideaTitle, // descriptionideaDescription,// description
										                                    "update status to publish", // changeLog,
										                                    true, //majorVersion,
										                                    dlFileEntry.getFileEntryTypeId(), // fileEntryTypeId,
										                                    null, //  fieldsMap,
										                                    f, // file
										                                    new FileInputStream(f), // is,
										                                    f.length(), // size,
										                                    serviceContext);
								
								//roles = RoleLocalServiceUtil.getRoles(user.getCompanyId());
								setPermission(actionRequest, Long.toString(dlFileEntry.getFileEntryId()), DLFileEntry.class.getName(),actionIdWiew,allRoles, user);
							}
						
						}catch(Exception e){ 
							System.out.println("updateIdeas folder exception: "+ e.getMessage()); 
						}	
					}
				}
								


			}
			catch(Exception e){ 
				System.out.println(e.getMessage()); 
			}	
					
					
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////Tweeting Idea///////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		
		if (IdeaManagementSystemProperties.getEnabledProperty("tweetingEnabled") ){
			
			String tipo="Idea";
			if(!idea.getIsNeed())//i Need non li twitto			
				Tweeting.twitta(themeDisplay, tipo, ideaTitle, ideaId, ideaHashTag, tagsArrgay, catsIdsLongArray, null );
		}
				
				
				
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////GESTIONE GRUPPO PER CHAT///////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
		
		try{

				//creo la chatGroup 								
				String  gName = idea.getIdeaTitle().replaceAll("[^\\p{L}\\p{Z}]","");//ripulisco da caratteri speciali
				
				//nome massimo gruppi 75 char
				if(gName.length() > 75){
					gName = gName.substring(0, 74);										
				}
				
				//controllo se gia' ci sia un gruppo con lo stesso nome
				DynamicQuery queryGruppo = DynamicQueryFactoryUtil.forClass(UserGroup.class)
											.add(PropertyFactoryUtil.forName("name").eq(gName));								
				List<UserGroup> gruppiUtente = UserGroupLocalServiceUtil.dynamicQuery(queryGruppo);
				
				String ideaIdForGroup = "_" + (idea.getIdeaID());
				
				
				//se gia' esiste metto l'id dell'idea assieme al nome
				if (gruppiUtente.size() >0){
															
					if(gName.length() + ideaIdForGroup.length() >75)
						gName = gName.substring(0, 74-ideaIdForGroup.length());
					else
						gName = gName + ideaIdForGroup;																									
				}
												
				UserGroup chatGroup = UserGroupLocalServiceUtil.addUserGroup(user.getUserId(), user.getCompanyId(), gName, "", serviceContext);
				idea.setChatGroup(chatGroup.getPrimaryKey());
				//aggiungo la chatGroup agli user group dell'autore
				UserGroupLocalServiceUtil.addUserUserGroup(user.getUserId(), chatGroup.getUserGroupId());
		
		}
		catch(Exception e){ 
			System.out.println(e.getMessage()); 
		}	
				
				
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////// FINE GESTIONE GRUPPO PER CHAT/////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////				
						
				

							
				idea = CLSIdeaLocalServiceUtil.addCLSIdea(idea);
				SocialActivityLocalServiceUtil.addActivity(user.getUserId(), ParamUtil.getLong(actionRequest, "groupid"),
						CLSIdea.class.getName(), ideaId, IdeasActivityKeys.ADD_IDEA, StringPool.BLANK, 0);
				

					
				//Creao la prima entry per il Log delle operazioni
				
				JSONObject opLogEstraData = JSONFactoryUtil.createJSONObject();
				opLogEstraData.put("operation","creation");
				
				CLSOpLogIdea opLog = CLSOpLogIdeaLocalServiceUtil.createCLSOpLogIdea(CounterLocalServiceUtil.increment(CLSOpLogIdea.class.getName()));
				opLog.setUserId( user.getUserId());
				opLog.setDate(new Date());
				opLog.setIdeaId(ideaId);
				opLog.setExtraData(opLogEstraData.toString());
				CLSOpLogIdeaLocalServiceUtil.addCLSOpLogIdea(opLog);
				
				///NOTIFICATION
				
				
				//se non vengo da una gara ed ho scelto la tipologia per ente
				if( challengeMunicType.equalsIgnoreCase("challengeMunicTypeMunic") || needSelection > 0){

					
					
					List<User> usersOrg =  UserLocalServiceUtil.getOrganizationUsers(municipalityOrganizationId);
					
					
					if (needSelection > 0){
						
						CLSIdea needSele = CLSIdeaLocalServiceUtil.getIdeasByIdeaId(needSelection);
						String needTitle = needSele.getIdeaTitle();
						
						String forTheNeed= new String(res.getString("ims.for-the-need") .getBytes("ISO-8859-1"), "UTF-8");
						textMessaggio =textMessaggio +" "+forTheNeed+" \""+needTitle+"\"";
						
						
						// if is an idea addressed to a Need, I send the notification also to the author of the need (only if they aren't equal)
						if (user.getUserId() != needSele.getUserId())
							CLSIdeaLocalServiceUtil.sendNotification(res, textMessaggio, needSele.getUserId() , senderNome, ideaId, actionRequest, "nuovaIdea");
						
						
					}
					
					//mando una notifica a tutte le org della municipalita di riferimento
					for (User userOrg: usersOrg){
						
						CLSIdeaLocalServiceUtil.sendNotification(res, textMessaggio, userOrg.getUserId(), senderNome, ideaId, actionRequest, "nuovaIdea");
					}
					
					
					
				}else{ //altrimenti in ogni caso devo scegliere la gara
					CLSChallenge challenge = CLSChallengeLocalServiceUtil.getCLSChallenge(challengeSelection);				
									
					String forTheChallenge = new String(res.getString("ims.for-the-challenge") .getBytes("ISO-8859-1"), "UTF-8");
					
					//testo della mail in caso di segnalazione senza gara
					textMessaggio =textMessaggio +" "+forTheChallenge+" \""+challenge.getChallengeTitle()+"\"";
					
					//mando una notifica alla municipalita di riferimento
					CLSIdeaLocalServiceUtil.sendNotification(res, textMessaggio, idea.getMunicipalityId(), senderNome, ideaId, actionRequest, "nuovaIdea");
				
					
				}
				
				 UpdateIdeaUtils.notifyCoworkers ( coworkersUserIdList, idea,  actionRequest,  res, user);
					
			} 
			else {
				
				///////////////////////////////////////////////////////////////////////////////////////////////////////////		
				///////////////////////////////UPDATE IDEA NON NUOVA //////////////////////////////////////////////////////		
				///////////////////////////////////////////////////////////////////////////////////////////////////////////					
				
				
			try{	
				
				// idea non nuova la folder gia' esiste
				// la recupero partendo dalla root
				folder = DLFolderLocalServiceUtil.getFolder(
						ParamUtil.getLong(actionRequest, "groupid"),// groupId
						ideaManagementFolderRoot.getFolderId(),// parent folderId,
						idea.getDmFolderName());// idea name
				File[] listOfFiles = folderUpload.listFiles();
				if (listOfFiles!=null){
					for (int i = 0; i < listOfFiles.length; i++) {
						
						try{	
						
						
						if (listOfFiles[i].isFile()) {
							File f = listOfFiles[i];
							
							String mimeType = URLConnection.guessContentTypeFromName(f.getName());
														
							if(mimeType==null){
								mimeType = Files.probeContentType(f.toPath()); 
							}
														
							DLFileEntry dlFileEntry = DLFileEntryLocalServiceUtil.addFileEntry(
									user.getUserId(), // userId,
									ParamUtil.getLong(actionRequest, "groupid"),// groupId
									ParamUtil.getLong(actionRequest, "groupid"),// repositoryId,
									folder.getFolderId(),// folderId
									f.getName(), // sourceFileName
									mimeType, // mimeType
									f.getName(), // title
									"Attached to idea " + ideaTitle, // descriptionideaDescription,// description
									"upload",// changeLog
									0,// entryTypeId
									null,
									f,// file
									new FileInputStream(f), f.length(),
									serviceContext);
							
						
								DLFileEntryLocalServiceUtil.updateFileEntry(user.getUserId(), // userId
			                            dlFileEntry.getFileEntryId(), //fileEntryId,
			                            f.getName(), // sourceFileName
			                            mimeType, // mimeType
			                            f.getName(), // title
			                            "Attached to idea" + ideaTitle, // descriptionideaDescription,// description
			                            "update status to publish", // changeLog,
			                            true, //majorVersion,
			                            dlFileEntry.getFileEntryTypeId(), // fileEntryTypeId,
			                            null, //  fieldsMap,
			                            f, // file
			                            new FileInputStream(f), // is,
			                            f.length(), // size,
			                            serviceContext);
								
								setPermission(actionRequest, Long.toString(dlFileEntry.getFileEntryId()), DLFileEntry.class.getName(),actionIdWiew,allRoles, user);
								idea.setIdFolder(folder.getFolderId());
						}
						
						}catch(Exception e){ 
							System.out.println("updateIdeas folder exception: "+ e.getMessage()); 
						}	
					}
				
				}
				
			}catch(Exception e){ 
				System.out.println("updateIdeas folder exception: "+ e.getMessage()); 
			}
				
				

				CLSIdeaLocalServiceUtil.updateCLSIdea(idea);
				SocialActivityLocalServiceUtil.addActivity(user.getUserId(), ParamUtil.getLong(actionRequest, "groupid"),
						CLSIdea.class.getName(), ideaId, IdeasActivityKeys.UPDATE_IDEA, StringPool.BLANK, 0);
								
				//per i caratteri speciali
				String theIdea = new String(res.getString("ims.idea") .getBytes("ISO-8859-1"), "UTF-8");
				String hasBeenModified = new String(res.getString("ims.modified") .getBytes("ISO-8859-1"), "UTF-8");
				
				//invio la notifica della modifica dell'idea a tutti gli utenti che la hanno segnata come preferita
				List<CLSFavouriteIdeas> favouriteIdeasEntries = CLSFavouriteIdeasLocalServiceUtil.getFavouriteIdeasEntriesByIdeaId(ideaId);
					
				for (CLSFavouriteIdeas favouriteIdeasEntry:favouriteIdeasEntries){
															
					String textMessage = theIdea +" "+ idea.getIdeaTitle() +" "+ hasBeenModified;										
					
					long selectedUserId = favouriteIdeasEntry.getUserId();
					String senderName = user.getFullName();
					
					if (selectedUserId != user.getUserId()) //no self-notitication
						CLSIdeaLocalServiceUtil.sendNotification(res,textMessage, selectedUserId, senderName, idea.getIdeaID(), actionRequest,  "modificata");
				}
				
				//invio la notifica della modifica dell'idea a tutti gli utenti che ne sono collaboratori			
				List<CLSCoworker> coworkers = CLSCoworkerLocalServiceUtil.getCoworkersByIdeaId(idea.getPrimaryKey());
					
				for(CLSCoworker clsCoworker:coworkers)	{
					
					String textMessage = theIdea +" "+ idea.getIdeaTitle() +" "+ hasBeenModified;
					long selectedUserId = clsCoworker.getUserId();
										
					String senderName = user.getFullName();
					
					if (selectedUserId != user.getUserId()) //no self-notitication
						CLSIdeaLocalServiceUtil.sendNotification(res,textMessage, selectedUserId, senderName, idea.getIdeaID(), actionRequest, "modificata");
				}
				
				//se la modifica non e' stata fatta dall'autore, allora e' stata fatta da un collaboratore; quindi gli devo inviare una notifica 
				if(isAuthor==false){
					String textMessage = theIdea +" "+ idea.getIdeaTitle() +" "+ hasBeenModified;
					long selectedUserId = idea.getUserId();
					String senderName = user.getFullName();
					CLSIdeaLocalServiceUtil.sendNotification(res,textMessage, selectedUserId, senderName, idea.getIdeaID(), actionRequest, "modificata");
				}
				
				//Creao la prima entry per il Log delle operazioni
				JSONObject opLogEstraData = JSONFactoryUtil.createJSONObject();
				opLogEstraData.put("operation","modify");
				
				CLSOpLogIdea opLog = CLSOpLogIdeaLocalServiceUtil.createCLSOpLogIdea(CounterLocalServiceUtil.increment(CLSOpLogIdea.class.getName()));
				opLog.setUserId( user.getUserId());
				opLog.setDate(new Date());
				opLog.setIdeaId(ideaId);
				opLog.setExtraData(opLogEstraData.toString());
				CLSOpLogIdeaLocalServiceUtil.addCLSOpLogIdea(opLog);
				

			}
			
				///////////////////////////////////////////////////////////////////////////////////////////////////////////		
				///////////////////////////////UPDATE IDEA NON NUOVA  FINE/////////////////////////////////////////////////		
				///////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			
			
			
			//folderUpload.delete();
			deleteDirectory(folderUpload);
			
			//Scrivo su db i poi dell'idea
			//prima di scriverli cancello tutti gia' associati se l'idea non e' nuova
			if (!isNew) {
				List<CLSIdeaPoi> oldPois = CLSIdeaPoiLocalServiceUtil.getIdeaPOIByIdeaId(ideaId);
				Iterator<CLSIdeaPoi> oldPoisIt = oldPois.iterator();
				while (oldPoisIt.hasNext()) {
					CLSIdeaPoi clsIdeaPoi = (CLSIdeaPoi) oldPoisIt.next();
					CLSIdeaPoiLocalServiceUtil.deleteCLSIdeaPoi(clsIdeaPoi);
				}
			}
			Collection<CLSIdeaPoi> poisCollection = pois.values();
			
			for (CLSIdeaPoi clsIdeaPoi:poisCollection){
				clsIdeaPoi.setIdeaId(ideaId);
				clsIdeaPoi.setDateAdded(new Date());
				CLSIdeaPoiLocalServiceUtil.addCLSIdeaPoi(clsIdeaPoi);
				
			}
			
			
			if(catsIdsLongArray==null){
				catsIdsLongArray = new long[0];
			}
			if(tagsArrgay==null){
				tagsArrgay = new String[0];
			}
			AssetEntryLocalServiceUtil.updateEntry(user.getUserId(),
					ParamUtil.getLong(actionRequest, "groupid"), CLSIdea.class.getName(),
					idea.getIdeaID(), catsIdsLongArray, tagsArrgay);
			
			if (isNew){
				ResourceLocalServiceUtil.addResources(idea.getCompanyId(), idea.getGroupId(), 
													  idea.getUserId(), CLSIdea.class.getName(), 
													  idea.getIdeaID(), false, true, true);
				
				//subscribe the author to idea comment
				IdeasListUtils.subscribeUserToIdeaComment(idea, idea.getUserId());
				
			}
			
			
			///////////////////////////////////////////////////////////////////////////////////////////////////////////		
			///////////////////////////////	Manage GenericEnabler			///////////////////////////////////////////		
			///////////////////////////////////////////////////////////////////////////////////////////////////////////
			boolean fiwareEnabled = IdeaManagementSystemProperties.getEnabledProperty("fiwareEnabled");
			
			if (fiwareEnabled){
				
				String ges=actionRequest.getParameter("selGes");
				UpdateIdeaUtils.manageGE(ideaId, ges );
				
				//update the attached PDF 
				boolean isIdeaWithSelectedGEs = FiwareUtils.isIdeaWithSelectedGEs(idea.getIdeaID());
				if (isIdeaWithSelectedGEs)
					FiwareUtils.createSavePdfByIdeaId(idea.getIdeaID(), actionRequest);
				
			}
			
			
			
			///////////////////////////////////////////////////////////////////////////////////////////////////////////		
			///////////////////////////////Gestione collaboratori - inizio/////////////////////////////////////////////		
			///////////////////////////////////////////////////////////////////////////////////////////////////////////
			//lista collaboratori vecchia
			List<CLSCoworker> coworkersListOld = CLSCoworkerLocalServiceUtil.getCoworkersByIdeaId(idea.getIdeaID());
			
			UpdateIdeaUtils.manageCoworkers( idea,  isAuthor,  isNew, coworkersUserIdList, coworkersListOld, actionRequest,  res, user);

			///////////////////////////////////////////////////////////////////////////////////////////////////////////		
			///////////////////////////////Gestione collaboratori - FINE/////////////////////////////////////////////		
			///////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			
			
			
			
			
			
			
			
			///////////////////////////////////////////////////////////////////////////////////////
			//////////////////////////		Visual Composer notifications for group 	  /////////
			///////////////////////////////////////////////////////////////////////////////////////
			UpdateIdeaUtils.notifyVC(idea, coworkersUserIdList);
			
			
			
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////Notifiche CDV///////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			UpdateIdeaUtils.notifyCDV(idea, coworkersUserIdList, user, isNew, coworkersListOld);
			
			
			//////////////////////////////////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////    Decision Engine NOTIFICATION - inizio
			//////////////////////////////////////////////////////////////////////////////////////////////////////
			
			if ( IdeaManagementSystemProperties.getEnabledProperty("deEnabled")  ){
				
				DecisionEngine.deNewUpdateIdeaNotifier(idea);
			}
			//////////////////////////////////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////    Decision Engine - fine
			//////////////////////////////////////////////////////////////////////////////////////////////////////
			
			
			
			
			///////////////////////////////////////////////////////////////////////////////////////////////////////////		
			///////////////////////////////------------ NOTIFICA JMS -------///////////////////////////////////////////		
			///////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			boolean jmsEnabled = false;
			try{ jmsEnabled = IdeaManagementSystemProperties.getEnabledProperty("jmsEnabled") ; }
			catch(Exception e){
				System.out.println(e.getMessage());
				jmsEnabled = false;
			}
			
			
			if(isNew && jmsEnabled){
				try {
					Producer sentimentProducer = new Producer(IdeaManagementSystemProperties.getProperty("jmsTopic"));
					sentimentProducer.sendIdea(new IdeaDataWrapper(ideaId));
				} 
				catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			
			for(iIdeaListener listener : ideaListeners){
				if(isNew){ listener.onIdeaPublish(idea); }
				else{ listener.onIdeaUpdate(idea); }
			}
			
			
			///////////////////////////////////////////////////////////////////////////////////////////////////////////		
			///////////////////////////////------------ GrayLog Notification -------///////////////////////////////////		
			///////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			if (IdeaManagementSystemProperties.getEnabledProperty("graylogEnabled") && isNew && needReport){
			
				GrayLogUtils.graylogNotify("NEED", idea.getIdeaID() );
			}
			
		} 
		catch (Exception e) { e.printStackTrace(); }
		
		
		
		
		return ideaId;
		
	}
	


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////				deleteFile		/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public void deleteFile(ActionRequest actionRequest,	ActionResponse actionRresponse) throws PortletException,IOException {

		Long fileEntryId = ParamUtil.getLong(actionRequest, "fileEntryId");
		try {
			DLFileEntryLocalServiceUtil.deleteDLFileEntry(fileEntryId);
		} catch (PortalException e) {
			
			e.printStackTrace();
		} catch (com.liferay.portal.kernel.exception.SystemException e) {
			
			e.printStackTrace();
		}
	
	}
	
/**
 * Delets a dir recursively deleting anything inside it.
 * @param dir The dir to delete
 * @return true if the dir was successfully deleted
 */
private boolean deleteDirectory(File dir) {
	    if(! dir.exists() || !dir.isDirectory())    {
	        return false;
	    }

	    String[] files = dir.list();
	    for(int i = 0, len = files.length; i < len; i++)    {
	        File f = new File(dir, files[i]);
	        if(f.isDirectory()) {
	            deleteDirectory(f);
	        }else   {	
	        	System.gc();//Added this part  
	            f.delete();	        	
	        }
	    }
	    return dir.delete();
	}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////			setPermission		/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

private void setPermission(ActionRequest actionRequest, String resourceId, String resourceType,String[] actionIdWiew, List<Role> roles, User user) throws com.liferay.portal.kernel.exception.SystemException, PortalException {
		//String[] actionIdsOwner =  {ActionKeys.VIEW, ActionKeys.};
		String[] actionIdAllFile =  {ActionKeys.VIEW,ActionKeys.ADD_DISCUSSION,ActionKeys.DELETE,ActionKeys.DELETE_DISCUSSION,ActionKeys.PERMISSIONS,ActionKeys.UPDATE,ActionKeys.UPDATE_DISCUSSION};
		String[] actionIdAllFolder =  {ActionKeys.VIEW,
				ActionKeys.ACCESS,
				ActionKeys.ADD_DOCUMENT,
				ActionKeys.ADD_SHORTCUT,
				ActionKeys.ADD_SUBFOLDER,
				ActionKeys.DELETE,
				ActionKeys.PERMISSIONS,
				ActionKeys.UPDATE};
		
		//List<Role> roles = RoleLocalServiceUtil.getRoles(com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS, com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS);
		Iterator<Role> rolesIt = roles.iterator();
		while(rolesIt.hasNext()){
			Role role = rolesIt.next();
			//ResourcePermissionLocalServiceUtil.setResourcePermissions(companyId, name, scope, primKey, roleId, actionIds)
			
			if(!role.getName().equals("Owner")){
				ResourcePermissionServiceUtil.setIndividualResourcePermissions(ParamUtil.getLong(actionRequest, "groupid"), //groupId
						   user.getCompanyId(), //companyId
						   resourceType, //name
						   resourceId, //primKey
						   role.getPrimaryKey(), //roleId
						   actionIdWiew);
			}
			
			final Role userRolsOwner = RoleLocalServiceUtil.getRole(user.getCompanyId(), RoleConstants.OWNER);
			
			if (resourceType.equals("com.liferay.portlet.documentlibrary.model.DLFileEntry")){
				ResourcePermissionServiceUtil.setIndividualResourcePermissions(ParamUtil.getLong(actionRequest, "groupid"), //groupId
						   user.getCompanyId(), //companyId
						   resourceType, //name
						   resourceId, //primKey
						   userRolsOwner.getPrimaryKey(), //roleId
						   actionIdAllFile);
			}else if (resourceType.equals("com.liferay.portlet.documentlibrary.model.DLFolder")){
				ResourcePermissionServiceUtil.setIndividualResourcePermissions(ParamUtil.getLong(actionRequest, "groupid"), //groupId
						   user.getCompanyId(), //companyId
						   resourceType, //name
						   resourceId, //primKey
						   userRolsOwner.getPrimaryKey(), //roleId
						   actionIdAllFolder);
			}	
		}
	}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////		takeCharge		/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * @param actionRequest
 * @param actionResponse
 */
public void takeCharge (ActionRequest actionRequest, ActionResponse actionResponse){
	
	User user = (User) actionRequest.getAttribute(WebKeys.USER);
	long ideaId = ParamUtil.getLong(actionRequest, "ideaId");
	long userId = ParamUtil.getLong(actionRequest, "userId");
	String transictionName = ParamUtil.getString(actionRequest, "transictionName");
	ThemeDisplay themeD = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	
	long orgId = MyUtils.getOrganizationIdByLeaderId(userId);
	ResourceBundle res = MyUtils.getResBundleViaPilotByAction(actionRequest);
	
	boolean isCompanyLeader = MyUtils.isCompanyLeader(user);
	boolean isIdeaTakeable = IdeasListUtils.isIdeaTakeable(ideaId);
	
	boolean isEnabled = isCompanyLeader && isIdeaTakeable;
	
	if (!isEnabled){//Security issue - only the enabled user can do this action
		SessionErrors.add(actionRequest, "error");
		return;
	}
	
	
	
	try {
		CLSIdea idea = CLSIdeaLocalServiceUtil.getCLSIdea(ideaId);
		idea.setMunicipalityOrganizationId(orgId);
		idea.setMunicipalityId(userId);
		idea.setTakenUp(true);
		idea.setIdeaStatus(MyConstants.IDEA_STATE_REFINEMENT);
		
		//to keep track of the activities of refinement not concluded yet
		idea.setStatus(0);
		
		//these 3 sets only for any utility - no current specific use
		idea.setStatusByUserId(themeD.getUserId());
		idea.setStatusByUserName(themeD.getUser().getFullName());
		idea.setStatusDate(new Date());
		
		CLSIdeaLocalServiceUtil.updateCLSIdea(idea);
		
		
	} catch (PortalException | SystemException e) {
		
		e.printStackTrace();
	}
	
	
	int numrequisitifunzionali = ParamUtil.getInteger(actionRequest,"numFunzionali");
	
	for(int i=0 ;i<numrequisitifunzionali;i++){
		
		String testoRequisito =ParamUtil.getString(actionRequest,"testorequisitifunzionali"+(i+1));
		if(!testoRequisito.isEmpty()){
			
			try {
				long reqId = CounterLocalServiceUtil.increment(CLSRequisiti.class.getName());
				CLSRequisiti requisito=CLSRequisitiLocalServiceUtil.createCLSRequisiti(reqId);
				requisito.setIdeaId(ideaId);
				requisito.setDescrizione(testoRequisito);
				requisito.setTipo("funzionale");
				requisito.setAuthorUser(themeD.getUserId());
				CLSRequisitiLocalServiceUtil.updateCLSRequisiti(requisito);
			} catch (SystemException e) {
				e.printStackTrace();
			}

		}
	}
	
	CLSIdeaLocalServiceUtil.sendNotificationForTransition(res,ideaId, transictionName, actionRequest);
	IdeasListUtils.redirectToIdea(actionRequest, actionResponse, ideaId);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////		fbRefinement		/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
* @param actionRequest
* @param actionResponse
*/
public void fbRefinement (ActionRequest actionRequest, ActionResponse actionResponse){

	User user = (User) actionRequest.getAttribute(WebKeys.USER);
	long ideaId = ParamUtil.getLong(actionRequest, "ideaId");
	String transictionName = ParamUtil.getString(actionRequest, "transictionName");
	ThemeDisplay themeD = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	
	ResourceBundle res = MyUtils.getResBundleViaPilotByAction(actionRequest);
	
	boolean isAuthor = IdeasListUtils.isAuthorByIdeaId(ideaId, user);
	
	
	CLSIdea idea=null;
	try {
		idea = CLSIdeaLocalServiceUtil.getCLSIdea(ideaId);
	} catch (PortalException | SystemException e1) {
		e1.printStackTrace();
	}
	
	if (idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_REFINEMENT)){//Security issue - only for avoid action after back on browser
		SessionErrors.add(actionRequest, "error");
		return;
	}
	
	
	if (!isAuthor){//Security issue - only the enabled user can do this action
		SessionErrors.add(actionRequest, "error");
		return;
	}
	
	
		
		idea.setIdeaStatus(MyConstants.IDEA_STATE_REFINEMENT);
		String cityName = ParamUtil.getString(actionRequest, "cityName");
		
		if (!cityName.isEmpty())
			idea.setCityName(cityName);
		
		
		//to keep track of the activities of refinement not concluded yet
		idea.setStatus(0);
		
		//these 3 sets only for any utility - no current specific use
		idea.setStatusByUserId(themeD.getUserId());
		idea.setStatusByUserName(themeD.getUser().getFullName());
		idea.setStatusDate(new Date());
		
		try {
			CLSIdeaLocalServiceUtil.updateCLSIdea(idea);
		} catch (SystemException e1) {
			e1.printStackTrace();
		}
	
	

	
	List<Requirement> requirements = new ArrayList<Requirement>();
	
	Requirement r1 = new Requirement();
	r1.setDescription("Challenges and solutions");
	r1.setCategory("Project information");
	r1.setMultiTask(true);
	r1.setOutcomeFile(false);
	r1.setTaskDoubleField(true);
	r1.setHelp("Describe the challenges cities/communities are facing that the team is addressing and describe the proposed solutions.");
	requirements.add(r1);
	
	Requirement r2 = new Requirement();
	r2.setDescription("Setup and configuration");
	r2.setCategory("Project information");
	r2.setMultiTask(false);
	r2.setOutcomeFile(false);
	r2.setTaskDoubleField(false);
	requirements.add(r2);
	
	Requirement r3 = new Requirement();
	r3.setDescription("Technical feasibility analysis");
	r3.setCategory("Project information");
	r3.setMultiTask(false);
	r3.setOutcomeFile(false);
	r3.setTaskDoubleField(false);
	requirements.add(r3);
	
	Requirement r4 = new Requirement();
	r4.setDescription("Hardware and network diagrams");
	r4.setCategory("Project information");
	r4.setMultiTask(true);
	r4.setOutcomeFile(true);
	r4.setTaskDoubleField(false);
	requirements.add(r4);
	
	Requirement r5 = new Requirement();
	r5.setDescription("Major requirements");
	r5.setCategory("Project approach");
	r5.setMultiTask(false);
	r5.setOutcomeFile(false);
	r5.setTaskDoubleField(false);
	requirements.add(r5);
	
	Requirement r6 = new Requirement();
	r6.setDescription("KPI measurement methods");
	r6.setCategory("Project approach");
	r6.setMultiTask(true);
	r6.setOutcomeFile(false);
	r6.setTaskDoubleField(true);
	r6.setHelp("Describe the methods to measure the performance/KPI impact to assess the benefits for the citizens/communities.");
	requirements.add(r6);
	
	Requirement r7 = new Requirement();
	r7.setDescription("Standars interoperability");
	r7.setCategory("Project approach");
	r7.setMultiTask(false);
	r7.setOutcomeFile(false);
	r7.setTaskDoubleField(false);
	r7.setHelp("Describe how the project could employ interoperable and standards-based concepts.");
	requirements.add(r7);
	
	Requirement r8 = new Requirement();
	r8.setDescription("Replicability scalability and sustainability");
	r8.setCategory("Project approach");
	r8.setMultiTask(false);
	r8.setOutcomeFile(false);
	r8.setTaskDoubleField(false);
	r8.setHelp("Describe how the project could ensure replicability, scalability and sustainability of the operation.");
	requirements.add(r8);
	
	Requirement r9 = new Requirement();
	r9.setDescription("Demostration deployment phases");
	r9.setCategory("Project approach");
	r9.setMultiTask(false);
	r9.setOutcomeFile(false);
	r9.setTaskDoubleField(false);
	r9.setHelp("Describe briefly the development and deployment stages of the project.");
	requirements.add(r9);
	
	Requirement r10 = new Requirement();
	r10.setDescription("Test plan and results");
	r10.setCategory("Project approach");
	r10.setMultiTask(false);
	r10.setOutcomeFile(false);
	r10.setTaskDoubleField(false);
	r10.setHelp("Describe briefly the testing and evaluation methodology, and the main results expected.");
	requirements.add(r10);
	
	Requirement r11 = new Requirement();
	r11.setDescription("Socio economic and societal impact");
	r11.setCategory("Project impact");
	r11.setMultiTask(false);
	r11.setOutcomeFile(false);
	r11.setTaskDoubleField(false);
	r11.setHelp("Describe the anticipated impacts of the project in terms of socio-economic and societal parameters.");
	requirements.add(r11);
	
	Requirement r12 = new Requirement();
	r12.setDescription("Differentation");
	r12.setCategory("Project impact");
	r12.setMultiTask(false);
	r12.setOutcomeFile(false);
	r12.setTaskDoubleField(false);
	r12.setHelp("Describe briefly what is the key differentiating aspect of this project in relation to the value it adds to the intended deployment environment.");
	requirements.add(r12);
	
	Requirement r13 = new Requirement();
	r13.setDescription("Benefits");
	r13.setCategory("Project impact");
	r13.setMultiTask(false);
	r13.setOutcomeFile(false);
	r13.setTaskDoubleField(false);
	r13.setHelp("Describe the anticipated economic benefits, as well as impacts on energy, health, safety, environment, or other quality of life aspects.");
	requirements.add(r13);
	
	Requirement r14 = new Requirement();
	r14.setDescription("Others");
	r14.setCategory("Project impact");
	r14.setMultiTask(false);
	r14.setOutcomeFile(false);
	r14.setTaskDoubleField(false);
	r14.setHelp("e.g. Status of Commitment, Status of Development, Affordability, Investment made by the applicant team in addition to the prize awarded by FIWARE, Risk Analysis...");
	requirements.add(r14);
	
	Requirement r15 = new Requirement();
	r15.setDescription("Partners");
	r15.setCategory("Project team");
	r15.setMultiTask(true);
	r15.setOutcomeFile(false);
	r15.setTaskDoubleField(false);
	requirements.add(r15);

	
	
	
	
	for(Requirement req: requirements){
	
		try {
			long reqId = CounterLocalServiceUtil.increment(CLSRequisiti.class.getName());
			CLSRequisiti requisito=CLSRequisitiLocalServiceUtil.createCLSRequisiti(reqId);
				requisito.setIdeaId(ideaId);
				requisito.setDescrizione(req.getDescription());
				requisito.setCategory(req.getCategory());
				requisito.setMultiTask(req.getMultiTask());
				requisito.setOutcomeFile(req.isOutcomeFile());
				requisito.setHelp(req.getHelp());
				requisito.setTaskDoubleField(req.isTaskDoubleField());
				requisito.setAuthorUser(themeD.getUserId());
			CLSRequisitiLocalServiceUtil.updateCLSRequisiti(requisito);
		} catch (SystemException e) {
			e.printStackTrace();
		}
	
	
}


CLSIdeaLocalServiceUtil.sendNotificationForTransition(res,ideaId, transictionName, actionRequest);
IdeasListUtils.redirectToIdea(actionRequest, actionResponse, ideaId);
}





/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////		addCoworkers    /////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/**
 * @param actionRequest
 * @param actionResponse
 * @throws SystemException 
 */
public void addCoworkers (ActionRequest actionRequest, ActionResponse actionResponse) throws SystemException{
	
	
	long ideaId = ParamUtil.getLong(actionRequest, "ideaId");
	User user = (User) actionRequest.getAttribute(WebKeys.USER);
	
	ResourceBundle res = MyUtils.getResBundleViaPilotByAction(actionRequest);
	
	CLSIdea idea = null;
	try {
		idea = CLSIdeaLocalServiceUtil.getIdeasByIdeaId(ideaId);
	} catch (SystemException e) {
		e.printStackTrace();
	}
	
	boolean isTakenUp = idea.isTakenUp();
	boolean isEnteRiferimento = IdeasListUtils.isEntityReferencebyIdea(idea, user) ;
	boolean isAuthor = IdeasListUtils.isAuthorByIdeaId(ideaId, user);
	
	boolean isEnabled = (isTakenUp && isEnteRiferimento) || isAuthor;
	
	if (!isEnabled){//Security issue - only the enabled user can do this action
		SessionErrors.add(actionRequest, "error");
		return;
	}
	
	String coworkersJsonString = ParamUtil.getString(actionRequest, "coworkersjson");
	
	ArrayList<Long> coworkersUserIdList = new ArrayList<Long>();
	coworkersUserIdList = UpdateIdeaUtils.coworkerStringToList(coworkersJsonString);
	
	//Old coworker list, before the current change
	List<CLSCoworker> coworkersListOld = CLSCoworkerLocalServiceUtil.getCoworkersByIdeaId(idea.getIdeaID());
	
	
	if (!isAuthor){ //if it is the companyLeader
		//Protect from editing the collborators who are not employees of the company, otherwise they would be lost
		for (CLSCoworker cow:coworkersListOld){
			boolean isUserOfMyCompany =   UpdateIdeaUtils.isUserOfCompanyLeader(user, cow.getUserId());
			
			if (!isUserOfMyCompany)
				coworkersUserIdList.add(cow.getUserId());
		}
	}
	
	
	UpdateIdeaUtils.manageCoworkers( idea,  isAuthor,  false, coworkersUserIdList, coworkersListOld, actionRequest,  res, user);

	UpdateIdeaUtils.notifyVC(idea, coworkersUserIdList);
	
	UpdateIdeaUtils.notifyCDV(idea, coworkersUserIdList, user, false, coworkersListOld);
	
	IdeasListUtils.redirectToIdea(actionRequest, actionResponse, ideaId);
}

/** 
 * @param actionRequest
 * @param actionResponse
 */
public void sendIdeaToFundingBox (ActionRequest actionRequest,ActionResponse actionResponse) {
	
	
	
	long ideaId = ParamUtil.getLong(actionRequest, "ideaId");
	User user = (User) actionRequest.getAttribute(WebKeys.USER);
	
	FiwareUtils.createSavePdfByIdeaId(ideaId, actionRequest); 
	
	String fundingBoxAPIAddress = IdeaManagementSystemProperties.getProperty("fundingBoxAPIAddress");
	
	JSONObject jOIdea =  FundingboxUtils.createJsonIdeaFundingbox(ideaId);
		
	
	//{"opencall":"fiware-gctc","data":{"test":"test","test2":"test2"}}
	JSONObject jsonToSend = JSONFactoryUtil.createJSONObject();
		jsonToSend.put("opencall", "fiware-gctc");
		jsonToSend.put("data", jOIdea);
	
	String userToken =user.getExpandoBridge().getAttribute("token").toString();
	
	
	if(IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")){
		System.out.println("jsonToSend "+jsonToSend);
	}
	
	
	String resp="";
	try {
	  	 resp = RestUtils.consumePostWsToKen(jsonToSend,fundingBoxAPIAddress, userToken ); 
	} catch (Exception e) {
			e.printStackTrace();
		}
		
	
	try {
		CLSIdea idea = CLSIdeaLocalServiceUtil.getCLSIdea(ideaId);
		idea.setIdeaStatus(MyConstants.IDEA_STATE_COMPLETED);
		
		//these 3 sets only for any utility - no current specific use
		idea.setStatusByUserId(user.getUserId());
		idea.setStatusByUserName(user.getFullName());
		idea.setStatusDate(new Date());
		
		CLSIdeaLocalServiceUtil.updateCLSIdea(idea);
		
		
	} catch (PortalException | SystemException e) {
		
		e.printStackTrace();
	}
	
	
	try {
		actionResponse.sendRedirect(FundingboxUtils.FUNDINGBOX_APPLY_URL);
	} catch (IOException e) {
		e.printStackTrace();
	}
	
	
}

}
