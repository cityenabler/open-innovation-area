package it.eng.rspa.ideas.ideas;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties;

import java.util.Locale;

import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.portlet.LiferayPortletRequest;
import com.liferay.portal.kernel.portlet.LiferayPortletResponse;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portlet.asset.model.BaseAssetRenderer;

public class IdeaAssetRenderer extends BaseAssetRenderer {
	
	CLSIdea idea = null;
	
	public IdeaAssetRenderer(CLSIdea idea){
		this.idea = idea;
	}

	@Override
	public String getURLViewInContext(
			LiferayPortletRequest liferayPortletRequest,
			LiferayPortletResponse liferayPortletResponse,
			String noSuchEntryRedirect) throws Exception {
		
		IdeaManagementSystemProperties imsp = new IdeaManagementSystemProperties();
		return imsp.getFriendlyUrlIdeas(idea.getIdeaID());
		
		//return getURLViewInContext(liferayPortletRequest, noSuchEntryRedirect, "/html/ideas/view_idea.jsp", "ideaId", idea.getIdeaID());
	}
	
//	@Override
//	public String getUrlTitle() {
//		return  "/html/ideas/view_idea.jsp";
//	}
	
	@Override
	public String getClassName() {
		return CLSIdea.class.getName();
	}

	@Override
	public long getClassPK() {
		return idea.getPrimaryKey();
	}

	@Override
	public long getGroupId() {
		return idea.getGroupId();
	}

	@Override
	public String getSummary(Locale locale) {
		String summary = idea.getIdeaDescription();
		
		String regex ="\\<[^\\>]*\\>";
		String replacement = "";
		summary = summary.replaceAll(regex, replacement);
		
		if(summary!=null){
			summary = StringUtil.shorten(HtmlUtil.stripHtml(summary), 200);
		}else{
			summary="";
		}
		
		return summary;
	}

	@Override
	public String getTitle(Locale locale) {
		return idea.getIdeaTitle();
	}

	@Override
	public long getUserId() {
		return idea.getUserId();
	}

	@Override
	public String getUserName() {
		try {
			return UserLocalServiceUtil.fetchUser(idea.getUserId()).getFullName();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}//null;
		return null;
	}

	@Override
	public String getUuid() {
		return idea.getUuid();
	}

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse, String template) throws Exception {
		
		renderRequest.setAttribute("idea", this.idea);
//		String page = "/html/ideas/abstract.jsp";
//		
//		 if (template.equals(TEMPLATE_FULL_CONTENT)) {
//			return "/html/ideas/full_content.jsp";
//		}
		
		 
		 String page = "/html/ideasexplorermaterial/ideasdetails/view.jsp";
		 
		return page;
	}
	
	@Override
	public String getIconPath(PortletRequest portletRequest) {
		// TODO Auto-generated method stub
		return "/Challenge62-portlet/img/icon/ideasImg.png";///super.getIconPath(portletRequest);
	}

//	@Override
//	public boolean hasViewPermission(PermissionChecker permissionChecker) {
//		return true;
//	}
}
