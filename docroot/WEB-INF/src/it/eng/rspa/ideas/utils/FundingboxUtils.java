package it.eng.rspa.ideas.utils;

import java.util.List;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.tasks.model.TasksDetails;

public class FundingboxUtils {
	
	public static final String FUNDINGBOX_APPLY_URL = "http://fiware-gctc.fundingbox.com/apply";
	
	
	/**
	 * @param ideaId
	 * @return
	 */
	public static JSONObject createJsonIdeaFundingbox(long ideaId){
		
		JSONObject jObj = JSONFactoryUtil.createJSONObject();
		
		CLSIdea idea=null;
		try {
			idea = CLSIdeaLocalServiceUtil.getIdeasByIdeaId(ideaId);
		} catch (SystemException e) {
			
			e.printStackTrace();
			return jObj;
		}
		
		
		
	/*	
		{
		       "project" : {
		        "title" : "String",
		        "description" : "Long string",
		        "challenges_and_solutions" : [ 
		            {
		                "challenge" : "String",
		                "solution" : "String"
		            }, 
		            {
		                "challenge" : "String",
		                "solution" : "String"
		            }
		        ],
		        "setup_and_configuration" : "String",
		        "technical_feasibility_analysis" : "String",
		        "hardware_and_network_diagrams" : [ 
		            {
		                "src" : "https://fundingbox-files.s3-eu-central-1.amazonaws.com/xurxo/zjH4Sw6n3a/file.txt",
		                "filename" : "file.txt",
		                "type" : "application/postscript",
		                "size" : 361735
		            }
		        ]
		    },
		    "approach" : {
		        "major_requeriments" : "Long string",
		        "kpi_measurent_methods" : [ 
		            {
		                "KPI" : "String",
		                "measurent_method" : "String"
		            }, 
		            {
		                "KPI" : "String",
		                "measurent_method" : "String"
		            }
		        ],
		        "standars_interoperability" : "Long string",
		        "replicability_scalability_and_sustainability" : "Long string",
		        "demostration_deployment_phases" : "Long string",
		        "test_plan_and_results" : "Long string"
		    },
		    "impact" : {
		        "socio_economic_and_societal_impact" : "Long string",
		        "benefits" : "sdfsdf",
		        "differentiation" : "Long string",
		        "others" : "Long string"
		    },
		    "team" : {
		        "name_team_lead" : "String",
		        "email_team_lead" : "email@mail.com",
		        "cities_and_members" : [ 
		            {
		                "city_name" : "String",
		                "member_name" : "String",
		                "member_email" : "email@mail.com"
		            }, 
		            {
		                "city_name" : "String",
		                "member_name" : "String",
		                "member_email" : "email@mail.com"
		            }
		        ],
		        "partners" : [ 
		            "String", 
		            "String", 
		            "String"
		        ]
		    }
		}
*/
		
		
		
		JSONObject jProject = JSONFactoryUtil.createJSONObject();
			jProject.put("title", idea.getIdeaTitle());
			jProject.put("description", IdeasListUtils.getSocialSummaryByIdeaNotLimited(idea));
			
			JSONArray jaChallengeSol = JSONFactoryUtil.createJSONArray();
			
			
			List<TasksDetails> tasksDetails1 = TaskUtils.getSolutionsByIdeaReq(idea, "Challenges and solutions");
    		for (TasksDetails td:tasksDetails1){			    
     
    			JSONObject joChallengeSol = JSONFactoryUtil.createJSONObject();
    				joChallengeSol.put("challenge" , td.getDescription().replaceAll("[\\r\\n]", "") );
    				joChallengeSol.put("solution", td.getSolution().replaceAll("[\\r\\n]", "") );
    			
    				jaChallengeSol.put(joChallengeSol);
    		}	    
			jProject.put("challenges_and_solutions",jaChallengeSol );
			
			jProject.put("setup_and_configuration", TaskUtils.getSolutionByIdeaReq(idea, "Setup and configuration"));
			jProject.put("technical_feasibility_analysis", TaskUtils.getSolutionByIdeaReq(idea, "Technical feasibility analysis"));
			
			JSONArray jaHW = JSONFactoryUtil.createJSONArray();
			List<TasksDetails> tasksDetails2 = TaskUtils.getSolutionsByIdeaReq(idea, "Hardware and network diagrams");
    		for (TasksDetails td:tasksDetails2){			    
     
    			JSONObject joHW = TaskUtils.getFileOutcomeByTaskDetails(td);
    			jaHW.put(joHW);
    		}
    		
    		boolean isIdeaWithSelectedGEs = FiwareUtils.isIdeaWithSelectedGEs(ideaId);
			if (isIdeaWithSelectedGEs){
	    		JSONObject joHWPdf = FiwareUtils.getFilePdfGE(ideaId);
	    		jaHW.put(joHWPdf);
			}
    		
			jProject.put("hardware_and_network_diagrams",jaHW );
			
		jObj.put("project", jProject);
		
		
		JSONObject jApproach = JSONFactoryUtil.createJSONObject();
			jApproach.put("major_requeriments", TaskUtils.getSolutionByIdeaReq(idea, "Major requirements").replaceAll("[\\r\\n]", ""));
		
			JSONArray jaKPI = JSONFactoryUtil.createJSONArray();
			
			
			List<TasksDetails> tasksDetails3 = TaskUtils.getSolutionsByIdeaReq(idea, "KPI measurement methods");
    		for (TasksDetails td:tasksDetails3){			    
     
    			JSONObject joKPI = JSONFactoryUtil.createJSONObject();
    				joKPI.put("KPI" , td.getDescription().replaceAll("[\\r\\n]", ""));
    				joKPI.put("measurent_method", td.getSolution().replaceAll("[\\r\\n]", ""));
    			
    				jaKPI.put(joKPI);
    		}	
    		jApproach.put("kpi_measurent_methods",jaKPI);
    		
    		jApproach.put("standars_interoperability", TaskUtils.getSolutionByIdeaReq(idea, "Standars interoperability").replaceAll("[\\r\\n]", ""));
    		jApproach.put("replicability_scalability_and_sustainability", TaskUtils.getSolutionByIdeaReq(idea, "Replicability scalability and sustainability").replaceAll("[\\r\\n]", ""));
    		jApproach.put("demostration_deployment_phases", TaskUtils.getSolutionByIdeaReq(idea, "Demostration deployment phases").replaceAll("[\\r\\n]", ""));
    		jApproach.put("test_plan_and_results", TaskUtils.getSolutionByIdeaReq(idea, "Test plan and results").replaceAll("[\\r\\n]", ""));
		jObj.put("approach", jApproach);
		
		JSONObject jImpact = JSONFactoryUtil.createJSONObject();
			jImpact.put("socio_economic_and_societal_impact", TaskUtils.getSolutionByIdeaReq(idea, "Socio economic and societal impact").replaceAll("[\\r\\n]", ""));
			jImpact.put("differentiation", TaskUtils.getSolutionByIdeaReq(idea, "Differentation").replaceAll("[\\r\\n]", ""));
			jImpact.put("others", TaskUtils.getSolutionByIdeaReq(idea, "Others").replaceAll("[\\r\\n]", ""));
			jImpact.put("benefits", TaskUtils.getSolutionByIdeaReq(idea, "Benefits").replaceAll("[\\r\\n]", ""));
		jObj.put("impact", jImpact);
		
		JSONObject jTeam = JSONFactoryUtil.createJSONObject();
			jTeam.put("name_team_lead", IdeasListUtils.getAuthorNameByIdea(idea));
			jTeam.put("email_team_lead", IdeasListUtils.getAuthorEmailByIdea(idea));
		
		
		JSONArray jaCitiesMembers = IdeasListUtils.getCoworkewsByIdea(idea);
		jTeam.put("cities_and_members", jaCitiesMembers);
		
		List<TasksDetails> tasksDetails4 = TaskUtils.getSolutionsByIdeaReq(idea, "Partners");
		JSONArray jaPartner = JSONFactoryUtil.createJSONArray();
		for (TasksDetails td:tasksDetails4)			    
			jaPartner.put(td.getDescription().replaceAll("[\\r\\n]", ""));
		
		jTeam.put("partners", jaPartner);
		
		jObj.put("team", jTeam);
		
		return jObj;
		
	}
	

}
