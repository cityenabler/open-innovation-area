package it.eng.rspa.ideas.utils;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.model.GESelected;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.GESelectedLocalServiceUtil;
import it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.MimetypesFileTypeMap;
import javax.portlet.ActionRequest;
import javax.portlet.RenderRequest;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class FiwareUtils {
	
	
	public static final String FIRST_PART_FIWARE_CATALOGUE_ADDRESS = "https://catalogue.fiware.org";
	public static final String DEPRECATED_GEIS = "Deprecated GEis";
	public static final String FIWARE_GEIS = "FIWARE GEis";
	public static final String FIWARE_GERIS = "FIWARE GEris";
	public static final String INCUBATED_GES_GERIS = "Incubated GEs/GEris";
	public static final String PDF_FILENAME = "FIA_Fiware_GE.pdf";
	
	
	/**
	 * @return
	 */
	public static JSONArray getAllGenericEnablers(){
		
		boolean fiwareRemoteCatalogueEnabled = IdeaManagementSystemProperties.getEnabledProperty("fiwareRemoteCatalogueEnabled");
		String fiwareCatalogueAddress = IdeaManagementSystemProperties.getProperty("fiwareCatalogueAddress");
		
		String resp="";
		
		if (fiwareRemoteCatalogueEnabled){
			try {
				resp =RestUtils.consumeGetWs(fiwareCatalogueAddress);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{//mocked response		
			resp = "[{\"short description\":\"The Trustworthy Factory help the developer to create trusted applications\",\"title\":\"<a href=\\\"/enablers/trustworthy-factory\\\"> Trustworthy Factory</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/security\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Security</a>\",\"rank\":\"<a href=\\\"/support-ranks/deprecated-geis\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Deprecated GEis</a>\",\"bundle short description\":null,\"nid\":\"1488\",\"type\":\"Enabler\"},{\"short description\":\"Support for advanced, Web-based, highly dynamic, and potential 3D user interfaces.\",\"title\":\"<a href=\\\"/enablers/2d-ui\\\">2D-UI</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/meshmoon-163x163_1.png\\\" width=\\\"163\\\" height=\\\"163\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/advanced-web-based-user-interface\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Advanced Web-based User Interface</a>\",\"rank\":\"<a href=\\\"/support-ranks/deprecated-geis\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Deprecated GEis</a>\",\"bundle short description\":null,\"nid\":\"1304\",\"type\":\"Enabler\"},{\"short description\":\"2D/3D Capture\",\"title\":\"<a href=\\\"/enablers/2d3d-capture\\\">2D/3D Capture</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/advanced-web-based-user-interface\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Advanced Web-based User Interface</a>\",\"rank\":\"<a href=\\\"/support-ranks/deprecated-geis\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Deprecated GEis</a>\",\"bundle short description\":null,\"nid\":\"1257\",\"type\":\"Enabler\"},{\"short description\":null,\"title\":\"<a href=\\\"/content/3d-ui-webtundra-synchronization-server\\\">3D-UI-WebTundra with Synchronization server</a>\",\"icon\":null,\"label\":null,\"chapter\":null,\"rank\":null,\"bundle short description\":\"3D-UI WebTundra client with Synchronization server\",\"nid\":\"1396\",\"type\":\"Bundle\"},{\"short description\":\"Extend the current declarative, rich media content model of HTML-5 to also include interactive 3D graphics.\",\"title\":\"<a href=\\\"/enablers/3d-ui-xml3d\\\">3D-UI-XML3D</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/xml3d-logo.png\\\" width=\\\"245\\\" height=\\\"106\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/advanced-web-based-user-interface\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Advanced Web-based User Interface</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"1204\",\"type\":\"Enabler\"},{\"short description\":null,\"title\":\"<a href=\\\"/content/3d-ui-xml3d-geo-visualization\\\">3D-UI-XML3D Geo Visualization</a>\",\"icon\":null,\"label\":null,\"chapter\":null,\"rank\":null,\"bundle short description\":\"Display GIS Map data and points of interest in an interactive 3D Visualization\",\"nid\":\"1390\",\"type\":\"Bundle\"},{\"short description\":\"realXtend&#039;s Web client for realtime collaborative 3d applications\",\"title\":\"<a href=\\\"/enablers/3dui-webtundra\\\">3DUI - WebTundra</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/advanced-web-based-user-interface\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Advanced Web-based User Interface</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geis\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEis</a>\",\"bundle short description\":null,\"nid\":\"1164\",\"type\":\"Enabler\"},{\"short description\":\"The Application Management Service GE - Murano provides the basic support for hardware deployment management (virtual servers, networks...) and software installation management\",\"title\":\"<a href=\\\"/enablers/application-management-murano\\\">Application Management - Murano</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/cloud-hosting\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Cloud Hosting</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"1510\",\"type\":\"Enabler\"},{\"short description\":\"Offers a composition editor and execution engine that allows end users with little or no programming skills to create and run a composite web application front-end as a mashup built from widgets and operators relaying on backend data sources and services\",\"title\":\"<a href=\\\"/enablers/application-mashup-wirecloud\\\">Application Mashup - Wirecloud</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/fiware-catalogue2_0.png\\\" width=\\\"326\\\" height=\\\"196\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/applicationsservices-and-data-delivery\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Applications/Services and Data Delivery</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"194\",\"type\":\"Enabler\"},{\"short description\":\"The augmented reality GE provides a JavaScript library, which enables development of web-baesed AR applications\",\"title\":\"<a href=\\\"/enablers/augmented-reality\\\">Augmented Reality</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/advanced-web-based-user-interface\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Advanced Web-based User Interface</a>\",\"rank\":\"<a href=\\\"/support-ranks/incubated-gesgeris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Incubated GEs/GEris</a>\",\"bundle short description\":null,\"nid\":\"1176\",\"type\":\"Enabler\"},{\"short description\":\"Reference Implementation of Authorization PDP (formerly Access Control GE)\",\"title\":\"<a href=\\\"/enablers/authorization-pdp-authzforce\\\">Authorization PDP - AuthZForce</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":\"A++\",\"chapter\":\"<a href=\\\"/chapter/security\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Security</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"568\",\"type\":\"Enabler\"},{\"short description\":\"Backend Device Management - IDAS (IoT Agents)\",\"title\":\"<a href=\\\"/enablers/backend-device-management-idas\\\">Backend Device Management - IDAS </a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":\"A\",\"chapter\":\"<a href=\\\"/chapter/internet-things-services-enablement\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Internet of Things Services Enablement</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"696\",\"type\":\"Enabler\"},{\"short description\":\"Monitoring and control of the BigData Analysis GE\",\"title\":\"<a href=\\\"/enablers/bigdata-analysis-cosmos\\\">BigData Analysis - Cosmos</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/cosmos_logo.png\\\" width=\\\"55\\\" height=\\\"55\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/datacontext-management\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Data/Context Management</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"158\",\"type\":\"Enabler\"},{\"short description\":\"The Business API Ecosystem offers support for selling apps, data, and services to both consumers and developers of Future Internet applications and services and for end-to-end managing of offerings, sales, and revenue sharing.\",\"title\":\"<a href=\\\"/enablers/business-api-ecosystem-biz-ecosystem-ri\\\">Business API Ecosystem - Biz Ecosystem RI</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/applicationsservices-and-data-delivery\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Applications/Services and Data Delivery</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"1639\",\"type\":\"Enabler\"},{\"short description\":null,\"title\":\"<a href=\\\"/content/business-framework-consumption\\\">Business Framework Consumption</a>\",\"icon\":null,\"label\":null,\"chapter\":null,\"rank\":null,\"bundle short description\":\"The Business Framework Consumption Bundle includes a set of GEis that support different monetization and revenue sharing features\",\"nid\":\"1375\",\"type\":\"Bundle\"},{\"short description\":\"Open Data Management Platform\",\"title\":\"<a href=\\\"/enablers/ckan\\\">CKAN</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/ckan-cake_0.png\\\" width=\\\"1741\\\" height=\\\"1741\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/datacontext-management\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Data/Context Management</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"1457\",\"type\":\"Enabler\"},{\"short description\":\"AEON is a cloud platform to create applications with real time communications channels.\",\"title\":\"<a href=\\\"/enablers/cloud-messaging-aeon\\\">Cloud Messaging - AEON</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/AEONIcon.png\\\" width=\\\"114\\\" height=\\\"114\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/datacontext-management\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Data/Context Management</a>\",\"rank\":\"<a href=\\\"/support-ranks/incubated-gesgeris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Incubated GEs/GEris</a>\",\"bundle short description\":null,\"nid\":\"1501\",\"type\":\"Enabler\"},{\"short description\":\"Server side rendering of 3D applications and controlling them from a web client\",\"title\":\"<a href=\\\"/enablers/cloud-rendering\\\">Cloud Rendering</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/meshmoon-163x163.png\\\" width=\\\"163\\\" height=\\\"163\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/advanced-web-based-user-interface\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Advanced Web-based User Interface</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"1286\",\"type\":\"Enabler\"},{\"short description\":\"Complex Event Processing GE\",\"title\":\"<a href=\\\"/enablers/complex-event-processing-cep-proactive-technology-online\\\">Complex Event Processing (CEP) - Proactive Technology Online</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/datacontext-management\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Data/Context Management</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"146\",\"type\":\"Enabler\"},{\"short description\":\"CyberCAPTOR provides a tool set to detect and evaluate Cyber Security risks and propose possible remediations.\",\"title\":\"<a href=\\\"/enablers/cyber-security-ge-cybercaptor\\\">Cyber Security GE - CyberCAPTOR</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/security\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Security</a>\",\"rank\":\"<a href=\\\"/support-ranks/deprecated-geis\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Deprecated GEis</a>\",\"bundle short description\":null,\"nid\":\"1526\",\"type\":\"Enabler\"},{\"short description\":null,\"title\":\"<a href=\\\"/content/data-context-streams\\\">Data Context Streams</a>\",\"icon\":null,\"label\":null,\"chapter\":null,\"rank\":null,\"bundle short description\":\"Context Streams generation, storage and analysis\",\"nid\":\"1380\",\"type\":\"Bundle\"},{\"short description\":\"Knowage is the professional open source suite for modern business analytics over traditional sources and big data systems.\",\"title\":\"<a href=\\\"/enablers/data-visualization-knowage\\\">Data Visualization - Knowage</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/logo_knowage_100px.png\\\" width=\\\"100\\\" height=\\\"100\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/applicationsservices-and-data-delivery\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Applications/Services and Data Delivery</a>\",\"rank\":\"<a href=\\\"/support-ranks/incubated-gesgeris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Incubated GEs/GEris</a>\",\"bundle short description\":null,\"nid\":\"1705\",\"type\":\"Enabler\"},{\"short description\":\"Data Visualization Generic Enabler - SpagoBI\",\"title\":\"<a href=\\\"/enablers/data-visualization-spagobi\\\">Data Visualization - SpagoBI</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/spagobi_logo.png\\\" width=\\\"41\\\" height=\\\"41\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/applicationsservices-and-data-delivery\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Applications/Services and Data Delivery</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"1436\",\"type\":\"Enabler\"},{\"short description\":\"Self-service provisioning and life cycle management of docker hosts, clusters, and containers and associated compute, storage and network resources, based on the Docker API.\",\"title\":\"<a href=\\\"/enablers/docker\\\">Docker</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/docker.png\\\" width=\\\"500\\\" height=\\\"500\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/cloud-hosting\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Cloud Hosting</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"1518\",\"type\":\"Enabler\"},{\"short description\":\"Domibus implements a standardised message exchange protocol (based on an AS4 profile) that ensures interoperable, secure and reliable data exchange through Access Points (4-corner model).\",\"title\":\"<a href=\\\"/enablers/electronic-data-exchange-domibus\\\">Electronic Data Exchange - Domibus</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/Domibus%20logo.png\\\" width=\\\"255\\\" height=\\\"300\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/datacontext-management\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Data/Context Management</a>\",\"rank\":\"<a href=\\\"/support-ranks/incubated-gesgeris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Incubated GEs/GEris</a>\",\"bundle short description\":null,\"nid\":\"1626\",\"type\":\"Enabler\"},{\"short description\":\"High Performance Protocol to share data\",\"title\":\"<a href=\\\"/enablers/fast-rtps\\\">Fast RTPS</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/Logo300.png\\\" width=\\\"300\\\" height=\\\"264\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/advanced-middleware-and-interfaces-network-and-devices\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Advanced middleware and interfaces to Network and Devices</a>\",\"rank\":\"<a href=\\\"/support-ranks/incubated-gesgeris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Incubated GEs/GEris</a>\",\"bundle short description\":null,\"nid\":\"1645\",\"type\":\"Enabler\"},{\"short description\":\"FIA Project Management Plugin enables FI-CoDE users to create Future Internet Applications that use instances of FIWARE generic enablers\",\"title\":\"<a href=\\\"/enablers/fia-project-management-plugin\\\">FIA Project Management Plugin</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/tools\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Tools</a>\",\"rank\":null,\"bundle short description\":null,\"nid\":\"929\",\"type\":\"Enabler\"},{\"short description\":\"The FIWARE NGSI10 TestServer allows you to test your software that is intend to interact with various IoT Generic Enablers using the FIWARE NGSI10 binding, such as the IoT Broker Generic Enabler.\",\"title\":\"<a href=\\\"/enablers/fiware-ngsi10-testserver\\\">FIWARE NGSI10 TestServer</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/tools\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Tools</a>\",\"rank\":null,\"bundle short description\":null,\"nid\":\"1009\",\"type\":\"Enabler\"},{\"short description\":\"The FusionForge Connector allows to interact with tickets and tasks, of a project hosted into a FusionForge instance, directly from the IDE.\",\"title\":\"<a href=\\\"/enablers/fusionforge-connector\\\">FusionForge Connector</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/tools\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Tools</a>\",\"rank\":null,\"bundle short description\":null,\"nid\":\"939\",\"type\":\"Enabler\"},{\"short description\":\"Geographical Information System Provider\",\"title\":\"<a href=\\\"/enablers/gis-data-provider-geoserver3d\\\">GIS Data Provider - Geoserver/3D</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/Cyber_163.jpeg\\\" width=\\\"163\\\" height=\\\"163\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/advanced-web-based-user-interface\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Advanced Web-based User Interface</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"1215\",\"type\":\"Enabler\"},{\"short description\":\"Self-service provisioning and life cycle management of virtual machines and associated compute, storage and network resources, based on OpenStack\",\"title\":\"<a href=\\\"/enablers/iaas-ge-fiware-reference-implementation\\\">IaaS GE - FIWARE Reference Implementation</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/os.jpg\\\" width=\\\"172\\\" height=\\\"172\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/cloud-hosting\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Cloud Hosting</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"130\",\"type\":\"Enabler\"},{\"short description\":\"Identity Management Generic Enabler - KeyRock\",\"title\":\"<a href=\\\"/enablers/identity-management-keyrock\\\">Identity Management - KeyRock</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":\"A+\",\"chapter\":\"<a href=\\\"/chapter/security\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Security</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"1045\",\"type\":\"Enabler\"},{\"short description\":\"Easy-to-use full manipulator / editor of 3D objects within a scene.\",\"title\":\"<a href=\\\"/enablers/interface-designer\\\">Interface Designer</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/meshmoon-163x163_0.png\\\" width=\\\"163\\\" height=\\\"163\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/advanced-web-based-user-interface\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Advanced Web-based User Interface</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"1292\",\"type\":\"Enabler\"},{\"short description\":\"The IoT Broker is a middleware component enabling applications to retrieve aggregated information from Internet-of-Things installations that consist of a multitude of devices and gateways.\",\"title\":\"<a href=\\\"/enablers/iot-broker\\\">IoT Broker</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/nec-empowered-by-innovation.jpg\\\" width=\\\"345\\\" height=\\\"165\\\" alt=\\\"\\\" />\",\"label\":\"A\",\"chapter\":\"<a href=\\\"/chapter/internet-things-services-enablement\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Internet of Things Services Enablement</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"476\",\"type\":\"Enabler\"},{\"short description\":\"This GE is in charge of real-time data processing within the &quot;edge&quot; part of the FIWARE platform.\",\"title\":\"<a href=\\\"/enablers/iot-data-edge-consolidation-ge-cepheus\\\">IoT Data Edge Consolidation GE - Cepheus</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/OrangeLogo.png\\\" width=\\\"400\\\" height=\\\"400\\\" alt=\\\"\\\" />\",\"label\":\"A++\",\"chapter\":\"<a href=\\\"/chapter/internet-things-services-enablement\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Internet of Things Services Enablement</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"198\",\"type\":\"Enabler\"},{\"short description\":\"Reference implementation for IoT Discovery GE\",\"title\":\"<a href=\\\"/enablers/iot-discovery\\\">IoT Discovery</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/University-of-Surrey-logo-square1.png\\\" width=\\\"200\\\" height=\\\"200\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/internet-things-services-enablement\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Internet of Things Services Enablement</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"635\",\"type\":\"Enabler\"},{\"short description\":\"KIARA Advanced Middleware is a Java based communication middleware for modern, efficient and secure applications.\",\"title\":\"<a href=\\\"/enablers/kiara-advanced-middleware\\\">Kiara Advanced Middleware</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/advanced-middleware-and-interfaces-network-and-devices\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Advanced middleware and interfaces to Network and Devices</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"1447\",\"type\":\"Enabler\"},{\"short description\":\"An instrument to facilitate commerce by bringing together vendors and buyers\",\"title\":\"<a href=\\\"/enablers/marketplace-wmarket\\\">Marketplace - WMarket</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/marketplace.png\\\" width=\\\"500\\\" height=\\\"313\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/applicationsservices-and-data-delivery\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Applications/Services and Data Delivery</a>\",\"rank\":\"<a href=\\\"/support-ranks/deprecated-geis\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Deprecated GEis</a>\",\"bundle short description\":null,\"nid\":\"95\",\"type\":\"Enabler\"},{\"short description\":\"This Generic Enabler implementation can be used for the monitoring of virtual machines deployed in any Cloud system.\",\"title\":\"<a href=\\\"/enablers/monitoring-ge-sextant\\\">Monitoring GE - Sextant</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/cloud-hosting\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Cloud Hosting</a>\",\"rank\":\"<a href=\\\"/support-ranks/deprecated-geis\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Deprecated GEis</a>\",\"bundle short description\":null,\"nid\":\"559\",\"type\":\"Enabler\"},{\"short description\":\"The myForge is the cloud-based environment that a developer (or a team) can use to set up the resources to support the development of software applications.\",\"title\":\"<a href=\\\"/enablers/myforge\\\">myForge</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/tools\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Tools</a>\",\"rank\":null,\"bundle short description\":null,\"nid\":\"1404\",\"type\":\"Enabler\"},{\"short description\":\"Openflow network information and control\",\"title\":\"<a href=\\\"/enablers/network-information-and-control-ofnic\\\">Network Information and Control - OFNIC</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/logo-sapienza_coloured.png\\\" width=\\\"164\\\" height=\\\"104\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/advanced-middleware-and-interfaces-network-and-devices\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Advanced middleware and interfaces to Network and Devices</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"497\",\"type\":\"Enabler\"},{\"short description\":\"This Generic Enabler Implementation allows objects to be stored in folders (called containers) in the cloud.\",\"title\":\"<a href=\\\"/enablers/object-storage-ge-fiware-implementation\\\">Object Storage GE - FIWARE Implementation</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/cloud-hosting\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Cloud Hosting</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"162\",\"type\":\"Enabler\"},{\"short description\":\"Self-Service provisioning and life cycle management of middleware including the provisioning of the required virtual resources at IaaS and configuration of the whole software stack on that virtual resources.\",\"title\":\"<a href=\\\"/enablers/paas-manager-pegasus\\\">PaaS Manager - Pegasus</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/cloud-hosting\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Cloud Hosting</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"528\",\"type\":\"Enabler\"},{\"short description\":\"Security PEP Proxy Generic Enabler allows you to secure your back-end services adding authentication and authorization based on FIWARE account\",\"title\":\"<a href=\\\"/enablers/pep-proxy-wilma\\\">PEP Proxy - Wilma</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/log.png\\\" width=\\\"256\\\" height=\\\"256\\\" alt=\\\"\\\" />\",\"label\":\"A++\",\"chapter\":\"<a href=\\\"/chapter/security\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Security</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"1412\",\"type\":\"Enabler\"},{\"short description\":\"POI-DP is a generic enabler, which provides spatial search services and data on Points of Interest via RESTful web service API.\",\"title\":\"<a href=\\\"/enablers/poi-data-provider\\\">POI Data Provider</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/advanced-web-based-user-interface\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Advanced Web-based User Interface</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"1170\",\"type\":\"Enabler\"},{\"short description\":\"Generic Enabler that allows estimating the needed of an operation (e.g. horizontal scalability) based on monitoring data received from the Monitoring GEi\",\"title\":\"<a href=\\\"/enablers/policy-manager-bosun\\\">Policy Manager - Bosun</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":\"A++\",\"chapter\":\"<a href=\\\"/chapter/cloud-hosting\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Cloud Hosting</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"1123\",\"type\":\"Enabler\"},{\"short description\":\"Privacy-Preserving Authentication\",\"title\":\"<a href=\\\"/enablers/privacy\\\">Privacy</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/security\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Security</a>\",\"rank\":\"<a href=\\\"/support-ranks/incubated-gesgeris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Incubated GEs/GEris</a>\",\"bundle short description\":null,\"nid\":\"1589\",\"type\":\"Enabler\"},{\"short description\":\"PROSA is an online testing framework targeted to ensure the constant availability of QoS monitoring data for a given service (used by a FI App).\",\"title\":\"<a href=\\\"/enablers/prosa\\\">PROSA</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/tools\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Tools</a>\",\"rank\":null,\"bundle short description\":null,\"nid\":\"911\",\"type\":\"Enabler\"},{\"short description\":\"A layer that enables  CoAP and 6LoWPAN communication with IoT Devices running the Moterunner platform.\",\"title\":\"<a href=\\\"/enablers/protocol-adapter-mr-coap\\\">Protocol Adapter - MR CoAP </a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/internet-things-services-enablement\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Internet of Things Services Enablement</a>\",\"rank\":\"<a href=\\\"/support-ranks/deprecated-geis\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Deprecated GEis</a>\",\"bundle short description\":null,\"nid\":\"1023\",\"type\":\"Enabler\"},{\"short description\":\"Orion Context Broker is an implementation of NGSI9 and NGIS10 with persistence storage based in MongoDB\",\"title\":\"<a href=\\\"/enablers/publishsubscribe-context-broker-orion-context-broker\\\">Publish/Subscribe Context Broker - Orion Context Broker</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/orion.png\\\" width=\\\"417\\\" height=\\\"417\\\" alt=\\\"\\\" />\",\"label\":\"A+++\",\"chapter\":\"<a href=\\\"/chapter/datacontext-management\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Data/Context Management</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"344\",\"type\":\"Enabler\"},{\"short description\":\"Real Virtual Interaction\",\"title\":\"<a href=\\\"/enablers/real-virtual-interaction\\\">Real Virtual Interaction</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/Cyber_163_0.jpeg\\\" width=\\\"163\\\" height=\\\"163\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/advanced-web-based-user-interface\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Advanced Web-based User Interface</a>\",\"rank\":\"<a href=\\\"/support-ranks/deprecated-geis\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Deprecated GEis</a>\",\"bundle short description\":null,\"nid\":\"1249\",\"type\":\"Enabler\"},{\"short description\":\"A service and application description repository\",\"title\":\"<a href=\\\"/enablers/repository-repository-ri\\\">Repository - Repository RI</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/linked-usdl-solo-transparent.png\\\" width=\\\"82\\\" height=\\\"77\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/applicationsservices-and-data-delivery\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Applications/Services and Data Delivery</a>\",\"rank\":\"<a href=\\\"/support-ranks/deprecated-geis\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Deprecated GEis</a>\",\"bundle short description\":null,\"nid\":\"58\",\"type\":\"Enabler\"},{\"short description\":\"The REST Client Generator allows to automatically create a Java client for a RESTful web service.\",\"title\":\"<a href=\\\"/enablers/rest-client-generator\\\">REST Client Generator</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/tools\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Tools</a>\",\"rank\":null,\"bundle short description\":null,\"nid\":\"917\",\"type\":\"Enabler\"},{\"short description\":\"The Revenue Settlement and Sharing System GE distributes revenues among stakeholders.\",\"title\":\"<a href=\\\"/enablers/revenue-settlement-and-sharing-system-rss-ri\\\">Revenue Settlement and Sharing System - RSS RI</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/applicationsservices-and-data-delivery\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Applications/Services and Data Delivery</a>\",\"rank\":\"<a href=\\\"/support-ranks/deprecated-geis\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Deprecated GEis</a>\",\"bundle short description\":null,\"nid\":\"554\",\"type\":\"Enabler\"},{\"short description\":\"Security monitoring is a suite of services for risk analysis, security visualization, decision making support and technical forensics.\",\"title\":\"<a href=\\\"/enablers/security-monitoring\\\">Security Monitoring</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/security\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Security</a>\",\"rank\":\"<a href=\\\"/support-ranks/deprecated-geis\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Deprecated GEis</a>\",\"bundle short description\":null,\"nid\":\"315\",\"type\":\"Enabler\"},{\"short description\":\"Self Service Interfaces for Cloud Hosting\",\"title\":\"<a href=\\\"/enablers/self-service-interfaces-cloud-portal\\\">Self Service Interfaces - Cloud Portal</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/cloud-hosting\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Cloud Hosting</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"626\",\"type\":\"Enabler\"},{\"short description\":\"This Generic Enabler implementation can be used for the deployment and configuration of complete applications on virtual machines.\",\"title\":\"<a href=\\\"/enablers/software-deployment-configuration-sagitta\\\">Software Deployment &amp; Configuration - Sagitta</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/cloud-hosting\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Cloud Hosting</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"537\",\"type\":\"Enabler\"},{\"short description\":\"SoPeCo is a framework for systematic performance evaluations of software systems.\",\"title\":\"<a href=\\\"/enablers/sopeco\\\">SoPeCo</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/tools\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Tools</a>\",\"rank\":null,\"bundle short description\":null,\"nid\":\"949\",\"type\":\"Enabler\"},{\"short description\":\"The Store GE offers support for selling services to both consumers and developers of Future Internet applications and services and for end-to-end managing of offerings and sales.\",\"title\":\"<a href=\\\"/enablers/store-wstore\\\">Store - WStore</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/WStore_v1.png\\\" width=\\\"616\\\" height=\\\"373\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/applicationsservices-and-data-delivery\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Applications/Services and Data Delivery</a>\",\"rank\":\"<a href=\\\"/support-ranks/deprecated-geis\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Deprecated GEis</a>\",\"bundle short description\":null,\"nid\":\"512\",\"type\":\"Enabler\"},{\"short description\":\"Powerful software stack devoted to simplify the creation of complex interactive multimedia applications by exposing a rich family of APIs on top of a J2EE application server.\",\"title\":\"<a href=\\\"/enablers/stream-oriented-kurento\\\">Stream-oriented - Kurento</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/only-circle.png\\\" width=\\\"410\\\" height=\\\"410\\\" alt=\\\"\\\" />\",\"label\":\"A+\",\"chapter\":\"<a href=\\\"/chapter/datacontext-management\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Data/Context Management</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"855\",\"type\":\"Enabler\"},{\"short description\":\"Bidirectional scene synchronization for multi-user applications\",\"title\":\"<a href=\\\"/enablers/synchronization\\\">Synchronization</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/advanced-web-based-user-interface\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Advanced Web-based User Interface</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"1182\",\"type\":\"Enabler\"},{\"short description\":\"A Flexible Virtual Environment server (alternative Synchronization GEi)\",\"title\":\"<a href=\\\"/enablers/synchronization-fives\\\">Synchronization - FiVES</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/advanced-web-based-user-interface\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Advanced Web-based User Interface</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geis\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEis</a>\",\"bundle short description\":null,\"nid\":\"1494\",\"type\":\"Enabler\"},{\"short description\":\"Trace Analyzer is an Eclipse plugin for graphical and numerical analysis of performance traces.\",\"title\":\"<a href=\\\"/enablers/trace-analyzer\\\">Trace Analyzer</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/tools\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Tools</a>\",\"rank\":null,\"bundle short description\":null,\"nid\":\"923\",\"type\":\"Enabler\"},{\"short description\":\"The Unit Functional Testing Framework (aka UFT Framework) is a framework to test REST services.\",\"title\":\"<a href=\\\"/enablers/unit-functional-testing-framework\\\">Unit Functional Testing Framework</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/tools\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Tools</a>\",\"rank\":null,\"bundle short description\":null,\"nid\":\"968\",\"type\":\"Enabler\"},{\"short description\":\"Character animation library for Web applications\",\"title\":\"<a href=\\\"/enablers/virtual-characters\\\">Virtual Characters</a>\",\"icon\":\"<img typeof=\\\"foaf:Image\\\" src=\\\"https://catalogue.fiware.org/sites/default/files/default_images/Lg-bw_1.png\\\" alt=\\\"\\\" />\",\"label\":null,\"chapter\":\"<a href=\\\"/chapter/advanced-web-based-user-interface\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">Advanced Web-based User Interface</a>\",\"rank\":\"<a href=\\\"/support-ranks/fiware-geris\\\" typeof=\\\"skos:Concept\\\" property=\\\"rdfs:label skos:prefLabel\\\" datatype=\\\"\\\">FIWARE GEris</a>\",\"bundle short description\":null,\"nid\":\"1188\",\"type\":\"Enabler\"},{\"short description\":null,\"title\":\"<a href=\\\"/content/wirecloud-monetization\\\">Wirecloud Monetization</a>\",\"icon\":null,\"label\":null,\"chapter\":null,\"rank\":null,\"bundle short description\":\"The Wirecloud (App Mashup) Monetization Bundle represents one of the uses cases of the  Business Framework and intends to show the potential of the BF features provided by FIWARE in a concrete use case. This use case is based on the use of the BF\",\"nid\":\"1363\",\"type\":\"Bundle\"}]";
		}	
		
		JSONArray jA=JSONFactoryUtil.createJSONArray();
		try {
			jA = JSONFactoryUtil.createJSONArray(resp);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		JSONArray jAp = JSONFactoryUtil.createJSONArray();
		
		//clean html tags
		for (int i = 0; i < jA.length(); i++) {
			
			JSONObject ge = jA.getJSONObject(i);
			
			String rank = ge.getString("rank").replaceAll("\\<[^>]*>","");
			
			if (!rank.isEmpty()  &&  !rank.equalsIgnoreCase(DEPRECATED_GEIS)){
			
				String shortDescription = ge.getString("short description");
				String title = ge.getString("title").replaceAll("\\<[^>]*>","");
				String url = getUrlByString(ge.getString("title"));
				
				String icon = ge.getString("icon");
				String label = ge.getString("label");
				String chapter = ge.getString("chapter").replaceAll("\\<[^>]*>","");
				
				String bundle = ge.getString("bundle");
				String nid = ge.getString("nid");
				String type = ge.getString("type");
	
				JSONObject gep = JSONFactoryUtil.createJSONObject();
				gep.put("shortDescription", shortDescription);
				gep.put("title", title);
				gep.put("icon", icon);
				gep.put("label", getQaByLabel(label));
				gep.put("chapter", chapter);
				gep.put("rank", rank);
				gep.put("bundle", bundle);
				gep.put("nid", nid);
				gep.put("type", type);
				gep.put("url", FIRST_PART_FIWARE_CATALOGUE_ADDRESS+url);
				gep.put("color", getColorByRank(rank));
				
				jAp.put(gep);
			}
		}
		
		return jAp;
	}
	
	/**
	 * @param allGES
	 * @param nid
	 * @return
	 */
	public static JSONObject getGEByAllGEs (JSONArray allGES, String nid){
		
		JSONObject geRet = JSONFactoryUtil.createJSONObject();
		
		for(int i=0; i<allGES.length(); i++){
			
			JSONObject ge = allGES.getJSONObject(i);
			if (ge.getString("nid").equalsIgnoreCase(nid))
				return ge;
		}
		
		return geRet;//default, never
		
	}
	
	

	/**
	 * @param rank
	 * @return
	 */
	public static String getColorByRank (String rank){
		
		String val="green";
		
		Map<String, String> rankColor = new HashMap<String, String>();
		rankColor.put(FIWARE_GEIS, "grey");
		rankColor.put(FIWARE_GERIS, "green");
		rankColor.put(DEPRECATED_GEIS, "red");
		rankColor.put(INCUBATED_GES_GERIS, "yellow");
		
		val = rankColor.get(rank);	
		
		return val;
		
	}
	

	
	/**
	 * @param rank
	 * @return
	 */
	private static String getQaByLabel (String label){
		
		String val="";
		
		Map<String, String> labelQA = new HashMap<String, String>();
		labelQA.put("A", "A");
		labelQA.put("A+", "A1");
		labelQA.put("A++", "A2");
		labelQA.put("A+++", "A3");
		
		val = labelQA.get(label);	
		
		return val;
		
	}
	
/**
 * @param qa
 * @return
 */
private static String getLabelbyQA (String qa){
		
		String val="";
		
		Map<String, String> labelQA = new HashMap<String, String>();
		labelQA.put("A", "A");
		labelQA.put("A1", "A+");
		labelQA.put("A2", "A++");
		labelQA.put("A3", "A+++");
		
		val = labelQA.get(qa);	
		
		return val;
		
	}
	
	/**
	 * @param html
	 * @return
	 */
	public static String getUrlByString (String html){
		
		Pattern p = Pattern.compile("href=\"(.*?)\"");
		Matcher m = p.matcher(html);
		String url = null;
		if (m.find()) {
		    url = m.group(1); // this variable should contain the link URL
		}
		
		return url;
	}
	
	/**
	 * @param ideaId
	 * @return
	 */
	public static boolean isIdeaWithSelectedGEs(long ideaId){
		
		List<GESelected> ges = GESelectedLocalServiceUtil.getGeSelectedsByIdeaId(ideaId);
		if (ges.size()>0)
			return true;
		else
			return false;
	}
	
	
	/**
	 * @param ideaId
	 * @param req
	 */
	public static void createSavePdfByIdeaId (long ideaId, ActionRequest actionRequest){
		
		CLSIdea idea = null;
		try {
			 idea = CLSIdeaLocalServiceUtil.getCLSIdea(ideaId);
		} catch (PortalException | SystemException e) {
			e.printStackTrace();
		}
		
		createSavePdf(idea, actionRequest);
		
	}
	
	/**
	 * @param ideaId
	 * @param req
	 */
	public static void createSavePdfByIdeaId (long ideaId, RenderRequest renderRequest){
		
		CLSIdea idea = null;
		try {
			 idea = CLSIdeaLocalServiceUtil.getCLSIdea(ideaId);
		} catch (PortalException | SystemException e) {
			e.printStackTrace();
		}
		
		createSavePdf(idea, renderRequest);
	}
	
	
	public static void createSavePdf (CLSIdea idea, RenderRequest renderRequest){
		
		User user = (User) renderRequest.getAttribute(WebKeys.USER);
		Image reprImage = IdeasListUtils.getRepresentativeImageByIdea(idea, renderRequest);
		
		createSavePdf ( idea,  user,reprImage);
		
	}
	
	public static void createSavePdf (CLSIdea idea, ActionRequest actionRequest){
			
		User user = (User) actionRequest.getAttribute(WebKeys.USER);
		Image reprImage = IdeasListUtils.getRepresentativeImageByIdea(idea, actionRequest);
		createSavePdf ( idea,  user,reprImage);	
	}
		
	
	/**
	 * @param idea
	 * @param req
	 */
	public static void createSavePdf (CLSIdea idea, User user, Image reprImage){
		
		boolean isIdeaWithSelectedGEs = isIdeaWithSelectedGEs(idea.getIdeaID());
		if (!isIdeaWithSelectedGEs)
			return;
		
		File prenoMemo = new File(PDF_FILENAME);
		
		try {	
			
			OutputStream file = new FileOutputStream(prenoMemo);                        
	         
	        Document document = new Document();
	        PdfWriter.getInstance(document, file);
	        document.open();
	        
	       
	        
	        document.add(reprImage);
	        document.addTitle(idea.getIdeaTitle());
	        
	        document.add( Chunk.NEWLINE );
	        
	        Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
	        
	        Paragraph p0 = new Paragraph(idea.getIdeaTitle(), boldFont );
	        p0.setAlignment(p0.ALIGN_CENTER);
	        document.add( p0  );
	        
	        document.add( Chunk.NEWLINE );
	        
	        Paragraph p1 = new Paragraph("This idea uses the following Fiware Generic Enablers:" );
	        p1.setAlignment(p1.ALIGN_JUSTIFIED);
	        document.add( p1  );
	        document.add( Chunk.NEWLINE );
	        
	        List<GESelected> ges = GESelectedLocalServiceUtil.getGeSelectedsByIdeaId(idea.getIdeaID());
	        
	        for (GESelected ge:ges){
	        	
	            PdfPTable table = new PdfPTable(2);
	            table.setTotalWidth(new float[]{ 80, 400 });
	            table.setLockedWidth(true);
	            
	            PdfPCell cell = new PdfPCell(new Phrase(ge.getTitle()));
	            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	            cell.setColspan(2);
	            table.addCell(cell);
	            
	            cell = new PdfPCell(new Phrase("Description"));
	            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	            table.addCell(cell);

	            cell = new PdfPCell(new Phrase(ge.getShortDescription()));
	            table.addCell(cell);
	            
	            cell = new PdfPCell(new Phrase("Chapter"));
	            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	            table.addCell(cell);

	            cell = new PdfPCell(new Phrase(ge.getChapter()));
	            table.addCell(cell);
	            
	            cell = new PdfPCell(new Phrase("Rank"));
	            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	            table.addCell(cell);

	            cell = new PdfPCell(new Phrase(ge.getRank()));
	            table.addCell(cell);
	            
	            if (!ge.getLabel().isEmpty()){
	            
		            cell = new PdfPCell(new Phrase("Quality"));
		            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		            table.addCell(cell);
	
		            cell = new PdfPCell(new Phrase(getLabelbyQA(ge.getLabel())));
		            table.addCell(cell);
	            }
	            
	            cell = new PdfPCell(new Phrase("More details"));
	            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	            table.addCell(cell);

	            Phrase phrase = new Phrase();
	            Chunk chunk = new Chunk(ge.getUrl());
	            chunk.setAnchor(ge.getUrl());
	            phrase.add(chunk);
	            cell = new PdfPCell(phrase);
	            
	            table.addCell(cell);
	            
	            
	            document.add(table);
	            
	            document.add( Chunk.NEWLINE );
	        }
	        
	        Font smallFont = new Font(Font.FontFamily.TIMES_ROMAN, 10);
	        
	        Paragraph p3 = new Paragraph("For more details about the idea: ", smallFont);
	        
	        Chunk chunk = new Chunk(IdeaManagementSystemProperties.getFriendlyUrlIdeas(idea.getIdeaID()));
	        chunk.setAnchor(IdeaManagementSystemProperties.getFriendlyUrlIdeas(idea.getIdeaID()));
	        
	        
	        p3.add(chunk);
	        
	        p3.setAlignment(p3.ALIGN_LEFT);
	        document.add( p3  );
	        document.add( Chunk.NEWLINE );
	        
	        Paragraph p4 = new Paragraph("For more details about the Fiware Generic Enablers: ", smallFont);
	        
	        Chunk chunk1 = new Chunk(FIRST_PART_FIWARE_CATALOGUE_ADDRESS );
	        chunk1.setAnchor(FIRST_PART_FIWARE_CATALOGUE_ADDRESS );
	        p4.add(chunk1);
	        
	        p4.setAlignment(p4.ALIGN_LEFT);
	        document.add( p4  );
	        document.add( Chunk.NEWLINE );
	        
	        
	        document.close();
	        file.close();
	        
	        
		} catch (Exception e) { e.printStackTrace(); }
		
		//Now I have the file, I'm going to save on DocumentLibrary
		
		Long folderId = idea.getIdFolder();
		long groudId = idea.getGroupId();
		long userId = user.getUserId();
		ServiceContext serviceContext = new ServiceContext();
		serviceContext.setScopeGroupId(groudId);
		
		
		//check if the file exists
		DynamicQuery queryFile = DynamicQueryFactoryUtil.forClass(DLFileEntry.class)
		.add(PropertyFactoryUtil.forName("userId").eq(new Long(userId)))
		.add(PropertyFactoryUtil.forName("groupId").eq(new Long(groudId)))				
		.add(PropertyFactoryUtil.forName("folderId").eq(new Long(folderId)))
		.add(PropertyFactoryUtil.forName("title").eq(PDF_FILENAME));
		
		List<DLFileEntry> dLFile = null;
		try {
			dLFile = DLFileEntryLocalServiceUtil.dynamicQuery(queryFile);
		} catch (SystemException e1) {
			e1.printStackTrace();
		}
		
		
		if (dLFile.size() >0){
				//If it exists, I delete it
				try {
					DLAppLocalServiceUtil.deleteFileEntry(dLFile.get(0).getFileEntryId());
				} catch (PortalException | SystemException e) {
					e.printStackTrace();
				}
		}
		
		
		try {
			DLAppLocalServiceUtil.addFileEntry(userId, groudId, folderId, PDF_FILENAME, new MimetypesFileTypeMap().getContentType(prenoMemo), PDF_FILENAME, "", "", prenoMemo, serviceContext);
		} catch (PortalException | SystemException e) {
			e.printStackTrace();
		}
		
		
	}
	
	
	/**
	 * @param ges
	 * @return
	 */
	public static String getCreateHtmlGEs(GESelected ges){
		

	StringBuilder myvar = new StringBuilder(); 
	myvar.append("<table style=\"float: left; width: 84px;\" border=\"2\">")
	     .append("<tbody>")
	     .append("<tr style=\"height: 22px;\">")
	     .append("<td style=\"height: 22px; width: 77px;\"> Title</td>")
	     .append("<td style=\"height: 22px; width: 10px;\">"+ges.getTitle() +"</td>")
	     .append("</tr>")
	     .append("<tr style=\"height: 21px;\">")
	     .append("<td style=\"height: 21px; width: 77px;\"> Description</td>")
	     .append("<td style=\"height: 21px; width: 10px;\">"+ges.getShortDescription() +" </td>")
	     .append("</tr>")
	     .append("<tr style=\"height: 21px;\">")
	     .append("<td style=\"height: 21px; width: 77px;\"> Quality</td>")
	     .append("<td style=\"height: 21px; width: 10px;\">"+ getQaByLabel(ges.getLabel())  +" </td>")
	     .append("</tr>")
	     .append("<tr style=\"height: 21px;\">")
	     .append("<td style=\"height: 21px; width: 77px;\"> Rank</td>")
	     .append("<td style=\"height: 21px; width: 10px;\">"+ges.getRank() +" </td>")
	     .append("</tr>")
	     .append("<tr style=\"height: 21px;\">")
	     .append("<td style=\"height: 21px; width: 77px;\"> Chapter</td>")
	     .append("<td style=\"height: 21px; width: 10px;\">"+  ges.getChapter() +" </td>")
	     .append("</tr>")
	     .append("<tr style=\"height: 21px;\">")
	     .append("<td style=\"height: 21px; width: 77px;\">More details</td>")
	     .append("<td style=\"height: 21px; width: 10px;\">"+ges.getUrl() +" </td>")
	     .append("</tr>")
	     .append("</tbody>")
	     .append("</table>");
	

		
		return myvar.toString();
		
		
	}
	
	
	
	/**
	 * @param tk
	 * @return
	 */
	public static JSONObject getFilePdfGE (long ideaId){
			
			JSONObject jObj = JSONFactoryUtil.createJSONObject();
			
			CLSIdea idea = null;
			
			try {
				idea = CLSIdeaLocalServiceUtil.getCLSIdea(ideaId);
			} catch (PortalException | SystemException e) {
				
				e.printStackTrace();
				return jObj;
			}
			
			long groupId = idea.getGroupId();
					
			List<DLFileEntry> dlFiles = IdeasListUtils.getFilesByIdeaId(ideaId, groupId);
			
			//cycle on all idea files
			for (DLFileEntry dlFile :dlFiles ){
				
				//trovero' il file dell'outcomes 
				if ( dlFile.getTitle().equals(FiwareUtils.PDF_FILENAME) ){
					
					String fileURL = IdeaManagementSystemProperties.getRootUrl()+"/documents/" + groupId + StringPool.SLASH +  dlFile.getUuid();
				
					jObj.put("src" ,fileURL );
					jObj.put("filename" , dlFile.getTitle());
					jObj.put("type", dlFile.getMimeType());
	  				jObj.put("size", dlFile.getSize());
				}
			}
			return jObj;
		}
	
	
	/**
	 * @param ideaId
	 * @return
	 */
	public static String getUrlFilePdfGE(long ideaId){
		
		JSONObject jObj =getFilePdfGE(ideaId);
		String url = jObj.getString("src");
		
		return url;
		
	}
	
	/**
	 * @param fiaCategory
	 * @return
	 */
	public static String getFiwareCategoryByFIACategoryMapping(String fiaCategory){

		
		Map<String, String> categories = new HashMap<String, String>();
		
		categories.put("Network Interoperability", "Advanced middleware and interfaces to Network and Devices");
		categories.put("Web Interface", "Advanced Web-based User Interface");
		categories.put("Business Intelligence", "Applications/Services and Data Delivery");
		categories.put("Cloud", "Cloud Hosting");
		categories.put("Big and Open Data", "Data/Context Management");
		categories.put("Internet of Things", "Internet of Things Services Enablement");
		categories.put("Security", "Security");
		
		return categories.get(fiaCategory);
	}
	
	
	
	/**
	 * @param sCat
	 * @return
	 */
	public static JSONArray getFiwareCategoriesByFiaCategories(String sCat){
		
		JSONArray jsonFIWARECat = JSONFactoryUtil.createJSONArray();
		
		JSONArray jsonFIACat=null;
		try {
			jsonFIACat = JSONFactoryUtil.createJSONArray(sCat);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		 for(int i=0; i<jsonFIACat.length(); i++){
			 
			 String fiaCat = jsonFIACat.getString(i);
			 
			 String fiwareCat = getFiwareCategoryByFIACategoryMapping(fiaCat);
			 jsonFIWARECat.put(fiwareCat);
		}
		
		return jsonFIWARECat;
	}
	
	
}
