package it.eng.rspa.ideas.utils.listener;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;

public interface iIdeaListener {

	/** Invocato alla pubblicazione di un'idea **/
	public void onIdeaPublish(CLSIdea i);
	
	/** Invocato alla cancellazione di un'idea **/
	public void onIdeaDelete(CLSIdea i);
	
	/** Invocato alla modifica di un'idea **/
	public void onIdeaUpdate(CLSIdea i);
	
	/** Invocato al cambio di stato di un'idea **/
	public void onIdeaStateChange(CLSIdea i, String sourceState, String targetState);
	
}
