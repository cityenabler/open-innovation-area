package it.eng.rspa.ideas.utils;

import it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;

public class GrayLogUtils {
	
	
	public static void graylogNotify(String type, long id){
		
		
		String endpoint = IdeaManagementSystemProperties.getProperty("graylogAddress");
		

		//{"short_message":"New Need on Open Innovation Area", "host":"oia.s4c.eng.it", "_component":"OIA", "_event_type": "NEED_CREATED", "_id": 123 }
		
		JSONObject json = JSONFactoryUtil.createJSONObject();
		json.put("short_message", "New "+type+" on Open Innovation Area");
		json.put("host", "oia.s4c.eng.it");
		json.put("_component", "OIA");
		json.put("_event_type", type+"_CREATED");
		json.put("_id", id);
		
		try {
			 RestUtils.consumePostWsNoAuth(json, endpoint);
		} catch (Exception e) {
			
			e.printStackTrace();
		}	
		
		
		
		
	}

}
