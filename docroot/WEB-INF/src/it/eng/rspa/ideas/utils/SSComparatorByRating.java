package it.eng.rspa.ideas.utils;

import java.util.Comparator;

import com.liferay.portlet.documentlibrary.model.DLFileEntry;

public class SSComparatorByRating implements Comparator<DLFileEntry>{

	

	@Override
	public int compare(DLFileEntry entry1, DLFileEntry entry2) {
		
		double rating1 = 0;
		double rating2 = 0;
		
		rating1 = IdeasListUtils.getAverageDoubleRatingsByDLFileEntry(entry1);
		rating2 = IdeasListUtils.getAverageDoubleRatingsByDLFileEntry(entry2);
			
		
		int ret=-1;
		if (rating2 > rating1)
			ret = 1;
		
		return ret;
	}

}
