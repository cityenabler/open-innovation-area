package it.eng.rspa.ideas.utils;

import java.util.ArrayList;
import java.util.List;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSRequisitiLocalServiceUtil;
import it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;
import com.liferay.tasks.model.TasksDetails;
import com.liferay.tasks.model.TasksEntry;
import com.liferay.tasks.service.TasksDetailsLocalServiceUtil;
import com.liferay.tasks.service.TasksEntryLocalServiceUtil;

public class TaskUtils {
	
	/**
	 * @param taskId
	 * @return
	 */
	public static CLSRequisiti getRequirementByTaskId (long taskId){
		
		TasksEntry tasksEntry=null;
		try {
			tasksEntry = TasksEntryLocalServiceUtil.fetchTasksEntry(taskId);
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		long reqId = tasksEntry.getRequisitoId();
		
		CLSRequisiti req = null;
		try {
			 req = CLSRequisitiLocalServiceUtil.getCLSRequisiti(reqId);
			
		} catch (PortalException | SystemException e) {
			e.printStackTrace();
		}
		
		return req;
		
	}
	
	/**
	 * @param task
	 * @return
	 */
	public static CLSRequisiti getRequirementByTask (TasksEntry task){
		
		CLSRequisiti req = getRequirementByTaskId(task.getTasksEntryId());
		return req;
		
	}
	
	/**
	 * @param task
	 * @return
	 */
	public static String getIdeaTitleByTask (TasksEntry task){
		
		String ideaTitle = " --- ";
		
		long ideaId = task.getIdeaId();
		
		if (Validator.isNull(ideaId))
			return ideaTitle;
		
		if (!(ideaId > 0))
			return ideaTitle;
		
		
		CLSIdea idea = null;
		
				try {
					idea = CLSIdeaLocalServiceUtil.getCLSIdea(ideaId);
					ideaTitle = idea.getIdeaTitle();
				} catch (PortalException | SystemException e) {
					
					System.out.println(e.getMessage());
					return ideaTitle ;
					
				}
				
		return ideaTitle ;
		
	}
	
	
	/**
	 * @param taskId
	 * @return
	 */
	public static boolean isTaskWithAttachment (long taskId){
		
		CLSRequisiti req = getRequirementByTaskId(taskId);
		return req.isOutcomeFile();
	}
	
	
	/**
	 * @param taskId
	 * @return
	 */
	public static boolean isTaskMultiTask (long taskId){
		
		CLSRequisiti req = getRequirementByTaskId(taskId);
		return req.getMultiTask();
	}
	
	/**
	 * @param taskId
	 * @return
	 */
	public static boolean isTaskDuobleField (long taskId){
		
		CLSRequisiti req = getRequirementByTaskId(taskId);
		return req.isTaskDoubleField();
	}
	
	/**
	 * @param taskId
	 * @return
	 */
	public static boolean isKPIreq (long taskId){
		return isReqDescription(taskId, "KPI measurement methods");
	}
	
	/**
	 * @param taskId
	 * @return
	 */
	public static boolean isReqChallengeAndSolution (long taskId){
		return isReqDescription(taskId, "Challenges and solutions");
	}
	
	/**
	 * @param taskId
	 * @return
	 */
	public static boolean isReqDescription (long taskId, String description){
		
		CLSRequisiti req = getRequirementByTaskId(taskId);
		if (req.getDescrizione().equalsIgnoreCase(description))
				return true;
		
		return false;
		
	}
	
	/**
	 * @param task
	 * @return
	 */
	public static String getRequirementTitleByTask(TasksEntry task){
		
		CLSRequisiti req = getRequirementByTask(task);
		return req.getDescrizione();
		
	}
	
	
	/**
	 * Ritorna il link al file legato all'outcomes del task (TasksDetails)
	 * @param tk
	 * @return
	 */
	public static String getFileOutcomeLinkByTaskDetails (TasksDetails tk){
		
		TasksEntry te = null;
		try {
			 te = TasksEntryLocalServiceUtil.getTasksEntry(tk.getTasksEntryId());
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
		}
		
		CLSIdea idea = null;
		
		try {
			idea = CLSIdeaLocalServiceUtil.getCLSIdea(te.getIdeaId());
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
		}
		
		long groupId = idea.getGroupId();
				
		List<DLFileEntry> dlFiles = getFilesByIdeaId(te.getIdeaId(), groupId);
		
		//scorro tutti i file legati all'idea
		for (DLFileEntry dlFile :dlFiles ){
			
			//trovero' il file dell'outcomes 
			if (dlFile.getName().equals(tk.getFilename())){
				  String fileURL = "/documents/" + groupId + StringPool.SLASH +  dlFile.getUuid();
				  return fileURL;
			}
			
		}
		
		return "";
	}
	
/**
 * @param tk
 * @return
 */
public static JSONObject getFileOutcomeByTaskDetails (TasksDetails tk){
		
		JSONObject jObj = JSONFactoryUtil.createJSONObject();
	
		TasksEntry te = null;
		try {
			 te = TasksEntryLocalServiceUtil.getTasksEntry(tk.getTasksEntryId());
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
			return jObj;
		}
		
		CLSIdea idea = null;
		
		try {
			idea = CLSIdeaLocalServiceUtil.getCLSIdea(te.getIdeaId());
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
			return jObj;
		}
		
		long groupId = idea.getGroupId();
				
		List<DLFileEntry> dlFiles = getFilesByIdeaId(te.getIdeaId(), groupId);
		
		//scorro tutti i file legati all'idea
		for (DLFileEntry dlFile :dlFiles ){
			
			//trovero' il file dell'outcomes 
			if (dlFile.getName().equals(tk.getFilename())  ){
				
				String fileURL = IdeaManagementSystemProperties.getRootUrl()+"/documents/" + groupId + StringPool.SLASH +  dlFile.getUuid();
			
				jObj.put("src" ,fileURL );
				jObj.put("filename" , dlFile.getTitle());
				jObj.put("type", dlFile.getMimeType());
  				jObj.put("size", dlFile.getSize());
				  
			}
			
		}
		
		return jObj;
	}

	
	/**
	 * @param ideaId
	 * @param groupId
	 * @return
	 */
	public static List<DLFileEntry> getFilesByIdeaId (long ideaId, long groupId){
		
		List<DLFileEntry> files = new ArrayList<DLFileEntry>();
		
		CLSIdea idea = null;
		try {
			idea = CLSIdeaLocalServiceUtil.getIdeasByIdeaId(ideaId);
		} catch (SystemException e1) {
			
			e1.printStackTrace();
			return files;
		}
		
		Long folderId = idea.getIdFolder();
		Integer count = 0;
		try {
			count = DLFileEntryLocalServiceUtil.getFileEntriesCount(groupId, folderId);
		} catch (SystemException e) {
			
			e.printStackTrace();
			return files;
		}
		OrderByComparator obc = OrderByComparatorFactoryUtil.create("DLFileEntry", "title", true);
		
		
		
		try {
			 files = DLFileEntryLocalServiceUtil.getFileEntries(groupId, folderId.longValue(), 0, count,obc);
		} catch (SystemException e) {
			
			e.printStackTrace();
			return files;
		}

		return files;
		
		
	}
	
	
	/**
	 * @param idea
	 * @param reqDescription
	 * @return
	 */
	public static List<TasksDetails> getSolutionsByIdeaReq(CLSIdea idea, String reqDescription){
		
		long ideaId = idea.getIdeaID();
		List<CLSRequisiti> reqs = new ArrayList<CLSRequisiti>();
		try {
			 reqs = CLSRequisitiLocalServiceUtil.getRequirementByIdeaIdAndDescr(ideaId, reqDescription);
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		CLSRequisiti req = null;
		if (reqs.size() > 0  )
			req = reqs.get(0);
		
		List<TasksEntry> tasks= new ArrayList<TasksEntry>();
		
		try {
			tasks=TasksEntryLocalServiceUtil.getByRequisitiId(req.getRequisitoId());
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		TasksEntry task = null;
		if (tasks.size() > 0  )
			task = tasks.get(0);
		
		
		List<TasksDetails> tasksDetails = new ArrayList<TasksDetails>();
		try {
			tasksDetails = TasksDetailsLocalServiceUtil.getByTaskId(task.getTasksEntryId());
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		return tasksDetails;
	}
	
	/**
	 * @param idea
	 * @param reqDescription
	 * @return
	 */
	public static String getSolutionByIdeaReq(CLSIdea idea, String reqDescription){
		
		String solution = "";
		List<TasksDetails> tasksDetails = getSolutionsByIdeaReq( idea,  reqDescription);

		if (tasksDetails.size() >0)
			solution=tasksDetails.get(0).getDescription();
		
		return solution;
	}
	
	
}
