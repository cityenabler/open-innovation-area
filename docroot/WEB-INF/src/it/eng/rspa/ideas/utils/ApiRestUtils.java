package it.eng.rspa.ideas.utils;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoi;
import it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.NeedLinkedChallengeLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.impl.RestAPIsServiceImpl;

import java.util.Formatter;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

import com.ibm.icu.text.SimpleDateFormat;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;

/**
 * @author UTENTE
 *
 */
public class ApiRestUtils {
	
	private static final String dateformat = "yyyy-MM-dd";
	private static final SimpleDateFormat sdf = new SimpleDateFormat(dateformat);
	private static final String challengeDetailsPagePattern = "/challenges_explorer/-/challenges_explorer_contest/%1$2s/view";
	private static final String needDetailsPagePattern = "/needs_explorer/-/needs_explorer_contest/%1$2s/view";
	private static final String ideaDetailsPagePattern = "/ideas_explorer/-/ideas_explorer_contest/%1$2s/view";
	
	private static final String challengedetailURIPattern ="/api/jsonws/Challenge62-portlet.apis/challenge/id/%1$2s";
	private static final String ideadetailURIPattern = "/api/jsonws/Challenge62-portlet.apis/idea/id/%1$2s";
	private static final String needdetailURIPattern = "/api/jsonws/Challenge62-portlet.apis/need/id/%1$2s";
	
	private static Logger _log = Logger.getLogger(RestAPIsServiceImpl.class.getName());
	
	
/**
 * @param c
 * @return
 */
public static JSONObject challenge2JsonObject(CLSChallenge c){
		
		JSONObject jChallenge = JSONFactoryUtil.createJSONObject();
		
		jChallenge.put("id", c.getChallengeId());
		jChallenge.put("title", c.getChallengeTitle());
		jChallenge.put("status", c.getChallengeStatus());
		
		JSONObject period = JSONFactoryUtil.createJSONObject();
			period.put("from", sdf.format(c.getDateStart()));
			period.put("to", sdf.format(c.getDateEnd()));
			period.put("dateFormat", dateformat);
			
		jChallenge.put("period", period);
		jChallenge.put("img", c.getRepresentativeImgUrl().trim());
		
			JSONArray link = JSONFactoryUtil.createJSONArray();
		
				JSONObject html = JSONFactoryUtil.createJSONObject();
					html.put("rel", "self");
					html.put("format", "html");
					Formatter accessUrlFormatter = new Formatter();
						html.put("href", accessUrlFormatter.format(challengeDetailsPagePattern, c.getChallengeId()).toString());
					accessUrlFormatter.close();
				
			link.put(html);
					
				JSONObject json = JSONFactoryUtil.createJSONObject();
					json.put("rel", "self");
					json.put("format", "json");
					accessUrlFormatter = new Formatter();
						json.put("href", accessUrlFormatter.format(challengedetailURIPattern, c.getChallengeId()).toString());
					accessUrlFormatter.close();
					
			link.put(json);
		
		jChallenge.put("links", link);
		
		//From Need
		try{
			List<NeedLinkedChallenge> fromNeeds = NeedLinkedChallengeLocalServiceUtil.getNeedLinkedChallengeByChallengeId(c.getChallengeId());
			if(!fromNeeds.isEmpty()){
				NeedLinkedChallenge fromNeed = fromNeeds.get(0);
				JSONObject need = JSONFactoryUtil.createJSONObject();
					need.put("id", fromNeed.getNeedId());
					
						link = JSONFactoryUtil.createJSONArray();
						
							html = JSONFactoryUtil.createJSONObject();
							html.put("rel", "self");
							html.put("format", "html");
							accessUrlFormatter = new Formatter();
								html.put("href", accessUrlFormatter.format(needDetailsPagePattern, fromNeed.getNeedId()).toString());
							accessUrlFormatter.close();
						
						link.put(html);
						
							json = JSONFactoryUtil.createJSONObject();
							json.put("rel", "self");
							json.put("format", "json");
							accessUrlFormatter = new Formatter();
								json.put("href", accessUrlFormatter.format(needdetailURIPattern, fromNeed.getNeedId()).toString());
							accessUrlFormatter.close();
						
						link.put(json);
					
					need.put("links", link);
				
				jChallenge.put("fromNeed",need);
			}
		}
		catch(Exception e){
			_log.warning(e.toString());
		}
		
		//Related Ideas
		JSONArray relatedIdeas = JSONFactoryUtil.createJSONArray();
		try{
			List<CLSIdea> ideas = CLSChallengeLocalServiceUtil.getIdeasForChallenge(c.getChallengeId());
			for(CLSIdea idea : ideas){
				try{
					JSONObject jIdea = JSONFactoryUtil.createJSONObject();
						jIdea.put("id", idea.getIdeaID());
						
							link = JSONFactoryUtil.createJSONArray();
						
								html = JSONFactoryUtil.createJSONObject();
								html.put("format", "html");
								html.put("rel", "self");
								accessUrlFormatter = new Formatter();
									html.put("href", accessUrlFormatter.format(ideaDetailsPagePattern, idea.getIdeaID()).toString());
								accessUrlFormatter.close();
						
							link.put(html);
						
								json = JSONFactoryUtil.createJSONObject();
								json.put("format", "json");
								json.put("rel", "self");
								accessUrlFormatter = new Formatter();
									json.put("href", accessUrlFormatter.format(ideadetailURIPattern, idea.getIdeaID()).toString());
								accessUrlFormatter.close();
						
							link.put(json);
						
						jIdea.put("links", link);
					
					relatedIdeas.put(jIdea);
				}
				catch(Exception e){
					_log.warning(e.toString());
				}
			}
		}
		catch(Exception e){
			_log.warning(e.toString());
		}
		jChallenge.put("ideas", relatedIdeas);
		
		return jChallenge;
	}
	
/**
 * @param i
 * @param isneed
 * @return
 */
public static JSONObject idea2JsonObject(CLSIdea i, boolean isneed) {
		JSONObject json = JSONFactoryUtil.createJSONObject();
		
		json.put("id", i.getIdeaID());
		json.put("title", i.getIdeaTitle());
		json.put("status", i.getIdeaStatus());
		
		json.put("img", i.getRepresentativeImgUrl().trim());
		
			JSONArray link = JSONFactoryUtil.createJSONArray();
		
				JSONObject html = JSONFactoryUtil.createJSONObject();
				html.put("rel", "self");
				html.put("format", "html");
				Formatter accessUrlFormatter = new Formatter();
					if(isneed)
						html.put("href", accessUrlFormatter.format(needDetailsPagePattern, i.getIdeaID()).toString());
					else
						html.put("href", accessUrlFormatter.format(ideaDetailsPagePattern, i.getIdeaID()).toString());
				accessUrlFormatter.close();
				
			link.put(html);
				
				JSONObject j = JSONFactoryUtil.createJSONObject();
				j.put("rel", "self");
				j.put("format", "json");
				accessUrlFormatter = new Formatter();
					if(isneed)
						j.put("href", accessUrlFormatter.format(needdetailURIPattern, i.getIdeaID()).toString());
					else
						j.put("href", accessUrlFormatter.format(ideadetailURIPattern, i.getIdeaID()).toString());
				accessUrlFormatter.close();
				
			link.put(j);
		
		json.put("links", link);
		
		return json;
	}
	

/**
 * @param need
 * @return
 */
public static JSONObject need2TypeJsonObject(CLSIdea need) {
	
	JSONObject json = JSONFactoryUtil.createJSONObject();
	
	json.put("id", need.getIdeaID());
	json.put("type", "Open311:ServiceType");
	json.put("dateCreated", need.getDateAdded());
	json.put("jurisdiction_id", need.getMunicipalityOrganizationId());
	json.put("open311:type", "need");
	json.put("service_code", need.getIdeaID());
	json.put("service_name", need.getIdeaTitle());
	json.put("description", IdeasListUtils.getSocialSummaryByIdeaNotLimited(need));
	json.put("keywords", IdeasListUtils.getTagsVirgoleByIdeaId(need.getIdeaID()));
	json.put("group", IdeasListUtils.getCategoriesVirgoleByIdeaId(need.getIdeaID(), Locale.ENGLISH));
	
	JSONObject jsonAttr = JSONFactoryUtil.createJSONObject();
	
	jsonAttr.put("variable", true);
	jsonAttr.put("code", "REQUEST_NAME");
	jsonAttr.put("datatype", "string");
	jsonAttr.put("required", true);
	jsonAttr.put("order", 1);
	jsonAttr.put("description", "What is the title of the problem?");
	
	JSONArray jsonAr = JSONFactoryUtil.createJSONArray();
	jsonAr.put(jsonAttr);
	
	json.put("attributes", jsonAr);
	
	return json;
}



/**
 * @param poi
 * @param need
 * @return
 */
public static JSONObject poiToRequestJsonObject(CLSIdeaPoi poi, int version) {
	
	CLSIdea need=null;
	try {
		need = CLSIdeaLocalServiceUtil.getCLSIdea(poi.getIdeaId());
	} catch (PortalException | SystemException e) {
		e.printStackTrace();
	}
	
	JSONObject json = JSONFactoryUtil.createJSONObject();
	
	json.put("id","service-request:"+poi.getPoiId());
	json.put("type", "Open311:ServiceRequest");
	json.put("service_request_id",poi.getPoiId());
	json.put("service_name", need.getIdeaTitle());
	json.put("service_code", need.getIdeaID());
	json.put("description", poi.getDescription());
	json.put("agency_responsible", IdeasListUtils.getMunicipalityOrganizationNameByIdea(need));
	json.put("requested_datetime", poi.getDateAdded());
	json.put("updated_datetime", poi.getDateAdded());
	
	JSONObject jsonTitle = JSONFactoryUtil.createJSONObject();
	jsonTitle.put("REQUEST_NAME", poi.getTitle());
		
	json.put("attributes",jsonTitle );
	
	JSONObject poiJson = JSONFactoryUtil.createJSONObject();
	
	if (version == 1){
		
		poiJson.put("type","point:");
		
		JSONArray jsonAr = JSONFactoryUtil.createJSONArray();
		jsonAr.put(Double.parseDouble(poi.getLatitude()));
		jsonAr.put(Double.parseDouble(poi.getLongitude()));
		
		poiJson.put("coordinates",jsonAr);
		json.put("location", poiJson);	
		
	}else{
		
		json.put("location", Double.parseDouble(poi.getLongitude())+","+Double.parseDouble(poi.getLatitude()));	//geoJson format
		
	}
		
	
		
		
	return json;
}



}
