package it.eng.rspa.ideas.utils;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSCoworkerLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;
import com.liferay.portal.model.User;
import com.liferay.portlet.asset.model.AssetEntry;
import com.liferay.portlet.asset.service.AssetEntryLocalServiceUtil;
import com.liferay.portlet.messageboards.model.MBMessage;
import com.liferay.portlet.messageboards.service.MBMessageLocalServiceUtil;


public class StatsUser {

	
	public static int getNeedsNumberByUser(User user){
	
	int needNumber = 0;
	
	List<CLSIdea> ideasStats;
	try {
		ideasStats = CLSIdeaLocalServiceUtil.getIdeasByUserId(user.getUserId());
		
		for (CLSIdea idea:ideasStats ){
			
			if (idea.getIsNeed())
				needNumber++;
		}
		
		
	} catch (SystemException e) {
		
		e.printStackTrace();
	}
	

	return needNumber;
	
	}
	
	
	/**
	 * @param idea
	 * @return
	 */
	public static  List getCommentsByIdea (CLSIdea idea){
	
		DynamicQuery mbMessageQuery = DynamicQueryFactoryUtil.forClass(MBMessage.class, PortalClassLoaderUtil.getClassLoader());
		AssetEntry entryIdea = null;
		try {
			entryIdea = AssetEntryLocalServiceUtil.getEntry(CLSIdea.class.getName(), Long.valueOf(idea.getIdeaID()));
		} catch (PortalException | SystemException e) {
			e.printStackTrace();
		}
		mbMessageQuery.add(RestrictionsFactoryUtil.eq("classNameId", Long.valueOf(entryIdea.getClassNameId())));
		mbMessageQuery.add(PropertyFactoryUtil.forName("parentMessageId").ne(Long.valueOf(0)));
		mbMessageQuery.add(PropertyFactoryUtil.forName("classNameId").eq(Long.valueOf(entryIdea.getClassNameId())));
		mbMessageQuery.add(PropertyFactoryUtil.forName("classPK").eq(Long.valueOf(idea.getIdeaID())));
		mbMessageQuery.add(PropertyFactoryUtil.forName("categoryId").eq(Long.valueOf(-1)));
		
		List comments = new ArrayList<>();
		try {
			 comments = MBMessageLocalServiceUtil.dynamicQuery(mbMessageQuery);
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
		return comments;
	}
	
	
	/**
	 * @param currentUser
	 * @return
	 */
	public static  List getUserCommentsIds (User currentUser){
		
		DynamicQuery mbMessageQuery2 = DynamicQueryFactoryUtil.forClass(MBMessage.class, PortalClassLoaderUtil.getClassLoader())
				.add(PropertyFactoryUtil.forName("parentMessageId").ne(Long.valueOf(0)))
				.add(PropertyFactoryUtil.forName("userId").eq(currentUser.getUserId()))
				.setProjection(ProjectionFactoryUtil.property("messageId"));
					
				List<Long> commentIds = new ArrayList<Long>();
		
				try {
					commentIds = MBMessageLocalServiceUtil.dynamicQuery(mbMessageQuery2);
				} catch (SystemException e) {
					e.printStackTrace();
				}
		return commentIds;
		
	}
	
	/**
	 * @param ideasStats
	 * @return
	 */
	public static Map<String, Integer> getNumIdeasInStatus (List<CLSIdea> ideasStats ){
		
		Map<String, Integer> statusNum = new HashMap<String, Integer>();
		
		int monitoringIdeas = 0;
		int implementationIdeas = 0;
		int refinementIdeas = 0;
		int selectedIdeas = 0;
		int evaluationIdeas = 0;
		int elaborationIdeas = 0;
		int rejectedIdeas = 0;
		int completedIdeas = 0;
		
		for (CLSIdea idea:ideasStats){
			
			if (!idea.getIsNeed()){
				if ((idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_ELABORATION))) {
					elaborationIdeas++;
				} else if ((idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_EVALUATION))) {
					evaluationIdeas++;	
				} else if ((idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_SELECTED))) {
					selectedIdeas++;
				} else if ((idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_REFINEMENT))) {
					refinementIdeas++;	
				} else if ((idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_IMPLEMENTATION))) {
					implementationIdeas++;
				} else if ((idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_MONITORING))) {
					monitoringIdeas++;
				} else if ((idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_REJECT))) {
					rejectedIdeas++;
				} else if ((idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_COMPLETED))) {
					completedIdeas++;
				}
			}
		}
		statusNum.put(MyConstants.IDEA_STATE_ELABORATION,elaborationIdeas );
		statusNum.put(MyConstants.IDEA_STATE_EVALUATION,evaluationIdeas );
		statusNum.put(MyConstants.IDEA_STATE_SELECTED,selectedIdeas );
		statusNum.put(MyConstants.IDEA_STATE_REFINEMENT,refinementIdeas );
		statusNum.put(MyConstants.IDEA_STATE_IMPLEMENTATION,implementationIdeas );
		statusNum.put(MyConstants.IDEA_STATE_MONITORING,monitoringIdeas );
		statusNum.put(MyConstants.IDEA_STATE_REJECT,rejectedIdeas );
		statusNum.put(MyConstants.IDEA_STATE_COMPLETED,completedIdeas );
		
		return statusNum;
	}
	
	/**
	 * @param ideasStats
	 * @return
	 */
	public static Map<String, Integer> getNumsCoworkerInIdeaStatus (List<CLSIdea> ideasStats ){
		
		Map<String, Integer> statusNum = new HashMap<String, Integer>();
		
		int monitoringIdeas = 0;
		int implementationIdeas = 0;
		int refinementIdeas = 0;
		int selectedIdeas = 0;
		int evaluationIdeas = 0;
		int elaborationIdeas = 0;
		int rejectedIdeas = 0;
		int completedIdeas = 0;
		
		int numCoworker = 0;
		int ideasCoworker = 0;
		
		for (CLSIdea idea:ideasStats){
			
			if (!idea.getIsNeed()){
				
				List<CLSCoworker> coworkers = new ArrayList<CLSCoworker>();
				
				try {
					 coworkers = CLSCoworkerLocalServiceUtil.getCoworkersByIdeaId(idea.getIdeaID());
				} catch (SystemException e) {
					e.printStackTrace();
				}
				
				
				
				if(coworkers.size()>0){
					numCoworker=numCoworker+coworkers.size();
					ideasCoworker++;
				
					if ((idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_ELABORATION))) {
						elaborationIdeas++;
					} else if ((idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_EVALUATION))) {
						evaluationIdeas++;	
					} else if ((idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_SELECTED))) {
						selectedIdeas++;
					} else if ((idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_REFINEMENT))) {
						refinementIdeas++;	
					} else if ((idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_IMPLEMENTATION))) {
						implementationIdeas++;
					} else if ((idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_MONITORING))) {
						monitoringIdeas++;
					} else if ((idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_REJECT))) {
						rejectedIdeas++;
					} else if ((idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_COMPLETED))) {
						completedIdeas++;
					}
				}
			}
		}
		statusNum.put(MyConstants.IDEA_STATE_ELABORATION,elaborationIdeas );
		statusNum.put(MyConstants.IDEA_STATE_EVALUATION,evaluationIdeas );
		statusNum.put(MyConstants.IDEA_STATE_SELECTED,selectedIdeas );
		statusNum.put(MyConstants.IDEA_STATE_REFINEMENT,refinementIdeas );
		statusNum.put(MyConstants.IDEA_STATE_IMPLEMENTATION,implementationIdeas );
		statusNum.put(MyConstants.IDEA_STATE_MONITORING,monitoringIdeas );
		statusNum.put(MyConstants.IDEA_STATE_REJECT,rejectedIdeas );
		statusNum.put(MyConstants.IDEA_STATE_COMPLETED,completedIdeas );
		statusNum.put("numCoworker",numCoworker );
		statusNum.put("ideasCoworker",ideasCoworker );
		
		return statusNum;
	}
	
	
}
