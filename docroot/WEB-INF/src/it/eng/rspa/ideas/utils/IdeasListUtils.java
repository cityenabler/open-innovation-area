/**
 * 
 */
package it.eng.rspa.ideas.utils;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoi;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti;
import it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria;
import it.eng.rspa.ideas.challenges.servicelayer.model.IdeaEvaluation;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSCategoriesSetLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSCoworkerLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteIdeasLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaPoiLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.EvaluationCriteriaLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.IdeaEvaluationLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.NeedLinkedChallengeLocalServiceUtil;
import it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.ResourceRequest;

import org.apache.commons.lang3.StringEscapeUtils;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Image;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.User;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.service.SubscriptionLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.asset.model.AssetCategory;
import com.liferay.portlet.asset.model.AssetEntry;
import com.liferay.portlet.asset.model.AssetVocabulary;
import com.liferay.portlet.asset.service.AssetEntryLocalServiceUtil;
import com.liferay.portlet.asset.service.AssetVocabularyLocalServiceUtil;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.model.DLFolder;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLFolderLocalServiceUtil;
import com.liferay.portlet.ratings.service.RatingsStatsLocalServiceUtil;
import com.liferay.util.portlet.PortletProps;

/**
 * @author Engineering
 *
 */
public class IdeasListUtils {
	
	

	/**
	 * Ritorna un JSONArray con la lista delle idee da visualizzare popolando la card con Ajax
	 * @param resourceRequest
	 * @return
	 */
	public static JSONArray getListaIdeeAjax (ResourceRequest resourceRequest, String pilot, boolean isNeed,
			Map<String, String> statiFiltri, Map<Long, String> vocabFiltri,  
			Map<String, String> persFiltri, Map<String, String> sorting ){
		
		JSONArray jsonArRisp = JSONFactoryUtil.createJSONArray();
		Locale locale = resourceRequest.getLocale();
		boolean reducedLifecycle = IdeaManagementSystemProperties.getEnabledProperty("reducedLifecycle");
		
		User user = (User) resourceRequest.getAttribute(WebKeys.USER);
		
		List<CLSIdea> ideasF = new ArrayList<CLSIdea>();
		ideasF = getListaIdeeFiltrate (resourceRequest, pilot, isNeed, statiFiltri, vocabFiltri, persFiltri, sorting); //filtro le idee sulla base dei valori della mappa passati con Ajax
		
		
		for (int i = 0; i<ideasF.size(); i++){
			
			CLSIdea idea = ideasF.get(i); //mi piglio l'idea corrente
			
			JSONObject jsonIdea = JSONFactoryUtil.createJSONObject();
			
			jsonIdea.put("idIdea", idea.getIdeaID());
			
			String nomeVocabulary=LanguageUtil.get(locale, "ims.idea");
			String iconVocabulary="fa "+MyConstants.ICON_IDEA_REDUCED;
			if (!reducedLifecycle){
				 nomeVocabulary = getVocabularyNameByIdeaId(idea.getIdeaID(), locale);
				 iconVocabulary = getIconForVocabularyByIdeaId(idea.getIdeaID());
			}
			
			jsonIdea.put("Vocabulary", nomeVocabulary);
			jsonIdea.put("iconVocabulary", iconVocabulary);
			
			String colorVocabulary = getColorForVocabularyByIdeaId(idea.getIdeaID());
			jsonIdea.put("colorVocabulary",colorVocabulary);
			
			jsonIdea.put("Title", idea.getIdeaTitle());
			
			String nomeAutore = getAuthorNameByIdeaId(idea.getIdeaID());
			jsonIdea.put("Author", nomeAutore);
			
			String[] arCateg = getCategoriesByIdeaId(idea.getIdeaID(), locale);
			JSONArray jsonArCateg = JSONFactoryUtil.createJSONArray();
			
			for(String categs :arCateg){
				jsonArCateg.put(categs);
			}
			
			jsonIdea.put("Categories",jsonArCateg);
			
			String dataCreazione = getDateDDMMYYYYByIdeaId(idea.getIdeaID());
			jsonIdea.put("CreationDate", dataCreazione);
			
			
			if (isNeed){
				int ratingStar = getAverageRatingsByIdeaOrNeed(idea) ;
				jsonIdea.put("RatingsStar", ratingStar);
				
				int numChallengeNeed =NeedUtils.getNumberChallengesLinkedToNeed(idea);
				jsonIdea.put("NumGare", numChallengeNeed);
				
				int numIdeeNeed =NeedUtils.getNumberIdeasProposedToNeed(idea);
				jsonIdea.put("numIdeas", numIdeeNeed);
				
			}else{
				int ratingStar = getAverageRatingsByIdeaOrNeed(idea) ;		
				jsonIdea.put("RatingsStar", ratingStar);
			}
				
				
			String authorityName = getMunicipalityNameByIdeaAndUser(idea, user);
			jsonIdea.put("Authority", authorityName);
			
			String imgUrl = getRepresentativeImgUrlByIdeaId(idea.getIdeaID(), resourceRequest.getContextPath());
			jsonIdea.put("ImageURL", imgUrl);
			
			//Metto la descrizione social in modo da evitare richtext (img, video) etc
			jsonIdea.put("Description", getSocialSummaryByIdeaNotLimited(idea));
			
			String challengeName = getChallengeNameByIdeaId ( idea.getIdeaID());
			jsonIdea.put("ChallengeName", challengeName);
			
			String urlChallenge = getUrlChallengeByIdeaId(idea.getIdeaID());
			jsonIdea.put("ChallengeURL", urlChallenge);

			String needName = getNeedNameByIdea ( idea);
			jsonIdea.put("NeedName", needName);
			
			String urlNeed = getUrlNeedByIdea ( idea);
			jsonIdea.put("NeedURL", urlNeed);
			
			jsonIdea.put("IdeaStatus", idea.getIdeaStatus());
			
			String iconaStato = getStatusIconByIdeaStatus(idea.getIdeaStatus());
			
			jsonIdea.put("IdeaStatusIcon",iconaStato);
			jsonIdea.put("IdeaId", idea.getIdeaID());
			
			jsonIdea.put("takenUp", idea.getTakenUp());
			
			String fUIdea="";

			 if(!isNeed)
				 fUIdea = IdeaManagementSystemProperties.getFriendlyUrlIdeas(idea.getIdeaID());
			 else
				 fUIdea = IdeaManagementSystemProperties.getFriendlyUrlNeeds(idea.getIdeaID());
			 
			jsonIdea.put("isAuthorOrCoworker", isAuthorOrCoworkerByIdeaId(idea.getIdeaID(), user));
			
			jsonIdea.put("isAuthor", isAuthorByIdeaId(idea.getIdeaID(), user));
			
			jsonIdea.put("isCoworker", isCoworkerByIdeaId(idea.getIdeaID(), user));
			
			jsonIdea.put("IdeaFriendlyUrl", fUIdea);
			
			
			
			jsonArRisp.put(jsonIdea);
	
		}
		
		
		return jsonArRisp;
		
	}
	
	
	/**
	 * @param ideasI
	 * @param statiFiltri
	 * @return
	 */
	private static List<CLSIdea> getListaIdeeFiltrate (ResourceRequest resourceRequest , String pilot, boolean isNeed,
			Map<String, String> statiFiltri, Map<Long, String> vocabFiltri, 
			Map<String, String> persFiltri, Map<String, String> sorting){
				
		boolean myIdeasCitizen = Boolean.valueOf(persFiltri.get("myIdeasCitizen"));
		boolean ideasReferredToMyAuthority = Boolean.valueOf(persFiltri.get("ideasReferredToMyAuthority"));
		boolean ideasReferredToMeAuthority = Boolean.valueOf(persFiltri.get("ideasReferredToMeAuthority"));
		boolean favoriteIdeas = Boolean.valueOf(persFiltri.get("favoriteIdeas"));
		boolean onlyWithoutChallenge = Boolean.valueOf(persFiltri.get("onlyWithoutChallenge"));
		boolean onlyMyPilotF = Boolean.valueOf(persFiltri.get("onlyMyPilot"));
		boolean publicIdeasEnabled = IdeaManagementSystemProperties.getEnabledProperty("publicIdeasEnabled");
		boolean reducedLifecycle = IdeaManagementSystemProperties.getEnabledProperty("reducedLifecycle");

		List<CLSIdea> ideasI = new ArrayList<CLSIdea>();
		List<CLSIdea> ideasFav = new ArrayList<CLSIdea>();
		List<CLSIdea> ideasPers = new ArrayList<CLSIdea>();
		
		User user = (User) resourceRequest.getAttribute(WebKeys.USER);
		
		
		try {
			
			if (!publicIdeasEnabled){
				
				if (Validator.isNull(user))
					return ideasI;//empty list
				
				long userId = user.getUserId();
				
				if (favoriteIdeas)
					ideasI = CLSFavouriteIdeasLocalServiceUtil.getFavouriteIdeasByUserId(userId);
				else
					ideasI=getListaMieIdeePlusCuiCollaboroByUserId(userId);
			}else{
			
			
				if (myIdeasCitizen || ideasReferredToMeAuthority || favoriteIdeas){
					
					long userId = user.getUserId();
					
					if (favoriteIdeas)
						ideasFav = CLSFavouriteIdeasLocalServiceUtil.getFavouriteIdeasByUserId(userId);
					
					if (myIdeasCitizen && !favoriteIdeas && !ideasReferredToMeAuthority){
						
						ideasI=getListaMieIdeePlusCuiCollaboroByUserId(userId);
						
					}else if (!myIdeasCitizen && favoriteIdeas && !ideasReferredToMeAuthority){
						ideasI = ideasFav;
					}else if(myIdeasCitizen && favoriteIdeas && !ideasReferredToMeAuthority){
						
						ideasPers=getListaMieIdeePlusCuiCollaboroByUserId(userId);
						
						ideasI = new ArrayList<CLSIdea>(ideasFav);
						ideasI.retainAll(ideasPers);//fa l'intersezione fra le due liste
						
					}else if(!myIdeasCitizen && !favoriteIdeas && ideasReferredToMeAuthority){
						
						ideasI=CLSIdeaLocalServiceUtil.getIdeasByMunicipalityId(userId);
					}else if(!myIdeasCitizen && favoriteIdeas && ideasReferredToMeAuthority){
						
						ideasPers=CLSIdeaLocalServiceUtil.getIdeasByMunicipalityId(userId);
						ideasI = new ArrayList<CLSIdea>(ideasFav);
						
						ideasI.retainAll(ideasPers);//fa l'intersezione fra le due liste
					}
						
				
				}else{//sono l'utente guest e le tiro tutte
					
					ideasI= CLSIdeaLocalServiceUtil.getCLSIdeas(0, CLSIdeaLocalServiceUtil.getCLSIdeasCount());
					
				}
			}
			
			//allargo le idee a quelle dell'Organization Authority
			if (ideasReferredToMyAuthority){
				
				long idOrganization = MyUtils.getOrganizationIdByMunicipalityId(user.getUserId());
				ideasPers=CLSIdeaLocalServiceUtil.getIdeasByMunicipalityOrganizationId(idOrganization);

				ideasI = MyUtils.getIdeaListWithoutDuplicates( ideasI, ideasPers);//unione
				
			}
			
			
			
		} catch (SystemException e) {
			
			e.printStackTrace();
			return ideasI;
		}
		
		
		List<CLSIdea> ideeFiltrate = new ArrayList<CLSIdea>();
		
		
			for (int i = 0; i<ideasI.size(); i++){
			
				CLSIdea idea = ideasI.get(i); 
				
				String statoIdea = idea.getIdeaStatus();
				
				Long vocId = getVocabularyIdByIdeaId(idea.getIdeaID());
				
				boolean valoreFiltroSenzaGara = false;
				
				boolean valoreFiltroStato = Boolean.valueOf(statiFiltri.get(statoIdea));
				boolean valoreFiltroVocab = Boolean.valueOf(vocabFiltri.get(vocId));
				
				if(reducedLifecycle)
					valoreFiltroVocab=true;//all categories
				
				boolean filtroOnlyMyPilot = false;
				boolean isIdeaOfUserPilot = isIdeaOfCurrentPilot(idea, pilot);
				
				if (isNeed){
					valoreFiltroStato=true;//per i need non abbiamo stato
					onlyMyPilotF = true; //abbiamo need solo all'interno dello stesso pilot
					
				}
				
				if (( onlyMyPilotF && isIdeaOfUserPilot) || (!onlyMyPilotF) )
					filtroOnlyMyPilot = true;
				
				
				if ((onlyWithoutChallenge && (idea.getChallengeId() <=0)) || !onlyWithoutChallenge )
					valoreFiltroSenzaGara = true;
				
				
				if (valoreFiltroStato && valoreFiltroVocab && valoreFiltroSenzaGara && filtroOnlyMyPilot && (idea.isIsNeed() == isNeed))
					ideeFiltrate.add(idea);
				
			}
		
			
			//Filtraggio per pilot
			ideeFiltrate = MyUtils.getListaIdeeFiltrataPerPilotLingue(ideeFiltrate, user, pilot);
			
			///////////  SORTING ///////////
			boolean sortByDate = Boolean.valueOf(sorting.get("sortByDate"));
			boolean sortDateAsc = Boolean.valueOf(sorting.get("sortDateAsc"));
			boolean sortByRating = Boolean.valueOf(sorting.get("sortByRating"));
			boolean sortRatingLower = Boolean.valueOf(sorting.get("sortRatingLower"));
			

			if (sortByDate){
			
					
				IdeaComparator ideaComparator= new IdeaComparator();
				ideeFiltrate=ListUtil.sort(ideeFiltrate, ideaComparator);	
				
				if (sortDateAsc)			
					Collections.reverse(ideeFiltrate);//ordine inverso della lista ordinata per data
			
			
			}else if (sortByRating){
				
				IdeaComparatorByRating ideaComparatorBR = new IdeaComparatorByRating();
				ideeFiltrate=ListUtil.sort(ideeFiltrate, ideaComparatorBR);
				
				
				if (sortRatingLower)
					Collections.reverse(ideeFiltrate);//ordine inverso della lista ordinata per rating
				
			}
			
			
			return ideeFiltrate;
		
	}
	
	
	
	/**
	 * @param ideaId
	 * @param locale
	 * @return
	 */
	public static String getVocabularyNameByIdeaId(long ideaId, Locale locale){
		
			String vocName = "";
			long vocId = getVocabularyIdByIdeaId(ideaId);
			vocName = getVocabularyNameByVocId(vocId,  locale);
		
		return vocName;
	}
	
	/**
	 * @param ideaId
	 * @param locale
	 * @return
	 */
	public static String getVocabularyNameByVocId(long vocId, Locale locale){
		
			String nomeVocabolario = "";
			
			if (vocId>-1){
			
				AssetVocabulary voc = null; 
				
				try {
					 voc = AssetVocabularyLocalServiceUtil.getVocabulary(vocId);
				} catch (PortalException | SystemException e) {
					
					e.printStackTrace();
					return null;
				}
				
				nomeVocabolario = voc.getTitle(locale);
			}
		
		return nomeVocabolario;
	}
	
	
	
	
	/**
	 * @param ideaId
	 * @return
	 */
	public static long getVocabularyIdByIdeaId(long ideaId){
		
			long vocId = -1;
			
			AssetEntry entry = null;
			try {
				entry = AssetEntryLocalServiceUtil.getEntry(CLSIdea.class.getName(), ideaId);
			} catch (PortalException | SystemException e) {
				
				System.out.println("Error on getVocabularyIdByIdeaId on ideaId "+ideaId);
				
				e.printStackTrace();
				return  vocId;
			}
			
			
			List<AssetCategory> categoriesList = new ArrayList<AssetCategory>();
			try {
				categoriesList = entry.getCategories();
			} catch (SystemException e) {
				
				e.printStackTrace();
				return vocId;
			}
			
			
			if (categoriesList.size() > 0){
				
				AssetCategory ac = 	categoriesList.get(0); //il vocabolario puo' essere al massimo uno
				
				 vocId = ac.getVocabularyId();
			}
			
			
		return vocId;

	}
	
	
	/**
	 * @param vocId
	 * @return
	 */
	public static String  getIconForVocabularyByVocabulariId (long vocId ){
		
		String vocIcon = "";
		CLSCategoriesSet catSet;
		try {
			catSet = CLSCategoriesSetLocalServiceUtil.getCLSCategoriesSet(vocId);
			vocIcon = catSet.getIconName();
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
		}
		
		return vocIcon;
		
	}
		
	
	/**
	 * @param vocId
	 * @return
	 */
	public static String  getColorForVocabularyByVocabulariId (long vocId ){
		
		String vocColor = "";
		CLSCategoriesSet catSet;
		try {
			catSet = CLSCategoriesSetLocalServiceUtil.getCLSCategoriesSet(vocId);
			vocColor = catSet.getColourName();
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
		}
		
		return vocColor;
		
	}
	
	
	/**
	 * @param ideaId
	 * @return
	 */
	public static String getIconForVocabularyByIdeaId( long ideaId){
		
		
		long vocId = getVocabularyIdByIdeaId(ideaId);
		
		String vocIcon = getIconForVocabularyByVocabulariId(vocId);
		
		
		return vocIcon;
	}
	
	/**
	 * @param ideaId
	 * @return
	 */
	public static String getColorForVocabularyByIdeaId( long ideaId){
		
		long vocId = getVocabularyIdByIdeaId(ideaId);
		String vocColor = getColorForVocabularyByVocabulariId(vocId);
		
		return vocColor;

	}
	
	
	
	
	
	
	
	
	/**
	 * @param ideaId
	 * @param locale
	 * @return
	 */
	public static String[] getCategoriesByIdeaId(long ideaId, Locale locale){
		
		String [] categs = new String [0] ;
		
		AssetEntry entry = null;
		try {
			entry = AssetEntryLocalServiceUtil.getEntry(CLSIdea.class.getName(), ideaId);
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
			return categs;
			
		}

		
		List<AssetCategory> categoriesList = new ArrayList<AssetCategory>();
		try {
			categoriesList = entry.getCategories();
		} catch (SystemException e) {
			
			e.printStackTrace();
			return categs;
		}
		
		 categs = new String [categoriesList.size()] ;
		
		
		for (int i=0; i < categoriesList.size(); i++){
			
			String nomeCat = categoriesList.get(i).getTitle(locale);
			categs[i]=nomeCat;
			
		}
		
		return categs;
		
	}
		
	
	/**
	 * @param ideaId
	 * @return
	 */
	public static long[] getCategoriesIdByIdeaId(long ideaId){
		
		long [] categs = new long [0] ;
		
		AssetEntry entry = null;
		try {
			entry = AssetEntryLocalServiceUtil.getEntry(CLSIdea.class.getName(), ideaId);
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
			return categs;
			
		}

		
		List<AssetCategory> categoriesList = new ArrayList<AssetCategory>();
		try {
			categoriesList = entry.getCategories();
		} catch (SystemException e) {
			
			e.printStackTrace();
			return categs;
		}
		
		 categs = new long [categoriesList.size()] ;
		
		
		for (int i=0; i < categoriesList.size(); i++){
			
			long catId = categoriesList.get(i).getCategoryId();
			categs[i]=catId;
			
		}
		
		return categs;
		
	}
	
	
	/**
	 * @param idea
	 * @return
	 */
	public static String getAuthorNameByIdeaId(long ideaId){
		
		String name ="";
		
		CLSIdea idea = null;
		try {
			idea = CLSIdeaLocalServiceUtil.getIdeasByIdeaId(ideaId);
		} catch (SystemException e1) {
			
			e1.printStackTrace();
			return name;
		}
		
		return getAuthorNameByIdea(idea);
		
		
	}
	
	/**
	 * @param idea
	 * @return
	 */
	public static String getAuthorNameByIdea(CLSIdea idea){
		
		String name ="";
		
		try {
			User user = UserLocalServiceUtil.getUser(idea.getUserId());
			name=user.getFullName();
			
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
			return name;
		}
		
		return name;
	}
	
	/**
	 * @param idea
	 * @return
	 */
	public static String getAuthorEmailByIdea(CLSIdea idea){
		
		String email ="";
		
		try {
			User user = UserLocalServiceUtil.getUser(idea.getUserId());
			email=user.getEmailAddress();
			
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
			return email;
		}
		
		return email;
	}
	
	
	/**
	 * @param ideaId
	 * @return
	 */
	public static String getDateDDMMYYYYByIdeaId (long ideaId){
		
		String data = "";
		
		CLSIdea idea = null;
		try {
			idea = CLSIdeaLocalServiceUtil.getIdeasByIdeaId(ideaId);
		} catch (SystemException e1) {
			
			e1.printStackTrace();
			return data;
		}
		
		SimpleDateFormat t = new SimpleDateFormat("dd/MM/yyyy");
		
		 data = t.format(idea.getDateAdded());
		
		
		return data;
	}

		
	
	
	/**
	 * @param need
	 * @return
	 */
	public static double getAverageDoubleRatingsByIdeaOrNeed (CLSIdea need){
		
		
		return getAverageDoubleRatingsByIdeaOrNeedId(need.getIdeaID());
	}
	
	
	/**
	 * @param ideaId
	 * @return
	 */
	public static double getAverageDoubleRatingsByIdeaOrNeedId (long ideaId){
		
		double ratingsStats=0;

		try {
			ratingsStats = RatingsStatsLocalServiceUtil.getStats(CLSIdea.class.getName(), ideaId).getAverageScore();
		} catch (SystemException e) {
			
			e.printStackTrace();
			return ratingsStats;
		}
		
		return ratingsStats;
	}
	
	
	/**
	 * @param ideaId
	 * @return
	 */
	public static int getNumberOfRatingsByIdeaOrNeedId (long ideaId){
		
		int ratingsStats=0;

		try {
			ratingsStats = RatingsStatsLocalServiceUtil.getStats(CLSIdea.class.getName(), ideaId).getTotalEntries();
		} catch (SystemException e) {
			
			e.printStackTrace();
			return ratingsStats;
		}
		
		return ratingsStats;
	}
	
	
	/**
	 * @param ideaId
	 * @return
	 */
	public static int getAverageRatingsByIdeaOrNeed (CLSIdea need){
		
		int black = 0;
		
		double average = getAverageDoubleRatingsByIdeaOrNeed(need);
		
		black=(int) Math.rint(average); //full stars
		
		return black;
	}
	
	
	/**
	 * @param entry
	 * @return
	 */
	public static double getAverageDoubleRatingsByDLFileEntry (DLFileEntry entry){
		
		double ratingsStats=0;

		try {
			ratingsStats = RatingsStatsLocalServiceUtil.getStats(DLFileEntry.class.getName(), entry.getFileEntryId()).getAverageScore();
			
		} catch (SystemException e) {
			
			e.printStackTrace();
			return ratingsStats;
		}
		
		return ratingsStats;
	}
	
	/**
	 * @param ideaId
	 * @return
	 */
	public static String getMunicipalityNameByIdea (CLSIdea idea){
		
		String name ="";
		
		try {
			User user = UserLocalServiceUtil.getUser(idea.getMunicipalityId());
			name=user.getFullName();
			
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
			return name;
		}
		
		return name;
	}
	
	/**
	 * @param ideaId
	 * @return
	 */
	public static String getMunicipalityOrganizationNameByIdea(CLSIdea idea){
		
		String name ="";		
		
		Organization org = null;
		try {
			 org = OrganizationLocalServiceUtil.getOrganization(idea.getMunicipalityOrganizationId());
			 
		} catch (PortalException | SystemException e) {

			e.printStackTrace();
			return name;
		}
		name = org.getName();
		return name;
	}
	
	
	
	/**
	 * @param ideaId
	 * @return
	 */
	public static String getMunicipalityNameByIdeaAndUser (CLSIdea idea, User user){
		
		String name = getMunicipalityOrganizationNameByIdea (idea);
		
		boolean isEnte = MyUtils.isAuthority(user);
		
		if (isEnte && (idea.getMunicipalityId() > 0)){
			
			name = getMunicipalityNameByIdea(idea)+" - "+name;
		}
		
		return name;
		
		
	}
	
	
	/**
	 * First element the name
	 * Second element the Url
	 * 
	 * @param req
	 * @return
	 */
	public static List<String> getAuthorRequirementNameAndUrl (CLSRequisiti req){
		
		
		 List<String> listRet = new ArrayList<String>();
		 String name = "";
		 String url = "";
		
		long reqAuthorUserId = req.getAuthorUser();
		
		if(reqAuthorUserId>0  ){	
			
			User reqAuthorUser = null;
			try {
				reqAuthorUser = UserLocalServiceUtil.getUser(reqAuthorUserId);
			} catch (PortalException | SystemException e) {
				
				e.printStackTrace();
				return listRet;
			}
			
			boolean isEnte = MyUtils.isAuthority(reqAuthorUser);
			boolean isCompanyLeader = MyUtils.isCompanyLeader(reqAuthorUser);
			
			if (isEnte || isCompanyLeader){
				
				CLSIdea idea=null;
				try {
					idea = CLSIdeaLocalServiceUtil.getCLSIdea(req.getIdeaId());
				} catch (PortalException | SystemException e) {
					
					e.printStackTrace();
					return listRet;
				}
				
				name = getMunicipalityOrganizationNameByIdea(idea);
				url = "";
				
				listRet.add(name);
				listRet.add(url);
				
				
			}			
			else{
				
				listRet.add(reqAuthorUser.getFullName());
				listRet.add(MyUtils.getUserProfileUrlByUser(reqAuthorUser));
				
			}
				 
		}
		
		
		return listRet;
		
	}
	
	
	/**
	 * @param ideaId
	 * @return
	 */
	public static String getRepresentativeImgUrlByIdeaId (long ideaId){
		
		return getRepresentativeImgUrlByIdeaId(ideaId, "/Challenge62-portlet");
	}

	/**
	 * @param idea
	 * @param actionRequest
	 * @return
	 */
	public static Image getRepresentativeImageByIdea(CLSIdea idea, ActionRequest actionRequest){
		
		String realPath = actionRequest.getPortletSession().getPortletContext().getRealPath("/img/ideasImg.png");
		return getRepresentativeImageByIdea(idea, realPath );
	}
	
	/**
	 * @param idea
	 * @param renderRequest
	 * @return
	 */
	public static Image getRepresentativeImageByIdea(CLSIdea idea, RenderRequest renderRequest){
		
		String realPath = renderRequest.getPortletSession().getPortletContext().getRealPath("/img/ideasImg.png");
		return getRepresentativeImageByIdea(idea, realPath );
	}
	
	
	/**
	 * @param ideaId
	 * @param resourceRequest
	 * @return
	 */
	public static Image getRepresentativeImageByIdea(CLSIdea idea, String realPath){
		
		Image image1 = null;
		if (idea.getRepresentativeImgUrl().isEmpty()){
		
			try {
				image1 = Image.getInstance(realPath);
			} catch (BadElementException | IOException e) {
				e.printStackTrace();
			}
		}else{
			
			
			String imgTitle = getDLRepresentativeImgByIdea(idea);
			List<DLFileEntry> files = MyUtils.getDLFileByTitle(imgTitle);
			
			
			if (files.size()>0){
				
				DLFileEntry dLFileEntry = files.get(0);

				
				byte[] bytesImg = null;
				try {
					bytesImg = FileUtil.getBytes(dLFileEntry.getContentStream());
				} catch (PortalException | SystemException | IOException e) {
					e.printStackTrace();
				}
				
				try {
					image1 =  Image.getInstance(bytesImg);
				} catch (BadElementException | IOException e) {
					e.printStackTrace();
				}
				
			} //files.size()>0
			
		}
		
		
		image1.scalePercent(15);   
		image1.setAlignment(image1.ALIGN_CENTER);
		
		
		return image1;
	}
	
	
	/**
	 * @param idea
	 */
	public static String getDLRepresentativeImgByIdea (CLSIdea idea){
		
		//		/documents/20181/38023/img_1505318378578.jpg
		String imgUrl = idea.getRepresentativeImgUrl();
		
		String imgName =  getImgNameByUrl(imgUrl);
		
		return imgName;
		
	}
	
	
	/**
	 * from	/documents/20181/38023/img_1505318378578.jpg
	 * to img_1505318378578.jpg
	 * 
	 * @param localUrl
	 * @return
	 */
	public static String getImgNameByUrl (String localUrl){
		
		
		
		String imgurlRev = new StringBuilder(localUrl).reverse().toString();
		String subS = imgurlRev.substring(0, imgurlRev.indexOf("/"));
		String imgurlS = new StringBuilder(subS).reverse().toString();
		
		return imgurlS;
		
	}
	
	

	/**
	 * @param ideaId
	 * @param resourceRequest
	 * @return
	 */
	public static String getRepresentativeImgUrlByIdeaId (long ideaId, String contextPath){
		
		String representativeImgUrl =contextPath+ "/img/ideasImg.png";
		
		
		
		CLSIdea idea = null;
		try {
			idea = CLSIdeaLocalServiceUtil.getIdeasByIdeaId(ideaId);
		} catch (SystemException e1) {
			
			e1.printStackTrace();
			return representativeImgUrl;
		}
		
		boolean isNeed = idea.getIsNeed();
		
		
		
		if(!idea.getRepresentativeImgUrl().trim().equals("")){
			representativeImgUrl = idea.getRepresentativeImgUrl();
		}else {
			
			if (!isNeed)
				representativeImgUrl =contextPath+ "/img/ideasImg.png";
			else			
				representativeImgUrl =contextPath+ "/img/needsImg.png";
		}
		
		return representativeImgUrl;
	}

	/**
	 * @param ideaId
	 * @param resourceRequest
	 * @return
	 */
	public static String getChallengeNameByIdeaId (long ideaId){
		
		String name ="";
		
		
		CLSIdea idea = null;
		try {
			idea = CLSIdeaLocalServiceUtil.getIdeasByIdeaId(ideaId);
		} catch (SystemException e1) {
			
			e1.printStackTrace();
			return name;
		}
		
		long challengeId = idea.getChallengeId();
		
		if (challengeId > -1){
			
			CLSChallenge gara = null;
			try {
				gara = CLSChallengeLocalServiceUtil.getCLSChallenge(challengeId);
			} catch (SystemException | PortalException e1) {
				
				e1.printStackTrace();
				return name;
			}	
			
			name = gara.getChallengeTitle();
		}
		
		return name;
	}

	
	/**
	 * @param ideaId
	 * @return
	 */
	public static String getUrlChallengeByIdeaId (long ideaId){
		
		String url ="";
		
		
		CLSIdea idea = null;
		try {
			idea = CLSIdeaLocalServiceUtil.getIdeasByIdeaId(ideaId);
		} catch (SystemException e1) {
			
			e1.printStackTrace();
			return url;
		}
		
		long challengeId = idea.getChallengeId();
		
		if (challengeId > -1){
			
			url = IdeaManagementSystemProperties.getFriendlyUrlChallenges(challengeId);
		}

		return url;
	}


/**
 * @param idea
 * @return
 */
public static String getNeedNameByIdea (CLSIdea idea){
		
		String name ="";
		
		long linkedNeedId = idea.getNeedId();
		
		if (linkedNeedId > 0){
			
			CLSIdea need = null;
			try {
				need = CLSIdeaLocalServiceUtil.getCLSIdea(linkedNeedId);
			} catch (SystemException | PortalException e1) {
				
				e1.printStackTrace();
				return name;
			}	
			
			name = need.getIdeaTitle();
		}
		
		return name;
	}

	
	/**
	 * @param ideaId
	 * @return
	 */
	public static String getUrlNeedByIdea (CLSIdea idea){
		
		String url ="";
		
		
		long linkedNeedId = idea.getNeedId();
		
		if (linkedNeedId > 0){
			
			url = IdeaManagementSystemProperties.getFriendlyUrlNeeds(linkedNeedId);
		}

		return url;
	}
	
	
	/**
	 * @param ideaStatus
	 * @return
	 */
	public static String getStatusIconByIdeaStatus(String ideaStatus){

		
		Map<String, String> statiIcone = new HashMap<String, String>();
		
		statiIcone.put(MyConstants.IDEA_STATE_ELABORATION, "icon-lightbulb");
		statiIcone.put(MyConstants.IDEA_STATE_REFINEMENT, "icon-retweet");
		statiIcone.put(MyConstants.IDEA_STATE_IMPLEMENTATION, "icon-cogs");
		statiIcone.put(MyConstants.IDEA_STATE_MONITORING, "icon-desktop");
		statiIcone.put(MyConstants.IDEA_STATE_COMPLETED, "icon-ok-sign");
		statiIcone.put(MyConstants.IDEA_STATE_REJECT, "fa fa-hand-paper-o");
		
		return statiIcone.get(ideaStatus);
	}
	
	/**
	 * @param ideaId
	 * @param locale
	 * @return
	 */
	public static String getCategoriesVirgoleByIdeaId(long ideaId, Locale locale){

		String[] categorie = IdeasListUtils.getCategoriesByIdeaId(ideaId, locale);

		String allcategorie = ""; 
		for (int i =0; i<categorie.length; i++ ){
			
			if (i!=categorie.length-1){
				allcategorie+=categorie[i]+",&nbsp;";
			}else{
				allcategorie+=categorie[i];
				
			}
		}
		 
		 return allcategorie;
	}
	
	/**
	 * @param ideaId
	 * @return
	 */
	public static String getCategoriesIdCommasByIdeaId(long ideaId){

		long[] categorie = getCategoriesIdByIdeaId(ideaId);

		String allcategories = ""; 
		for (int i =0; i<categorie.length; i++ ){
			
			if (i!=categorie.length-1){
				allcategories+=categorie[i]+",";
			}else{
				allcategories+=categorie[i];
				
			}
		}
		 
		 return allcategories;
	}
	
	
	/**
	 * @param ideaId
	 * @param locale
	 * @return
	 */
	public static String getCategoriesCommasForArrayJSByIdeaId(long ideaId, Locale locale){

		String[] categorie = IdeasListUtils.getCategoriesByIdeaId(ideaId, locale);

		String allcategorie = ""; 
	
		
		for (int i =0; i<categorie.length; i++ ){
			
			if (i!=categorie.length-1){
				allcategorie+="\""+categorie[i]+"\",";
			}else{
				allcategorie+="\""+categorie[i]+"\"";
				
			}
		}
		
		 
		 return allcategorie;
	}
	
	/**
	 * @param ideaId
	 * @return
	 */
	public static String getTagsVirgoleByIdeaId(long ideaId){

		String[] tags = {""} ;
		String allTags = ""; 
		
		AssetEntry entry;
		try {
			entry = AssetEntryLocalServiceUtil.getEntry(CLSIdea.class.getName(), ideaId);
			tags = entry.getTagNames();
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
			return allTags;
		}
		
		for (int i =0; i<tags.length; i++ ){
			
			if (i!=tags.length-1){
				allTags+=tags[i]+",&nbsp;";
			}else{
				allTags+=tags[i];
				
			}
		}

		 
		 return allTags;
	}
	
	/**
	 * @param ideaId
	 * @return
	 */
	public static String getTagsCommasForArrayJSByIdeaId(long ideaId){

		String[] tags = {""} ;
		String allTags = ""; 
		
		AssetEntry entry;
		try {
			entry = AssetEntryLocalServiceUtil.getEntry(CLSIdea.class.getName(), ideaId);
			tags = entry.getTagNames();
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
			return allTags;
		}
		
		for (int i =0; i<tags.length; i++ ){
			
			if (i!=tags.length-1){
				allTags+="\""+tags[i]+"\",";
			}else{
				allTags+="\""+tags[i]+"\"";
				
			}
		}

		 
		 return allTags;
	}
	
	
	/**
	 * @param ideaId
	 * @return
	 */
	public static JSONArray getTagsByIdeaId(long ideaId){

		JSONArray allTags = JSONFactoryUtil.createJSONArray();
		
		
		String[] tags = {""} ;
		
		AssetEntry entry;
		try {
			entry = AssetEntryLocalServiceUtil.getEntry(CLSIdea.class.getName(), ideaId);
			tags = entry.getTagNames();
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
			return allTags;
		}
		
		for (int i =0; i<tags.length; i++ ){
			
			allTags.put(tags[i]);
			
		}

		 
		 return allTags;
	}
	
	/**
	 * @param ideaId
	 * @param acronimLanguage
	 * @return
	 */
	public static JSONArray getThemeCategoriesTagsByIdeaIdAcronimLanguage(long ideaId, String acronimLanguage){

		JSONArray allElements = JSONFactoryUtil.createJSONArray();
		
		Locale loc = MyUtils.getLocaleByAcronimLanguage(acronimLanguage);
		
		//Theme-Vocabulary
		String theme = IdeasListUtils.getVocabularyNameByIdeaId(ideaId, loc);
		allElements.put(theme);
		
		
		//Categories
		String[] categories = IdeasListUtils.getCategoriesByIdeaId(ideaId, loc);

		for (int i =0; i<categories.length; i++ ){
			
			allElements.put(categories[i]);
		}
		 

		//tags
		String[] tags = {""} ;
		
		AssetEntry entry;
		try {
			entry = AssetEntryLocalServiceUtil.getEntry(CLSIdea.class.getName(), ideaId);
			tags = entry.getTagNames();
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
		}
		
		for (int i =0; i<tags.length; i++ ){
			
			allElements.put(tags[i]);
			
		}

		 
		 return allElements;
	}
	
	/**
	 * @param ideaId
	 * @param currentUser
	 * @return
	 */
	public static boolean isEntityReferencebyIdeaId(long ideaId, User currentUser){
		
		CLSIdea idea;
		try {
			idea = CLSIdeaLocalServiceUtil.getCLSIdea(ideaId);
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
			return false;
		}
		
		return isEntityReferencebyIdea(idea, currentUser);
	}
	
	/**
	 * @param ideaId
	 * @param currentUser
	 * @return
	 */
	public static boolean isEntityReferencebyIdea(CLSIdea idea, User currentUser){
		
		if( Validator.isNull(currentUser) )	
			return false;
		
		//se idea e' assegnata
		if (idea.getMunicipalityId() > 0){
			
			if (currentUser.getUserId()==idea.getMunicipalityId())
				return true;
		}else{
			
			return MyUtils.isUserOfOrganizationAuthority(currentUser, idea.getMunicipalityOrganizationId());
		}
		
		return false;	
	}
	
	
	/**
	 * @param ideaId
	 * @param groupId
	 * @return
	 */
	public static List<DLFileEntry> getFilesByIdeaId (long ideaId, long groupId){
		
		List<DLFileEntry> files = new ArrayList<DLFileEntry>();
		
		CLSIdea idea = null;
		try {
			idea = CLSIdeaLocalServiceUtil.getIdeasByIdeaId(ideaId);
		} catch (SystemException e1) {
			
			e1.printStackTrace();
			return files;
		}
		
		Long folderId = idea.getIdFolder();
		Integer count = 0;
		try {
			count = DLFileEntryLocalServiceUtil.getFileEntriesCount(groupId, folderId);
		} catch (SystemException e) {
			
			e.printStackTrace();
			return files;
		}
		OrderByComparator obc = OrderByComparatorFactoryUtil.create("DLFileEntry", "title", true);
		
		
		
		try {
			 files = DLFileEntryLocalServiceUtil.getFileEntries(groupId, folderId.longValue(), 0, count,obc);
		} catch (SystemException e) {
			
			e.printStackTrace();
			return files;
		}

		return files;
		
		
	}
	
	/**
	 * @param ideaId
	 * @param currentUser
	 * @return
	 */
	public static boolean isAuthorByIdeaId (long ideaId, User currentUser){
		
		if(currentUser == null)
			return false;
		
		CLSIdea idea = null;
		try {
			idea = CLSIdeaLocalServiceUtil.getIdeasByIdeaId(ideaId);
		} catch (SystemException e1) {
			
			e1.printStackTrace();
			return false;
		}
		//vedo se sia autore
		if (currentUser.getUserId() == idea.getUserId())
			return true;
		
		
		return false;
	}
	
	/**
	 * @param ideaId
	 * @param currentUser
	 * @return
	 */
	public static boolean isCoworkerByIdeaId (long ideaId, User currentUser){
		
		
		if(currentUser == null)
			return false;
		
		
		
		List<CLSCoworker> cwks = new ArrayList<CLSCoworker>();
		
		try {
			cwks = CLSCoworkerLocalServiceUtil.getCoworkersByIdeaId(ideaId);
		} catch (SystemException e) {
			
			e.printStackTrace();
			return false;
		}
		
		
		if(cwks.size() == 0){
			return false; 
		}else{
				
			for (CLSCoworker cwk: cwks){
				
				if (cwk.getUserId() == currentUser.getUserId())
					return true;
			}
		}
		
		return false;
	}
	
	
	/**
	 * @param ideaId
	 * @param currentUser
	 * @return
	 */
	public static boolean isAuthorOrCoworkerByIdeaId (long ideaId, User currentUser){
		
		if(currentUser == null)
			return false;
		
		boolean isAuthor = isAuthorByIdeaId( ideaId,  currentUser);
		boolean isCoworker = isCoworkerByIdeaId(ideaId, currentUser);
		
		
		boolean isAuthorOrCoworker = isAuthor || isCoworker;
		
		return isAuthorOrCoworker;
	}
	
	/**
	 * @param ideaId
	 * @param currentUser
	 * @return
	 */
	public static boolean isAuthorOrCoworkerOrAdminByIdeaId (long ideaId, User currentUser){
		
		if( Validator.isNull(currentUser))
			return false;
		
		
		if (PortalUtil.isOmniadmin(currentUser.getUserId()))
			return true;
		
		return isAuthorOrCoworkerByIdeaId(ideaId, currentUser);
	}
	
	/**
	 * @param ideaId
	 * @param currentUser
	 * @return
	 */
	public static boolean isFavoriteIdeas (long ideaId, User currentUser){
		
		if(currentUser == null)
			return false;
		
		List<CLSFavouriteIdeas> favIdeas = new ArrayList<CLSFavouriteIdeas>();
		try {
			favIdeas = CLSFavouriteIdeasLocalServiceUtil.getFavouriteIdeas(ideaId, currentUser.getUserId());
		} catch (SystemException e) {
			
			e.printStackTrace();
			return false;
		}
				
				
		if (favIdeas.size() >0)
			return true;
		
		return false;
	}
	
	
	
	/**
	 * @param idea
	 * @return
	 */
	public static String getSocialSummaryByIdea(CLSIdea idea){
		
		
		String summary = idea.getIdeaDescription().replaceAll("[\\r\\n]", "") ;
		summary = StringEscapeUtils.unescapeHtml4(summary);
		if(summary!=null){
			if(summary.trim()!=""){
				String regex ="\\<[^\\>]*\\>";
		   		String replacement = "";
		   		summary = summary.replaceAll(regex, replacement);
		   		summary = summary.replaceAll("\"", replacement);
				summary = StringUtil.shorten(HtmlUtil.stripHtml(summary), 200);
			}else{
				summary="";
			}
		}else{
			summary="";
		}
		
		return summary;
		
		
	}
	
	/**
	 * @param idea
	 * @return
	 */
	public static String getSocialSummaryByIdeaNotLimited(CLSIdea idea){
		
		
		String summary = idea.getIdeaDescription().replaceAll("[\\r\\n]", "") ;
		summary = StringEscapeUtils.unescapeHtml4(summary);
		if(summary!=null){
			if(summary.trim()!=""){
				String regex ="\\<[^\\>]*\\>";
		   		String replacement = "";
		   		summary = summary.replaceAll(regex, replacement);
		   		summary = summary.replaceAll("\"", replacement);
				//summary = StringUtil.shorten(HtmlUtil.stripHtml(summary), 200);
			}else{
				summary="";
			}
		}else{
			summary="";
		}
		
		return summary;
		
		
	}
	
	/**
	 * @param idea
	 * @return
	 */
	public static long getScreenshotFolderIdByIdea (CLSIdea idea){
		
		long screenFolderId = 0;
		
		List<DLFolder> dLFolderSS = new ArrayList<DLFolder>();
		
		//controllo se la cartella utente esiste
		 DynamicQuery queryUtente = DynamicQueryFactoryUtil.forClass(DLFolder.class)
				 .add(PropertyFactoryUtil.forName("groupId").eq(new Long(idea.getGroupId())))
				 .add(PropertyFactoryUtil.forName("parentFolderId").eq(idea.getIdFolder()))
				 .add(PropertyFactoryUtil.forName("name").eq(PortletProps.get("screenshot.foldername")));
		 try {
			 dLFolderSS = DLFolderLocalServiceUtil.dynamicQuery(queryUtente);
		} catch (SystemException e1) {
			e1.printStackTrace();
			return screenFolderId;
		}
		
		try{
			
			if (dLFolderSS.size() > 0)
				screenFolderId = DLFolderLocalServiceUtil.getFolder(idea.getGroupId(), idea.getIdFolder(), PortletProps.get("screenshot.foldername")).getFolderId();
			
		}
		catch(Exception e){ 
			screenFolderId = 0; 
			System.out.println(" eccezione screenshot.jsp - screenFolderId ");

		}
		
		return screenFolderId;
		
	}
	
	
	/**
	 * @param idea
	 * @return
	 */
	public static boolean isIdeaWithPoi (CLSIdea idea){
		
		if (idea.getChallengeId() > -1)// solo le idee senza gara hanno POI
			return false;
		
		
		List<CLSIdeaPoi> poisList = new ArrayList<CLSIdeaPoi>();
		
		try {
			poisList = CLSIdeaPoiLocalServiceUtil.getIdeaPOIByIdeaId(idea.getIdeaID());
		} catch (SystemException e) {
			
			e.printStackTrace();
			return false;
			
		}
		
		
		if (poisList.size() > 0)
			return true;
		
		
		return false;
		
	}
	
	
	/**
	 * @param userId
	 * @return
	 */
	private static List<CLSIdea> getListaIdeeACuiCollaboroByUserId (long userId){

		List<CLSIdea> ideas=new ArrayList<CLSIdea>();
		List<CLSCoworker> coworkersList =new ArrayList<CLSCoworker>();
		
		try {
			coworkersList = CLSCoworkerLocalServiceUtil.getCoworkersByUserId(userId);
		} catch (SystemException e) {
			
			e.printStackTrace();
			return ideas;
			
		}
		
		
		for (CLSCoworker cw : coworkersList){
			
			CLSIdea idea;
			try {
				idea = CLSIdeaLocalServiceUtil.getIdeasByIdeaId(cw.getIdeaID());
			} catch (SystemException e) {
				
				e.printStackTrace();
				return ideas;
			}
			
			ideas.add(idea);
			
		}
		
		return ideas;
	}
	
	
	/**
	 * @param userId
	 * @return
	 */
	private static List<CLSIdea> getListaMieIdeePlusCuiCollaboroByUserId (long userId){
		
		List<CLSIdea> myIdeas = new ArrayList<CLSIdea>();

		try {
			myIdeas=CLSIdeaLocalServiceUtil.getIdeasByUserId(userId);
		} catch (SystemException e) {
			
			e.printStackTrace();
			return myIdeas;
		}
		
		List<CLSIdea> ideasCuiCollaboro = getListaIdeeACuiCollaboroByUserId(userId);
		
		Set<CLSIdea> set = new HashSet<>(myIdeas);//uso per rimuovere i duplicati
		set.addAll(ideasCuiCollaboro);
		List<CLSIdea> mergedList = new ArrayList<>(set);
		
		return mergedList;
		
		
	}
	
	/**
	 * @param userId
	 * @return
	 */
	public static List<CLSIdea> getOnlyMyIdeasPlusCollaborationByUserId (long userId){
		
		List<CLSIdea> myIdeas = new ArrayList<CLSIdea>();

		try {
			myIdeas=CLSIdeaLocalServiceUtil.getOnlyIdeasByUserId(userId);
		} catch (SystemException e) {
			
			e.printStackTrace();
			return myIdeas;
		}
		
		List<CLSIdea> ideasCuiCollaboro = getListaIdeeACuiCollaboroByUserId(userId);
		
		Set<CLSIdea> set = new HashSet<>(myIdeas);//uso per rimuovere i duplicati
		set.addAll(ideasCuiCollaboro);
		List<CLSIdea> mergedList = new ArrayList<>(set);
		
		return mergedList;
		
		
	}
	
	
	/**
	 *	To be associated there must be a challenge with a common vocabulary to the idea
	 * 
	 * @Deprecated because with removed idea without challenge/need
	 * Now a need always has a referred challenge or need
	 * 
	 * @param ideaId
	 * @return
	 */
	@Deprecated
	public static boolean isIdeaAssociabileGareEnte (CLSIdea idea, User currentUser){
		
		//se gia abbiamo la gara
		if (idea.getChallengeId() >-1)
			return false;
		
		
		String authorPilot = MyUtils.getPilotByUserId(idea.getUserId());
		long vocabolarioId = getVocabularyIdByIdeaId(idea.getIdeaID());
		
		
		if (idea.getMunicipalityId() > 0){
		
			
				String municipalitypilot = MyUtils.getPilotByUserId(idea.getMunicipalityId());
				
				//se author ed ente sono pilot diversi (non dovrebbe capitare piu' lascio il controllo per lo storico) //TODO eliminare appena si fa pulizia
				if (!authorPilot.equalsIgnoreCase(municipalitypilot))
					return false;
				
				List<CLSChallenge> gareAttiveAuthority = ChallengesUtils.getActiveChallengesByAuthorityId(idea.getMunicipalityId());
				
				if (gareAttiveAuthority.size() == 0)
					return false;
				
				
				for (CLSChallenge challenge : gareAttiveAuthority) {
					
					long challVocId = ChallengesUtils.getVocabularyIdByChallengeId(challenge.getChallengeId());
					if ((challVocId == vocabolarioId) && (idea.getLanguage().equalsIgnoreCase(challenge.getLanguage()))   )
						return true;
				}
		
		
		}else{//idea non ancora assegnata
			
			//tiro e ciclo tutti gli utenti dell'organization
			List<User> users = MyUtils.getUsersByOrganizationId(idea.getMunicipalityOrganizationId());
			
			for(User user:users){
				
				List<CLSChallenge> gareAttiveAuthority = ChallengesUtils.getActiveChallengesByAuthorityId(user.getUserId());
				
				//prendo le gare attive di tutti gli utenti dell'organzation
				for (CLSChallenge gara: gareAttiveAuthority){
					long challVocId = ChallengesUtils.getVocabularyIdByChallengeId(gara.getChallengeId());
					if ((challVocId == vocabolarioId) && (idea.getLanguage().equalsIgnoreCase(gara.getLanguage())))
							return true;
				}
			}
		}
		
		return false;
		
	}
	
	
	
	
	
	/**
	 * It returns all challenges created by the referred need of this idea
	 * @param idea
	 * @return
	 */
	public static List<CLSChallenge> getChallengesStartedByReferredNeedConnectableWithIdea (CLSIdea idea){
		
		List<CLSChallenge> connectableChallenges = new ArrayList<CLSChallenge>();//return list
		long vocIdIdea = IdeasListUtils.getVocabularyIdByIdeaId(idea.getIdeaID());
		
		List<CLSChallenge> linkedchallenge = new ArrayList<CLSChallenge>();//challenges started from the Need of this idea
		
		try {
			
			linkedchallenge = NeedLinkedChallengeLocalServiceUtil.getChallengeByNeedId(idea.getNeedId());
			
		} catch (SystemException e) {
			e.printStackTrace();
		}
		
			
		for (CLSChallenge ch: linkedchallenge){
			
			
			long vocIdChallenge = ChallengesUtils.getVocabularyIdByChallengeId(ch.getChallengeId());
			
				
			if (ChallengesUtils.isActiveChallenge(ch) &&  vocIdIdea == vocIdChallenge )
				connectableChallenges.add(ch);
		}
		
		
		return connectableChallenges;
	}
	
	
	
	
	
	/**
	 * To be associated there must be a challenge created by the referred idea's need
	 * @param ideaId
	 * @return
	 */
	public static boolean isIdeaConnectableWithChallengeStartedByNeed(CLSIdea idea){
	
		//if it already has the challenge or it isn't referred to a need
		if ( (idea.getChallengeId() >-1) || (idea.getNeedId() <= 0) )
			return false;
		
		List<CLSChallenge> connectableChallenges = getChallengesStartedByReferredNeedConnectableWithIdea(idea);

		if (connectableChallenges.size() > 0 )
			return true;
		
		return false;
		
	}
	
	
	
	
	/**
	 * @param renderRequest
	 * @return
	 */
	public static String getPilot (RenderRequest renderRequest){
		
		boolean pilotingEnabled = IdeaManagementSystemProperties.getEnabledProperty("pilotingEnabled");
		
		if (!pilotingEnabled)
			return "";
		
		
		
		String pilot = "";
		
		User currentUser = null;
		
		try {
			 currentUser = PortalUtil.getUser(renderRequest);
		} catch (PortalException | SystemException e) {
			
			e.printStackTrace();
			
		}
		
		try {
		
		
			if (Validator.isNull(currentUser)){
			
				PortletSession sessionUser = renderRequest.getPortletSession();
				pilot = (String) sessionUser.getAttribute(MyConstants.KEY_PILOT, PortletSession.APPLICATION_SCOPE); //questo valore viene settato da un'altro tool: il Controller
			}else{
				
				
				String[] pilotUtente = (String[]) currentUser.getExpandoBridge().getAttribute(MyConstants.CUSTOMFIELD_PILOT);
				pilot = pilotUtente[0];
				
			}
		
		
		}	 catch (Exception e) {
			
			e.printStackTrace();
			return pilot;
		}
		
		if (pilot == null)
			return "";
		
		
		return pilot;
		
	}
	
	
	
	/**
	 * @param ideaId
	 * @return
	 */
	public static boolean isNeedByIdeaId (long ideaId){
		
		boolean isNeed = false;
		
		
		CLSIdea idea = null;
		try {
			idea = CLSIdeaLocalServiceUtil.getIdeasByIdeaId(ideaId);
		} catch (SystemException e1) {
			
			e1.printStackTrace();
			return isNeed;
		}
		
		
		isNeed = idea.getIsNeed();
		
		return isNeed;
	}
	
	
	/**
	 * @param idea
	 * @param pilot
	 * @return
	 */
	private static boolean isIdeaOfCurrentPilot (CLSIdea idea, String pilot){
		
		String pilotIdea = MyUtils.getPilotByIdea(idea);
		
		if (pilotIdea.equalsIgnoreCase(pilot))
			return true;
		
		return false;
		
		
	}
	
	
	
/**
 * 
 * @Deprecated because now we can associate only with challenge started by idea's referred need
 * 
 * @param idea
 * @return
 */
	@Deprecated
public static List<CLSChallenge> getChallengesAssociableWithIdea (CLSIdea idea){
		
	List<CLSChallenge> gareAssociabili = new ArrayList<CLSChallenge>();	
	
	long vocabolarioId = getVocabularyIdByIdeaId(idea.getIdeaID());
	
		
		if (idea.getMunicipalityId() > 0){//Idea assigned to a Municipality user
		
				List<CLSChallenge> gareAttiveAuthority = ChallengesUtils.getActiveChallengesByAuthorityId(idea.getMunicipalityId());
								
				
				for (CLSChallenge challenge : gareAttiveAuthority) {
					
					long challVocId = ChallengesUtils.getVocabularyIdByChallengeId(challenge.getChallengeId());
					if ((challVocId == vocabolarioId) && (idea.getLanguage().equalsIgnoreCase(challenge.getLanguage())) && !gareAssociabili.contains(challenge)  )
						gareAssociabili.add(challenge);
						
				}
		
		
		}else{// Idea not assigned yet
			
			
			//tiro e ciclo tutti gli utenti dell'organization
			List<User> users = MyUtils.getUsersByOrganizationId(idea.getMunicipalityOrganizationId());
			
			for(User user:users){
				
				List<CLSChallenge> gareAttiveAuthority = ChallengesUtils.getActiveChallengesByAuthorityId(user.getUserId());
				
				//prendo le gare attive di tutti gli utenti dell'organzation
				for (CLSChallenge gara: gareAttiveAuthority){
					
					long challVocId = ChallengesUtils.getVocabularyIdByChallengeId(gara.getChallengeId());
					if (challVocId == vocabolarioId && (idea.getLanguage().equalsIgnoreCase(gara.getLanguage())) && !gareAssociabili.contains(gara))
						gareAssociabili.add(gara);
					
				}
			}
				 
				
			
		}
		
		
		return gareAssociabili;
		
	}
	
	
/**
 * @param idea
 * @return
 */
public static String getAuthorProfileUrlByIdea (CLSIdea idea){
	
	String url = "";
	
	User user;
	
	try {
		user = UserLocalServiceUtil.getUser(idea.getUserId());
				
	} catch (PortalException | SystemException e) {
		
		e.printStackTrace();
		return url;
	}
	
	url = MyUtils.getUserProfileUrlByUser(user);
	
	return url;
	
}
	


/**
 * @param listaI
 * @param pilot
 * @return
 */
public static List<CLSIdea> getIdeasListFilterByPilot (List<CLSIdea> listaI, String pilot){
	
	List<CLSIdea> listaF = new ArrayList<CLSIdea>();
	
	
	for (int i = 0; i<listaI.size(); i++){//slide beginnig list
		
		CLSIdea idea = listaI.get(i); //the current idea
		
		String pilotEnte = MyUtils.getPilotByIdea(idea);
		
		if (pilotEnte.equalsIgnoreCase(pilot))
			listaF.add(idea);

	}
	
	//I show before the newest
	IdeaComparator ic = new IdeaComparator();
	listaF=ListUtil.sort(listaF, ic);
	
	
	return listaF;
}//metodo


/**
 * @param listaI
 * @param language
 * @return
 */
public static List<CLSIdea> getIdeasListFilterByLanguage (List<CLSIdea> listaI, String language){
	
	List<CLSIdea> listaF = new ArrayList<CLSIdea>();
	
	
	for (int i = 0; i<listaI.size(); i++){//slide beginnig list
		
		CLSIdea idea = listaI.get(i); //the current idea
		
		if (idea.getLanguage().equalsIgnoreCase(language))
			listaF.add(idea);

	}
	
	//I show before the newest
	IdeaComparator ic = new IdeaComparator();
	listaF=ListUtil.sort(listaF, ic);
	
	
	return listaF;
}//metodo



/**
 * @param idea
 * @param userId
 */
public static void subscribeUserToIdeaComment (CLSIdea idea, long userId){
	
	
	try {
		SubscriptionLocalServiceUtil.addSubscription(userId, idea.getGroupId(), CLSIdea.class.getName(), idea.getIdeaID(), "instant");
	} catch (PortalException | SystemException e) {
		
		e.printStackTrace();
	}
	
}


/**
 * @param listI
 * @param pilot
 * @return
 */
public static List<CLSIdea> getIdeasFilteredByPilot (List<CLSIdea> listI,  String pilot){
	
	List<CLSIdea> listaF = new ArrayList<CLSIdea>();
	
	
	for (int i = 0; i<listI.size(); i++){
		
		CLSIdea idea = listI.get(i); 
	
		String pilotAuthority = MyUtils.getPilotByIdea(idea);
		
		
		if (pilotAuthority.equalsIgnoreCase(pilot)){
			listaF.add(idea);
		}
	}
	
	return listaF;
}//method	



/**
 * redirect to the rigth page
 * @param actionRequest
 * @param actionResponse
 * @param ideaId
 */
public static void redirectToIdea(ActionRequest actionRequest,	ActionResponse actionResponse, long ideaId){
	 
	 String totalPath = IdeaManagementSystemProperties.getFriendlyUrlIdeas(ideaId);
	 
	try {
		actionResponse.sendRedirect(totalPath);
	} catch (IOException e) {
		e.printStackTrace();
	} //go to the page that you put as second parameter
}



/**
 * @param ideaId
 * @return
 */
public static boolean isIdeaTakeable (long ideaId){
	
	CLSIdea idea = null;	
	try {
		idea = CLSIdeaLocalServiceUtil.getIdeasByIdeaId(ideaId);
	} catch (SystemException e) {
		
		e.printStackTrace();
	}	
	
	if (idea.isTakenUp())
		return false;
		
		
	
	if (idea.getIdeaStatus().equals(MyConstants.IDEA_STATE_REJECT) || idea.getChallengeId() ==-1)
		return true;
	
	return false;
}



/**
 * @param ideaId
 * @return
 */
public static boolean isIdeaToEvaluate (long ideaId){
	
	CLSIdea idea = null;	
	try {
		idea = CLSIdeaLocalServiceUtil.getIdeasByIdeaId(ideaId);
	} catch (SystemException e) {
		
		e.printStackTrace();
	}	
	
	if (idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_EVALUATION))
		return true;
	
	return false;
}

/**
 * @param ideaId
 * @return
 */
public static boolean isIdeaWithCoworkerByIdeadId (long ideaId){
	

	List<CLSCoworker> cowks = new ArrayList<CLSCoworker>();
	
	 try {
		cowks = CLSCoworkerLocalServiceUtil.getCoworkersByIdeaId(ideaId);
	} catch (SystemException e) {
		
		e.printStackTrace();
	}
	 
	if (cowks.size() > 0)
		return true;
	 
	 
	
	return false;
}

/**
 * @param ideaId
 * @return
 */
public static boolean isIdeaOnRefinement (long ideaId){
	
	CLSIdea idea = null;	
	try {
		idea = CLSIdeaLocalServiceUtil.getIdeasByIdeaId(ideaId);
	} catch (SystemException e) {
		
		e.printStackTrace();
	}	
	
	if (idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_REFINEMENT))
		return true;
	
	return false;
}

/**
 * @param ideaId
 * @return
 */
public static boolean isIdeaOnRefinementOrMonitoring (long ideaId){
	
	CLSIdea idea = null;	
	try {
		idea = CLSIdeaLocalServiceUtil.getIdeasByIdeaId(ideaId);
	} catch (SystemException e) {
		
		e.printStackTrace();
	}	
	
	if (idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_REFINEMENT) || 
			idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_MONITORING)
		)
		return true;
	
	return false;
}

/**
 * @param ideaId
 * @return
 */
public static boolean isIdeaOnRefinementOrImplementationOrMonitoring (long ideaId){
	
	CLSIdea idea = null;	
	try {
		idea = CLSIdeaLocalServiceUtil.getIdeasByIdeaId(ideaId);
	} catch (SystemException e) {
		
		e.printStackTrace();
	}	
	
	if (idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_REFINEMENT) || 
			idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_IMPLEMENTATION)||
			idea.getIdeaStatus().equalsIgnoreCase(MyConstants.IDEA_STATE_MONITORING)
		)
		return true;
	
	return false;
}

	
/**
 * @param ideaId
 * @return
 */
public static List<EvaluationCriteria> getBarrierEvaluationCriteriaByIdeaId(long ideaId){
	
	List<EvaluationCriteria> barrierCriteria = new ArrayList<EvaluationCriteria>();
	
	CLSIdea idea = null;
	
	try {
		 idea = CLSIdeaLocalServiceUtil.getCLSIdea(ideaId);
	} catch (PortalException | SystemException e) {
		
		e.printStackTrace();
		return barrierCriteria;
	}
	
	barrierCriteria = EvaluationCriteriaLocalServiceUtil.getEnabledCriteriaByChallengeIdAndIsBarriera(idea.getChallengeId(), true);
	
	return barrierCriteria;
	
}



/**
 * @param ideaId
 * @return
 */
public static List<EvaluationCriteria> getQuantitativeEvaluationCriteriaByIdeaId(long ideaId){
	
	List<EvaluationCriteria> qCriteria = new ArrayList<EvaluationCriteria>();
	
	CLSIdea idea = null;
	
	try {
		 idea = CLSIdeaLocalServiceUtil.getCLSIdea(ideaId);
	} catch (PortalException | SystemException e) {
		
		e.printStackTrace();
		return qCriteria;
	}
	
	qCriteria = EvaluationCriteriaLocalServiceUtil.getEnabledCriteriaByChallengeIdAndIsBarriera(idea.getChallengeId(), false);
	
	return qCriteria;
	
}


/** Method of JOIN between IdeaEvaluation and EvaluationCriteria, filter by isBarriera=false 
 * @param ideaId
 * @return
 */
public static List<IdeaEvaluation> getQuantitativeIdeaEvaluationByIdeaId(long ideaId){
	
	List<IdeaEvaluation> qEval = new ArrayList<IdeaEvaluation>();
	
	List<IdeaEvaluation> allEvals = IdeaEvaluationLocalServiceUtil.getIdeaEvaluationsByIdeaId(ideaId);
	List<EvaluationCriteria> qCriterias = getQuantitativeEvaluationCriteriaByIdeaId( ideaId);
	
	for (IdeaEvaluation eval: allEvals){
		
		for (EvaluationCriteria crit: qCriterias){
			
			if (eval.getCriteriaId() == crit.getCriteriaId()){
				qEval.add(eval);
				break;
			}
		}
	}
	
	return qEval;
}


/** Method of JOIN between IdeaEvaluation and EvaluationCriteria, filter by isBarriera=true 
 * @param ideaId
 * @return
 */
public static List<IdeaEvaluation> getBarrierIdeaEvaluationByIdeaId(long ideaId){
	
	List<IdeaEvaluation> qEval = new ArrayList<IdeaEvaluation>();
	
	List<IdeaEvaluation> allEvals = IdeaEvaluationLocalServiceUtil.getIdeaEvaluationsByIdeaId(ideaId);
	List<EvaluationCriteria> bCriterias = getBarrierEvaluationCriteriaByIdeaId(ideaId);
	
	for (IdeaEvaluation eval: allEvals){
		
		for (EvaluationCriteria crit: bCriterias){
			
			if (eval.getCriteriaId() == crit.getCriteriaId()){
				qEval.add(eval);
				break;
			}
		}
	}
	
	return qEval;
}

/**
 * @param criteriaId
 * @return
 */
public static double getWeightByCriteriaId (long criteriaId){

	EvaluationCriteria ec = EvaluationCriteriaLocalServiceUtil.getCriteriaByCriteriaId(criteriaId);
	
	return ec.getWeight();
	
}


/** Get Evaluation score
 * @param ideaId
 * @return
 */
public static double getTotalScoreIdeaByIdeaId(long ideaId){
	
	List<IdeaEvaluation> iEvals = getQuantitativeIdeaEvaluationByIdeaId(ideaId);
	
	double totalScore = 0;
	
	for(IdeaEvaluation iEval:iEvals){
		
		long criteriaId = iEval.getCriteriaId();
		double weight = getWeightByCriteriaId(criteriaId);
		
		totalScore =totalScore+ iEval.getScore() * weight;
	}
	

	totalScore =Math.floor(totalScore * 100.0) / 100.0; //rounding to 2 decimal
	
	return totalScore;
}


/**
 * @param challengeId
 * @return
 */
public static List<CLSIdea> getIdeasByChallengeIdOrderedByEvaluationScore (long challengeId){
	
	List<CLSIdea> ideas = new ArrayList<CLSIdea>();
	
	try {
		 ideas = CLSIdeaLocalServiceUtil.getIdeasByChallengeId(challengeId);
	} catch (SystemException e) {
		
		e.printStackTrace();
		return ideas;
	}

	IdeaComparatorByEvaluationScore ideaComparatorBES = new IdeaComparatorByEvaluationScore();

	ideas=ListUtil.sort(ideas, ideaComparatorBES);
	
	
	return ideas;

}


/**
 * @param idea
 * @return
 */
public static int getEvaluationRankingPositionByIdea (CLSIdea idea){
	
	List<CLSIdea> ideas = getIdeasByChallengeIdOrderedByEvaluationScore(idea.getChallengeId());
	
	if (ideas.size() == 0)
		return 0;
	
	for (int i=0; i<ideas.size(); i++){
		
		if (ideas.get(i).getIdeaID() == idea.getIdeaID())
			return i+1;
		
	}
	
	return 0;
	
}

/**
 * @param userid
 * @return
 */
public static List<CLSIdea> getIdeasOfAuthorityIdAndNotAssignedByUserId(long userid) {
	
	long orgId = MyUtils.getMunicipalityOrganizationIdByUserId(userid);
	
	List<CLSIdea> ideas = new ArrayList<CLSIdea>();
	List<CLSIdea> ideasF = new ArrayList<CLSIdea>();
	
	try {
		ideas = CLSIdeaLocalServiceUtil.getOnlyIdeasByMunicipalityOrganizationId(orgId);
		
		for (CLSIdea idea: ideas){
			
			if ( 
				(idea.getMunicipalityId() == userid) ||
					(idea.getMunicipalityId() == 0)
			   )
				ideasF.add(idea);
		}
		
		
	} catch (SystemException e) {
		
		e.printStackTrace();
		return ideas;
	}
	return ideas;
}

public static JSONArray getCoworkewsByIdea (CLSIdea idea){
	
	JSONArray jA = JSONFactoryUtil.createJSONArray();
	
	List<CLSCoworker> coworkersList = new ArrayList<CLSCoworker>();
		try {
			coworkersList = CLSCoworkerLocalServiceUtil.getCoworkersByIdeaId(idea.getIdeaID());
		} catch (SystemException e1) {
			e1.printStackTrace();
			return jA;
		}
		for(CLSCoworker coworker: coworkersList){
			try{ 
				User u = UserLocalServiceUtil.getUser(coworker.getUserId());
				JSONObject jObj = JSONFactoryUtil.createJSONObject();
				jObj.put("city_name", idea.getCityName());
				jObj.put("member_name", u.getFullName());
				jObj.put("member_email", u.getEmailAddress());
				jA.put(jObj);
			}
			catch(Exception e){ 
				e.printStackTrace(); 
			}
		}
	return jA;
	
		
	
}



}