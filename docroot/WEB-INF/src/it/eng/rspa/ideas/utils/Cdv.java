package it.eng.rspa.ideas.utils;


import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;

public class Cdv {

	private static final String CDV_ADDRESS = IdeaManagementSystemProperties.getProperty("cdvAddress");
	
	
	//inviato alla creazione/modifica idea
	public static void cdvEventNotification(CLSIdea idea, long autoreUserID, ArrayList<Long> coworkersUserIdList, String eventoSuIdea , List<CLSCoworker> coworkersListOld ){
		
	
//		[
//		{
//        "ccUserID": 12345 ,       
//        "eventName": IdeaCreated ,       
//		}
//		{
//      "ccUserID": 12346 ,       
//      "eventName": NewCollaboration ,       
//		}
//		{
//      "ccUserID": 12347 ,       
//      "eventName": CollaborationRemoved ,       
//		}
//		....
//		]
		
		JSONArray jsonInvio = JSONFactoryUtil.createJSONArray();
		
		
		JSONArray jsonArrayDatiIdea =  JSONFactoryUtil.createJSONArray();
		
		JSONObject jsonIdeaId =  JSONFactoryUtil.createJSONObject();
		jsonIdeaId.put("key", "ideaid");
		jsonIdeaId.put("value", idea.getIdeaID());
		jsonArrayDatiIdea.put(jsonIdeaId);
		
		JSONObject jsonIdeaTitle =  JSONFactoryUtil.createJSONObject();
		jsonIdeaTitle.put("key", "ideatitle");
		jsonIdeaTitle.put("value", idea.getIdeaTitle());
		jsonArrayDatiIdea.put(jsonIdeaTitle);
		
		JSONObject jsonIdeaIsNeed =  JSONFactoryUtil.createJSONObject();
		jsonIdeaIsNeed.put("key", "isneed");
		jsonIdeaIsNeed.put("value", idea.getIsNeed());
		jsonArrayDatiIdea.put(jsonIdeaIsNeed);
		
		
		
		if (eventoSuIdea.equalsIgnoreCase("nuova")){
			
			long CCUserID = MyUtils.getCCUserIdbyUserId(autoreUserID);
			
			
			
			JSONObject jsonAutore =  JSONFactoryUtil.createJSONObject();
			jsonAutore.put("ccUserID", CCUserID);
			jsonAutore.put("eventName", "IdeaCreated"); 
			jsonAutore.put("entries", jsonArrayDatiIdea);		
			
			
			
			jsonInvio.put(jsonAutore);
			
			for (Long cw : coworkersUserIdList){
				
				long CCUserID_CW = MyUtils.getCCUserIdbyUserId(cw);
				
				JSONObject jsonCollaboratore =  JSONFactoryUtil.createJSONObject();
				jsonCollaboratore.put("ccUserID", CCUserID_CW);
				jsonCollaboratore.put("eventName", "NewCollaboration");
				jsonCollaboratore.put("entries", jsonArrayDatiIdea);	
				jsonInvio.put(jsonCollaboratore);
			}
			
			
			
		}else if (eventoSuIdea.equalsIgnoreCase("modifica")) {//siamo in modifica e invio solo i nuovi collaboratori
			
			//vedo se ci sono nuovi collaboratori
			for (Long coworkerUserId : coworkersUserIdList){//collaboratori attuali
				boolean coworkerOld = false;
				for (int y=0; y<coworkersListOld.size(); y++){//collaboratori di prima
					if (coworkersListOld.get(y).getUserId() ==  coworkerUserId ){
						coworkerOld = true;
						break;
					}
				}
				
				
				if (!coworkerOld){
					
					long CCUserID_CW = MyUtils.getCCUserIdbyUserId(coworkerUserId);
					
					JSONObject jsonCollaboratore =  JSONFactoryUtil.createJSONObject();
					jsonCollaboratore.put("ccUserID", CCUserID_CW);
					jsonCollaboratore.put("eventName", "NewCollaboration");
					jsonCollaboratore.put("entries", jsonArrayDatiIdea);
					jsonInvio.put(jsonCollaboratore);
					
				}//coworkerOld
			}//for collaboratori attuali
			
			
			//vedo se  sono stati rimossi vecchi collaboratori
			for (int y=0; y<coworkersListOld.size(); y++){//collaboratori di prima
				boolean coworkerConfermato = false;
				for (Long coworkerUserId : coworkersUserIdList){//collaboratori attuali
					if (coworkersListOld.get(y).getUserId() ==  coworkerUserId ){
						coworkerConfermato = true;
						break;
					}
				}
				
				
				if (!coworkerConfermato){
					
					long CCUserID_CW = MyUtils.getCCUserIdbyUserId(coworkersListOld.get(y).getUserId());
					
					JSONObject jsonCollaboratore =  JSONFactoryUtil.createJSONObject();
					jsonCollaboratore.put("ccUserID", CCUserID_CW);
					jsonCollaboratore.put("eventName", "CollaborationRemoved");
					jsonCollaboratore.put("entries", jsonArrayDatiIdea);
					jsonInvio.put(jsonCollaboratore);
					
				}//coworkerOld
			}//for collaboratori attuali
			
			
			
			
		}else{//cancellazione
			
			//autore
			long CCUserID = MyUtils.getCCUserIdbyUserId(idea.getUserId());
			JSONObject jsonAutore =  JSONFactoryUtil.createJSONObject();
			jsonAutore.put("ccUserID", CCUserID);
			jsonAutore.put("eventName", "IdeaDeleted");
			jsonAutore.put("entries", jsonArrayDatiIdea);
			
			jsonInvio.put(jsonAutore);
		
			
			//collaboratori
			for (int y=0; y<coworkersListOld.size(); y++){//collaboratori di prima
			
				long CCUserID_CW = MyUtils.getCCUserIdbyUserId(coworkersListOld.get(y).getUserId());
				
				JSONObject jsonCollaboratore =  JSONFactoryUtil.createJSONObject();
				jsonCollaboratore.put("ccUserID", CCUserID_CW);
				jsonCollaboratore.put("eventName", "CollaborationRemoved");
				jsonCollaboratore.put("entries", jsonArrayDatiIdea);
				jsonInvio.put(jsonCollaboratore);
			
			}
			
			
			
			
			
		}//if eventoSuIdea
		
		
		
		if (jsonInvio.length() >0){//potrebbe essere vuoto solo in caso di update
		
			if (IdeaManagementSystemProperties.getEnabledProperty("verboseEnabled")  ){
				System.out.println("CDV jsonSent "+jsonInvio);
			}
			
			try {
			//	RestUtils.consumePostWs(jsonInvio, CDV_ADDRESS+"/cdv/api/datasource/operation/push");
				
				// SITE/dev/api/cdv/push
				RestUtils.consumePostWs(jsonInvio, CDV_ADDRESS+"/push");
				
				
				
			} catch (Exception e) {
				
				e.printStackTrace();
			} 
		}	

	}
	
	

	
	

	
}

