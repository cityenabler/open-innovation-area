package it.eng.rspa.ideas.utils.listener;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge;

public interface iChallengeListener {

	/** Invocato alla pubblicazione di una gara**/
	public void onChallengePublish(CLSChallenge c);
	
	/** Invocato alla cancellazione di una gara **/
	public void onChallengeDelete(CLSChallenge c);
	
	/** Invocato alla modifica di una gara **/
	public void onChallengeUpdate(CLSChallenge c);
		
}
