package it.eng.rspa.ideas.utils.model;


public class Requirement {

	private String description;
	private Boolean multiTask;
	private String category;
	private String help;
	private boolean outcomeFile;
	private boolean taskDoubleField;
	
	

	public boolean isTaskDoubleField() {
		return taskDoubleField;
	}
	public void setTaskDoubleField(boolean taskDoubleField) {
		this.taskDoubleField = taskDoubleField;
	}
	public boolean isOutcomeFile() {
		return outcomeFile;
	}
	public void setOutcomeFile(boolean outcomeFile) {
		this.outcomeFile = outcomeFile;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Boolean getMultiTask() {
		return multiTask;
	}
	public void setMultiTask(Boolean multiTask) {
		this.multiTask = multiTask;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getHelp() {
		return help;
	}
	public void setHelp(String help) {
		this.help = help;
	}
	
	
	
	
	
	
}
