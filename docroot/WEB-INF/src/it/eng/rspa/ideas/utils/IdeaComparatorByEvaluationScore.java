package it.eng.rspa.ideas.utils;


import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;

import java.util.Comparator;

public class IdeaComparatorByEvaluationScore implements Comparator<CLSIdea>{

	

	@Override
	public int compare(CLSIdea idea1, CLSIdea idea2) {
		
		double score1 = 0;
		double score2 = 0;

			
		score1 = IdeasListUtils.getTotalScoreIdeaByIdeaId(idea1.getIdeaID());
		score2 = IdeasListUtils.getTotalScoreIdeaByIdeaId(idea2.getIdeaID());
			
		
		if (score2 > score1)
			return 1;
		
		if (score1 > score2)
			return -1;
		
		return 0;
		
	}

}
