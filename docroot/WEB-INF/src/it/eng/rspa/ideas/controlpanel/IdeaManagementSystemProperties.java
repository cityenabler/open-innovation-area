package it.eng.rspa.ideas.controlpanel;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSet;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengesCalendar;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSFriendlyUrlSuffix;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCategoriesSetImpl;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengesCalendarImpl;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFriendlyUrlSuffixImpl;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSMapPropertiesImpl;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSCategoriesSetLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeLocalService;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengesCalendarLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSFriendlyUrlSuffixLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSMapPropertiesLocalServiceUtil;
import it.eng.rspa.ideas.utils.ChallengesUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.model.Company;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class IdeaManagementSystemProperties
 */
public class IdeaManagementSystemProperties extends MVCPortlet {
	
	public void savePreferencies(ActionRequest actionRequest,
			ActionResponse actionResponse)
					throws IOException, PortletException, PortalException, SystemException {
		
		
		//elimino tutte i dati salvati
		List<CLSCategoriesSet> allcategoriesSets = CLSCategoriesSetLocalServiceUtil.getCLSCategoriesSets(com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS, com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS);
		if(allcategoriesSets!=null){

			Iterator<CLSCategoriesSet> allcategoriesSetsIt = allcategoriesSets.iterator();
			while (allcategoriesSetsIt.hasNext()) {
				CLSCategoriesSet clsCategoriesSet = (CLSCategoriesSet) allcategoriesSetsIt.next();
				CLSCategoriesSetLocalServiceUtil.deleteCLSCategoriesSet(clsCategoriesSet);
			}
		}
		
		
		//Recupero e salvo le nuove impostazioni
		Map<String, String[]> paramMap = actionRequest.getParameterMap();
		Set<String> set = paramMap.keySet();
		

		
		Iterator setIt = set.iterator();

		//cerco i parameti passati e li salvo
		while(setIt.hasNext()){
			String key = (String) setIt.next();
			

			
			if (key.startsWith("checkbox_vocabulary_#") && key.endsWith("#Checkbox")) {
				
				String vocabularyParameterValue = actionRequest.getParameter(key);
				
				if(Boolean.parseBoolean(vocabularyParameterValue)){
					
					//creo un nuovo oggetto per le categorie
					CLSCategoriesSet categoriesSet = new CLSCategoriesSetImpl();

				
					String vocabularyId = key.substring("checkbox_vocabulary_#".length());
					vocabularyId = vocabularyId.substring(0,vocabularyId.lastIndexOf("#Checkbox"));
					categoriesSet.setCategoriesSetID(Long.parseLong(vocabularyId));
					
					String inputColorForVocabulary = "colourName_"+ vocabularyId;
					String inputColorValue = actionRequest.getParameter(inputColorForVocabulary);
					categoriesSet.setColourName(inputColorValue);
					
					
					String inputIconForVocabulary = "iconName_"+ vocabularyId;
					String inputIconValue = actionRequest.getParameter(inputIconForVocabulary);
					categoriesSet.setIconName(inputIconValue);
					
					CLSCategoriesSetLocalServiceUtil.addCLSCategoriesSet(categoriesSet);
				}
			}
			
		
			// recupero i suffissi
			String suffixImsHome = ParamUtil.getString(actionRequest,"sfxHomeIms");	
			String sfxIdeas = ParamUtil.getString(actionRequest,"sfxIdeas");
			String sfxNeeds = ParamUtil.getString(actionRequest,"sfxNeeds");
			String sfxChallenges = ParamUtil.getString(actionRequest,"sfxChallenges");
			
			//email
			String senderNotificheMailIdeario = ParamUtil.getString(actionRequest,"senderNotificheMailIdeario");	
			String oggettoNotificheMailIdeario = ParamUtil.getString(actionRequest,"oggettoNotificheMailIdeario");	
			String firmaNotificheMailIdeario = ParamUtil.getString(actionRequest,"firmaNotificheMailIdeario");	
			String utenzaMail = ParamUtil.getString(actionRequest,"utenzaMail");	
			
			//service
			boolean mktEnabled =  ParamUtil.getBoolean(actionRequest,"mktEnabled");	
			boolean cdvEnabled =  ParamUtil.getBoolean(actionRequest,"cdvEnabled");	
			String cdvAddress = ParamUtil.getString(actionRequest,"cdvAddress");
			boolean vcEnabled =  ParamUtil.getBoolean(actionRequest,"vcEnabled");	
			String vcAddress = ParamUtil.getString(actionRequest,"vcAddress");
			String vcWSAddress = ParamUtil.getString(actionRequest,"vcWSAddress");
			boolean deEnabled =  ParamUtil.getBoolean(actionRequest,"deEnabled");	
			String deAddress = ParamUtil.getString(actionRequest,"deAddress");
			boolean lbbEnabled =  ParamUtil.getBoolean(actionRequest,"lbbEnabled");	
			String lbbAddress = ParamUtil.getString(actionRequest,"lbbAddress");
			String oiaAppId4lbb = ParamUtil.getString(actionRequest,"oiaAppId4lbb");
			boolean tweetingEnabled =  ParamUtil.getBoolean(actionRequest,"tweetingEnabled");
			boolean mockEnabled =  ParamUtil.getBoolean(actionRequest,"mockEnabled");	
			
			boolean fiwareEnabled =  ParamUtil.getBoolean(actionRequest,"fiwareEnabled");
			boolean fiwareRemoteCatalogueEnabled =  ParamUtil.getBoolean(actionRequest,"fiwareRemoteCatalogueEnabled");
			String fiwareCatalogueAddress = ParamUtil.getString(actionRequest,"fiwareCatalogueAddress");
			
			String basicAuthUser = ParamUtil.getString(actionRequest,"basicAuthUser");
			String basicAuthPwd = ParamUtil.getString(actionRequest,"basicAuthPwd");
			
			boolean emailNotificationsEnabled =  ParamUtil.getBoolean(actionRequest,"emailNotificationsEnabled");	
			boolean dockbarNotificationsEnabled =  ParamUtil.getBoolean(actionRequest,"dockbarNotificationsEnabled");	
			
			boolean jmsEnabled =  ParamUtil.getBoolean(actionRequest,"jmsEnabled");
			String brokerJMSusername = ParamUtil.getString(actionRequest,"brokerJMSusername");
			String brokerJMSpassword = ParamUtil.getString(actionRequest,"brokerJMSpassword");
			String brokerJMSurl = ParamUtil.getString(actionRequest,"brokerJMSurl");
			String jmsTopic = ParamUtil.getString(actionRequest,"jmsTopic");
			
			boolean verboseEnabled =  ParamUtil.getBoolean(actionRequest,"verboseEnabled");	
			boolean pilotingEnabled =  ParamUtil.getBoolean(actionRequest,"pilotingEnabled");
			boolean needEnabled =  ParamUtil.getBoolean(actionRequest,"needEnabled");
			boolean publicIdeasEnabled =  ParamUtil.getBoolean(actionRequest,"publicIdeasEnabled");
			boolean reducedLifecycle =  ParamUtil.getBoolean(actionRequest,"reducedLifecycle");
			boolean fundingBoxEnabled =  ParamUtil.getBoolean(actionRequest,"fundingBoxEnabled");
			
			String fundingBoxAddress = ParamUtil.getString(actionRequest,"fundingBoxAddress");
			String fundingBoxAPIAddress = ParamUtil.getString(actionRequest,"fundingBoxAPIAddress");
			
			boolean googleMapsAPIKeyEnabled =  ParamUtil.getBoolean(actionRequest,"googleMapsAPIKeyEnabled");
			String googleMapsAPIKey = ParamUtil.getString(actionRequest,"googleMapsAPIKey");
			
			boolean graylogEnabled =  ParamUtil.getBoolean(actionRequest,"graylogEnabled");
			String graylogAddress = ParamUtil.getString(actionRequest,"graylogAddress");
			
			boolean emailOnNewChallengeEnabled =  ParamUtil.getBoolean(actionRequest,"emailOnNewChallengeEnabled");
			boolean virtuosityPointsEnabled =  ParamUtil.getBoolean(actionRequest,"virtuosityPointsEnabled");
			String orionUrl = ParamUtil.getString(actionRequest,"orionUrl");
			

			// creo una nuova entita suffix
			CLSFriendlyUrlSuffix friendlysuffix = new CLSFriendlyUrlSuffixImpl();
			friendlysuffix.setFriendlyUrlSuffixID(1);
			friendlysuffix.setUrlSuffixImsHome(suffixImsHome);
			friendlysuffix.setUrlSuffixChallenge(sfxChallenges);
			friendlysuffix.setUrlSuffixIdea(sfxIdeas);
			friendlysuffix.setUrlSuffixNeed(sfxNeeds);
			friendlysuffix.setSenderNotificheMailIdeario(senderNotificheMailIdeario);
			friendlysuffix.setOggettoNotificheMailIdeario(oggettoNotificheMailIdeario);
			friendlysuffix.setFirmaNotificheMailIdeario(firmaNotificheMailIdeario);
			friendlysuffix.setUtenzaMail(utenzaMail);
			friendlysuffix.setMktEnabled(mktEnabled);
			friendlysuffix.setMockEnabled(mockEnabled);
			friendlysuffix.setFiwareEnabled(fiwareEnabled);
			friendlysuffix.setFiwareRemoteCatalogueEnabled(fiwareRemoteCatalogueEnabled);
			friendlysuffix.setFiwareCatalogueAddress(fiwareCatalogueAddress);
			friendlysuffix.setCdvEnabled(cdvEnabled);
			friendlysuffix.setCdvAddress(cdvAddress);
			friendlysuffix.setVcEnabled(vcEnabled);
			friendlysuffix.setVcAddress(vcAddress);
			friendlysuffix.setVcWSAddress(vcWSAddress);
			friendlysuffix.setDeEnabled(deEnabled);
			friendlysuffix.setDeAddress(deAddress);
			friendlysuffix.setLbbEnabled(lbbEnabled);
			friendlysuffix.setLbbAddress(lbbAddress);
			friendlysuffix.setOiaAppId4lbb(oiaAppId4lbb);
			friendlysuffix.setTweetingEnabled(tweetingEnabled);
			friendlysuffix.setBasicAuthUser(basicAuthUser);
			friendlysuffix.setBasicAuthPwd(basicAuthPwd);
			friendlysuffix.setJmsEnabled(jmsEnabled);
			friendlysuffix.setEmailNotificationsEnabled(emailNotificationsEnabled);	
			friendlysuffix.setDockbarNotificationsEnabled(dockbarNotificationsEnabled);
			friendlysuffix.setBrokerJMSusername(brokerJMSusername);
			friendlysuffix.setBrokerJMSpassword(brokerJMSpassword);
			friendlysuffix.setBrokerJMSurl(brokerJMSurl);
			friendlysuffix.setJmsTopic(jmsTopic);
			friendlysuffix.setVerboseEnabled(verboseEnabled);
			friendlysuffix.setPilotingEnabled(pilotingEnabled);
			friendlysuffix.setNeedEnabled(needEnabled);
			friendlysuffix.setPublicIdeasEnabled(publicIdeasEnabled);
			friendlysuffix.setReducedLifecycle(reducedLifecycle);
			friendlysuffix.setFundingBoxEnabled(fundingBoxEnabled);
			friendlysuffix.setFundingBoxAddress(fundingBoxAddress);
			friendlysuffix.setFundingBoxAPIAddress(fundingBoxAPIAddress);
			friendlysuffix.setGoogleMapsAPIKeyEnabled(googleMapsAPIKeyEnabled);
			friendlysuffix.setGoogleMapsAPIKey(googleMapsAPIKey);
			friendlysuffix.setGraylogEnabled(graylogEnabled);
			friendlysuffix.setGraylogAddress(graylogAddress);
			friendlysuffix.setEmailOnNewChallengeEnabled(emailOnNewChallengeEnabled);
			friendlysuffix.setVirtuosityPointsEnabled(virtuosityPointsEnabled);
			friendlysuffix.setOrionUrl(orionUrl);

			if(CLSFriendlyUrlSuffixLocalServiceUtil.getCLSFriendlyUrlSuffixsCount()>0){				
				CLSFriendlyUrlSuffixLocalServiceUtil.updateCLSFriendlyUrlSuffix(friendlysuffix);
			}else{
				CLSFriendlyUrlSuffixLocalServiceUtil.addCLSFriendlyUrlSuffix(friendlysuffix);
			}
		}
				
		// recupero latitudine e longitudine per il centro della mappa
		String latitude = ParamUtil.getString(actionRequest,"Latitude");
		String longitude = ParamUtil.getString(actionRequest,"Longitude");
		CLSMapProperties mapProperties = new CLSMapPropertiesImpl();
		mapProperties.setMapPropertiesId(1);
		mapProperties.setMapCenterLatitude(latitude);
		mapProperties.setMapCenterLongitude(longitude);
		if(CLSMapPropertiesLocalServiceUtil.getCLSMapPropertiesesCount()>0){
			CLSMapPropertiesLocalServiceUtil.updateCLSMapProperties(mapProperties);
		}else{
			CLSMapPropertiesLocalServiceUtil.addCLSMapProperties(mapProperties);
		}
		
		//recupero il calendario di riferimento per le Gare di Idee
		long calendarId = ParamUtil.getLong(actionRequest,"challengeCalendar");
		CLSChallengesCalendar challengesCalendar = new CLSChallengesCalendarImpl();
		challengesCalendar.setChallengesCalendarId(1);
		challengesCalendar.setReferenceCalendarId(calendarId);
		if(CLSChallengesCalendarLocalServiceUtil.getCLSChallengesCalendarsCount()>0){
			CLSChallengesCalendarLocalServiceUtil.updateCLSChallengesCalendar(challengesCalendar);
		}else{
			CLSChallengesCalendarLocalServiceUtil.addCLSChallengesCalendar(challengesCalendar);
		}
		
		
	}
	
	
	public long getChallengesReferenceCalendarId() throws SystemException{
		long referenceCalendarId = -1;
		if(CLSChallengesCalendarLocalServiceUtil.getCLSChallengesCalendarsCount()>0){
			CLSChallengesCalendar challengesCalendar = CLSChallengesCalendarLocalServiceUtil.fetchCLSChallengesCalendar(1);
			referenceCalendarId = challengesCalendar.getReferenceCalendarId();
		}
		return referenceCalendarId;
	}
	
	public List<CLSCategoriesSet> getCategoriesSetIMS() throws SystemException, PortletException{
		List<CLSCategoriesSet> allcategoriesSets = CLSCategoriesSetLocalServiceUtil.getCLSCategoriesSets(com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS, com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS);
		return allcategoriesSets;
	}

	public List<Long> getMapCategoriesSetIMS() throws SystemException, PortletException{
		ArrayList<Long> mapCategoriesSet = new ArrayList<Long>();
		List<CLSCategoriesSet> allcategoriesSets = CLSCategoriesSetLocalServiceUtil.getCLSCategoriesSets(com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS, com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS);
		Iterator<CLSCategoriesSet> allcategoriesSetIt = allcategoriesSets.iterator();
		while (allcategoriesSetIt.hasNext()) {
			CLSCategoriesSet clsCategoriesSet = (CLSCategoriesSet) allcategoriesSetIt.next();
			mapCategoriesSet.add(clsCategoriesSet.getCategoriesSetID());
		}
		
		return mapCategoriesSet;
	}
	
	public static String getMapCenterLatitude() throws SystemException{
		String latitude = "40.353237"; //,18.172545"
		if(CLSMapPropertiesLocalServiceUtil.getCLSMapPropertiesesCount()>0){
			CLSMapProperties mapProperty = CLSMapPropertiesLocalServiceUtil.fetchCLSMapProperties(1);
			latitude = mapProperty.getMapCenterLatitude();
		}
		return latitude;
	}
	
	public static String getMapCenterLongitude() throws SystemException{
		String longitude = "18.172545";
		if(CLSMapPropertiesLocalServiceUtil.getCLSMapPropertiesesCount()>0){
			CLSMapProperties mapProperty = CLSMapPropertiesLocalServiceUtil.fetchCLSMapProperties(1);
			longitude = mapProperty.getMapCenterLongitude();
		}
		return longitude;
	}
	
	
	/**
	    * @return Home IMS SUffix 
	    */
	public static String getUrlSuffixImsHome()throws IOException, PortletException, PortalException, SystemException {
		String retVal = ""; 
		if(CLSFriendlyUrlSuffixLocalServiceUtil.getCLSFriendlyUrlSuffixsCount()>0){
			List<CLSFriendlyUrlSuffix> friendlyUrlSuffix = CLSFriendlyUrlSuffixLocalServiceUtil.getCLSFriendlyUrlSuffixs(0, CLSFriendlyUrlSuffixLocalServiceUtil.getCLSFriendlyUrlSuffixsCount());//.getCLSFriendlyUrlSuffix(1);
			if(friendlyUrlSuffix!=null){
				retVal = friendlyUrlSuffix.get(0).getUrlSuffixImsHome();
			}		
		}
		
		return retVal;
	}
	
	
	 /**
	    * @return IDEA Friendly URL Suffix 
	    */
	public static String getFriendlyUrlSuffixIdeas()throws IOException, PortletException, PortalException, SystemException {
		String retVal = ""; 
		if(CLSFriendlyUrlSuffixLocalServiceUtil.getCLSFriendlyUrlSuffixsCount()>0){
			List<CLSFriendlyUrlSuffix> friendlyUrlSuffix = CLSFriendlyUrlSuffixLocalServiceUtil.getCLSFriendlyUrlSuffixs(com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS, com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS);//.getCLSFriendlyUrlSuffix(1);
			if(friendlyUrlSuffix!=null){
				retVal = friendlyUrlSuffix.get(0).getUrlSuffixIdea();
			}		
		}
		
		return retVal;
	}
	
	/**
	    * @return Need Friendly URL Suffix 
	    */
	public static String getFriendlyUrlSuffixNeeds()throws IOException, PortletException, PortalException, SystemException {
		String retVal = ""; 
		if(CLSFriendlyUrlSuffixLocalServiceUtil.getCLSFriendlyUrlSuffixsCount()>0){
			List<CLSFriendlyUrlSuffix> friendlyUrlSuffix = CLSFriendlyUrlSuffixLocalServiceUtil.getCLSFriendlyUrlSuffixs(com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS, com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS);//.getCLSFriendlyUrlSuffix(1);
			if(friendlyUrlSuffix!=null){
				retVal = friendlyUrlSuffix.get(0).getUrlSuffixNeed();
			}		
		}
		
		return retVal;
	}
	
	
	 /**
	    * @return CHALLENGE Friendly URL Suffix
	    */
	public static String getFriendlyUrlSuffixChallenges()throws IOException, PortletException, PortalException, SystemException {
		String retVal = ""; 
		
		if(CLSFriendlyUrlSuffixLocalServiceUtil.getCLSFriendlyUrlSuffixsCount()>0){		
			List<CLSFriendlyUrlSuffix> friendlyUrlSuffix = CLSFriendlyUrlSuffixLocalServiceUtil.getCLSFriendlyUrlSuffixs(com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS, com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS); //.getCLSFriendlyUrlSuffix(1); //
			if(friendlyUrlSuffix!=null){
				retVal = friendlyUrlSuffix.get(0).getUrlSuffixChallenge();
			}
		}
		
		return retVal;
	}
	
	
	/**
	 * @param challengeID
	 * @return
	 */
	public static String getFriendlyUrlChallenges(long challengeID) {
		String retVal = ""; 

		try {
			String rootUrl = getRootUrl();
			
			retVal = rootUrl+"/"+getFriendlyUrlSuffixChallenges()+"/-/challenges_explorer_contest/"+challengeID+"/view";
		} catch (PortalException | SystemException | IOException | PortletException e) {
			
			e.printStackTrace();
			return retVal;
		}
		
		return retVal;
	}
	
	/**
	 * @param challengeID
	 * @return
	 */
	public static String getFriendlyUrlChallengesCheckHttps(long challengeID, boolean isSecure) {
		String retVal = ""; 

		try {
			String rootUrl = getRootUrlCheckHttps(isSecure);
			
			retVal = rootUrl+"/"+getFriendlyUrlSuffixChallenges()+"/-/challenges_explorer_contest/"+challengeID+"/view";
		} catch (PortalException | SystemException | IOException | PortletException e) {
			
			e.printStackTrace();
			return retVal;
		}
		
		return retVal;
	}
	
	
	/**
	 * @param ideaID
	 * @return
	 */
	public static String getFriendlyUrlIdeas(long ideaID) {
		
			String retVal = "";
			String rootUrl = getRootUrl();
			
			try {
				retVal = rootUrl+"/"+getFriendlyUrlSuffixIdeas()+"/-/ideas_explorer_contest/"+ideaID+"/view";
			} catch (PortalException | SystemException | IOException | PortletException e) {
				
				e.printStackTrace();
				return retVal;
			}
		

		return retVal;
	}
	
	/**
	 * @param ideaID
	 * @return
	 */
	public static String getFriendlyUrlIdeasCheckHttps(long ideaID, boolean isSecure) {
		
			String retVal = "";
			String rootUrl = getRootUrlCheckHttps(isSecure);
			
			try {
				retVal = rootUrl+"/"+getFriendlyUrlSuffixIdeas()+"/-/ideas_explorer_contest/"+ideaID+"/view";
			} catch (PortalException | SystemException | IOException | PortletException e) {
				
				e.printStackTrace();
				return retVal;
			}
		

		return retVal;
	}
	
	/**
	 * @param ideaID
	 * @return
	 */
	public static String getFriendlyUrlNeeds(long ideaID) {
		String retVal = ""; 

		try {
			String rootUrl = getRootUrl();
			
			retVal = rootUrl+"/"+getFriendlyUrlSuffixNeeds()+"/-/needs_explorer_contest/"+ideaID+"/view";
		} catch (PortalException | SystemException | IOException | PortletException e) {
			
			e.printStackTrace();
			return retVal;
		}
		 		
		return retVal;
	}
	
	/**
	 * @param ideaID
	 * @return
	 */
	public static String getFriendlyUrlNeedsCheckHttps(long ideaID, boolean isSecure) {
		String retVal = ""; 

		try {
			String rootUrl = getRootUrlCheckHttps(isSecure);
			
			retVal = rootUrl+"/"+getFriendlyUrlSuffixNeeds()+"/-/needs_explorer_contest/"+ideaID+"/view";
		} catch (PortalException | SystemException | IOException | PortletException e) {
			
			e.printStackTrace();
			return retVal;
		}
		 		
		return retVal;
	}
	
	
	/**
	 * @return
	 */
	public static String getUrlHomeIMS() {
		
		String retVal = ""; 
			
		String rootUrl = getRootUrl();

			try {
				retVal = rootUrl+"/"+getUrlSuffixImsHome();
			} catch (PortalException | SystemException | IOException | PortletException e) {
				
				e.printStackTrace();
				return retVal;
			}
		
		return retVal;
	}
	

	
	/**
	 * @return
	 */
	public static String getRootUrl() {
		
		String rootUrl = ""; 

		//Ottengo utente default Liferay
		 String webId = new String("liferay.com");
		 Company company = null;
		try {
			company = CompanyLocalServiceUtil.getCompanyByWebId(webId);
			 rootUrl = PortalUtil.getPortalURL(company.getVirtualHostname(), PortalUtil.getPortalPort(), false);
			
		} catch (PortalException | SystemException e) {
            e.printStackTrace();
			 return rootUrl;
		}
		
		rootUrl= rootUrl.replaceAll(":-1", "");
		
		return rootUrl;
	}
	
	/**
	 * @return
	 */
	public static String getRootUrlCheckHttps(boolean isSecure) {
		
		String rootUrl = ""; 

		//Ottengo utente default Liferay
		 String webId = new String("liferay.com");
		 Company company = null;
		try {
			company = CompanyLocalServiceUtil.getCompanyByWebId(webId);
			 rootUrl = PortalUtil.getPortalURL(company.getVirtualHostname(), PortalUtil.getPortalPort(), isSecure);
			
		} catch (PortalException | SystemException e) {
            e.printStackTrace();
			 return rootUrl;
		}
		
		
		rootUrl= rootUrl.replaceAll(":-1", "");
		
		return rootUrl;
	}

	
	
	
	/**
	 * @return
	 */
	public static String getUrlChallengesExplorerPage (){
		
		boolean reducedLifecycle = getEnabledProperty("reducedLifecycle");
		if (reducedLifecycle){
			long challengeId = ChallengesUtils.getDefaultChallengeIdForReducedLifecycle();
			return getFriendlyUrlChallenges(challengeId);
		}
		
		String rootUrl = getRootUrl();
		
		String suffix = "";
		try {
			suffix = getFriendlyUrlSuffixChallenges();
		} catch (PortalException | SystemException | IOException| PortletException e) {
			
			e.printStackTrace();
			return rootUrl;
		}
		
		String completo = rootUrl +"/" + suffix ;
		
		return completo;
		
	}
	
	/**
	 * @return
	 */
	public static String getFriendyUrlCreateChallenge (){
		
		String page =getUrlChallengesExplorerPage();
		
		String completo = page +"/-/challenges_explorer_contest/new-challenge";
		
		return completo;
		
	}
	
	/**
	 * @return
	 */
	public static String getUrlIdeasExplorerPage (){
		
		String rootUrl = getRootUrl();
		
		
		String suffix = "";
		try {
			suffix = getFriendlyUrlSuffixIdeas();
		} catch (PortalException | SystemException | IOException| PortletException e) {
			
			e.printStackTrace();
			return rootUrl;
		}
		
		String completo = rootUrl +"/" + suffix ;
		
		return completo;
		
	}
	
	/**
	 * @return
	 */
	public static String getFriendyUrlCreateIdea (){
		
		String page =getUrlIdeasExplorerPage();
		
		String completo = page +"/-/ideas_explorer_contest/new-idea";
		
		return completo;
		
	}
	
	
	/**
	 * @return
	 */
	public static String getUrlNeedsExplorerPage (){
		
		String rootUrl = getRootUrl();
		
		
		String suffix = "";
		try {
			suffix = getFriendlyUrlSuffixNeeds();
		} catch (PortalException | SystemException | IOException| PortletException e) {
			
			e.printStackTrace();
			return rootUrl;
		}
		
		String completo = rootUrl +"/" + suffix ;
		
		return completo;
		
	}
	
	/**
	 * @return
	 */
	public static String getFriendyUrlCreateNeed (){
		
		String page =getUrlNeedsExplorerPage();
		
		String completo = page +"/-/needs_explorer_contest/new-need";
		
		return completo;
		
	}
	

	
	/**
	 * @return
	 */
	public static String getFriendyUrlCreateIdeaWizard (){
		
		String page =getUrlHomeIMS();
		String full = page +"#sectionOne/slide3";
		
		boolean reducedLifecycle = getEnabledProperty("reducedLifecycle");
		
		if (reducedLifecycle){
			
			long challengeId = ChallengesUtils.getDefaultChallengeIdForReducedLifecycle();
			full= page+"?c="+challengeId;
		}
		
		
		
		return full;
		
	}
	
	/**
	 * @return
	 */
	public static String getFriendyUrlCreateNeedWizard (){
		
		String page =getUrlHomeIMS();
		
		String completo = page +"#sectionOne/slide1";
		
		return completo;
		
	}
	
	
	 /**
	    * @return properties filtrate per input
	    */
	public static String getProperty(String input ){
		String retVal = ""; 
		
		int quantiRecord = 0;
		
		 try {
			quantiRecord = CLSFriendlyUrlSuffixLocalServiceUtil.getCLSFriendlyUrlSuffixsCount();
		} catch (SystemException e) {
			
			e.printStackTrace();
		}
		
		
		
		if(quantiRecord>0){		
			List<CLSFriendlyUrlSuffix> friendlyUrlSuffix = new ArrayList<CLSFriendlyUrlSuffix>();
			try {
				friendlyUrlSuffix = CLSFriendlyUrlSuffixLocalServiceUtil.getCLSFriendlyUrlSuffixs(0, quantiRecord);
			} catch (SystemException e) {
				
				e.printStackTrace();
				return retVal;
			} 
			
			
			if(friendlyUrlSuffix!=null){
				
				switch (input){
				
						case "senderNotificheMailIdeario":
							retVal = friendlyUrlSuffix.get(0).getSenderNotificheMailIdeario(); break;
							
						case "oggettoNotificheMailIdeario":
							retVal = friendlyUrlSuffix.get(0).getOggettoNotificheMailIdeario(); break;
							
						case "firmaNotificheMailIdeario":
							retVal = friendlyUrlSuffix.get(0).getFirmaNotificheMailIdeario(); break;
							
						case "utenzaMail":
							retVal = friendlyUrlSuffix.get(0).getUtenzaMail(); break;
							
						case "cdvAddress":
							retVal = friendlyUrlSuffix.get(0).getCdvAddress(); break;
							
						case "vcAddress":
							retVal = friendlyUrlSuffix.get(0).getVcAddress(); break;
							
						case "vcWSAddress":
							retVal = friendlyUrlSuffix.get(0).getVcWSAddress(); break;
							
						case "deAddress":
							retVal = friendlyUrlSuffix.get(0).getDeAddress(); break;
							
						case "lbbAddress":
							retVal = friendlyUrlSuffix.get(0).getLbbAddress(); break;
							
						case "oiaAppId4lbb":
							retVal = friendlyUrlSuffix.get(0).getOiaAppId4lbb(); break;
							
						case "basicAuthUser":
							retVal = friendlyUrlSuffix.get(0).getBasicAuthUser(); break;
							
						case "basicAuthPwd":
							retVal = friendlyUrlSuffix.get(0).getBasicAuthPwd(); break;
							
						case "brokerJMSusername":
							retVal = friendlyUrlSuffix.get(0).getBrokerJMSusername(); break;
							
						case "brokerJMSpassword":
							retVal = friendlyUrlSuffix.get(0).getBrokerJMSpassword(); break;
							
						case "brokerJMSurl":
							retVal = friendlyUrlSuffix.get(0).getBrokerJMSurl(); break;
							
						case "jmsTopic":
							retVal = friendlyUrlSuffix.get(0).getJmsTopic(); break;
							
						case "fiwareCatalogueAddress":
							retVal = friendlyUrlSuffix.get(0).getFiwareCatalogueAddress(); break;
							
						case "fundingBoxAddress":
							retVal = friendlyUrlSuffix.get(0).getFundingBoxAddress(); break;
							
						case "fundingBoxAPIAddress":
							retVal = friendlyUrlSuffix.get(0).getFundingBoxAPIAddress(); break;
							
						case "googleMapsAPIKey":
							retVal = friendlyUrlSuffix.get(0).getGoogleMapsAPIKey(); break;
							
						case "graylogAddress":
							retVal = friendlyUrlSuffix.get(0).getGraylogAddress(); break;
							
						case "orionUrl":
							retVal = friendlyUrlSuffix.get(0).getOrionUrl(); break;
							
				}

			}
			
		}
		
		return retVal;
	}
	
	
	/**
	    * @return properties filtrate per input
	    */
	public static boolean getEnabledProperty(String input ){
		boolean retVal = false; 
		
		int quantiRecord = 0;
		
		 try {
			quantiRecord = CLSFriendlyUrlSuffixLocalServiceUtil.getCLSFriendlyUrlSuffixsCount();
		} catch (SystemException e) {
			
			e.printStackTrace();
		}
		
		
		
		if(quantiRecord>0){		
			List<CLSFriendlyUrlSuffix> friendlyUrlSuffix = new ArrayList<CLSFriendlyUrlSuffix>();
			try {
				friendlyUrlSuffix = CLSFriendlyUrlSuffixLocalServiceUtil.getCLSFriendlyUrlSuffixs(0, quantiRecord);
			} catch (SystemException e) {
				
				e.printStackTrace();
				return retVal;
			} 
			
			
			if(friendlyUrlSuffix!=null){
				
				switch (input){
						case "mktEnabled":
							retVal = friendlyUrlSuffix.get(0).getMktEnabled(); break;
							
						case "cdvEnabled":
							retVal = friendlyUrlSuffix.get(0).getCdvEnabled(); break;
							
						case "vcEnabled":
							retVal = friendlyUrlSuffix.get(0).getVcEnabled(); break;
							
						case "jmsEnabled":
							retVal = friendlyUrlSuffix.get(0).getJmsEnabled(); break;
							
						case "emailNotificationsEnabled":
							retVal = friendlyUrlSuffix.get(0).getEmailNotificationsEnabled(); break;
							
						case "dockbarNotificationsEnabled":
							retVal = friendlyUrlSuffix.get(0).getDockbarNotificationsEnabled(); break;
							
						case "deEnabled":
							retVal = friendlyUrlSuffix.get(0).getDeEnabled(); break;
							
						case "lbbEnabled":
							retVal = friendlyUrlSuffix.get(0).getLbbEnabled(); break;
							
						case "tweetingEnabled":
							retVal = friendlyUrlSuffix.get(0).getTweetingEnabled(); break;
							
						case "verboseEnabled":
							retVal = friendlyUrlSuffix.get(0).getVerboseEnabled(); break;
							
						case "pilotingEnabled":
							retVal = friendlyUrlSuffix.get(0).getPilotingEnabled(); break;
							
						case "needEnabled":
							retVal = friendlyUrlSuffix.get(0).getNeedEnabled(); break;	
							
						case "mockEnabled":
							retVal = friendlyUrlSuffix.get(0).getMockEnabled(); break;
							
						case "fiwareEnabled":
							retVal = friendlyUrlSuffix.get(0).getFiwareEnabled(); break;
							
						case "fiwareRemoteCatalogueEnabled":
							retVal = friendlyUrlSuffix.get(0).getFiwareRemoteCatalogueEnabled(); break;
							
						case "publicIdeasEnabled":
							retVal = friendlyUrlSuffix.get(0).getPublicIdeasEnabled(); break;
							
						case "reducedLifecycle":
							retVal = friendlyUrlSuffix.get(0).getReducedLifecycle(); break;
							
						case "fundingBoxEnabled":
							retVal = friendlyUrlSuffix.get(0).getFundingBoxEnabled(); break;
							
						case "googleMapsAPIKeyEnabled":
							retVal = friendlyUrlSuffix.get(0).getGoogleMapsAPIKeyEnabled(); break;
							
						case "graylogEnabled":
							retVal = friendlyUrlSuffix.get(0).getGraylogEnabled(); break;
							
						case "virtuosityPointsEnabled":
							retVal = friendlyUrlSuffix.get(0).getVirtuosityPointsEnabled(); break;
							
						case "emailOnNewChallengeEnabled":
							retVal = friendlyUrlSuffix.get(0).getEmailOnNewChallengeEnabled(); break;
							
							
				}

			}
		}
		
		
		return retVal;
	}
	
	
}
