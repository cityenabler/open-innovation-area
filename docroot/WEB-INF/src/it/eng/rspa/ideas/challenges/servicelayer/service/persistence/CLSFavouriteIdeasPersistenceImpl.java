/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteIdeasImpl;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteIdeasModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the c l s favourite ideas service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSFavouriteIdeasPersistence
 * @see CLSFavouriteIdeasUtil
 * @generated
 */
public class CLSFavouriteIdeasPersistenceImpl extends BasePersistenceImpl<CLSFavouriteIdeas>
	implements CLSFavouriteIdeasPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CLSFavouriteIdeasUtil} to access the c l s favourite ideas persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CLSFavouriteIdeasImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CLSFavouriteIdeasModelImpl.ENTITY_CACHE_ENABLED,
			CLSFavouriteIdeasModelImpl.FINDER_CACHE_ENABLED,
			CLSFavouriteIdeasImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CLSFavouriteIdeasModelImpl.ENTITY_CACHE_ENABLED,
			CLSFavouriteIdeasModelImpl.FINDER_CACHE_ENABLED,
			CLSFavouriteIdeasImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CLSFavouriteIdeasModelImpl.ENTITY_CACHE_ENABLED,
			CLSFavouriteIdeasModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEAID = new FinderPath(CLSFavouriteIdeasModelImpl.ENTITY_CACHE_ENABLED,
			CLSFavouriteIdeasModelImpl.FINDER_CACHE_ENABLED,
			CLSFavouriteIdeasImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByIdeaID",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAID =
		new FinderPath(CLSFavouriteIdeasModelImpl.ENTITY_CACHE_ENABLED,
			CLSFavouriteIdeasModelImpl.FINDER_CACHE_ENABLED,
			CLSFavouriteIdeasImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByIdeaID",
			new String[] { Long.class.getName() },
			CLSFavouriteIdeasModelImpl.IDEAID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_IDEAID = new FinderPath(CLSFavouriteIdeasModelImpl.ENTITY_CACHE_ENABLED,
			CLSFavouriteIdeasModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByIdeaID",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the c l s favourite ideases where ideaID = &#63;.
	 *
	 * @param ideaID the idea i d
	 * @return the matching c l s favourite ideases
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSFavouriteIdeas> findByIdeaID(long ideaID)
		throws SystemException {
		return findByIdeaID(ideaID, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s favourite ideases where ideaID = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteIdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaID the idea i d
	 * @param start the lower bound of the range of c l s favourite ideases
	 * @param end the upper bound of the range of c l s favourite ideases (not inclusive)
	 * @return the range of matching c l s favourite ideases
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSFavouriteIdeas> findByIdeaID(long ideaID, int start, int end)
		throws SystemException {
		return findByIdeaID(ideaID, start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s favourite ideases where ideaID = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteIdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaID the idea i d
	 * @param start the lower bound of the range of c l s favourite ideases
	 * @param end the upper bound of the range of c l s favourite ideases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s favourite ideases
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSFavouriteIdeas> findByIdeaID(long ideaID, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAID;
			finderArgs = new Object[] { ideaID };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEAID;
			finderArgs = new Object[] { ideaID, start, end, orderByComparator };
		}

		List<CLSFavouriteIdeas> list = (List<CLSFavouriteIdeas>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSFavouriteIdeas clsFavouriteIdeas : list) {
				if ((ideaID != clsFavouriteIdeas.getIdeaID())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CLSFAVOURITEIDEAS_WHERE);

			query.append(_FINDER_COLUMN_IDEAID_IDEAID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSFavouriteIdeasModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaID);

				if (!pagination) {
					list = (List<CLSFavouriteIdeas>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSFavouriteIdeas>(list);
				}
				else {
					list = (List<CLSFavouriteIdeas>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s favourite ideas in the ordered set where ideaID = &#63;.
	 *
	 * @param ideaID the idea i d
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s favourite ideas
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException if a matching c l s favourite ideas could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteIdeas findByIdeaID_First(long ideaID,
		OrderByComparator orderByComparator)
		throws NoSuchCLSFavouriteIdeasException, SystemException {
		CLSFavouriteIdeas clsFavouriteIdeas = fetchByIdeaID_First(ideaID,
				orderByComparator);

		if (clsFavouriteIdeas != null) {
			return clsFavouriteIdeas;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaID=");
		msg.append(ideaID);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSFavouriteIdeasException(msg.toString());
	}

	/**
	 * Returns the first c l s favourite ideas in the ordered set where ideaID = &#63;.
	 *
	 * @param ideaID the idea i d
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s favourite ideas, or <code>null</code> if a matching c l s favourite ideas could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteIdeas fetchByIdeaID_First(long ideaID,
		OrderByComparator orderByComparator) throws SystemException {
		List<CLSFavouriteIdeas> list = findByIdeaID(ideaID, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s favourite ideas in the ordered set where ideaID = &#63;.
	 *
	 * @param ideaID the idea i d
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s favourite ideas
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException if a matching c l s favourite ideas could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteIdeas findByIdeaID_Last(long ideaID,
		OrderByComparator orderByComparator)
		throws NoSuchCLSFavouriteIdeasException, SystemException {
		CLSFavouriteIdeas clsFavouriteIdeas = fetchByIdeaID_Last(ideaID,
				orderByComparator);

		if (clsFavouriteIdeas != null) {
			return clsFavouriteIdeas;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaID=");
		msg.append(ideaID);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSFavouriteIdeasException(msg.toString());
	}

	/**
	 * Returns the last c l s favourite ideas in the ordered set where ideaID = &#63;.
	 *
	 * @param ideaID the idea i d
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s favourite ideas, or <code>null</code> if a matching c l s favourite ideas could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteIdeas fetchByIdeaID_Last(long ideaID,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByIdeaID(ideaID);

		if (count == 0) {
			return null;
		}

		List<CLSFavouriteIdeas> list = findByIdeaID(ideaID, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s favourite ideases before and after the current c l s favourite ideas in the ordered set where ideaID = &#63;.
	 *
	 * @param clsFavouriteIdeasPK the primary key of the current c l s favourite ideas
	 * @param ideaID the idea i d
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s favourite ideas
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException if a c l s favourite ideas with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteIdeas[] findByIdeaID_PrevAndNext(
		CLSFavouriteIdeasPK clsFavouriteIdeasPK, long ideaID,
		OrderByComparator orderByComparator)
		throws NoSuchCLSFavouriteIdeasException, SystemException {
		CLSFavouriteIdeas clsFavouriteIdeas = findByPrimaryKey(clsFavouriteIdeasPK);

		Session session = null;

		try {
			session = openSession();

			CLSFavouriteIdeas[] array = new CLSFavouriteIdeasImpl[3];

			array[0] = getByIdeaID_PrevAndNext(session, clsFavouriteIdeas,
					ideaID, orderByComparator, true);

			array[1] = clsFavouriteIdeas;

			array[2] = getByIdeaID_PrevAndNext(session, clsFavouriteIdeas,
					ideaID, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSFavouriteIdeas getByIdeaID_PrevAndNext(Session session,
		CLSFavouriteIdeas clsFavouriteIdeas, long ideaID,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSFAVOURITEIDEAS_WHERE);

		query.append(_FINDER_COLUMN_IDEAID_IDEAID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSFavouriteIdeasModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(ideaID);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsFavouriteIdeas);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSFavouriteIdeas> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s favourite ideases where ideaID = &#63; from the database.
	 *
	 * @param ideaID the idea i d
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByIdeaID(long ideaID) throws SystemException {
		for (CLSFavouriteIdeas clsFavouriteIdeas : findByIdeaID(ideaID,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(clsFavouriteIdeas);
		}
	}

	/**
	 * Returns the number of c l s favourite ideases where ideaID = &#63;.
	 *
	 * @param ideaID the idea i d
	 * @return the number of matching c l s favourite ideases
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByIdeaID(long ideaID) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_IDEAID;

		Object[] finderArgs = new Object[] { ideaID };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLSFAVOURITEIDEAS_WHERE);

			query.append(_FINDER_COLUMN_IDEAID_IDEAID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaID);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_IDEAID_IDEAID_2 = "clsFavouriteIdeas.id.ideaID = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID = new FinderPath(CLSFavouriteIdeasModelImpl.ENTITY_CACHE_ENABLED,
			CLSFavouriteIdeasModelImpl.FINDER_CACHE_ENABLED,
			CLSFavouriteIdeasImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUserID",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID =
		new FinderPath(CLSFavouriteIdeasModelImpl.ENTITY_CACHE_ENABLED,
			CLSFavouriteIdeasModelImpl.FINDER_CACHE_ENABLED,
			CLSFavouriteIdeasImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUserID",
			new String[] { Long.class.getName() },
			CLSFavouriteIdeasModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_USERID = new FinderPath(CLSFavouriteIdeasModelImpl.ENTITY_CACHE_ENABLED,
			CLSFavouriteIdeasModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUserID",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the c l s favourite ideases where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching c l s favourite ideases
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSFavouriteIdeas> findByUserID(long userId)
		throws SystemException {
		return findByUserID(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s favourite ideases where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteIdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of c l s favourite ideases
	 * @param end the upper bound of the range of c l s favourite ideases (not inclusive)
	 * @return the range of matching c l s favourite ideases
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSFavouriteIdeas> findByUserID(long userId, int start, int end)
		throws SystemException {
		return findByUserID(userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s favourite ideases where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteIdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of c l s favourite ideases
	 * @param end the upper bound of the range of c l s favourite ideases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s favourite ideases
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSFavouriteIdeas> findByUserID(long userId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_USERID;
			finderArgs = new Object[] { userId, start, end, orderByComparator };
		}

		List<CLSFavouriteIdeas> list = (List<CLSFavouriteIdeas>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSFavouriteIdeas clsFavouriteIdeas : list) {
				if ((userId != clsFavouriteIdeas.getUserId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CLSFAVOURITEIDEAS_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSFavouriteIdeasModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				if (!pagination) {
					list = (List<CLSFavouriteIdeas>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSFavouriteIdeas>(list);
				}
				else {
					list = (List<CLSFavouriteIdeas>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s favourite ideas in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s favourite ideas
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException if a matching c l s favourite ideas could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteIdeas findByUserID_First(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSFavouriteIdeasException, SystemException {
		CLSFavouriteIdeas clsFavouriteIdeas = fetchByUserID_First(userId,
				orderByComparator);

		if (clsFavouriteIdeas != null) {
			return clsFavouriteIdeas;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSFavouriteIdeasException(msg.toString());
	}

	/**
	 * Returns the first c l s favourite ideas in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s favourite ideas, or <code>null</code> if a matching c l s favourite ideas could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteIdeas fetchByUserID_First(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		List<CLSFavouriteIdeas> list = findByUserID(userId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s favourite ideas in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s favourite ideas
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException if a matching c l s favourite ideas could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteIdeas findByUserID_Last(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSFavouriteIdeasException, SystemException {
		CLSFavouriteIdeas clsFavouriteIdeas = fetchByUserID_Last(userId,
				orderByComparator);

		if (clsFavouriteIdeas != null) {
			return clsFavouriteIdeas;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSFavouriteIdeasException(msg.toString());
	}

	/**
	 * Returns the last c l s favourite ideas in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s favourite ideas, or <code>null</code> if a matching c l s favourite ideas could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteIdeas fetchByUserID_Last(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByUserID(userId);

		if (count == 0) {
			return null;
		}

		List<CLSFavouriteIdeas> list = findByUserID(userId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s favourite ideases before and after the current c l s favourite ideas in the ordered set where userId = &#63;.
	 *
	 * @param clsFavouriteIdeasPK the primary key of the current c l s favourite ideas
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s favourite ideas
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException if a c l s favourite ideas with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteIdeas[] findByUserID_PrevAndNext(
		CLSFavouriteIdeasPK clsFavouriteIdeasPK, long userId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSFavouriteIdeasException, SystemException {
		CLSFavouriteIdeas clsFavouriteIdeas = findByPrimaryKey(clsFavouriteIdeasPK);

		Session session = null;

		try {
			session = openSession();

			CLSFavouriteIdeas[] array = new CLSFavouriteIdeasImpl[3];

			array[0] = getByUserID_PrevAndNext(session, clsFavouriteIdeas,
					userId, orderByComparator, true);

			array[1] = clsFavouriteIdeas;

			array[2] = getByUserID_PrevAndNext(session, clsFavouriteIdeas,
					userId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSFavouriteIdeas getByUserID_PrevAndNext(Session session,
		CLSFavouriteIdeas clsFavouriteIdeas, long userId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSFAVOURITEIDEAS_WHERE);

		query.append(_FINDER_COLUMN_USERID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSFavouriteIdeasModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsFavouriteIdeas);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSFavouriteIdeas> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s favourite ideases where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByUserID(long userId) throws SystemException {
		for (CLSFavouriteIdeas clsFavouriteIdeas : findByUserID(userId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(clsFavouriteIdeas);
		}
	}

	/**
	 * Returns the number of c l s favourite ideases where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching c l s favourite ideases
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUserID(long userId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_USERID;

		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLSFAVOURITEIDEAS_WHERE);

			query.append(_FINDER_COLUMN_USERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_USERID_USERID_2 = "clsFavouriteIdeas.id.userId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_IDEAIDANDUSERID = new FinderPath(CLSFavouriteIdeasModelImpl.ENTITY_CACHE_ENABLED,
			CLSFavouriteIdeasModelImpl.FINDER_CACHE_ENABLED,
			CLSFavouriteIdeasImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByIdeaIDAndUserID",
			new String[] { Long.class.getName(), Long.class.getName() },
			CLSFavouriteIdeasModelImpl.IDEAID_COLUMN_BITMASK |
			CLSFavouriteIdeasModelImpl.USERID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_IDEAIDANDUSERID = new FinderPath(CLSFavouriteIdeasModelImpl.ENTITY_CACHE_ENABLED,
			CLSFavouriteIdeasModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByIdeaIDAndUserID",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns the c l s favourite ideas where ideaID = &#63; and userId = &#63; or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException} if it could not be found.
	 *
	 * @param ideaID the idea i d
	 * @param userId the user ID
	 * @return the matching c l s favourite ideas
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException if a matching c l s favourite ideas could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteIdeas findByIdeaIDAndUserID(long ideaID, long userId)
		throws NoSuchCLSFavouriteIdeasException, SystemException {
		CLSFavouriteIdeas clsFavouriteIdeas = fetchByIdeaIDAndUserID(ideaID,
				userId);

		if (clsFavouriteIdeas == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("ideaID=");
			msg.append(ideaID);

			msg.append(", userId=");
			msg.append(userId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchCLSFavouriteIdeasException(msg.toString());
		}

		return clsFavouriteIdeas;
	}

	/**
	 * Returns the c l s favourite ideas where ideaID = &#63; and userId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param ideaID the idea i d
	 * @param userId the user ID
	 * @return the matching c l s favourite ideas, or <code>null</code> if a matching c l s favourite ideas could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteIdeas fetchByIdeaIDAndUserID(long ideaID, long userId)
		throws SystemException {
		return fetchByIdeaIDAndUserID(ideaID, userId, true);
	}

	/**
	 * Returns the c l s favourite ideas where ideaID = &#63; and userId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param ideaID the idea i d
	 * @param userId the user ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching c l s favourite ideas, or <code>null</code> if a matching c l s favourite ideas could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteIdeas fetchByIdeaIDAndUserID(long ideaID, long userId,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { ideaID, userId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_IDEAIDANDUSERID,
					finderArgs, this);
		}

		if (result instanceof CLSFavouriteIdeas) {
			CLSFavouriteIdeas clsFavouriteIdeas = (CLSFavouriteIdeas)result;

			if ((ideaID != clsFavouriteIdeas.getIdeaID()) ||
					(userId != clsFavouriteIdeas.getUserId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_CLSFAVOURITEIDEAS_WHERE);

			query.append(_FINDER_COLUMN_IDEAIDANDUSERID_IDEAID_2);

			query.append(_FINDER_COLUMN_IDEAIDANDUSERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaID);

				qPos.add(userId);

				List<CLSFavouriteIdeas> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDEAIDANDUSERID,
						finderArgs, list);
				}
				else {
					CLSFavouriteIdeas clsFavouriteIdeas = list.get(0);

					result = clsFavouriteIdeas;

					cacheResult(clsFavouriteIdeas);

					if ((clsFavouriteIdeas.getIdeaID() != ideaID) ||
							(clsFavouriteIdeas.getUserId() != userId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDEAIDANDUSERID,
							finderArgs, clsFavouriteIdeas);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_IDEAIDANDUSERID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (CLSFavouriteIdeas)result;
		}
	}

	/**
	 * Removes the c l s favourite ideas where ideaID = &#63; and userId = &#63; from the database.
	 *
	 * @param ideaID the idea i d
	 * @param userId the user ID
	 * @return the c l s favourite ideas that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteIdeas removeByIdeaIDAndUserID(long ideaID, long userId)
		throws NoSuchCLSFavouriteIdeasException, SystemException {
		CLSFavouriteIdeas clsFavouriteIdeas = findByIdeaIDAndUserID(ideaID,
				userId);

		return remove(clsFavouriteIdeas);
	}

	/**
	 * Returns the number of c l s favourite ideases where ideaID = &#63; and userId = &#63;.
	 *
	 * @param ideaID the idea i d
	 * @param userId the user ID
	 * @return the number of matching c l s favourite ideases
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByIdeaIDAndUserID(long ideaID, long userId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_IDEAIDANDUSERID;

		Object[] finderArgs = new Object[] { ideaID, userId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CLSFAVOURITEIDEAS_WHERE);

			query.append(_FINDER_COLUMN_IDEAIDANDUSERID_IDEAID_2);

			query.append(_FINDER_COLUMN_IDEAIDANDUSERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaID);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_IDEAIDANDUSERID_IDEAID_2 = "clsFavouriteIdeas.id.ideaID = ? AND ";
	private static final String _FINDER_COLUMN_IDEAIDANDUSERID_USERID_2 = "clsFavouriteIdeas.id.userId = ?";

	public CLSFavouriteIdeasPersistenceImpl() {
		setModelClass(CLSFavouriteIdeas.class);
	}

	/**
	 * Caches the c l s favourite ideas in the entity cache if it is enabled.
	 *
	 * @param clsFavouriteIdeas the c l s favourite ideas
	 */
	@Override
	public void cacheResult(CLSFavouriteIdeas clsFavouriteIdeas) {
		EntityCacheUtil.putResult(CLSFavouriteIdeasModelImpl.ENTITY_CACHE_ENABLED,
			CLSFavouriteIdeasImpl.class, clsFavouriteIdeas.getPrimaryKey(),
			clsFavouriteIdeas);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDEAIDANDUSERID,
			new Object[] {
				clsFavouriteIdeas.getIdeaID(), clsFavouriteIdeas.getUserId()
			}, clsFavouriteIdeas);

		clsFavouriteIdeas.resetOriginalValues();
	}

	/**
	 * Caches the c l s favourite ideases in the entity cache if it is enabled.
	 *
	 * @param clsFavouriteIdeases the c l s favourite ideases
	 */
	@Override
	public void cacheResult(List<CLSFavouriteIdeas> clsFavouriteIdeases) {
		for (CLSFavouriteIdeas clsFavouriteIdeas : clsFavouriteIdeases) {
			if (EntityCacheUtil.getResult(
						CLSFavouriteIdeasModelImpl.ENTITY_CACHE_ENABLED,
						CLSFavouriteIdeasImpl.class,
						clsFavouriteIdeas.getPrimaryKey()) == null) {
				cacheResult(clsFavouriteIdeas);
			}
			else {
				clsFavouriteIdeas.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all c l s favourite ideases.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CLSFavouriteIdeasImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CLSFavouriteIdeasImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the c l s favourite ideas.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CLSFavouriteIdeas clsFavouriteIdeas) {
		EntityCacheUtil.removeResult(CLSFavouriteIdeasModelImpl.ENTITY_CACHE_ENABLED,
			CLSFavouriteIdeasImpl.class, clsFavouriteIdeas.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(clsFavouriteIdeas);
	}

	@Override
	public void clearCache(List<CLSFavouriteIdeas> clsFavouriteIdeases) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CLSFavouriteIdeas clsFavouriteIdeas : clsFavouriteIdeases) {
			EntityCacheUtil.removeResult(CLSFavouriteIdeasModelImpl.ENTITY_CACHE_ENABLED,
				CLSFavouriteIdeasImpl.class, clsFavouriteIdeas.getPrimaryKey());

			clearUniqueFindersCache(clsFavouriteIdeas);
		}
	}

	protected void cacheUniqueFindersCache(CLSFavouriteIdeas clsFavouriteIdeas) {
		if (clsFavouriteIdeas.isNew()) {
			Object[] args = new Object[] {
					clsFavouriteIdeas.getIdeaID(), clsFavouriteIdeas.getUserId()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_IDEAIDANDUSERID,
				args, Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDEAIDANDUSERID,
				args, clsFavouriteIdeas);
		}
		else {
			CLSFavouriteIdeasModelImpl clsFavouriteIdeasModelImpl = (CLSFavouriteIdeasModelImpl)clsFavouriteIdeas;

			if ((clsFavouriteIdeasModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_IDEAIDANDUSERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsFavouriteIdeas.getIdeaID(),
						clsFavouriteIdeas.getUserId()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_IDEAIDANDUSERID,
					args, Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDEAIDANDUSERID,
					args, clsFavouriteIdeas);
			}
		}
	}

	protected void clearUniqueFindersCache(CLSFavouriteIdeas clsFavouriteIdeas) {
		CLSFavouriteIdeasModelImpl clsFavouriteIdeasModelImpl = (CLSFavouriteIdeasModelImpl)clsFavouriteIdeas;

		Object[] args = new Object[] {
				clsFavouriteIdeas.getIdeaID(), clsFavouriteIdeas.getUserId()
			};

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEAIDANDUSERID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_IDEAIDANDUSERID, args);

		if ((clsFavouriteIdeasModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_IDEAIDANDUSERID.getColumnBitmask()) != 0) {
			args = new Object[] {
					clsFavouriteIdeasModelImpl.getOriginalIdeaID(),
					clsFavouriteIdeasModelImpl.getOriginalUserId()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEAIDANDUSERID,
				args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_IDEAIDANDUSERID,
				args);
		}
	}

	/**
	 * Creates a new c l s favourite ideas with the primary key. Does not add the c l s favourite ideas to the database.
	 *
	 * @param clsFavouriteIdeasPK the primary key for the new c l s favourite ideas
	 * @return the new c l s favourite ideas
	 */
	@Override
	public CLSFavouriteIdeas create(CLSFavouriteIdeasPK clsFavouriteIdeasPK) {
		CLSFavouriteIdeas clsFavouriteIdeas = new CLSFavouriteIdeasImpl();

		clsFavouriteIdeas.setNew(true);
		clsFavouriteIdeas.setPrimaryKey(clsFavouriteIdeasPK);

		return clsFavouriteIdeas;
	}

	/**
	 * Removes the c l s favourite ideas with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param clsFavouriteIdeasPK the primary key of the c l s favourite ideas
	 * @return the c l s favourite ideas that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException if a c l s favourite ideas with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteIdeas remove(CLSFavouriteIdeasPK clsFavouriteIdeasPK)
		throws NoSuchCLSFavouriteIdeasException, SystemException {
		return remove((Serializable)clsFavouriteIdeasPK);
	}

	/**
	 * Removes the c l s favourite ideas with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the c l s favourite ideas
	 * @return the c l s favourite ideas that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException if a c l s favourite ideas with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteIdeas remove(Serializable primaryKey)
		throws NoSuchCLSFavouriteIdeasException, SystemException {
		Session session = null;

		try {
			session = openSession();

			CLSFavouriteIdeas clsFavouriteIdeas = (CLSFavouriteIdeas)session.get(CLSFavouriteIdeasImpl.class,
					primaryKey);

			if (clsFavouriteIdeas == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCLSFavouriteIdeasException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(clsFavouriteIdeas);
		}
		catch (NoSuchCLSFavouriteIdeasException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CLSFavouriteIdeas removeImpl(CLSFavouriteIdeas clsFavouriteIdeas)
		throws SystemException {
		clsFavouriteIdeas = toUnwrappedModel(clsFavouriteIdeas);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(clsFavouriteIdeas)) {
				clsFavouriteIdeas = (CLSFavouriteIdeas)session.get(CLSFavouriteIdeasImpl.class,
						clsFavouriteIdeas.getPrimaryKeyObj());
			}

			if (clsFavouriteIdeas != null) {
				session.delete(clsFavouriteIdeas);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (clsFavouriteIdeas != null) {
			clearCache(clsFavouriteIdeas);
		}

		return clsFavouriteIdeas;
	}

	@Override
	public CLSFavouriteIdeas updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas clsFavouriteIdeas)
		throws SystemException {
		clsFavouriteIdeas = toUnwrappedModel(clsFavouriteIdeas);

		boolean isNew = clsFavouriteIdeas.isNew();

		CLSFavouriteIdeasModelImpl clsFavouriteIdeasModelImpl = (CLSFavouriteIdeasModelImpl)clsFavouriteIdeas;

		Session session = null;

		try {
			session = openSession();

			if (clsFavouriteIdeas.isNew()) {
				session.save(clsFavouriteIdeas);

				clsFavouriteIdeas.setNew(false);
			}
			else {
				session.merge(clsFavouriteIdeas);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !CLSFavouriteIdeasModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((clsFavouriteIdeasModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsFavouriteIdeasModelImpl.getOriginalIdeaID()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEAID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAID,
					args);

				args = new Object[] { clsFavouriteIdeasModelImpl.getIdeaID() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEAID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAID,
					args);
			}

			if ((clsFavouriteIdeasModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsFavouriteIdeasModelImpl.getOriginalUserId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);

				args = new Object[] { clsFavouriteIdeasModelImpl.getUserId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USERID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_USERID,
					args);
			}
		}

		EntityCacheUtil.putResult(CLSFavouriteIdeasModelImpl.ENTITY_CACHE_ENABLED,
			CLSFavouriteIdeasImpl.class, clsFavouriteIdeas.getPrimaryKey(),
			clsFavouriteIdeas);

		clearUniqueFindersCache(clsFavouriteIdeas);
		cacheUniqueFindersCache(clsFavouriteIdeas);

		return clsFavouriteIdeas;
	}

	protected CLSFavouriteIdeas toUnwrappedModel(
		CLSFavouriteIdeas clsFavouriteIdeas) {
		if (clsFavouriteIdeas instanceof CLSFavouriteIdeasImpl) {
			return clsFavouriteIdeas;
		}

		CLSFavouriteIdeasImpl clsFavouriteIdeasImpl = new CLSFavouriteIdeasImpl();

		clsFavouriteIdeasImpl.setNew(clsFavouriteIdeas.isNew());
		clsFavouriteIdeasImpl.setPrimaryKey(clsFavouriteIdeas.getPrimaryKey());

		clsFavouriteIdeasImpl.setIdeaID(clsFavouriteIdeas.getIdeaID());
		clsFavouriteIdeasImpl.setUserId(clsFavouriteIdeas.getUserId());

		return clsFavouriteIdeasImpl;
	}

	/**
	 * Returns the c l s favourite ideas with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the c l s favourite ideas
	 * @return the c l s favourite ideas
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException if a c l s favourite ideas with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteIdeas findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCLSFavouriteIdeasException, SystemException {
		CLSFavouriteIdeas clsFavouriteIdeas = fetchByPrimaryKey(primaryKey);

		if (clsFavouriteIdeas == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCLSFavouriteIdeasException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return clsFavouriteIdeas;
	}

	/**
	 * Returns the c l s favourite ideas with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException} if it could not be found.
	 *
	 * @param clsFavouriteIdeasPK the primary key of the c l s favourite ideas
	 * @return the c l s favourite ideas
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFavouriteIdeasException if a c l s favourite ideas with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteIdeas findByPrimaryKey(
		CLSFavouriteIdeasPK clsFavouriteIdeasPK)
		throws NoSuchCLSFavouriteIdeasException, SystemException {
		return findByPrimaryKey((Serializable)clsFavouriteIdeasPK);
	}

	/**
	 * Returns the c l s favourite ideas with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the c l s favourite ideas
	 * @return the c l s favourite ideas, or <code>null</code> if a c l s favourite ideas with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteIdeas fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		CLSFavouriteIdeas clsFavouriteIdeas = (CLSFavouriteIdeas)EntityCacheUtil.getResult(CLSFavouriteIdeasModelImpl.ENTITY_CACHE_ENABLED,
				CLSFavouriteIdeasImpl.class, primaryKey);

		if (clsFavouriteIdeas == _nullCLSFavouriteIdeas) {
			return null;
		}

		if (clsFavouriteIdeas == null) {
			Session session = null;

			try {
				session = openSession();

				clsFavouriteIdeas = (CLSFavouriteIdeas)session.get(CLSFavouriteIdeasImpl.class,
						primaryKey);

				if (clsFavouriteIdeas != null) {
					cacheResult(clsFavouriteIdeas);
				}
				else {
					EntityCacheUtil.putResult(CLSFavouriteIdeasModelImpl.ENTITY_CACHE_ENABLED,
						CLSFavouriteIdeasImpl.class, primaryKey,
						_nullCLSFavouriteIdeas);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(CLSFavouriteIdeasModelImpl.ENTITY_CACHE_ENABLED,
					CLSFavouriteIdeasImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return clsFavouriteIdeas;
	}

	/**
	 * Returns the c l s favourite ideas with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param clsFavouriteIdeasPK the primary key of the c l s favourite ideas
	 * @return the c l s favourite ideas, or <code>null</code> if a c l s favourite ideas with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFavouriteIdeas fetchByPrimaryKey(
		CLSFavouriteIdeasPK clsFavouriteIdeasPK) throws SystemException {
		return fetchByPrimaryKey((Serializable)clsFavouriteIdeasPK);
	}

	/**
	 * Returns all the c l s favourite ideases.
	 *
	 * @return the c l s favourite ideases
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSFavouriteIdeas> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s favourite ideases.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteIdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of c l s favourite ideases
	 * @param end the upper bound of the range of c l s favourite ideases (not inclusive)
	 * @return the range of c l s favourite ideases
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSFavouriteIdeas> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s favourite ideases.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteIdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of c l s favourite ideases
	 * @param end the upper bound of the range of c l s favourite ideases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of c l s favourite ideases
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSFavouriteIdeas> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CLSFavouriteIdeas> list = (List<CLSFavouriteIdeas>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CLSFAVOURITEIDEAS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CLSFAVOURITEIDEAS;

				if (pagination) {
					sql = sql.concat(CLSFavouriteIdeasModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<CLSFavouriteIdeas>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSFavouriteIdeas>(list);
				}
				else {
					list = (List<CLSFavouriteIdeas>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the c l s favourite ideases from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (CLSFavouriteIdeas clsFavouriteIdeas : findAll()) {
			remove(clsFavouriteIdeas);
		}
	}

	/**
	 * Returns the number of c l s favourite ideases.
	 *
	 * @return the number of c l s favourite ideases
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CLSFAVOURITEIDEAS);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the c l s favourite ideas persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<CLSFavouriteIdeas>> listenersList = new ArrayList<ModelListener<CLSFavouriteIdeas>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<CLSFavouriteIdeas>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CLSFavouriteIdeasImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_CLSFAVOURITEIDEAS = "SELECT clsFavouriteIdeas FROM CLSFavouriteIdeas clsFavouriteIdeas";
	private static final String _SQL_SELECT_CLSFAVOURITEIDEAS_WHERE = "SELECT clsFavouriteIdeas FROM CLSFavouriteIdeas clsFavouriteIdeas WHERE ";
	private static final String _SQL_COUNT_CLSFAVOURITEIDEAS = "SELECT COUNT(clsFavouriteIdeas) FROM CLSFavouriteIdeas clsFavouriteIdeas";
	private static final String _SQL_COUNT_CLSFAVOURITEIDEAS_WHERE = "SELECT COUNT(clsFavouriteIdeas) FROM CLSFavouriteIdeas clsFavouriteIdeas WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "clsFavouriteIdeas.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CLSFavouriteIdeas exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No CLSFavouriteIdeas exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CLSFavouriteIdeasPersistenceImpl.class);
	private static CLSFavouriteIdeas _nullCLSFavouriteIdeas = new CLSFavouriteIdeasImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<CLSFavouriteIdeas> toCacheModel() {
				return _nullCLSFavouriteIdeasCacheModel;
			}
		};

	private static CacheModel<CLSFavouriteIdeas> _nullCLSFavouriteIdeasCacheModel =
		new CacheModel<CLSFavouriteIdeas>() {
			@Override
			public CLSFavouriteIdeas toEntityModel() {
				return _nullCLSFavouriteIdeas;
			}
		};
}