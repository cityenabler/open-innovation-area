/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model.impl;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.json.JSON;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifactsModel;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifactsSoap;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSArtifactsPK;

import java.io.Serializable;

import java.sql.Types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The base model implementation for the CLSArtifacts service. Represents a row in the &quot;CSL_CLSArtifacts&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifactsModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link CLSArtifactsImpl}.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSArtifactsImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts
 * @see it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifactsModel
 * @generated
 */
@JSON(strict = true)
public class CLSArtifactsModelImpl extends BaseModelImpl<CLSArtifacts>
	implements CLSArtifactsModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a c l s artifacts model instance should use the {@link it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts} interface instead.
	 */
	public static final String TABLE_NAME = "CSL_CLSArtifacts";
	public static final Object[][] TABLE_COLUMNS = {
			{ "ideaId", Types.BIGINT },
			{ "artifactId", Types.BIGINT }
		};
	public static final String TABLE_SQL_CREATE = "create table CSL_CLSArtifacts (ideaId LONG not null,artifactId LONG not null,primary key (ideaId, artifactId))";
	public static final String TABLE_SQL_DROP = "drop table CSL_CLSArtifacts";
	public static final String ORDER_BY_JPQL = " ORDER BY clsArtifacts.id.ideaId ASC, clsArtifacts.id.artifactId ASC";
	public static final String ORDER_BY_SQL = " ORDER BY CSL_CLSArtifacts.ideaId ASC, CSL_CLSArtifacts.artifactId ASC";
	public static final String DATA_SOURCE = "liferayDataSource";
	public static final String SESSION_FACTORY = "liferaySessionFactory";
	public static final String TX_MANAGER = "liferayTransactionManager";
	public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.entity.cache.enabled.it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts"),
			true);
	public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.finder.cache.enabled.it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts"),
			true);
	public static final boolean COLUMN_BITMASK_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.column.bitmask.enabled.it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts"),
			true);
	public static long ARTIFACTID_COLUMN_BITMASK = 1L;
	public static long IDEAID_COLUMN_BITMASK = 2L;

	/**
	 * Converts the soap model instance into a normal model instance.
	 *
	 * @param soapModel the soap model instance to convert
	 * @return the normal model instance
	 */
	public static CLSArtifacts toModel(CLSArtifactsSoap soapModel) {
		if (soapModel == null) {
			return null;
		}

		CLSArtifacts model = new CLSArtifactsImpl();

		model.setIdeaId(soapModel.getIdeaId());
		model.setArtifactId(soapModel.getArtifactId());

		return model;
	}

	/**
	 * Converts the soap model instances into normal model instances.
	 *
	 * @param soapModels the soap model instances to convert
	 * @return the normal model instances
	 */
	public static List<CLSArtifacts> toModels(CLSArtifactsSoap[] soapModels) {
		if (soapModels == null) {
			return null;
		}

		List<CLSArtifacts> models = new ArrayList<CLSArtifacts>(soapModels.length);

		for (CLSArtifactsSoap soapModel : soapModels) {
			models.add(toModel(soapModel));
		}

		return models;
	}

	public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
				"lock.expiration.time.it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts"));

	public CLSArtifactsModelImpl() {
	}

	@Override
	public CLSArtifactsPK getPrimaryKey() {
		return new CLSArtifactsPK(_ideaId, _artifactId);
	}

	@Override
	public void setPrimaryKey(CLSArtifactsPK primaryKey) {
		setIdeaId(primaryKey.ideaId);
		setArtifactId(primaryKey.artifactId);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return new CLSArtifactsPK(_ideaId, _artifactId);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey((CLSArtifactsPK)primaryKeyObj);
	}

	@Override
	public Class<?> getModelClass() {
		return CLSArtifacts.class;
	}

	@Override
	public String getModelClassName() {
		return CLSArtifacts.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ideaId", getIdeaId());
		attributes.put("artifactId", getArtifactId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ideaId = (Long)attributes.get("ideaId");

		if (ideaId != null) {
			setIdeaId(ideaId);
		}

		Long artifactId = (Long)attributes.get("artifactId");

		if (artifactId != null) {
			setArtifactId(artifactId);
		}
	}

	@JSON
	@Override
	public long getIdeaId() {
		return _ideaId;
	}

	@Override
	public void setIdeaId(long ideaId) {
		_columnBitmask |= IDEAID_COLUMN_BITMASK;

		if (!_setOriginalIdeaId) {
			_setOriginalIdeaId = true;

			_originalIdeaId = _ideaId;
		}

		_ideaId = ideaId;
	}

	public long getOriginalIdeaId() {
		return _originalIdeaId;
	}

	@JSON
	@Override
	public long getArtifactId() {
		return _artifactId;
	}

	@Override
	public void setArtifactId(long artifactId) {
		_columnBitmask |= ARTIFACTID_COLUMN_BITMASK;

		if (!_setOriginalArtifactId) {
			_setOriginalArtifactId = true;

			_originalArtifactId = _artifactId;
		}

		_artifactId = artifactId;
	}

	public long getOriginalArtifactId() {
		return _originalArtifactId;
	}

	public long getColumnBitmask() {
		return _columnBitmask;
	}

	@Override
	public CLSArtifacts toEscapedModel() {
		if (_escapedModel == null) {
			_escapedModel = (CLSArtifacts)ProxyUtil.newProxyInstance(_classLoader,
					_escapedModelInterfaces, new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		CLSArtifactsImpl clsArtifactsImpl = new CLSArtifactsImpl();

		clsArtifactsImpl.setIdeaId(getIdeaId());
		clsArtifactsImpl.setArtifactId(getArtifactId());

		clsArtifactsImpl.resetOriginalValues();

		return clsArtifactsImpl;
	}

	@Override
	public int compareTo(CLSArtifacts clsArtifacts) {
		CLSArtifactsPK primaryKey = clsArtifacts.getPrimaryKey();

		return getPrimaryKey().compareTo(primaryKey);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSArtifacts)) {
			return false;
		}

		CLSArtifacts clsArtifacts = (CLSArtifacts)obj;

		CLSArtifactsPK primaryKey = clsArtifacts.getPrimaryKey();

		if (getPrimaryKey().equals(primaryKey)) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return getPrimaryKey().hashCode();
	}

	@Override
	public void resetOriginalValues() {
		CLSArtifactsModelImpl clsArtifactsModelImpl = this;

		clsArtifactsModelImpl._originalIdeaId = clsArtifactsModelImpl._ideaId;

		clsArtifactsModelImpl._setOriginalIdeaId = false;

		clsArtifactsModelImpl._originalArtifactId = clsArtifactsModelImpl._artifactId;

		clsArtifactsModelImpl._setOriginalArtifactId = false;

		clsArtifactsModelImpl._columnBitmask = 0;
	}

	@Override
	public CacheModel<CLSArtifacts> toCacheModel() {
		CLSArtifactsCacheModel clsArtifactsCacheModel = new CLSArtifactsCacheModel();

		clsArtifactsCacheModel.ideaId = getIdeaId();

		clsArtifactsCacheModel.artifactId = getArtifactId();

		return clsArtifactsCacheModel;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{ideaId=");
		sb.append(getIdeaId());
		sb.append(", artifactId=");
		sb.append(getArtifactId());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(10);

		sb.append("<model><model-name>");
		sb.append(
			"it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>ideaId</column-name><column-value><![CDATA[");
		sb.append(getIdeaId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>artifactId</column-name><column-value><![CDATA[");
		sb.append(getArtifactId());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private static ClassLoader _classLoader = CLSArtifacts.class.getClassLoader();
	private static Class<?>[] _escapedModelInterfaces = new Class[] {
			CLSArtifacts.class
		};
	private long _ideaId;
	private long _originalIdeaId;
	private boolean _setOriginalIdeaId;
	private long _artifactId;
	private long _originalArtifactId;
	private boolean _setOriginalArtifactId;
	private long _columnBitmask;
	private CLSArtifacts _escapedModel;
}