/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing VirtuosityPoints in entity cache.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see VirtuosityPoints
 * @generated
 */
public class VirtuosityPointsCacheModel implements CacheModel<VirtuosityPoints>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{challengeId=");
		sb.append(challengeId);
		sb.append(", position=");
		sb.append(position);
		sb.append(", points=");
		sb.append(points);
		sb.append(", dateAdded=");
		sb.append(dateAdded);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public VirtuosityPoints toEntityModel() {
		VirtuosityPointsImpl virtuosityPointsImpl = new VirtuosityPointsImpl();

		virtuosityPointsImpl.setChallengeId(challengeId);
		virtuosityPointsImpl.setPosition(position);
		virtuosityPointsImpl.setPoints(points);

		if (dateAdded == Long.MIN_VALUE) {
			virtuosityPointsImpl.setDateAdded(null);
		}
		else {
			virtuosityPointsImpl.setDateAdded(new Date(dateAdded));
		}

		virtuosityPointsImpl.resetOriginalValues();

		return virtuosityPointsImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		challengeId = objectInput.readLong();
		position = objectInput.readLong();
		points = objectInput.readLong();
		dateAdded = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(challengeId);
		objectOutput.writeLong(position);
		objectOutput.writeLong(points);
		objectOutput.writeLong(dateAdded);
	}

	public long challengeId;
	public long position;
	public long points;
	public long dateAdded;
}