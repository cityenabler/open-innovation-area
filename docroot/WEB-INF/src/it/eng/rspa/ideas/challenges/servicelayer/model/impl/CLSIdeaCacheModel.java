/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing CLSIdea in entity cache.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSIdea
 * @generated
 */
public class CLSIdeaCacheModel implements CacheModel<CLSIdea>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(57);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", ideaID=");
		sb.append(ideaID);
		sb.append(", ideaTitle=");
		sb.append(ideaTitle);
		sb.append(", ideaDescription=");
		sb.append(ideaDescription);
		sb.append(", dateAdded=");
		sb.append(dateAdded);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", municipalityId=");
		sb.append(municipalityId);
		sb.append(", municipalityOrganizationId=");
		sb.append(municipalityOrganizationId);
		sb.append(", challengeId=");
		sb.append(challengeId);
		sb.append(", takenUp=");
		sb.append(takenUp);
		sb.append(", needId=");
		sb.append(needId);
		sb.append(", dmFolderName=");
		sb.append(dmFolderName);
		sb.append(", idFolder=");
		sb.append(idFolder);
		sb.append(", representativeImgUrl=");
		sb.append(representativeImgUrl);
		sb.append(", isNeed=");
		sb.append(isNeed);
		sb.append(", status=");
		sb.append(status);
		sb.append(", statusByUserId=");
		sb.append(statusByUserId);
		sb.append(", statusByUserName=");
		sb.append(statusByUserName);
		sb.append(", statusDate=");
		sb.append(statusDate);
		sb.append(", chatGroup=");
		sb.append(chatGroup);
		sb.append(", ideaHashTag=");
		sb.append(ideaHashTag);
		sb.append(", ideaStatus=");
		sb.append(ideaStatus);
		sb.append(", language=");
		sb.append(language);
		sb.append(", finalMotivation=");
		sb.append(finalMotivation);
		sb.append(", evaluationPassed=");
		sb.append(evaluationPassed);
		sb.append(", cityName=");
		sb.append(cityName);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public CLSIdea toEntityModel() {
		CLSIdeaImpl clsIdeaImpl = new CLSIdeaImpl();

		if (uuid == null) {
			clsIdeaImpl.setUuid(StringPool.BLANK);
		}
		else {
			clsIdeaImpl.setUuid(uuid);
		}

		clsIdeaImpl.setIdeaID(ideaID);

		if (ideaTitle == null) {
			clsIdeaImpl.setIdeaTitle(StringPool.BLANK);
		}
		else {
			clsIdeaImpl.setIdeaTitle(ideaTitle);
		}

		if (ideaDescription == null) {
			clsIdeaImpl.setIdeaDescription(StringPool.BLANK);
		}
		else {
			clsIdeaImpl.setIdeaDescription(ideaDescription);
		}

		if (dateAdded == Long.MIN_VALUE) {
			clsIdeaImpl.setDateAdded(null);
		}
		else {
			clsIdeaImpl.setDateAdded(new Date(dateAdded));
		}

		clsIdeaImpl.setCompanyId(companyId);
		clsIdeaImpl.setGroupId(groupId);
		clsIdeaImpl.setUserId(userId);
		clsIdeaImpl.setMunicipalityId(municipalityId);
		clsIdeaImpl.setMunicipalityOrganizationId(municipalityOrganizationId);
		clsIdeaImpl.setChallengeId(challengeId);
		clsIdeaImpl.setTakenUp(takenUp);
		clsIdeaImpl.setNeedId(needId);

		if (dmFolderName == null) {
			clsIdeaImpl.setDmFolderName(StringPool.BLANK);
		}
		else {
			clsIdeaImpl.setDmFolderName(dmFolderName);
		}

		clsIdeaImpl.setIdFolder(idFolder);

		if (representativeImgUrl == null) {
			clsIdeaImpl.setRepresentativeImgUrl(StringPool.BLANK);
		}
		else {
			clsIdeaImpl.setRepresentativeImgUrl(representativeImgUrl);
		}

		clsIdeaImpl.setIsNeed(isNeed);
		clsIdeaImpl.setStatus(status);
		clsIdeaImpl.setStatusByUserId(statusByUserId);

		if (statusByUserName == null) {
			clsIdeaImpl.setStatusByUserName(StringPool.BLANK);
		}
		else {
			clsIdeaImpl.setStatusByUserName(statusByUserName);
		}

		if (statusDate == Long.MIN_VALUE) {
			clsIdeaImpl.setStatusDate(null);
		}
		else {
			clsIdeaImpl.setStatusDate(new Date(statusDate));
		}

		clsIdeaImpl.setChatGroup(chatGroup);

		if (ideaHashTag == null) {
			clsIdeaImpl.setIdeaHashTag(StringPool.BLANK);
		}
		else {
			clsIdeaImpl.setIdeaHashTag(ideaHashTag);
		}

		if (ideaStatus == null) {
			clsIdeaImpl.setIdeaStatus(StringPool.BLANK);
		}
		else {
			clsIdeaImpl.setIdeaStatus(ideaStatus);
		}

		if (language == null) {
			clsIdeaImpl.setLanguage(StringPool.BLANK);
		}
		else {
			clsIdeaImpl.setLanguage(language);
		}

		if (finalMotivation == null) {
			clsIdeaImpl.setFinalMotivation(StringPool.BLANK);
		}
		else {
			clsIdeaImpl.setFinalMotivation(finalMotivation);
		}

		clsIdeaImpl.setEvaluationPassed(evaluationPassed);

		if (cityName == null) {
			clsIdeaImpl.setCityName(StringPool.BLANK);
		}
		else {
			clsIdeaImpl.setCityName(cityName);
		}

		clsIdeaImpl.resetOriginalValues();

		return clsIdeaImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();
		ideaID = objectInput.readLong();
		ideaTitle = objectInput.readUTF();
		ideaDescription = objectInput.readUTF();
		dateAdded = objectInput.readLong();
		companyId = objectInput.readLong();
		groupId = objectInput.readLong();
		userId = objectInput.readLong();
		municipalityId = objectInput.readLong();
		municipalityOrganizationId = objectInput.readLong();
		challengeId = objectInput.readLong();
		takenUp = objectInput.readBoolean();
		needId = objectInput.readLong();
		dmFolderName = objectInput.readUTF();
		idFolder = objectInput.readLong();
		representativeImgUrl = objectInput.readUTF();
		isNeed = objectInput.readBoolean();
		status = objectInput.readInt();
		statusByUserId = objectInput.readLong();
		statusByUserName = objectInput.readUTF();
		statusDate = objectInput.readLong();
		chatGroup = objectInput.readLong();
		ideaHashTag = objectInput.readUTF();
		ideaStatus = objectInput.readUTF();
		language = objectInput.readUTF();
		finalMotivation = objectInput.readUTF();
		evaluationPassed = objectInput.readBoolean();
		cityName = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(ideaID);

		if (ideaTitle == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ideaTitle);
		}

		if (ideaDescription == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ideaDescription);
		}

		objectOutput.writeLong(dateAdded);
		objectOutput.writeLong(companyId);
		objectOutput.writeLong(groupId);
		objectOutput.writeLong(userId);
		objectOutput.writeLong(municipalityId);
		objectOutput.writeLong(municipalityOrganizationId);
		objectOutput.writeLong(challengeId);
		objectOutput.writeBoolean(takenUp);
		objectOutput.writeLong(needId);

		if (dmFolderName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(dmFolderName);
		}

		objectOutput.writeLong(idFolder);

		if (representativeImgUrl == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(representativeImgUrl);
		}

		objectOutput.writeBoolean(isNeed);
		objectOutput.writeInt(status);
		objectOutput.writeLong(statusByUserId);

		if (statusByUserName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(statusByUserName);
		}

		objectOutput.writeLong(statusDate);
		objectOutput.writeLong(chatGroup);

		if (ideaHashTag == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ideaHashTag);
		}

		if (ideaStatus == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ideaStatus);
		}

		if (language == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(language);
		}

		if (finalMotivation == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(finalMotivation);
		}

		objectOutput.writeBoolean(evaluationPassed);

		if (cityName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(cityName);
		}
	}

	public String uuid;
	public long ideaID;
	public String ideaTitle;
	public String ideaDescription;
	public long dateAdded;
	public long companyId;
	public long groupId;
	public long userId;
	public long municipalityId;
	public long municipalityOrganizationId;
	public long challengeId;
	public boolean takenUp;
	public long needId;
	public String dmFolderName;
	public long idFolder;
	public String representativeImgUrl;
	public boolean isNeed;
	public int status;
	public long statusByUserId;
	public String statusByUserName;
	public long statusDate;
	public long chatGroup;
	public String ideaHashTag;
	public String ideaStatus;
	public String language;
	public String finalMotivation;
	public boolean evaluationPassed;
	public String cityName;
}