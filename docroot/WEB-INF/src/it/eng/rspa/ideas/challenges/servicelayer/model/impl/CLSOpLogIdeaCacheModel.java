/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSOpLogIdea;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing CLSOpLogIdea in entity cache.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSOpLogIdea
 * @generated
 */
public class CLSOpLogIdeaCacheModel implements CacheModel<CLSOpLogIdea>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{opLogId=");
		sb.append(opLogId);
		sb.append(", ideaId=");
		sb.append(ideaId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", date=");
		sb.append(date);
		sb.append(", extraData=");
		sb.append(extraData);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public CLSOpLogIdea toEntityModel() {
		CLSOpLogIdeaImpl clsOpLogIdeaImpl = new CLSOpLogIdeaImpl();

		clsOpLogIdeaImpl.setOpLogId(opLogId);
		clsOpLogIdeaImpl.setIdeaId(ideaId);
		clsOpLogIdeaImpl.setUserId(userId);

		if (date == Long.MIN_VALUE) {
			clsOpLogIdeaImpl.setDate(null);
		}
		else {
			clsOpLogIdeaImpl.setDate(new Date(date));
		}

		if (extraData == null) {
			clsOpLogIdeaImpl.setExtraData(StringPool.BLANK);
		}
		else {
			clsOpLogIdeaImpl.setExtraData(extraData);
		}

		clsOpLogIdeaImpl.resetOriginalValues();

		return clsOpLogIdeaImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		opLogId = objectInput.readLong();
		ideaId = objectInput.readLong();
		userId = objectInput.readLong();
		date = objectInput.readLong();
		extraData = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(opLogId);
		objectOutput.writeLong(ideaId);
		objectOutput.writeLong(userId);
		objectOutput.writeLong(date);

		if (extraData == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(extraData);
		}
	}

	public long opLogId;
	public long ideaId;
	public long userId;
	public long date;
	public String extraData;
}