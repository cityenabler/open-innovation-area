/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException;
import it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaImpl;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the evaluation criteria service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see EvaluationCriteriaPersistence
 * @see EvaluationCriteriaUtil
 * @generated
 */
public class EvaluationCriteriaPersistenceImpl extends BasePersistenceImpl<EvaluationCriteria>
	implements EvaluationCriteriaPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link EvaluationCriteriaUtil} to access the evaluation criteria persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = EvaluationCriteriaImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
			EvaluationCriteriaModelImpl.FINDER_CACHE_ENABLED,
			EvaluationCriteriaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
			EvaluationCriteriaModelImpl.FINDER_CACHE_ENABLED,
			EvaluationCriteriaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
			EvaluationCriteriaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CRITERIABYCHALLENGE =
		new FinderPath(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
			EvaluationCriteriaModelImpl.FINDER_CACHE_ENABLED,
			EvaluationCriteriaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findBycriteriaByChallenge",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CRITERIABYCHALLENGE =
		new FinderPath(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
			EvaluationCriteriaModelImpl.FINDER_CACHE_ENABLED,
			EvaluationCriteriaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findBycriteriaByChallenge", new String[] { Long.class.getName() },
			EvaluationCriteriaModelImpl.CHALLENGEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CRITERIABYCHALLENGE = new FinderPath(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
			EvaluationCriteriaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countBycriteriaByChallenge", new String[] { Long.class.getName() });

	/**
	 * Returns all the evaluation criterias where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @return the matching evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EvaluationCriteria> findBycriteriaByChallenge(long challengeId)
		throws SystemException {
		return findBycriteriaByChallenge(challengeId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the evaluation criterias where challengeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param challengeId the challenge ID
	 * @param start the lower bound of the range of evaluation criterias
	 * @param end the upper bound of the range of evaluation criterias (not inclusive)
	 * @return the range of matching evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EvaluationCriteria> findBycriteriaByChallenge(
		long challengeId, int start, int end) throws SystemException {
		return findBycriteriaByChallenge(challengeId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the evaluation criterias where challengeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param challengeId the challenge ID
	 * @param start the lower bound of the range of evaluation criterias
	 * @param end the upper bound of the range of evaluation criterias (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EvaluationCriteria> findBycriteriaByChallenge(
		long challengeId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CRITERIABYCHALLENGE;
			finderArgs = new Object[] { challengeId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CRITERIABYCHALLENGE;
			finderArgs = new Object[] { challengeId, start, end, orderByComparator };
		}

		List<EvaluationCriteria> list = (List<EvaluationCriteria>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (EvaluationCriteria evaluationCriteria : list) {
				if ((challengeId != evaluationCriteria.getChallengeId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_EVALUATIONCRITERIA_WHERE);

			query.append(_FINDER_COLUMN_CRITERIABYCHALLENGE_CHALLENGEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(EvaluationCriteriaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(challengeId);

				if (!pagination) {
					list = (List<EvaluationCriteria>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<EvaluationCriteria>(list);
				}
				else {
					list = (List<EvaluationCriteria>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first evaluation criteria in the ordered set where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching evaluation criteria
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria findBycriteriaByChallenge_First(
		long challengeId, OrderByComparator orderByComparator)
		throws NoSuchEvaluationCriteriaException, SystemException {
		EvaluationCriteria evaluationCriteria = fetchBycriteriaByChallenge_First(challengeId,
				orderByComparator);

		if (evaluationCriteria != null) {
			return evaluationCriteria;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("challengeId=");
		msg.append(challengeId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEvaluationCriteriaException(msg.toString());
	}

	/**
	 * Returns the first evaluation criteria in the ordered set where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria fetchBycriteriaByChallenge_First(
		long challengeId, OrderByComparator orderByComparator)
		throws SystemException {
		List<EvaluationCriteria> list = findBycriteriaByChallenge(challengeId,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last evaluation criteria in the ordered set where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching evaluation criteria
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria findBycriteriaByChallenge_Last(long challengeId,
		OrderByComparator orderByComparator)
		throws NoSuchEvaluationCriteriaException, SystemException {
		EvaluationCriteria evaluationCriteria = fetchBycriteriaByChallenge_Last(challengeId,
				orderByComparator);

		if (evaluationCriteria != null) {
			return evaluationCriteria;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("challengeId=");
		msg.append(challengeId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEvaluationCriteriaException(msg.toString());
	}

	/**
	 * Returns the last evaluation criteria in the ordered set where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria fetchBycriteriaByChallenge_Last(
		long challengeId, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countBycriteriaByChallenge(challengeId);

		if (count == 0) {
			return null;
		}

		List<EvaluationCriteria> list = findBycriteriaByChallenge(challengeId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the evaluation criterias before and after the current evaluation criteria in the ordered set where challengeId = &#63;.
	 *
	 * @param evaluationCriteriaPK the primary key of the current evaluation criteria
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next evaluation criteria
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a evaluation criteria with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria[] findBycriteriaByChallenge_PrevAndNext(
		EvaluationCriteriaPK evaluationCriteriaPK, long challengeId,
		OrderByComparator orderByComparator)
		throws NoSuchEvaluationCriteriaException, SystemException {
		EvaluationCriteria evaluationCriteria = findByPrimaryKey(evaluationCriteriaPK);

		Session session = null;

		try {
			session = openSession();

			EvaluationCriteria[] array = new EvaluationCriteriaImpl[3];

			array[0] = getBycriteriaByChallenge_PrevAndNext(session,
					evaluationCriteria, challengeId, orderByComparator, true);

			array[1] = evaluationCriteria;

			array[2] = getBycriteriaByChallenge_PrevAndNext(session,
					evaluationCriteria, challengeId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected EvaluationCriteria getBycriteriaByChallenge_PrevAndNext(
		Session session, EvaluationCriteria evaluationCriteria,
		long challengeId, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_EVALUATIONCRITERIA_WHERE);

		query.append(_FINDER_COLUMN_CRITERIABYCHALLENGE_CHALLENGEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(EvaluationCriteriaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(challengeId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(evaluationCriteria);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<EvaluationCriteria> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the evaluation criterias where challengeId = &#63; from the database.
	 *
	 * @param challengeId the challenge ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBycriteriaByChallenge(long challengeId)
		throws SystemException {
		for (EvaluationCriteria evaluationCriteria : findBycriteriaByChallenge(
				challengeId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(evaluationCriteria);
		}
	}

	/**
	 * Returns the number of evaluation criterias where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @return the number of matching evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBycriteriaByChallenge(long challengeId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CRITERIABYCHALLENGE;

		Object[] finderArgs = new Object[] { challengeId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_EVALUATIONCRITERIA_WHERE);

			query.append(_FINDER_COLUMN_CRITERIABYCHALLENGE_CHALLENGEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(challengeId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CRITERIABYCHALLENGE_CHALLENGEID_2 =
		"evaluationCriteria.id.challengeId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CRITERIABYCHALLENGEANDENABLED =
		new FinderPath(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
			EvaluationCriteriaModelImpl.FINDER_CACHE_ENABLED,
			EvaluationCriteriaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findBycriteriaByChallengeAndEnabled",
			new String[] {
				Long.class.getName(), Boolean.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CRITERIABYCHALLENGEANDENABLED =
		new FinderPath(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
			EvaluationCriteriaModelImpl.FINDER_CACHE_ENABLED,
			EvaluationCriteriaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findBycriteriaByChallengeAndEnabled",
			new String[] { Long.class.getName(), Boolean.class.getName() },
			EvaluationCriteriaModelImpl.CHALLENGEID_COLUMN_BITMASK |
			EvaluationCriteriaModelImpl.ENABLED_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CRITERIABYCHALLENGEANDENABLED =
		new FinderPath(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
			EvaluationCriteriaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countBycriteriaByChallengeAndEnabled",
			new String[] { Long.class.getName(), Boolean.class.getName() });

	/**
	 * Returns all the evaluation criterias where challengeId = &#63; and enabled = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param enabled the enabled
	 * @return the matching evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EvaluationCriteria> findBycriteriaByChallengeAndEnabled(
		long challengeId, boolean enabled) throws SystemException {
		return findBycriteriaByChallengeAndEnabled(challengeId, enabled,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the evaluation criterias where challengeId = &#63; and enabled = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param challengeId the challenge ID
	 * @param enabled the enabled
	 * @param start the lower bound of the range of evaluation criterias
	 * @param end the upper bound of the range of evaluation criterias (not inclusive)
	 * @return the range of matching evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EvaluationCriteria> findBycriteriaByChallengeAndEnabled(
		long challengeId, boolean enabled, int start, int end)
		throws SystemException {
		return findBycriteriaByChallengeAndEnabled(challengeId, enabled, start,
			end, null);
	}

	/**
	 * Returns an ordered range of all the evaluation criterias where challengeId = &#63; and enabled = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param challengeId the challenge ID
	 * @param enabled the enabled
	 * @param start the lower bound of the range of evaluation criterias
	 * @param end the upper bound of the range of evaluation criterias (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EvaluationCriteria> findBycriteriaByChallengeAndEnabled(
		long challengeId, boolean enabled, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CRITERIABYCHALLENGEANDENABLED;
			finderArgs = new Object[] { challengeId, enabled };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CRITERIABYCHALLENGEANDENABLED;
			finderArgs = new Object[] {
					challengeId, enabled,
					
					start, end, orderByComparator
				};
		}

		List<EvaluationCriteria> list = (List<EvaluationCriteria>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (EvaluationCriteria evaluationCriteria : list) {
				if ((challengeId != evaluationCriteria.getChallengeId()) ||
						(enabled != evaluationCriteria.getEnabled())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_EVALUATIONCRITERIA_WHERE);

			query.append(_FINDER_COLUMN_CRITERIABYCHALLENGEANDENABLED_CHALLENGEID_2);

			query.append(_FINDER_COLUMN_CRITERIABYCHALLENGEANDENABLED_ENABLED_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(EvaluationCriteriaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(challengeId);

				qPos.add(enabled);

				if (!pagination) {
					list = (List<EvaluationCriteria>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<EvaluationCriteria>(list);
				}
				else {
					list = (List<EvaluationCriteria>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first evaluation criteria in the ordered set where challengeId = &#63; and enabled = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param enabled the enabled
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching evaluation criteria
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria findBycriteriaByChallengeAndEnabled_First(
		long challengeId, boolean enabled, OrderByComparator orderByComparator)
		throws NoSuchEvaluationCriteriaException, SystemException {
		EvaluationCriteria evaluationCriteria = fetchBycriteriaByChallengeAndEnabled_First(challengeId,
				enabled, orderByComparator);

		if (evaluationCriteria != null) {
			return evaluationCriteria;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("challengeId=");
		msg.append(challengeId);

		msg.append(", enabled=");
		msg.append(enabled);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEvaluationCriteriaException(msg.toString());
	}

	/**
	 * Returns the first evaluation criteria in the ordered set where challengeId = &#63; and enabled = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param enabled the enabled
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria fetchBycriteriaByChallengeAndEnabled_First(
		long challengeId, boolean enabled, OrderByComparator orderByComparator)
		throws SystemException {
		List<EvaluationCriteria> list = findBycriteriaByChallengeAndEnabled(challengeId,
				enabled, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last evaluation criteria in the ordered set where challengeId = &#63; and enabled = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param enabled the enabled
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching evaluation criteria
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria findBycriteriaByChallengeAndEnabled_Last(
		long challengeId, boolean enabled, OrderByComparator orderByComparator)
		throws NoSuchEvaluationCriteriaException, SystemException {
		EvaluationCriteria evaluationCriteria = fetchBycriteriaByChallengeAndEnabled_Last(challengeId,
				enabled, orderByComparator);

		if (evaluationCriteria != null) {
			return evaluationCriteria;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("challengeId=");
		msg.append(challengeId);

		msg.append(", enabled=");
		msg.append(enabled);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEvaluationCriteriaException(msg.toString());
	}

	/**
	 * Returns the last evaluation criteria in the ordered set where challengeId = &#63; and enabled = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param enabled the enabled
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria fetchBycriteriaByChallengeAndEnabled_Last(
		long challengeId, boolean enabled, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countBycriteriaByChallengeAndEnabled(challengeId, enabled);

		if (count == 0) {
			return null;
		}

		List<EvaluationCriteria> list = findBycriteriaByChallengeAndEnabled(challengeId,
				enabled, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the evaluation criterias before and after the current evaluation criteria in the ordered set where challengeId = &#63; and enabled = &#63;.
	 *
	 * @param evaluationCriteriaPK the primary key of the current evaluation criteria
	 * @param challengeId the challenge ID
	 * @param enabled the enabled
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next evaluation criteria
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a evaluation criteria with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria[] findBycriteriaByChallengeAndEnabled_PrevAndNext(
		EvaluationCriteriaPK evaluationCriteriaPK, long challengeId,
		boolean enabled, OrderByComparator orderByComparator)
		throws NoSuchEvaluationCriteriaException, SystemException {
		EvaluationCriteria evaluationCriteria = findByPrimaryKey(evaluationCriteriaPK);

		Session session = null;

		try {
			session = openSession();

			EvaluationCriteria[] array = new EvaluationCriteriaImpl[3];

			array[0] = getBycriteriaByChallengeAndEnabled_PrevAndNext(session,
					evaluationCriteria, challengeId, enabled,
					orderByComparator, true);

			array[1] = evaluationCriteria;

			array[2] = getBycriteriaByChallengeAndEnabled_PrevAndNext(session,
					evaluationCriteria, challengeId, enabled,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected EvaluationCriteria getBycriteriaByChallengeAndEnabled_PrevAndNext(
		Session session, EvaluationCriteria evaluationCriteria,
		long challengeId, boolean enabled, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_EVALUATIONCRITERIA_WHERE);

		query.append(_FINDER_COLUMN_CRITERIABYCHALLENGEANDENABLED_CHALLENGEID_2);

		query.append(_FINDER_COLUMN_CRITERIABYCHALLENGEANDENABLED_ENABLED_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(EvaluationCriteriaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(challengeId);

		qPos.add(enabled);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(evaluationCriteria);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<EvaluationCriteria> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the evaluation criterias where challengeId = &#63; and enabled = &#63; from the database.
	 *
	 * @param challengeId the challenge ID
	 * @param enabled the enabled
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBycriteriaByChallengeAndEnabled(long challengeId,
		boolean enabled) throws SystemException {
		for (EvaluationCriteria evaluationCriteria : findBycriteriaByChallengeAndEnabled(
				challengeId, enabled, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(evaluationCriteria);
		}
	}

	/**
	 * Returns the number of evaluation criterias where challengeId = &#63; and enabled = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param enabled the enabled
	 * @return the number of matching evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBycriteriaByChallengeAndEnabled(long challengeId,
		boolean enabled) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CRITERIABYCHALLENGEANDENABLED;

		Object[] finderArgs = new Object[] { challengeId, enabled };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_EVALUATIONCRITERIA_WHERE);

			query.append(_FINDER_COLUMN_CRITERIABYCHALLENGEANDENABLED_CHALLENGEID_2);

			query.append(_FINDER_COLUMN_CRITERIABYCHALLENGEANDENABLED_ENABLED_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(challengeId);

				qPos.add(enabled);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CRITERIABYCHALLENGEANDENABLED_CHALLENGEID_2 =
		"evaluationCriteria.id.challengeId = ? AND ";
	private static final String _FINDER_COLUMN_CRITERIABYCHALLENGEANDENABLED_ENABLED_2 =
		"evaluationCriteria.enabled = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_CRITERIAID = new FinderPath(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
			EvaluationCriteriaModelImpl.FINDER_CACHE_ENABLED,
			EvaluationCriteriaImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchBycriteriaId", new String[] { Long.class.getName() },
			EvaluationCriteriaModelImpl.CRITERIAID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CRITERIAID = new FinderPath(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
			EvaluationCriteriaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBycriteriaId",
			new String[] { Long.class.getName() });

	/**
	 * Returns the evaluation criteria where criteriaId = &#63; or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException} if it could not be found.
	 *
	 * @param criteriaId the criteria ID
	 * @return the matching evaluation criteria
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria findBycriteriaId(long criteriaId)
		throws NoSuchEvaluationCriteriaException, SystemException {
		EvaluationCriteria evaluationCriteria = fetchBycriteriaId(criteriaId);

		if (evaluationCriteria == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("criteriaId=");
			msg.append(criteriaId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchEvaluationCriteriaException(msg.toString());
		}

		return evaluationCriteria;
	}

	/**
	 * Returns the evaluation criteria where criteriaId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param criteriaId the criteria ID
	 * @return the matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria fetchBycriteriaId(long criteriaId)
		throws SystemException {
		return fetchBycriteriaId(criteriaId, true);
	}

	/**
	 * Returns the evaluation criteria where criteriaId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param criteriaId the criteria ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria fetchBycriteriaId(long criteriaId,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { criteriaId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_CRITERIAID,
					finderArgs, this);
		}

		if (result instanceof EvaluationCriteria) {
			EvaluationCriteria evaluationCriteria = (EvaluationCriteria)result;

			if ((criteriaId != evaluationCriteria.getCriteriaId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_EVALUATIONCRITERIA_WHERE);

			query.append(_FINDER_COLUMN_CRITERIAID_CRITERIAID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(criteriaId);

				List<EvaluationCriteria> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CRITERIAID,
						finderArgs, list);
				}
				else {
					EvaluationCriteria evaluationCriteria = list.get(0);

					result = evaluationCriteria;

					cacheResult(evaluationCriteria);

					if ((evaluationCriteria.getCriteriaId() != criteriaId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CRITERIAID,
							finderArgs, evaluationCriteria);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CRITERIAID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (EvaluationCriteria)result;
		}
	}

	/**
	 * Removes the evaluation criteria where criteriaId = &#63; from the database.
	 *
	 * @param criteriaId the criteria ID
	 * @return the evaluation criteria that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria removeBycriteriaId(long criteriaId)
		throws NoSuchEvaluationCriteriaException, SystemException {
		EvaluationCriteria evaluationCriteria = findBycriteriaId(criteriaId);

		return remove(evaluationCriteria);
	}

	/**
	 * Returns the number of evaluation criterias where criteriaId = &#63;.
	 *
	 * @param criteriaId the criteria ID
	 * @return the number of matching evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBycriteriaId(long criteriaId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CRITERIAID;

		Object[] finderArgs = new Object[] { criteriaId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_EVALUATIONCRITERIA_WHERE);

			query.append(_FINDER_COLUMN_CRITERIAID_CRITERIAID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(criteriaId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CRITERIAID_CRITERIAID_2 = "evaluationCriteria.id.criteriaId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CHALLENGEANDISBARRIERA =
		new FinderPath(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
			EvaluationCriteriaModelImpl.FINDER_CACHE_ENABLED,
			EvaluationCriteriaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByChallengeAndIsBarriera",
			new String[] {
				Long.class.getName(), Boolean.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEANDISBARRIERA =
		new FinderPath(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
			EvaluationCriteriaModelImpl.FINDER_CACHE_ENABLED,
			EvaluationCriteriaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByChallengeAndIsBarriera",
			new String[] { Long.class.getName(), Boolean.class.getName() },
			EvaluationCriteriaModelImpl.CHALLENGEID_COLUMN_BITMASK |
			EvaluationCriteriaModelImpl.ISBARRIERA_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CHALLENGEANDISBARRIERA = new FinderPath(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
			EvaluationCriteriaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByChallengeAndIsBarriera",
			new String[] { Long.class.getName(), Boolean.class.getName() });

	/**
	 * Returns all the evaluation criterias where challengeId = &#63; and isBarriera = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param isBarriera the is barriera
	 * @return the matching evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EvaluationCriteria> findByChallengeAndIsBarriera(
		long challengeId, boolean isBarriera) throws SystemException {
		return findByChallengeAndIsBarriera(challengeId, isBarriera,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the evaluation criterias where challengeId = &#63; and isBarriera = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param challengeId the challenge ID
	 * @param isBarriera the is barriera
	 * @param start the lower bound of the range of evaluation criterias
	 * @param end the upper bound of the range of evaluation criterias (not inclusive)
	 * @return the range of matching evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EvaluationCriteria> findByChallengeAndIsBarriera(
		long challengeId, boolean isBarriera, int start, int end)
		throws SystemException {
		return findByChallengeAndIsBarriera(challengeId, isBarriera, start,
			end, null);
	}

	/**
	 * Returns an ordered range of all the evaluation criterias where challengeId = &#63; and isBarriera = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param challengeId the challenge ID
	 * @param isBarriera the is barriera
	 * @param start the lower bound of the range of evaluation criterias
	 * @param end the upper bound of the range of evaluation criterias (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EvaluationCriteria> findByChallengeAndIsBarriera(
		long challengeId, boolean isBarriera, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEANDISBARRIERA;
			finderArgs = new Object[] { challengeId, isBarriera };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CHALLENGEANDISBARRIERA;
			finderArgs = new Object[] {
					challengeId, isBarriera,
					
					start, end, orderByComparator
				};
		}

		List<EvaluationCriteria> list = (List<EvaluationCriteria>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (EvaluationCriteria evaluationCriteria : list) {
				if ((challengeId != evaluationCriteria.getChallengeId()) ||
						(isBarriera != evaluationCriteria.getIsBarriera())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_EVALUATIONCRITERIA_WHERE);

			query.append(_FINDER_COLUMN_CHALLENGEANDISBARRIERA_CHALLENGEID_2);

			query.append(_FINDER_COLUMN_CHALLENGEANDISBARRIERA_ISBARRIERA_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(EvaluationCriteriaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(challengeId);

				qPos.add(isBarriera);

				if (!pagination) {
					list = (List<EvaluationCriteria>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<EvaluationCriteria>(list);
				}
				else {
					list = (List<EvaluationCriteria>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first evaluation criteria in the ordered set where challengeId = &#63; and isBarriera = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param isBarriera the is barriera
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching evaluation criteria
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria findByChallengeAndIsBarriera_First(
		long challengeId, boolean isBarriera,
		OrderByComparator orderByComparator)
		throws NoSuchEvaluationCriteriaException, SystemException {
		EvaluationCriteria evaluationCriteria = fetchByChallengeAndIsBarriera_First(challengeId,
				isBarriera, orderByComparator);

		if (evaluationCriteria != null) {
			return evaluationCriteria;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("challengeId=");
		msg.append(challengeId);

		msg.append(", isBarriera=");
		msg.append(isBarriera);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEvaluationCriteriaException(msg.toString());
	}

	/**
	 * Returns the first evaluation criteria in the ordered set where challengeId = &#63; and isBarriera = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param isBarriera the is barriera
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria fetchByChallengeAndIsBarriera_First(
		long challengeId, boolean isBarriera,
		OrderByComparator orderByComparator) throws SystemException {
		List<EvaluationCriteria> list = findByChallengeAndIsBarriera(challengeId,
				isBarriera, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last evaluation criteria in the ordered set where challengeId = &#63; and isBarriera = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param isBarriera the is barriera
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching evaluation criteria
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria findByChallengeAndIsBarriera_Last(
		long challengeId, boolean isBarriera,
		OrderByComparator orderByComparator)
		throws NoSuchEvaluationCriteriaException, SystemException {
		EvaluationCriteria evaluationCriteria = fetchByChallengeAndIsBarriera_Last(challengeId,
				isBarriera, orderByComparator);

		if (evaluationCriteria != null) {
			return evaluationCriteria;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("challengeId=");
		msg.append(challengeId);

		msg.append(", isBarriera=");
		msg.append(isBarriera);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEvaluationCriteriaException(msg.toString());
	}

	/**
	 * Returns the last evaluation criteria in the ordered set where challengeId = &#63; and isBarriera = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param isBarriera the is barriera
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria fetchByChallengeAndIsBarriera_Last(
		long challengeId, boolean isBarriera,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByChallengeAndIsBarriera(challengeId, isBarriera);

		if (count == 0) {
			return null;
		}

		List<EvaluationCriteria> list = findByChallengeAndIsBarriera(challengeId,
				isBarriera, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the evaluation criterias before and after the current evaluation criteria in the ordered set where challengeId = &#63; and isBarriera = &#63;.
	 *
	 * @param evaluationCriteriaPK the primary key of the current evaluation criteria
	 * @param challengeId the challenge ID
	 * @param isBarriera the is barriera
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next evaluation criteria
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a evaluation criteria with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria[] findByChallengeAndIsBarriera_PrevAndNext(
		EvaluationCriteriaPK evaluationCriteriaPK, long challengeId,
		boolean isBarriera, OrderByComparator orderByComparator)
		throws NoSuchEvaluationCriteriaException, SystemException {
		EvaluationCriteria evaluationCriteria = findByPrimaryKey(evaluationCriteriaPK);

		Session session = null;

		try {
			session = openSession();

			EvaluationCriteria[] array = new EvaluationCriteriaImpl[3];

			array[0] = getByChallengeAndIsBarriera_PrevAndNext(session,
					evaluationCriteria, challengeId, isBarriera,
					orderByComparator, true);

			array[1] = evaluationCriteria;

			array[2] = getByChallengeAndIsBarriera_PrevAndNext(session,
					evaluationCriteria, challengeId, isBarriera,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected EvaluationCriteria getByChallengeAndIsBarriera_PrevAndNext(
		Session session, EvaluationCriteria evaluationCriteria,
		long challengeId, boolean isBarriera,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_EVALUATIONCRITERIA_WHERE);

		query.append(_FINDER_COLUMN_CHALLENGEANDISBARRIERA_CHALLENGEID_2);

		query.append(_FINDER_COLUMN_CHALLENGEANDISBARRIERA_ISBARRIERA_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(EvaluationCriteriaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(challengeId);

		qPos.add(isBarriera);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(evaluationCriteria);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<EvaluationCriteria> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the evaluation criterias where challengeId = &#63; and isBarriera = &#63; from the database.
	 *
	 * @param challengeId the challenge ID
	 * @param isBarriera the is barriera
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByChallengeAndIsBarriera(long challengeId,
		boolean isBarriera) throws SystemException {
		for (EvaluationCriteria evaluationCriteria : findByChallengeAndIsBarriera(
				challengeId, isBarriera, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
				null)) {
			remove(evaluationCriteria);
		}
	}

	/**
	 * Returns the number of evaluation criterias where challengeId = &#63; and isBarriera = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param isBarriera the is barriera
	 * @return the number of matching evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByChallengeAndIsBarriera(long challengeId,
		boolean isBarriera) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CHALLENGEANDISBARRIERA;

		Object[] finderArgs = new Object[] { challengeId, isBarriera };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_EVALUATIONCRITERIA_WHERE);

			query.append(_FINDER_COLUMN_CHALLENGEANDISBARRIERA_CHALLENGEID_2);

			query.append(_FINDER_COLUMN_CHALLENGEANDISBARRIERA_ISBARRIERA_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(challengeId);

				qPos.add(isBarriera);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CHALLENGEANDISBARRIERA_CHALLENGEID_2 =
		"evaluationCriteria.id.challengeId = ? AND ";
	private static final String _FINDER_COLUMN_CHALLENGEANDISBARRIERA_ISBARRIERA_2 =
		"evaluationCriteria.isBarriera = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CHALLENGEANDISBARRIERAANDENABLED =
		new FinderPath(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
			EvaluationCriteriaModelImpl.FINDER_CACHE_ENABLED,
			EvaluationCriteriaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByChallengeAndIsBarrieraAndEnabled",
			new String[] {
				Long.class.getName(), Boolean.class.getName(),
				Boolean.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEANDISBARRIERAANDENABLED =
		new FinderPath(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
			EvaluationCriteriaModelImpl.FINDER_CACHE_ENABLED,
			EvaluationCriteriaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByChallengeAndIsBarrieraAndEnabled",
			new String[] {
				Long.class.getName(), Boolean.class.getName(),
				Boolean.class.getName()
			},
			EvaluationCriteriaModelImpl.CHALLENGEID_COLUMN_BITMASK |
			EvaluationCriteriaModelImpl.ISBARRIERA_COLUMN_BITMASK |
			EvaluationCriteriaModelImpl.ENABLED_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CHALLENGEANDISBARRIERAANDENABLED =
		new FinderPath(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
			EvaluationCriteriaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByChallengeAndIsBarrieraAndEnabled",
			new String[] {
				Long.class.getName(), Boolean.class.getName(),
				Boolean.class.getName()
			});

	/**
	 * Returns all the evaluation criterias where challengeId = &#63; and isBarriera = &#63; and enabled = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param isBarriera the is barriera
	 * @param enabled the enabled
	 * @return the matching evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EvaluationCriteria> findByChallengeAndIsBarrieraAndEnabled(
		long challengeId, boolean isBarriera, boolean enabled)
		throws SystemException {
		return findByChallengeAndIsBarrieraAndEnabled(challengeId, isBarriera,
			enabled, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the evaluation criterias where challengeId = &#63; and isBarriera = &#63; and enabled = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param challengeId the challenge ID
	 * @param isBarriera the is barriera
	 * @param enabled the enabled
	 * @param start the lower bound of the range of evaluation criterias
	 * @param end the upper bound of the range of evaluation criterias (not inclusive)
	 * @return the range of matching evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EvaluationCriteria> findByChallengeAndIsBarrieraAndEnabled(
		long challengeId, boolean isBarriera, boolean enabled, int start,
		int end) throws SystemException {
		return findByChallengeAndIsBarrieraAndEnabled(challengeId, isBarriera,
			enabled, start, end, null);
	}

	/**
	 * Returns an ordered range of all the evaluation criterias where challengeId = &#63; and isBarriera = &#63; and enabled = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param challengeId the challenge ID
	 * @param isBarriera the is barriera
	 * @param enabled the enabled
	 * @param start the lower bound of the range of evaluation criterias
	 * @param end the upper bound of the range of evaluation criterias (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EvaluationCriteria> findByChallengeAndIsBarrieraAndEnabled(
		long challengeId, boolean isBarriera, boolean enabled, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEANDISBARRIERAANDENABLED;
			finderArgs = new Object[] { challengeId, isBarriera, enabled };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CHALLENGEANDISBARRIERAANDENABLED;
			finderArgs = new Object[] {
					challengeId, isBarriera, enabled,
					
					start, end, orderByComparator
				};
		}

		List<EvaluationCriteria> list = (List<EvaluationCriteria>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (EvaluationCriteria evaluationCriteria : list) {
				if ((challengeId != evaluationCriteria.getChallengeId()) ||
						(isBarriera != evaluationCriteria.getIsBarriera()) ||
						(enabled != evaluationCriteria.getEnabled())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(5 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(5);
			}

			query.append(_SQL_SELECT_EVALUATIONCRITERIA_WHERE);

			query.append(_FINDER_COLUMN_CHALLENGEANDISBARRIERAANDENABLED_CHALLENGEID_2);

			query.append(_FINDER_COLUMN_CHALLENGEANDISBARRIERAANDENABLED_ISBARRIERA_2);

			query.append(_FINDER_COLUMN_CHALLENGEANDISBARRIERAANDENABLED_ENABLED_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(EvaluationCriteriaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(challengeId);

				qPos.add(isBarriera);

				qPos.add(enabled);

				if (!pagination) {
					list = (List<EvaluationCriteria>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<EvaluationCriteria>(list);
				}
				else {
					list = (List<EvaluationCriteria>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first evaluation criteria in the ordered set where challengeId = &#63; and isBarriera = &#63; and enabled = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param isBarriera the is barriera
	 * @param enabled the enabled
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching evaluation criteria
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria findByChallengeAndIsBarrieraAndEnabled_First(
		long challengeId, boolean isBarriera, boolean enabled,
		OrderByComparator orderByComparator)
		throws NoSuchEvaluationCriteriaException, SystemException {
		EvaluationCriteria evaluationCriteria = fetchByChallengeAndIsBarrieraAndEnabled_First(challengeId,
				isBarriera, enabled, orderByComparator);

		if (evaluationCriteria != null) {
			return evaluationCriteria;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("challengeId=");
		msg.append(challengeId);

		msg.append(", isBarriera=");
		msg.append(isBarriera);

		msg.append(", enabled=");
		msg.append(enabled);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEvaluationCriteriaException(msg.toString());
	}

	/**
	 * Returns the first evaluation criteria in the ordered set where challengeId = &#63; and isBarriera = &#63; and enabled = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param isBarriera the is barriera
	 * @param enabled the enabled
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria fetchByChallengeAndIsBarrieraAndEnabled_First(
		long challengeId, boolean isBarriera, boolean enabled,
		OrderByComparator orderByComparator) throws SystemException {
		List<EvaluationCriteria> list = findByChallengeAndIsBarrieraAndEnabled(challengeId,
				isBarriera, enabled, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last evaluation criteria in the ordered set where challengeId = &#63; and isBarriera = &#63; and enabled = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param isBarriera the is barriera
	 * @param enabled the enabled
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching evaluation criteria
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria findByChallengeAndIsBarrieraAndEnabled_Last(
		long challengeId, boolean isBarriera, boolean enabled,
		OrderByComparator orderByComparator)
		throws NoSuchEvaluationCriteriaException, SystemException {
		EvaluationCriteria evaluationCriteria = fetchByChallengeAndIsBarrieraAndEnabled_Last(challengeId,
				isBarriera, enabled, orderByComparator);

		if (evaluationCriteria != null) {
			return evaluationCriteria;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("challengeId=");
		msg.append(challengeId);

		msg.append(", isBarriera=");
		msg.append(isBarriera);

		msg.append(", enabled=");
		msg.append(enabled);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEvaluationCriteriaException(msg.toString());
	}

	/**
	 * Returns the last evaluation criteria in the ordered set where challengeId = &#63; and isBarriera = &#63; and enabled = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param isBarriera the is barriera
	 * @param enabled the enabled
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria fetchByChallengeAndIsBarrieraAndEnabled_Last(
		long challengeId, boolean isBarriera, boolean enabled,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByChallengeAndIsBarrieraAndEnabled(challengeId,
				isBarriera, enabled);

		if (count == 0) {
			return null;
		}

		List<EvaluationCriteria> list = findByChallengeAndIsBarrieraAndEnabled(challengeId,
				isBarriera, enabled, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the evaluation criterias before and after the current evaluation criteria in the ordered set where challengeId = &#63; and isBarriera = &#63; and enabled = &#63;.
	 *
	 * @param evaluationCriteriaPK the primary key of the current evaluation criteria
	 * @param challengeId the challenge ID
	 * @param isBarriera the is barriera
	 * @param enabled the enabled
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next evaluation criteria
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a evaluation criteria with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria[] findByChallengeAndIsBarrieraAndEnabled_PrevAndNext(
		EvaluationCriteriaPK evaluationCriteriaPK, long challengeId,
		boolean isBarriera, boolean enabled, OrderByComparator orderByComparator)
		throws NoSuchEvaluationCriteriaException, SystemException {
		EvaluationCriteria evaluationCriteria = findByPrimaryKey(evaluationCriteriaPK);

		Session session = null;

		try {
			session = openSession();

			EvaluationCriteria[] array = new EvaluationCriteriaImpl[3];

			array[0] = getByChallengeAndIsBarrieraAndEnabled_PrevAndNext(session,
					evaluationCriteria, challengeId, isBarriera, enabled,
					orderByComparator, true);

			array[1] = evaluationCriteria;

			array[2] = getByChallengeAndIsBarrieraAndEnabled_PrevAndNext(session,
					evaluationCriteria, challengeId, isBarriera, enabled,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected EvaluationCriteria getByChallengeAndIsBarrieraAndEnabled_PrevAndNext(
		Session session, EvaluationCriteria evaluationCriteria,
		long challengeId, boolean isBarriera, boolean enabled,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_EVALUATIONCRITERIA_WHERE);

		query.append(_FINDER_COLUMN_CHALLENGEANDISBARRIERAANDENABLED_CHALLENGEID_2);

		query.append(_FINDER_COLUMN_CHALLENGEANDISBARRIERAANDENABLED_ISBARRIERA_2);

		query.append(_FINDER_COLUMN_CHALLENGEANDISBARRIERAANDENABLED_ENABLED_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(EvaluationCriteriaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(challengeId);

		qPos.add(isBarriera);

		qPos.add(enabled);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(evaluationCriteria);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<EvaluationCriteria> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the evaluation criterias where challengeId = &#63; and isBarriera = &#63; and enabled = &#63; from the database.
	 *
	 * @param challengeId the challenge ID
	 * @param isBarriera the is barriera
	 * @param enabled the enabled
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByChallengeAndIsBarrieraAndEnabled(long challengeId,
		boolean isBarriera, boolean enabled) throws SystemException {
		for (EvaluationCriteria evaluationCriteria : findByChallengeAndIsBarrieraAndEnabled(
				challengeId, isBarriera, enabled, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(evaluationCriteria);
		}
	}

	/**
	 * Returns the number of evaluation criterias where challengeId = &#63; and isBarriera = &#63; and enabled = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param isBarriera the is barriera
	 * @param enabled the enabled
	 * @return the number of matching evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByChallengeAndIsBarrieraAndEnabled(long challengeId,
		boolean isBarriera, boolean enabled) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CHALLENGEANDISBARRIERAANDENABLED;

		Object[] finderArgs = new Object[] { challengeId, isBarriera, enabled };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_EVALUATIONCRITERIA_WHERE);

			query.append(_FINDER_COLUMN_CHALLENGEANDISBARRIERAANDENABLED_CHALLENGEID_2);

			query.append(_FINDER_COLUMN_CHALLENGEANDISBARRIERAANDENABLED_ISBARRIERA_2);

			query.append(_FINDER_COLUMN_CHALLENGEANDISBARRIERAANDENABLED_ENABLED_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(challengeId);

				qPos.add(isBarriera);

				qPos.add(enabled);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CHALLENGEANDISBARRIERAANDENABLED_CHALLENGEID_2 =
		"evaluationCriteria.id.challengeId = ? AND ";
	private static final String _FINDER_COLUMN_CHALLENGEANDISBARRIERAANDENABLED_ISBARRIERA_2 =
		"evaluationCriteria.isBarriera = ? AND ";
	private static final String _FINDER_COLUMN_CHALLENGEANDISBARRIERAANDENABLED_ENABLED_2 =
		"evaluationCriteria.enabled = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CHALLENGEANDBARRIERAANDCUSTOM =
		new FinderPath(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
			EvaluationCriteriaModelImpl.FINDER_CACHE_ENABLED,
			EvaluationCriteriaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByChallengeAndBarrieraAndCustom",
			new String[] {
				Long.class.getName(), Boolean.class.getName(),
				Boolean.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEANDBARRIERAANDCUSTOM =
		new FinderPath(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
			EvaluationCriteriaModelImpl.FINDER_CACHE_ENABLED,
			EvaluationCriteriaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByChallengeAndBarrieraAndCustom",
			new String[] {
				Long.class.getName(), Boolean.class.getName(),
				Boolean.class.getName()
			},
			EvaluationCriteriaModelImpl.CHALLENGEID_COLUMN_BITMASK |
			EvaluationCriteriaModelImpl.ISBARRIERA_COLUMN_BITMASK |
			EvaluationCriteriaModelImpl.ISCUSTOM_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CHALLENGEANDBARRIERAANDCUSTOM =
		new FinderPath(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
			EvaluationCriteriaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByChallengeAndBarrieraAndCustom",
			new String[] {
				Long.class.getName(), Boolean.class.getName(),
				Boolean.class.getName()
			});

	/**
	 * Returns all the evaluation criterias where challengeId = &#63; and isBarriera = &#63; and isCustom = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param isBarriera the is barriera
	 * @param isCustom the is custom
	 * @return the matching evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EvaluationCriteria> findByChallengeAndBarrieraAndCustom(
		long challengeId, boolean isBarriera, boolean isCustom)
		throws SystemException {
		return findByChallengeAndBarrieraAndCustom(challengeId, isBarriera,
			isCustom, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the evaluation criterias where challengeId = &#63; and isBarriera = &#63; and isCustom = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param challengeId the challenge ID
	 * @param isBarriera the is barriera
	 * @param isCustom the is custom
	 * @param start the lower bound of the range of evaluation criterias
	 * @param end the upper bound of the range of evaluation criterias (not inclusive)
	 * @return the range of matching evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EvaluationCriteria> findByChallengeAndBarrieraAndCustom(
		long challengeId, boolean isBarriera, boolean isCustom, int start,
		int end) throws SystemException {
		return findByChallengeAndBarrieraAndCustom(challengeId, isBarriera,
			isCustom, start, end, null);
	}

	/**
	 * Returns an ordered range of all the evaluation criterias where challengeId = &#63; and isBarriera = &#63; and isCustom = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param challengeId the challenge ID
	 * @param isBarriera the is barriera
	 * @param isCustom the is custom
	 * @param start the lower bound of the range of evaluation criterias
	 * @param end the upper bound of the range of evaluation criterias (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EvaluationCriteria> findByChallengeAndBarrieraAndCustom(
		long challengeId, boolean isBarriera, boolean isCustom, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEANDBARRIERAANDCUSTOM;
			finderArgs = new Object[] { challengeId, isBarriera, isCustom };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CHALLENGEANDBARRIERAANDCUSTOM;
			finderArgs = new Object[] {
					challengeId, isBarriera, isCustom,
					
					start, end, orderByComparator
				};
		}

		List<EvaluationCriteria> list = (List<EvaluationCriteria>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (EvaluationCriteria evaluationCriteria : list) {
				if ((challengeId != evaluationCriteria.getChallengeId()) ||
						(isBarriera != evaluationCriteria.getIsBarriera()) ||
						(isCustom != evaluationCriteria.getIsCustom())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(5 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(5);
			}

			query.append(_SQL_SELECT_EVALUATIONCRITERIA_WHERE);

			query.append(_FINDER_COLUMN_CHALLENGEANDBARRIERAANDCUSTOM_CHALLENGEID_2);

			query.append(_FINDER_COLUMN_CHALLENGEANDBARRIERAANDCUSTOM_ISBARRIERA_2);

			query.append(_FINDER_COLUMN_CHALLENGEANDBARRIERAANDCUSTOM_ISCUSTOM_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(EvaluationCriteriaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(challengeId);

				qPos.add(isBarriera);

				qPos.add(isCustom);

				if (!pagination) {
					list = (List<EvaluationCriteria>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<EvaluationCriteria>(list);
				}
				else {
					list = (List<EvaluationCriteria>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first evaluation criteria in the ordered set where challengeId = &#63; and isBarriera = &#63; and isCustom = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param isBarriera the is barriera
	 * @param isCustom the is custom
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching evaluation criteria
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria findByChallengeAndBarrieraAndCustom_First(
		long challengeId, boolean isBarriera, boolean isCustom,
		OrderByComparator orderByComparator)
		throws NoSuchEvaluationCriteriaException, SystemException {
		EvaluationCriteria evaluationCriteria = fetchByChallengeAndBarrieraAndCustom_First(challengeId,
				isBarriera, isCustom, orderByComparator);

		if (evaluationCriteria != null) {
			return evaluationCriteria;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("challengeId=");
		msg.append(challengeId);

		msg.append(", isBarriera=");
		msg.append(isBarriera);

		msg.append(", isCustom=");
		msg.append(isCustom);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEvaluationCriteriaException(msg.toString());
	}

	/**
	 * Returns the first evaluation criteria in the ordered set where challengeId = &#63; and isBarriera = &#63; and isCustom = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param isBarriera the is barriera
	 * @param isCustom the is custom
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria fetchByChallengeAndBarrieraAndCustom_First(
		long challengeId, boolean isBarriera, boolean isCustom,
		OrderByComparator orderByComparator) throws SystemException {
		List<EvaluationCriteria> list = findByChallengeAndBarrieraAndCustom(challengeId,
				isBarriera, isCustom, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last evaluation criteria in the ordered set where challengeId = &#63; and isBarriera = &#63; and isCustom = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param isBarriera the is barriera
	 * @param isCustom the is custom
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching evaluation criteria
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria findByChallengeAndBarrieraAndCustom_Last(
		long challengeId, boolean isBarriera, boolean isCustom,
		OrderByComparator orderByComparator)
		throws NoSuchEvaluationCriteriaException, SystemException {
		EvaluationCriteria evaluationCriteria = fetchByChallengeAndBarrieraAndCustom_Last(challengeId,
				isBarriera, isCustom, orderByComparator);

		if (evaluationCriteria != null) {
			return evaluationCriteria;
		}

		StringBundler msg = new StringBundler(8);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("challengeId=");
		msg.append(challengeId);

		msg.append(", isBarriera=");
		msg.append(isBarriera);

		msg.append(", isCustom=");
		msg.append(isCustom);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEvaluationCriteriaException(msg.toString());
	}

	/**
	 * Returns the last evaluation criteria in the ordered set where challengeId = &#63; and isBarriera = &#63; and isCustom = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param isBarriera the is barriera
	 * @param isCustom the is custom
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria fetchByChallengeAndBarrieraAndCustom_Last(
		long challengeId, boolean isBarriera, boolean isCustom,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByChallengeAndBarrieraAndCustom(challengeId,
				isBarriera, isCustom);

		if (count == 0) {
			return null;
		}

		List<EvaluationCriteria> list = findByChallengeAndBarrieraAndCustom(challengeId,
				isBarriera, isCustom, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the evaluation criterias before and after the current evaluation criteria in the ordered set where challengeId = &#63; and isBarriera = &#63; and isCustom = &#63;.
	 *
	 * @param evaluationCriteriaPK the primary key of the current evaluation criteria
	 * @param challengeId the challenge ID
	 * @param isBarriera the is barriera
	 * @param isCustom the is custom
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next evaluation criteria
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a evaluation criteria with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria[] findByChallengeAndBarrieraAndCustom_PrevAndNext(
		EvaluationCriteriaPK evaluationCriteriaPK, long challengeId,
		boolean isBarriera, boolean isCustom,
		OrderByComparator orderByComparator)
		throws NoSuchEvaluationCriteriaException, SystemException {
		EvaluationCriteria evaluationCriteria = findByPrimaryKey(evaluationCriteriaPK);

		Session session = null;

		try {
			session = openSession();

			EvaluationCriteria[] array = new EvaluationCriteriaImpl[3];

			array[0] = getByChallengeAndBarrieraAndCustom_PrevAndNext(session,
					evaluationCriteria, challengeId, isBarriera, isCustom,
					orderByComparator, true);

			array[1] = evaluationCriteria;

			array[2] = getByChallengeAndBarrieraAndCustom_PrevAndNext(session,
					evaluationCriteria, challengeId, isBarriera, isCustom,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected EvaluationCriteria getByChallengeAndBarrieraAndCustom_PrevAndNext(
		Session session, EvaluationCriteria evaluationCriteria,
		long challengeId, boolean isBarriera, boolean isCustom,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_EVALUATIONCRITERIA_WHERE);

		query.append(_FINDER_COLUMN_CHALLENGEANDBARRIERAANDCUSTOM_CHALLENGEID_2);

		query.append(_FINDER_COLUMN_CHALLENGEANDBARRIERAANDCUSTOM_ISBARRIERA_2);

		query.append(_FINDER_COLUMN_CHALLENGEANDBARRIERAANDCUSTOM_ISCUSTOM_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(EvaluationCriteriaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(challengeId);

		qPos.add(isBarriera);

		qPos.add(isCustom);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(evaluationCriteria);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<EvaluationCriteria> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the evaluation criterias where challengeId = &#63; and isBarriera = &#63; and isCustom = &#63; from the database.
	 *
	 * @param challengeId the challenge ID
	 * @param isBarriera the is barriera
	 * @param isCustom the is custom
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByChallengeAndBarrieraAndCustom(long challengeId,
		boolean isBarriera, boolean isCustom) throws SystemException {
		for (EvaluationCriteria evaluationCriteria : findByChallengeAndBarrieraAndCustom(
				challengeId, isBarriera, isCustom, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(evaluationCriteria);
		}
	}

	/**
	 * Returns the number of evaluation criterias where challengeId = &#63; and isBarriera = &#63; and isCustom = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param isBarriera the is barriera
	 * @param isCustom the is custom
	 * @return the number of matching evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByChallengeAndBarrieraAndCustom(long challengeId,
		boolean isBarriera, boolean isCustom) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CHALLENGEANDBARRIERAANDCUSTOM;

		Object[] finderArgs = new Object[] { challengeId, isBarriera, isCustom };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_COUNT_EVALUATIONCRITERIA_WHERE);

			query.append(_FINDER_COLUMN_CHALLENGEANDBARRIERAANDCUSTOM_CHALLENGEID_2);

			query.append(_FINDER_COLUMN_CHALLENGEANDBARRIERAANDCUSTOM_ISBARRIERA_2);

			query.append(_FINDER_COLUMN_CHALLENGEANDBARRIERAANDCUSTOM_ISCUSTOM_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(challengeId);

				qPos.add(isBarriera);

				qPos.add(isCustom);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CHALLENGEANDBARRIERAANDCUSTOM_CHALLENGEID_2 =
		"evaluationCriteria.id.challengeId = ? AND ";
	private static final String _FINDER_COLUMN_CHALLENGEANDBARRIERAANDCUSTOM_ISBARRIERA_2 =
		"evaluationCriteria.isBarriera = ? AND ";
	private static final String _FINDER_COLUMN_CHALLENGEANDBARRIERAANDCUSTOM_ISCUSTOM_2 =
		"evaluationCriteria.isCustom = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CHALLENGEIDANDINPUTID =
		new FinderPath(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
			EvaluationCriteriaModelImpl.FINDER_CACHE_ENABLED,
			EvaluationCriteriaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findBychallengeIdAndInputId",
			new String[] {
				Long.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEIDANDINPUTID =
		new FinderPath(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
			EvaluationCriteriaModelImpl.FINDER_CACHE_ENABLED,
			EvaluationCriteriaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findBychallengeIdAndInputId",
			new String[] { Long.class.getName(), String.class.getName() },
			EvaluationCriteriaModelImpl.CHALLENGEID_COLUMN_BITMASK |
			EvaluationCriteriaModelImpl.INPUTID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CHALLENGEIDANDINPUTID = new FinderPath(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
			EvaluationCriteriaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countBychallengeIdAndInputId",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns all the evaluation criterias where challengeId = &#63; and inputId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param inputId the input ID
	 * @return the matching evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EvaluationCriteria> findBychallengeIdAndInputId(
		long challengeId, String inputId) throws SystemException {
		return findBychallengeIdAndInputId(challengeId, inputId,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the evaluation criterias where challengeId = &#63; and inputId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param challengeId the challenge ID
	 * @param inputId the input ID
	 * @param start the lower bound of the range of evaluation criterias
	 * @param end the upper bound of the range of evaluation criterias (not inclusive)
	 * @return the range of matching evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EvaluationCriteria> findBychallengeIdAndInputId(
		long challengeId, String inputId, int start, int end)
		throws SystemException {
		return findBychallengeIdAndInputId(challengeId, inputId, start, end,
			null);
	}

	/**
	 * Returns an ordered range of all the evaluation criterias where challengeId = &#63; and inputId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param challengeId the challenge ID
	 * @param inputId the input ID
	 * @param start the lower bound of the range of evaluation criterias
	 * @param end the upper bound of the range of evaluation criterias (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EvaluationCriteria> findBychallengeIdAndInputId(
		long challengeId, String inputId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEIDANDINPUTID;
			finderArgs = new Object[] { challengeId, inputId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CHALLENGEIDANDINPUTID;
			finderArgs = new Object[] {
					challengeId, inputId,
					
					start, end, orderByComparator
				};
		}

		List<EvaluationCriteria> list = (List<EvaluationCriteria>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (EvaluationCriteria evaluationCriteria : list) {
				if ((challengeId != evaluationCriteria.getChallengeId()) ||
						!Validator.equals(inputId,
							evaluationCriteria.getInputId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_EVALUATIONCRITERIA_WHERE);

			query.append(_FINDER_COLUMN_CHALLENGEIDANDINPUTID_CHALLENGEID_2);

			boolean bindInputId = false;

			if (inputId == null) {
				query.append(_FINDER_COLUMN_CHALLENGEIDANDINPUTID_INPUTID_1);
			}
			else if (inputId.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CHALLENGEIDANDINPUTID_INPUTID_3);
			}
			else {
				bindInputId = true;

				query.append(_FINDER_COLUMN_CHALLENGEIDANDINPUTID_INPUTID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(EvaluationCriteriaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(challengeId);

				if (bindInputId) {
					qPos.add(inputId);
				}

				if (!pagination) {
					list = (List<EvaluationCriteria>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<EvaluationCriteria>(list);
				}
				else {
					list = (List<EvaluationCriteria>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first evaluation criteria in the ordered set where challengeId = &#63; and inputId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param inputId the input ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching evaluation criteria
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria findBychallengeIdAndInputId_First(
		long challengeId, String inputId, OrderByComparator orderByComparator)
		throws NoSuchEvaluationCriteriaException, SystemException {
		EvaluationCriteria evaluationCriteria = fetchBychallengeIdAndInputId_First(challengeId,
				inputId, orderByComparator);

		if (evaluationCriteria != null) {
			return evaluationCriteria;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("challengeId=");
		msg.append(challengeId);

		msg.append(", inputId=");
		msg.append(inputId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEvaluationCriteriaException(msg.toString());
	}

	/**
	 * Returns the first evaluation criteria in the ordered set where challengeId = &#63; and inputId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param inputId the input ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria fetchBychallengeIdAndInputId_First(
		long challengeId, String inputId, OrderByComparator orderByComparator)
		throws SystemException {
		List<EvaluationCriteria> list = findBychallengeIdAndInputId(challengeId,
				inputId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last evaluation criteria in the ordered set where challengeId = &#63; and inputId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param inputId the input ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching evaluation criteria
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria findBychallengeIdAndInputId_Last(
		long challengeId, String inputId, OrderByComparator orderByComparator)
		throws NoSuchEvaluationCriteriaException, SystemException {
		EvaluationCriteria evaluationCriteria = fetchBychallengeIdAndInputId_Last(challengeId,
				inputId, orderByComparator);

		if (evaluationCriteria != null) {
			return evaluationCriteria;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("challengeId=");
		msg.append(challengeId);

		msg.append(", inputId=");
		msg.append(inputId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEvaluationCriteriaException(msg.toString());
	}

	/**
	 * Returns the last evaluation criteria in the ordered set where challengeId = &#63; and inputId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param inputId the input ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria fetchBychallengeIdAndInputId_Last(
		long challengeId, String inputId, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countBychallengeIdAndInputId(challengeId, inputId);

		if (count == 0) {
			return null;
		}

		List<EvaluationCriteria> list = findBychallengeIdAndInputId(challengeId,
				inputId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the evaluation criterias before and after the current evaluation criteria in the ordered set where challengeId = &#63; and inputId = &#63;.
	 *
	 * @param evaluationCriteriaPK the primary key of the current evaluation criteria
	 * @param challengeId the challenge ID
	 * @param inputId the input ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next evaluation criteria
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a evaluation criteria with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria[] findBychallengeIdAndInputId_PrevAndNext(
		EvaluationCriteriaPK evaluationCriteriaPK, long challengeId,
		String inputId, OrderByComparator orderByComparator)
		throws NoSuchEvaluationCriteriaException, SystemException {
		EvaluationCriteria evaluationCriteria = findByPrimaryKey(evaluationCriteriaPK);

		Session session = null;

		try {
			session = openSession();

			EvaluationCriteria[] array = new EvaluationCriteriaImpl[3];

			array[0] = getBychallengeIdAndInputId_PrevAndNext(session,
					evaluationCriteria, challengeId, inputId,
					orderByComparator, true);

			array[1] = evaluationCriteria;

			array[2] = getBychallengeIdAndInputId_PrevAndNext(session,
					evaluationCriteria, challengeId, inputId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected EvaluationCriteria getBychallengeIdAndInputId_PrevAndNext(
		Session session, EvaluationCriteria evaluationCriteria,
		long challengeId, String inputId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_EVALUATIONCRITERIA_WHERE);

		query.append(_FINDER_COLUMN_CHALLENGEIDANDINPUTID_CHALLENGEID_2);

		boolean bindInputId = false;

		if (inputId == null) {
			query.append(_FINDER_COLUMN_CHALLENGEIDANDINPUTID_INPUTID_1);
		}
		else if (inputId.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_CHALLENGEIDANDINPUTID_INPUTID_3);
		}
		else {
			bindInputId = true;

			query.append(_FINDER_COLUMN_CHALLENGEIDANDINPUTID_INPUTID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(EvaluationCriteriaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(challengeId);

		if (bindInputId) {
			qPos.add(inputId);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(evaluationCriteria);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<EvaluationCriteria> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the evaluation criterias where challengeId = &#63; and inputId = &#63; from the database.
	 *
	 * @param challengeId the challenge ID
	 * @param inputId the input ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBychallengeIdAndInputId(long challengeId, String inputId)
		throws SystemException {
		for (EvaluationCriteria evaluationCriteria : findBychallengeIdAndInputId(
				challengeId, inputId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(evaluationCriteria);
		}
	}

	/**
	 * Returns the number of evaluation criterias where challengeId = &#63; and inputId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param inputId the input ID
	 * @return the number of matching evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBychallengeIdAndInputId(long challengeId, String inputId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CHALLENGEIDANDINPUTID;

		Object[] finderArgs = new Object[] { challengeId, inputId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_EVALUATIONCRITERIA_WHERE);

			query.append(_FINDER_COLUMN_CHALLENGEIDANDINPUTID_CHALLENGEID_2);

			boolean bindInputId = false;

			if (inputId == null) {
				query.append(_FINDER_COLUMN_CHALLENGEIDANDINPUTID_INPUTID_1);
			}
			else if (inputId.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CHALLENGEIDANDINPUTID_INPUTID_3);
			}
			else {
				bindInputId = true;

				query.append(_FINDER_COLUMN_CHALLENGEIDANDINPUTID_INPUTID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(challengeId);

				if (bindInputId) {
					qPos.add(inputId);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CHALLENGEIDANDINPUTID_CHALLENGEID_2 =
		"evaluationCriteria.id.challengeId = ? AND ";
	private static final String _FINDER_COLUMN_CHALLENGEIDANDINPUTID_INPUTID_1 = "evaluationCriteria.inputId IS NULL";
	private static final String _FINDER_COLUMN_CHALLENGEIDANDINPUTID_INPUTID_2 = "evaluationCriteria.inputId = ?";
	private static final String _FINDER_COLUMN_CHALLENGEIDANDINPUTID_INPUTID_3 = "(evaluationCriteria.inputId IS NULL OR evaluationCriteria.inputId = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ISCUSTOMANDLANGUAGE =
		new FinderPath(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
			EvaluationCriteriaModelImpl.FINDER_CACHE_ENABLED,
			EvaluationCriteriaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByisCustomAndLanguage",
			new String[] {
				Boolean.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ISCUSTOMANDLANGUAGE =
		new FinderPath(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
			EvaluationCriteriaModelImpl.FINDER_CACHE_ENABLED,
			EvaluationCriteriaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByisCustomAndLanguage",
			new String[] { Boolean.class.getName(), String.class.getName() },
			EvaluationCriteriaModelImpl.ISCUSTOM_COLUMN_BITMASK |
			EvaluationCriteriaModelImpl.LANGUAGE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_ISCUSTOMANDLANGUAGE = new FinderPath(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
			EvaluationCriteriaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByisCustomAndLanguage",
			new String[] { Boolean.class.getName(), String.class.getName() });

	/**
	 * Returns all the evaluation criterias where isCustom = &#63; and language = &#63;.
	 *
	 * @param isCustom the is custom
	 * @param language the language
	 * @return the matching evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EvaluationCriteria> findByisCustomAndLanguage(
		boolean isCustom, String language) throws SystemException {
		return findByisCustomAndLanguage(isCustom, language, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the evaluation criterias where isCustom = &#63; and language = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param isCustom the is custom
	 * @param language the language
	 * @param start the lower bound of the range of evaluation criterias
	 * @param end the upper bound of the range of evaluation criterias (not inclusive)
	 * @return the range of matching evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EvaluationCriteria> findByisCustomAndLanguage(
		boolean isCustom, String language, int start, int end)
		throws SystemException {
		return findByisCustomAndLanguage(isCustom, language, start, end, null);
	}

	/**
	 * Returns an ordered range of all the evaluation criterias where isCustom = &#63; and language = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param isCustom the is custom
	 * @param language the language
	 * @param start the lower bound of the range of evaluation criterias
	 * @param end the upper bound of the range of evaluation criterias (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EvaluationCriteria> findByisCustomAndLanguage(
		boolean isCustom, String language, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ISCUSTOMANDLANGUAGE;
			finderArgs = new Object[] { isCustom, language };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ISCUSTOMANDLANGUAGE;
			finderArgs = new Object[] {
					isCustom, language,
					
					start, end, orderByComparator
				};
		}

		List<EvaluationCriteria> list = (List<EvaluationCriteria>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (EvaluationCriteria evaluationCriteria : list) {
				if ((isCustom != evaluationCriteria.getIsCustom()) ||
						!Validator.equals(language,
							evaluationCriteria.getLanguage())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_EVALUATIONCRITERIA_WHERE);

			query.append(_FINDER_COLUMN_ISCUSTOMANDLANGUAGE_ISCUSTOM_2);

			boolean bindLanguage = false;

			if (language == null) {
				query.append(_FINDER_COLUMN_ISCUSTOMANDLANGUAGE_LANGUAGE_1);
			}
			else if (language.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ISCUSTOMANDLANGUAGE_LANGUAGE_3);
			}
			else {
				bindLanguage = true;

				query.append(_FINDER_COLUMN_ISCUSTOMANDLANGUAGE_LANGUAGE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(EvaluationCriteriaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(isCustom);

				if (bindLanguage) {
					qPos.add(language);
				}

				if (!pagination) {
					list = (List<EvaluationCriteria>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<EvaluationCriteria>(list);
				}
				else {
					list = (List<EvaluationCriteria>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first evaluation criteria in the ordered set where isCustom = &#63; and language = &#63;.
	 *
	 * @param isCustom the is custom
	 * @param language the language
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching evaluation criteria
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria findByisCustomAndLanguage_First(
		boolean isCustom, String language, OrderByComparator orderByComparator)
		throws NoSuchEvaluationCriteriaException, SystemException {
		EvaluationCriteria evaluationCriteria = fetchByisCustomAndLanguage_First(isCustom,
				language, orderByComparator);

		if (evaluationCriteria != null) {
			return evaluationCriteria;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("isCustom=");
		msg.append(isCustom);

		msg.append(", language=");
		msg.append(language);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEvaluationCriteriaException(msg.toString());
	}

	/**
	 * Returns the first evaluation criteria in the ordered set where isCustom = &#63; and language = &#63;.
	 *
	 * @param isCustom the is custom
	 * @param language the language
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria fetchByisCustomAndLanguage_First(
		boolean isCustom, String language, OrderByComparator orderByComparator)
		throws SystemException {
		List<EvaluationCriteria> list = findByisCustomAndLanguage(isCustom,
				language, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last evaluation criteria in the ordered set where isCustom = &#63; and language = &#63;.
	 *
	 * @param isCustom the is custom
	 * @param language the language
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching evaluation criteria
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria findByisCustomAndLanguage_Last(boolean isCustom,
		String language, OrderByComparator orderByComparator)
		throws NoSuchEvaluationCriteriaException, SystemException {
		EvaluationCriteria evaluationCriteria = fetchByisCustomAndLanguage_Last(isCustom,
				language, orderByComparator);

		if (evaluationCriteria != null) {
			return evaluationCriteria;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("isCustom=");
		msg.append(isCustom);

		msg.append(", language=");
		msg.append(language);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchEvaluationCriteriaException(msg.toString());
	}

	/**
	 * Returns the last evaluation criteria in the ordered set where isCustom = &#63; and language = &#63;.
	 *
	 * @param isCustom the is custom
	 * @param language the language
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching evaluation criteria, or <code>null</code> if a matching evaluation criteria could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria fetchByisCustomAndLanguage_Last(
		boolean isCustom, String language, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByisCustomAndLanguage(isCustom, language);

		if (count == 0) {
			return null;
		}

		List<EvaluationCriteria> list = findByisCustomAndLanguage(isCustom,
				language, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the evaluation criterias before and after the current evaluation criteria in the ordered set where isCustom = &#63; and language = &#63;.
	 *
	 * @param evaluationCriteriaPK the primary key of the current evaluation criteria
	 * @param isCustom the is custom
	 * @param language the language
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next evaluation criteria
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a evaluation criteria with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria[] findByisCustomAndLanguage_PrevAndNext(
		EvaluationCriteriaPK evaluationCriteriaPK, boolean isCustom,
		String language, OrderByComparator orderByComparator)
		throws NoSuchEvaluationCriteriaException, SystemException {
		EvaluationCriteria evaluationCriteria = findByPrimaryKey(evaluationCriteriaPK);

		Session session = null;

		try {
			session = openSession();

			EvaluationCriteria[] array = new EvaluationCriteriaImpl[3];

			array[0] = getByisCustomAndLanguage_PrevAndNext(session,
					evaluationCriteria, isCustom, language, orderByComparator,
					true);

			array[1] = evaluationCriteria;

			array[2] = getByisCustomAndLanguage_PrevAndNext(session,
					evaluationCriteria, isCustom, language, orderByComparator,
					false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected EvaluationCriteria getByisCustomAndLanguage_PrevAndNext(
		Session session, EvaluationCriteria evaluationCriteria,
		boolean isCustom, String language, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_EVALUATIONCRITERIA_WHERE);

		query.append(_FINDER_COLUMN_ISCUSTOMANDLANGUAGE_ISCUSTOM_2);

		boolean bindLanguage = false;

		if (language == null) {
			query.append(_FINDER_COLUMN_ISCUSTOMANDLANGUAGE_LANGUAGE_1);
		}
		else if (language.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_ISCUSTOMANDLANGUAGE_LANGUAGE_3);
		}
		else {
			bindLanguage = true;

			query.append(_FINDER_COLUMN_ISCUSTOMANDLANGUAGE_LANGUAGE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(EvaluationCriteriaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(isCustom);

		if (bindLanguage) {
			qPos.add(language);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(evaluationCriteria);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<EvaluationCriteria> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the evaluation criterias where isCustom = &#63; and language = &#63; from the database.
	 *
	 * @param isCustom the is custom
	 * @param language the language
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByisCustomAndLanguage(boolean isCustom, String language)
		throws SystemException {
		for (EvaluationCriteria evaluationCriteria : findByisCustomAndLanguage(
				isCustom, language, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(evaluationCriteria);
		}
	}

	/**
	 * Returns the number of evaluation criterias where isCustom = &#63; and language = &#63;.
	 *
	 * @param isCustom the is custom
	 * @param language the language
	 * @return the number of matching evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByisCustomAndLanguage(boolean isCustom, String language)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_ISCUSTOMANDLANGUAGE;

		Object[] finderArgs = new Object[] { isCustom, language };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_EVALUATIONCRITERIA_WHERE);

			query.append(_FINDER_COLUMN_ISCUSTOMANDLANGUAGE_ISCUSTOM_2);

			boolean bindLanguage = false;

			if (language == null) {
				query.append(_FINDER_COLUMN_ISCUSTOMANDLANGUAGE_LANGUAGE_1);
			}
			else if (language.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_ISCUSTOMANDLANGUAGE_LANGUAGE_3);
			}
			else {
				bindLanguage = true;

				query.append(_FINDER_COLUMN_ISCUSTOMANDLANGUAGE_LANGUAGE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(isCustom);

				if (bindLanguage) {
					qPos.add(language);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_ISCUSTOMANDLANGUAGE_ISCUSTOM_2 = "evaluationCriteria.isCustom = ? AND ";
	private static final String _FINDER_COLUMN_ISCUSTOMANDLANGUAGE_LANGUAGE_1 = "evaluationCriteria.language IS NULL";
	private static final String _FINDER_COLUMN_ISCUSTOMANDLANGUAGE_LANGUAGE_2 = "evaluationCriteria.language = ?";
	private static final String _FINDER_COLUMN_ISCUSTOMANDLANGUAGE_LANGUAGE_3 = "(evaluationCriteria.language IS NULL OR evaluationCriteria.language = '')";

	public EvaluationCriteriaPersistenceImpl() {
		setModelClass(EvaluationCriteria.class);
	}

	/**
	 * Caches the evaluation criteria in the entity cache if it is enabled.
	 *
	 * @param evaluationCriteria the evaluation criteria
	 */
	@Override
	public void cacheResult(EvaluationCriteria evaluationCriteria) {
		EntityCacheUtil.putResult(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
			EvaluationCriteriaImpl.class, evaluationCriteria.getPrimaryKey(),
			evaluationCriteria);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CRITERIAID,
			new Object[] { evaluationCriteria.getCriteriaId() },
			evaluationCriteria);

		evaluationCriteria.resetOriginalValues();
	}

	/**
	 * Caches the evaluation criterias in the entity cache if it is enabled.
	 *
	 * @param evaluationCriterias the evaluation criterias
	 */
	@Override
	public void cacheResult(List<EvaluationCriteria> evaluationCriterias) {
		for (EvaluationCriteria evaluationCriteria : evaluationCriterias) {
			if (EntityCacheUtil.getResult(
						EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
						EvaluationCriteriaImpl.class,
						evaluationCriteria.getPrimaryKey()) == null) {
				cacheResult(evaluationCriteria);
			}
			else {
				evaluationCriteria.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all evaluation criterias.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(EvaluationCriteriaImpl.class.getName());
		}

		EntityCacheUtil.clearCache(EvaluationCriteriaImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the evaluation criteria.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(EvaluationCriteria evaluationCriteria) {
		EntityCacheUtil.removeResult(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
			EvaluationCriteriaImpl.class, evaluationCriteria.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(evaluationCriteria);
	}

	@Override
	public void clearCache(List<EvaluationCriteria> evaluationCriterias) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (EvaluationCriteria evaluationCriteria : evaluationCriterias) {
			EntityCacheUtil.removeResult(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
				EvaluationCriteriaImpl.class, evaluationCriteria.getPrimaryKey());

			clearUniqueFindersCache(evaluationCriteria);
		}
	}

	protected void cacheUniqueFindersCache(
		EvaluationCriteria evaluationCriteria) {
		if (evaluationCriteria.isNew()) {
			Object[] args = new Object[] { evaluationCriteria.getCriteriaId() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_CRITERIAID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CRITERIAID, args,
				evaluationCriteria);
		}
		else {
			EvaluationCriteriaModelImpl evaluationCriteriaModelImpl = (EvaluationCriteriaModelImpl)evaluationCriteria;

			if ((evaluationCriteriaModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_CRITERIAID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { evaluationCriteria.getCriteriaId() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_CRITERIAID,
					args, Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CRITERIAID,
					args, evaluationCriteria);
			}
		}
	}

	protected void clearUniqueFindersCache(
		EvaluationCriteria evaluationCriteria) {
		EvaluationCriteriaModelImpl evaluationCriteriaModelImpl = (EvaluationCriteriaModelImpl)evaluationCriteria;

		Object[] args = new Object[] { evaluationCriteria.getCriteriaId() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CRITERIAID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CRITERIAID, args);

		if ((evaluationCriteriaModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_CRITERIAID.getColumnBitmask()) != 0) {
			args = new Object[] {
					evaluationCriteriaModelImpl.getOriginalCriteriaId()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CRITERIAID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CRITERIAID, args);
		}
	}

	/**
	 * Creates a new evaluation criteria with the primary key. Does not add the evaluation criteria to the database.
	 *
	 * @param evaluationCriteriaPK the primary key for the new evaluation criteria
	 * @return the new evaluation criteria
	 */
	@Override
	public EvaluationCriteria create(EvaluationCriteriaPK evaluationCriteriaPK) {
		EvaluationCriteria evaluationCriteria = new EvaluationCriteriaImpl();

		evaluationCriteria.setNew(true);
		evaluationCriteria.setPrimaryKey(evaluationCriteriaPK);

		return evaluationCriteria;
	}

	/**
	 * Removes the evaluation criteria with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param evaluationCriteriaPK the primary key of the evaluation criteria
	 * @return the evaluation criteria that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a evaluation criteria with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria remove(EvaluationCriteriaPK evaluationCriteriaPK)
		throws NoSuchEvaluationCriteriaException, SystemException {
		return remove((Serializable)evaluationCriteriaPK);
	}

	/**
	 * Removes the evaluation criteria with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the evaluation criteria
	 * @return the evaluation criteria that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a evaluation criteria with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria remove(Serializable primaryKey)
		throws NoSuchEvaluationCriteriaException, SystemException {
		Session session = null;

		try {
			session = openSession();

			EvaluationCriteria evaluationCriteria = (EvaluationCriteria)session.get(EvaluationCriteriaImpl.class,
					primaryKey);

			if (evaluationCriteria == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchEvaluationCriteriaException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(evaluationCriteria);
		}
		catch (NoSuchEvaluationCriteriaException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected EvaluationCriteria removeImpl(
		EvaluationCriteria evaluationCriteria) throws SystemException {
		evaluationCriteria = toUnwrappedModel(evaluationCriteria);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(evaluationCriteria)) {
				evaluationCriteria = (EvaluationCriteria)session.get(EvaluationCriteriaImpl.class,
						evaluationCriteria.getPrimaryKeyObj());
			}

			if (evaluationCriteria != null) {
				session.delete(evaluationCriteria);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (evaluationCriteria != null) {
			clearCache(evaluationCriteria);
		}

		return evaluationCriteria;
	}

	@Override
	public EvaluationCriteria updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria evaluationCriteria)
		throws SystemException {
		evaluationCriteria = toUnwrappedModel(evaluationCriteria);

		boolean isNew = evaluationCriteria.isNew();

		EvaluationCriteriaModelImpl evaluationCriteriaModelImpl = (EvaluationCriteriaModelImpl)evaluationCriteria;

		Session session = null;

		try {
			session = openSession();

			if (evaluationCriteria.isNew()) {
				session.save(evaluationCriteria);

				evaluationCriteria.setNew(false);
			}
			else {
				session.merge(evaluationCriteria);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !EvaluationCriteriaModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((evaluationCriteriaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CRITERIABYCHALLENGE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						evaluationCriteriaModelImpl.getOriginalChallengeId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CRITERIABYCHALLENGE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CRITERIABYCHALLENGE,
					args);

				args = new Object[] { evaluationCriteriaModelImpl.getChallengeId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CRITERIABYCHALLENGE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CRITERIABYCHALLENGE,
					args);
			}

			if ((evaluationCriteriaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CRITERIABYCHALLENGEANDENABLED.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						evaluationCriteriaModelImpl.getOriginalChallengeId(),
						evaluationCriteriaModelImpl.getOriginalEnabled()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CRITERIABYCHALLENGEANDENABLED,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CRITERIABYCHALLENGEANDENABLED,
					args);

				args = new Object[] {
						evaluationCriteriaModelImpl.getChallengeId(),
						evaluationCriteriaModelImpl.getEnabled()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CRITERIABYCHALLENGEANDENABLED,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CRITERIABYCHALLENGEANDENABLED,
					args);
			}

			if ((evaluationCriteriaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEANDISBARRIERA.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						evaluationCriteriaModelImpl.getOriginalChallengeId(),
						evaluationCriteriaModelImpl.getOriginalIsBarriera()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CHALLENGEANDISBARRIERA,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEANDISBARRIERA,
					args);

				args = new Object[] {
						evaluationCriteriaModelImpl.getChallengeId(),
						evaluationCriteriaModelImpl.getIsBarriera()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CHALLENGEANDISBARRIERA,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEANDISBARRIERA,
					args);
			}

			if ((evaluationCriteriaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEANDISBARRIERAANDENABLED.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						evaluationCriteriaModelImpl.getOriginalChallengeId(),
						evaluationCriteriaModelImpl.getOriginalIsBarriera(),
						evaluationCriteriaModelImpl.getOriginalEnabled()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CHALLENGEANDISBARRIERAANDENABLED,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEANDISBARRIERAANDENABLED,
					args);

				args = new Object[] {
						evaluationCriteriaModelImpl.getChallengeId(),
						evaluationCriteriaModelImpl.getIsBarriera(),
						evaluationCriteriaModelImpl.getEnabled()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CHALLENGEANDISBARRIERAANDENABLED,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEANDISBARRIERAANDENABLED,
					args);
			}

			if ((evaluationCriteriaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEANDBARRIERAANDCUSTOM.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						evaluationCriteriaModelImpl.getOriginalChallengeId(),
						evaluationCriteriaModelImpl.getOriginalIsBarriera(),
						evaluationCriteriaModelImpl.getOriginalIsCustom()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CHALLENGEANDBARRIERAANDCUSTOM,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEANDBARRIERAANDCUSTOM,
					args);

				args = new Object[] {
						evaluationCriteriaModelImpl.getChallengeId(),
						evaluationCriteriaModelImpl.getIsBarriera(),
						evaluationCriteriaModelImpl.getIsCustom()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CHALLENGEANDBARRIERAANDCUSTOM,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEANDBARRIERAANDCUSTOM,
					args);
			}

			if ((evaluationCriteriaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEIDANDINPUTID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						evaluationCriteriaModelImpl.getOriginalChallengeId(),
						evaluationCriteriaModelImpl.getOriginalInputId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CHALLENGEIDANDINPUTID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEIDANDINPUTID,
					args);

				args = new Object[] {
						evaluationCriteriaModelImpl.getChallengeId(),
						evaluationCriteriaModelImpl.getInputId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CHALLENGEIDANDINPUTID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEIDANDINPUTID,
					args);
			}

			if ((evaluationCriteriaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ISCUSTOMANDLANGUAGE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						evaluationCriteriaModelImpl.getOriginalIsCustom(),
						evaluationCriteriaModelImpl.getOriginalLanguage()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ISCUSTOMANDLANGUAGE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ISCUSTOMANDLANGUAGE,
					args);

				args = new Object[] {
						evaluationCriteriaModelImpl.getIsCustom(),
						evaluationCriteriaModelImpl.getLanguage()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ISCUSTOMANDLANGUAGE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ISCUSTOMANDLANGUAGE,
					args);
			}
		}

		EntityCacheUtil.putResult(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
			EvaluationCriteriaImpl.class, evaluationCriteria.getPrimaryKey(),
			evaluationCriteria);

		clearUniqueFindersCache(evaluationCriteria);
		cacheUniqueFindersCache(evaluationCriteria);

		return evaluationCriteria;
	}

	protected EvaluationCriteria toUnwrappedModel(
		EvaluationCriteria evaluationCriteria) {
		if (evaluationCriteria instanceof EvaluationCriteriaImpl) {
			return evaluationCriteria;
		}

		EvaluationCriteriaImpl evaluationCriteriaImpl = new EvaluationCriteriaImpl();

		evaluationCriteriaImpl.setNew(evaluationCriteria.isNew());
		evaluationCriteriaImpl.setPrimaryKey(evaluationCriteria.getPrimaryKey());

		evaluationCriteriaImpl.setCriteriaId(evaluationCriteria.getCriteriaId());
		evaluationCriteriaImpl.setChallengeId(evaluationCriteria.getChallengeId());
		evaluationCriteriaImpl.setEnabled(evaluationCriteria.isEnabled());
		evaluationCriteriaImpl.setDescription(evaluationCriteria.getDescription());
		evaluationCriteriaImpl.setIsBarriera(evaluationCriteria.isIsBarriera());
		evaluationCriteriaImpl.setIsCustom(evaluationCriteria.isIsCustom());
		evaluationCriteriaImpl.setLanguage(evaluationCriteria.getLanguage());
		evaluationCriteriaImpl.setInputId(evaluationCriteria.getInputId());
		evaluationCriteriaImpl.setWeight(evaluationCriteria.getWeight());
		evaluationCriteriaImpl.setThreshold(evaluationCriteria.getThreshold());
		evaluationCriteriaImpl.setDate(evaluationCriteria.getDate());
		evaluationCriteriaImpl.setAuthorId(evaluationCriteria.getAuthorId());

		return evaluationCriteriaImpl;
	}

	/**
	 * Returns the evaluation criteria with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the evaluation criteria
	 * @return the evaluation criteria
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a evaluation criteria with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria findByPrimaryKey(Serializable primaryKey)
		throws NoSuchEvaluationCriteriaException, SystemException {
		EvaluationCriteria evaluationCriteria = fetchByPrimaryKey(primaryKey);

		if (evaluationCriteria == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchEvaluationCriteriaException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return evaluationCriteria;
	}

	/**
	 * Returns the evaluation criteria with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException} if it could not be found.
	 *
	 * @param evaluationCriteriaPK the primary key of the evaluation criteria
	 * @return the evaluation criteria
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException if a evaluation criteria with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria findByPrimaryKey(
		EvaluationCriteriaPK evaluationCriteriaPK)
		throws NoSuchEvaluationCriteriaException, SystemException {
		return findByPrimaryKey((Serializable)evaluationCriteriaPK);
	}

	/**
	 * Returns the evaluation criteria with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the evaluation criteria
	 * @return the evaluation criteria, or <code>null</code> if a evaluation criteria with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		EvaluationCriteria evaluationCriteria = (EvaluationCriteria)EntityCacheUtil.getResult(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
				EvaluationCriteriaImpl.class, primaryKey);

		if (evaluationCriteria == _nullEvaluationCriteria) {
			return null;
		}

		if (evaluationCriteria == null) {
			Session session = null;

			try {
				session = openSession();

				evaluationCriteria = (EvaluationCriteria)session.get(EvaluationCriteriaImpl.class,
						primaryKey);

				if (evaluationCriteria != null) {
					cacheResult(evaluationCriteria);
				}
				else {
					EntityCacheUtil.putResult(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
						EvaluationCriteriaImpl.class, primaryKey,
						_nullEvaluationCriteria);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(EvaluationCriteriaModelImpl.ENTITY_CACHE_ENABLED,
					EvaluationCriteriaImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return evaluationCriteria;
	}

	/**
	 * Returns the evaluation criteria with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param evaluationCriteriaPK the primary key of the evaluation criteria
	 * @return the evaluation criteria, or <code>null</code> if a evaluation criteria with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EvaluationCriteria fetchByPrimaryKey(
		EvaluationCriteriaPK evaluationCriteriaPK) throws SystemException {
		return fetchByPrimaryKey((Serializable)evaluationCriteriaPK);
	}

	/**
	 * Returns all the evaluation criterias.
	 *
	 * @return the evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EvaluationCriteria> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the evaluation criterias.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of evaluation criterias
	 * @param end the upper bound of the range of evaluation criterias (not inclusive)
	 * @return the range of evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EvaluationCriteria> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the evaluation criterias.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.EvaluationCriteriaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of evaluation criterias
	 * @param end the upper bound of the range of evaluation criterias (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EvaluationCriteria> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<EvaluationCriteria> list = (List<EvaluationCriteria>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_EVALUATIONCRITERIA);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_EVALUATIONCRITERIA;

				if (pagination) {
					sql = sql.concat(EvaluationCriteriaModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<EvaluationCriteria>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<EvaluationCriteria>(list);
				}
				else {
					list = (List<EvaluationCriteria>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the evaluation criterias from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (EvaluationCriteria evaluationCriteria : findAll()) {
			remove(evaluationCriteria);
		}
	}

	/**
	 * Returns the number of evaluation criterias.
	 *
	 * @return the number of evaluation criterias
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_EVALUATIONCRITERIA);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the evaluation criteria persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<EvaluationCriteria>> listenersList = new ArrayList<ModelListener<EvaluationCriteria>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<EvaluationCriteria>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(EvaluationCriteriaImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_EVALUATIONCRITERIA = "SELECT evaluationCriteria FROM EvaluationCriteria evaluationCriteria";
	private static final String _SQL_SELECT_EVALUATIONCRITERIA_WHERE = "SELECT evaluationCriteria FROM EvaluationCriteria evaluationCriteria WHERE ";
	private static final String _SQL_COUNT_EVALUATIONCRITERIA = "SELECT COUNT(evaluationCriteria) FROM EvaluationCriteria evaluationCriteria";
	private static final String _SQL_COUNT_EVALUATIONCRITERIA_WHERE = "SELECT COUNT(evaluationCriteria) FROM EvaluationCriteria evaluationCriteria WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "evaluationCriteria.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No EvaluationCriteria exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No EvaluationCriteria exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(EvaluationCriteriaPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"date"
			});
	private static EvaluationCriteria _nullEvaluationCriteria = new EvaluationCriteriaImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<EvaluationCriteria> toCacheModel() {
				return _nullEvaluationCriteriaCacheModel;
			}
		};

	private static CacheModel<EvaluationCriteria> _nullEvaluationCriteriaCacheModel =
		new CacheModel<EvaluationCriteria>() {
			@Override
			public EvaluationCriteria toEntityModel() {
				return _nullEvaluationCriteria;
			}
		};
}