/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model.impl;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSON;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.util.PortalUtil;

import com.liferay.portlet.expando.model.ExpandoBridge;
import com.liferay.portlet.expando.util.ExpandoBridgeFactoryUtil;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengeModel;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengeSoap;

import java.io.Serializable;

import java.sql.Types;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The base model implementation for the CLSChallenge service. Represents a row in the &quot;CSL_CLSChallenge&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengeModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link CLSChallengeImpl}.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSChallengeImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge
 * @see it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengeModel
 * @generated
 */
@JSON(strict = true)
public class CLSChallengeModelImpl extends BaseModelImpl<CLSChallenge>
	implements CLSChallengeModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a c l s challenge model instance should use the {@link it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge} interface instead.
	 */
	public static final String TABLE_NAME = "CSL_CLSChallenge";
	public static final Object[][] TABLE_COLUMNS = {
			{ "uuid_", Types.VARCHAR },
			{ "challengeId", Types.BIGINT },
			{ "challengeTitle", Types.VARCHAR },
			{ "challengeDescription", Types.CLOB },
			{ "challengeWithReward", Types.BOOLEAN },
			{ "challengeReward", Types.CLOB },
			{ "numberOfSelectableIdeas", Types.INTEGER },
			{ "dateAdded", Types.TIMESTAMP },
			{ "challengeStatus", Types.VARCHAR },
			{ "status", Types.INTEGER },
			{ "statusByUserId", Types.BIGINT },
			{ "statusByUserName", Types.VARCHAR },
			{ "statusDate", Types.TIMESTAMP },
			{ "companyId", Types.BIGINT },
			{ "groupId", Types.BIGINT },
			{ "userId", Types.BIGINT },
			{ "dateEnd", Types.TIMESTAMP },
			{ "dateStart", Types.TIMESTAMP },
			{ "language", Types.VARCHAR },
			{ "challengeHashTag", Types.VARCHAR },
			{ "dmFolderName", Types.VARCHAR },
			{ "idFolder", Types.BIGINT },
			{ "representativeImgUrl", Types.VARCHAR },
			{ "calendarBooking", Types.BIGINT }
		};
	public static final String TABLE_SQL_CREATE = "create table CSL_CLSChallenge (uuid_ VARCHAR(75) null,challengeId LONG not null primary key,challengeTitle STRING null,challengeDescription TEXT null,challengeWithReward BOOLEAN,challengeReward TEXT null,numberOfSelectableIdeas INTEGER,dateAdded DATE null,challengeStatus VARCHAR(75) null,status INTEGER,statusByUserId LONG,statusByUserName VARCHAR(75) null,statusDate DATE null,companyId LONG,groupId LONG,userId LONG,dateEnd DATE null,dateStart DATE null,language STRING null,challengeHashTag VARCHAR(75) null,dmFolderName STRING null,idFolder LONG,representativeImgUrl STRING null,calendarBooking LONG)";
	public static final String TABLE_SQL_DROP = "drop table CSL_CLSChallenge";
	public static final String ORDER_BY_JPQL = " ORDER BY clsChallenge.challengeId DESC, clsChallenge.dateAdded DESC";
	public static final String ORDER_BY_SQL = " ORDER BY CSL_CLSChallenge.challengeId DESC, CSL_CLSChallenge.dateAdded DESC";
	public static final String DATA_SOURCE = "liferayDataSource";
	public static final String SESSION_FACTORY = "liferaySessionFactory";
	public static final String TX_MANAGER = "liferayTransactionManager";
	public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.entity.cache.enabled.it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge"),
			true);
	public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.finder.cache.enabled.it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge"),
			true);
	public static final boolean COLUMN_BITMASK_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.column.bitmask.enabled.it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge"),
			true);
	public static long CHALLENGETITLE_COLUMN_BITMASK = 1L;
	public static long COMPANYID_COLUMN_BITMASK = 2L;
	public static long GROUPID_COLUMN_BITMASK = 4L;
	public static long USERID_COLUMN_BITMASK = 8L;
	public static long UUID_COLUMN_BITMASK = 16L;
	public static long CHALLENGEID_COLUMN_BITMASK = 32L;
	public static long DATEADDED_COLUMN_BITMASK = 64L;

	/**
	 * Converts the soap model instance into a normal model instance.
	 *
	 * @param soapModel the soap model instance to convert
	 * @return the normal model instance
	 */
	public static CLSChallenge toModel(CLSChallengeSoap soapModel) {
		if (soapModel == null) {
			return null;
		}

		CLSChallenge model = new CLSChallengeImpl();

		model.setUuid(soapModel.getUuid());
		model.setChallengeId(soapModel.getChallengeId());
		model.setChallengeTitle(soapModel.getChallengeTitle());
		model.setChallengeDescription(soapModel.getChallengeDescription());
		model.setChallengeWithReward(soapModel.getChallengeWithReward());
		model.setChallengeReward(soapModel.getChallengeReward());
		model.setNumberOfSelectableIdeas(soapModel.getNumberOfSelectableIdeas());
		model.setDateAdded(soapModel.getDateAdded());
		model.setChallengeStatus(soapModel.getChallengeStatus());
		model.setStatus(soapModel.getStatus());
		model.setStatusByUserId(soapModel.getStatusByUserId());
		model.setStatusByUserName(soapModel.getStatusByUserName());
		model.setStatusDate(soapModel.getStatusDate());
		model.setCompanyId(soapModel.getCompanyId());
		model.setGroupId(soapModel.getGroupId());
		model.setUserId(soapModel.getUserId());
		model.setDateEnd(soapModel.getDateEnd());
		model.setDateStart(soapModel.getDateStart());
		model.setLanguage(soapModel.getLanguage());
		model.setChallengeHashTag(soapModel.getChallengeHashTag());
		model.setDmFolderName(soapModel.getDmFolderName());
		model.setIdFolder(soapModel.getIdFolder());
		model.setRepresentativeImgUrl(soapModel.getRepresentativeImgUrl());
		model.setCalendarBooking(soapModel.getCalendarBooking());

		return model;
	}

	/**
	 * Converts the soap model instances into normal model instances.
	 *
	 * @param soapModels the soap model instances to convert
	 * @return the normal model instances
	 */
	public static List<CLSChallenge> toModels(CLSChallengeSoap[] soapModels) {
		if (soapModels == null) {
			return null;
		}

		List<CLSChallenge> models = new ArrayList<CLSChallenge>(soapModels.length);

		for (CLSChallengeSoap soapModel : soapModels) {
			models.add(toModel(soapModel));
		}

		return models;
	}

	public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
				"lock.expiration.time.it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge"));

	public CLSChallengeModelImpl() {
	}

	@Override
	public long getPrimaryKey() {
		return _challengeId;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setChallengeId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _challengeId;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Class<?> getModelClass() {
		return CLSChallenge.class;
	}

	@Override
	public String getModelClassName() {
		return CLSChallenge.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("challengeId", getChallengeId());
		attributes.put("challengeTitle", getChallengeTitle());
		attributes.put("challengeDescription", getChallengeDescription());
		attributes.put("challengeWithReward", getChallengeWithReward());
		attributes.put("challengeReward", getChallengeReward());
		attributes.put("numberOfSelectableIdeas", getNumberOfSelectableIdeas());
		attributes.put("dateAdded", getDateAdded());
		attributes.put("challengeStatus", getChallengeStatus());
		attributes.put("status", getStatus());
		attributes.put("statusByUserId", getStatusByUserId());
		attributes.put("statusByUserName", getStatusByUserName());
		attributes.put("statusDate", getStatusDate());
		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("userId", getUserId());
		attributes.put("dateEnd", getDateEnd());
		attributes.put("dateStart", getDateStart());
		attributes.put("language", getLanguage());
		attributes.put("challengeHashTag", getChallengeHashTag());
		attributes.put("dmFolderName", getDmFolderName());
		attributes.put("idFolder", getIdFolder());
		attributes.put("representativeImgUrl", getRepresentativeImgUrl());
		attributes.put("calendarBooking", getCalendarBooking());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long challengeId = (Long)attributes.get("challengeId");

		if (challengeId != null) {
			setChallengeId(challengeId);
		}

		String challengeTitle = (String)attributes.get("challengeTitle");

		if (challengeTitle != null) {
			setChallengeTitle(challengeTitle);
		}

		String challengeDescription = (String)attributes.get(
				"challengeDescription");

		if (challengeDescription != null) {
			setChallengeDescription(challengeDescription);
		}

		Boolean challengeWithReward = (Boolean)attributes.get(
				"challengeWithReward");

		if (challengeWithReward != null) {
			setChallengeWithReward(challengeWithReward);
		}

		String challengeReward = (String)attributes.get("challengeReward");

		if (challengeReward != null) {
			setChallengeReward(challengeReward);
		}

		Integer numberOfSelectableIdeas = (Integer)attributes.get(
				"numberOfSelectableIdeas");

		if (numberOfSelectableIdeas != null) {
			setNumberOfSelectableIdeas(numberOfSelectableIdeas);
		}

		Date dateAdded = (Date)attributes.get("dateAdded");

		if (dateAdded != null) {
			setDateAdded(dateAdded);
		}

		String challengeStatus = (String)attributes.get("challengeStatus");

		if (challengeStatus != null) {
			setChallengeStatus(challengeStatus);
		}

		Integer status = (Integer)attributes.get("status");

		if (status != null) {
			setStatus(status);
		}

		Long statusByUserId = (Long)attributes.get("statusByUserId");

		if (statusByUserId != null) {
			setStatusByUserId(statusByUserId);
		}

		String statusByUserName = (String)attributes.get("statusByUserName");

		if (statusByUserName != null) {
			setStatusByUserName(statusByUserName);
		}

		Date statusDate = (Date)attributes.get("statusDate");

		if (statusDate != null) {
			setStatusDate(statusDate);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Date dateEnd = (Date)attributes.get("dateEnd");

		if (dateEnd != null) {
			setDateEnd(dateEnd);
		}

		Date dateStart = (Date)attributes.get("dateStart");

		if (dateStart != null) {
			setDateStart(dateStart);
		}

		String language = (String)attributes.get("language");

		if (language != null) {
			setLanguage(language);
		}

		String challengeHashTag = (String)attributes.get("challengeHashTag");

		if (challengeHashTag != null) {
			setChallengeHashTag(challengeHashTag);
		}

		String dmFolderName = (String)attributes.get("dmFolderName");

		if (dmFolderName != null) {
			setDmFolderName(dmFolderName);
		}

		Long idFolder = (Long)attributes.get("idFolder");

		if (idFolder != null) {
			setIdFolder(idFolder);
		}

		String representativeImgUrl = (String)attributes.get(
				"representativeImgUrl");

		if (representativeImgUrl != null) {
			setRepresentativeImgUrl(representativeImgUrl);
		}

		Long calendarBooking = (Long)attributes.get("calendarBooking");

		if (calendarBooking != null) {
			setCalendarBooking(calendarBooking);
		}
	}

	@JSON
	@Override
	public String getUuid() {
		if (_uuid == null) {
			return StringPool.BLANK;
		}
		else {
			return _uuid;
		}
	}

	@Override
	public void setUuid(String uuid) {
		if (_originalUuid == null) {
			_originalUuid = _uuid;
		}

		_uuid = uuid;
	}

	public String getOriginalUuid() {
		return GetterUtil.getString(_originalUuid);
	}

	@JSON
	@Override
	public long getChallengeId() {
		return _challengeId;
	}

	@Override
	public void setChallengeId(long challengeId) {
		_columnBitmask = -1L;

		_challengeId = challengeId;
	}

	@JSON
	@Override
	public String getChallengeTitle() {
		if (_challengeTitle == null) {
			return StringPool.BLANK;
		}
		else {
			return _challengeTitle;
		}
	}

	@Override
	public void setChallengeTitle(String challengeTitle) {
		_columnBitmask |= CHALLENGETITLE_COLUMN_BITMASK;

		if (_originalChallengeTitle == null) {
			_originalChallengeTitle = _challengeTitle;
		}

		_challengeTitle = challengeTitle;
	}

	public String getOriginalChallengeTitle() {
		return GetterUtil.getString(_originalChallengeTitle);
	}

	@JSON
	@Override
	public String getChallengeDescription() {
		if (_challengeDescription == null) {
			return StringPool.BLANK;
		}
		else {
			return _challengeDescription;
		}
	}

	@Override
	public void setChallengeDescription(String challengeDescription) {
		_challengeDescription = challengeDescription;
	}

	@JSON
	@Override
	public boolean getChallengeWithReward() {
		return _challengeWithReward;
	}

	@Override
	public boolean isChallengeWithReward() {
		return _challengeWithReward;
	}

	@Override
	public void setChallengeWithReward(boolean challengeWithReward) {
		_challengeWithReward = challengeWithReward;
	}

	@JSON
	@Override
	public String getChallengeReward() {
		if (_challengeReward == null) {
			return StringPool.BLANK;
		}
		else {
			return _challengeReward;
		}
	}

	@Override
	public void setChallengeReward(String challengeReward) {
		_challengeReward = challengeReward;
	}

	@JSON
	@Override
	public int getNumberOfSelectableIdeas() {
		return _numberOfSelectableIdeas;
	}

	@Override
	public void setNumberOfSelectableIdeas(int numberOfSelectableIdeas) {
		_numberOfSelectableIdeas = numberOfSelectableIdeas;
	}

	@JSON
	@Override
	public Date getDateAdded() {
		return _dateAdded;
	}

	@Override
	public void setDateAdded(Date dateAdded) {
		_columnBitmask = -1L;

		_dateAdded = dateAdded;
	}

	@JSON
	@Override
	public String getChallengeStatus() {
		if (_challengeStatus == null) {
			return StringPool.BLANK;
		}
		else {
			return _challengeStatus;
		}
	}

	@Override
	public void setChallengeStatus(String challengeStatus) {
		_challengeStatus = challengeStatus;
	}

	@JSON
	@Override
	public int getStatus() {
		return _status;
	}

	@Override
	public void setStatus(int status) {
		_status = status;
	}

	@JSON
	@Override
	public long getStatusByUserId() {
		return _statusByUserId;
	}

	@Override
	public void setStatusByUserId(long statusByUserId) {
		_statusByUserId = statusByUserId;
	}

	@Override
	public String getStatusByUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getStatusByUserId(), "uuid",
			_statusByUserUuid);
	}

	@Override
	public void setStatusByUserUuid(String statusByUserUuid) {
		_statusByUserUuid = statusByUserUuid;
	}

	@JSON
	@Override
	public String getStatusByUserName() {
		if (_statusByUserName == null) {
			return StringPool.BLANK;
		}
		else {
			return _statusByUserName;
		}
	}

	@Override
	public void setStatusByUserName(String statusByUserName) {
		_statusByUserName = statusByUserName;
	}

	@JSON
	@Override
	public Date getStatusDate() {
		return _statusDate;
	}

	@Override
	public void setStatusDate(Date statusDate) {
		_statusDate = statusDate;
	}

	@JSON
	@Override
	public long getCompanyId() {
		return _companyId;
	}

	@Override
	public void setCompanyId(long companyId) {
		_columnBitmask |= COMPANYID_COLUMN_BITMASK;

		if (!_setOriginalCompanyId) {
			_setOriginalCompanyId = true;

			_originalCompanyId = _companyId;
		}

		_companyId = companyId;
	}

	public long getOriginalCompanyId() {
		return _originalCompanyId;
	}

	@JSON
	@Override
	public long getGroupId() {
		return _groupId;
	}

	@Override
	public void setGroupId(long groupId) {
		_columnBitmask |= GROUPID_COLUMN_BITMASK;

		if (!_setOriginalGroupId) {
			_setOriginalGroupId = true;

			_originalGroupId = _groupId;
		}

		_groupId = groupId;
	}

	public long getOriginalGroupId() {
		return _originalGroupId;
	}

	@JSON
	@Override
	public long getUserId() {
		return _userId;
	}

	@Override
	public void setUserId(long userId) {
		_columnBitmask |= USERID_COLUMN_BITMASK;

		if (!_setOriginalUserId) {
			_setOriginalUserId = true;

			_originalUserId = _userId;
		}

		_userId = userId;
	}

	@Override
	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	@Override
	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public long getOriginalUserId() {
		return _originalUserId;
	}

	@JSON
	@Override
	public Date getDateEnd() {
		return _dateEnd;
	}

	@Override
	public void setDateEnd(Date dateEnd) {
		_dateEnd = dateEnd;
	}

	@JSON
	@Override
	public Date getDateStart() {
		return _dateStart;
	}

	@Override
	public void setDateStart(Date dateStart) {
		_dateStart = dateStart;
	}

	@JSON
	@Override
	public String getLanguage() {
		if (_language == null) {
			return StringPool.BLANK;
		}
		else {
			return _language;
		}
	}

	@Override
	public void setLanguage(String language) {
		_language = language;
	}

	@JSON
	@Override
	public String getChallengeHashTag() {
		if (_challengeHashTag == null) {
			return StringPool.BLANK;
		}
		else {
			return _challengeHashTag;
		}
	}

	@Override
	public void setChallengeHashTag(String challengeHashTag) {
		_challengeHashTag = challengeHashTag;
	}

	@JSON
	@Override
	public String getDmFolderName() {
		if (_dmFolderName == null) {
			return StringPool.BLANK;
		}
		else {
			return _dmFolderName;
		}
	}

	@Override
	public void setDmFolderName(String dmFolderName) {
		_dmFolderName = dmFolderName;
	}

	@JSON
	@Override
	public long getIdFolder() {
		return _idFolder;
	}

	@Override
	public void setIdFolder(long idFolder) {
		_idFolder = idFolder;
	}

	@JSON
	@Override
	public String getRepresentativeImgUrl() {
		if (_representativeImgUrl == null) {
			return StringPool.BLANK;
		}
		else {
			return _representativeImgUrl;
		}
	}

	@Override
	public void setRepresentativeImgUrl(String representativeImgUrl) {
		_representativeImgUrl = representativeImgUrl;
	}

	@JSON
	@Override
	public long getCalendarBooking() {
		return _calendarBooking;
	}

	@Override
	public void setCalendarBooking(long calendarBooking) {
		_calendarBooking = calendarBooking;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #isApproved}
	 */
	@Override
	public boolean getApproved() {
		return isApproved();
	}

	@Override
	public boolean isApproved() {
		if (getStatus() == WorkflowConstants.STATUS_APPROVED) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean isDenied() {
		if (getStatus() == WorkflowConstants.STATUS_DENIED) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean isDraft() {
		if (getStatus() == WorkflowConstants.STATUS_DRAFT) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean isExpired() {
		if (getStatus() == WorkflowConstants.STATUS_EXPIRED) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean isInactive() {
		if (getStatus() == WorkflowConstants.STATUS_INACTIVE) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean isIncomplete() {
		if (getStatus() == WorkflowConstants.STATUS_INCOMPLETE) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean isPending() {
		if (getStatus() == WorkflowConstants.STATUS_PENDING) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean isScheduled() {
		if (getStatus() == WorkflowConstants.STATUS_SCHEDULED) {
			return true;
		}
		else {
			return false;
		}
	}

	public long getColumnBitmask() {
		return _columnBitmask;
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return ExpandoBridgeFactoryUtil.getExpandoBridge(getCompanyId(),
			CLSChallenge.class.getName(), getPrimaryKey());
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		ExpandoBridge expandoBridge = getExpandoBridge();

		expandoBridge.setAttributes(serviceContext);
	}

	@Override
	public CLSChallenge toEscapedModel() {
		if (_escapedModel == null) {
			_escapedModel = (CLSChallenge)ProxyUtil.newProxyInstance(_classLoader,
					_escapedModelInterfaces, new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		CLSChallengeImpl clsChallengeImpl = new CLSChallengeImpl();

		clsChallengeImpl.setUuid(getUuid());
		clsChallengeImpl.setChallengeId(getChallengeId());
		clsChallengeImpl.setChallengeTitle(getChallengeTitle());
		clsChallengeImpl.setChallengeDescription(getChallengeDescription());
		clsChallengeImpl.setChallengeWithReward(getChallengeWithReward());
		clsChallengeImpl.setChallengeReward(getChallengeReward());
		clsChallengeImpl.setNumberOfSelectableIdeas(getNumberOfSelectableIdeas());
		clsChallengeImpl.setDateAdded(getDateAdded());
		clsChallengeImpl.setChallengeStatus(getChallengeStatus());
		clsChallengeImpl.setStatus(getStatus());
		clsChallengeImpl.setStatusByUserId(getStatusByUserId());
		clsChallengeImpl.setStatusByUserName(getStatusByUserName());
		clsChallengeImpl.setStatusDate(getStatusDate());
		clsChallengeImpl.setCompanyId(getCompanyId());
		clsChallengeImpl.setGroupId(getGroupId());
		clsChallengeImpl.setUserId(getUserId());
		clsChallengeImpl.setDateEnd(getDateEnd());
		clsChallengeImpl.setDateStart(getDateStart());
		clsChallengeImpl.setLanguage(getLanguage());
		clsChallengeImpl.setChallengeHashTag(getChallengeHashTag());
		clsChallengeImpl.setDmFolderName(getDmFolderName());
		clsChallengeImpl.setIdFolder(getIdFolder());
		clsChallengeImpl.setRepresentativeImgUrl(getRepresentativeImgUrl());
		clsChallengeImpl.setCalendarBooking(getCalendarBooking());

		clsChallengeImpl.resetOriginalValues();

		return clsChallengeImpl;
	}

	@Override
	public int compareTo(CLSChallenge clsChallenge) {
		int value = 0;

		if (getChallengeId() < clsChallenge.getChallengeId()) {
			value = -1;
		}
		else if (getChallengeId() > clsChallenge.getChallengeId()) {
			value = 1;
		}
		else {
			value = 0;
		}

		value = value * -1;

		if (value != 0) {
			return value;
		}

		value = DateUtil.compareTo(getDateAdded(), clsChallenge.getDateAdded());

		value = value * -1;

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CLSChallenge)) {
			return false;
		}

		CLSChallenge clsChallenge = (CLSChallenge)obj;

		long primaryKey = clsChallenge.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public void resetOriginalValues() {
		CLSChallengeModelImpl clsChallengeModelImpl = this;

		clsChallengeModelImpl._originalUuid = clsChallengeModelImpl._uuid;

		clsChallengeModelImpl._originalChallengeTitle = clsChallengeModelImpl._challengeTitle;

		clsChallengeModelImpl._originalCompanyId = clsChallengeModelImpl._companyId;

		clsChallengeModelImpl._setOriginalCompanyId = false;

		clsChallengeModelImpl._originalGroupId = clsChallengeModelImpl._groupId;

		clsChallengeModelImpl._setOriginalGroupId = false;

		clsChallengeModelImpl._originalUserId = clsChallengeModelImpl._userId;

		clsChallengeModelImpl._setOriginalUserId = false;

		clsChallengeModelImpl._columnBitmask = 0;
	}

	@Override
	public CacheModel<CLSChallenge> toCacheModel() {
		CLSChallengeCacheModel clsChallengeCacheModel = new CLSChallengeCacheModel();

		clsChallengeCacheModel.uuid = getUuid();

		String uuid = clsChallengeCacheModel.uuid;

		if ((uuid != null) && (uuid.length() == 0)) {
			clsChallengeCacheModel.uuid = null;
		}

		clsChallengeCacheModel.challengeId = getChallengeId();

		clsChallengeCacheModel.challengeTitle = getChallengeTitle();

		String challengeTitle = clsChallengeCacheModel.challengeTitle;

		if ((challengeTitle != null) && (challengeTitle.length() == 0)) {
			clsChallengeCacheModel.challengeTitle = null;
		}

		clsChallengeCacheModel.challengeDescription = getChallengeDescription();

		String challengeDescription = clsChallengeCacheModel.challengeDescription;

		if ((challengeDescription != null) &&
				(challengeDescription.length() == 0)) {
			clsChallengeCacheModel.challengeDescription = null;
		}

		clsChallengeCacheModel.challengeWithReward = getChallengeWithReward();

		clsChallengeCacheModel.challengeReward = getChallengeReward();

		String challengeReward = clsChallengeCacheModel.challengeReward;

		if ((challengeReward != null) && (challengeReward.length() == 0)) {
			clsChallengeCacheModel.challengeReward = null;
		}

		clsChallengeCacheModel.numberOfSelectableIdeas = getNumberOfSelectableIdeas();

		Date dateAdded = getDateAdded();

		if (dateAdded != null) {
			clsChallengeCacheModel.dateAdded = dateAdded.getTime();
		}
		else {
			clsChallengeCacheModel.dateAdded = Long.MIN_VALUE;
		}

		clsChallengeCacheModel.challengeStatus = getChallengeStatus();

		String challengeStatus = clsChallengeCacheModel.challengeStatus;

		if ((challengeStatus != null) && (challengeStatus.length() == 0)) {
			clsChallengeCacheModel.challengeStatus = null;
		}

		clsChallengeCacheModel.status = getStatus();

		clsChallengeCacheModel.statusByUserId = getStatusByUserId();

		clsChallengeCacheModel.statusByUserName = getStatusByUserName();

		String statusByUserName = clsChallengeCacheModel.statusByUserName;

		if ((statusByUserName != null) && (statusByUserName.length() == 0)) {
			clsChallengeCacheModel.statusByUserName = null;
		}

		Date statusDate = getStatusDate();

		if (statusDate != null) {
			clsChallengeCacheModel.statusDate = statusDate.getTime();
		}
		else {
			clsChallengeCacheModel.statusDate = Long.MIN_VALUE;
		}

		clsChallengeCacheModel.companyId = getCompanyId();

		clsChallengeCacheModel.groupId = getGroupId();

		clsChallengeCacheModel.userId = getUserId();

		Date dateEnd = getDateEnd();

		if (dateEnd != null) {
			clsChallengeCacheModel.dateEnd = dateEnd.getTime();
		}
		else {
			clsChallengeCacheModel.dateEnd = Long.MIN_VALUE;
		}

		Date dateStart = getDateStart();

		if (dateStart != null) {
			clsChallengeCacheModel.dateStart = dateStart.getTime();
		}
		else {
			clsChallengeCacheModel.dateStart = Long.MIN_VALUE;
		}

		clsChallengeCacheModel.language = getLanguage();

		String language = clsChallengeCacheModel.language;

		if ((language != null) && (language.length() == 0)) {
			clsChallengeCacheModel.language = null;
		}

		clsChallengeCacheModel.challengeHashTag = getChallengeHashTag();

		String challengeHashTag = clsChallengeCacheModel.challengeHashTag;

		if ((challengeHashTag != null) && (challengeHashTag.length() == 0)) {
			clsChallengeCacheModel.challengeHashTag = null;
		}

		clsChallengeCacheModel.dmFolderName = getDmFolderName();

		String dmFolderName = clsChallengeCacheModel.dmFolderName;

		if ((dmFolderName != null) && (dmFolderName.length() == 0)) {
			clsChallengeCacheModel.dmFolderName = null;
		}

		clsChallengeCacheModel.idFolder = getIdFolder();

		clsChallengeCacheModel.representativeImgUrl = getRepresentativeImgUrl();

		String representativeImgUrl = clsChallengeCacheModel.representativeImgUrl;

		if ((representativeImgUrl != null) &&
				(representativeImgUrl.length() == 0)) {
			clsChallengeCacheModel.representativeImgUrl = null;
		}

		clsChallengeCacheModel.calendarBooking = getCalendarBooking();

		return clsChallengeCacheModel;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(49);

		sb.append("{uuid=");
		sb.append(getUuid());
		sb.append(", challengeId=");
		sb.append(getChallengeId());
		sb.append(", challengeTitle=");
		sb.append(getChallengeTitle());
		sb.append(", challengeDescription=");
		sb.append(getChallengeDescription());
		sb.append(", challengeWithReward=");
		sb.append(getChallengeWithReward());
		sb.append(", challengeReward=");
		sb.append(getChallengeReward());
		sb.append(", numberOfSelectableIdeas=");
		sb.append(getNumberOfSelectableIdeas());
		sb.append(", dateAdded=");
		sb.append(getDateAdded());
		sb.append(", challengeStatus=");
		sb.append(getChallengeStatus());
		sb.append(", status=");
		sb.append(getStatus());
		sb.append(", statusByUserId=");
		sb.append(getStatusByUserId());
		sb.append(", statusByUserName=");
		sb.append(getStatusByUserName());
		sb.append(", statusDate=");
		sb.append(getStatusDate());
		sb.append(", companyId=");
		sb.append(getCompanyId());
		sb.append(", groupId=");
		sb.append(getGroupId());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", dateEnd=");
		sb.append(getDateEnd());
		sb.append(", dateStart=");
		sb.append(getDateStart());
		sb.append(", language=");
		sb.append(getLanguage());
		sb.append(", challengeHashTag=");
		sb.append(getChallengeHashTag());
		sb.append(", dmFolderName=");
		sb.append(getDmFolderName());
		sb.append(", idFolder=");
		sb.append(getIdFolder());
		sb.append(", representativeImgUrl=");
		sb.append(getRepresentativeImgUrl());
		sb.append(", calendarBooking=");
		sb.append(getCalendarBooking());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(76);

		sb.append("<model><model-name>");
		sb.append(
			"it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>uuid</column-name><column-value><![CDATA[");
		sb.append(getUuid());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>challengeId</column-name><column-value><![CDATA[");
		sb.append(getChallengeId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>challengeTitle</column-name><column-value><![CDATA[");
		sb.append(getChallengeTitle());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>challengeDescription</column-name><column-value><![CDATA[");
		sb.append(getChallengeDescription());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>challengeWithReward</column-name><column-value><![CDATA[");
		sb.append(getChallengeWithReward());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>challengeReward</column-name><column-value><![CDATA[");
		sb.append(getChallengeReward());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>numberOfSelectableIdeas</column-name><column-value><![CDATA[");
		sb.append(getNumberOfSelectableIdeas());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dateAdded</column-name><column-value><![CDATA[");
		sb.append(getDateAdded());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>challengeStatus</column-name><column-value><![CDATA[");
		sb.append(getChallengeStatus());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>status</column-name><column-value><![CDATA[");
		sb.append(getStatus());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>statusByUserId</column-name><column-value><![CDATA[");
		sb.append(getStatusByUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>statusByUserName</column-name><column-value><![CDATA[");
		sb.append(getStatusByUserName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>statusDate</column-name><column-value><![CDATA[");
		sb.append(getStatusDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>companyId</column-name><column-value><![CDATA[");
		sb.append(getCompanyId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>groupId</column-name><column-value><![CDATA[");
		sb.append(getGroupId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dateEnd</column-name><column-value><![CDATA[");
		sb.append(getDateEnd());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dateStart</column-name><column-value><![CDATA[");
		sb.append(getDateStart());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>language</column-name><column-value><![CDATA[");
		sb.append(getLanguage());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>challengeHashTag</column-name><column-value><![CDATA[");
		sb.append(getChallengeHashTag());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dmFolderName</column-name><column-value><![CDATA[");
		sb.append(getDmFolderName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>idFolder</column-name><column-value><![CDATA[");
		sb.append(getIdFolder());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>representativeImgUrl</column-name><column-value><![CDATA[");
		sb.append(getRepresentativeImgUrl());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>calendarBooking</column-name><column-value><![CDATA[");
		sb.append(getCalendarBooking());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private static ClassLoader _classLoader = CLSChallenge.class.getClassLoader();
	private static Class<?>[] _escapedModelInterfaces = new Class[] {
			CLSChallenge.class
		};
	private String _uuid;
	private String _originalUuid;
	private long _challengeId;
	private String _challengeTitle;
	private String _originalChallengeTitle;
	private String _challengeDescription;
	private boolean _challengeWithReward;
	private String _challengeReward;
	private int _numberOfSelectableIdeas;
	private Date _dateAdded;
	private String _challengeStatus;
	private int _status;
	private long _statusByUserId;
	private String _statusByUserUuid;
	private String _statusByUserName;
	private Date _statusDate;
	private long _companyId;
	private long _originalCompanyId;
	private boolean _setOriginalCompanyId;
	private long _groupId;
	private long _originalGroupId;
	private boolean _setOriginalGroupId;
	private long _userId;
	private String _userUuid;
	private long _originalUserId;
	private boolean _setOriginalUserId;
	private Date _dateEnd;
	private Date _dateStart;
	private String _language;
	private String _challengeHashTag;
	private String _dmFolderName;
	private long _idFolder;
	private String _representativeImgUrl;
	private long _calendarBooking;
	private long _columnBitmask;
	private CLSChallenge _escapedModel;
}