/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.impl;

import it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCoworker;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSCoworkerLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteIdeasLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.base.CLSIdeaLocalServiceBaseImpl;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSIdeaUtil;
import it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties;
import it.eng.rspa.ideas.utils.ChallengesUtils;
import it.eng.rspa.ideas.utils.IdeasListUtils;
import it.eng.rspa.ideas.utils.MailUtil;
import it.eng.rspa.ideas.utils.MyConstants;
import it.eng.rspa.ideas.utils.MyUtils;
import it.eng.rspa.ideas.utils.NeedUtils;
import it.eng.rspa.ideas.utils.RTBF;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.portlet.ActionRequest;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.notifications.NotificationEvent;
import com.liferay.portal.kernel.notifications.NotificationEventFactoryUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.model.Company;
import com.liferay.portal.model.User;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.service.UserNotificationEventLocalServiceUtil;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;

/**
 * The implementation of the c l s idea local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.base.CLSIdeaLocalServiceBaseImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil
 */


/**
 * @author UTENTE
 *
 */
public class CLSIdeaLocalServiceImpl extends CLSIdeaLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil} to access the c l s idea local service.
	 */
	

	
	public void sendNotificationForTransition(ResourceBundle res,long ideaId, String transictionName, ActionRequest actionRequest) {
		
		boolean reducedLifecycle = IdeaManagementSystemProperties.getEnabledProperty("reducedLifecycle");
		ThemeDisplay themeD = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String senderName = themeD.getUser().getFullName();
		
		boolean notificationCoworker = true;
		boolean notificationUserFavourite = true;
		boolean notificationMunicipality = false;
		boolean notificationAuthor = true;
		
		if (reducedLifecycle)
			notificationAuthor=false;
		
		 String congrat="Congratulations!";
		 String ideaS="The idea";
		 String needS="The Need Report";
		 String passedInEvaluationS="passed in evaluation phase";
		 String selectedS="has been selected";
		 String passedInRefinementS="passed in the refinement phase";
		 String rejected="has been rejected";
		 
		 String passedImplementationS="passed in the implementation phase";
		 String passedMonitoringS="passed in the monitoring phase";
		 String thelifecycleS="The lifecycle of the idea";
		 String concludedS="has been concluded";
		 String associatedTo="has been associated with the challenge";
		 String refinementOfIdea="The refinement of the idea ";
		 String reopenedS="has been reopened";
		 String notificationLinkIdeaToChallengeFromNeedS=" is addressed to this need report.\nSo now you have the opportunity to associate it to this challenge.\nOn the idea detail page, you can use the button 'Choose a challenge' by the menu 'MANAGE'.\n\nRemember that an idea can be associated exclusively to a single challenge.\nIf you do not associate it with this challenge and in the future a new challenge will be created by this need report, you will receive a new notification.\nIf you decide to associate your idea to this challenge, in the future you can not associate it with any other challenge";
		 String itWasTakenUpByTheCompany="It was taken over by the company";
		 

		try {
			 congrat=new String(res.getString("ims.congratulations") .getBytes("ISO-8859-1"), "UTF-8");
			 ideaS = new String(res.getString("ims.idea") .getBytes("ISO-8859-1"), "UTF-8");
			 needS = new String(res.getString("ims.the-need-report") .getBytes("ISO-8859-1"), "UTF-8");
			 passedInRefinementS = new String(res.getString("ims.passed-in-refinement-phase") .getBytes("ISO-8859-1"), "UTF-8");
			 passedInEvaluationS = new String(res.getString("ims.passed-in-evaluation-phase") .getBytes("ISO-8859-1"), "UTF-8");
			 rejected = new String(res.getString("ims.has-been-rejected") .getBytes("ISO-8859-1"), "UTF-8");
			 passedImplementationS = new String(res.getString("ims.passed-in-implementation-phase") .getBytes("ISO-8859-1"), "UTF-8");
			 selectedS= new String(res.getString("ims.has-been-selected") .getBytes("ISO-8859-1"), "UTF-8");
			 passedMonitoringS = new String(res.getString("ims.passed-in-monitoring-phase") .getBytes("ISO-8859-1"), "UTF-8");
			 thelifecycleS = new String(res.getString("ims.idea-monitoring") .getBytes("ISO-8859-1"), "UTF-8");
			 concludedS = new String(res.getString("ims.monitoring-concluded") .getBytes("ISO-8859-1"), "UTF-8");
			 associatedTo = new String(res.getString("ims.has-been-associated-with-the-challenge") .getBytes("ISO-8859-1"), "UTF-8");
			 refinementOfIdea = new String(res.getString("ims.the-refinement-of-the-idea") .getBytes("ISO-8859-1"), "UTF-8");
			 reopenedS = new String(res.getString("ims.has-been-reopened") .getBytes("ISO-8859-1"), "UTF-8");
			 notificationLinkIdeaToChallengeFromNeedS = new String(res.getString("ims.notification-link-idea-to-challenge-from-need") .getBytes("ISO-8859-1"), "UTF-8");
			 itWasTakenUpByTheCompany = new String(res.getString("ims.it-was-taken-up-by-the-company") .getBytes("ISO-8859-1"), "UTF-8");
			 
		} catch (UnsupportedEncodingException e1) {
			
			e1.printStackTrace();
		}
		
		CLSIdea idea = null;
		try {
			idea = CLSIdeaLocalServiceUtil.fetchCLSIdea(ideaId);
		} catch (SystemException e) {
			
			e.printStackTrace();
		}
		
		List<CLSCoworker> coworkers = new ArrayList<CLSCoworker>();
		try {
			coworkers = CLSCoworkerLocalServiceUtil.getCoworkersByIdeaId(ideaId);
		} catch (SystemException e1) {
			
			e1.printStackTrace();
		}
		
		
		List<CLSFavouriteIdeas> favouriteIdeasEntries = new ArrayList<CLSFavouriteIdeas>();
		 try {
			favouriteIdeasEntries = CLSFavouriteIdeasLocalServiceUtil.getFavouriteIdeasEntriesByIdeaId(ideaId);
		} catch (SystemException e1) {
			e1.printStackTrace();
		}
		
		//I build the string for the email body 
		
		String textMessage = "";
		
		if(transictionName.equals(MyConstants.IDEA_STATE_REFINEMENT) || transictionName.equals("takeCharge") || transictionName.equals("fbRefinement") ){
			textMessage = ideaS + " '" + idea.getIdeaTitle() + "' " + passedInRefinementS;
			
			
			if (transictionName.equals("takeCharge")){
			
				String companyName = MyUtils.getOrganizationNameByLeaderId(themeD.getUser().getUserId());
				textMessage+= "\n"+itWasTakenUpByTheCompany +" "+ companyName;
			
			}
			
		}else if(transictionName.equals(MyConstants.IDEA_STATE_EVALUATION)){
			textMessage = ideaS + " '" + idea.getIdeaTitle() + "' " + passedInEvaluationS;
		}else if(transictionName.equals(MyConstants.IDEA_STATE_SELECTED)){
			textMessage = ideaS + " '" + idea.getIdeaTitle() + "' " + selectedS;
		}else if(transictionName.equals(MyConstants.IDEA_STATE_REJECT)){
			textMessage = ideaS + " '" + idea.getIdeaTitle() + "' " + rejected;
		}else if(transictionName.equals(MyConstants.IDEA_STATE_IMPLEMENTATION)){
			textMessage =congrat+" "+ ideaS + " '" + idea.getIdeaTitle() + "' " + passedImplementationS;
		}else if(transictionName.equals(MyConstants.IDEA_STATE_MONITORING)){
			textMessage = congrat+" "+ideaS + " '" + idea.getIdeaTitle() + "' " + passedMonitoringS;
		}else if(transictionName.equals(MyConstants.IDEA_STATE_COMPLETED)){
			textMessage = thelifecycleS + " '" + idea.getIdeaTitle() + "' " + concludedS;
		}else if(  transictionName.equals("associateChallengeByAuthor") ||    transictionName.equals("associateChallengeByMunicipality")  ){
			textMessage = ideaS + " '" + idea.getIdeaTitle() + "' " + associatedTo+ " '" + IdeasListUtils.getChallengeNameByIdeaId(ideaId)+ "'.";
			
			if(  transictionName.equals("associateChallengeByAuthor"))
					notificationMunicipality = true;
			
		}else if(  transictionName.equals("challengeFromNeed")  ){	
			
			long challengeId = (long)actionRequest.getAttribute("challengeForNeed");
			textMessage = needS + " '" + idea.getIdeaTitle() + "' " + associatedTo+ " '" + ChallengesUtils.getChallengeNameByChallengeId(challengeId)+ "'";
			
			
		}else if(  transictionName.equals("linkIdeaToChallengeFromNeed")  ){	
			
			long challengeId = (long)actionRequest.getAttribute("challengeForNeed");
			
			String needName = IdeasListUtils.getNeedNameByIdea(idea);
			textMessage = needS + " '" + needName + "' " + associatedTo+ " '" + ChallengesUtils.getChallengeNameByChallengeId(challengeId)+ "'. ";
			
			String thirdRow= "\n"+ ideaS+ " '"+ idea.getIdeaTitle()+ "' " + notificationLinkIdeaToChallengeFromNeedS;
					
			textMessage= textMessage+thirdRow;
			
			notificationCoworker = false;
			notificationUserFavourite = false;
			
		}else if(  transictionName.equals("refinementActivitiesFinished") ){
			textMessage = refinementOfIdea + " '" + idea.getIdeaTitle() + "' " + concludedS;
			
			if (reducedLifecycle){
				notificationAuthor=true;
			}else{
				notificationMunicipality = true;
			}
			
		}else if(  transictionName.equals("refinementReopenActivities") ){
			textMessage = refinementOfIdea + " '" + idea.getIdeaTitle() + "' " + reopenedS;
		}
		

		if (notificationCoworker){
		
			//send notification to coworkers
			for (CLSCoworker cw:coworkers){
				long selectedUserId = cw.getUserId();
				
				if (selectedUserId !=themeD.getUser().getUserId()){
					try {
						CLSIdeaLocalServiceUtil.sendNotification(res,textMessage, selectedUserId, senderName, ideaId, actionRequest, "passaggioDiStato");
					} catch (PortalException | SystemException e) {
						e.printStackTrace();
					}
				}
			}
		}
		

		if (notificationUserFavourite){
		
			//send notification to those who have marked as favorite
			for (CLSFavouriteIdeas fe:favouriteIdeasEntries){
				
				long selectedUserId = fe.getUserId();
				try {
					CLSIdeaLocalServiceUtil.sendNotification(res,textMessage, selectedUserId, senderName, ideaId, actionRequest,"passaggioDiStato");
				} catch (PortalException | SystemException e) {
					
					e.printStackTrace();
				}
			}
		}
		
		
		
		
		//sends the notification to the author of the reference challemge only if the action has not done by him
		if( notificationMunicipality){
			
			long municipalityId = idea.getMunicipalityId();
			if(municipalityId>0){
				//allora invio al titolare della gara
				try {
					CLSIdeaLocalServiceUtil.sendNotification(res,textMessage, municipalityId, senderName, ideaId, actionRequest, "passaggioDiStato");
				} catch (PortalException | SystemException e) {
					
					e.printStackTrace();
				}
			}//if
			
		}else{//sends the notification to the author of the idea only if the action has not done by him
			
			if (notificationAuthor){
				try {
					CLSIdeaLocalServiceUtil.sendNotification(res,textMessage, idea.getUserId(), senderName, ideaId, actionRequest,"passaggioDiStato");
				} catch (PortalException | SystemException e) {
					
					e.printStackTrace();
				}
			}
		}		
		
	}
	
	/**
	 * Send notifications in dockbar and emails 
	 * 
	 * @param  azione Fase in cui avviene la notifica (puo' assumere: "nuovaIdea", "delete", "passaggioDiStato", "aggiuntaCollaboratore", "modificata" )
	 * 
	 */ 
	
	public void sendNotification(ResourceBundle res,String textMessage, long destinationUderId, String senderUserName, 
			long ideaId, ActionRequest actionRequest, String azione) throws PortalException, SystemException{
		
		
		boolean emailNotificationsEnabled = IdeaManagementSystemProperties.getEnabledProperty("emailNotificationsEnabled");
		boolean dockbarNotificationsEnabled = IdeaManagementSystemProperties.getEnabledProperty("dockbarNotificationsEnabled");
		
		User destinationUser = UserLocalServiceUtil.fetchUser(destinationUderId);
		
		String senderName = IdeaManagementSystemProperties.getProperty("senderNotificheMailIdeario"); //"IMS";
		
		String oggettoMail = LanguageUtil.get(destinationUser.getLocale(), "ims.open-innovation-area");
		//String oggettoMail = IdeaManagementSystemProperties.getProperty("oggettoNotificheMailIdeario"); //"Idea Management System";
		
		String utenzaMail = IdeaManagementSystemProperties.getProperty("utenzaMail"); //"openinnovationarea";
		
		
		JSONObject notificationEventJSONObject = JSONFactoryUtil.createJSONObject();
		
		String textMessageNotifica = textMessage;			
		
		ThemeDisplay td  = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String lfu = PortalUtil.getLayoutFriendlyURL(td.getLayout(), td); //http://localhost:8080/web/guest/innovation-area	
		String urlDettaglio =  IdeaManagementSystemProperties.getFriendlyUrlIdeas(ideaId);
		 
		boolean isNeed = IdeasListUtils.isNeedByIdeaId(ideaId);		
		
		if (isNeed)
			 urlDettaglio = IdeaManagementSystemProperties.getFriendlyUrlNeeds(ideaId);
		else
			 urlDettaglio = IdeaManagementSystemProperties.getFriendlyUrlIdeas(ideaId);
		 
		notificationEventJSONObject.put("text-message",textMessageNotifica);
		notificationEventJSONObject.put("userId",destinationUderId);
		notificationEventJSONObject.put("ideaId",ideaId);
		notificationEventJSONObject.put("sender",senderUserName);
		notificationEventJSONObject.put("classPK", ideaId);
		
		String goTo = "";
		String goToidea = "Go to idea";
		String goToNeed = "Go to need";
		if (azione.equalsIgnoreCase("delete")){
			notificationEventJSONObject.put("currentURL", lfu );
			goToidea = "";//If I'm on deletion, I don't have link to show
		}else{
			notificationEventJSONObject.put("currentURL", urlDettaglio );
			
			try {
				 goToidea = new String(res.getString("ims.go-to-idea") .getBytes("ISO-8859-1"), "UTF-8");
				 goToNeed = new String(res.getString("ims.go-to-need") .getBytes("ISO-8859-1"), "UTF-8");
			} catch (UnsupportedEncodingException e1) {
				
				e1.printStackTrace();
			}
			
			if (isNeed)
				goTo=goToNeed+" "+urlDettaglio;
			else
				goTo = goToidea+" "+urlDettaglio;
			
		}
		
		if (dockbarNotificationsEnabled){
			NotificationEvent notificationEvent = NotificationEventFactoryUtil.createNotificationEvent(System.currentTimeMillis(),"Ideas_WAR_Challenge62portlet", notificationEventJSONObject);
			notificationEvent.setDeliveryRequired(0);
			UserNotificationEventLocalServiceUtil.addUserNotificationEvent(destinationUderId,notificationEvent);
		}
		
		
		Company  company = CompanyLocalServiceUtil.fetchCompany(destinationUser.getCompanyId());
		
		
		String dearUser="Dear user";
		String regards= "Regards";
		
		try {
			 dearUser = new String(res.getString("ims.dear-user").getBytes("ISO-8859-1"), "UTF-8");
			 regards = new String(res.getString("ims.regards").getBytes("ISO-8859-1"), "UTF-8");
		} catch (UnsupportedEncodingException e1) {
			
			e1.printStackTrace();
		}

		
		String fromMail = utenzaMail+"@" + company.getVirtualHostname();
		String textMail = dearUser+" "+destinationUser.getFullName()+",\n\n" + textMessage+".";	
		textMail += "\n"+goTo;
		textMail += "\n\n"+regards+"\n";	
		textMail += senderName+"\n";;
		textMail += IdeaManagementSystemProperties.getRootUrl();
		
		
		if (emailNotificationsEnabled){
			try {
				MailUtil.sendMail(fromMail, senderName , destinationUser.getEmailAddress(), destinationUser.getFullName(), oggettoMail, textMail, false);
			} catch (UnsupportedEncodingException e) {
				
				e.printStackTrace();
			}
		}
	
	}//END method
	
	
	
	/*****************************************************/
	
	
	public List<CLSIdea> getIdeasByUserId(long userId) throws SystemException{
		return CLSIdeaUtil.findByIdeaUserId(userId);
	}
	
	public CLSIdea getIdeasByIdeaId(long ideaId) throws SystemException{
		try {
			return CLSIdeaUtil.findByIdeaId(ideaId);
		} catch (NoSuchCLSIdeaException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<CLSIdea> getIdeasByChallengeId(long challengeId) throws SystemException{
		
			return CLSIdeaUtil.findByIdeaChallengeId(challengeId);

	}
	
	//per la municipalita
	public List<CLSIdea> getIdeasByMunicipalityId(long municipalityId) throws SystemException{
			return CLSIdeaUtil.findByIdeaMunicipalityId(municipalityId);
	}
	
	
	//per la organizzazione di municipalita
	public List<CLSIdea> getIdeasByMunicipalityOrganizationId(long municipalityOrganizationId) throws SystemException{
			return CLSIdeaUtil.findByIdeaMunicipalityOrganizationId(municipalityOrganizationId);
	}
	
	
	//per need
	public List<CLSIdea> getIdeasByNeedId(long needId) throws SystemException{
				return CLSIdeaUtil.findByIdeaNeedId(needId);
	}
	
	
	/** 
	 * Return a list of all Needs
	 * @return
	 * @throws SystemException
	 */
	public List<CLSIdea> getNeeds() throws SystemException{
		
		List<CLSIdea> allNeeds = new ArrayList<CLSIdea>();
		
		List<CLSIdea> allIdeas =  CLSIdeaUtil.findAll();
		
		for (CLSIdea idea : allIdeas){
			
			if (idea.getIsNeed())
				allNeeds.add(idea);
			
		}
		
		
		return allNeeds;
	}	
		
	
	/**
	 * Return a list of all Needs of the Municipality Organization
	 * @param userId
	 * @return
	 * @throws SystemException
	 */
	public List<CLSIdea> getNeedsByMunicipalityOrganizationId(long municipalityOrganizationId) throws SystemException{
		
		List<CLSIdea> allNeeds = new ArrayList<CLSIdea>();
		
		List<CLSIdea> allIdeas =  CLSIdeaUtil.findAll();
		
		for (CLSIdea idea : allIdeas){
			
			if (idea.getIsNeed() && (idea.getMunicipalityOrganizationId() == municipalityOrganizationId) )
				allNeeds.add(idea);
			
		}
		
		return allNeeds;
	}
	
	/**
	 * Return a list of all Ideas of the Municipality Organization
	 * @param userId
	 * @return
	 * @throws SystemException
	 */
	public List<CLSIdea> getOnlyIdeasByMunicipalityOrganizationId(long municipalityOrganizationId) throws SystemException{
		
		List<CLSIdea> allNeeds = new ArrayList<CLSIdea>();
		
		List<CLSIdea> allIdeas =  CLSIdeaUtil.findAll();
		
		for (CLSIdea idea : allIdeas){
			
			if (!idea.getIsNeed() && (idea.getMunicipalityOrganizationId() == municipalityOrganizationId) )
				allNeeds.add(idea);
			
		}
		
		return allNeeds;
	}
	
	
	/**
	 * Return a list of all Needs of the Municipality
	 * @param userId
	 * @return
	 * @throws SystemException
	 */
	public List<CLSIdea> getNeedsByMunicipalityId(long userId) throws SystemException{
		
		List<CLSIdea> allNeeds = new ArrayList<CLSIdea>();
		
		List<CLSIdea> allIdeas =  CLSIdeaUtil.findAll();
		
		for (CLSIdea idea : allIdeas){
			
			if (idea.getIsNeed() && (idea.getMunicipalityId() ==userId) )
				allNeeds.add(idea);
			
		}
		
		return allNeeds;
	}
	
	/**
	 * Return a list of all Ideas of the Municipality
	 * @param userId
	 * @return
	 * @throws SystemException
	 */
	public List<CLSIdea> getOnlyIdeasByMunicipalityId(long userId) throws SystemException{
		
		List<CLSIdea> allNeeds = new ArrayList<CLSIdea>();
		
		List<CLSIdea> allIdeas =  CLSIdeaUtil.findAll();
		
		for (CLSIdea idea : allIdeas){
			
			if (!idea.getIsNeed() && (idea.getMunicipalityId() ==userId) )
				allNeeds.add(idea);
			
		}
		
		return allNeeds;
	}
	

	/**
	 * Return a list of all Needs of the user
	 * @param userId
	 * @return
	 * @throws SystemException
	 */
	public List<CLSIdea> getNeedsByUserId(long userId) throws SystemException{
		
		List<CLSIdea> allNeeds = new ArrayList<CLSIdea>();
		
		List<CLSIdea> allIdeas =  CLSIdeaUtil.findAll();
		
		for (CLSIdea idea : allIdeas){
			
			if (idea.getIsNeed() && (idea.getUserId() ==userId) )
				allNeeds.add(idea);
			
		}
		
		
		return allNeeds;
	}
	
	/** 
	 * Return a list of all Ideas, without need
	 * @return
	 * @throws SystemException
	 */
	public List<CLSIdea> getOnlyIdeas() throws SystemException{
		
		List<CLSIdea> allIdeas = new ArrayList<CLSIdea>();
		
		List<CLSIdea> allIdeasAndNeed =  CLSIdeaUtil.findAll();
		
		for (CLSIdea idea : allIdeasAndNeed){
			
			if (!idea.getIsNeed() )
				allIdeas.add(idea);
			
		}
		
		return allIdeas;
	}	
	
	/** 
	 * Return a list of all Ideas (without need) of the user
	 * @return
	 * @throws SystemException
	 */
	public List<CLSIdea> getOnlyIdeasByUserId(long userId) throws SystemException{
		
		List<CLSIdea> allIdeas = new ArrayList<CLSIdea>();
		
		List<CLSIdea> allIdeasAndNeed =  CLSIdeaUtil.findAll();
		
		for (CLSIdea idea : allIdeasAndNeed){
			
			if (!idea.getIsNeed() && (idea.getUserId() ==userId)    )
				allIdeas.add(idea);
			
		}
		
		return allIdeas;
	}
	
	
	
	
	
	
	public void addCoworker(long indeaId, User coworker){
		
	}
	
	
	/**
	 * @return
	 */
	public JSONObject doRightToBeForgottenIMS(long liferayUserId, boolean deleteAllContent, ActionRequest actionRequest){
		
		JSONObject OKonIMS = RTBF.doRightToBeForgottenIMS(liferayUserId, deleteAllContent, actionRequest);
		
		return OKonIMS;
	}
	
	
	/**
	 * @param pilot
	 * @param isNeed
	 * @param maxNumber
	 * @return
	 */
	public JSONArray getTopRatedIdeasOrNeedsByPilot(String pilot, boolean isNeed, int maxNumber, Locale locale){
		
		return NeedUtils.getTopRatedIdeasOrNeedsByPilotOrLanguage(pilot, isNeed, maxNumber, "", locale);
		
	}
	
	/**
	 * @param language_acronim (en,es, it, sr, fi)
	 * @param isNeed
	 * @param maxNumber
	 * @return
	 */
	public JSONArray getTopRatedIdeasOrNeedsByLanguage(String language, boolean isNeed, int maxNumber, Locale locale){
		
		return NeedUtils.getTopRatedIdeasOrNeedsByPilotOrLanguage("", isNeed, maxNumber, language, locale);
		
	}
	
	
	/**
	 * @param liferayUserId
	 * @param isNeed
	 * @param maxNumber
	 * @return
	 */
	public JSONArray getIdeasOrNeedsByUserId(long liferayUserId, boolean isNeed, int maxNumber, Locale locale){
		
		return NeedUtils.getIdeasOrNeedsByUserId(liferayUserId, isNeed, maxNumber,  locale);
		
	}
	
	
	
	
	
	
	@Deprecated
	public CLSIdea updateStatus(long userId, long resourcePrimKey, int wStatus,ServiceContext serviceContext)  
			throws PortalException, SystemException    
	  {
		
		 User user = UserServiceUtil.getUserById(userId);

		 Date now = new Date();
		CLSIdea entity= CLSIdeaLocalServiceUtil.getCLSIdea(resourcePrimKey);//getCLSIdea(resourcePrimKey);

		entity.setStatusDate(serviceContext.getModifiedDate(now));
        entity.setStatus(wStatus);
        entity.setStatusByUserId(userId);
        entity.setStatusByUserName(user.getFullName());
        
        
        CLSIdeaLocalServiceUtil.updateCLSIdea(entity);

        if (wStatus != WorkflowConstants.STATUS_APPROVED) {
           return entity;
        }
		        
		return entity;
	  }
	
	
	/** Access from other plugin
	 * @param properties
	 * @return
	 */
	public String getIMSProperties (String properties){
	
	String propValue = IdeaManagementSystemProperties.getProperty(properties);
	
	return propValue;

	}
	
	/** Access from other plugin
	 * @param property
	 * @return
	 */
	public boolean getIMSEnabledProperties (String property){
	
		boolean enabled = IdeaManagementSystemProperties.getEnabledProperty(property);
	
	return enabled;

	}
	
	
}