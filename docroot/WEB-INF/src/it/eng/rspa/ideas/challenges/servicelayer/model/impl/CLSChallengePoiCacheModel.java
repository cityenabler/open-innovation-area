/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing CLSChallengePoi in entity cache.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSChallengePoi
 * @generated
 */
public class CLSChallengePoiCacheModel implements CacheModel<CLSChallengePoi>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(15);

		sb.append("{challengeId=");
		sb.append(challengeId);
		sb.append(", latitude=");
		sb.append(latitude);
		sb.append(", longitude=");
		sb.append(longitude);
		sb.append(", description=");
		sb.append(description);
		sb.append(", poiId=");
		sb.append(poiId);
		sb.append(", title=");
		sb.append(title);
		sb.append(", dateAdded=");
		sb.append(dateAdded);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public CLSChallengePoi toEntityModel() {
		CLSChallengePoiImpl clsChallengePoiImpl = new CLSChallengePoiImpl();

		clsChallengePoiImpl.setChallengeId(challengeId);

		if (latitude == null) {
			clsChallengePoiImpl.setLatitude(StringPool.BLANK);
		}
		else {
			clsChallengePoiImpl.setLatitude(latitude);
		}

		if (longitude == null) {
			clsChallengePoiImpl.setLongitude(StringPool.BLANK);
		}
		else {
			clsChallengePoiImpl.setLongitude(longitude);
		}

		if (description == null) {
			clsChallengePoiImpl.setDescription(StringPool.BLANK);
		}
		else {
			clsChallengePoiImpl.setDescription(description);
		}

		clsChallengePoiImpl.setPoiId(poiId);

		if (title == null) {
			clsChallengePoiImpl.setTitle(StringPool.BLANK);
		}
		else {
			clsChallengePoiImpl.setTitle(title);
		}

		if (dateAdded == Long.MIN_VALUE) {
			clsChallengePoiImpl.setDateAdded(null);
		}
		else {
			clsChallengePoiImpl.setDateAdded(new Date(dateAdded));
		}

		clsChallengePoiImpl.resetOriginalValues();

		return clsChallengePoiImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		challengeId = objectInput.readLong();
		latitude = objectInput.readUTF();
		longitude = objectInput.readUTF();
		description = objectInput.readUTF();
		poiId = objectInput.readLong();
		title = objectInput.readUTF();
		dateAdded = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(challengeId);

		if (latitude == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(latitude);
		}

		if (longitude == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(longitude);
		}

		if (description == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(description);
		}

		objectOutput.writeLong(poiId);

		if (title == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(title);
		}

		objectOutput.writeLong(dateAdded);
	}

	public long challengeId;
	public String latitude;
	public String longitude;
	public String description;
	public long poiId;
	public String title;
	public long dateAdded;
}