/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengesCalendarException;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengesCalendar;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengesCalendarImpl;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengesCalendarModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the c l s challenges calendar service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSChallengesCalendarPersistence
 * @see CLSChallengesCalendarUtil
 * @generated
 */
public class CLSChallengesCalendarPersistenceImpl extends BasePersistenceImpl<CLSChallengesCalendar>
	implements CLSChallengesCalendarPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CLSChallengesCalendarUtil} to access the c l s challenges calendar persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CLSChallengesCalendarImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CLSChallengesCalendarModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengesCalendarModelImpl.FINDER_CACHE_ENABLED,
			CLSChallengesCalendarImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CLSChallengesCalendarModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengesCalendarModelImpl.FINDER_CACHE_ENABLED,
			CLSChallengesCalendarImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CLSChallengesCalendarModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengesCalendarModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public CLSChallengesCalendarPersistenceImpl() {
		setModelClass(CLSChallengesCalendar.class);
	}

	/**
	 * Caches the c l s challenges calendar in the entity cache if it is enabled.
	 *
	 * @param clsChallengesCalendar the c l s challenges calendar
	 */
	@Override
	public void cacheResult(CLSChallengesCalendar clsChallengesCalendar) {
		EntityCacheUtil.putResult(CLSChallengesCalendarModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengesCalendarImpl.class,
			clsChallengesCalendar.getPrimaryKey(), clsChallengesCalendar);

		clsChallengesCalendar.resetOriginalValues();
	}

	/**
	 * Caches the c l s challenges calendars in the entity cache if it is enabled.
	 *
	 * @param clsChallengesCalendars the c l s challenges calendars
	 */
	@Override
	public void cacheResult(List<CLSChallengesCalendar> clsChallengesCalendars) {
		for (CLSChallengesCalendar clsChallengesCalendar : clsChallengesCalendars) {
			if (EntityCacheUtil.getResult(
						CLSChallengesCalendarModelImpl.ENTITY_CACHE_ENABLED,
						CLSChallengesCalendarImpl.class,
						clsChallengesCalendar.getPrimaryKey()) == null) {
				cacheResult(clsChallengesCalendar);
			}
			else {
				clsChallengesCalendar.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all c l s challenges calendars.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CLSChallengesCalendarImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CLSChallengesCalendarImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the c l s challenges calendar.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CLSChallengesCalendar clsChallengesCalendar) {
		EntityCacheUtil.removeResult(CLSChallengesCalendarModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengesCalendarImpl.class,
			clsChallengesCalendar.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<CLSChallengesCalendar> clsChallengesCalendars) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CLSChallengesCalendar clsChallengesCalendar : clsChallengesCalendars) {
			EntityCacheUtil.removeResult(CLSChallengesCalendarModelImpl.ENTITY_CACHE_ENABLED,
				CLSChallengesCalendarImpl.class,
				clsChallengesCalendar.getPrimaryKey());
		}
	}

	/**
	 * Creates a new c l s challenges calendar with the primary key. Does not add the c l s challenges calendar to the database.
	 *
	 * @param challengesCalendarId the primary key for the new c l s challenges calendar
	 * @return the new c l s challenges calendar
	 */
	@Override
	public CLSChallengesCalendar create(long challengesCalendarId) {
		CLSChallengesCalendar clsChallengesCalendar = new CLSChallengesCalendarImpl();

		clsChallengesCalendar.setNew(true);
		clsChallengesCalendar.setPrimaryKey(challengesCalendarId);

		return clsChallengesCalendar;
	}

	/**
	 * Removes the c l s challenges calendar with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param challengesCalendarId the primary key of the c l s challenges calendar
	 * @return the c l s challenges calendar that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengesCalendarException if a c l s challenges calendar with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallengesCalendar remove(long challengesCalendarId)
		throws NoSuchCLSChallengesCalendarException, SystemException {
		return remove((Serializable)challengesCalendarId);
	}

	/**
	 * Removes the c l s challenges calendar with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the c l s challenges calendar
	 * @return the c l s challenges calendar that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengesCalendarException if a c l s challenges calendar with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallengesCalendar remove(Serializable primaryKey)
		throws NoSuchCLSChallengesCalendarException, SystemException {
		Session session = null;

		try {
			session = openSession();

			CLSChallengesCalendar clsChallengesCalendar = (CLSChallengesCalendar)session.get(CLSChallengesCalendarImpl.class,
					primaryKey);

			if (clsChallengesCalendar == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCLSChallengesCalendarException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(clsChallengesCalendar);
		}
		catch (NoSuchCLSChallengesCalendarException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CLSChallengesCalendar removeImpl(
		CLSChallengesCalendar clsChallengesCalendar) throws SystemException {
		clsChallengesCalendar = toUnwrappedModel(clsChallengesCalendar);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(clsChallengesCalendar)) {
				clsChallengesCalendar = (CLSChallengesCalendar)session.get(CLSChallengesCalendarImpl.class,
						clsChallengesCalendar.getPrimaryKeyObj());
			}

			if (clsChallengesCalendar != null) {
				session.delete(clsChallengesCalendar);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (clsChallengesCalendar != null) {
			clearCache(clsChallengesCalendar);
		}

		return clsChallengesCalendar;
	}

	@Override
	public CLSChallengesCalendar updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengesCalendar clsChallengesCalendar)
		throws SystemException {
		clsChallengesCalendar = toUnwrappedModel(clsChallengesCalendar);

		boolean isNew = clsChallengesCalendar.isNew();

		Session session = null;

		try {
			session = openSession();

			if (clsChallengesCalendar.isNew()) {
				session.save(clsChallengesCalendar);

				clsChallengesCalendar.setNew(false);
			}
			else {
				session.merge(clsChallengesCalendar);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(CLSChallengesCalendarModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengesCalendarImpl.class,
			clsChallengesCalendar.getPrimaryKey(), clsChallengesCalendar);

		return clsChallengesCalendar;
	}

	protected CLSChallengesCalendar toUnwrappedModel(
		CLSChallengesCalendar clsChallengesCalendar) {
		if (clsChallengesCalendar instanceof CLSChallengesCalendarImpl) {
			return clsChallengesCalendar;
		}

		CLSChallengesCalendarImpl clsChallengesCalendarImpl = new CLSChallengesCalendarImpl();

		clsChallengesCalendarImpl.setNew(clsChallengesCalendar.isNew());
		clsChallengesCalendarImpl.setPrimaryKey(clsChallengesCalendar.getPrimaryKey());

		clsChallengesCalendarImpl.setChallengesCalendarId(clsChallengesCalendar.getChallengesCalendarId());
		clsChallengesCalendarImpl.setReferenceCalendarId(clsChallengesCalendar.getReferenceCalendarId());

		return clsChallengesCalendarImpl;
	}

	/**
	 * Returns the c l s challenges calendar with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the c l s challenges calendar
	 * @return the c l s challenges calendar
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengesCalendarException if a c l s challenges calendar with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallengesCalendar findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCLSChallengesCalendarException, SystemException {
		CLSChallengesCalendar clsChallengesCalendar = fetchByPrimaryKey(primaryKey);

		if (clsChallengesCalendar == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCLSChallengesCalendarException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return clsChallengesCalendar;
	}

	/**
	 * Returns the c l s challenges calendar with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengesCalendarException} if it could not be found.
	 *
	 * @param challengesCalendarId the primary key of the c l s challenges calendar
	 * @return the c l s challenges calendar
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengesCalendarException if a c l s challenges calendar with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallengesCalendar findByPrimaryKey(long challengesCalendarId)
		throws NoSuchCLSChallengesCalendarException, SystemException {
		return findByPrimaryKey((Serializable)challengesCalendarId);
	}

	/**
	 * Returns the c l s challenges calendar with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the c l s challenges calendar
	 * @return the c l s challenges calendar, or <code>null</code> if a c l s challenges calendar with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallengesCalendar fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		CLSChallengesCalendar clsChallengesCalendar = (CLSChallengesCalendar)EntityCacheUtil.getResult(CLSChallengesCalendarModelImpl.ENTITY_CACHE_ENABLED,
				CLSChallengesCalendarImpl.class, primaryKey);

		if (clsChallengesCalendar == _nullCLSChallengesCalendar) {
			return null;
		}

		if (clsChallengesCalendar == null) {
			Session session = null;

			try {
				session = openSession();

				clsChallengesCalendar = (CLSChallengesCalendar)session.get(CLSChallengesCalendarImpl.class,
						primaryKey);

				if (clsChallengesCalendar != null) {
					cacheResult(clsChallengesCalendar);
				}
				else {
					EntityCacheUtil.putResult(CLSChallengesCalendarModelImpl.ENTITY_CACHE_ENABLED,
						CLSChallengesCalendarImpl.class, primaryKey,
						_nullCLSChallengesCalendar);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(CLSChallengesCalendarModelImpl.ENTITY_CACHE_ENABLED,
					CLSChallengesCalendarImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return clsChallengesCalendar;
	}

	/**
	 * Returns the c l s challenges calendar with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param challengesCalendarId the primary key of the c l s challenges calendar
	 * @return the c l s challenges calendar, or <code>null</code> if a c l s challenges calendar with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallengesCalendar fetchByPrimaryKey(long challengesCalendarId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)challengesCalendarId);
	}

	/**
	 * Returns all the c l s challenges calendars.
	 *
	 * @return the c l s challenges calendars
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSChallengesCalendar> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s challenges calendars.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengesCalendarModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of c l s challenges calendars
	 * @param end the upper bound of the range of c l s challenges calendars (not inclusive)
	 * @return the range of c l s challenges calendars
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSChallengesCalendar> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s challenges calendars.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengesCalendarModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of c l s challenges calendars
	 * @param end the upper bound of the range of c l s challenges calendars (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of c l s challenges calendars
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSChallengesCalendar> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CLSChallengesCalendar> list = (List<CLSChallengesCalendar>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CLSCHALLENGESCALENDAR);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CLSCHALLENGESCALENDAR;

				if (pagination) {
					sql = sql.concat(CLSChallengesCalendarModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<CLSChallengesCalendar>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSChallengesCalendar>(list);
				}
				else {
					list = (List<CLSChallengesCalendar>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the c l s challenges calendars from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (CLSChallengesCalendar clsChallengesCalendar : findAll()) {
			remove(clsChallengesCalendar);
		}
	}

	/**
	 * Returns the number of c l s challenges calendars.
	 *
	 * @return the number of c l s challenges calendars
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CLSCHALLENGESCALENDAR);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the c l s challenges calendar persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengesCalendar")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<CLSChallengesCalendar>> listenersList = new ArrayList<ModelListener<CLSChallengesCalendar>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<CLSChallengesCalendar>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CLSChallengesCalendarImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_CLSCHALLENGESCALENDAR = "SELECT clsChallengesCalendar FROM CLSChallengesCalendar clsChallengesCalendar";
	private static final String _SQL_COUNT_CLSCHALLENGESCALENDAR = "SELECT COUNT(clsChallengesCalendar) FROM CLSChallengesCalendar clsChallengesCalendar";
	private static final String _ORDER_BY_ENTITY_ALIAS = "clsChallengesCalendar.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CLSChallengesCalendar exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CLSChallengesCalendarPersistenceImpl.class);
	private static CLSChallengesCalendar _nullCLSChallengesCalendar = new CLSChallengesCalendarImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<CLSChallengesCalendar> toCacheModel() {
				return _nullCLSChallengesCalendarCacheModel;
			}
		};

	private static CacheModel<CLSChallengesCalendar> _nullCLSChallengesCalendarCacheModel =
		new CacheModel<CLSChallengesCalendar>() {
			@Override
			public CLSChallengesCalendar toEntityModel() {
				return _nullCLSChallengesCalendar;
			}
		};
}