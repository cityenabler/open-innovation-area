/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.ideas.challenges.servicelayer.NoSuchVirtuosityPointsException;
import it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.VirtuosityPointsImpl;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.VirtuosityPointsModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the virtuosity points service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see VirtuosityPointsPersistence
 * @see VirtuosityPointsUtil
 * @generated
 */
public class VirtuosityPointsPersistenceImpl extends BasePersistenceImpl<VirtuosityPoints>
	implements VirtuosityPointsPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link VirtuosityPointsUtil} to access the virtuosity points persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = VirtuosityPointsImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(VirtuosityPointsModelImpl.ENTITY_CACHE_ENABLED,
			VirtuosityPointsModelImpl.FINDER_CACHE_ENABLED,
			VirtuosityPointsImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(VirtuosityPointsModelImpl.ENTITY_CACHE_ENABLED,
			VirtuosityPointsModelImpl.FINDER_CACHE_ENABLED,
			VirtuosityPointsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(VirtuosityPointsModelImpl.ENTITY_CACHE_ENABLED,
			VirtuosityPointsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BYCHALLENGEID =
		new FinderPath(VirtuosityPointsModelImpl.ENTITY_CACHE_ENABLED,
			VirtuosityPointsModelImpl.FINDER_CACHE_ENABLED,
			VirtuosityPointsImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findBybyChallengeId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BYCHALLENGEID =
		new FinderPath(VirtuosityPointsModelImpl.ENTITY_CACHE_ENABLED,
			VirtuosityPointsModelImpl.FINDER_CACHE_ENABLED,
			VirtuosityPointsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBybyChallengeId",
			new String[] { Long.class.getName() },
			VirtuosityPointsModelImpl.CHALLENGEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BYCHALLENGEID = new FinderPath(VirtuosityPointsModelImpl.ENTITY_CACHE_ENABLED,
			VirtuosityPointsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBybyChallengeId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the virtuosity pointses where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @return the matching virtuosity pointses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<VirtuosityPoints> findBybyChallengeId(long challengeId)
		throws SystemException {
		return findBybyChallengeId(challengeId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the virtuosity pointses where challengeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.VirtuosityPointsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param challengeId the challenge ID
	 * @param start the lower bound of the range of virtuosity pointses
	 * @param end the upper bound of the range of virtuosity pointses (not inclusive)
	 * @return the range of matching virtuosity pointses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<VirtuosityPoints> findBybyChallengeId(long challengeId,
		int start, int end) throws SystemException {
		return findBybyChallengeId(challengeId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the virtuosity pointses where challengeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.VirtuosityPointsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param challengeId the challenge ID
	 * @param start the lower bound of the range of virtuosity pointses
	 * @param end the upper bound of the range of virtuosity pointses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching virtuosity pointses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<VirtuosityPoints> findBybyChallengeId(long challengeId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BYCHALLENGEID;
			finderArgs = new Object[] { challengeId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BYCHALLENGEID;
			finderArgs = new Object[] { challengeId, start, end, orderByComparator };
		}

		List<VirtuosityPoints> list = (List<VirtuosityPoints>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (VirtuosityPoints virtuosityPoints : list) {
				if ((challengeId != virtuosityPoints.getChallengeId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_VIRTUOSITYPOINTS_WHERE);

			query.append(_FINDER_COLUMN_BYCHALLENGEID_CHALLENGEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(VirtuosityPointsModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(challengeId);

				if (!pagination) {
					list = (List<VirtuosityPoints>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<VirtuosityPoints>(list);
				}
				else {
					list = (List<VirtuosityPoints>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first virtuosity points in the ordered set where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching virtuosity points
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchVirtuosityPointsException if a matching virtuosity points could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public VirtuosityPoints findBybyChallengeId_First(long challengeId,
		OrderByComparator orderByComparator)
		throws NoSuchVirtuosityPointsException, SystemException {
		VirtuosityPoints virtuosityPoints = fetchBybyChallengeId_First(challengeId,
				orderByComparator);

		if (virtuosityPoints != null) {
			return virtuosityPoints;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("challengeId=");
		msg.append(challengeId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchVirtuosityPointsException(msg.toString());
	}

	/**
	 * Returns the first virtuosity points in the ordered set where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching virtuosity points, or <code>null</code> if a matching virtuosity points could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public VirtuosityPoints fetchBybyChallengeId_First(long challengeId,
		OrderByComparator orderByComparator) throws SystemException {
		List<VirtuosityPoints> list = findBybyChallengeId(challengeId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last virtuosity points in the ordered set where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching virtuosity points
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchVirtuosityPointsException if a matching virtuosity points could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public VirtuosityPoints findBybyChallengeId_Last(long challengeId,
		OrderByComparator orderByComparator)
		throws NoSuchVirtuosityPointsException, SystemException {
		VirtuosityPoints virtuosityPoints = fetchBybyChallengeId_Last(challengeId,
				orderByComparator);

		if (virtuosityPoints != null) {
			return virtuosityPoints;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("challengeId=");
		msg.append(challengeId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchVirtuosityPointsException(msg.toString());
	}

	/**
	 * Returns the last virtuosity points in the ordered set where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching virtuosity points, or <code>null</code> if a matching virtuosity points could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public VirtuosityPoints fetchBybyChallengeId_Last(long challengeId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBybyChallengeId(challengeId);

		if (count == 0) {
			return null;
		}

		List<VirtuosityPoints> list = findBybyChallengeId(challengeId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the virtuosity pointses before and after the current virtuosity points in the ordered set where challengeId = &#63;.
	 *
	 * @param virtuosityPointsPK the primary key of the current virtuosity points
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next virtuosity points
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchVirtuosityPointsException if a virtuosity points with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public VirtuosityPoints[] findBybyChallengeId_PrevAndNext(
		VirtuosityPointsPK virtuosityPointsPK, long challengeId,
		OrderByComparator orderByComparator)
		throws NoSuchVirtuosityPointsException, SystemException {
		VirtuosityPoints virtuosityPoints = findByPrimaryKey(virtuosityPointsPK);

		Session session = null;

		try {
			session = openSession();

			VirtuosityPoints[] array = new VirtuosityPointsImpl[3];

			array[0] = getBybyChallengeId_PrevAndNext(session,
					virtuosityPoints, challengeId, orderByComparator, true);

			array[1] = virtuosityPoints;

			array[2] = getBybyChallengeId_PrevAndNext(session,
					virtuosityPoints, challengeId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected VirtuosityPoints getBybyChallengeId_PrevAndNext(Session session,
		VirtuosityPoints virtuosityPoints, long challengeId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_VIRTUOSITYPOINTS_WHERE);

		query.append(_FINDER_COLUMN_BYCHALLENGEID_CHALLENGEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(VirtuosityPointsModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(challengeId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(virtuosityPoints);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<VirtuosityPoints> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the virtuosity pointses where challengeId = &#63; from the database.
	 *
	 * @param challengeId the challenge ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBybyChallengeId(long challengeId)
		throws SystemException {
		for (VirtuosityPoints virtuosityPoints : findBybyChallengeId(
				challengeId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(virtuosityPoints);
		}
	}

	/**
	 * Returns the number of virtuosity pointses where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @return the number of matching virtuosity pointses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBybyChallengeId(long challengeId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_BYCHALLENGEID;

		Object[] finderArgs = new Object[] { challengeId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_VIRTUOSITYPOINTS_WHERE);

			query.append(_FINDER_COLUMN_BYCHALLENGEID_CHALLENGEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(challengeId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_BYCHALLENGEID_CHALLENGEID_2 = "virtuosityPoints.id.challengeId = ?";

	public VirtuosityPointsPersistenceImpl() {
		setModelClass(VirtuosityPoints.class);
	}

	/**
	 * Caches the virtuosity points in the entity cache if it is enabled.
	 *
	 * @param virtuosityPoints the virtuosity points
	 */
	@Override
	public void cacheResult(VirtuosityPoints virtuosityPoints) {
		EntityCacheUtil.putResult(VirtuosityPointsModelImpl.ENTITY_CACHE_ENABLED,
			VirtuosityPointsImpl.class, virtuosityPoints.getPrimaryKey(),
			virtuosityPoints);

		virtuosityPoints.resetOriginalValues();
	}

	/**
	 * Caches the virtuosity pointses in the entity cache if it is enabled.
	 *
	 * @param virtuosityPointses the virtuosity pointses
	 */
	@Override
	public void cacheResult(List<VirtuosityPoints> virtuosityPointses) {
		for (VirtuosityPoints virtuosityPoints : virtuosityPointses) {
			if (EntityCacheUtil.getResult(
						VirtuosityPointsModelImpl.ENTITY_CACHE_ENABLED,
						VirtuosityPointsImpl.class,
						virtuosityPoints.getPrimaryKey()) == null) {
				cacheResult(virtuosityPoints);
			}
			else {
				virtuosityPoints.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all virtuosity pointses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(VirtuosityPointsImpl.class.getName());
		}

		EntityCacheUtil.clearCache(VirtuosityPointsImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the virtuosity points.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(VirtuosityPoints virtuosityPoints) {
		EntityCacheUtil.removeResult(VirtuosityPointsModelImpl.ENTITY_CACHE_ENABLED,
			VirtuosityPointsImpl.class, virtuosityPoints.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<VirtuosityPoints> virtuosityPointses) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (VirtuosityPoints virtuosityPoints : virtuosityPointses) {
			EntityCacheUtil.removeResult(VirtuosityPointsModelImpl.ENTITY_CACHE_ENABLED,
				VirtuosityPointsImpl.class, virtuosityPoints.getPrimaryKey());
		}
	}

	/**
	 * Creates a new virtuosity points with the primary key. Does not add the virtuosity points to the database.
	 *
	 * @param virtuosityPointsPK the primary key for the new virtuosity points
	 * @return the new virtuosity points
	 */
	@Override
	public VirtuosityPoints create(VirtuosityPointsPK virtuosityPointsPK) {
		VirtuosityPoints virtuosityPoints = new VirtuosityPointsImpl();

		virtuosityPoints.setNew(true);
		virtuosityPoints.setPrimaryKey(virtuosityPointsPK);

		return virtuosityPoints;
	}

	/**
	 * Removes the virtuosity points with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param virtuosityPointsPK the primary key of the virtuosity points
	 * @return the virtuosity points that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchVirtuosityPointsException if a virtuosity points with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public VirtuosityPoints remove(VirtuosityPointsPK virtuosityPointsPK)
		throws NoSuchVirtuosityPointsException, SystemException {
		return remove((Serializable)virtuosityPointsPK);
	}

	/**
	 * Removes the virtuosity points with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the virtuosity points
	 * @return the virtuosity points that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchVirtuosityPointsException if a virtuosity points with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public VirtuosityPoints remove(Serializable primaryKey)
		throws NoSuchVirtuosityPointsException, SystemException {
		Session session = null;

		try {
			session = openSession();

			VirtuosityPoints virtuosityPoints = (VirtuosityPoints)session.get(VirtuosityPointsImpl.class,
					primaryKey);

			if (virtuosityPoints == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchVirtuosityPointsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(virtuosityPoints);
		}
		catch (NoSuchVirtuosityPointsException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected VirtuosityPoints removeImpl(VirtuosityPoints virtuosityPoints)
		throws SystemException {
		virtuosityPoints = toUnwrappedModel(virtuosityPoints);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(virtuosityPoints)) {
				virtuosityPoints = (VirtuosityPoints)session.get(VirtuosityPointsImpl.class,
						virtuosityPoints.getPrimaryKeyObj());
			}

			if (virtuosityPoints != null) {
				session.delete(virtuosityPoints);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (virtuosityPoints != null) {
			clearCache(virtuosityPoints);
		}

		return virtuosityPoints;
	}

	@Override
	public VirtuosityPoints updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints virtuosityPoints)
		throws SystemException {
		virtuosityPoints = toUnwrappedModel(virtuosityPoints);

		boolean isNew = virtuosityPoints.isNew();

		VirtuosityPointsModelImpl virtuosityPointsModelImpl = (VirtuosityPointsModelImpl)virtuosityPoints;

		Session session = null;

		try {
			session = openSession();

			if (virtuosityPoints.isNew()) {
				session.save(virtuosityPoints);

				virtuosityPoints.setNew(false);
			}
			else {
				session.merge(virtuosityPoints);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !VirtuosityPointsModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((virtuosityPointsModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BYCHALLENGEID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						virtuosityPointsModelImpl.getOriginalChallengeId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BYCHALLENGEID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BYCHALLENGEID,
					args);

				args = new Object[] { virtuosityPointsModelImpl.getChallengeId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BYCHALLENGEID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BYCHALLENGEID,
					args);
			}
		}

		EntityCacheUtil.putResult(VirtuosityPointsModelImpl.ENTITY_CACHE_ENABLED,
			VirtuosityPointsImpl.class, virtuosityPoints.getPrimaryKey(),
			virtuosityPoints);

		return virtuosityPoints;
	}

	protected VirtuosityPoints toUnwrappedModel(
		VirtuosityPoints virtuosityPoints) {
		if (virtuosityPoints instanceof VirtuosityPointsImpl) {
			return virtuosityPoints;
		}

		VirtuosityPointsImpl virtuosityPointsImpl = new VirtuosityPointsImpl();

		virtuosityPointsImpl.setNew(virtuosityPoints.isNew());
		virtuosityPointsImpl.setPrimaryKey(virtuosityPoints.getPrimaryKey());

		virtuosityPointsImpl.setChallengeId(virtuosityPoints.getChallengeId());
		virtuosityPointsImpl.setPosition(virtuosityPoints.getPosition());
		virtuosityPointsImpl.setPoints(virtuosityPoints.getPoints());
		virtuosityPointsImpl.setDateAdded(virtuosityPoints.getDateAdded());

		return virtuosityPointsImpl;
	}

	/**
	 * Returns the virtuosity points with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the virtuosity points
	 * @return the virtuosity points
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchVirtuosityPointsException if a virtuosity points with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public VirtuosityPoints findByPrimaryKey(Serializable primaryKey)
		throws NoSuchVirtuosityPointsException, SystemException {
		VirtuosityPoints virtuosityPoints = fetchByPrimaryKey(primaryKey);

		if (virtuosityPoints == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchVirtuosityPointsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return virtuosityPoints;
	}

	/**
	 * Returns the virtuosity points with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchVirtuosityPointsException} if it could not be found.
	 *
	 * @param virtuosityPointsPK the primary key of the virtuosity points
	 * @return the virtuosity points
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchVirtuosityPointsException if a virtuosity points with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public VirtuosityPoints findByPrimaryKey(
		VirtuosityPointsPK virtuosityPointsPK)
		throws NoSuchVirtuosityPointsException, SystemException {
		return findByPrimaryKey((Serializable)virtuosityPointsPK);
	}

	/**
	 * Returns the virtuosity points with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the virtuosity points
	 * @return the virtuosity points, or <code>null</code> if a virtuosity points with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public VirtuosityPoints fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		VirtuosityPoints virtuosityPoints = (VirtuosityPoints)EntityCacheUtil.getResult(VirtuosityPointsModelImpl.ENTITY_CACHE_ENABLED,
				VirtuosityPointsImpl.class, primaryKey);

		if (virtuosityPoints == _nullVirtuosityPoints) {
			return null;
		}

		if (virtuosityPoints == null) {
			Session session = null;

			try {
				session = openSession();

				virtuosityPoints = (VirtuosityPoints)session.get(VirtuosityPointsImpl.class,
						primaryKey);

				if (virtuosityPoints != null) {
					cacheResult(virtuosityPoints);
				}
				else {
					EntityCacheUtil.putResult(VirtuosityPointsModelImpl.ENTITY_CACHE_ENABLED,
						VirtuosityPointsImpl.class, primaryKey,
						_nullVirtuosityPoints);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(VirtuosityPointsModelImpl.ENTITY_CACHE_ENABLED,
					VirtuosityPointsImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return virtuosityPoints;
	}

	/**
	 * Returns the virtuosity points with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param virtuosityPointsPK the primary key of the virtuosity points
	 * @return the virtuosity points, or <code>null</code> if a virtuosity points with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public VirtuosityPoints fetchByPrimaryKey(
		VirtuosityPointsPK virtuosityPointsPK) throws SystemException {
		return fetchByPrimaryKey((Serializable)virtuosityPointsPK);
	}

	/**
	 * Returns all the virtuosity pointses.
	 *
	 * @return the virtuosity pointses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<VirtuosityPoints> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the virtuosity pointses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.VirtuosityPointsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of virtuosity pointses
	 * @param end the upper bound of the range of virtuosity pointses (not inclusive)
	 * @return the range of virtuosity pointses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<VirtuosityPoints> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the virtuosity pointses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.VirtuosityPointsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of virtuosity pointses
	 * @param end the upper bound of the range of virtuosity pointses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of virtuosity pointses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<VirtuosityPoints> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<VirtuosityPoints> list = (List<VirtuosityPoints>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_VIRTUOSITYPOINTS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_VIRTUOSITYPOINTS;

				if (pagination) {
					sql = sql.concat(VirtuosityPointsModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<VirtuosityPoints>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<VirtuosityPoints>(list);
				}
				else {
					list = (List<VirtuosityPoints>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the virtuosity pointses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (VirtuosityPoints virtuosityPoints : findAll()) {
			remove(virtuosityPoints);
		}
	}

	/**
	 * Returns the number of virtuosity pointses.
	 *
	 * @return the number of virtuosity pointses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_VIRTUOSITYPOINTS);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the virtuosity points persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.ideas.challenges.servicelayer.model.VirtuosityPoints")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<VirtuosityPoints>> listenersList = new ArrayList<ModelListener<VirtuosityPoints>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<VirtuosityPoints>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(VirtuosityPointsImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_VIRTUOSITYPOINTS = "SELECT virtuosityPoints FROM VirtuosityPoints virtuosityPoints";
	private static final String _SQL_SELECT_VIRTUOSITYPOINTS_WHERE = "SELECT virtuosityPoints FROM VirtuosityPoints virtuosityPoints WHERE ";
	private static final String _SQL_COUNT_VIRTUOSITYPOINTS = "SELECT COUNT(virtuosityPoints) FROM VirtuosityPoints virtuosityPoints";
	private static final String _SQL_COUNT_VIRTUOSITYPOINTS_WHERE = "SELECT COUNT(virtuosityPoints) FROM VirtuosityPoints virtuosityPoints WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "virtuosityPoints.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No VirtuosityPoints exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No VirtuosityPoints exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(VirtuosityPointsPersistenceImpl.class);
	private static VirtuosityPoints _nullVirtuosityPoints = new VirtuosityPointsImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<VirtuosityPoints> toCacheModel() {
				return _nullVirtuosityPointsCacheModel;
			}
		};

	private static CacheModel<VirtuosityPoints> _nullVirtuosityPointsCacheModel = new CacheModel<VirtuosityPoints>() {
			@Override
			public VirtuosityPoints toEntityModel() {
				return _nullVirtuosityPoints;
			}
		};
}