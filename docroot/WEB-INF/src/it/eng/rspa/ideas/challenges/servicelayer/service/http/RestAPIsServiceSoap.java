/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.http;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import it.eng.rspa.ideas.challenges.servicelayer.service.RestAPIsServiceUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link it.eng.rspa.ideas.challenges.servicelayer.service.RestAPIsServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see RestAPIsServiceHttp
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.RestAPIsServiceUtil
 * @generated
 */
public class RestAPIsServiceSoap {
	public static java.lang.String challenges() throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONArray returnValue = RestAPIsServiceUtil.challenges();

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String onechallenge(long id)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = RestAPIsServiceUtil.onechallenge(id);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String ideas() throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONArray returnValue = RestAPIsServiceUtil.ideas();

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String oneidea(long id) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = RestAPIsServiceUtil.oneidea(id);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String allNeeds() throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONArray returnValue = RestAPIsServiceUtil.allNeeds();

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String oneneed(long id) throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = RestAPIsServiceUtil.oneneed(id);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String allNeedsOpen311() throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONArray returnValue = RestAPIsServiceUtil.allNeedsOpen311();

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String oneneedOpen311(long id)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = RestAPIsServiceUtil.oneneedOpen311(id);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String allPoisOpen311() throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONArray returnValue = RestAPIsServiceUtil.allPoisOpen311();

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String onepoiOpen311(long id)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = RestAPIsServiceUtil.onepoiOpen311(id);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String allPoisOpen311_V2()
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONArray returnValue = RestAPIsServiceUtil.allPoisOpen311_V2();

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String onepoiOpen311_V2(long id)
		throws RemoteException {
		try {
			com.liferay.portal.kernel.json.JSONObject returnValue = RestAPIsServiceUtil.onepoiOpen311_V2(id);

			return returnValue.toString();
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	private static Log _log = LogFactoryUtil.getLog(RestAPIsServiceSoap.class);
}