/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengeException;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengeImpl;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengeModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the c l s challenge service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSChallengePersistence
 * @see CLSChallengeUtil
 * @generated
 */
public class CLSChallengePersistenceImpl extends BasePersistenceImpl<CLSChallenge>
	implements CLSChallengePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CLSChallengeUtil} to access the c l s challenge persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CLSChallengeImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CLSChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengeModelImpl.FINDER_CACHE_ENABLED, CLSChallengeImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CLSChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengeModelImpl.FINDER_CACHE_ENABLED, CLSChallengeImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CLSChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengeModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID = new FinderPath(CLSChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengeModelImpl.FINDER_CACHE_ENABLED, CLSChallengeImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID = new FinderPath(CLSChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengeModelImpl.FINDER_CACHE_ENABLED, CLSChallengeImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] { String.class.getName() },
			CLSChallengeModelImpl.UUID_COLUMN_BITMASK |
			CLSChallengeModelImpl.DATEADDED_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID = new FinderPath(CLSChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengeModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] { String.class.getName() });

	/**
	 * Returns all the c l s challenges where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching c l s challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSChallenge> findByUuid(String uuid) throws SystemException {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s challenges where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of c l s challenges
	 * @param end the upper bound of the range of c l s challenges (not inclusive)
	 * @return the range of matching c l s challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSChallenge> findByUuid(String uuid, int start, int end)
		throws SystemException {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s challenges where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of c l s challenges
	 * @param end the upper bound of the range of c l s challenges (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSChallenge> findByUuid(String uuid, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid, start, end, orderByComparator };
		}

		List<CLSChallenge> list = (List<CLSChallenge>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSChallenge clsChallenge : list) {
				if (!Validator.equals(uuid, clsChallenge.getUuid())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CLSCHALLENGE_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSChallengeModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<CLSChallenge>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSChallenge>(list);
				}
				else {
					list = (List<CLSChallenge>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s challenge in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengeException if a matching c l s challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallenge findByUuid_First(String uuid,
		OrderByComparator orderByComparator)
		throws NoSuchCLSChallengeException, SystemException {
		CLSChallenge clsChallenge = fetchByUuid_First(uuid, orderByComparator);

		if (clsChallenge != null) {
			return clsChallenge;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSChallengeException(msg.toString());
	}

	/**
	 * Returns the first c l s challenge in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s challenge, or <code>null</code> if a matching c l s challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallenge fetchByUuid_First(String uuid,
		OrderByComparator orderByComparator) throws SystemException {
		List<CLSChallenge> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s challenge in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengeException if a matching c l s challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallenge findByUuid_Last(String uuid,
		OrderByComparator orderByComparator)
		throws NoSuchCLSChallengeException, SystemException {
		CLSChallenge clsChallenge = fetchByUuid_Last(uuid, orderByComparator);

		if (clsChallenge != null) {
			return clsChallenge;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSChallengeException(msg.toString());
	}

	/**
	 * Returns the last c l s challenge in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s challenge, or <code>null</code> if a matching c l s challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallenge fetchByUuid_Last(String uuid,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<CLSChallenge> list = findByUuid(uuid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s challenges before and after the current c l s challenge in the ordered set where uuid = &#63;.
	 *
	 * @param challengeId the primary key of the current c l s challenge
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengeException if a c l s challenge with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallenge[] findByUuid_PrevAndNext(long challengeId, String uuid,
		OrderByComparator orderByComparator)
		throws NoSuchCLSChallengeException, SystemException {
		CLSChallenge clsChallenge = findByPrimaryKey(challengeId);

		Session session = null;

		try {
			session = openSession();

			CLSChallenge[] array = new CLSChallengeImpl[3];

			array[0] = getByUuid_PrevAndNext(session, clsChallenge, uuid,
					orderByComparator, true);

			array[1] = clsChallenge;

			array[2] = getByUuid_PrevAndNext(session, clsChallenge, uuid,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSChallenge getByUuid_PrevAndNext(Session session,
		CLSChallenge clsChallenge, String uuid,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSCHALLENGE_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSChallengeModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsChallenge);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSChallenge> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s challenges where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByUuid(String uuid) throws SystemException {
		for (CLSChallenge clsChallenge : findByUuid(uuid, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(clsChallenge);
		}
	}

	/**
	 * Returns the number of c l s challenges where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching c l s challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUuid(String uuid) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID;

		Object[] finderArgs = new Object[] { uuid };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLSCHALLENGE_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_1 = "clsChallenge.uuid IS NULL";
	private static final String _FINDER_COLUMN_UUID_UUID_2 = "clsChallenge.uuid = ?";
	private static final String _FINDER_COLUMN_UUID_UUID_3 = "(clsChallenge.uuid IS NULL OR clsChallenge.uuid = '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_UUID_G = new FinderPath(CLSChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengeModelImpl.FINDER_CACHE_ENABLED, CLSChallengeImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() },
			CLSChallengeModelImpl.UUID_COLUMN_BITMASK |
			CLSChallengeModelImpl.GROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_G = new FinderPath(CLSChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengeModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns the c l s challenge where uuid = &#63; and groupId = &#63; or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengeException} if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching c l s challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengeException if a matching c l s challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallenge findByUUID_G(String uuid, long groupId)
		throws NoSuchCLSChallengeException, SystemException {
		CLSChallenge clsChallenge = fetchByUUID_G(uuid, groupId);

		if (clsChallenge == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("uuid=");
			msg.append(uuid);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchCLSChallengeException(msg.toString());
		}

		return clsChallenge;
	}

	/**
	 * Returns the c l s challenge where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching c l s challenge, or <code>null</code> if a matching c l s challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallenge fetchByUUID_G(String uuid, long groupId)
		throws SystemException {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the c l s challenge where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching c l s challenge, or <code>null</code> if a matching c l s challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallenge fetchByUUID_G(String uuid, long groupId,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { uuid, groupId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_UUID_G,
					finderArgs, this);
		}

		if (result instanceof CLSChallenge) {
			CLSChallenge clsChallenge = (CLSChallenge)result;

			if (!Validator.equals(uuid, clsChallenge.getUuid()) ||
					(groupId != clsChallenge.getGroupId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_CLSCHALLENGE_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				List<CLSChallenge> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G,
						finderArgs, list);
				}
				else {
					CLSChallenge clsChallenge = list.get(0);

					result = clsChallenge;

					cacheResult(clsChallenge);

					if ((clsChallenge.getUuid() == null) ||
							!clsChallenge.getUuid().equals(uuid) ||
							(clsChallenge.getGroupId() != groupId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G,
							finderArgs, clsChallenge);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_UUID_G,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (CLSChallenge)result;
		}
	}

	/**
	 * Removes the c l s challenge where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the c l s challenge that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallenge removeByUUID_G(String uuid, long groupId)
		throws NoSuchCLSChallengeException, SystemException {
		CLSChallenge clsChallenge = findByUUID_G(uuid, groupId);

		return remove(clsChallenge);
	}

	/**
	 * Returns the number of c l s challenges where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching c l s challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_G;

		Object[] finderArgs = new Object[] { uuid, groupId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CLSCHALLENGE_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_1 = "clsChallenge.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_2 = "clsChallenge.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_3 = "(clsChallenge.uuid IS NULL OR clsChallenge.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 = "clsChallenge.groupId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C = new FinderPath(CLSChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengeModelImpl.FINDER_CACHE_ENABLED, CLSChallengeImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C =
		new FinderPath(CLSChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengeModelImpl.FINDER_CACHE_ENABLED, CLSChallengeImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() },
			CLSChallengeModelImpl.UUID_COLUMN_BITMASK |
			CLSChallengeModelImpl.COMPANYID_COLUMN_BITMASK |
			CLSChallengeModelImpl.DATEADDED_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_C = new FinderPath(CLSChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengeModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns all the c l s challenges where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching c l s challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSChallenge> findByUuid_C(String uuid, long companyId)
		throws SystemException {
		return findByUuid_C(uuid, companyId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s challenges where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of c l s challenges
	 * @param end the upper bound of the range of c l s challenges (not inclusive)
	 * @return the range of matching c l s challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSChallenge> findByUuid_C(String uuid, long companyId,
		int start, int end) throws SystemException {
		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s challenges where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of c l s challenges
	 * @param end the upper bound of the range of c l s challenges (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSChallenge> findByUuid_C(String uuid, long companyId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] { uuid, companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] {
					uuid, companyId,
					
					start, end, orderByComparator
				};
		}

		List<CLSChallenge> list = (List<CLSChallenge>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSChallenge clsChallenge : list) {
				if (!Validator.equals(uuid, clsChallenge.getUuid()) ||
						(companyId != clsChallenge.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_CLSCHALLENGE_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSChallengeModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<CLSChallenge>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSChallenge>(list);
				}
				else {
					list = (List<CLSChallenge>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s challenge in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengeException if a matching c l s challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallenge findByUuid_C_First(String uuid, long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSChallengeException, SystemException {
		CLSChallenge clsChallenge = fetchByUuid_C_First(uuid, companyId,
				orderByComparator);

		if (clsChallenge != null) {
			return clsChallenge;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSChallengeException(msg.toString());
	}

	/**
	 * Returns the first c l s challenge in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s challenge, or <code>null</code> if a matching c l s challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallenge fetchByUuid_C_First(String uuid, long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		List<CLSChallenge> list = findByUuid_C(uuid, companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s challenge in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengeException if a matching c l s challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallenge findByUuid_C_Last(String uuid, long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSChallengeException, SystemException {
		CLSChallenge clsChallenge = fetchByUuid_C_Last(uuid, companyId,
				orderByComparator);

		if (clsChallenge != null) {
			return clsChallenge;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSChallengeException(msg.toString());
	}

	/**
	 * Returns the last c l s challenge in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s challenge, or <code>null</code> if a matching c l s challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallenge fetchByUuid_C_Last(String uuid, long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<CLSChallenge> list = findByUuid_C(uuid, companyId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s challenges before and after the current c l s challenge in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param challengeId the primary key of the current c l s challenge
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengeException if a c l s challenge with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallenge[] findByUuid_C_PrevAndNext(long challengeId,
		String uuid, long companyId, OrderByComparator orderByComparator)
		throws NoSuchCLSChallengeException, SystemException {
		CLSChallenge clsChallenge = findByPrimaryKey(challengeId);

		Session session = null;

		try {
			session = openSession();

			CLSChallenge[] array = new CLSChallengeImpl[3];

			array[0] = getByUuid_C_PrevAndNext(session, clsChallenge, uuid,
					companyId, orderByComparator, true);

			array[1] = clsChallenge;

			array[2] = getByUuid_C_PrevAndNext(session, clsChallenge, uuid,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSChallenge getByUuid_C_PrevAndNext(Session session,
		CLSChallenge clsChallenge, String uuid, long companyId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSCHALLENGE_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSChallengeModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsChallenge);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSChallenge> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s challenges where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId)
		throws SystemException {
		for (CLSChallenge clsChallenge : findByUuid_C(uuid, companyId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(clsChallenge);
		}
	}

	/**
	 * Returns the number of c l s challenges where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching c l s challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_C;

		Object[] finderArgs = new Object[] { uuid, companyId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CLSCHALLENGE_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_1 = "clsChallenge.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_2 = "clsChallenge.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_3 = "(clsChallenge.uuid IS NULL OR clsChallenge.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 = "clsChallenge.companyId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CHALLENGETITLE =
		new FinderPath(CLSChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengeModelImpl.FINDER_CACHE_ENABLED, CLSChallengeImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByChallengeTitle",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGETITLE =
		new FinderPath(CLSChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengeModelImpl.FINDER_CACHE_ENABLED, CLSChallengeImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByChallengeTitle",
			new String[] { String.class.getName() },
			CLSChallengeModelImpl.CHALLENGETITLE_COLUMN_BITMASK |
			CLSChallengeModelImpl.DATEADDED_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CHALLENGETITLE = new FinderPath(CLSChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengeModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByChallengeTitle",
			new String[] { String.class.getName() });

	/**
	 * Returns all the c l s challenges where challengeTitle = &#63;.
	 *
	 * @param challengeTitle the challenge title
	 * @return the matching c l s challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSChallenge> findByChallengeTitle(String challengeTitle)
		throws SystemException {
		return findByChallengeTitle(challengeTitle, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s challenges where challengeTitle = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param challengeTitle the challenge title
	 * @param start the lower bound of the range of c l s challenges
	 * @param end the upper bound of the range of c l s challenges (not inclusive)
	 * @return the range of matching c l s challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSChallenge> findByChallengeTitle(String challengeTitle,
		int start, int end) throws SystemException {
		return findByChallengeTitle(challengeTitle, start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s challenges where challengeTitle = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param challengeTitle the challenge title
	 * @param start the lower bound of the range of c l s challenges
	 * @param end the upper bound of the range of c l s challenges (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSChallenge> findByChallengeTitle(String challengeTitle,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGETITLE;
			finderArgs = new Object[] { challengeTitle };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CHALLENGETITLE;
			finderArgs = new Object[] {
					challengeTitle,
					
					start, end, orderByComparator
				};
		}

		List<CLSChallenge> list = (List<CLSChallenge>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSChallenge clsChallenge : list) {
				if (!Validator.equals(challengeTitle,
							clsChallenge.getChallengeTitle())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CLSCHALLENGE_WHERE);

			boolean bindChallengeTitle = false;

			if (challengeTitle == null) {
				query.append(_FINDER_COLUMN_CHALLENGETITLE_CHALLENGETITLE_1);
			}
			else if (challengeTitle.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CHALLENGETITLE_CHALLENGETITLE_3);
			}
			else {
				bindChallengeTitle = true;

				query.append(_FINDER_COLUMN_CHALLENGETITLE_CHALLENGETITLE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSChallengeModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindChallengeTitle) {
					qPos.add(challengeTitle);
				}

				if (!pagination) {
					list = (List<CLSChallenge>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSChallenge>(list);
				}
				else {
					list = (List<CLSChallenge>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s challenge in the ordered set where challengeTitle = &#63;.
	 *
	 * @param challengeTitle the challenge title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengeException if a matching c l s challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallenge findByChallengeTitle_First(String challengeTitle,
		OrderByComparator orderByComparator)
		throws NoSuchCLSChallengeException, SystemException {
		CLSChallenge clsChallenge = fetchByChallengeTitle_First(challengeTitle,
				orderByComparator);

		if (clsChallenge != null) {
			return clsChallenge;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("challengeTitle=");
		msg.append(challengeTitle);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSChallengeException(msg.toString());
	}

	/**
	 * Returns the first c l s challenge in the ordered set where challengeTitle = &#63;.
	 *
	 * @param challengeTitle the challenge title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s challenge, or <code>null</code> if a matching c l s challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallenge fetchByChallengeTitle_First(String challengeTitle,
		OrderByComparator orderByComparator) throws SystemException {
		List<CLSChallenge> list = findByChallengeTitle(challengeTitle, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s challenge in the ordered set where challengeTitle = &#63;.
	 *
	 * @param challengeTitle the challenge title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengeException if a matching c l s challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallenge findByChallengeTitle_Last(String challengeTitle,
		OrderByComparator orderByComparator)
		throws NoSuchCLSChallengeException, SystemException {
		CLSChallenge clsChallenge = fetchByChallengeTitle_Last(challengeTitle,
				orderByComparator);

		if (clsChallenge != null) {
			return clsChallenge;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("challengeTitle=");
		msg.append(challengeTitle);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSChallengeException(msg.toString());
	}

	/**
	 * Returns the last c l s challenge in the ordered set where challengeTitle = &#63;.
	 *
	 * @param challengeTitle the challenge title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s challenge, or <code>null</code> if a matching c l s challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallenge fetchByChallengeTitle_Last(String challengeTitle,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByChallengeTitle(challengeTitle);

		if (count == 0) {
			return null;
		}

		List<CLSChallenge> list = findByChallengeTitle(challengeTitle,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s challenges before and after the current c l s challenge in the ordered set where challengeTitle = &#63;.
	 *
	 * @param challengeId the primary key of the current c l s challenge
	 * @param challengeTitle the challenge title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengeException if a c l s challenge with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallenge[] findByChallengeTitle_PrevAndNext(long challengeId,
		String challengeTitle, OrderByComparator orderByComparator)
		throws NoSuchCLSChallengeException, SystemException {
		CLSChallenge clsChallenge = findByPrimaryKey(challengeId);

		Session session = null;

		try {
			session = openSession();

			CLSChallenge[] array = new CLSChallengeImpl[3];

			array[0] = getByChallengeTitle_PrevAndNext(session, clsChallenge,
					challengeTitle, orderByComparator, true);

			array[1] = clsChallenge;

			array[2] = getByChallengeTitle_PrevAndNext(session, clsChallenge,
					challengeTitle, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSChallenge getByChallengeTitle_PrevAndNext(Session session,
		CLSChallenge clsChallenge, String challengeTitle,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSCHALLENGE_WHERE);

		boolean bindChallengeTitle = false;

		if (challengeTitle == null) {
			query.append(_FINDER_COLUMN_CHALLENGETITLE_CHALLENGETITLE_1);
		}
		else if (challengeTitle.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_CHALLENGETITLE_CHALLENGETITLE_3);
		}
		else {
			bindChallengeTitle = true;

			query.append(_FINDER_COLUMN_CHALLENGETITLE_CHALLENGETITLE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSChallengeModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindChallengeTitle) {
			qPos.add(challengeTitle);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsChallenge);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSChallenge> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s challenges where challengeTitle = &#63; from the database.
	 *
	 * @param challengeTitle the challenge title
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByChallengeTitle(String challengeTitle)
		throws SystemException {
		for (CLSChallenge clsChallenge : findByChallengeTitle(challengeTitle,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(clsChallenge);
		}
	}

	/**
	 * Returns the number of c l s challenges where challengeTitle = &#63;.
	 *
	 * @param challengeTitle the challenge title
	 * @return the number of matching c l s challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByChallengeTitle(String challengeTitle)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CHALLENGETITLE;

		Object[] finderArgs = new Object[] { challengeTitle };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLSCHALLENGE_WHERE);

			boolean bindChallengeTitle = false;

			if (challengeTitle == null) {
				query.append(_FINDER_COLUMN_CHALLENGETITLE_CHALLENGETITLE_1);
			}
			else if (challengeTitle.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CHALLENGETITLE_CHALLENGETITLE_3);
			}
			else {
				bindChallengeTitle = true;

				query.append(_FINDER_COLUMN_CHALLENGETITLE_CHALLENGETITLE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindChallengeTitle) {
					qPos.add(challengeTitle);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CHALLENGETITLE_CHALLENGETITLE_1 = "clsChallenge.challengeTitle IS NULL";
	private static final String _FINDER_COLUMN_CHALLENGETITLE_CHALLENGETITLE_2 = "clsChallenge.challengeTitle = ?";
	private static final String _FINDER_COLUMN_CHALLENGETITLE_CHALLENGETITLE_3 = "(clsChallenge.challengeTitle IS NULL OR clsChallenge.challengeTitle = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CHALLENGEUSERID =
		new FinderPath(CLSChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengeModelImpl.FINDER_CACHE_ENABLED, CLSChallengeImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByChallengeUserId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEUSERID =
		new FinderPath(CLSChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengeModelImpl.FINDER_CACHE_ENABLED, CLSChallengeImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByChallengeUserId",
			new String[] { Long.class.getName() },
			CLSChallengeModelImpl.USERID_COLUMN_BITMASK |
			CLSChallengeModelImpl.DATEADDED_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CHALLENGEUSERID = new FinderPath(CLSChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengeModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByChallengeUserId", new String[] { Long.class.getName() });

	/**
	 * Returns all the c l s challenges where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching c l s challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSChallenge> findByChallengeUserId(long userId)
		throws SystemException {
		return findByChallengeUserId(userId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s challenges where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of c l s challenges
	 * @param end the upper bound of the range of c l s challenges (not inclusive)
	 * @return the range of matching c l s challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSChallenge> findByChallengeUserId(long userId, int start,
		int end) throws SystemException {
		return findByChallengeUserId(userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s challenges where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of c l s challenges
	 * @param end the upper bound of the range of c l s challenges (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSChallenge> findByChallengeUserId(long userId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEUSERID;
			finderArgs = new Object[] { userId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CHALLENGEUSERID;
			finderArgs = new Object[] { userId, start, end, orderByComparator };
		}

		List<CLSChallenge> list = (List<CLSChallenge>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSChallenge clsChallenge : list) {
				if ((userId != clsChallenge.getUserId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CLSCHALLENGE_WHERE);

			query.append(_FINDER_COLUMN_CHALLENGEUSERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSChallengeModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				if (!pagination) {
					list = (List<CLSChallenge>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSChallenge>(list);
				}
				else {
					list = (List<CLSChallenge>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s challenge in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengeException if a matching c l s challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallenge findByChallengeUserId_First(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSChallengeException, SystemException {
		CLSChallenge clsChallenge = fetchByChallengeUserId_First(userId,
				orderByComparator);

		if (clsChallenge != null) {
			return clsChallenge;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSChallengeException(msg.toString());
	}

	/**
	 * Returns the first c l s challenge in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s challenge, or <code>null</code> if a matching c l s challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallenge fetchByChallengeUserId_First(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		List<CLSChallenge> list = findByChallengeUserId(userId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s challenge in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengeException if a matching c l s challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallenge findByChallengeUserId_Last(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSChallengeException, SystemException {
		CLSChallenge clsChallenge = fetchByChallengeUserId_Last(userId,
				orderByComparator);

		if (clsChallenge != null) {
			return clsChallenge;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSChallengeException(msg.toString());
	}

	/**
	 * Returns the last c l s challenge in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s challenge, or <code>null</code> if a matching c l s challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallenge fetchByChallengeUserId_Last(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByChallengeUserId(userId);

		if (count == 0) {
			return null;
		}

		List<CLSChallenge> list = findByChallengeUserId(userId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s challenges before and after the current c l s challenge in the ordered set where userId = &#63;.
	 *
	 * @param challengeId the primary key of the current c l s challenge
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengeException if a c l s challenge with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallenge[] findByChallengeUserId_PrevAndNext(long challengeId,
		long userId, OrderByComparator orderByComparator)
		throws NoSuchCLSChallengeException, SystemException {
		CLSChallenge clsChallenge = findByPrimaryKey(challengeId);

		Session session = null;

		try {
			session = openSession();

			CLSChallenge[] array = new CLSChallengeImpl[3];

			array[0] = getByChallengeUserId_PrevAndNext(session, clsChallenge,
					userId, orderByComparator, true);

			array[1] = clsChallenge;

			array[2] = getByChallengeUserId_PrevAndNext(session, clsChallenge,
					userId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSChallenge getByChallengeUserId_PrevAndNext(Session session,
		CLSChallenge clsChallenge, long userId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSCHALLENGE_WHERE);

		query.append(_FINDER_COLUMN_CHALLENGEUSERID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSChallengeModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsChallenge);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSChallenge> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s challenges where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByChallengeUserId(long userId) throws SystemException {
		for (CLSChallenge clsChallenge : findByChallengeUserId(userId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(clsChallenge);
		}
	}

	/**
	 * Returns the number of c l s challenges where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching c l s challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByChallengeUserId(long userId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CHALLENGEUSERID;

		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLSCHALLENGE_WHERE);

			query.append(_FINDER_COLUMN_CHALLENGEUSERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CHALLENGEUSERID_USERID_2 = "clsChallenge.userId = ?";

	public CLSChallengePersistenceImpl() {
		setModelClass(CLSChallenge.class);
	}

	/**
	 * Caches the c l s challenge in the entity cache if it is enabled.
	 *
	 * @param clsChallenge the c l s challenge
	 */
	@Override
	public void cacheResult(CLSChallenge clsChallenge) {
		EntityCacheUtil.putResult(CLSChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengeImpl.class, clsChallenge.getPrimaryKey(), clsChallenge);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G,
			new Object[] { clsChallenge.getUuid(), clsChallenge.getGroupId() },
			clsChallenge);

		clsChallenge.resetOriginalValues();
	}

	/**
	 * Caches the c l s challenges in the entity cache if it is enabled.
	 *
	 * @param clsChallenges the c l s challenges
	 */
	@Override
	public void cacheResult(List<CLSChallenge> clsChallenges) {
		for (CLSChallenge clsChallenge : clsChallenges) {
			if (EntityCacheUtil.getResult(
						CLSChallengeModelImpl.ENTITY_CACHE_ENABLED,
						CLSChallengeImpl.class, clsChallenge.getPrimaryKey()) == null) {
				cacheResult(clsChallenge);
			}
			else {
				clsChallenge.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all c l s challenges.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CLSChallengeImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CLSChallengeImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the c l s challenge.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CLSChallenge clsChallenge) {
		EntityCacheUtil.removeResult(CLSChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengeImpl.class, clsChallenge.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(clsChallenge);
	}

	@Override
	public void clearCache(List<CLSChallenge> clsChallenges) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CLSChallenge clsChallenge : clsChallenges) {
			EntityCacheUtil.removeResult(CLSChallengeModelImpl.ENTITY_CACHE_ENABLED,
				CLSChallengeImpl.class, clsChallenge.getPrimaryKey());

			clearUniqueFindersCache(clsChallenge);
		}
	}

	protected void cacheUniqueFindersCache(CLSChallenge clsChallenge) {
		if (clsChallenge.isNew()) {
			Object[] args = new Object[] {
					clsChallenge.getUuid(), clsChallenge.getGroupId()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_UUID_G, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G, args,
				clsChallenge);
		}
		else {
			CLSChallengeModelImpl clsChallengeModelImpl = (CLSChallengeModelImpl)clsChallenge;

			if ((clsChallengeModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsChallenge.getUuid(), clsChallenge.getGroupId()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_UUID_G, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G, args,
					clsChallenge);
			}
		}
	}

	protected void clearUniqueFindersCache(CLSChallenge clsChallenge) {
		CLSChallengeModelImpl clsChallengeModelImpl = (CLSChallengeModelImpl)clsChallenge;

		Object[] args = new Object[] {
				clsChallenge.getUuid(), clsChallenge.getGroupId()
			};

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);

		if ((clsChallengeModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
			args = new Object[] {
					clsChallengeModelImpl.getOriginalUuid(),
					clsChallengeModelImpl.getOriginalGroupId()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);
		}
	}

	/**
	 * Creates a new c l s challenge with the primary key. Does not add the c l s challenge to the database.
	 *
	 * @param challengeId the primary key for the new c l s challenge
	 * @return the new c l s challenge
	 */
	@Override
	public CLSChallenge create(long challengeId) {
		CLSChallenge clsChallenge = new CLSChallengeImpl();

		clsChallenge.setNew(true);
		clsChallenge.setPrimaryKey(challengeId);

		String uuid = PortalUUIDUtil.generate();

		clsChallenge.setUuid(uuid);

		return clsChallenge;
	}

	/**
	 * Removes the c l s challenge with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param challengeId the primary key of the c l s challenge
	 * @return the c l s challenge that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengeException if a c l s challenge with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallenge remove(long challengeId)
		throws NoSuchCLSChallengeException, SystemException {
		return remove((Serializable)challengeId);
	}

	/**
	 * Removes the c l s challenge with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the c l s challenge
	 * @return the c l s challenge that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengeException if a c l s challenge with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallenge remove(Serializable primaryKey)
		throws NoSuchCLSChallengeException, SystemException {
		Session session = null;

		try {
			session = openSession();

			CLSChallenge clsChallenge = (CLSChallenge)session.get(CLSChallengeImpl.class,
					primaryKey);

			if (clsChallenge == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCLSChallengeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(clsChallenge);
		}
		catch (NoSuchCLSChallengeException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CLSChallenge removeImpl(CLSChallenge clsChallenge)
		throws SystemException {
		clsChallenge = toUnwrappedModel(clsChallenge);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(clsChallenge)) {
				clsChallenge = (CLSChallenge)session.get(CLSChallengeImpl.class,
						clsChallenge.getPrimaryKeyObj());
			}

			if (clsChallenge != null) {
				session.delete(clsChallenge);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (clsChallenge != null) {
			clearCache(clsChallenge);
		}

		return clsChallenge;
	}

	@Override
	public CLSChallenge updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge clsChallenge)
		throws SystemException {
		clsChallenge = toUnwrappedModel(clsChallenge);

		boolean isNew = clsChallenge.isNew();

		CLSChallengeModelImpl clsChallengeModelImpl = (CLSChallengeModelImpl)clsChallenge;

		if (Validator.isNull(clsChallenge.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			clsChallenge.setUuid(uuid);
		}

		Session session = null;

		try {
			session = openSession();

			if (clsChallenge.isNew()) {
				session.save(clsChallenge);

				clsChallenge.setNew(false);
			}
			else {
				session.merge(clsChallenge);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !CLSChallengeModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((clsChallengeModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsChallengeModelImpl.getOriginalUuid()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);

				args = new Object[] { clsChallengeModelImpl.getUuid() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);
			}

			if ((clsChallengeModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsChallengeModelImpl.getOriginalUuid(),
						clsChallengeModelImpl.getOriginalCompanyId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);

				args = new Object[] {
						clsChallengeModelImpl.getUuid(),
						clsChallengeModelImpl.getCompanyId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);
			}

			if ((clsChallengeModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGETITLE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsChallengeModelImpl.getOriginalChallengeTitle()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CHALLENGETITLE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGETITLE,
					args);

				args = new Object[] { clsChallengeModelImpl.getChallengeTitle() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CHALLENGETITLE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGETITLE,
					args);
			}

			if ((clsChallengeModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEUSERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsChallengeModelImpl.getOriginalUserId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CHALLENGEUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEUSERID,
					args);

				args = new Object[] { clsChallengeModelImpl.getUserId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CHALLENGEUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEUSERID,
					args);
			}
		}

		EntityCacheUtil.putResult(CLSChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengeImpl.class, clsChallenge.getPrimaryKey(), clsChallenge);

		clearUniqueFindersCache(clsChallenge);
		cacheUniqueFindersCache(clsChallenge);

		return clsChallenge;
	}

	protected CLSChallenge toUnwrappedModel(CLSChallenge clsChallenge) {
		if (clsChallenge instanceof CLSChallengeImpl) {
			return clsChallenge;
		}

		CLSChallengeImpl clsChallengeImpl = new CLSChallengeImpl();

		clsChallengeImpl.setNew(clsChallenge.isNew());
		clsChallengeImpl.setPrimaryKey(clsChallenge.getPrimaryKey());

		clsChallengeImpl.setUuid(clsChallenge.getUuid());
		clsChallengeImpl.setChallengeId(clsChallenge.getChallengeId());
		clsChallengeImpl.setChallengeTitle(clsChallenge.getChallengeTitle());
		clsChallengeImpl.setChallengeDescription(clsChallenge.getChallengeDescription());
		clsChallengeImpl.setChallengeWithReward(clsChallenge.isChallengeWithReward());
		clsChallengeImpl.setChallengeReward(clsChallenge.getChallengeReward());
		clsChallengeImpl.setNumberOfSelectableIdeas(clsChallenge.getNumberOfSelectableIdeas());
		clsChallengeImpl.setDateAdded(clsChallenge.getDateAdded());
		clsChallengeImpl.setChallengeStatus(clsChallenge.getChallengeStatus());
		clsChallengeImpl.setStatus(clsChallenge.getStatus());
		clsChallengeImpl.setStatusByUserId(clsChallenge.getStatusByUserId());
		clsChallengeImpl.setStatusByUserName(clsChallenge.getStatusByUserName());
		clsChallengeImpl.setStatusDate(clsChallenge.getStatusDate());
		clsChallengeImpl.setCompanyId(clsChallenge.getCompanyId());
		clsChallengeImpl.setGroupId(clsChallenge.getGroupId());
		clsChallengeImpl.setUserId(clsChallenge.getUserId());
		clsChallengeImpl.setDateEnd(clsChallenge.getDateEnd());
		clsChallengeImpl.setDateStart(clsChallenge.getDateStart());
		clsChallengeImpl.setLanguage(clsChallenge.getLanguage());
		clsChallengeImpl.setChallengeHashTag(clsChallenge.getChallengeHashTag());
		clsChallengeImpl.setDmFolderName(clsChallenge.getDmFolderName());
		clsChallengeImpl.setIdFolder(clsChallenge.getIdFolder());
		clsChallengeImpl.setRepresentativeImgUrl(clsChallenge.getRepresentativeImgUrl());
		clsChallengeImpl.setCalendarBooking(clsChallenge.getCalendarBooking());

		return clsChallengeImpl;
	}

	/**
	 * Returns the c l s challenge with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the c l s challenge
	 * @return the c l s challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengeException if a c l s challenge with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallenge findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCLSChallengeException, SystemException {
		CLSChallenge clsChallenge = fetchByPrimaryKey(primaryKey);

		if (clsChallenge == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCLSChallengeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return clsChallenge;
	}

	/**
	 * Returns the c l s challenge with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengeException} if it could not be found.
	 *
	 * @param challengeId the primary key of the c l s challenge
	 * @return the c l s challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengeException if a c l s challenge with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallenge findByPrimaryKey(long challengeId)
		throws NoSuchCLSChallengeException, SystemException {
		return findByPrimaryKey((Serializable)challengeId);
	}

	/**
	 * Returns the c l s challenge with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the c l s challenge
	 * @return the c l s challenge, or <code>null</code> if a c l s challenge with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallenge fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		CLSChallenge clsChallenge = (CLSChallenge)EntityCacheUtil.getResult(CLSChallengeModelImpl.ENTITY_CACHE_ENABLED,
				CLSChallengeImpl.class, primaryKey);

		if (clsChallenge == _nullCLSChallenge) {
			return null;
		}

		if (clsChallenge == null) {
			Session session = null;

			try {
				session = openSession();

				clsChallenge = (CLSChallenge)session.get(CLSChallengeImpl.class,
						primaryKey);

				if (clsChallenge != null) {
					cacheResult(clsChallenge);
				}
				else {
					EntityCacheUtil.putResult(CLSChallengeModelImpl.ENTITY_CACHE_ENABLED,
						CLSChallengeImpl.class, primaryKey, _nullCLSChallenge);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(CLSChallengeModelImpl.ENTITY_CACHE_ENABLED,
					CLSChallengeImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return clsChallenge;
	}

	/**
	 * Returns the c l s challenge with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param challengeId the primary key of the c l s challenge
	 * @return the c l s challenge, or <code>null</code> if a c l s challenge with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallenge fetchByPrimaryKey(long challengeId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)challengeId);
	}

	/**
	 * Returns all the c l s challenges.
	 *
	 * @return the c l s challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSChallenge> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s challenges.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of c l s challenges
	 * @param end the upper bound of the range of c l s challenges (not inclusive)
	 * @return the range of c l s challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSChallenge> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s challenges.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of c l s challenges
	 * @param end the upper bound of the range of c l s challenges (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of c l s challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSChallenge> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CLSChallenge> list = (List<CLSChallenge>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CLSCHALLENGE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CLSCHALLENGE;

				if (pagination) {
					sql = sql.concat(CLSChallengeModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<CLSChallenge>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSChallenge>(list);
				}
				else {
					list = (List<CLSChallenge>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the c l s challenges from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (CLSChallenge clsChallenge : findAll()) {
			remove(clsChallenge);
		}
	}

	/**
	 * Returns the number of c l s challenges.
	 *
	 * @return the number of c l s challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CLSCHALLENGE);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the c l s challenge persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<CLSChallenge>> listenersList = new ArrayList<ModelListener<CLSChallenge>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<CLSChallenge>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CLSChallengeImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_CLSCHALLENGE = "SELECT clsChallenge FROM CLSChallenge clsChallenge";
	private static final String _SQL_SELECT_CLSCHALLENGE_WHERE = "SELECT clsChallenge FROM CLSChallenge clsChallenge WHERE ";
	private static final String _SQL_COUNT_CLSCHALLENGE = "SELECT COUNT(clsChallenge) FROM CLSChallenge clsChallenge";
	private static final String _SQL_COUNT_CLSCHALLENGE_WHERE = "SELECT COUNT(clsChallenge) FROM CLSChallenge clsChallenge WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "clsChallenge.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CLSChallenge exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No CLSChallenge exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CLSChallengePersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"uuid"
			});
	private static CLSChallenge _nullCLSChallenge = new CLSChallengeImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<CLSChallenge> toCacheModel() {
				return _nullCLSChallengeCacheModel;
			}
		};

	private static CacheModel<CLSChallenge> _nullCLSChallengeCacheModel = new CacheModel<CLSChallenge>() {
			@Override
			public CLSChallenge toEntityModel() {
				return _nullCLSChallenge;
			}
		};
}