/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing NeedLinkedChallenge in entity cache.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see NeedLinkedChallenge
 * @generated
 */
public class NeedLinkedChallengeCacheModel implements CacheModel<NeedLinkedChallenge>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{needId=");
		sb.append(needId);
		sb.append(", challengeId=");
		sb.append(challengeId);
		sb.append(", date=");
		sb.append(date);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public NeedLinkedChallenge toEntityModel() {
		NeedLinkedChallengeImpl needLinkedChallengeImpl = new NeedLinkedChallengeImpl();

		needLinkedChallengeImpl.setNeedId(needId);
		needLinkedChallengeImpl.setChallengeId(challengeId);

		if (date == Long.MIN_VALUE) {
			needLinkedChallengeImpl.setDate(null);
		}
		else {
			needLinkedChallengeImpl.setDate(new Date(date));
		}

		needLinkedChallengeImpl.resetOriginalValues();

		return needLinkedChallengeImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		needId = objectInput.readLong();
		challengeId = objectInput.readLong();
		date = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(needId);
		objectOutput.writeLong(challengeId);
		objectOutput.writeLong(date);
	}

	public long needId;
	public long challengeId;
	public long date;
}