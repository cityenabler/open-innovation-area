/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSVmeProjects;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing CLSVmeProjects in entity cache.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSVmeProjects
 * @generated
 */
public class CLSVmeProjectsCacheModel implements CacheModel<CLSVmeProjects>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{recordId=");
		sb.append(recordId);
		sb.append(", vmeProjectId=");
		sb.append(vmeProjectId);
		sb.append(", VmeProjectName=");
		sb.append(VmeProjectName);
		sb.append(", isMockup=");
		sb.append(isMockup);
		sb.append(", ideaId=");
		sb.append(ideaId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public CLSVmeProjects toEntityModel() {
		CLSVmeProjectsImpl clsVmeProjectsImpl = new CLSVmeProjectsImpl();

		clsVmeProjectsImpl.setRecordId(recordId);
		clsVmeProjectsImpl.setVmeProjectId(vmeProjectId);

		if (VmeProjectName == null) {
			clsVmeProjectsImpl.setVmeProjectName(StringPool.BLANK);
		}
		else {
			clsVmeProjectsImpl.setVmeProjectName(VmeProjectName);
		}

		clsVmeProjectsImpl.setIsMockup(isMockup);
		clsVmeProjectsImpl.setIdeaId(ideaId);

		clsVmeProjectsImpl.resetOriginalValues();

		return clsVmeProjectsImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		recordId = objectInput.readLong();
		vmeProjectId = objectInput.readLong();
		VmeProjectName = objectInput.readUTF();
		isMockup = objectInput.readBoolean();
		ideaId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(recordId);
		objectOutput.writeLong(vmeProjectId);

		if (VmeProjectName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(VmeProjectName);
		}

		objectOutput.writeBoolean(isMockup);
		objectOutput.writeLong(ideaId);
	}

	public long recordId;
	public long vmeProjectId;
	public String VmeProjectName;
	public boolean isMockup;
	public long ideaId;
}