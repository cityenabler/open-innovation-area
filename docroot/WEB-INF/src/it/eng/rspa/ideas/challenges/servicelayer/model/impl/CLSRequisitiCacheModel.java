/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing CLSRequisiti in entity cache.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSRequisiti
 * @generated
 */
public class CLSRequisitiCacheModel implements CacheModel<CLSRequisiti>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(23);

		sb.append("{requisitoId=");
		sb.append(requisitoId);
		sb.append(", ideaId=");
		sb.append(ideaId);
		sb.append(", taskId=");
		sb.append(taskId);
		sb.append(", tipo=");
		sb.append(tipo);
		sb.append(", descrizione=");
		sb.append(descrizione);
		sb.append(", help=");
		sb.append(help);
		sb.append(", authorUser=");
		sb.append(authorUser);
		sb.append(", category=");
		sb.append(category);
		sb.append(", multiTask=");
		sb.append(multiTask);
		sb.append(", outcomeFile=");
		sb.append(outcomeFile);
		sb.append(", taskDoubleField=");
		sb.append(taskDoubleField);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public CLSRequisiti toEntityModel() {
		CLSRequisitiImpl clsRequisitiImpl = new CLSRequisitiImpl();

		clsRequisitiImpl.setRequisitoId(requisitoId);
		clsRequisitiImpl.setIdeaId(ideaId);
		clsRequisitiImpl.setTaskId(taskId);

		if (tipo == null) {
			clsRequisitiImpl.setTipo(StringPool.BLANK);
		}
		else {
			clsRequisitiImpl.setTipo(tipo);
		}

		if (descrizione == null) {
			clsRequisitiImpl.setDescrizione(StringPool.BLANK);
		}
		else {
			clsRequisitiImpl.setDescrizione(descrizione);
		}

		if (help == null) {
			clsRequisitiImpl.setHelp(StringPool.BLANK);
		}
		else {
			clsRequisitiImpl.setHelp(help);
		}

		clsRequisitiImpl.setAuthorUser(authorUser);

		if (category == null) {
			clsRequisitiImpl.setCategory(StringPool.BLANK);
		}
		else {
			clsRequisitiImpl.setCategory(category);
		}

		clsRequisitiImpl.setMultiTask(multiTask);
		clsRequisitiImpl.setOutcomeFile(outcomeFile);
		clsRequisitiImpl.setTaskDoubleField(taskDoubleField);

		clsRequisitiImpl.resetOriginalValues();

		return clsRequisitiImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		requisitoId = objectInput.readLong();
		ideaId = objectInput.readLong();
		taskId = objectInput.readLong();
		tipo = objectInput.readUTF();
		descrizione = objectInput.readUTF();
		help = objectInput.readUTF();
		authorUser = objectInput.readLong();
		category = objectInput.readUTF();
		multiTask = objectInput.readBoolean();
		outcomeFile = objectInput.readBoolean();
		taskDoubleField = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(requisitoId);
		objectOutput.writeLong(ideaId);
		objectOutput.writeLong(taskId);

		if (tipo == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(tipo);
		}

		if (descrizione == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(descrizione);
		}

		if (help == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(help);
		}

		objectOutput.writeLong(authorUser);

		if (category == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(category);
		}

		objectOutput.writeBoolean(multiTask);
		objectOutput.writeBoolean(outcomeFile);
		objectOutput.writeBoolean(taskDoubleField);
	}

	public long requisitoId;
	public long ideaId;
	public long taskId;
	public String tipo;
	public String descrizione;
	public String help;
	public long authorUser;
	public String category;
	public boolean multiTask;
	public boolean outcomeFile;
	public boolean taskDoubleField;
}