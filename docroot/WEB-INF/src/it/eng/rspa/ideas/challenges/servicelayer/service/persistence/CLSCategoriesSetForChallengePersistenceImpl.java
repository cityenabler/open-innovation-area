/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCategoriesSetForChallengeImpl;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCategoriesSetForChallengeModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the c l s categories set for challenge service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSCategoriesSetForChallengePersistence
 * @see CLSCategoriesSetForChallengeUtil
 * @generated
 */
public class CLSCategoriesSetForChallengePersistenceImpl
	extends BasePersistenceImpl<CLSCategoriesSetForChallenge>
	implements CLSCategoriesSetForChallengePersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CLSCategoriesSetForChallengeUtil} to access the c l s categories set for challenge persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CLSCategoriesSetForChallengeImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CLSCategoriesSetForChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSCategoriesSetForChallengeModelImpl.FINDER_CACHE_ENABLED,
			CLSCategoriesSetForChallengeImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CLSCategoriesSetForChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSCategoriesSetForChallengeModelImpl.FINDER_CACHE_ENABLED,
			CLSCategoriesSetForChallengeImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CLSCategoriesSetForChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSCategoriesSetForChallengeModelImpl.FINDER_CACHE_ENABLED,
			Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CHALLENGEID =
		new FinderPath(CLSCategoriesSetForChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSCategoriesSetForChallengeModelImpl.FINDER_CACHE_ENABLED,
			CLSCategoriesSetForChallengeImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByChallengeId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEID =
		new FinderPath(CLSCategoriesSetForChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSCategoriesSetForChallengeModelImpl.FINDER_CACHE_ENABLED,
			CLSCategoriesSetForChallengeImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByChallengeId",
			new String[] { Long.class.getName() },
			CLSCategoriesSetForChallengeModelImpl.CHALLENGEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CHALLENGEID = new FinderPath(CLSCategoriesSetForChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSCategoriesSetForChallengeModelImpl.FINDER_CACHE_ENABLED,
			Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByChallengeId", new String[] { Long.class.getName() });

	/**
	 * Returns all the c l s categories set for challenges where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @return the matching c l s categories set for challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSCategoriesSetForChallenge> findByChallengeId(
		long challengeId) throws SystemException {
		return findByChallengeId(challengeId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s categories set for challenges where challengeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCategoriesSetForChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param challengeId the challenge ID
	 * @param start the lower bound of the range of c l s categories set for challenges
	 * @param end the upper bound of the range of c l s categories set for challenges (not inclusive)
	 * @return the range of matching c l s categories set for challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSCategoriesSetForChallenge> findByChallengeId(
		long challengeId, int start, int end) throws SystemException {
		return findByChallengeId(challengeId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s categories set for challenges where challengeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCategoriesSetForChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param challengeId the challenge ID
	 * @param start the lower bound of the range of c l s categories set for challenges
	 * @param end the upper bound of the range of c l s categories set for challenges (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s categories set for challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSCategoriesSetForChallenge> findByChallengeId(
		long challengeId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEID;
			finderArgs = new Object[] { challengeId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CHALLENGEID;
			finderArgs = new Object[] { challengeId, start, end, orderByComparator };
		}

		List<CLSCategoriesSetForChallenge> list = (List<CLSCategoriesSetForChallenge>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSCategoriesSetForChallenge clsCategoriesSetForChallenge : list) {
				if ((challengeId != clsCategoriesSetForChallenge.getChallengeId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CLSCATEGORIESSETFORCHALLENGE_WHERE);

			query.append(_FINDER_COLUMN_CHALLENGEID_CHALLENGEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSCategoriesSetForChallengeModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(challengeId);

				if (!pagination) {
					list = (List<CLSCategoriesSetForChallenge>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSCategoriesSetForChallenge>(list);
				}
				else {
					list = (List<CLSCategoriesSetForChallenge>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s categories set for challenge in the ordered set where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s categories set for challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException if a matching c l s categories set for challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCategoriesSetForChallenge findByChallengeId_First(
		long challengeId, OrderByComparator orderByComparator)
		throws NoSuchCLSCategoriesSetForChallengeException, SystemException {
		CLSCategoriesSetForChallenge clsCategoriesSetForChallenge = fetchByChallengeId_First(challengeId,
				orderByComparator);

		if (clsCategoriesSetForChallenge != null) {
			return clsCategoriesSetForChallenge;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("challengeId=");
		msg.append(challengeId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSCategoriesSetForChallengeException(msg.toString());
	}

	/**
	 * Returns the first c l s categories set for challenge in the ordered set where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s categories set for challenge, or <code>null</code> if a matching c l s categories set for challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCategoriesSetForChallenge fetchByChallengeId_First(
		long challengeId, OrderByComparator orderByComparator)
		throws SystemException {
		List<CLSCategoriesSetForChallenge> list = findByChallengeId(challengeId,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s categories set for challenge in the ordered set where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s categories set for challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException if a matching c l s categories set for challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCategoriesSetForChallenge findByChallengeId_Last(
		long challengeId, OrderByComparator orderByComparator)
		throws NoSuchCLSCategoriesSetForChallengeException, SystemException {
		CLSCategoriesSetForChallenge clsCategoriesSetForChallenge = fetchByChallengeId_Last(challengeId,
				orderByComparator);

		if (clsCategoriesSetForChallenge != null) {
			return clsCategoriesSetForChallenge;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("challengeId=");
		msg.append(challengeId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSCategoriesSetForChallengeException(msg.toString());
	}

	/**
	 * Returns the last c l s categories set for challenge in the ordered set where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s categories set for challenge, or <code>null</code> if a matching c l s categories set for challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCategoriesSetForChallenge fetchByChallengeId_Last(
		long challengeId, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByChallengeId(challengeId);

		if (count == 0) {
			return null;
		}

		List<CLSCategoriesSetForChallenge> list = findByChallengeId(challengeId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s categories set for challenges before and after the current c l s categories set for challenge in the ordered set where challengeId = &#63;.
	 *
	 * @param clsCategoriesSetForChallengePK the primary key of the current c l s categories set for challenge
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s categories set for challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException if a c l s categories set for challenge with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCategoriesSetForChallenge[] findByChallengeId_PrevAndNext(
		CLSCategoriesSetForChallengePK clsCategoriesSetForChallengePK,
		long challengeId, OrderByComparator orderByComparator)
		throws NoSuchCLSCategoriesSetForChallengeException, SystemException {
		CLSCategoriesSetForChallenge clsCategoriesSetForChallenge = findByPrimaryKey(clsCategoriesSetForChallengePK);

		Session session = null;

		try {
			session = openSession();

			CLSCategoriesSetForChallenge[] array = new CLSCategoriesSetForChallengeImpl[3];

			array[0] = getByChallengeId_PrevAndNext(session,
					clsCategoriesSetForChallenge, challengeId,
					orderByComparator, true);

			array[1] = clsCategoriesSetForChallenge;

			array[2] = getByChallengeId_PrevAndNext(session,
					clsCategoriesSetForChallenge, challengeId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSCategoriesSetForChallenge getByChallengeId_PrevAndNext(
		Session session,
		CLSCategoriesSetForChallenge clsCategoriesSetForChallenge,
		long challengeId, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSCATEGORIESSETFORCHALLENGE_WHERE);

		query.append(_FINDER_COLUMN_CHALLENGEID_CHALLENGEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSCategoriesSetForChallengeModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(challengeId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsCategoriesSetForChallenge);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSCategoriesSetForChallenge> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s categories set for challenges where challengeId = &#63; from the database.
	 *
	 * @param challengeId the challenge ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByChallengeId(long challengeId) throws SystemException {
		for (CLSCategoriesSetForChallenge clsCategoriesSetForChallenge : findByChallengeId(
				challengeId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(clsCategoriesSetForChallenge);
		}
	}

	/**
	 * Returns the number of c l s categories set for challenges where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @return the number of matching c l s categories set for challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByChallengeId(long challengeId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CHALLENGEID;

		Object[] finderArgs = new Object[] { challengeId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLSCATEGORIESSETFORCHALLENGE_WHERE);

			query.append(_FINDER_COLUMN_CHALLENGEID_CHALLENGEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(challengeId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CHALLENGEID_CHALLENGEID_2 = "clsCategoriesSetForChallenge.id.challengeId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CATEGORYSETID =
		new FinderPath(CLSCategoriesSetForChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSCategoriesSetForChallengeModelImpl.FINDER_CACHE_ENABLED,
			CLSCategoriesSetForChallengeImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCategorySetID",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CATEGORYSETID =
		new FinderPath(CLSCategoriesSetForChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSCategoriesSetForChallengeModelImpl.FINDER_CACHE_ENABLED,
			CLSCategoriesSetForChallengeImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCategorySetID",
			new String[] { Long.class.getName() },
			CLSCategoriesSetForChallengeModelImpl.CATEGORIESSETID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CATEGORYSETID = new FinderPath(CLSCategoriesSetForChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSCategoriesSetForChallengeModelImpl.FINDER_CACHE_ENABLED,
			Long.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByCategorySetID", new String[] { Long.class.getName() });

	/**
	 * Returns all the c l s categories set for challenges where categoriesSetID = &#63;.
	 *
	 * @param categoriesSetID the categories set i d
	 * @return the matching c l s categories set for challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSCategoriesSetForChallenge> findByCategorySetID(
		long categoriesSetID) throws SystemException {
		return findByCategorySetID(categoriesSetID, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s categories set for challenges where categoriesSetID = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCategoriesSetForChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param categoriesSetID the categories set i d
	 * @param start the lower bound of the range of c l s categories set for challenges
	 * @param end the upper bound of the range of c l s categories set for challenges (not inclusive)
	 * @return the range of matching c l s categories set for challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSCategoriesSetForChallenge> findByCategorySetID(
		long categoriesSetID, int start, int end) throws SystemException {
		return findByCategorySetID(categoriesSetID, start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s categories set for challenges where categoriesSetID = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCategoriesSetForChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param categoriesSetID the categories set i d
	 * @param start the lower bound of the range of c l s categories set for challenges
	 * @param end the upper bound of the range of c l s categories set for challenges (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s categories set for challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSCategoriesSetForChallenge> findByCategorySetID(
		long categoriesSetID, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CATEGORYSETID;
			finderArgs = new Object[] { categoriesSetID };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CATEGORYSETID;
			finderArgs = new Object[] {
					categoriesSetID,
					
					start, end, orderByComparator
				};
		}

		List<CLSCategoriesSetForChallenge> list = (List<CLSCategoriesSetForChallenge>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSCategoriesSetForChallenge clsCategoriesSetForChallenge : list) {
				if ((categoriesSetID != clsCategoriesSetForChallenge.getCategoriesSetID())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CLSCATEGORIESSETFORCHALLENGE_WHERE);

			query.append(_FINDER_COLUMN_CATEGORYSETID_CATEGORIESSETID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSCategoriesSetForChallengeModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(categoriesSetID);

				if (!pagination) {
					list = (List<CLSCategoriesSetForChallenge>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSCategoriesSetForChallenge>(list);
				}
				else {
					list = (List<CLSCategoriesSetForChallenge>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s categories set for challenge in the ordered set where categoriesSetID = &#63;.
	 *
	 * @param categoriesSetID the categories set i d
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s categories set for challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException if a matching c l s categories set for challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCategoriesSetForChallenge findByCategorySetID_First(
		long categoriesSetID, OrderByComparator orderByComparator)
		throws NoSuchCLSCategoriesSetForChallengeException, SystemException {
		CLSCategoriesSetForChallenge clsCategoriesSetForChallenge = fetchByCategorySetID_First(categoriesSetID,
				orderByComparator);

		if (clsCategoriesSetForChallenge != null) {
			return clsCategoriesSetForChallenge;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("categoriesSetID=");
		msg.append(categoriesSetID);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSCategoriesSetForChallengeException(msg.toString());
	}

	/**
	 * Returns the first c l s categories set for challenge in the ordered set where categoriesSetID = &#63;.
	 *
	 * @param categoriesSetID the categories set i d
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s categories set for challenge, or <code>null</code> if a matching c l s categories set for challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCategoriesSetForChallenge fetchByCategorySetID_First(
		long categoriesSetID, OrderByComparator orderByComparator)
		throws SystemException {
		List<CLSCategoriesSetForChallenge> list = findByCategorySetID(categoriesSetID,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s categories set for challenge in the ordered set where categoriesSetID = &#63;.
	 *
	 * @param categoriesSetID the categories set i d
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s categories set for challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException if a matching c l s categories set for challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCategoriesSetForChallenge findByCategorySetID_Last(
		long categoriesSetID, OrderByComparator orderByComparator)
		throws NoSuchCLSCategoriesSetForChallengeException, SystemException {
		CLSCategoriesSetForChallenge clsCategoriesSetForChallenge = fetchByCategorySetID_Last(categoriesSetID,
				orderByComparator);

		if (clsCategoriesSetForChallenge != null) {
			return clsCategoriesSetForChallenge;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("categoriesSetID=");
		msg.append(categoriesSetID);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSCategoriesSetForChallengeException(msg.toString());
	}

	/**
	 * Returns the last c l s categories set for challenge in the ordered set where categoriesSetID = &#63;.
	 *
	 * @param categoriesSetID the categories set i d
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s categories set for challenge, or <code>null</code> if a matching c l s categories set for challenge could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCategoriesSetForChallenge fetchByCategorySetID_Last(
		long categoriesSetID, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByCategorySetID(categoriesSetID);

		if (count == 0) {
			return null;
		}

		List<CLSCategoriesSetForChallenge> list = findByCategorySetID(categoriesSetID,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s categories set for challenges before and after the current c l s categories set for challenge in the ordered set where categoriesSetID = &#63;.
	 *
	 * @param clsCategoriesSetForChallengePK the primary key of the current c l s categories set for challenge
	 * @param categoriesSetID the categories set i d
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s categories set for challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException if a c l s categories set for challenge with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCategoriesSetForChallenge[] findByCategorySetID_PrevAndNext(
		CLSCategoriesSetForChallengePK clsCategoriesSetForChallengePK,
		long categoriesSetID, OrderByComparator orderByComparator)
		throws NoSuchCLSCategoriesSetForChallengeException, SystemException {
		CLSCategoriesSetForChallenge clsCategoriesSetForChallenge = findByPrimaryKey(clsCategoriesSetForChallengePK);

		Session session = null;

		try {
			session = openSession();

			CLSCategoriesSetForChallenge[] array = new CLSCategoriesSetForChallengeImpl[3];

			array[0] = getByCategorySetID_PrevAndNext(session,
					clsCategoriesSetForChallenge, categoriesSetID,
					orderByComparator, true);

			array[1] = clsCategoriesSetForChallenge;

			array[2] = getByCategorySetID_PrevAndNext(session,
					clsCategoriesSetForChallenge, categoriesSetID,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSCategoriesSetForChallenge getByCategorySetID_PrevAndNext(
		Session session,
		CLSCategoriesSetForChallenge clsCategoriesSetForChallenge,
		long categoriesSetID, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSCATEGORIESSETFORCHALLENGE_WHERE);

		query.append(_FINDER_COLUMN_CATEGORYSETID_CATEGORIESSETID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSCategoriesSetForChallengeModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(categoriesSetID);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsCategoriesSetForChallenge);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSCategoriesSetForChallenge> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s categories set for challenges where categoriesSetID = &#63; from the database.
	 *
	 * @param categoriesSetID the categories set i d
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByCategorySetID(long categoriesSetID)
		throws SystemException {
		for (CLSCategoriesSetForChallenge clsCategoriesSetForChallenge : findByCategorySetID(
				categoriesSetID, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(clsCategoriesSetForChallenge);
		}
	}

	/**
	 * Returns the number of c l s categories set for challenges where categoriesSetID = &#63;.
	 *
	 * @param categoriesSetID the categories set i d
	 * @return the number of matching c l s categories set for challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByCategorySetID(long categoriesSetID)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CATEGORYSETID;

		Object[] finderArgs = new Object[] { categoriesSetID };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLSCATEGORIESSETFORCHALLENGE_WHERE);

			query.append(_FINDER_COLUMN_CATEGORYSETID_CATEGORIESSETID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(categoriesSetID);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CATEGORYSETID_CATEGORIESSETID_2 = "clsCategoriesSetForChallenge.id.categoriesSetID = ?";

	public CLSCategoriesSetForChallengePersistenceImpl() {
		setModelClass(CLSCategoriesSetForChallenge.class);
	}

	/**
	 * Caches the c l s categories set for challenge in the entity cache if it is enabled.
	 *
	 * @param clsCategoriesSetForChallenge the c l s categories set for challenge
	 */
	@Override
	public void cacheResult(
		CLSCategoriesSetForChallenge clsCategoriesSetForChallenge) {
		EntityCacheUtil.putResult(CLSCategoriesSetForChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSCategoriesSetForChallengeImpl.class,
			clsCategoriesSetForChallenge.getPrimaryKey(),
			clsCategoriesSetForChallenge);

		clsCategoriesSetForChallenge.resetOriginalValues();
	}

	/**
	 * Caches the c l s categories set for challenges in the entity cache if it is enabled.
	 *
	 * @param clsCategoriesSetForChallenges the c l s categories set for challenges
	 */
	@Override
	public void cacheResult(
		List<CLSCategoriesSetForChallenge> clsCategoriesSetForChallenges) {
		for (CLSCategoriesSetForChallenge clsCategoriesSetForChallenge : clsCategoriesSetForChallenges) {
			if (EntityCacheUtil.getResult(
						CLSCategoriesSetForChallengeModelImpl.ENTITY_CACHE_ENABLED,
						CLSCategoriesSetForChallengeImpl.class,
						clsCategoriesSetForChallenge.getPrimaryKey()) == null) {
				cacheResult(clsCategoriesSetForChallenge);
			}
			else {
				clsCategoriesSetForChallenge.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all c l s categories set for challenges.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CLSCategoriesSetForChallengeImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CLSCategoriesSetForChallengeImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the c l s categories set for challenge.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(
		CLSCategoriesSetForChallenge clsCategoriesSetForChallenge) {
		EntityCacheUtil.removeResult(CLSCategoriesSetForChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSCategoriesSetForChallengeImpl.class,
			clsCategoriesSetForChallenge.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(
		List<CLSCategoriesSetForChallenge> clsCategoriesSetForChallenges) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CLSCategoriesSetForChallenge clsCategoriesSetForChallenge : clsCategoriesSetForChallenges) {
			EntityCacheUtil.removeResult(CLSCategoriesSetForChallengeModelImpl.ENTITY_CACHE_ENABLED,
				CLSCategoriesSetForChallengeImpl.class,
				clsCategoriesSetForChallenge.getPrimaryKey());
		}
	}

	/**
	 * Creates a new c l s categories set for challenge with the primary key. Does not add the c l s categories set for challenge to the database.
	 *
	 * @param clsCategoriesSetForChallengePK the primary key for the new c l s categories set for challenge
	 * @return the new c l s categories set for challenge
	 */
	@Override
	public CLSCategoriesSetForChallenge create(
		CLSCategoriesSetForChallengePK clsCategoriesSetForChallengePK) {
		CLSCategoriesSetForChallenge clsCategoriesSetForChallenge = new CLSCategoriesSetForChallengeImpl();

		clsCategoriesSetForChallenge.setNew(true);
		clsCategoriesSetForChallenge.setPrimaryKey(clsCategoriesSetForChallengePK);

		return clsCategoriesSetForChallenge;
	}

	/**
	 * Removes the c l s categories set for challenge with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param clsCategoriesSetForChallengePK the primary key of the c l s categories set for challenge
	 * @return the c l s categories set for challenge that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException if a c l s categories set for challenge with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCategoriesSetForChallenge remove(
		CLSCategoriesSetForChallengePK clsCategoriesSetForChallengePK)
		throws NoSuchCLSCategoriesSetForChallengeException, SystemException {
		return remove((Serializable)clsCategoriesSetForChallengePK);
	}

	/**
	 * Removes the c l s categories set for challenge with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the c l s categories set for challenge
	 * @return the c l s categories set for challenge that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException if a c l s categories set for challenge with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCategoriesSetForChallenge remove(Serializable primaryKey)
		throws NoSuchCLSCategoriesSetForChallengeException, SystemException {
		Session session = null;

		try {
			session = openSession();

			CLSCategoriesSetForChallenge clsCategoriesSetForChallenge = (CLSCategoriesSetForChallenge)session.get(CLSCategoriesSetForChallengeImpl.class,
					primaryKey);

			if (clsCategoriesSetForChallenge == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCLSCategoriesSetForChallengeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(clsCategoriesSetForChallenge);
		}
		catch (NoSuchCLSCategoriesSetForChallengeException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CLSCategoriesSetForChallenge removeImpl(
		CLSCategoriesSetForChallenge clsCategoriesSetForChallenge)
		throws SystemException {
		clsCategoriesSetForChallenge = toUnwrappedModel(clsCategoriesSetForChallenge);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(clsCategoriesSetForChallenge)) {
				clsCategoriesSetForChallenge = (CLSCategoriesSetForChallenge)session.get(CLSCategoriesSetForChallengeImpl.class,
						clsCategoriesSetForChallenge.getPrimaryKeyObj());
			}

			if (clsCategoriesSetForChallenge != null) {
				session.delete(clsCategoriesSetForChallenge);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (clsCategoriesSetForChallenge != null) {
			clearCache(clsCategoriesSetForChallenge);
		}

		return clsCategoriesSetForChallenge;
	}

	@Override
	public CLSCategoriesSetForChallenge updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge clsCategoriesSetForChallenge)
		throws SystemException {
		clsCategoriesSetForChallenge = toUnwrappedModel(clsCategoriesSetForChallenge);

		boolean isNew = clsCategoriesSetForChallenge.isNew();

		CLSCategoriesSetForChallengeModelImpl clsCategoriesSetForChallengeModelImpl =
			(CLSCategoriesSetForChallengeModelImpl)clsCategoriesSetForChallenge;

		Session session = null;

		try {
			session = openSession();

			if (clsCategoriesSetForChallenge.isNew()) {
				session.save(clsCategoriesSetForChallenge);

				clsCategoriesSetForChallenge.setNew(false);
			}
			else {
				session.merge(clsCategoriesSetForChallenge);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew ||
				!CLSCategoriesSetForChallengeModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((clsCategoriesSetForChallengeModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsCategoriesSetForChallengeModelImpl.getOriginalChallengeId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CHALLENGEID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEID,
					args);

				args = new Object[] {
						clsCategoriesSetForChallengeModelImpl.getChallengeId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CHALLENGEID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEID,
					args);
			}

			if ((clsCategoriesSetForChallengeModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CATEGORYSETID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsCategoriesSetForChallengeModelImpl.getOriginalCategoriesSetID()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CATEGORYSETID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CATEGORYSETID,
					args);

				args = new Object[] {
						clsCategoriesSetForChallengeModelImpl.getCategoriesSetID()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CATEGORYSETID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CATEGORYSETID,
					args);
			}
		}

		EntityCacheUtil.putResult(CLSCategoriesSetForChallengeModelImpl.ENTITY_CACHE_ENABLED,
			CLSCategoriesSetForChallengeImpl.class,
			clsCategoriesSetForChallenge.getPrimaryKey(),
			clsCategoriesSetForChallenge);

		return clsCategoriesSetForChallenge;
	}

	protected CLSCategoriesSetForChallenge toUnwrappedModel(
		CLSCategoriesSetForChallenge clsCategoriesSetForChallenge) {
		if (clsCategoriesSetForChallenge instanceof CLSCategoriesSetForChallengeImpl) {
			return clsCategoriesSetForChallenge;
		}

		CLSCategoriesSetForChallengeImpl clsCategoriesSetForChallengeImpl = new CLSCategoriesSetForChallengeImpl();

		clsCategoriesSetForChallengeImpl.setNew(clsCategoriesSetForChallenge.isNew());
		clsCategoriesSetForChallengeImpl.setPrimaryKey(clsCategoriesSetForChallenge.getPrimaryKey());

		clsCategoriesSetForChallengeImpl.setCategoriesSetID(clsCategoriesSetForChallenge.getCategoriesSetID());
		clsCategoriesSetForChallengeImpl.setChallengeId(clsCategoriesSetForChallenge.getChallengeId());
		clsCategoriesSetForChallengeImpl.setType(clsCategoriesSetForChallenge.getType());

		return clsCategoriesSetForChallengeImpl;
	}

	/**
	 * Returns the c l s categories set for challenge with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the c l s categories set for challenge
	 * @return the c l s categories set for challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException if a c l s categories set for challenge with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCategoriesSetForChallenge findByPrimaryKey(
		Serializable primaryKey)
		throws NoSuchCLSCategoriesSetForChallengeException, SystemException {
		CLSCategoriesSetForChallenge clsCategoriesSetForChallenge = fetchByPrimaryKey(primaryKey);

		if (clsCategoriesSetForChallenge == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCLSCategoriesSetForChallengeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return clsCategoriesSetForChallenge;
	}

	/**
	 * Returns the c l s categories set for challenge with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException} if it could not be found.
	 *
	 * @param clsCategoriesSetForChallengePK the primary key of the c l s categories set for challenge
	 * @return the c l s categories set for challenge
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSCategoriesSetForChallengeException if a c l s categories set for challenge with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCategoriesSetForChallenge findByPrimaryKey(
		CLSCategoriesSetForChallengePK clsCategoriesSetForChallengePK)
		throws NoSuchCLSCategoriesSetForChallengeException, SystemException {
		return findByPrimaryKey((Serializable)clsCategoriesSetForChallengePK);
	}

	/**
	 * Returns the c l s categories set for challenge with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the c l s categories set for challenge
	 * @return the c l s categories set for challenge, or <code>null</code> if a c l s categories set for challenge with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCategoriesSetForChallenge fetchByPrimaryKey(
		Serializable primaryKey) throws SystemException {
		CLSCategoriesSetForChallenge clsCategoriesSetForChallenge = (CLSCategoriesSetForChallenge)EntityCacheUtil.getResult(CLSCategoriesSetForChallengeModelImpl.ENTITY_CACHE_ENABLED,
				CLSCategoriesSetForChallengeImpl.class, primaryKey);

		if (clsCategoriesSetForChallenge == _nullCLSCategoriesSetForChallenge) {
			return null;
		}

		if (clsCategoriesSetForChallenge == null) {
			Session session = null;

			try {
				session = openSession();

				clsCategoriesSetForChallenge = (CLSCategoriesSetForChallenge)session.get(CLSCategoriesSetForChallengeImpl.class,
						primaryKey);

				if (clsCategoriesSetForChallenge != null) {
					cacheResult(clsCategoriesSetForChallenge);
				}
				else {
					EntityCacheUtil.putResult(CLSCategoriesSetForChallengeModelImpl.ENTITY_CACHE_ENABLED,
						CLSCategoriesSetForChallengeImpl.class, primaryKey,
						_nullCLSCategoriesSetForChallenge);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(CLSCategoriesSetForChallengeModelImpl.ENTITY_CACHE_ENABLED,
					CLSCategoriesSetForChallengeImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return clsCategoriesSetForChallenge;
	}

	/**
	 * Returns the c l s categories set for challenge with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param clsCategoriesSetForChallengePK the primary key of the c l s categories set for challenge
	 * @return the c l s categories set for challenge, or <code>null</code> if a c l s categories set for challenge with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSCategoriesSetForChallenge fetchByPrimaryKey(
		CLSCategoriesSetForChallengePK clsCategoriesSetForChallengePK)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)clsCategoriesSetForChallengePK);
	}

	/**
	 * Returns all the c l s categories set for challenges.
	 *
	 * @return the c l s categories set for challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSCategoriesSetForChallenge> findAll()
		throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s categories set for challenges.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCategoriesSetForChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of c l s categories set for challenges
	 * @param end the upper bound of the range of c l s categories set for challenges (not inclusive)
	 * @return the range of c l s categories set for challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSCategoriesSetForChallenge> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s categories set for challenges.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSCategoriesSetForChallengeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of c l s categories set for challenges
	 * @param end the upper bound of the range of c l s categories set for challenges (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of c l s categories set for challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSCategoriesSetForChallenge> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CLSCategoriesSetForChallenge> list = (List<CLSCategoriesSetForChallenge>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CLSCATEGORIESSETFORCHALLENGE);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CLSCATEGORIESSETFORCHALLENGE;

				if (pagination) {
					sql = sql.concat(CLSCategoriesSetForChallengeModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<CLSCategoriesSetForChallenge>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSCategoriesSetForChallenge>(list);
				}
				else {
					list = (List<CLSCategoriesSetForChallenge>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the c l s categories set for challenges from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (CLSCategoriesSetForChallenge clsCategoriesSetForChallenge : findAll()) {
			remove(clsCategoriesSetForChallenge);
		}
	}

	/**
	 * Returns the number of c l s categories set for challenges.
	 *
	 * @return the number of c l s categories set for challenges
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CLSCATEGORIESSETFORCHALLENGE);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the c l s categories set for challenge persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.ideas.challenges.servicelayer.model.CLSCategoriesSetForChallenge")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<CLSCategoriesSetForChallenge>> listenersList = new ArrayList<ModelListener<CLSCategoriesSetForChallenge>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<CLSCategoriesSetForChallenge>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CLSCategoriesSetForChallengeImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_CLSCATEGORIESSETFORCHALLENGE = "SELECT clsCategoriesSetForChallenge FROM CLSCategoriesSetForChallenge clsCategoriesSetForChallenge";
	private static final String _SQL_SELECT_CLSCATEGORIESSETFORCHALLENGE_WHERE = "SELECT clsCategoriesSetForChallenge FROM CLSCategoriesSetForChallenge clsCategoriesSetForChallenge WHERE ";
	private static final String _SQL_COUNT_CLSCATEGORIESSETFORCHALLENGE = "SELECT COUNT(clsCategoriesSetForChallenge) FROM CLSCategoriesSetForChallenge clsCategoriesSetForChallenge";
	private static final String _SQL_COUNT_CLSCATEGORIESSETFORCHALLENGE_WHERE = "SELECT COUNT(clsCategoriesSetForChallenge) FROM CLSCategoriesSetForChallenge clsCategoriesSetForChallenge WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "clsCategoriesSetForChallenge.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CLSCategoriesSetForChallenge exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No CLSCategoriesSetForChallenge exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CLSCategoriesSetForChallengePersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"type"
			});
	private static CLSCategoriesSetForChallenge _nullCLSCategoriesSetForChallenge =
		new CLSCategoriesSetForChallengeImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<CLSCategoriesSetForChallenge> toCacheModel() {
				return _nullCLSCategoriesSetForChallengeCacheModel;
			}
		};

	private static CacheModel<CLSCategoriesSetForChallenge> _nullCLSCategoriesSetForChallengeCacheModel =
		new CacheModel<CLSCategoriesSetForChallenge>() {
			@Override
			public CLSCategoriesSetForChallenge toEntityModel() {
				return _nullCLSCategoriesSetForChallenge;
			}
		};
}