/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

import it.eng.rspa.ideas.challenges.servicelayer.NoSuchEvaluationCriteriaException;
import it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria;
import it.eng.rspa.ideas.challenges.servicelayer.service.EvaluationCriteriaLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.base.EvaluationCriteriaLocalServiceBaseImpl;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.EvaluationCriteriaUtil;

/**
 * The implementation of the evaluation criteria local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.rspa.ideas.challenges.servicelayer.service.EvaluationCriteriaLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.base.EvaluationCriteriaLocalServiceBaseImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.EvaluationCriteriaLocalServiceUtil
 */
public class EvaluationCriteriaLocalServiceImpl
	extends EvaluationCriteriaLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.eng.rspa.ideas.challenges.servicelayer.service.EvaluationCriteriaLocalServiceUtil} to access the evaluation criteria local service.
	 */
	
	
	/** Get criteria by Challenge
	 * @param challengeId
	 * @return
	 */
	public List<EvaluationCriteria> criteriaByChallengeId (long challengeId){
		
		List<EvaluationCriteria> ret = new ArrayList<EvaluationCriteria>();
		
		try {
			return EvaluationCriteriaUtil.findBycriteriaByChallenge(challengeId);
		} catch (SystemException e) {
			
			e.printStackTrace();
			return ret;
		}
	} 
	
	/** Get custom criteria by Language
	 * @param challengeId
	 * @return
	 */
	public List<EvaluationCriteria> getCustomCriteriaByLanguage (String language){
		
		List<EvaluationCriteria> ret = new ArrayList<EvaluationCriteria>();
		
		try {
			return EvaluationCriteriaUtil.findByisCustomAndLanguage(true, language);
		} catch (SystemException e) {
			
			e.printStackTrace();
			return ret;
		}
	} 
	
	
	/** Get criteria for this challenge with this inputId
	 * It is an utility to use on update of Criteria page
	 * @param challengeId
	 * @return
	 */
	public List<EvaluationCriteria> getCriteriaByChallengeIdAndInputId (long challengeId, String inputId){
		
		List<EvaluationCriteria> ret = new ArrayList<EvaluationCriteria>();
		
		try {
			return EvaluationCriteriaUtil.findBychallengeIdAndInputId(challengeId, inputId);
		} catch (SystemException e) {
			
			e.printStackTrace();
			return ret;
		}
	} 
	
	
	/** Get criteria for this challenge, filter by is Barriera
	 * It is an utility to use on update of Criteria page
	 * @param challengeId
	 * @return
	 */
	public List<EvaluationCriteria> getCriteriaByChallengeIdAndIsBarriera (long challengeId, boolean isBarriera){
		
		List<EvaluationCriteria> ret = new ArrayList<EvaluationCriteria>();
		
		try {
			return EvaluationCriteriaUtil.findByChallengeAndIsBarriera(challengeId, isBarriera);
		} catch (SystemException e) {
			
			e.printStackTrace();
			return ret;
		}
	}
	
	/** Get criteria for this challenge, filter by is Barriera
	 * It is an utility to use on update of Criteria page
	 * @param challengeId
	 * @return
	 */
	public List<EvaluationCriteria> getEnabledCriteriaByChallengeIdAndIsBarriera (long challengeId, boolean isBarriera){
		
		List<EvaluationCriteria> ret = new ArrayList<EvaluationCriteria>();
		
		try {
			return EvaluationCriteriaUtil.findByChallengeAndIsBarrieraAndEnabled(challengeId, isBarriera, true);
		} catch (SystemException e) {
			
			e.printStackTrace();
			return ret;
		}
	}
	
	
	
	/** Get criteria for this challenge, filter by is Barriera and is Custom
	 * It is an utility to use on update of Criteria page
	 * @param challengeId
	 * @return
	 */
	public List<EvaluationCriteria> getCriteriaByChallengeIdAndIsBarrieraAndIsCustom (long challengeId, boolean isBarriera, boolean isCustom){
		
		List<EvaluationCriteria> ret = new ArrayList<EvaluationCriteria>();
		
		try {
			return EvaluationCriteriaUtil.findByChallengeAndBarrieraAndCustom(challengeId, isBarriera, isCustom);
		} catch (SystemException e) {
			
			e.printStackTrace();
			return ret;
		}
	}
	
	/**
	 * @param criteriaId
	 * @return
	 */
	public EvaluationCriteria getCriteriaByCriteriaId (long criteriaId){
		
		EvaluationCriteria ec = null;
	
			try {
				return EvaluationCriteriaUtil.findBycriteriaId(criteriaId);
			} catch (NoSuchEvaluationCriteriaException | SystemException e) {
				
				e.printStackTrace();
				return ec;
			}
		
		
	}
	
	/** Get criteria for this challenge, filter by enabled
	 * It is an utility to use on update of Criteria page
	 * @param challengeId
	 * @return
	 */
	public List<EvaluationCriteria> getEnabledCriteriaByChallengeId (long challengeId){
		
		List<EvaluationCriteria> ret = new ArrayList<EvaluationCriteria>();
		
		try {
			return EvaluationCriteriaUtil.findBycriteriaByChallengeAndEnabled(challengeId, true);
		} catch (SystemException e) {
			
			e.printStackTrace();
			return ret;
		}
	}
	
}