/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.impl;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteChallenges;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSFavouriteIdeas;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFavouriteIdeasImpl;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteIdeasLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.base.CLSFavouriteIdeasLocalServiceBaseImpl;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteChallengesUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteIdeasPK;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSFavouriteIdeasUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSIdeaUtil;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the c l s favourite ideas local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteIdeasLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.base.CLSFavouriteIdeasLocalServiceBaseImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteIdeasLocalServiceUtil
 */
public class CLSFavouriteIdeasLocalServiceImpl
	extends CLSFavouriteIdeasLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.eng.rspa.ideas.challenges.servicelayer.service.CLSFavouriteIdeasLocalServiceUtil} to access the c l s favourite ideas local service.
	 */
	
	public void removeFavouriteIdeasOnIdeaDelete(long ideaId){
		try{
			List<CLSFavouriteIdeas> favouriteIdeas = CLSFavouriteIdeasUtil.findByIdeaID(ideaId);
			
			for(CLSFavouriteIdeas favouriteIdea : favouriteIdeas) {
				try{
					CLSFavouriteIdeasPK favouriteIdeasPK = new CLSFavouriteIdeasPK(favouriteIdea.getIdeaID(), favouriteIdea.getUserId());
					CLSFavouriteIdeasUtil.remove(favouriteIdeasPK);
				}
				catch(Exception e){
					System.out.println(e.getLocalizedMessage());
				}
			}
		}
		catch(Exception e){
			System.out.println(e.getLocalizedMessage());
		}
	}
	
	public List<CLSFavouriteIdeas> getFavouriteIdeasEntriesByIdeaId(long ideaId) throws SystemException{
		return CLSFavouriteIdeasUtil.findByIdeaID(ideaId);
	 }
	
	public List<CLSIdea> getFavouriteIdeasByUserId(long userId) throws SystemException{
		List<CLSFavouriteIdeas> favouriteIdeas = CLSFavouriteIdeasUtil.findByUserID(userId);
		List<CLSIdea> ideas = new ArrayList<CLSIdea>();
		
		for(CLSFavouriteIdeas favouriteIdea : favouriteIdeas) {
			ideas.add( CLSIdeaUtil.fetchByPrimaryKey(favouriteIdea.getIdeaID()));
		}
		return ideas;
	}
	
	
	public List<CLSFavouriteIdeas> getFavouriteIdeasEntriesByUserId(long userId) throws SystemException{
		return CLSFavouriteIdeasUtil.findByUserID(userId);
	 }
	
	
	
	
	public List<CLSFavouriteIdeas> getFavouriteIdeas(Long ideaId, Long userId) throws SystemException {		 	   
			
			List<CLSFavouriteIdeas> l_favIdea = new ArrayList<CLSFavouriteIdeas>();
			// verifico se l'idea sia tra i miei preferiti
			Criterion c_favIdea1 = RestrictionsFactoryUtil.eq("primaryKey.ideaID", ideaId);
			Criterion c_favIdea2 = RestrictionsFactoryUtil.eq("primaryKey.userId",userId);
			Criterion c_favIdea3 = RestrictionsFactoryUtil.and(c_favIdea1,c_favIdea2);
			DynamicQuery dq_favIdea = DynamicQueryFactoryUtil.forClass(CLSFavouriteIdeas.class,this.getClassLoader()).add(c_favIdea3);
			l_favIdea =CLSFavouriteIdeasLocalServiceUtil.dynamicQuery(dq_favIdea);
			return (List<CLSFavouriteIdeas>)l_favIdea;
	    }
	

	
	public boolean addFavouriteIdea(Long favIdeaId, Long userId) throws  SystemException{
		boolean retVal = true;
		try {
			
			CLSFavouriteIdeas favIdea = new CLSFavouriteIdeasImpl();
					
			favIdea.setIdeaID(favIdeaId);
			favIdea.setUserId(userId);			
			
			CLSFavouriteIdeasLocalServiceUtil.addCLSFavouriteIdeas(favIdea);

		} catch (com.liferay.portal.kernel.exception.SystemException e) {
			retVal = false;
		
			throw new SystemException(e.getMessage());
		}
		
		
		return retVal;
	}
		 

		
	 public boolean removeFavouriteIdea(Long favIdeaId, Long userId) throws  SystemException {
			boolean retVal = true;
			try {

				CLSFavouriteIdeasPK  clsFavouriteIdeasPK = new CLSFavouriteIdeasPK(favIdeaId, userId);
				CLSFavouriteIdeasLocalServiceUtil.deleteCLSFavouriteIdeas(clsFavouriteIdeasPK);
				
			} catch (com.liferay.portal.kernel.exception.PortalException e) {
				retVal = false;
				throw new SystemException(e.getMessage());
			}
					
			return retVal;
		}



	@Override
	public boolean removeFavouriteIdea(Long favideaId) throws SystemException {
		return false;
	}
	
	
	
	
}