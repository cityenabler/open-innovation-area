/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException;
import it.eng.rspa.ideas.challenges.servicelayer.model.GESelected;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.GESelectedImpl;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.GESelectedModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the g e selected service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see GESelectedPersistence
 * @see GESelectedUtil
 * @generated
 */
public class GESelectedPersistenceImpl extends BasePersistenceImpl<GESelected>
	implements GESelectedPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link GESelectedUtil} to access the g e selected persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = GESelectedImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(GESelectedModelImpl.ENTITY_CACHE_ENABLED,
			GESelectedModelImpl.FINDER_CACHE_ENABLED, GESelectedImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(GESelectedModelImpl.ENTITY_CACHE_ENABLED,
			GESelectedModelImpl.FINDER_CACHE_ENABLED, GESelectedImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(GESelectedModelImpl.ENTITY_CACHE_ENABLED,
			GESelectedModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_GESBYIDEAID =
		new FinderPath(GESelectedModelImpl.ENTITY_CACHE_ENABLED,
			GESelectedModelImpl.FINDER_CACHE_ENABLED, GESelectedImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBygesByIdeaId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GESBYIDEAID =
		new FinderPath(GESelectedModelImpl.ENTITY_CACHE_ENABLED,
			GESelectedModelImpl.FINDER_CACHE_ENABLED, GESelectedImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBygesByIdeaId",
			new String[] { Long.class.getName() },
			GESelectedModelImpl.IDEAID_COLUMN_BITMASK |
			GESelectedModelImpl.TITLE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_GESBYIDEAID = new FinderPath(GESelectedModelImpl.ENTITY_CACHE_ENABLED,
			GESelectedModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBygesByIdeaId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the g e selecteds where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @return the matching g e selecteds
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<GESelected> findBygesByIdeaId(long ideaId)
		throws SystemException {
		return findBygesByIdeaId(ideaId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the g e selecteds where ideaId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.GESelectedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaId the idea ID
	 * @param start the lower bound of the range of g e selecteds
	 * @param end the upper bound of the range of g e selecteds (not inclusive)
	 * @return the range of matching g e selecteds
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<GESelected> findBygesByIdeaId(long ideaId, int start, int end)
		throws SystemException {
		return findBygesByIdeaId(ideaId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the g e selecteds where ideaId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.GESelectedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaId the idea ID
	 * @param start the lower bound of the range of g e selecteds
	 * @param end the upper bound of the range of g e selecteds (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching g e selecteds
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<GESelected> findBygesByIdeaId(long ideaId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GESBYIDEAID;
			finderArgs = new Object[] { ideaId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_GESBYIDEAID;
			finderArgs = new Object[] { ideaId, start, end, orderByComparator };
		}

		List<GESelected> list = (List<GESelected>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (GESelected geSelected : list) {
				if ((ideaId != geSelected.getIdeaId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_GESELECTED_WHERE);

			query.append(_FINDER_COLUMN_GESBYIDEAID_IDEAID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(GESelectedModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaId);

				if (!pagination) {
					list = (List<GESelected>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<GESelected>(list);
				}
				else {
					list = (List<GESelected>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first g e selected in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching g e selected
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException if a matching g e selected could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public GESelected findBygesByIdeaId_First(long ideaId,
		OrderByComparator orderByComparator)
		throws NoSuchGESelectedException, SystemException {
		GESelected geSelected = fetchBygesByIdeaId_First(ideaId,
				orderByComparator);

		if (geSelected != null) {
			return geSelected;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaId=");
		msg.append(ideaId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchGESelectedException(msg.toString());
	}

	/**
	 * Returns the first g e selected in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching g e selected, or <code>null</code> if a matching g e selected could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public GESelected fetchBygesByIdeaId_First(long ideaId,
		OrderByComparator orderByComparator) throws SystemException {
		List<GESelected> list = findBygesByIdeaId(ideaId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last g e selected in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching g e selected
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException if a matching g e selected could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public GESelected findBygesByIdeaId_Last(long ideaId,
		OrderByComparator orderByComparator)
		throws NoSuchGESelectedException, SystemException {
		GESelected geSelected = fetchBygesByIdeaId_Last(ideaId,
				orderByComparator);

		if (geSelected != null) {
			return geSelected;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaId=");
		msg.append(ideaId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchGESelectedException(msg.toString());
	}

	/**
	 * Returns the last g e selected in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching g e selected, or <code>null</code> if a matching g e selected could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public GESelected fetchBygesByIdeaId_Last(long ideaId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBygesByIdeaId(ideaId);

		if (count == 0) {
			return null;
		}

		List<GESelected> list = findBygesByIdeaId(ideaId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the g e selecteds before and after the current g e selected in the ordered set where ideaId = &#63;.
	 *
	 * @param geSelectedPK the primary key of the current g e selected
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next g e selected
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException if a g e selected with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public GESelected[] findBygesByIdeaId_PrevAndNext(
		GESelectedPK geSelectedPK, long ideaId,
		OrderByComparator orderByComparator)
		throws NoSuchGESelectedException, SystemException {
		GESelected geSelected = findByPrimaryKey(geSelectedPK);

		Session session = null;

		try {
			session = openSession();

			GESelected[] array = new GESelectedImpl[3];

			array[0] = getBygesByIdeaId_PrevAndNext(session, geSelected,
					ideaId, orderByComparator, true);

			array[1] = geSelected;

			array[2] = getBygesByIdeaId_PrevAndNext(session, geSelected,
					ideaId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected GESelected getBygesByIdeaId_PrevAndNext(Session session,
		GESelected geSelected, long ideaId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_GESELECTED_WHERE);

		query.append(_FINDER_COLUMN_GESBYIDEAID_IDEAID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(GESelectedModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(ideaId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(geSelected);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<GESelected> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the g e selecteds where ideaId = &#63; from the database.
	 *
	 * @param ideaId the idea ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBygesByIdeaId(long ideaId) throws SystemException {
		for (GESelected geSelected : findBygesByIdeaId(ideaId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(geSelected);
		}
	}

	/**
	 * Returns the number of g e selecteds where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @return the number of matching g e selecteds
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBygesByIdeaId(long ideaId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_GESBYIDEAID;

		Object[] finderArgs = new Object[] { ideaId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_GESELECTED_WHERE);

			query.append(_FINDER_COLUMN_GESBYIDEAID_IDEAID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GESBYIDEAID_IDEAID_2 = "geSelected.id.ideaId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_GESBYNID = new FinderPath(GESelectedModelImpl.ENTITY_CACHE_ENABLED,
			GESelectedModelImpl.FINDER_CACHE_ENABLED, GESelectedImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBygesByNid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GESBYNID =
		new FinderPath(GESelectedModelImpl.ENTITY_CACHE_ENABLED,
			GESelectedModelImpl.FINDER_CACHE_ENABLED, GESelectedImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBygesByNid",
			new String[] { String.class.getName() },
			GESelectedModelImpl.NID_COLUMN_BITMASK |
			GESelectedModelImpl.TITLE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_GESBYNID = new FinderPath(GESelectedModelImpl.ENTITY_CACHE_ENABLED,
			GESelectedModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBygesByNid",
			new String[] { String.class.getName() });

	/**
	 * Returns all the g e selecteds where nid = &#63;.
	 *
	 * @param nid the nid
	 * @return the matching g e selecteds
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<GESelected> findBygesByNid(String nid)
		throws SystemException {
		return findBygesByNid(nid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the g e selecteds where nid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.GESelectedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param nid the nid
	 * @param start the lower bound of the range of g e selecteds
	 * @param end the upper bound of the range of g e selecteds (not inclusive)
	 * @return the range of matching g e selecteds
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<GESelected> findBygesByNid(String nid, int start, int end)
		throws SystemException {
		return findBygesByNid(nid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the g e selecteds where nid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.GESelectedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param nid the nid
	 * @param start the lower bound of the range of g e selecteds
	 * @param end the upper bound of the range of g e selecteds (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching g e selecteds
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<GESelected> findBygesByNid(String nid, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GESBYNID;
			finderArgs = new Object[] { nid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_GESBYNID;
			finderArgs = new Object[] { nid, start, end, orderByComparator };
		}

		List<GESelected> list = (List<GESelected>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (GESelected geSelected : list) {
				if (!Validator.equals(nid, geSelected.getNid())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_GESELECTED_WHERE);

			boolean bindNid = false;

			if (nid == null) {
				query.append(_FINDER_COLUMN_GESBYNID_NID_1);
			}
			else if (nid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_GESBYNID_NID_3);
			}
			else {
				bindNid = true;

				query.append(_FINDER_COLUMN_GESBYNID_NID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(GESelectedModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindNid) {
					qPos.add(nid);
				}

				if (!pagination) {
					list = (List<GESelected>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<GESelected>(list);
				}
				else {
					list = (List<GESelected>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first g e selected in the ordered set where nid = &#63;.
	 *
	 * @param nid the nid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching g e selected
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException if a matching g e selected could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public GESelected findBygesByNid_First(String nid,
		OrderByComparator orderByComparator)
		throws NoSuchGESelectedException, SystemException {
		GESelected geSelected = fetchBygesByNid_First(nid, orderByComparator);

		if (geSelected != null) {
			return geSelected;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("nid=");
		msg.append(nid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchGESelectedException(msg.toString());
	}

	/**
	 * Returns the first g e selected in the ordered set where nid = &#63;.
	 *
	 * @param nid the nid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching g e selected, or <code>null</code> if a matching g e selected could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public GESelected fetchBygesByNid_First(String nid,
		OrderByComparator orderByComparator) throws SystemException {
		List<GESelected> list = findBygesByNid(nid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last g e selected in the ordered set where nid = &#63;.
	 *
	 * @param nid the nid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching g e selected
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException if a matching g e selected could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public GESelected findBygesByNid_Last(String nid,
		OrderByComparator orderByComparator)
		throws NoSuchGESelectedException, SystemException {
		GESelected geSelected = fetchBygesByNid_Last(nid, orderByComparator);

		if (geSelected != null) {
			return geSelected;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("nid=");
		msg.append(nid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchGESelectedException(msg.toString());
	}

	/**
	 * Returns the last g e selected in the ordered set where nid = &#63;.
	 *
	 * @param nid the nid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching g e selected, or <code>null</code> if a matching g e selected could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public GESelected fetchBygesByNid_Last(String nid,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBygesByNid(nid);

		if (count == 0) {
			return null;
		}

		List<GESelected> list = findBygesByNid(nid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the g e selecteds before and after the current g e selected in the ordered set where nid = &#63;.
	 *
	 * @param geSelectedPK the primary key of the current g e selected
	 * @param nid the nid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next g e selected
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException if a g e selected with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public GESelected[] findBygesByNid_PrevAndNext(GESelectedPK geSelectedPK,
		String nid, OrderByComparator orderByComparator)
		throws NoSuchGESelectedException, SystemException {
		GESelected geSelected = findByPrimaryKey(geSelectedPK);

		Session session = null;

		try {
			session = openSession();

			GESelected[] array = new GESelectedImpl[3];

			array[0] = getBygesByNid_PrevAndNext(session, geSelected, nid,
					orderByComparator, true);

			array[1] = geSelected;

			array[2] = getBygesByNid_PrevAndNext(session, geSelected, nid,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected GESelected getBygesByNid_PrevAndNext(Session session,
		GESelected geSelected, String nid, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_GESELECTED_WHERE);

		boolean bindNid = false;

		if (nid == null) {
			query.append(_FINDER_COLUMN_GESBYNID_NID_1);
		}
		else if (nid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_GESBYNID_NID_3);
		}
		else {
			bindNid = true;

			query.append(_FINDER_COLUMN_GESBYNID_NID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(GESelectedModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindNid) {
			qPos.add(nid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(geSelected);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<GESelected> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the g e selecteds where nid = &#63; from the database.
	 *
	 * @param nid the nid
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBygesByNid(String nid) throws SystemException {
		for (GESelected geSelected : findBygesByNid(nid, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(geSelected);
		}
	}

	/**
	 * Returns the number of g e selecteds where nid = &#63;.
	 *
	 * @param nid the nid
	 * @return the number of matching g e selecteds
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBygesByNid(String nid) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_GESBYNID;

		Object[] finderArgs = new Object[] { nid };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_GESELECTED_WHERE);

			boolean bindNid = false;

			if (nid == null) {
				query.append(_FINDER_COLUMN_GESBYNID_NID_1);
			}
			else if (nid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_GESBYNID_NID_3);
			}
			else {
				bindNid = true;

				query.append(_FINDER_COLUMN_GESBYNID_NID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindNid) {
					qPos.add(nid);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_GESBYNID_NID_1 = "geSelected.id.nid IS NULL";
	private static final String _FINDER_COLUMN_GESBYNID_NID_2 = "geSelected.id.nid = ?";
	private static final String _FINDER_COLUMN_GESBYNID_NID_3 = "(geSelected.id.nid IS NULL OR geSelected.id.nid = '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_IDEAIDANDNID = new FinderPath(GESelectedModelImpl.ENTITY_CACHE_ENABLED,
			GESelectedModelImpl.FINDER_CACHE_ENABLED, GESelectedImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByideaIdAndNid",
			new String[] { Long.class.getName(), String.class.getName() },
			GESelectedModelImpl.IDEAID_COLUMN_BITMASK |
			GESelectedModelImpl.NID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_IDEAIDANDNID = new FinderPath(GESelectedModelImpl.ENTITY_CACHE_ENABLED,
			GESelectedModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByideaIdAndNid",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns the g e selected where ideaId = &#63; and nid = &#63; or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException} if it could not be found.
	 *
	 * @param ideaId the idea ID
	 * @param nid the nid
	 * @return the matching g e selected
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException if a matching g e selected could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public GESelected findByideaIdAndNid(long ideaId, String nid)
		throws NoSuchGESelectedException, SystemException {
		GESelected geSelected = fetchByideaIdAndNid(ideaId, nid);

		if (geSelected == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("ideaId=");
			msg.append(ideaId);

			msg.append(", nid=");
			msg.append(nid);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchGESelectedException(msg.toString());
		}

		return geSelected;
	}

	/**
	 * Returns the g e selected where ideaId = &#63; and nid = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param ideaId the idea ID
	 * @param nid the nid
	 * @return the matching g e selected, or <code>null</code> if a matching g e selected could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public GESelected fetchByideaIdAndNid(long ideaId, String nid)
		throws SystemException {
		return fetchByideaIdAndNid(ideaId, nid, true);
	}

	/**
	 * Returns the g e selected where ideaId = &#63; and nid = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param ideaId the idea ID
	 * @param nid the nid
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching g e selected, or <code>null</code> if a matching g e selected could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public GESelected fetchByideaIdAndNid(long ideaId, String nid,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { ideaId, nid };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_IDEAIDANDNID,
					finderArgs, this);
		}

		if (result instanceof GESelected) {
			GESelected geSelected = (GESelected)result;

			if ((ideaId != geSelected.getIdeaId()) ||
					!Validator.equals(nid, geSelected.getNid())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_GESELECTED_WHERE);

			query.append(_FINDER_COLUMN_IDEAIDANDNID_IDEAID_2);

			boolean bindNid = false;

			if (nid == null) {
				query.append(_FINDER_COLUMN_IDEAIDANDNID_NID_1);
			}
			else if (nid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_IDEAIDANDNID_NID_3);
			}
			else {
				bindNid = true;

				query.append(_FINDER_COLUMN_IDEAIDANDNID_NID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaId);

				if (bindNid) {
					qPos.add(nid);
				}

				List<GESelected> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDEAIDANDNID,
						finderArgs, list);
				}
				else {
					GESelected geSelected = list.get(0);

					result = geSelected;

					cacheResult(geSelected);

					if ((geSelected.getIdeaId() != ideaId) ||
							(geSelected.getNid() == null) ||
							!geSelected.getNid().equals(nid)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDEAIDANDNID,
							finderArgs, geSelected);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_IDEAIDANDNID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (GESelected)result;
		}
	}

	/**
	 * Removes the g e selected where ideaId = &#63; and nid = &#63; from the database.
	 *
	 * @param ideaId the idea ID
	 * @param nid the nid
	 * @return the g e selected that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public GESelected removeByideaIdAndNid(long ideaId, String nid)
		throws NoSuchGESelectedException, SystemException {
		GESelected geSelected = findByideaIdAndNid(ideaId, nid);

		return remove(geSelected);
	}

	/**
	 * Returns the number of g e selecteds where ideaId = &#63; and nid = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param nid the nid
	 * @return the number of matching g e selecteds
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByideaIdAndNid(long ideaId, String nid)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_IDEAIDANDNID;

		Object[] finderArgs = new Object[] { ideaId, nid };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_GESELECTED_WHERE);

			query.append(_FINDER_COLUMN_IDEAIDANDNID_IDEAID_2);

			boolean bindNid = false;

			if (nid == null) {
				query.append(_FINDER_COLUMN_IDEAIDANDNID_NID_1);
			}
			else if (nid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_IDEAIDANDNID_NID_3);
			}
			else {
				bindNid = true;

				query.append(_FINDER_COLUMN_IDEAIDANDNID_NID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaId);

				if (bindNid) {
					qPos.add(nid);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_IDEAIDANDNID_IDEAID_2 = "geSelected.id.ideaId = ? AND ";
	private static final String _FINDER_COLUMN_IDEAIDANDNID_NID_1 = "geSelected.id.nid IS NULL";
	private static final String _FINDER_COLUMN_IDEAIDANDNID_NID_2 = "geSelected.id.nid = ?";
	private static final String _FINDER_COLUMN_IDEAIDANDNID_NID_3 = "(geSelected.id.nid IS NULL OR geSelected.id.nid = '')";

	public GESelectedPersistenceImpl() {
		setModelClass(GESelected.class);
	}

	/**
	 * Caches the g e selected in the entity cache if it is enabled.
	 *
	 * @param geSelected the g e selected
	 */
	@Override
	public void cacheResult(GESelected geSelected) {
		EntityCacheUtil.putResult(GESelectedModelImpl.ENTITY_CACHE_ENABLED,
			GESelectedImpl.class, geSelected.getPrimaryKey(), geSelected);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDEAIDANDNID,
			new Object[] { geSelected.getIdeaId(), geSelected.getNid() },
			geSelected);

		geSelected.resetOriginalValues();
	}

	/**
	 * Caches the g e selecteds in the entity cache if it is enabled.
	 *
	 * @param geSelecteds the g e selecteds
	 */
	@Override
	public void cacheResult(List<GESelected> geSelecteds) {
		for (GESelected geSelected : geSelecteds) {
			if (EntityCacheUtil.getResult(
						GESelectedModelImpl.ENTITY_CACHE_ENABLED,
						GESelectedImpl.class, geSelected.getPrimaryKey()) == null) {
				cacheResult(geSelected);
			}
			else {
				geSelected.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all g e selecteds.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(GESelectedImpl.class.getName());
		}

		EntityCacheUtil.clearCache(GESelectedImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the g e selected.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(GESelected geSelected) {
		EntityCacheUtil.removeResult(GESelectedModelImpl.ENTITY_CACHE_ENABLED,
			GESelectedImpl.class, geSelected.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(geSelected);
	}

	@Override
	public void clearCache(List<GESelected> geSelecteds) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (GESelected geSelected : geSelecteds) {
			EntityCacheUtil.removeResult(GESelectedModelImpl.ENTITY_CACHE_ENABLED,
				GESelectedImpl.class, geSelected.getPrimaryKey());

			clearUniqueFindersCache(geSelected);
		}
	}

	protected void cacheUniqueFindersCache(GESelected geSelected) {
		if (geSelected.isNew()) {
			Object[] args = new Object[] {
					geSelected.getIdeaId(), geSelected.getNid()
				};

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_IDEAIDANDNID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDEAIDANDNID, args,
				geSelected);
		}
		else {
			GESelectedModelImpl geSelectedModelImpl = (GESelectedModelImpl)geSelected;

			if ((geSelectedModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_IDEAIDANDNID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						geSelected.getIdeaId(), geSelected.getNid()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_IDEAIDANDNID,
					args, Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDEAIDANDNID,
					args, geSelected);
			}
		}
	}

	protected void clearUniqueFindersCache(GESelected geSelected) {
		GESelectedModelImpl geSelectedModelImpl = (GESelectedModelImpl)geSelected;

		Object[] args = new Object[] { geSelected.getIdeaId(), geSelected.getNid() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEAIDANDNID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_IDEAIDANDNID, args);

		if ((geSelectedModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_IDEAIDANDNID.getColumnBitmask()) != 0) {
			args = new Object[] {
					geSelectedModelImpl.getOriginalIdeaId(),
					geSelectedModelImpl.getOriginalNid()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEAIDANDNID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_IDEAIDANDNID, args);
		}
	}

	/**
	 * Creates a new g e selected with the primary key. Does not add the g e selected to the database.
	 *
	 * @param geSelectedPK the primary key for the new g e selected
	 * @return the new g e selected
	 */
	@Override
	public GESelected create(GESelectedPK geSelectedPK) {
		GESelected geSelected = new GESelectedImpl();

		geSelected.setNew(true);
		geSelected.setPrimaryKey(geSelectedPK);

		return geSelected;
	}

	/**
	 * Removes the g e selected with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param geSelectedPK the primary key of the g e selected
	 * @return the g e selected that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException if a g e selected with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public GESelected remove(GESelectedPK geSelectedPK)
		throws NoSuchGESelectedException, SystemException {
		return remove((Serializable)geSelectedPK);
	}

	/**
	 * Removes the g e selected with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the g e selected
	 * @return the g e selected that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException if a g e selected with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public GESelected remove(Serializable primaryKey)
		throws NoSuchGESelectedException, SystemException {
		Session session = null;

		try {
			session = openSession();

			GESelected geSelected = (GESelected)session.get(GESelectedImpl.class,
					primaryKey);

			if (geSelected == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchGESelectedException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(geSelected);
		}
		catch (NoSuchGESelectedException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected GESelected removeImpl(GESelected geSelected)
		throws SystemException {
		geSelected = toUnwrappedModel(geSelected);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(geSelected)) {
				geSelected = (GESelected)session.get(GESelectedImpl.class,
						geSelected.getPrimaryKeyObj());
			}

			if (geSelected != null) {
				session.delete(geSelected);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (geSelected != null) {
			clearCache(geSelected);
		}

		return geSelected;
	}

	@Override
	public GESelected updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.GESelected geSelected)
		throws SystemException {
		geSelected = toUnwrappedModel(geSelected);

		boolean isNew = geSelected.isNew();

		GESelectedModelImpl geSelectedModelImpl = (GESelectedModelImpl)geSelected;

		Session session = null;

		try {
			session = openSession();

			if (geSelected.isNew()) {
				session.save(geSelected);

				geSelected.setNew(false);
			}
			else {
				session.merge(geSelected);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !GESelectedModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((geSelectedModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GESBYIDEAID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						geSelectedModelImpl.getOriginalIdeaId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_GESBYIDEAID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GESBYIDEAID,
					args);

				args = new Object[] { geSelectedModelImpl.getIdeaId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_GESBYIDEAID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GESBYIDEAID,
					args);
			}

			if ((geSelectedModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GESBYNID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						geSelectedModelImpl.getOriginalNid()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_GESBYNID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GESBYNID,
					args);

				args = new Object[] { geSelectedModelImpl.getNid() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_GESBYNID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_GESBYNID,
					args);
			}
		}

		EntityCacheUtil.putResult(GESelectedModelImpl.ENTITY_CACHE_ENABLED,
			GESelectedImpl.class, geSelected.getPrimaryKey(), geSelected);

		clearUniqueFindersCache(geSelected);
		cacheUniqueFindersCache(geSelected);

		return geSelected;
	}

	protected GESelected toUnwrappedModel(GESelected geSelected) {
		if (geSelected instanceof GESelectedImpl) {
			return geSelected;
		}

		GESelectedImpl geSelectedImpl = new GESelectedImpl();

		geSelectedImpl.setNew(geSelected.isNew());
		geSelectedImpl.setPrimaryKey(geSelected.getPrimaryKey());

		geSelectedImpl.setIdeaId(geSelected.getIdeaId());
		geSelectedImpl.setNid(geSelected.getNid());
		geSelectedImpl.setUrl(geSelected.getUrl());
		geSelectedImpl.setIcon(geSelected.getIcon());
		geSelectedImpl.setTitle(geSelected.getTitle());
		geSelectedImpl.setShortDescription(geSelected.getShortDescription());
		geSelectedImpl.setLabel(geSelected.getLabel());
		geSelectedImpl.setRank(geSelected.getRank());
		geSelectedImpl.setChapter(geSelected.getChapter());

		return geSelectedImpl;
	}

	/**
	 * Returns the g e selected with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the g e selected
	 * @return the g e selected
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException if a g e selected with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public GESelected findByPrimaryKey(Serializable primaryKey)
		throws NoSuchGESelectedException, SystemException {
		GESelected geSelected = fetchByPrimaryKey(primaryKey);

		if (geSelected == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchGESelectedException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return geSelected;
	}

	/**
	 * Returns the g e selected with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException} if it could not be found.
	 *
	 * @param geSelectedPK the primary key of the g e selected
	 * @return the g e selected
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException if a g e selected with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public GESelected findByPrimaryKey(GESelectedPK geSelectedPK)
		throws NoSuchGESelectedException, SystemException {
		return findByPrimaryKey((Serializable)geSelectedPK);
	}

	/**
	 * Returns the g e selected with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the g e selected
	 * @return the g e selected, or <code>null</code> if a g e selected with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public GESelected fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		GESelected geSelected = (GESelected)EntityCacheUtil.getResult(GESelectedModelImpl.ENTITY_CACHE_ENABLED,
				GESelectedImpl.class, primaryKey);

		if (geSelected == _nullGESelected) {
			return null;
		}

		if (geSelected == null) {
			Session session = null;

			try {
				session = openSession();

				geSelected = (GESelected)session.get(GESelectedImpl.class,
						primaryKey);

				if (geSelected != null) {
					cacheResult(geSelected);
				}
				else {
					EntityCacheUtil.putResult(GESelectedModelImpl.ENTITY_CACHE_ENABLED,
						GESelectedImpl.class, primaryKey, _nullGESelected);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(GESelectedModelImpl.ENTITY_CACHE_ENABLED,
					GESelectedImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return geSelected;
	}

	/**
	 * Returns the g e selected with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param geSelectedPK the primary key of the g e selected
	 * @return the g e selected, or <code>null</code> if a g e selected with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public GESelected fetchByPrimaryKey(GESelectedPK geSelectedPK)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)geSelectedPK);
	}

	/**
	 * Returns all the g e selecteds.
	 *
	 * @return the g e selecteds
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<GESelected> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the g e selecteds.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.GESelectedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of g e selecteds
	 * @param end the upper bound of the range of g e selecteds (not inclusive)
	 * @return the range of g e selecteds
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<GESelected> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the g e selecteds.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.GESelectedModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of g e selecteds
	 * @param end the upper bound of the range of g e selecteds (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of g e selecteds
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<GESelected> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<GESelected> list = (List<GESelected>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_GESELECTED);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_GESELECTED;

				if (pagination) {
					sql = sql.concat(GESelectedModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<GESelected>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<GESelected>(list);
				}
				else {
					list = (List<GESelected>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the g e selecteds from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (GESelected geSelected : findAll()) {
			remove(geSelected);
		}
	}

	/**
	 * Returns the number of g e selecteds.
	 *
	 * @return the number of g e selecteds
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_GESELECTED);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the g e selected persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.ideas.challenges.servicelayer.model.GESelected")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<GESelected>> listenersList = new ArrayList<ModelListener<GESelected>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<GESelected>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(GESelectedImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_GESELECTED = "SELECT geSelected FROM GESelected geSelected";
	private static final String _SQL_SELECT_GESELECTED_WHERE = "SELECT geSelected FROM GESelected geSelected WHERE ";
	private static final String _SQL_COUNT_GESELECTED = "SELECT COUNT(geSelected) FROM GESelected geSelected";
	private static final String _SQL_COUNT_GESELECTED_WHERE = "SELECT COUNT(geSelected) FROM GESelected geSelected WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "geSelected.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No GESelected exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No GESelected exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(GESelectedPersistenceImpl.class);
	private static GESelected _nullGESelected = new GESelectedImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<GESelected> toCacheModel() {
				return _nullGESelectedCacheModel;
			}
		};

	private static CacheModel<GESelected> _nullGESelectedCacheModel = new CacheModel<GESelected>() {
			@Override
			public GESelected toEntityModel() {
				return _nullGESelected;
			}
		};
}