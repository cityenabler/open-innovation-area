/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.impl;

import java.util.ArrayList;
import java.util.List;

import it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException;
import it.eng.rspa.ideas.challenges.servicelayer.model.GESelected;
import it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail;
import it.eng.rspa.ideas.challenges.servicelayer.service.base.InvitedMailLocalServiceBaseImpl;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.InvitedMailUtil;

import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the invited mail local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.rspa.ideas.challenges.servicelayer.service.InvitedMailLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.base.InvitedMailLocalServiceBaseImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.InvitedMailLocalServiceUtil
 */
public class InvitedMailLocalServiceImpl extends InvitedMailLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.eng.rspa.ideas.challenges.servicelayer.service.InvitedMailLocalServiceUtil} to access the invited mail local service.
	 */
	
	/**
	 * @param ideaId
	 * @return
	 */
	public List<InvitedMail> getInvitedMailByMail(String mail){
		
		List<InvitedMail> ret = new ArrayList<InvitedMail>();
		
					try {
						return  InvitedMailUtil.findBybyMail(mail);
					} catch (SystemException e) {
						e.printStackTrace();
					}
					return ret;
	}
	
	
	/**
	 * @param ideaId
	 * @return
	 */
	public List<InvitedMail> getInvitedMailByIdeaId(long ideaId){
		
		List<InvitedMail> ret = new ArrayList<InvitedMail>();
		
					try {
						return  InvitedMailUtil.findBybyIdeaId(ideaId);
					} catch (SystemException e) {
						e.printStackTrace();
					}
					return ret;
	}
	
}