/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengePoiException;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengePoiImpl;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengePoiModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the c l s challenge poi service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSChallengePoiPersistence
 * @see CLSChallengePoiUtil
 * @generated
 */
public class CLSChallengePoiPersistenceImpl extends BasePersistenceImpl<CLSChallengePoi>
	implements CLSChallengePoiPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CLSChallengePoiUtil} to access the c l s challenge poi persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CLSChallengePoiImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CLSChallengePoiModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengePoiModelImpl.FINDER_CACHE_ENABLED,
			CLSChallengePoiImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CLSChallengePoiModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengePoiModelImpl.FINDER_CACHE_ENABLED,
			CLSChallengePoiImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CLSChallengePoiModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengePoiModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CHALLENGEID =
		new FinderPath(CLSChallengePoiModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengePoiModelImpl.FINDER_CACHE_ENABLED,
			CLSChallengePoiImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByChallengeId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEID =
		new FinderPath(CLSChallengePoiModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengePoiModelImpl.FINDER_CACHE_ENABLED,
			CLSChallengePoiImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByChallengeId",
			new String[] { Long.class.getName() },
			CLSChallengePoiModelImpl.CHALLENGEID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CHALLENGEID = new FinderPath(CLSChallengePoiModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengePoiModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByChallengeId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the c l s challenge pois where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @return the matching c l s challenge pois
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSChallengePoi> findByChallengeId(long challengeId)
		throws SystemException {
		return findByChallengeId(challengeId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s challenge pois where challengeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengePoiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param challengeId the challenge ID
	 * @param start the lower bound of the range of c l s challenge pois
	 * @param end the upper bound of the range of c l s challenge pois (not inclusive)
	 * @return the range of matching c l s challenge pois
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSChallengePoi> findByChallengeId(long challengeId, int start,
		int end) throws SystemException {
		return findByChallengeId(challengeId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s challenge pois where challengeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengePoiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param challengeId the challenge ID
	 * @param start the lower bound of the range of c l s challenge pois
	 * @param end the upper bound of the range of c l s challenge pois (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s challenge pois
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSChallengePoi> findByChallengeId(long challengeId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEID;
			finderArgs = new Object[] { challengeId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CHALLENGEID;
			finderArgs = new Object[] { challengeId, start, end, orderByComparator };
		}

		List<CLSChallengePoi> list = (List<CLSChallengePoi>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSChallengePoi clsChallengePoi : list) {
				if ((challengeId != clsChallengePoi.getChallengeId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CLSCHALLENGEPOI_WHERE);

			query.append(_FINDER_COLUMN_CHALLENGEID_CHALLENGEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSChallengePoiModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(challengeId);

				if (!pagination) {
					list = (List<CLSChallengePoi>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSChallengePoi>(list);
				}
				else {
					list = (List<CLSChallengePoi>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s challenge poi in the ordered set where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s challenge poi
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengePoiException if a matching c l s challenge poi could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallengePoi findByChallengeId_First(long challengeId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSChallengePoiException, SystemException {
		CLSChallengePoi clsChallengePoi = fetchByChallengeId_First(challengeId,
				orderByComparator);

		if (clsChallengePoi != null) {
			return clsChallengePoi;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("challengeId=");
		msg.append(challengeId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSChallengePoiException(msg.toString());
	}

	/**
	 * Returns the first c l s challenge poi in the ordered set where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s challenge poi, or <code>null</code> if a matching c l s challenge poi could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallengePoi fetchByChallengeId_First(long challengeId,
		OrderByComparator orderByComparator) throws SystemException {
		List<CLSChallengePoi> list = findByChallengeId(challengeId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s challenge poi in the ordered set where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s challenge poi
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengePoiException if a matching c l s challenge poi could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallengePoi findByChallengeId_Last(long challengeId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSChallengePoiException, SystemException {
		CLSChallengePoi clsChallengePoi = fetchByChallengeId_Last(challengeId,
				orderByComparator);

		if (clsChallengePoi != null) {
			return clsChallengePoi;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("challengeId=");
		msg.append(challengeId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSChallengePoiException(msg.toString());
	}

	/**
	 * Returns the last c l s challenge poi in the ordered set where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s challenge poi, or <code>null</code> if a matching c l s challenge poi could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallengePoi fetchByChallengeId_Last(long challengeId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByChallengeId(challengeId);

		if (count == 0) {
			return null;
		}

		List<CLSChallengePoi> list = findByChallengeId(challengeId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s challenge pois before and after the current c l s challenge poi in the ordered set where challengeId = &#63;.
	 *
	 * @param poiId the primary key of the current c l s challenge poi
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s challenge poi
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengePoiException if a c l s challenge poi with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallengePoi[] findByChallengeId_PrevAndNext(long poiId,
		long challengeId, OrderByComparator orderByComparator)
		throws NoSuchCLSChallengePoiException, SystemException {
		CLSChallengePoi clsChallengePoi = findByPrimaryKey(poiId);

		Session session = null;

		try {
			session = openSession();

			CLSChallengePoi[] array = new CLSChallengePoiImpl[3];

			array[0] = getByChallengeId_PrevAndNext(session, clsChallengePoi,
					challengeId, orderByComparator, true);

			array[1] = clsChallengePoi;

			array[2] = getByChallengeId_PrevAndNext(session, clsChallengePoi,
					challengeId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSChallengePoi getByChallengeId_PrevAndNext(Session session,
		CLSChallengePoi clsChallengePoi, long challengeId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSCHALLENGEPOI_WHERE);

		query.append(_FINDER_COLUMN_CHALLENGEID_CHALLENGEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSChallengePoiModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(challengeId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsChallengePoi);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSChallengePoi> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s challenge pois where challengeId = &#63; from the database.
	 *
	 * @param challengeId the challenge ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByChallengeId(long challengeId) throws SystemException {
		for (CLSChallengePoi clsChallengePoi : findByChallengeId(challengeId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(clsChallengePoi);
		}
	}

	/**
	 * Returns the number of c l s challenge pois where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @return the number of matching c l s challenge pois
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByChallengeId(long challengeId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_CHALLENGEID;

		Object[] finderArgs = new Object[] { challengeId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLSCHALLENGEPOI_WHERE);

			query.append(_FINDER_COLUMN_CHALLENGEID_CHALLENGEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(challengeId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_CHALLENGEID_CHALLENGEID_2 = "clsChallengePoi.challengeId = ?";

	public CLSChallengePoiPersistenceImpl() {
		setModelClass(CLSChallengePoi.class);
	}

	/**
	 * Caches the c l s challenge poi in the entity cache if it is enabled.
	 *
	 * @param clsChallengePoi the c l s challenge poi
	 */
	@Override
	public void cacheResult(CLSChallengePoi clsChallengePoi) {
		EntityCacheUtil.putResult(CLSChallengePoiModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengePoiImpl.class, clsChallengePoi.getPrimaryKey(),
			clsChallengePoi);

		clsChallengePoi.resetOriginalValues();
	}

	/**
	 * Caches the c l s challenge pois in the entity cache if it is enabled.
	 *
	 * @param clsChallengePois the c l s challenge pois
	 */
	@Override
	public void cacheResult(List<CLSChallengePoi> clsChallengePois) {
		for (CLSChallengePoi clsChallengePoi : clsChallengePois) {
			if (EntityCacheUtil.getResult(
						CLSChallengePoiModelImpl.ENTITY_CACHE_ENABLED,
						CLSChallengePoiImpl.class,
						clsChallengePoi.getPrimaryKey()) == null) {
				cacheResult(clsChallengePoi);
			}
			else {
				clsChallengePoi.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all c l s challenge pois.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CLSChallengePoiImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CLSChallengePoiImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the c l s challenge poi.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CLSChallengePoi clsChallengePoi) {
		EntityCacheUtil.removeResult(CLSChallengePoiModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengePoiImpl.class, clsChallengePoi.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<CLSChallengePoi> clsChallengePois) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CLSChallengePoi clsChallengePoi : clsChallengePois) {
			EntityCacheUtil.removeResult(CLSChallengePoiModelImpl.ENTITY_CACHE_ENABLED,
				CLSChallengePoiImpl.class, clsChallengePoi.getPrimaryKey());
		}
	}

	/**
	 * Creates a new c l s challenge poi with the primary key. Does not add the c l s challenge poi to the database.
	 *
	 * @param poiId the primary key for the new c l s challenge poi
	 * @return the new c l s challenge poi
	 */
	@Override
	public CLSChallengePoi create(long poiId) {
		CLSChallengePoi clsChallengePoi = new CLSChallengePoiImpl();

		clsChallengePoi.setNew(true);
		clsChallengePoi.setPrimaryKey(poiId);

		return clsChallengePoi;
	}

	/**
	 * Removes the c l s challenge poi with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param poiId the primary key of the c l s challenge poi
	 * @return the c l s challenge poi that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengePoiException if a c l s challenge poi with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallengePoi remove(long poiId)
		throws NoSuchCLSChallengePoiException, SystemException {
		return remove((Serializable)poiId);
	}

	/**
	 * Removes the c l s challenge poi with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the c l s challenge poi
	 * @return the c l s challenge poi that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengePoiException if a c l s challenge poi with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallengePoi remove(Serializable primaryKey)
		throws NoSuchCLSChallengePoiException, SystemException {
		Session session = null;

		try {
			session = openSession();

			CLSChallengePoi clsChallengePoi = (CLSChallengePoi)session.get(CLSChallengePoiImpl.class,
					primaryKey);

			if (clsChallengePoi == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCLSChallengePoiException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(clsChallengePoi);
		}
		catch (NoSuchCLSChallengePoiException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CLSChallengePoi removeImpl(CLSChallengePoi clsChallengePoi)
		throws SystemException {
		clsChallengePoi = toUnwrappedModel(clsChallengePoi);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(clsChallengePoi)) {
				clsChallengePoi = (CLSChallengePoi)session.get(CLSChallengePoiImpl.class,
						clsChallengePoi.getPrimaryKeyObj());
			}

			if (clsChallengePoi != null) {
				session.delete(clsChallengePoi);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (clsChallengePoi != null) {
			clearCache(clsChallengePoi);
		}

		return clsChallengePoi;
	}

	@Override
	public CLSChallengePoi updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi clsChallengePoi)
		throws SystemException {
		clsChallengePoi = toUnwrappedModel(clsChallengePoi);

		boolean isNew = clsChallengePoi.isNew();

		CLSChallengePoiModelImpl clsChallengePoiModelImpl = (CLSChallengePoiModelImpl)clsChallengePoi;

		Session session = null;

		try {
			session = openSession();

			if (clsChallengePoi.isNew()) {
				session.save(clsChallengePoi);

				clsChallengePoi.setNew(false);
			}
			else {
				session.merge(clsChallengePoi);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !CLSChallengePoiModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((clsChallengePoiModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsChallengePoiModelImpl.getOriginalChallengeId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CHALLENGEID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEID,
					args);

				args = new Object[] { clsChallengePoiModelImpl.getChallengeId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CHALLENGEID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CHALLENGEID,
					args);
			}
		}

		EntityCacheUtil.putResult(CLSChallengePoiModelImpl.ENTITY_CACHE_ENABLED,
			CLSChallengePoiImpl.class, clsChallengePoi.getPrimaryKey(),
			clsChallengePoi);

		return clsChallengePoi;
	}

	protected CLSChallengePoi toUnwrappedModel(CLSChallengePoi clsChallengePoi) {
		if (clsChallengePoi instanceof CLSChallengePoiImpl) {
			return clsChallengePoi;
		}

		CLSChallengePoiImpl clsChallengePoiImpl = new CLSChallengePoiImpl();

		clsChallengePoiImpl.setNew(clsChallengePoi.isNew());
		clsChallengePoiImpl.setPrimaryKey(clsChallengePoi.getPrimaryKey());

		clsChallengePoiImpl.setChallengeId(clsChallengePoi.getChallengeId());
		clsChallengePoiImpl.setLatitude(clsChallengePoi.getLatitude());
		clsChallengePoiImpl.setLongitude(clsChallengePoi.getLongitude());
		clsChallengePoiImpl.setDescription(clsChallengePoi.getDescription());
		clsChallengePoiImpl.setPoiId(clsChallengePoi.getPoiId());
		clsChallengePoiImpl.setTitle(clsChallengePoi.getTitle());
		clsChallengePoiImpl.setDateAdded(clsChallengePoi.getDateAdded());

		return clsChallengePoiImpl;
	}

	/**
	 * Returns the c l s challenge poi with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the c l s challenge poi
	 * @return the c l s challenge poi
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengePoiException if a c l s challenge poi with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallengePoi findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCLSChallengePoiException, SystemException {
		CLSChallengePoi clsChallengePoi = fetchByPrimaryKey(primaryKey);

		if (clsChallengePoi == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCLSChallengePoiException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return clsChallengePoi;
	}

	/**
	 * Returns the c l s challenge poi with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengePoiException} if it could not be found.
	 *
	 * @param poiId the primary key of the c l s challenge poi
	 * @return the c l s challenge poi
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSChallengePoiException if a c l s challenge poi with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallengePoi findByPrimaryKey(long poiId)
		throws NoSuchCLSChallengePoiException, SystemException {
		return findByPrimaryKey((Serializable)poiId);
	}

	/**
	 * Returns the c l s challenge poi with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the c l s challenge poi
	 * @return the c l s challenge poi, or <code>null</code> if a c l s challenge poi with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallengePoi fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		CLSChallengePoi clsChallengePoi = (CLSChallengePoi)EntityCacheUtil.getResult(CLSChallengePoiModelImpl.ENTITY_CACHE_ENABLED,
				CLSChallengePoiImpl.class, primaryKey);

		if (clsChallengePoi == _nullCLSChallengePoi) {
			return null;
		}

		if (clsChallengePoi == null) {
			Session session = null;

			try {
				session = openSession();

				clsChallengePoi = (CLSChallengePoi)session.get(CLSChallengePoiImpl.class,
						primaryKey);

				if (clsChallengePoi != null) {
					cacheResult(clsChallengePoi);
				}
				else {
					EntityCacheUtil.putResult(CLSChallengePoiModelImpl.ENTITY_CACHE_ENABLED,
						CLSChallengePoiImpl.class, primaryKey,
						_nullCLSChallengePoi);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(CLSChallengePoiModelImpl.ENTITY_CACHE_ENABLED,
					CLSChallengePoiImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return clsChallengePoi;
	}

	/**
	 * Returns the c l s challenge poi with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param poiId the primary key of the c l s challenge poi
	 * @return the c l s challenge poi, or <code>null</code> if a c l s challenge poi with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSChallengePoi fetchByPrimaryKey(long poiId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)poiId);
	}

	/**
	 * Returns all the c l s challenge pois.
	 *
	 * @return the c l s challenge pois
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSChallengePoi> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s challenge pois.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengePoiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of c l s challenge pois
	 * @param end the upper bound of the range of c l s challenge pois (not inclusive)
	 * @return the range of c l s challenge pois
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSChallengePoi> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s challenge pois.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSChallengePoiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of c l s challenge pois
	 * @param end the upper bound of the range of c l s challenge pois (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of c l s challenge pois
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSChallengePoi> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CLSChallengePoi> list = (List<CLSChallengePoi>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CLSCHALLENGEPOI);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CLSCHALLENGEPOI;

				if (pagination) {
					sql = sql.concat(CLSChallengePoiModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<CLSChallengePoi>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSChallengePoi>(list);
				}
				else {
					list = (List<CLSChallengePoi>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the c l s challenge pois from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (CLSChallengePoi clsChallengePoi : findAll()) {
			remove(clsChallengePoi);
		}
	}

	/**
	 * Returns the number of c l s challenge pois.
	 *
	 * @return the number of c l s challenge pois
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CLSCHALLENGEPOI);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the c l s challenge poi persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallengePoi")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<CLSChallengePoi>> listenersList = new ArrayList<ModelListener<CLSChallengePoi>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<CLSChallengePoi>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CLSChallengePoiImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_CLSCHALLENGEPOI = "SELECT clsChallengePoi FROM CLSChallengePoi clsChallengePoi";
	private static final String _SQL_SELECT_CLSCHALLENGEPOI_WHERE = "SELECT clsChallengePoi FROM CLSChallengePoi clsChallengePoi WHERE ";
	private static final String _SQL_COUNT_CLSCHALLENGEPOI = "SELECT COUNT(clsChallengePoi) FROM CLSChallengePoi clsChallengePoi";
	private static final String _SQL_COUNT_CLSCHALLENGEPOI_WHERE = "SELECT COUNT(clsChallengePoi) FROM CLSChallengePoi clsChallengePoi WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "clsChallengePoi.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CLSChallengePoi exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No CLSChallengePoi exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CLSChallengePoiPersistenceImpl.class);
	private static CLSChallengePoi _nullCLSChallengePoi = new CLSChallengePoiImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<CLSChallengePoi> toCacheModel() {
				return _nullCLSChallengePoiCacheModel;
			}
		};

	private static CacheModel<CLSChallengePoi> _nullCLSChallengePoiCacheModel = new CacheModel<CLSChallengePoi>() {
			@Override
			public CLSChallengePoi toEntityModel() {
				return _nullCLSChallengePoi;
			}
		};
}