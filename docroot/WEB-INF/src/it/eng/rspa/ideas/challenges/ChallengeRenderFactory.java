package it.eng.rspa.ideas.challenges;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeLocalServiceUtil;

import java.util.Locale;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portlet.asset.model.AssetRenderer;
import com.liferay.portlet.asset.model.BaseAssetRendererFactory;

public class ChallengeRenderFactory extends BaseAssetRendererFactory {

	@Override
	public AssetRenderer getAssetRenderer(long classPK, int type)
			throws PortalException, SystemException {		
		CLSChallenge challenge = CLSChallengeLocalServiceUtil.getCLSChallenge(classPK);
		return new ChallengeAssetRenderer(challenge);
	}

	@Override
	public String getClassName() {
		return CLSChallenge.class.getName();
	}

	@Override
	public String getType() {
		return "CLSChallenge";
	}
	
	@Override
	public String getTypeName(Locale locale, boolean hasSubtypes) {
		return LanguageUtil.get(LocaleUtil.getDefault(), CLSChallenge.class.getName());
	}

}
