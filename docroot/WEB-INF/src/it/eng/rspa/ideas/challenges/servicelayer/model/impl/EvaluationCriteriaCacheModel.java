/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing EvaluationCriteria in entity cache.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see EvaluationCriteria
 * @generated
 */
public class EvaluationCriteriaCacheModel implements CacheModel<EvaluationCriteria>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{criteriaId=");
		sb.append(criteriaId);
		sb.append(", challengeId=");
		sb.append(challengeId);
		sb.append(", enabled=");
		sb.append(enabled);
		sb.append(", description=");
		sb.append(description);
		sb.append(", isBarriera=");
		sb.append(isBarriera);
		sb.append(", isCustom=");
		sb.append(isCustom);
		sb.append(", language=");
		sb.append(language);
		sb.append(", inputId=");
		sb.append(inputId);
		sb.append(", weight=");
		sb.append(weight);
		sb.append(", threshold=");
		sb.append(threshold);
		sb.append(", date=");
		sb.append(date);
		sb.append(", authorId=");
		sb.append(authorId);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public EvaluationCriteria toEntityModel() {
		EvaluationCriteriaImpl evaluationCriteriaImpl = new EvaluationCriteriaImpl();

		evaluationCriteriaImpl.setCriteriaId(criteriaId);
		evaluationCriteriaImpl.setChallengeId(challengeId);
		evaluationCriteriaImpl.setEnabled(enabled);

		if (description == null) {
			evaluationCriteriaImpl.setDescription(StringPool.BLANK);
		}
		else {
			evaluationCriteriaImpl.setDescription(description);
		}

		evaluationCriteriaImpl.setIsBarriera(isBarriera);
		evaluationCriteriaImpl.setIsCustom(isCustom);

		if (language == null) {
			evaluationCriteriaImpl.setLanguage(StringPool.BLANK);
		}
		else {
			evaluationCriteriaImpl.setLanguage(language);
		}

		if (inputId == null) {
			evaluationCriteriaImpl.setInputId(StringPool.BLANK);
		}
		else {
			evaluationCriteriaImpl.setInputId(inputId);
		}

		evaluationCriteriaImpl.setWeight(weight);
		evaluationCriteriaImpl.setThreshold(threshold);

		if (date == Long.MIN_VALUE) {
			evaluationCriteriaImpl.setDate(null);
		}
		else {
			evaluationCriteriaImpl.setDate(new Date(date));
		}

		evaluationCriteriaImpl.setAuthorId(authorId);

		evaluationCriteriaImpl.resetOriginalValues();

		return evaluationCriteriaImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		criteriaId = objectInput.readLong();
		challengeId = objectInput.readLong();
		enabled = objectInput.readBoolean();
		description = objectInput.readUTF();
		isBarriera = objectInput.readBoolean();
		isCustom = objectInput.readBoolean();
		language = objectInput.readUTF();
		inputId = objectInput.readUTF();
		weight = objectInput.readDouble();
		threshold = objectInput.readInt();
		date = objectInput.readLong();
		authorId = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(criteriaId);
		objectOutput.writeLong(challengeId);
		objectOutput.writeBoolean(enabled);

		if (description == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(description);
		}

		objectOutput.writeBoolean(isBarriera);
		objectOutput.writeBoolean(isCustom);

		if (language == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(language);
		}

		if (inputId == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(inputId);
		}

		objectOutput.writeDouble(weight);
		objectOutput.writeInt(threshold);
		objectOutput.writeLong(date);
		objectOutput.writeLong(authorId);
	}

	public long criteriaId;
	public long challengeId;
	public boolean enabled;
	public String description;
	public boolean isBarriera;
	public boolean isCustom;
	public String language;
	public String inputId;
	public double weight;
	public int threshold;
	public long date;
	public long authorId;
}