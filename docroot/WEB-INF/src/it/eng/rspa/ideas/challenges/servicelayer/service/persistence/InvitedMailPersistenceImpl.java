/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException;
import it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.InvitedMailImpl;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.InvitedMailModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the invited mail service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see InvitedMailPersistence
 * @see InvitedMailUtil
 * @generated
 */
public class InvitedMailPersistenceImpl extends BasePersistenceImpl<InvitedMail>
	implements InvitedMailPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link InvitedMailUtil} to access the invited mail persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = InvitedMailImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(InvitedMailModelImpl.ENTITY_CACHE_ENABLED,
			InvitedMailModelImpl.FINDER_CACHE_ENABLED, InvitedMailImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(InvitedMailModelImpl.ENTITY_CACHE_ENABLED,
			InvitedMailModelImpl.FINDER_CACHE_ENABLED, InvitedMailImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(InvitedMailModelImpl.ENTITY_CACHE_ENABLED,
			InvitedMailModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BYMAIL = new FinderPath(InvitedMailModelImpl.ENTITY_CACHE_ENABLED,
			InvitedMailModelImpl.FINDER_CACHE_ENABLED, InvitedMailImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBybyMail",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BYMAIL =
		new FinderPath(InvitedMailModelImpl.ENTITY_CACHE_ENABLED,
			InvitedMailModelImpl.FINDER_CACHE_ENABLED, InvitedMailImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBybyMail",
			new String[] { String.class.getName() },
			InvitedMailModelImpl.MAIL_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BYMAIL = new FinderPath(InvitedMailModelImpl.ENTITY_CACHE_ENABLED,
			InvitedMailModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBybyMail",
			new String[] { String.class.getName() });

	/**
	 * Returns all the invited mails where mail = &#63;.
	 *
	 * @param mail the mail
	 * @return the matching invited mails
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<InvitedMail> findBybyMail(String mail)
		throws SystemException {
		return findBybyMail(mail, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the invited mails where mail = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.InvitedMailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param mail the mail
	 * @param start the lower bound of the range of invited mails
	 * @param end the upper bound of the range of invited mails (not inclusive)
	 * @return the range of matching invited mails
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<InvitedMail> findBybyMail(String mail, int start, int end)
		throws SystemException {
		return findBybyMail(mail, start, end, null);
	}

	/**
	 * Returns an ordered range of all the invited mails where mail = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.InvitedMailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param mail the mail
	 * @param start the lower bound of the range of invited mails
	 * @param end the upper bound of the range of invited mails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching invited mails
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<InvitedMail> findBybyMail(String mail, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BYMAIL;
			finderArgs = new Object[] { mail };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BYMAIL;
			finderArgs = new Object[] { mail, start, end, orderByComparator };
		}

		List<InvitedMail> list = (List<InvitedMail>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (InvitedMail invitedMail : list) {
				if (!Validator.equals(mail, invitedMail.getMail())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_INVITEDMAIL_WHERE);

			boolean bindMail = false;

			if (mail == null) {
				query.append(_FINDER_COLUMN_BYMAIL_MAIL_1);
			}
			else if (mail.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_BYMAIL_MAIL_3);
			}
			else {
				bindMail = true;

				query.append(_FINDER_COLUMN_BYMAIL_MAIL_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(InvitedMailModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindMail) {
					qPos.add(mail);
				}

				if (!pagination) {
					list = (List<InvitedMail>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<InvitedMail>(list);
				}
				else {
					list = (List<InvitedMail>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first invited mail in the ordered set where mail = &#63;.
	 *
	 * @param mail the mail
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching invited mail
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException if a matching invited mail could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InvitedMail findBybyMail_First(String mail,
		OrderByComparator orderByComparator)
		throws NoSuchInvitedMailException, SystemException {
		InvitedMail invitedMail = fetchBybyMail_First(mail, orderByComparator);

		if (invitedMail != null) {
			return invitedMail;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("mail=");
		msg.append(mail);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchInvitedMailException(msg.toString());
	}

	/**
	 * Returns the first invited mail in the ordered set where mail = &#63;.
	 *
	 * @param mail the mail
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching invited mail, or <code>null</code> if a matching invited mail could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InvitedMail fetchBybyMail_First(String mail,
		OrderByComparator orderByComparator) throws SystemException {
		List<InvitedMail> list = findBybyMail(mail, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last invited mail in the ordered set where mail = &#63;.
	 *
	 * @param mail the mail
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching invited mail
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException if a matching invited mail could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InvitedMail findBybyMail_Last(String mail,
		OrderByComparator orderByComparator)
		throws NoSuchInvitedMailException, SystemException {
		InvitedMail invitedMail = fetchBybyMail_Last(mail, orderByComparator);

		if (invitedMail != null) {
			return invitedMail;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("mail=");
		msg.append(mail);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchInvitedMailException(msg.toString());
	}

	/**
	 * Returns the last invited mail in the ordered set where mail = &#63;.
	 *
	 * @param mail the mail
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching invited mail, or <code>null</code> if a matching invited mail could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InvitedMail fetchBybyMail_Last(String mail,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBybyMail(mail);

		if (count == 0) {
			return null;
		}

		List<InvitedMail> list = findBybyMail(mail, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the invited mails before and after the current invited mail in the ordered set where mail = &#63;.
	 *
	 * @param invitedMailPK the primary key of the current invited mail
	 * @param mail the mail
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next invited mail
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException if a invited mail with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InvitedMail[] findBybyMail_PrevAndNext(InvitedMailPK invitedMailPK,
		String mail, OrderByComparator orderByComparator)
		throws NoSuchInvitedMailException, SystemException {
		InvitedMail invitedMail = findByPrimaryKey(invitedMailPK);

		Session session = null;

		try {
			session = openSession();

			InvitedMail[] array = new InvitedMailImpl[3];

			array[0] = getBybyMail_PrevAndNext(session, invitedMail, mail,
					orderByComparator, true);

			array[1] = invitedMail;

			array[2] = getBybyMail_PrevAndNext(session, invitedMail, mail,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected InvitedMail getBybyMail_PrevAndNext(Session session,
		InvitedMail invitedMail, String mail,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_INVITEDMAIL_WHERE);

		boolean bindMail = false;

		if (mail == null) {
			query.append(_FINDER_COLUMN_BYMAIL_MAIL_1);
		}
		else if (mail.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_BYMAIL_MAIL_3);
		}
		else {
			bindMail = true;

			query.append(_FINDER_COLUMN_BYMAIL_MAIL_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(InvitedMailModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindMail) {
			qPos.add(mail);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(invitedMail);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<InvitedMail> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the invited mails where mail = &#63; from the database.
	 *
	 * @param mail the mail
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBybyMail(String mail) throws SystemException {
		for (InvitedMail invitedMail : findBybyMail(mail, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(invitedMail);
		}
	}

	/**
	 * Returns the number of invited mails where mail = &#63;.
	 *
	 * @param mail the mail
	 * @return the number of matching invited mails
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBybyMail(String mail) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_BYMAIL;

		Object[] finderArgs = new Object[] { mail };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_INVITEDMAIL_WHERE);

			boolean bindMail = false;

			if (mail == null) {
				query.append(_FINDER_COLUMN_BYMAIL_MAIL_1);
			}
			else if (mail.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_BYMAIL_MAIL_3);
			}
			else {
				bindMail = true;

				query.append(_FINDER_COLUMN_BYMAIL_MAIL_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindMail) {
					qPos.add(mail);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_BYMAIL_MAIL_1 = "invitedMail.id.mail IS NULL";
	private static final String _FINDER_COLUMN_BYMAIL_MAIL_2 = "invitedMail.id.mail = ?";
	private static final String _FINDER_COLUMN_BYMAIL_MAIL_3 = "(invitedMail.id.mail IS NULL OR invitedMail.id.mail = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BYIDEAID = new FinderPath(InvitedMailModelImpl.ENTITY_CACHE_ENABLED,
			InvitedMailModelImpl.FINDER_CACHE_ENABLED, InvitedMailImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBybyIdeaId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BYIDEAID =
		new FinderPath(InvitedMailModelImpl.ENTITY_CACHE_ENABLED,
			InvitedMailModelImpl.FINDER_CACHE_ENABLED, InvitedMailImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBybyIdeaId",
			new String[] { Long.class.getName() },
			InvitedMailModelImpl.IDEAID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_BYIDEAID = new FinderPath(InvitedMailModelImpl.ENTITY_CACHE_ENABLED,
			InvitedMailModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBybyIdeaId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the invited mails where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @return the matching invited mails
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<InvitedMail> findBybyIdeaId(long ideaId)
		throws SystemException {
		return findBybyIdeaId(ideaId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the invited mails where ideaId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.InvitedMailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaId the idea ID
	 * @param start the lower bound of the range of invited mails
	 * @param end the upper bound of the range of invited mails (not inclusive)
	 * @return the range of matching invited mails
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<InvitedMail> findBybyIdeaId(long ideaId, int start, int end)
		throws SystemException {
		return findBybyIdeaId(ideaId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the invited mails where ideaId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.InvitedMailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaId the idea ID
	 * @param start the lower bound of the range of invited mails
	 * @param end the upper bound of the range of invited mails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching invited mails
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<InvitedMail> findBybyIdeaId(long ideaId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BYIDEAID;
			finderArgs = new Object[] { ideaId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BYIDEAID;
			finderArgs = new Object[] { ideaId, start, end, orderByComparator };
		}

		List<InvitedMail> list = (List<InvitedMail>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (InvitedMail invitedMail : list) {
				if ((ideaId != invitedMail.getIdeaId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_INVITEDMAIL_WHERE);

			query.append(_FINDER_COLUMN_BYIDEAID_IDEAID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(InvitedMailModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaId);

				if (!pagination) {
					list = (List<InvitedMail>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<InvitedMail>(list);
				}
				else {
					list = (List<InvitedMail>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first invited mail in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching invited mail
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException if a matching invited mail could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InvitedMail findBybyIdeaId_First(long ideaId,
		OrderByComparator orderByComparator)
		throws NoSuchInvitedMailException, SystemException {
		InvitedMail invitedMail = fetchBybyIdeaId_First(ideaId,
				orderByComparator);

		if (invitedMail != null) {
			return invitedMail;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaId=");
		msg.append(ideaId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchInvitedMailException(msg.toString());
	}

	/**
	 * Returns the first invited mail in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching invited mail, or <code>null</code> if a matching invited mail could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InvitedMail fetchBybyIdeaId_First(long ideaId,
		OrderByComparator orderByComparator) throws SystemException {
		List<InvitedMail> list = findBybyIdeaId(ideaId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last invited mail in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching invited mail
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException if a matching invited mail could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InvitedMail findBybyIdeaId_Last(long ideaId,
		OrderByComparator orderByComparator)
		throws NoSuchInvitedMailException, SystemException {
		InvitedMail invitedMail = fetchBybyIdeaId_Last(ideaId, orderByComparator);

		if (invitedMail != null) {
			return invitedMail;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaId=");
		msg.append(ideaId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchInvitedMailException(msg.toString());
	}

	/**
	 * Returns the last invited mail in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching invited mail, or <code>null</code> if a matching invited mail could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InvitedMail fetchBybyIdeaId_Last(long ideaId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countBybyIdeaId(ideaId);

		if (count == 0) {
			return null;
		}

		List<InvitedMail> list = findBybyIdeaId(ideaId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the invited mails before and after the current invited mail in the ordered set where ideaId = &#63;.
	 *
	 * @param invitedMailPK the primary key of the current invited mail
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next invited mail
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException if a invited mail with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InvitedMail[] findBybyIdeaId_PrevAndNext(
		InvitedMailPK invitedMailPK, long ideaId,
		OrderByComparator orderByComparator)
		throws NoSuchInvitedMailException, SystemException {
		InvitedMail invitedMail = findByPrimaryKey(invitedMailPK);

		Session session = null;

		try {
			session = openSession();

			InvitedMail[] array = new InvitedMailImpl[3];

			array[0] = getBybyIdeaId_PrevAndNext(session, invitedMail, ideaId,
					orderByComparator, true);

			array[1] = invitedMail;

			array[2] = getBybyIdeaId_PrevAndNext(session, invitedMail, ideaId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected InvitedMail getBybyIdeaId_PrevAndNext(Session session,
		InvitedMail invitedMail, long ideaId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_INVITEDMAIL_WHERE);

		query.append(_FINDER_COLUMN_BYIDEAID_IDEAID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(InvitedMailModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(ideaId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(invitedMail);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<InvitedMail> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the invited mails where ideaId = &#63; from the database.
	 *
	 * @param ideaId the idea ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeBybyIdeaId(long ideaId) throws SystemException {
		for (InvitedMail invitedMail : findBybyIdeaId(ideaId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(invitedMail);
		}
	}

	/**
	 * Returns the number of invited mails where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @return the number of matching invited mails
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countBybyIdeaId(long ideaId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_BYIDEAID;

		Object[] finderArgs = new Object[] { ideaId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_INVITEDMAIL_WHERE);

			query.append(_FINDER_COLUMN_BYIDEAID_IDEAID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_BYIDEAID_IDEAID_2 = "invitedMail.id.ideaId = ?";

	public InvitedMailPersistenceImpl() {
		setModelClass(InvitedMail.class);
	}

	/**
	 * Caches the invited mail in the entity cache if it is enabled.
	 *
	 * @param invitedMail the invited mail
	 */
	@Override
	public void cacheResult(InvitedMail invitedMail) {
		EntityCacheUtil.putResult(InvitedMailModelImpl.ENTITY_CACHE_ENABLED,
			InvitedMailImpl.class, invitedMail.getPrimaryKey(), invitedMail);

		invitedMail.resetOriginalValues();
	}

	/**
	 * Caches the invited mails in the entity cache if it is enabled.
	 *
	 * @param invitedMails the invited mails
	 */
	@Override
	public void cacheResult(List<InvitedMail> invitedMails) {
		for (InvitedMail invitedMail : invitedMails) {
			if (EntityCacheUtil.getResult(
						InvitedMailModelImpl.ENTITY_CACHE_ENABLED,
						InvitedMailImpl.class, invitedMail.getPrimaryKey()) == null) {
				cacheResult(invitedMail);
			}
			else {
				invitedMail.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all invited mails.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(InvitedMailImpl.class.getName());
		}

		EntityCacheUtil.clearCache(InvitedMailImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the invited mail.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(InvitedMail invitedMail) {
		EntityCacheUtil.removeResult(InvitedMailModelImpl.ENTITY_CACHE_ENABLED,
			InvitedMailImpl.class, invitedMail.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<InvitedMail> invitedMails) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (InvitedMail invitedMail : invitedMails) {
			EntityCacheUtil.removeResult(InvitedMailModelImpl.ENTITY_CACHE_ENABLED,
				InvitedMailImpl.class, invitedMail.getPrimaryKey());
		}
	}

	/**
	 * Creates a new invited mail with the primary key. Does not add the invited mail to the database.
	 *
	 * @param invitedMailPK the primary key for the new invited mail
	 * @return the new invited mail
	 */
	@Override
	public InvitedMail create(InvitedMailPK invitedMailPK) {
		InvitedMail invitedMail = new InvitedMailImpl();

		invitedMail.setNew(true);
		invitedMail.setPrimaryKey(invitedMailPK);

		return invitedMail;
	}

	/**
	 * Removes the invited mail with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param invitedMailPK the primary key of the invited mail
	 * @return the invited mail that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException if a invited mail with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InvitedMail remove(InvitedMailPK invitedMailPK)
		throws NoSuchInvitedMailException, SystemException {
		return remove((Serializable)invitedMailPK);
	}

	/**
	 * Removes the invited mail with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the invited mail
	 * @return the invited mail that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException if a invited mail with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InvitedMail remove(Serializable primaryKey)
		throws NoSuchInvitedMailException, SystemException {
		Session session = null;

		try {
			session = openSession();

			InvitedMail invitedMail = (InvitedMail)session.get(InvitedMailImpl.class,
					primaryKey);

			if (invitedMail == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchInvitedMailException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(invitedMail);
		}
		catch (NoSuchInvitedMailException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected InvitedMail removeImpl(InvitedMail invitedMail)
		throws SystemException {
		invitedMail = toUnwrappedModel(invitedMail);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(invitedMail)) {
				invitedMail = (InvitedMail)session.get(InvitedMailImpl.class,
						invitedMail.getPrimaryKeyObj());
			}

			if (invitedMail != null) {
				session.delete(invitedMail);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (invitedMail != null) {
			clearCache(invitedMail);
		}

		return invitedMail;
	}

	@Override
	public InvitedMail updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail invitedMail)
		throws SystemException {
		invitedMail = toUnwrappedModel(invitedMail);

		boolean isNew = invitedMail.isNew();

		InvitedMailModelImpl invitedMailModelImpl = (InvitedMailModelImpl)invitedMail;

		Session session = null;

		try {
			session = openSession();

			if (invitedMail.isNew()) {
				session.save(invitedMail);

				invitedMail.setNew(false);
			}
			else {
				session.merge(invitedMail);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !InvitedMailModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((invitedMailModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BYMAIL.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						invitedMailModelImpl.getOriginalMail()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BYMAIL, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BYMAIL,
					args);

				args = new Object[] { invitedMailModelImpl.getMail() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BYMAIL, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BYMAIL,
					args);
			}

			if ((invitedMailModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BYIDEAID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						invitedMailModelImpl.getOriginalIdeaId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BYIDEAID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BYIDEAID,
					args);

				args = new Object[] { invitedMailModelImpl.getIdeaId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BYIDEAID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BYIDEAID,
					args);
			}
		}

		EntityCacheUtil.putResult(InvitedMailModelImpl.ENTITY_CACHE_ENABLED,
			InvitedMailImpl.class, invitedMail.getPrimaryKey(), invitedMail);

		return invitedMail;
	}

	protected InvitedMail toUnwrappedModel(InvitedMail invitedMail) {
		if (invitedMail instanceof InvitedMailImpl) {
			return invitedMail;
		}

		InvitedMailImpl invitedMailImpl = new InvitedMailImpl();

		invitedMailImpl.setNew(invitedMail.isNew());
		invitedMailImpl.setPrimaryKey(invitedMail.getPrimaryKey());

		invitedMailImpl.setIdeaId(invitedMail.getIdeaId());
		invitedMailImpl.setMail(invitedMail.getMail());

		return invitedMailImpl;
	}

	/**
	 * Returns the invited mail with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the invited mail
	 * @return the invited mail
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException if a invited mail with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InvitedMail findByPrimaryKey(Serializable primaryKey)
		throws NoSuchInvitedMailException, SystemException {
		InvitedMail invitedMail = fetchByPrimaryKey(primaryKey);

		if (invitedMail == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchInvitedMailException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return invitedMail;
	}

	/**
	 * Returns the invited mail with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException} if it could not be found.
	 *
	 * @param invitedMailPK the primary key of the invited mail
	 * @return the invited mail
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchInvitedMailException if a invited mail with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InvitedMail findByPrimaryKey(InvitedMailPK invitedMailPK)
		throws NoSuchInvitedMailException, SystemException {
		return findByPrimaryKey((Serializable)invitedMailPK);
	}

	/**
	 * Returns the invited mail with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the invited mail
	 * @return the invited mail, or <code>null</code> if a invited mail with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InvitedMail fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		InvitedMail invitedMail = (InvitedMail)EntityCacheUtil.getResult(InvitedMailModelImpl.ENTITY_CACHE_ENABLED,
				InvitedMailImpl.class, primaryKey);

		if (invitedMail == _nullInvitedMail) {
			return null;
		}

		if (invitedMail == null) {
			Session session = null;

			try {
				session = openSession();

				invitedMail = (InvitedMail)session.get(InvitedMailImpl.class,
						primaryKey);

				if (invitedMail != null) {
					cacheResult(invitedMail);
				}
				else {
					EntityCacheUtil.putResult(InvitedMailModelImpl.ENTITY_CACHE_ENABLED,
						InvitedMailImpl.class, primaryKey, _nullInvitedMail);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(InvitedMailModelImpl.ENTITY_CACHE_ENABLED,
					InvitedMailImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return invitedMail;
	}

	/**
	 * Returns the invited mail with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param invitedMailPK the primary key of the invited mail
	 * @return the invited mail, or <code>null</code> if a invited mail with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public InvitedMail fetchByPrimaryKey(InvitedMailPK invitedMailPK)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)invitedMailPK);
	}

	/**
	 * Returns all the invited mails.
	 *
	 * @return the invited mails
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<InvitedMail> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the invited mails.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.InvitedMailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of invited mails
	 * @param end the upper bound of the range of invited mails (not inclusive)
	 * @return the range of invited mails
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<InvitedMail> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the invited mails.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.InvitedMailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of invited mails
	 * @param end the upper bound of the range of invited mails (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of invited mails
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<InvitedMail> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<InvitedMail> list = (List<InvitedMail>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_INVITEDMAIL);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_INVITEDMAIL;

				if (pagination) {
					sql = sql.concat(InvitedMailModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<InvitedMail>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<InvitedMail>(list);
				}
				else {
					list = (List<InvitedMail>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the invited mails from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (InvitedMail invitedMail : findAll()) {
			remove(invitedMail);
		}
	}

	/**
	 * Returns the number of invited mails.
	 *
	 * @return the number of invited mails
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_INVITEDMAIL);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the invited mail persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<InvitedMail>> listenersList = new ArrayList<ModelListener<InvitedMail>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<InvitedMail>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(InvitedMailImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_INVITEDMAIL = "SELECT invitedMail FROM InvitedMail invitedMail";
	private static final String _SQL_SELECT_INVITEDMAIL_WHERE = "SELECT invitedMail FROM InvitedMail invitedMail WHERE ";
	private static final String _SQL_COUNT_INVITEDMAIL = "SELECT COUNT(invitedMail) FROM InvitedMail invitedMail";
	private static final String _SQL_COUNT_INVITEDMAIL_WHERE = "SELECT COUNT(invitedMail) FROM InvitedMail invitedMail WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "invitedMail.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No InvitedMail exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No InvitedMail exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(InvitedMailPersistenceImpl.class);
	private static InvitedMail _nullInvitedMail = new InvitedMailImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<InvitedMail> toCacheModel() {
				return _nullInvitedMailCacheModel;
			}
		};

	private static CacheModel<InvitedMail> _nullInvitedMailCacheModel = new CacheModel<InvitedMail>() {
			@Override
			public InvitedMail toEntityModel() {
				return _nullInvitedMail;
			}
		};
}