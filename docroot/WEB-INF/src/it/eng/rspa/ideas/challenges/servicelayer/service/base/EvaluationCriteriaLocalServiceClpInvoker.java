/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.base;

import it.eng.rspa.ideas.challenges.servicelayer.service.EvaluationCriteriaLocalServiceUtil;

import java.util.Arrays;

/**
 * @author Engineering Ingegneria Informatica S.p.A.
 * @generated
 */
public class EvaluationCriteriaLocalServiceClpInvoker {
	public EvaluationCriteriaLocalServiceClpInvoker() {
		_methodName0 = "addEvaluationCriteria";

		_methodParameterTypes0 = new String[] {
				"it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria"
			};

		_methodName1 = "createEvaluationCriteria";

		_methodParameterTypes1 = new String[] {
				"it.eng.rspa.ideas.challenges.servicelayer.service.persistence.EvaluationCriteriaPK"
			};

		_methodName2 = "deleteEvaluationCriteria";

		_methodParameterTypes2 = new String[] {
				"it.eng.rspa.ideas.challenges.servicelayer.service.persistence.EvaluationCriteriaPK"
			};

		_methodName3 = "deleteEvaluationCriteria";

		_methodParameterTypes3 = new String[] {
				"it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria"
			};

		_methodName4 = "dynamicQuery";

		_methodParameterTypes4 = new String[] {  };

		_methodName5 = "dynamicQuery";

		_methodParameterTypes5 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery"
			};

		_methodName6 = "dynamicQuery";

		_methodParameterTypes6 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int"
			};

		_methodName7 = "dynamicQuery";

		_methodParameterTypes7 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int",
				"com.liferay.portal.kernel.util.OrderByComparator"
			};

		_methodName8 = "dynamicQueryCount";

		_methodParameterTypes8 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery"
			};

		_methodName9 = "dynamicQueryCount";

		_methodParameterTypes9 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery",
				"com.liferay.portal.kernel.dao.orm.Projection"
			};

		_methodName10 = "fetchEvaluationCriteria";

		_methodParameterTypes10 = new String[] {
				"it.eng.rspa.ideas.challenges.servicelayer.service.persistence.EvaluationCriteriaPK"
			};

		_methodName11 = "getEvaluationCriteria";

		_methodParameterTypes11 = new String[] {
				"it.eng.rspa.ideas.challenges.servicelayer.service.persistence.EvaluationCriteriaPK"
			};

		_methodName12 = "getPersistedModel";

		_methodParameterTypes12 = new String[] { "java.io.Serializable" };

		_methodName13 = "getEvaluationCriterias";

		_methodParameterTypes13 = new String[] { "int", "int" };

		_methodName14 = "getEvaluationCriteriasCount";

		_methodParameterTypes14 = new String[] {  };

		_methodName15 = "updateEvaluationCriteria";

		_methodParameterTypes15 = new String[] {
				"it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria"
			};

		_methodName162 = "getBeanIdentifier";

		_methodParameterTypes162 = new String[] {  };

		_methodName163 = "setBeanIdentifier";

		_methodParameterTypes163 = new String[] { "java.lang.String" };

		_methodName168 = "criteriaByChallengeId";

		_methodParameterTypes168 = new String[] { "long" };

		_methodName169 = "getCustomCriteriaByLanguage";

		_methodParameterTypes169 = new String[] { "java.lang.String" };

		_methodName170 = "getCriteriaByChallengeIdAndInputId";

		_methodParameterTypes170 = new String[] { "long", "java.lang.String" };

		_methodName171 = "getCriteriaByChallengeIdAndIsBarriera";

		_methodParameterTypes171 = new String[] { "long", "boolean" };

		_methodName172 = "getEnabledCriteriaByChallengeIdAndIsBarriera";

		_methodParameterTypes172 = new String[] { "long", "boolean" };

		_methodName173 = "getCriteriaByChallengeIdAndIsBarrieraAndIsCustom";

		_methodParameterTypes173 = new String[] { "long", "boolean", "boolean" };

		_methodName174 = "getCriteriaByCriteriaId";

		_methodParameterTypes174 = new String[] { "long" };

		_methodName175 = "getEnabledCriteriaByChallengeId";

		_methodParameterTypes175 = new String[] { "long" };
	}

	public Object invokeMethod(String name, String[] parameterTypes,
		Object[] arguments) throws Throwable {
		if (_methodName0.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes0, parameterTypes)) {
			return EvaluationCriteriaLocalServiceUtil.addEvaluationCriteria((it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria)arguments[0]);
		}

		if (_methodName1.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes1, parameterTypes)) {
			return EvaluationCriteriaLocalServiceUtil.createEvaluationCriteria((it.eng.rspa.ideas.challenges.servicelayer.service.persistence.EvaluationCriteriaPK)arguments[0]);
		}

		if (_methodName2.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes2, parameterTypes)) {
			return EvaluationCriteriaLocalServiceUtil.deleteEvaluationCriteria((it.eng.rspa.ideas.challenges.servicelayer.service.persistence.EvaluationCriteriaPK)arguments[0]);
		}

		if (_methodName3.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes3, parameterTypes)) {
			return EvaluationCriteriaLocalServiceUtil.deleteEvaluationCriteria((it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria)arguments[0]);
		}

		if (_methodName4.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes4, parameterTypes)) {
			return EvaluationCriteriaLocalServiceUtil.dynamicQuery();
		}

		if (_methodName5.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes5, parameterTypes)) {
			return EvaluationCriteriaLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0]);
		}

		if (_methodName6.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes6, parameterTypes)) {
			return EvaluationCriteriaLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0],
				((Integer)arguments[1]).intValue(),
				((Integer)arguments[2]).intValue());
		}

		if (_methodName7.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes7, parameterTypes)) {
			return EvaluationCriteriaLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0],
				((Integer)arguments[1]).intValue(),
				((Integer)arguments[2]).intValue(),
				(com.liferay.portal.kernel.util.OrderByComparator)arguments[3]);
		}

		if (_methodName8.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes8, parameterTypes)) {
			return EvaluationCriteriaLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0]);
		}

		if (_methodName9.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes9, parameterTypes)) {
			return EvaluationCriteriaLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery)arguments[0],
				(com.liferay.portal.kernel.dao.orm.Projection)arguments[1]);
		}

		if (_methodName10.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes10, parameterTypes)) {
			return EvaluationCriteriaLocalServiceUtil.fetchEvaluationCriteria((it.eng.rspa.ideas.challenges.servicelayer.service.persistence.EvaluationCriteriaPK)arguments[0]);
		}

		if (_methodName11.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes11, parameterTypes)) {
			return EvaluationCriteriaLocalServiceUtil.getEvaluationCriteria((it.eng.rspa.ideas.challenges.servicelayer.service.persistence.EvaluationCriteriaPK)arguments[0]);
		}

		if (_methodName12.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes12, parameterTypes)) {
			return EvaluationCriteriaLocalServiceUtil.getPersistedModel((java.io.Serializable)arguments[0]);
		}

		if (_methodName13.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes13, parameterTypes)) {
			return EvaluationCriteriaLocalServiceUtil.getEvaluationCriterias(((Integer)arguments[0]).intValue(),
				((Integer)arguments[1]).intValue());
		}

		if (_methodName14.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes14, parameterTypes)) {
			return EvaluationCriteriaLocalServiceUtil.getEvaluationCriteriasCount();
		}

		if (_methodName15.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes15, parameterTypes)) {
			return EvaluationCriteriaLocalServiceUtil.updateEvaluationCriteria((it.eng.rspa.ideas.challenges.servicelayer.model.EvaluationCriteria)arguments[0]);
		}

		if (_methodName162.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes162, parameterTypes)) {
			return EvaluationCriteriaLocalServiceUtil.getBeanIdentifier();
		}

		if (_methodName163.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes163, parameterTypes)) {
			EvaluationCriteriaLocalServiceUtil.setBeanIdentifier((java.lang.String)arguments[0]);

			return null;
		}

		if (_methodName168.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes168, parameterTypes)) {
			return EvaluationCriteriaLocalServiceUtil.criteriaByChallengeId(((Long)arguments[0]).longValue());
		}

		if (_methodName169.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes169, parameterTypes)) {
			return EvaluationCriteriaLocalServiceUtil.getCustomCriteriaByLanguage((java.lang.String)arguments[0]);
		}

		if (_methodName170.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes170, parameterTypes)) {
			return EvaluationCriteriaLocalServiceUtil.getCriteriaByChallengeIdAndInputId(((Long)arguments[0]).longValue(),
				(java.lang.String)arguments[1]);
		}

		if (_methodName171.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes171, parameterTypes)) {
			return EvaluationCriteriaLocalServiceUtil.getCriteriaByChallengeIdAndIsBarriera(((Long)arguments[0]).longValue(),
				((Boolean)arguments[1]).booleanValue());
		}

		if (_methodName172.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes172, parameterTypes)) {
			return EvaluationCriteriaLocalServiceUtil.getEnabledCriteriaByChallengeIdAndIsBarriera(((Long)arguments[0]).longValue(),
				((Boolean)arguments[1]).booleanValue());
		}

		if (_methodName173.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes173, parameterTypes)) {
			return EvaluationCriteriaLocalServiceUtil.getCriteriaByChallengeIdAndIsBarrieraAndIsCustom(((Long)arguments[0]).longValue(),
				((Boolean)arguments[1]).booleanValue(),
				((Boolean)arguments[2]).booleanValue());
		}

		if (_methodName174.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes174, parameterTypes)) {
			return EvaluationCriteriaLocalServiceUtil.getCriteriaByCriteriaId(((Long)arguments[0]).longValue());
		}

		if (_methodName175.equals(name) &&
				Arrays.deepEquals(_methodParameterTypes175, parameterTypes)) {
			return EvaluationCriteriaLocalServiceUtil.getEnabledCriteriaByChallengeId(((Long)arguments[0]).longValue());
		}

		throw new UnsupportedOperationException();
	}

	private String _methodName0;
	private String[] _methodParameterTypes0;
	private String _methodName1;
	private String[] _methodParameterTypes1;
	private String _methodName2;
	private String[] _methodParameterTypes2;
	private String _methodName3;
	private String[] _methodParameterTypes3;
	private String _methodName4;
	private String[] _methodParameterTypes4;
	private String _methodName5;
	private String[] _methodParameterTypes5;
	private String _methodName6;
	private String[] _methodParameterTypes6;
	private String _methodName7;
	private String[] _methodParameterTypes7;
	private String _methodName8;
	private String[] _methodParameterTypes8;
	private String _methodName9;
	private String[] _methodParameterTypes9;
	private String _methodName10;
	private String[] _methodParameterTypes10;
	private String _methodName11;
	private String[] _methodParameterTypes11;
	private String _methodName12;
	private String[] _methodParameterTypes12;
	private String _methodName13;
	private String[] _methodParameterTypes13;
	private String _methodName14;
	private String[] _methodParameterTypes14;
	private String _methodName15;
	private String[] _methodParameterTypes15;
	private String _methodName162;
	private String[] _methodParameterTypes162;
	private String _methodName163;
	private String[] _methodParameterTypes163;
	private String _methodName168;
	private String[] _methodParameterTypes168;
	private String _methodName169;
	private String[] _methodParameterTypes169;
	private String _methodName170;
	private String[] _methodParameterTypes170;
	private String _methodName171;
	private String[] _methodParameterTypes171;
	private String _methodName172;
	private String[] _methodParameterTypes172;
	private String _methodName173;
	private String[] _methodParameterTypes173;
	private String _methodName174;
	private String[] _methodParameterTypes174;
	private String _methodName175;
	private String[] _methodParameterTypes175;
}