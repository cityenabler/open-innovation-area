/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSRequisitiImpl;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSRequisitiModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the c l s requisiti service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSRequisitiPersistence
 * @see CLSRequisitiUtil
 * @generated
 */
public class CLSRequisitiPersistenceImpl extends BasePersistenceImpl<CLSRequisiti>
	implements CLSRequisitiPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CLSRequisitiUtil} to access the c l s requisiti persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CLSRequisitiImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CLSRequisitiModelImpl.ENTITY_CACHE_ENABLED,
			CLSRequisitiModelImpl.FINDER_CACHE_ENABLED, CLSRequisitiImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CLSRequisitiModelImpl.ENTITY_CACHE_ENABLED,
			CLSRequisitiModelImpl.FINDER_CACHE_ENABLED, CLSRequisitiImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CLSRequisitiModelImpl.ENTITY_CACHE_ENABLED,
			CLSRequisitiModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_REQUISITOBYIDEAID =
		new FinderPath(CLSRequisitiModelImpl.ENTITY_CACHE_ENABLED,
			CLSRequisitiModelImpl.FINDER_CACHE_ENABLED, CLSRequisitiImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByrequisitoByIdeaId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REQUISITOBYIDEAID =
		new FinderPath(CLSRequisitiModelImpl.ENTITY_CACHE_ENABLED,
			CLSRequisitiModelImpl.FINDER_CACHE_ENABLED, CLSRequisitiImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByrequisitoByIdeaId", new String[] { Long.class.getName() },
			CLSRequisitiModelImpl.IDEAID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_REQUISITOBYIDEAID = new FinderPath(CLSRequisitiModelImpl.ENTITY_CACHE_ENABLED,
			CLSRequisitiModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByrequisitoByIdeaId", new String[] { Long.class.getName() });

	/**
	 * Returns all the c l s requisitis where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @return the matching c l s requisitis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSRequisiti> findByrequisitoByIdeaId(long ideaId)
		throws SystemException {
		return findByrequisitoByIdeaId(ideaId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s requisitis where ideaId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSRequisitiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaId the idea ID
	 * @param start the lower bound of the range of c l s requisitis
	 * @param end the upper bound of the range of c l s requisitis (not inclusive)
	 * @return the range of matching c l s requisitis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSRequisiti> findByrequisitoByIdeaId(long ideaId, int start,
		int end) throws SystemException {
		return findByrequisitoByIdeaId(ideaId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s requisitis where ideaId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSRequisitiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaId the idea ID
	 * @param start the lower bound of the range of c l s requisitis
	 * @param end the upper bound of the range of c l s requisitis (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s requisitis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSRequisiti> findByrequisitoByIdeaId(long ideaId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REQUISITOBYIDEAID;
			finderArgs = new Object[] { ideaId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_REQUISITOBYIDEAID;
			finderArgs = new Object[] { ideaId, start, end, orderByComparator };
		}

		List<CLSRequisiti> list = (List<CLSRequisiti>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSRequisiti clsRequisiti : list) {
				if ((ideaId != clsRequisiti.getIdeaId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CLSREQUISITI_WHERE);

			query.append(_FINDER_COLUMN_REQUISITOBYIDEAID_IDEAID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSRequisitiModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaId);

				if (!pagination) {
					list = (List<CLSRequisiti>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSRequisiti>(list);
				}
				else {
					list = (List<CLSRequisiti>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s requisiti in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s requisiti
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException if a matching c l s requisiti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSRequisiti findByrequisitoByIdeaId_First(long ideaId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSRequisitiException, SystemException {
		CLSRequisiti clsRequisiti = fetchByrequisitoByIdeaId_First(ideaId,
				orderByComparator);

		if (clsRequisiti != null) {
			return clsRequisiti;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaId=");
		msg.append(ideaId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSRequisitiException(msg.toString());
	}

	/**
	 * Returns the first c l s requisiti in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s requisiti, or <code>null</code> if a matching c l s requisiti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSRequisiti fetchByrequisitoByIdeaId_First(long ideaId,
		OrderByComparator orderByComparator) throws SystemException {
		List<CLSRequisiti> list = findByrequisitoByIdeaId(ideaId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s requisiti in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s requisiti
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException if a matching c l s requisiti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSRequisiti findByrequisitoByIdeaId_Last(long ideaId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSRequisitiException, SystemException {
		CLSRequisiti clsRequisiti = fetchByrequisitoByIdeaId_Last(ideaId,
				orderByComparator);

		if (clsRequisiti != null) {
			return clsRequisiti;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaId=");
		msg.append(ideaId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSRequisitiException(msg.toString());
	}

	/**
	 * Returns the last c l s requisiti in the ordered set where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s requisiti, or <code>null</code> if a matching c l s requisiti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSRequisiti fetchByrequisitoByIdeaId_Last(long ideaId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByrequisitoByIdeaId(ideaId);

		if (count == 0) {
			return null;
		}

		List<CLSRequisiti> list = findByrequisitoByIdeaId(ideaId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s requisitis before and after the current c l s requisiti in the ordered set where ideaId = &#63;.
	 *
	 * @param requisitoId the primary key of the current c l s requisiti
	 * @param ideaId the idea ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s requisiti
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException if a c l s requisiti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSRequisiti[] findByrequisitoByIdeaId_PrevAndNext(
		long requisitoId, long ideaId, OrderByComparator orderByComparator)
		throws NoSuchCLSRequisitiException, SystemException {
		CLSRequisiti clsRequisiti = findByPrimaryKey(requisitoId);

		Session session = null;

		try {
			session = openSession();

			CLSRequisiti[] array = new CLSRequisitiImpl[3];

			array[0] = getByrequisitoByIdeaId_PrevAndNext(session,
					clsRequisiti, ideaId, orderByComparator, true);

			array[1] = clsRequisiti;

			array[2] = getByrequisitoByIdeaId_PrevAndNext(session,
					clsRequisiti, ideaId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSRequisiti getByrequisitoByIdeaId_PrevAndNext(Session session,
		CLSRequisiti clsRequisiti, long ideaId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSREQUISITI_WHERE);

		query.append(_FINDER_COLUMN_REQUISITOBYIDEAID_IDEAID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSRequisitiModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(ideaId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsRequisiti);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSRequisiti> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s requisitis where ideaId = &#63; from the database.
	 *
	 * @param ideaId the idea ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByrequisitoByIdeaId(long ideaId)
		throws SystemException {
		for (CLSRequisiti clsRequisiti : findByrequisitoByIdeaId(ideaId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(clsRequisiti);
		}
	}

	/**
	 * Returns the number of c l s requisitis where ideaId = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @return the number of matching c l s requisitis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByrequisitoByIdeaId(long ideaId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_REQUISITOBYIDEAID;

		Object[] finderArgs = new Object[] { ideaId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLSREQUISITI_WHERE);

			query.append(_FINDER_COLUMN_REQUISITOBYIDEAID_IDEAID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_REQUISITOBYIDEAID_IDEAID_2 = "clsRequisiti.ideaId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_REQUISITOBYAUTHORID =
		new FinderPath(CLSRequisitiModelImpl.ENTITY_CACHE_ENABLED,
			CLSRequisitiModelImpl.FINDER_CACHE_ENABLED, CLSRequisitiImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByrequisitoByAuthorid",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REQUISITOBYAUTHORID =
		new FinderPath(CLSRequisitiModelImpl.ENTITY_CACHE_ENABLED,
			CLSRequisitiModelImpl.FINDER_CACHE_ENABLED, CLSRequisitiImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByrequisitoByAuthorid", new String[] { Long.class.getName() },
			CLSRequisitiModelImpl.AUTHORUSER_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_REQUISITOBYAUTHORID = new FinderPath(CLSRequisitiModelImpl.ENTITY_CACHE_ENABLED,
			CLSRequisitiModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByrequisitoByAuthorid", new String[] { Long.class.getName() });

	/**
	 * Returns all the c l s requisitis where authorUser = &#63;.
	 *
	 * @param authorUser the author user
	 * @return the matching c l s requisitis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSRequisiti> findByrequisitoByAuthorid(long authorUser)
		throws SystemException {
		return findByrequisitoByAuthorid(authorUser, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s requisitis where authorUser = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSRequisitiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param authorUser the author user
	 * @param start the lower bound of the range of c l s requisitis
	 * @param end the upper bound of the range of c l s requisitis (not inclusive)
	 * @return the range of matching c l s requisitis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSRequisiti> findByrequisitoByAuthorid(long authorUser,
		int start, int end) throws SystemException {
		return findByrequisitoByAuthorid(authorUser, start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s requisitis where authorUser = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSRequisitiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param authorUser the author user
	 * @param start the lower bound of the range of c l s requisitis
	 * @param end the upper bound of the range of c l s requisitis (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s requisitis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSRequisiti> findByrequisitoByAuthorid(long authorUser,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REQUISITOBYAUTHORID;
			finderArgs = new Object[] { authorUser };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_REQUISITOBYAUTHORID;
			finderArgs = new Object[] { authorUser, start, end, orderByComparator };
		}

		List<CLSRequisiti> list = (List<CLSRequisiti>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSRequisiti clsRequisiti : list) {
				if ((authorUser != clsRequisiti.getAuthorUser())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CLSREQUISITI_WHERE);

			query.append(_FINDER_COLUMN_REQUISITOBYAUTHORID_AUTHORUSER_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSRequisitiModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(authorUser);

				if (!pagination) {
					list = (List<CLSRequisiti>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSRequisiti>(list);
				}
				else {
					list = (List<CLSRequisiti>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s requisiti in the ordered set where authorUser = &#63;.
	 *
	 * @param authorUser the author user
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s requisiti
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException if a matching c l s requisiti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSRequisiti findByrequisitoByAuthorid_First(long authorUser,
		OrderByComparator orderByComparator)
		throws NoSuchCLSRequisitiException, SystemException {
		CLSRequisiti clsRequisiti = fetchByrequisitoByAuthorid_First(authorUser,
				orderByComparator);

		if (clsRequisiti != null) {
			return clsRequisiti;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("authorUser=");
		msg.append(authorUser);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSRequisitiException(msg.toString());
	}

	/**
	 * Returns the first c l s requisiti in the ordered set where authorUser = &#63;.
	 *
	 * @param authorUser the author user
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s requisiti, or <code>null</code> if a matching c l s requisiti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSRequisiti fetchByrequisitoByAuthorid_First(long authorUser,
		OrderByComparator orderByComparator) throws SystemException {
		List<CLSRequisiti> list = findByrequisitoByAuthorid(authorUser, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s requisiti in the ordered set where authorUser = &#63;.
	 *
	 * @param authorUser the author user
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s requisiti
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException if a matching c l s requisiti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSRequisiti findByrequisitoByAuthorid_Last(long authorUser,
		OrderByComparator orderByComparator)
		throws NoSuchCLSRequisitiException, SystemException {
		CLSRequisiti clsRequisiti = fetchByrequisitoByAuthorid_Last(authorUser,
				orderByComparator);

		if (clsRequisiti != null) {
			return clsRequisiti;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("authorUser=");
		msg.append(authorUser);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSRequisitiException(msg.toString());
	}

	/**
	 * Returns the last c l s requisiti in the ordered set where authorUser = &#63;.
	 *
	 * @param authorUser the author user
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s requisiti, or <code>null</code> if a matching c l s requisiti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSRequisiti fetchByrequisitoByAuthorid_Last(long authorUser,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByrequisitoByAuthorid(authorUser);

		if (count == 0) {
			return null;
		}

		List<CLSRequisiti> list = findByrequisitoByAuthorid(authorUser,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s requisitis before and after the current c l s requisiti in the ordered set where authorUser = &#63;.
	 *
	 * @param requisitoId the primary key of the current c l s requisiti
	 * @param authorUser the author user
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s requisiti
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException if a c l s requisiti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSRequisiti[] findByrequisitoByAuthorid_PrevAndNext(
		long requisitoId, long authorUser, OrderByComparator orderByComparator)
		throws NoSuchCLSRequisitiException, SystemException {
		CLSRequisiti clsRequisiti = findByPrimaryKey(requisitoId);

		Session session = null;

		try {
			session = openSession();

			CLSRequisiti[] array = new CLSRequisitiImpl[3];

			array[0] = getByrequisitoByAuthorid_PrevAndNext(session,
					clsRequisiti, authorUser, orderByComparator, true);

			array[1] = clsRequisiti;

			array[2] = getByrequisitoByAuthorid_PrevAndNext(session,
					clsRequisiti, authorUser, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSRequisiti getByrequisitoByAuthorid_PrevAndNext(
		Session session, CLSRequisiti clsRequisiti, long authorUser,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSREQUISITI_WHERE);

		query.append(_FINDER_COLUMN_REQUISITOBYAUTHORID_AUTHORUSER_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSRequisitiModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(authorUser);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsRequisiti);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSRequisiti> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s requisitis where authorUser = &#63; from the database.
	 *
	 * @param authorUser the author user
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByrequisitoByAuthorid(long authorUser)
		throws SystemException {
		for (CLSRequisiti clsRequisiti : findByrequisitoByAuthorid(authorUser,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(clsRequisiti);
		}
	}

	/**
	 * Returns the number of c l s requisitis where authorUser = &#63;.
	 *
	 * @param authorUser the author user
	 * @return the number of matching c l s requisitis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByrequisitoByAuthorid(long authorUser)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_REQUISITOBYAUTHORID;

		Object[] finderArgs = new Object[] { authorUser };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLSREQUISITI_WHERE);

			query.append(_FINDER_COLUMN_REQUISITOBYAUTHORID_AUTHORUSER_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(authorUser);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_REQUISITOBYAUTHORID_AUTHORUSER_2 = "clsRequisiti.authorUser = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_REQUISITOBYIDEAIDANDAUTHORID =
		new FinderPath(CLSRequisitiModelImpl.ENTITY_CACHE_ENABLED,
			CLSRequisitiModelImpl.FINDER_CACHE_ENABLED, CLSRequisitiImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByrequisitoByIdeaIdandAuthorid",
			new String[] {
				Long.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REQUISITOBYIDEAIDANDAUTHORID =
		new FinderPath(CLSRequisitiModelImpl.ENTITY_CACHE_ENABLED,
			CLSRequisitiModelImpl.FINDER_CACHE_ENABLED, CLSRequisitiImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByrequisitoByIdeaIdandAuthorid",
			new String[] { Long.class.getName(), Long.class.getName() },
			CLSRequisitiModelImpl.IDEAID_COLUMN_BITMASK |
			CLSRequisitiModelImpl.AUTHORUSER_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_REQUISITOBYIDEAIDANDAUTHORID =
		new FinderPath(CLSRequisitiModelImpl.ENTITY_CACHE_ENABLED,
			CLSRequisitiModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByrequisitoByIdeaIdandAuthorid",
			new String[] { Long.class.getName(), Long.class.getName() });

	/**
	 * Returns all the c l s requisitis where ideaId = &#63; and authorUser = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param authorUser the author user
	 * @return the matching c l s requisitis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSRequisiti> findByrequisitoByIdeaIdandAuthorid(long ideaId,
		long authorUser) throws SystemException {
		return findByrequisitoByIdeaIdandAuthorid(ideaId, authorUser,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s requisitis where ideaId = &#63; and authorUser = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSRequisitiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaId the idea ID
	 * @param authorUser the author user
	 * @param start the lower bound of the range of c l s requisitis
	 * @param end the upper bound of the range of c l s requisitis (not inclusive)
	 * @return the range of matching c l s requisitis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSRequisiti> findByrequisitoByIdeaIdandAuthorid(long ideaId,
		long authorUser, int start, int end) throws SystemException {
		return findByrequisitoByIdeaIdandAuthorid(ideaId, authorUser, start,
			end, null);
	}

	/**
	 * Returns an ordered range of all the c l s requisitis where ideaId = &#63; and authorUser = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSRequisitiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaId the idea ID
	 * @param authorUser the author user
	 * @param start the lower bound of the range of c l s requisitis
	 * @param end the upper bound of the range of c l s requisitis (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s requisitis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSRequisiti> findByrequisitoByIdeaIdandAuthorid(long ideaId,
		long authorUser, int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REQUISITOBYIDEAIDANDAUTHORID;
			finderArgs = new Object[] { ideaId, authorUser };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_REQUISITOBYIDEAIDANDAUTHORID;
			finderArgs = new Object[] {
					ideaId, authorUser,
					
					start, end, orderByComparator
				};
		}

		List<CLSRequisiti> list = (List<CLSRequisiti>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSRequisiti clsRequisiti : list) {
				if ((ideaId != clsRequisiti.getIdeaId()) ||
						(authorUser != clsRequisiti.getAuthorUser())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_CLSREQUISITI_WHERE);

			query.append(_FINDER_COLUMN_REQUISITOBYIDEAIDANDAUTHORID_IDEAID_2);

			query.append(_FINDER_COLUMN_REQUISITOBYIDEAIDANDAUTHORID_AUTHORUSER_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSRequisitiModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaId);

				qPos.add(authorUser);

				if (!pagination) {
					list = (List<CLSRequisiti>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSRequisiti>(list);
				}
				else {
					list = (List<CLSRequisiti>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s requisiti in the ordered set where ideaId = &#63; and authorUser = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param authorUser the author user
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s requisiti
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException if a matching c l s requisiti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSRequisiti findByrequisitoByIdeaIdandAuthorid_First(long ideaId,
		long authorUser, OrderByComparator orderByComparator)
		throws NoSuchCLSRequisitiException, SystemException {
		CLSRequisiti clsRequisiti = fetchByrequisitoByIdeaIdandAuthorid_First(ideaId,
				authorUser, orderByComparator);

		if (clsRequisiti != null) {
			return clsRequisiti;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaId=");
		msg.append(ideaId);

		msg.append(", authorUser=");
		msg.append(authorUser);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSRequisitiException(msg.toString());
	}

	/**
	 * Returns the first c l s requisiti in the ordered set where ideaId = &#63; and authorUser = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param authorUser the author user
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s requisiti, or <code>null</code> if a matching c l s requisiti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSRequisiti fetchByrequisitoByIdeaIdandAuthorid_First(long ideaId,
		long authorUser, OrderByComparator orderByComparator)
		throws SystemException {
		List<CLSRequisiti> list = findByrequisitoByIdeaIdandAuthorid(ideaId,
				authorUser, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s requisiti in the ordered set where ideaId = &#63; and authorUser = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param authorUser the author user
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s requisiti
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException if a matching c l s requisiti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSRequisiti findByrequisitoByIdeaIdandAuthorid_Last(long ideaId,
		long authorUser, OrderByComparator orderByComparator)
		throws NoSuchCLSRequisitiException, SystemException {
		CLSRequisiti clsRequisiti = fetchByrequisitoByIdeaIdandAuthorid_Last(ideaId,
				authorUser, orderByComparator);

		if (clsRequisiti != null) {
			return clsRequisiti;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaId=");
		msg.append(ideaId);

		msg.append(", authorUser=");
		msg.append(authorUser);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSRequisitiException(msg.toString());
	}

	/**
	 * Returns the last c l s requisiti in the ordered set where ideaId = &#63; and authorUser = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param authorUser the author user
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s requisiti, or <code>null</code> if a matching c l s requisiti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSRequisiti fetchByrequisitoByIdeaIdandAuthorid_Last(long ideaId,
		long authorUser, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByrequisitoByIdeaIdandAuthorid(ideaId, authorUser);

		if (count == 0) {
			return null;
		}

		List<CLSRequisiti> list = findByrequisitoByIdeaIdandAuthorid(ideaId,
				authorUser, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s requisitis before and after the current c l s requisiti in the ordered set where ideaId = &#63; and authorUser = &#63;.
	 *
	 * @param requisitoId the primary key of the current c l s requisiti
	 * @param ideaId the idea ID
	 * @param authorUser the author user
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s requisiti
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException if a c l s requisiti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSRequisiti[] findByrequisitoByIdeaIdandAuthorid_PrevAndNext(
		long requisitoId, long ideaId, long authorUser,
		OrderByComparator orderByComparator)
		throws NoSuchCLSRequisitiException, SystemException {
		CLSRequisiti clsRequisiti = findByPrimaryKey(requisitoId);

		Session session = null;

		try {
			session = openSession();

			CLSRequisiti[] array = new CLSRequisitiImpl[3];

			array[0] = getByrequisitoByIdeaIdandAuthorid_PrevAndNext(session,
					clsRequisiti, ideaId, authorUser, orderByComparator, true);

			array[1] = clsRequisiti;

			array[2] = getByrequisitoByIdeaIdandAuthorid_PrevAndNext(session,
					clsRequisiti, ideaId, authorUser, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSRequisiti getByrequisitoByIdeaIdandAuthorid_PrevAndNext(
		Session session, CLSRequisiti clsRequisiti, long ideaId,
		long authorUser, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSREQUISITI_WHERE);

		query.append(_FINDER_COLUMN_REQUISITOBYIDEAIDANDAUTHORID_IDEAID_2);

		query.append(_FINDER_COLUMN_REQUISITOBYIDEAIDANDAUTHORID_AUTHORUSER_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSRequisitiModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(ideaId);

		qPos.add(authorUser);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsRequisiti);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSRequisiti> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s requisitis where ideaId = &#63; and authorUser = &#63; from the database.
	 *
	 * @param ideaId the idea ID
	 * @param authorUser the author user
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByrequisitoByIdeaIdandAuthorid(long ideaId,
		long authorUser) throws SystemException {
		for (CLSRequisiti clsRequisiti : findByrequisitoByIdeaIdandAuthorid(
				ideaId, authorUser, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(clsRequisiti);
		}
	}

	/**
	 * Returns the number of c l s requisitis where ideaId = &#63; and authorUser = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param authorUser the author user
	 * @return the number of matching c l s requisitis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByrequisitoByIdeaIdandAuthorid(long ideaId, long authorUser)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_REQUISITOBYIDEAIDANDAUTHORID;

		Object[] finderArgs = new Object[] { ideaId, authorUser };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CLSREQUISITI_WHERE);

			query.append(_FINDER_COLUMN_REQUISITOBYIDEAIDANDAUTHORID_IDEAID_2);

			query.append(_FINDER_COLUMN_REQUISITOBYIDEAIDANDAUTHORID_AUTHORUSER_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaId);

				qPos.add(authorUser);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_REQUISITOBYIDEAIDANDAUTHORID_IDEAID_2 =
		"clsRequisiti.ideaId = ? AND ";
	private static final String _FINDER_COLUMN_REQUISITOBYIDEAIDANDAUTHORID_AUTHORUSER_2 =
		"clsRequisiti.authorUser = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_REQBYIDEAID_DESCR =
		new FinderPath(CLSRequisitiModelImpl.ENTITY_CACHE_ENABLED,
			CLSRequisitiModelImpl.FINDER_CACHE_ENABLED, CLSRequisitiImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByreqByIdeaId_Descr",
			new String[] {
				Long.class.getName(), String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REQBYIDEAID_DESCR =
		new FinderPath(CLSRequisitiModelImpl.ENTITY_CACHE_ENABLED,
			CLSRequisitiModelImpl.FINDER_CACHE_ENABLED, CLSRequisitiImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByreqByIdeaId_Descr",
			new String[] { Long.class.getName(), String.class.getName() },
			CLSRequisitiModelImpl.IDEAID_COLUMN_BITMASK |
			CLSRequisitiModelImpl.DESCRIZIONE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_REQBYIDEAID_DESCR = new FinderPath(CLSRequisitiModelImpl.ENTITY_CACHE_ENABLED,
			CLSRequisitiModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByreqByIdeaId_Descr",
			new String[] { Long.class.getName(), String.class.getName() });

	/**
	 * Returns all the c l s requisitis where ideaId = &#63; and descrizione = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param descrizione the descrizione
	 * @return the matching c l s requisitis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSRequisiti> findByreqByIdeaId_Descr(long ideaId,
		String descrizione) throws SystemException {
		return findByreqByIdeaId_Descr(ideaId, descrizione, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s requisitis where ideaId = &#63; and descrizione = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSRequisitiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaId the idea ID
	 * @param descrizione the descrizione
	 * @param start the lower bound of the range of c l s requisitis
	 * @param end the upper bound of the range of c l s requisitis (not inclusive)
	 * @return the range of matching c l s requisitis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSRequisiti> findByreqByIdeaId_Descr(long ideaId,
		String descrizione, int start, int end) throws SystemException {
		return findByreqByIdeaId_Descr(ideaId, descrizione, start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s requisitis where ideaId = &#63; and descrizione = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSRequisitiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaId the idea ID
	 * @param descrizione the descrizione
	 * @param start the lower bound of the range of c l s requisitis
	 * @param end the upper bound of the range of c l s requisitis (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s requisitis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSRequisiti> findByreqByIdeaId_Descr(long ideaId,
		String descrizione, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REQBYIDEAID_DESCR;
			finderArgs = new Object[] { ideaId, descrizione };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_REQBYIDEAID_DESCR;
			finderArgs = new Object[] {
					ideaId, descrizione,
					
					start, end, orderByComparator
				};
		}

		List<CLSRequisiti> list = (List<CLSRequisiti>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSRequisiti clsRequisiti : list) {
				if ((ideaId != clsRequisiti.getIdeaId()) ||
						!Validator.equals(descrizione,
							clsRequisiti.getDescrizione())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_CLSREQUISITI_WHERE);

			query.append(_FINDER_COLUMN_REQBYIDEAID_DESCR_IDEAID_2);

			boolean bindDescrizione = false;

			if (descrizione == null) {
				query.append(_FINDER_COLUMN_REQBYIDEAID_DESCR_DESCRIZIONE_1);
			}
			else if (descrizione.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_REQBYIDEAID_DESCR_DESCRIZIONE_3);
			}
			else {
				bindDescrizione = true;

				query.append(_FINDER_COLUMN_REQBYIDEAID_DESCR_DESCRIZIONE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSRequisitiModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaId);

				if (bindDescrizione) {
					qPos.add(descrizione);
				}

				if (!pagination) {
					list = (List<CLSRequisiti>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSRequisiti>(list);
				}
				else {
					list = (List<CLSRequisiti>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s requisiti in the ordered set where ideaId = &#63; and descrizione = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param descrizione the descrizione
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s requisiti
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException if a matching c l s requisiti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSRequisiti findByreqByIdeaId_Descr_First(long ideaId,
		String descrizione, OrderByComparator orderByComparator)
		throws NoSuchCLSRequisitiException, SystemException {
		CLSRequisiti clsRequisiti = fetchByreqByIdeaId_Descr_First(ideaId,
				descrizione, orderByComparator);

		if (clsRequisiti != null) {
			return clsRequisiti;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaId=");
		msg.append(ideaId);

		msg.append(", descrizione=");
		msg.append(descrizione);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSRequisitiException(msg.toString());
	}

	/**
	 * Returns the first c l s requisiti in the ordered set where ideaId = &#63; and descrizione = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param descrizione the descrizione
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s requisiti, or <code>null</code> if a matching c l s requisiti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSRequisiti fetchByreqByIdeaId_Descr_First(long ideaId,
		String descrizione, OrderByComparator orderByComparator)
		throws SystemException {
		List<CLSRequisiti> list = findByreqByIdeaId_Descr(ideaId, descrizione,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s requisiti in the ordered set where ideaId = &#63; and descrizione = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param descrizione the descrizione
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s requisiti
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException if a matching c l s requisiti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSRequisiti findByreqByIdeaId_Descr_Last(long ideaId,
		String descrizione, OrderByComparator orderByComparator)
		throws NoSuchCLSRequisitiException, SystemException {
		CLSRequisiti clsRequisiti = fetchByreqByIdeaId_Descr_Last(ideaId,
				descrizione, orderByComparator);

		if (clsRequisiti != null) {
			return clsRequisiti;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaId=");
		msg.append(ideaId);

		msg.append(", descrizione=");
		msg.append(descrizione);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSRequisitiException(msg.toString());
	}

	/**
	 * Returns the last c l s requisiti in the ordered set where ideaId = &#63; and descrizione = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param descrizione the descrizione
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s requisiti, or <code>null</code> if a matching c l s requisiti could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSRequisiti fetchByreqByIdeaId_Descr_Last(long ideaId,
		String descrizione, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByreqByIdeaId_Descr(ideaId, descrizione);

		if (count == 0) {
			return null;
		}

		List<CLSRequisiti> list = findByreqByIdeaId_Descr(ideaId, descrizione,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s requisitis before and after the current c l s requisiti in the ordered set where ideaId = &#63; and descrizione = &#63;.
	 *
	 * @param requisitoId the primary key of the current c l s requisiti
	 * @param ideaId the idea ID
	 * @param descrizione the descrizione
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s requisiti
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException if a c l s requisiti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSRequisiti[] findByreqByIdeaId_Descr_PrevAndNext(
		long requisitoId, long ideaId, String descrizione,
		OrderByComparator orderByComparator)
		throws NoSuchCLSRequisitiException, SystemException {
		CLSRequisiti clsRequisiti = findByPrimaryKey(requisitoId);

		Session session = null;

		try {
			session = openSession();

			CLSRequisiti[] array = new CLSRequisitiImpl[3];

			array[0] = getByreqByIdeaId_Descr_PrevAndNext(session,
					clsRequisiti, ideaId, descrizione, orderByComparator, true);

			array[1] = clsRequisiti;

			array[2] = getByreqByIdeaId_Descr_PrevAndNext(session,
					clsRequisiti, ideaId, descrizione, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSRequisiti getByreqByIdeaId_Descr_PrevAndNext(Session session,
		CLSRequisiti clsRequisiti, long ideaId, String descrizione,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSREQUISITI_WHERE);

		query.append(_FINDER_COLUMN_REQBYIDEAID_DESCR_IDEAID_2);

		boolean bindDescrizione = false;

		if (descrizione == null) {
			query.append(_FINDER_COLUMN_REQBYIDEAID_DESCR_DESCRIZIONE_1);
		}
		else if (descrizione.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_REQBYIDEAID_DESCR_DESCRIZIONE_3);
		}
		else {
			bindDescrizione = true;

			query.append(_FINDER_COLUMN_REQBYIDEAID_DESCR_DESCRIZIONE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSRequisitiModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(ideaId);

		if (bindDescrizione) {
			qPos.add(descrizione);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsRequisiti);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSRequisiti> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s requisitis where ideaId = &#63; and descrizione = &#63; from the database.
	 *
	 * @param ideaId the idea ID
	 * @param descrizione the descrizione
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByreqByIdeaId_Descr(long ideaId, String descrizione)
		throws SystemException {
		for (CLSRequisiti clsRequisiti : findByreqByIdeaId_Descr(ideaId,
				descrizione, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(clsRequisiti);
		}
	}

	/**
	 * Returns the number of c l s requisitis where ideaId = &#63; and descrizione = &#63;.
	 *
	 * @param ideaId the idea ID
	 * @param descrizione the descrizione
	 * @return the number of matching c l s requisitis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByreqByIdeaId_Descr(long ideaId, String descrizione)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_REQBYIDEAID_DESCR;

		Object[] finderArgs = new Object[] { ideaId, descrizione };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CLSREQUISITI_WHERE);

			query.append(_FINDER_COLUMN_REQBYIDEAID_DESCR_IDEAID_2);

			boolean bindDescrizione = false;

			if (descrizione == null) {
				query.append(_FINDER_COLUMN_REQBYIDEAID_DESCR_DESCRIZIONE_1);
			}
			else if (descrizione.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_REQBYIDEAID_DESCR_DESCRIZIONE_3);
			}
			else {
				bindDescrizione = true;

				query.append(_FINDER_COLUMN_REQBYIDEAID_DESCR_DESCRIZIONE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaId);

				if (bindDescrizione) {
					qPos.add(descrizione);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_REQBYIDEAID_DESCR_IDEAID_2 = "clsRequisiti.ideaId = ? AND ";
	private static final String _FINDER_COLUMN_REQBYIDEAID_DESCR_DESCRIZIONE_1 = "clsRequisiti.descrizione IS NULL";
	private static final String _FINDER_COLUMN_REQBYIDEAID_DESCR_DESCRIZIONE_2 = "clsRequisiti.descrizione = ?";
	private static final String _FINDER_COLUMN_REQBYIDEAID_DESCR_DESCRIZIONE_3 = "(clsRequisiti.descrizione IS NULL OR clsRequisiti.descrizione = '')";

	public CLSRequisitiPersistenceImpl() {
		setModelClass(CLSRequisiti.class);
	}

	/**
	 * Caches the c l s requisiti in the entity cache if it is enabled.
	 *
	 * @param clsRequisiti the c l s requisiti
	 */
	@Override
	public void cacheResult(CLSRequisiti clsRequisiti) {
		EntityCacheUtil.putResult(CLSRequisitiModelImpl.ENTITY_CACHE_ENABLED,
			CLSRequisitiImpl.class, clsRequisiti.getPrimaryKey(), clsRequisiti);

		clsRequisiti.resetOriginalValues();
	}

	/**
	 * Caches the c l s requisitis in the entity cache if it is enabled.
	 *
	 * @param clsRequisitis the c l s requisitis
	 */
	@Override
	public void cacheResult(List<CLSRequisiti> clsRequisitis) {
		for (CLSRequisiti clsRequisiti : clsRequisitis) {
			if (EntityCacheUtil.getResult(
						CLSRequisitiModelImpl.ENTITY_CACHE_ENABLED,
						CLSRequisitiImpl.class, clsRequisiti.getPrimaryKey()) == null) {
				cacheResult(clsRequisiti);
			}
			else {
				clsRequisiti.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all c l s requisitis.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CLSRequisitiImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CLSRequisitiImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the c l s requisiti.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CLSRequisiti clsRequisiti) {
		EntityCacheUtil.removeResult(CLSRequisitiModelImpl.ENTITY_CACHE_ENABLED,
			CLSRequisitiImpl.class, clsRequisiti.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<CLSRequisiti> clsRequisitis) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CLSRequisiti clsRequisiti : clsRequisitis) {
			EntityCacheUtil.removeResult(CLSRequisitiModelImpl.ENTITY_CACHE_ENABLED,
				CLSRequisitiImpl.class, clsRequisiti.getPrimaryKey());
		}
	}

	/**
	 * Creates a new c l s requisiti with the primary key. Does not add the c l s requisiti to the database.
	 *
	 * @param requisitoId the primary key for the new c l s requisiti
	 * @return the new c l s requisiti
	 */
	@Override
	public CLSRequisiti create(long requisitoId) {
		CLSRequisiti clsRequisiti = new CLSRequisitiImpl();

		clsRequisiti.setNew(true);
		clsRequisiti.setPrimaryKey(requisitoId);

		return clsRequisiti;
	}

	/**
	 * Removes the c l s requisiti with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param requisitoId the primary key of the c l s requisiti
	 * @return the c l s requisiti that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException if a c l s requisiti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSRequisiti remove(long requisitoId)
		throws NoSuchCLSRequisitiException, SystemException {
		return remove((Serializable)requisitoId);
	}

	/**
	 * Removes the c l s requisiti with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the c l s requisiti
	 * @return the c l s requisiti that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException if a c l s requisiti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSRequisiti remove(Serializable primaryKey)
		throws NoSuchCLSRequisitiException, SystemException {
		Session session = null;

		try {
			session = openSession();

			CLSRequisiti clsRequisiti = (CLSRequisiti)session.get(CLSRequisitiImpl.class,
					primaryKey);

			if (clsRequisiti == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCLSRequisitiException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(clsRequisiti);
		}
		catch (NoSuchCLSRequisitiException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CLSRequisiti removeImpl(CLSRequisiti clsRequisiti)
		throws SystemException {
		clsRequisiti = toUnwrappedModel(clsRequisiti);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(clsRequisiti)) {
				clsRequisiti = (CLSRequisiti)session.get(CLSRequisitiImpl.class,
						clsRequisiti.getPrimaryKeyObj());
			}

			if (clsRequisiti != null) {
				session.delete(clsRequisiti);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (clsRequisiti != null) {
			clearCache(clsRequisiti);
		}

		return clsRequisiti;
	}

	@Override
	public CLSRequisiti updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti clsRequisiti)
		throws SystemException {
		clsRequisiti = toUnwrappedModel(clsRequisiti);

		boolean isNew = clsRequisiti.isNew();

		CLSRequisitiModelImpl clsRequisitiModelImpl = (CLSRequisitiModelImpl)clsRequisiti;

		Session session = null;

		try {
			session = openSession();

			if (clsRequisiti.isNew()) {
				session.save(clsRequisiti);

				clsRequisiti.setNew(false);
			}
			else {
				session.merge(clsRequisiti);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !CLSRequisitiModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((clsRequisitiModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REQUISITOBYIDEAID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsRequisitiModelImpl.getOriginalIdeaId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_REQUISITOBYIDEAID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REQUISITOBYIDEAID,
					args);

				args = new Object[] { clsRequisitiModelImpl.getIdeaId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_REQUISITOBYIDEAID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REQUISITOBYIDEAID,
					args);
			}

			if ((clsRequisitiModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REQUISITOBYAUTHORID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsRequisitiModelImpl.getOriginalAuthorUser()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_REQUISITOBYAUTHORID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REQUISITOBYAUTHORID,
					args);

				args = new Object[] { clsRequisitiModelImpl.getAuthorUser() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_REQUISITOBYAUTHORID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REQUISITOBYAUTHORID,
					args);
			}

			if ((clsRequisitiModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REQUISITOBYIDEAIDANDAUTHORID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsRequisitiModelImpl.getOriginalIdeaId(),
						clsRequisitiModelImpl.getOriginalAuthorUser()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_REQUISITOBYIDEAIDANDAUTHORID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REQUISITOBYIDEAIDANDAUTHORID,
					args);

				args = new Object[] {
						clsRequisitiModelImpl.getIdeaId(),
						clsRequisitiModelImpl.getAuthorUser()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_REQUISITOBYIDEAIDANDAUTHORID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REQUISITOBYIDEAIDANDAUTHORID,
					args);
			}

			if ((clsRequisitiModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REQBYIDEAID_DESCR.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsRequisitiModelImpl.getOriginalIdeaId(),
						clsRequisitiModelImpl.getOriginalDescrizione()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_REQBYIDEAID_DESCR,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REQBYIDEAID_DESCR,
					args);

				args = new Object[] {
						clsRequisitiModelImpl.getIdeaId(),
						clsRequisitiModelImpl.getDescrizione()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_REQBYIDEAID_DESCR,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_REQBYIDEAID_DESCR,
					args);
			}
		}

		EntityCacheUtil.putResult(CLSRequisitiModelImpl.ENTITY_CACHE_ENABLED,
			CLSRequisitiImpl.class, clsRequisiti.getPrimaryKey(), clsRequisiti);

		return clsRequisiti;
	}

	protected CLSRequisiti toUnwrappedModel(CLSRequisiti clsRequisiti) {
		if (clsRequisiti instanceof CLSRequisitiImpl) {
			return clsRequisiti;
		}

		CLSRequisitiImpl clsRequisitiImpl = new CLSRequisitiImpl();

		clsRequisitiImpl.setNew(clsRequisiti.isNew());
		clsRequisitiImpl.setPrimaryKey(clsRequisiti.getPrimaryKey());

		clsRequisitiImpl.setRequisitoId(clsRequisiti.getRequisitoId());
		clsRequisitiImpl.setIdeaId(clsRequisiti.getIdeaId());
		clsRequisitiImpl.setTaskId(clsRequisiti.getTaskId());
		clsRequisitiImpl.setTipo(clsRequisiti.getTipo());
		clsRequisitiImpl.setDescrizione(clsRequisiti.getDescrizione());
		clsRequisitiImpl.setHelp(clsRequisiti.getHelp());
		clsRequisitiImpl.setAuthorUser(clsRequisiti.getAuthorUser());
		clsRequisitiImpl.setCategory(clsRequisiti.getCategory());
		clsRequisitiImpl.setMultiTask(clsRequisiti.isMultiTask());
		clsRequisitiImpl.setOutcomeFile(clsRequisiti.isOutcomeFile());
		clsRequisitiImpl.setTaskDoubleField(clsRequisiti.isTaskDoubleField());

		return clsRequisitiImpl;
	}

	/**
	 * Returns the c l s requisiti with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the c l s requisiti
	 * @return the c l s requisiti
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException if a c l s requisiti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSRequisiti findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCLSRequisitiException, SystemException {
		CLSRequisiti clsRequisiti = fetchByPrimaryKey(primaryKey);

		if (clsRequisiti == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCLSRequisitiException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return clsRequisiti;
	}

	/**
	 * Returns the c l s requisiti with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException} if it could not be found.
	 *
	 * @param requisitoId the primary key of the c l s requisiti
	 * @return the c l s requisiti
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSRequisitiException if a c l s requisiti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSRequisiti findByPrimaryKey(long requisitoId)
		throws NoSuchCLSRequisitiException, SystemException {
		return findByPrimaryKey((Serializable)requisitoId);
	}

	/**
	 * Returns the c l s requisiti with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the c l s requisiti
	 * @return the c l s requisiti, or <code>null</code> if a c l s requisiti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSRequisiti fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		CLSRequisiti clsRequisiti = (CLSRequisiti)EntityCacheUtil.getResult(CLSRequisitiModelImpl.ENTITY_CACHE_ENABLED,
				CLSRequisitiImpl.class, primaryKey);

		if (clsRequisiti == _nullCLSRequisiti) {
			return null;
		}

		if (clsRequisiti == null) {
			Session session = null;

			try {
				session = openSession();

				clsRequisiti = (CLSRequisiti)session.get(CLSRequisitiImpl.class,
						primaryKey);

				if (clsRequisiti != null) {
					cacheResult(clsRequisiti);
				}
				else {
					EntityCacheUtil.putResult(CLSRequisitiModelImpl.ENTITY_CACHE_ENABLED,
						CLSRequisitiImpl.class, primaryKey, _nullCLSRequisiti);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(CLSRequisitiModelImpl.ENTITY_CACHE_ENABLED,
					CLSRequisitiImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return clsRequisiti;
	}

	/**
	 * Returns the c l s requisiti with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param requisitoId the primary key of the c l s requisiti
	 * @return the c l s requisiti, or <code>null</code> if a c l s requisiti with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSRequisiti fetchByPrimaryKey(long requisitoId)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)requisitoId);
	}

	/**
	 * Returns all the c l s requisitis.
	 *
	 * @return the c l s requisitis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSRequisiti> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s requisitis.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSRequisitiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of c l s requisitis
	 * @param end the upper bound of the range of c l s requisitis (not inclusive)
	 * @return the range of c l s requisitis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSRequisiti> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s requisitis.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSRequisitiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of c l s requisitis
	 * @param end the upper bound of the range of c l s requisitis (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of c l s requisitis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSRequisiti> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CLSRequisiti> list = (List<CLSRequisiti>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CLSREQUISITI);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CLSREQUISITI;

				if (pagination) {
					sql = sql.concat(CLSRequisitiModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<CLSRequisiti>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSRequisiti>(list);
				}
				else {
					list = (List<CLSRequisiti>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the c l s requisitis from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (CLSRequisiti clsRequisiti : findAll()) {
			remove(clsRequisiti);
		}
	}

	/**
	 * Returns the number of c l s requisitis.
	 *
	 * @return the number of c l s requisitis
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CLSREQUISITI);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the c l s requisiti persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.ideas.challenges.servicelayer.model.CLSRequisiti")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<CLSRequisiti>> listenersList = new ArrayList<ModelListener<CLSRequisiti>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<CLSRequisiti>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CLSRequisitiImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_CLSREQUISITI = "SELECT clsRequisiti FROM CLSRequisiti clsRequisiti";
	private static final String _SQL_SELECT_CLSREQUISITI_WHERE = "SELECT clsRequisiti FROM CLSRequisiti clsRequisiti WHERE ";
	private static final String _SQL_COUNT_CLSREQUISITI = "SELECT COUNT(clsRequisiti) FROM CLSRequisiti clsRequisiti";
	private static final String _SQL_COUNT_CLSREQUISITI_WHERE = "SELECT COUNT(clsRequisiti) FROM CLSRequisiti clsRequisiti WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "clsRequisiti.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CLSRequisiti exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No CLSRequisiti exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CLSRequisitiPersistenceImpl.class);
	private static CLSRequisiti _nullCLSRequisiti = new CLSRequisitiImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<CLSRequisiti> toCacheModel() {
				return _nullCLSRequisitiCacheModel;
			}
		};

	private static CacheModel<CLSRequisiti> _nullCLSRequisitiCacheModel = new CacheModel<CLSRequisiti>() {
			@Override
			public CLSRequisiti toEntityModel() {
				return _nullCLSRequisiti;
			}
		};
}