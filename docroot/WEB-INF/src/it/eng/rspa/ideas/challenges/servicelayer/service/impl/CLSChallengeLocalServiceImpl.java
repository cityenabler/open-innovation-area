/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.impl;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.base.CLSChallengeLocalServiceBaseImpl;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSChallengeUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.CLSIdeaUtil;
import it.eng.rspa.ideas.controlpanel.IdeaManagementSystemProperties;
import it.eng.rspa.ideas.utils.ChallengesUtils;
import it.eng.rspa.ideas.utils.MailUtil;
import it.eng.rspa.ideas.utils.MyUtils;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.portlet.ActionRequest;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.notifications.NotificationEvent;
import com.liferay.portal.kernel.notifications.NotificationEventFactoryUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Company;
import com.liferay.portal.model.User;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.service.UserNotificationEventLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;

/**
 * The implementation of the c l s challenge local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.base.CLSChallengeLocalServiceBaseImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeLocalServiceUtil
 */
public class CLSChallengeLocalServiceImpl
	extends CLSChallengeLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeLocalServiceUtil} to access the c l s challenge local service.
	 */
	
	
	/*Gestione localizzazione*/
	ResourceBundle res = ResourceBundle.getBundle("Language", new Locale(Locale.getDefault().toString(), Locale.getDefault().getDisplayCountry()));
	
	
	public void sendNotification(String textMessage, long destinationUderId, String senderUserName, long challengeId, ActionRequest actionRequest, String azione) throws PortalException, SystemException{
		
		
		boolean emailNotificationsEnabled = IdeaManagementSystemProperties.getEnabledProperty("emailNotificationsEnabled");
		boolean dockbarNotificationsEnabled = IdeaManagementSystemProperties.getEnabledProperty("dockbarNotificationsEnabled");
		
		User destinationUser = UserLocalServiceUtil.fetchUser(destinationUderId);
		
		String senderName = IdeaManagementSystemProperties.getProperty("senderNotificheMailIdeario"); //"IMS";
		
		String oggettoMail = LanguageUtil.get(destinationUser.getLocale(), "ims.open-innovation-area");
		//String oggettoMail = IdeaManagementSystemProperties.getProperty("oggettoNotificheMailIdeario"); //"Idea Management System";
		
		String utenzaMail = IdeaManagementSystemProperties.getProperty("utenzaMail"); //"openinnovationarea";
		
		ThemeDisplay td  = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String lfu = PortalUtil.getLayoutFriendlyURL(td.getLayout(), td); //http://localhost:8080/web/guest/innovation-area	
		//String urlGara = lfu + "/-/challenges_explorer_contest/"+String.valueOf(challengeId)+"/view";
		
		String urlGara =  IdeaManagementSystemProperties.getFriendlyUrlChallenges(challengeId);
		
		
		JSONObject notificationEventJSONObject = JSONFactoryUtil.createJSONObject();
		
		notificationEventJSONObject.put("text-message",textMessage);
		notificationEventJSONObject.put("userId",destinationUderId);
		notificationEventJSONObject.put("challengeId",challengeId);
		notificationEventJSONObject.put("sender",senderUserName);
		notificationEventJSONObject.put("classPK", challengeId);
		
		if (azione.equalsIgnoreCase("delete"))
			notificationEventJSONObject.put("currentURL", lfu );
		else
			notificationEventJSONObject.put("currentURL", urlGara );
		
		if (dockbarNotificationsEnabled){

			NotificationEvent notificationEvent = NotificationEventFactoryUtil.createNotificationEvent(System.currentTimeMillis(),"Challenges_WAR_Challenge62portlet", notificationEventJSONObject);
			notificationEvent.setDeliveryRequired(0);
			UserNotificationEventLocalServiceUtil.addUserNotificationEvent(destinationUderId,notificationEvent);
		}
		
		
		CLSChallenge challenge = CLSChallengeLocalServiceUtil.fetchCLSChallenge(challengeId);
		Company  company = CompanyLocalServiceUtil.fetchCompany(challenge.getCompanyId());
		String fromMail = utenzaMail+"@" + company.getVirtualHostname();
		
		
		if (emailNotificationsEnabled){

			try {
				MailUtil.sendMail(fromMail, senderName, destinationUser.getEmailAddress(), destinationUser.getFullName(), oggettoMail, textMessage, false);
							
				
			} catch (UnsupportedEncodingException e) {
	
				e.printStackTrace();
			}
		}
	}
	
	
	
	
	public void sendNotification(String textMessage, long destinationUderId, String senderUserName) throws PortalException, SystemException{
		
		boolean dockbarNotificationsEnabled = IdeaManagementSystemProperties.getEnabledProperty("dockbarNotificationsEnabled");
		
		if (!dockbarNotificationsEnabled)
			return;
		
		
		JSONObject notificationEventJSONObject = JSONFactoryUtil.createJSONObject();
		
		notificationEventJSONObject.put("text-message",textMessage);
		notificationEventJSONObject.put("userId",destinationUderId);
		notificationEventJSONObject.put("sender",senderUserName);

		NotificationEvent notificationEvent = NotificationEventFactoryUtil.createNotificationEvent(System.currentTimeMillis(),"Challenges_WAR_Challenge62portlet", notificationEventJSONObject);
		notificationEvent.setDeliveryRequired(0);
		UserNotificationEventLocalServiceUtil.addUserNotificationEvent(destinationUderId,notificationEvent);
	}
	
	public List<CLSIdea> getIdeasForChallenge(long challengeId) throws SystemException{
		return CLSIdeaUtil.findByIdeaChallengeId(challengeId);
	}
	
	public List<CLSChallenge> getChallengesByUserId(long userId) throws SystemException{
		return CLSChallengeUtil.findByChallengeUserId(userId);
	}
	
	
	/**
	 * @param userId
	 * @param maxNumber
	 * @param locale
	 * @return
	 */
	public JSONArray getChallengesForMeByUserId (long userId, int maxNumber, Locale locale){
		return ChallengesUtils.getChallengesForMeByUserId(userId,maxNumber,locale );
	}
	
	/**
	 * @param pilot
	 * @param maxNumber
	 * @param locale
	 * @return
	 */
	public JSONArray getTopRatedChallengesByPilot (String pilot, int maxNumber, Locale locale){
		return ChallengesUtils.getTopRatedChallengesByPilotOrLanguage(pilot,"",maxNumber,locale );
	}
	
	/**
	 * @param language
	 * @param maxNumber
	 * @param locale
	 * @return
	 */
	public JSONArray getTopRatedChallengesByLanguage (String language, int maxNumber, Locale locale){
		return ChallengesUtils.getTopRatedChallengesByPilotOrLanguage("",language,maxNumber,locale );
	}
	
	public List<CLSChallenge> getActiveChallenges() throws SystemException{
		Date now = new Date();
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(CLSChallenge.class, "challenge");
		dynamicQuery.add(PropertyFactoryUtil.forName("challenge.dateStart").le(now));
		dynamicQuery.add(PropertyFactoryUtil.forName("challenge.dateEnd").gt(now));
		dynamicQuery.setLimit(com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS, com.liferay.portal.kernel.dao.orm.QueryUtil.ALL_POS);		
		return CLSChallengeUtil.findWithDynamicQuery(dynamicQuery);
	}
	
	
	
}