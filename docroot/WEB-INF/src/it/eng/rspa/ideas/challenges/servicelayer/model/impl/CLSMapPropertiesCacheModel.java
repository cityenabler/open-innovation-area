/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSMapProperties;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing CLSMapProperties in entity cache.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSMapProperties
 * @generated
 */
public class CLSMapPropertiesCacheModel implements CacheModel<CLSMapProperties>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(7);

		sb.append("{mapPropertiesId=");
		sb.append(mapPropertiesId);
		sb.append(", mapCenterLatitude=");
		sb.append(mapCenterLatitude);
		sb.append(", mapCenterLongitude=");
		sb.append(mapCenterLongitude);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public CLSMapProperties toEntityModel() {
		CLSMapPropertiesImpl clsMapPropertiesImpl = new CLSMapPropertiesImpl();

		clsMapPropertiesImpl.setMapPropertiesId(mapPropertiesId);

		if (mapCenterLatitude == null) {
			clsMapPropertiesImpl.setMapCenterLatitude(StringPool.BLANK);
		}
		else {
			clsMapPropertiesImpl.setMapCenterLatitude(mapCenterLatitude);
		}

		if (mapCenterLongitude == null) {
			clsMapPropertiesImpl.setMapCenterLongitude(StringPool.BLANK);
		}
		else {
			clsMapPropertiesImpl.setMapCenterLongitude(mapCenterLongitude);
		}

		clsMapPropertiesImpl.resetOriginalValues();

		return clsMapPropertiesImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		mapPropertiesId = objectInput.readLong();
		mapCenterLatitude = objectInput.readUTF();
		mapCenterLongitude = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(mapPropertiesId);

		if (mapCenterLatitude == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(mapCenterLatitude);
		}

		if (mapCenterLongitude == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(mapCenterLongitude);
		}
	}

	public long mapPropertiesId;
	public String mapCenterLatitude;
	public String mapCenterLongitude;
}