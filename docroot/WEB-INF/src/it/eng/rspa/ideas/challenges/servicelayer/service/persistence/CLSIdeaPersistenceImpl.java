/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaImpl;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the c l s idea service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSIdeaPersistence
 * @see CLSIdeaUtil
 * @generated
 */
public class CLSIdeaPersistenceImpl extends BasePersistenceImpl<CLSIdea>
	implements CLSIdeaPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CLSIdeaUtil} to access the c l s idea persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CLSIdeaImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, CLSIdeaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, CLSIdeaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID = new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, CLSIdeaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID = new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, CLSIdeaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] { String.class.getName() },
			CLSIdeaModelImpl.UUID_COLUMN_BITMASK |
			CLSIdeaModelImpl.DATEADDED_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID = new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] { String.class.getName() });

	/**
	 * Returns all the c l s ideas where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdea> findByUuid(String uuid) throws SystemException {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s ideas where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of c l s ideas
	 * @param end the upper bound of the range of c l s ideas (not inclusive)
	 * @return the range of matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdea> findByUuid(String uuid, int start, int end)
		throws SystemException {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s ideas where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of c l s ideas
	 * @param end the upper bound of the range of c l s ideas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdea> findByUuid(String uuid, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID;
			finderArgs = new Object[] { uuid, start, end, orderByComparator };
		}

		List<CLSIdea> list = (List<CLSIdea>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSIdea clsIdea : list) {
				if (!Validator.equals(uuid, clsIdea.getUuid())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CLSIDEA_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSIdeaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<CLSIdea>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSIdea>(list);
				}
				else {
					list = (List<CLSIdea>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s idea in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s idea
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea findByUuid_First(String uuid,
		OrderByComparator orderByComparator)
		throws NoSuchCLSIdeaException, SystemException {
		CLSIdea clsIdea = fetchByUuid_First(uuid, orderByComparator);

		if (clsIdea != null) {
			return clsIdea;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSIdeaException(msg.toString());
	}

	/**
	 * Returns the first c l s idea in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea fetchByUuid_First(String uuid,
		OrderByComparator orderByComparator) throws SystemException {
		List<CLSIdea> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s idea in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s idea
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea findByUuid_Last(String uuid,
		OrderByComparator orderByComparator)
		throws NoSuchCLSIdeaException, SystemException {
		CLSIdea clsIdea = fetchByUuid_Last(uuid, orderByComparator);

		if (clsIdea != null) {
			return clsIdea;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSIdeaException(msg.toString());
	}

	/**
	 * Returns the last c l s idea in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea fetchByUuid_Last(String uuid,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<CLSIdea> list = findByUuid(uuid, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s ideas before and after the current c l s idea in the ordered set where uuid = &#63;.
	 *
	 * @param ideaID the primary key of the current c l s idea
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s idea
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a c l s idea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea[] findByUuid_PrevAndNext(long ideaID, String uuid,
		OrderByComparator orderByComparator)
		throws NoSuchCLSIdeaException, SystemException {
		CLSIdea clsIdea = findByPrimaryKey(ideaID);

		Session session = null;

		try {
			session = openSession();

			CLSIdea[] array = new CLSIdeaImpl[3];

			array[0] = getByUuid_PrevAndNext(session, clsIdea, uuid,
					orderByComparator, true);

			array[1] = clsIdea;

			array[2] = getByUuid_PrevAndNext(session, clsIdea, uuid,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSIdea getByUuid_PrevAndNext(Session session, CLSIdea clsIdea,
		String uuid, OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSIDEA_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSIdeaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsIdea);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSIdea> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s ideas where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByUuid(String uuid) throws SystemException {
		for (CLSIdea clsIdea : findByUuid(uuid, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(clsIdea);
		}
	}

	/**
	 * Returns the number of c l s ideas where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUuid(String uuid) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID;

		Object[] finderArgs = new Object[] { uuid };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLSIDEA_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_1 = "clsIdea.uuid IS NULL";
	private static final String _FINDER_COLUMN_UUID_UUID_2 = "clsIdea.uuid = ?";
	private static final String _FINDER_COLUMN_UUID_UUID_3 = "(clsIdea.uuid IS NULL OR clsIdea.uuid = '')";
	public static final FinderPath FINDER_PATH_FETCH_BY_UUID_G = new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, CLSIdeaImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() },
			CLSIdeaModelImpl.UUID_COLUMN_BITMASK |
			CLSIdeaModelImpl.GROUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_G = new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns the c l s idea where uuid = &#63; and groupId = &#63; or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException} if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching c l s idea
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea findByUUID_G(String uuid, long groupId)
		throws NoSuchCLSIdeaException, SystemException {
		CLSIdea clsIdea = fetchByUUID_G(uuid, groupId);

		if (clsIdea == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("uuid=");
			msg.append(uuid);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchCLSIdeaException(msg.toString());
		}

		return clsIdea;
	}

	/**
	 * Returns the c l s idea where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea fetchByUUID_G(String uuid, long groupId)
		throws SystemException {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the c l s idea where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea fetchByUUID_G(String uuid, long groupId,
		boolean retrieveFromCache) throws SystemException {
		Object[] finderArgs = new Object[] { uuid, groupId };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_UUID_G,
					finderArgs, this);
		}

		if (result instanceof CLSIdea) {
			CLSIdea clsIdea = (CLSIdea)result;

			if (!Validator.equals(uuid, clsIdea.getUuid()) ||
					(groupId != clsIdea.getGroupId())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_CLSIDEA_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				List<CLSIdea> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G,
						finderArgs, list);
				}
				else {
					CLSIdea clsIdea = list.get(0);

					result = clsIdea;

					cacheResult(clsIdea);

					if ((clsIdea.getUuid() == null) ||
							!clsIdea.getUuid().equals(uuid) ||
							(clsIdea.getGroupId() != groupId)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G,
							finderArgs, clsIdea);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_UUID_G,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (CLSIdea)result;
		}
	}

	/**
	 * Removes the c l s idea where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the c l s idea that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea removeByUUID_G(String uuid, long groupId)
		throws NoSuchCLSIdeaException, SystemException {
		CLSIdea clsIdea = findByUUID_G(uuid, groupId);

		return remove(clsIdea);
	}

	/**
	 * Returns the number of c l s ideas where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_G;

		Object[] finderArgs = new Object[] { uuid, groupId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CLSIDEA_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_1 = "clsIdea.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_2 = "clsIdea.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_G_UUID_3 = "(clsIdea.uuid IS NULL OR clsIdea.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 = "clsIdea.groupId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C = new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, CLSIdeaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C =
		new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, CLSIdeaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() },
			CLSIdeaModelImpl.UUID_COLUMN_BITMASK |
			CLSIdeaModelImpl.COMPANYID_COLUMN_BITMASK |
			CLSIdeaModelImpl.DATEADDED_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_UUID_C = new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] { String.class.getName(), Long.class.getName() });

	/**
	 * Returns all the c l s ideas where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdea> findByUuid_C(String uuid, long companyId)
		throws SystemException {
		return findByUuid_C(uuid, companyId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s ideas where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of c l s ideas
	 * @param end the upper bound of the range of c l s ideas (not inclusive)
	 * @return the range of matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdea> findByUuid_C(String uuid, long companyId, int start,
		int end) throws SystemException {
		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s ideas where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of c l s ideas
	 * @param end the upper bound of the range of c l s ideas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdea> findByUuid_C(String uuid, long companyId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] { uuid, companyId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_UUID_C;
			finderArgs = new Object[] {
					uuid, companyId,
					
					start, end, orderByComparator
				};
		}

		List<CLSIdea> list = (List<CLSIdea>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSIdea clsIdea : list) {
				if (!Validator.equals(uuid, clsIdea.getUuid()) ||
						(companyId != clsIdea.getCompanyId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(4 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_CLSIDEA_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSIdeaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<CLSIdea>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSIdea>(list);
				}
				else {
					list = (List<CLSIdea>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s idea in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s idea
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea findByUuid_C_First(String uuid, long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSIdeaException, SystemException {
		CLSIdea clsIdea = fetchByUuid_C_First(uuid, companyId, orderByComparator);

		if (clsIdea != null) {
			return clsIdea;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSIdeaException(msg.toString());
	}

	/**
	 * Returns the first c l s idea in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea fetchByUuid_C_First(String uuid, long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		List<CLSIdea> list = findByUuid_C(uuid, companyId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s idea in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s idea
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea findByUuid_C_Last(String uuid, long companyId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSIdeaException, SystemException {
		CLSIdea clsIdea = fetchByUuid_C_Last(uuid, companyId, orderByComparator);

		if (clsIdea != null) {
			return clsIdea;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSIdeaException(msg.toString());
	}

	/**
	 * Returns the last c l s idea in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea fetchByUuid_C_Last(String uuid, long companyId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<CLSIdea> list = findByUuid_C(uuid, companyId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s ideas before and after the current c l s idea in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param ideaID the primary key of the current c l s idea
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s idea
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a c l s idea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea[] findByUuid_C_PrevAndNext(long ideaID, String uuid,
		long companyId, OrderByComparator orderByComparator)
		throws NoSuchCLSIdeaException, SystemException {
		CLSIdea clsIdea = findByPrimaryKey(ideaID);

		Session session = null;

		try {
			session = openSession();

			CLSIdea[] array = new CLSIdeaImpl[3];

			array[0] = getByUuid_C_PrevAndNext(session, clsIdea, uuid,
					companyId, orderByComparator, true);

			array[1] = clsIdea;

			array[2] = getByUuid_C_PrevAndNext(session, clsIdea, uuid,
					companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSIdea getByUuid_C_PrevAndNext(Session session, CLSIdea clsIdea,
		String uuid, long companyId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSIDEA_WHERE);

		boolean bindUuid = false;

		if (uuid == null) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_1);
		}
		else if (uuid.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSIdeaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsIdea);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSIdea> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s ideas where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId)
		throws SystemException {
		for (CLSIdea clsIdea : findByUuid_C(uuid, companyId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(clsIdea);
		}
	}

	/**
	 * Returns the number of c l s ideas where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_UUID_C;

		Object[] finderArgs = new Object[] { uuid, companyId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CLSIDEA_WHERE);

			boolean bindUuid = false;

			if (uuid == null) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_1);
			}
			else if (uuid.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_1 = "clsIdea.uuid IS NULL AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_2 = "clsIdea.uuid = ? AND ";
	private static final String _FINDER_COLUMN_UUID_C_UUID_3 = "(clsIdea.uuid IS NULL OR clsIdea.uuid = '') AND ";
	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 = "clsIdea.companyId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEATITLE =
		new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, CLSIdeaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByIdeaTitle",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEATITLE =
		new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, CLSIdeaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByIdeaTitle",
			new String[] { String.class.getName() },
			CLSIdeaModelImpl.IDEATITLE_COLUMN_BITMASK |
			CLSIdeaModelImpl.DATEADDED_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_IDEATITLE = new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByIdeaTitle",
			new String[] { String.class.getName() });

	/**
	 * Returns all the c l s ideas where ideaTitle = &#63;.
	 *
	 * @param ideaTitle the idea title
	 * @return the matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdea> findByIdeaTitle(String ideaTitle)
		throws SystemException {
		return findByIdeaTitle(ideaTitle, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the c l s ideas where ideaTitle = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaTitle the idea title
	 * @param start the lower bound of the range of c l s ideas
	 * @param end the upper bound of the range of c l s ideas (not inclusive)
	 * @return the range of matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdea> findByIdeaTitle(String ideaTitle, int start, int end)
		throws SystemException {
		return findByIdeaTitle(ideaTitle, start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s ideas where ideaTitle = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param ideaTitle the idea title
	 * @param start the lower bound of the range of c l s ideas
	 * @param end the upper bound of the range of c l s ideas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdea> findByIdeaTitle(String ideaTitle, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEATITLE;
			finderArgs = new Object[] { ideaTitle };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEATITLE;
			finderArgs = new Object[] { ideaTitle, start, end, orderByComparator };
		}

		List<CLSIdea> list = (List<CLSIdea>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSIdea clsIdea : list) {
				if (!Validator.equals(ideaTitle, clsIdea.getIdeaTitle())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CLSIDEA_WHERE);

			boolean bindIdeaTitle = false;

			if (ideaTitle == null) {
				query.append(_FINDER_COLUMN_IDEATITLE_IDEATITLE_1);
			}
			else if (ideaTitle.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_IDEATITLE_IDEATITLE_3);
			}
			else {
				bindIdeaTitle = true;

				query.append(_FINDER_COLUMN_IDEATITLE_IDEATITLE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSIdeaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindIdeaTitle) {
					qPos.add(ideaTitle);
				}

				if (!pagination) {
					list = (List<CLSIdea>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSIdea>(list);
				}
				else {
					list = (List<CLSIdea>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s idea in the ordered set where ideaTitle = &#63;.
	 *
	 * @param ideaTitle the idea title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s idea
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea findByIdeaTitle_First(String ideaTitle,
		OrderByComparator orderByComparator)
		throws NoSuchCLSIdeaException, SystemException {
		CLSIdea clsIdea = fetchByIdeaTitle_First(ideaTitle, orderByComparator);

		if (clsIdea != null) {
			return clsIdea;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaTitle=");
		msg.append(ideaTitle);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSIdeaException(msg.toString());
	}

	/**
	 * Returns the first c l s idea in the ordered set where ideaTitle = &#63;.
	 *
	 * @param ideaTitle the idea title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea fetchByIdeaTitle_First(String ideaTitle,
		OrderByComparator orderByComparator) throws SystemException {
		List<CLSIdea> list = findByIdeaTitle(ideaTitle, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s idea in the ordered set where ideaTitle = &#63;.
	 *
	 * @param ideaTitle the idea title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s idea
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea findByIdeaTitle_Last(String ideaTitle,
		OrderByComparator orderByComparator)
		throws NoSuchCLSIdeaException, SystemException {
		CLSIdea clsIdea = fetchByIdeaTitle_Last(ideaTitle, orderByComparator);

		if (clsIdea != null) {
			return clsIdea;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("ideaTitle=");
		msg.append(ideaTitle);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSIdeaException(msg.toString());
	}

	/**
	 * Returns the last c l s idea in the ordered set where ideaTitle = &#63;.
	 *
	 * @param ideaTitle the idea title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea fetchByIdeaTitle_Last(String ideaTitle,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByIdeaTitle(ideaTitle);

		if (count == 0) {
			return null;
		}

		List<CLSIdea> list = findByIdeaTitle(ideaTitle, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s ideas before and after the current c l s idea in the ordered set where ideaTitle = &#63;.
	 *
	 * @param ideaID the primary key of the current c l s idea
	 * @param ideaTitle the idea title
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s idea
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a c l s idea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea[] findByIdeaTitle_PrevAndNext(long ideaID, String ideaTitle,
		OrderByComparator orderByComparator)
		throws NoSuchCLSIdeaException, SystemException {
		CLSIdea clsIdea = findByPrimaryKey(ideaID);

		Session session = null;

		try {
			session = openSession();

			CLSIdea[] array = new CLSIdeaImpl[3];

			array[0] = getByIdeaTitle_PrevAndNext(session, clsIdea, ideaTitle,
					orderByComparator, true);

			array[1] = clsIdea;

			array[2] = getByIdeaTitle_PrevAndNext(session, clsIdea, ideaTitle,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSIdea getByIdeaTitle_PrevAndNext(Session session,
		CLSIdea clsIdea, String ideaTitle, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSIDEA_WHERE);

		boolean bindIdeaTitle = false;

		if (ideaTitle == null) {
			query.append(_FINDER_COLUMN_IDEATITLE_IDEATITLE_1);
		}
		else if (ideaTitle.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_IDEATITLE_IDEATITLE_3);
		}
		else {
			bindIdeaTitle = true;

			query.append(_FINDER_COLUMN_IDEATITLE_IDEATITLE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSIdeaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindIdeaTitle) {
			qPos.add(ideaTitle);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsIdea);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSIdea> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s ideas where ideaTitle = &#63; from the database.
	 *
	 * @param ideaTitle the idea title
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByIdeaTitle(String ideaTitle) throws SystemException {
		for (CLSIdea clsIdea : findByIdeaTitle(ideaTitle, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(clsIdea);
		}
	}

	/**
	 * Returns the number of c l s ideas where ideaTitle = &#63;.
	 *
	 * @param ideaTitle the idea title
	 * @return the number of matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByIdeaTitle(String ideaTitle) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_IDEATITLE;

		Object[] finderArgs = new Object[] { ideaTitle };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLSIDEA_WHERE);

			boolean bindIdeaTitle = false;

			if (ideaTitle == null) {
				query.append(_FINDER_COLUMN_IDEATITLE_IDEATITLE_1);
			}
			else if (ideaTitle.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_IDEATITLE_IDEATITLE_3);
			}
			else {
				bindIdeaTitle = true;

				query.append(_FINDER_COLUMN_IDEATITLE_IDEATITLE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindIdeaTitle) {
					qPos.add(ideaTitle);
				}

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_IDEATITLE_IDEATITLE_1 = "clsIdea.ideaTitle IS NULL";
	private static final String _FINDER_COLUMN_IDEATITLE_IDEATITLE_2 = "clsIdea.ideaTitle = ?";
	private static final String _FINDER_COLUMN_IDEATITLE_IDEATITLE_3 = "(clsIdea.ideaTitle IS NULL OR clsIdea.ideaTitle = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEAUSERID =
		new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, CLSIdeaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByIdeaUserId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAUSERID =
		new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, CLSIdeaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByIdeaUserId",
			new String[] { Long.class.getName() },
			CLSIdeaModelImpl.USERID_COLUMN_BITMASK |
			CLSIdeaModelImpl.DATEADDED_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_IDEAUSERID = new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByIdeaUserId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the c l s ideas where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdea> findByIdeaUserId(long userId)
		throws SystemException {
		return findByIdeaUserId(userId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the c l s ideas where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of c l s ideas
	 * @param end the upper bound of the range of c l s ideas (not inclusive)
	 * @return the range of matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdea> findByIdeaUserId(long userId, int start, int end)
		throws SystemException {
		return findByIdeaUserId(userId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s ideas where userId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param userId the user ID
	 * @param start the lower bound of the range of c l s ideas
	 * @param end the upper bound of the range of c l s ideas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdea> findByIdeaUserId(long userId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAUSERID;
			finderArgs = new Object[] { userId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEAUSERID;
			finderArgs = new Object[] { userId, start, end, orderByComparator };
		}

		List<CLSIdea> list = (List<CLSIdea>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSIdea clsIdea : list) {
				if ((userId != clsIdea.getUserId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CLSIDEA_WHERE);

			query.append(_FINDER_COLUMN_IDEAUSERID_USERID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSIdeaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				if (!pagination) {
					list = (List<CLSIdea>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSIdea>(list);
				}
				else {
					list = (List<CLSIdea>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s idea in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s idea
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea findByIdeaUserId_First(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSIdeaException, SystemException {
		CLSIdea clsIdea = fetchByIdeaUserId_First(userId, orderByComparator);

		if (clsIdea != null) {
			return clsIdea;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSIdeaException(msg.toString());
	}

	/**
	 * Returns the first c l s idea in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea fetchByIdeaUserId_First(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		List<CLSIdea> list = findByIdeaUserId(userId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s idea in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s idea
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea findByIdeaUserId_Last(long userId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSIdeaException, SystemException {
		CLSIdea clsIdea = fetchByIdeaUserId_Last(userId, orderByComparator);

		if (clsIdea != null) {
			return clsIdea;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("userId=");
		msg.append(userId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSIdeaException(msg.toString());
	}

	/**
	 * Returns the last c l s idea in the ordered set where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea fetchByIdeaUserId_Last(long userId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByIdeaUserId(userId);

		if (count == 0) {
			return null;
		}

		List<CLSIdea> list = findByIdeaUserId(userId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s ideas before and after the current c l s idea in the ordered set where userId = &#63;.
	 *
	 * @param ideaID the primary key of the current c l s idea
	 * @param userId the user ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s idea
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a c l s idea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea[] findByIdeaUserId_PrevAndNext(long ideaID, long userId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSIdeaException, SystemException {
		CLSIdea clsIdea = findByPrimaryKey(ideaID);

		Session session = null;

		try {
			session = openSession();

			CLSIdea[] array = new CLSIdeaImpl[3];

			array[0] = getByIdeaUserId_PrevAndNext(session, clsIdea, userId,
					orderByComparator, true);

			array[1] = clsIdea;

			array[2] = getByIdeaUserId_PrevAndNext(session, clsIdea, userId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSIdea getByIdeaUserId_PrevAndNext(Session session,
		CLSIdea clsIdea, long userId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSIDEA_WHERE);

		query.append(_FINDER_COLUMN_IDEAUSERID_USERID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSIdeaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(userId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsIdea);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSIdea> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s ideas where userId = &#63; from the database.
	 *
	 * @param userId the user ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByIdeaUserId(long userId) throws SystemException {
		for (CLSIdea clsIdea : findByIdeaUserId(userId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(clsIdea);
		}
	}

	/**
	 * Returns the number of c l s ideas where userId = &#63;.
	 *
	 * @param userId the user ID
	 * @return the number of matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByIdeaUserId(long userId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_IDEAUSERID;

		Object[] finderArgs = new Object[] { userId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLSIDEA_WHERE);

			query.append(_FINDER_COLUMN_IDEAUSERID_USERID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(userId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_IDEAUSERID_USERID_2 = "clsIdea.userId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEACHALLENGEID =
		new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, CLSIdeaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByIdeaChallengeId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEACHALLENGEID =
		new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, CLSIdeaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByIdeaChallengeId",
			new String[] { Long.class.getName() },
			CLSIdeaModelImpl.CHALLENGEID_COLUMN_BITMASK |
			CLSIdeaModelImpl.DATEADDED_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_IDEACHALLENGEID = new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByIdeaChallengeId", new String[] { Long.class.getName() });

	/**
	 * Returns all the c l s ideas where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @return the matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdea> findByIdeaChallengeId(long challengeId)
		throws SystemException {
		return findByIdeaChallengeId(challengeId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s ideas where challengeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param challengeId the challenge ID
	 * @param start the lower bound of the range of c l s ideas
	 * @param end the upper bound of the range of c l s ideas (not inclusive)
	 * @return the range of matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdea> findByIdeaChallengeId(long challengeId, int start,
		int end) throws SystemException {
		return findByIdeaChallengeId(challengeId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s ideas where challengeId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param challengeId the challenge ID
	 * @param start the lower bound of the range of c l s ideas
	 * @param end the upper bound of the range of c l s ideas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdea> findByIdeaChallengeId(long challengeId, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEACHALLENGEID;
			finderArgs = new Object[] { challengeId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEACHALLENGEID;
			finderArgs = new Object[] { challengeId, start, end, orderByComparator };
		}

		List<CLSIdea> list = (List<CLSIdea>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSIdea clsIdea : list) {
				if ((challengeId != clsIdea.getChallengeId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CLSIDEA_WHERE);

			query.append(_FINDER_COLUMN_IDEACHALLENGEID_CHALLENGEID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSIdeaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(challengeId);

				if (!pagination) {
					list = (List<CLSIdea>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSIdea>(list);
				}
				else {
					list = (List<CLSIdea>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s idea in the ordered set where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s idea
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea findByIdeaChallengeId_First(long challengeId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSIdeaException, SystemException {
		CLSIdea clsIdea = fetchByIdeaChallengeId_First(challengeId,
				orderByComparator);

		if (clsIdea != null) {
			return clsIdea;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("challengeId=");
		msg.append(challengeId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSIdeaException(msg.toString());
	}

	/**
	 * Returns the first c l s idea in the ordered set where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea fetchByIdeaChallengeId_First(long challengeId,
		OrderByComparator orderByComparator) throws SystemException {
		List<CLSIdea> list = findByIdeaChallengeId(challengeId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s idea in the ordered set where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s idea
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea findByIdeaChallengeId_Last(long challengeId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSIdeaException, SystemException {
		CLSIdea clsIdea = fetchByIdeaChallengeId_Last(challengeId,
				orderByComparator);

		if (clsIdea != null) {
			return clsIdea;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("challengeId=");
		msg.append(challengeId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSIdeaException(msg.toString());
	}

	/**
	 * Returns the last c l s idea in the ordered set where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea fetchByIdeaChallengeId_Last(long challengeId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByIdeaChallengeId(challengeId);

		if (count == 0) {
			return null;
		}

		List<CLSIdea> list = findByIdeaChallengeId(challengeId, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s ideas before and after the current c l s idea in the ordered set where challengeId = &#63;.
	 *
	 * @param ideaID the primary key of the current c l s idea
	 * @param challengeId the challenge ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s idea
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a c l s idea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea[] findByIdeaChallengeId_PrevAndNext(long ideaID,
		long challengeId, OrderByComparator orderByComparator)
		throws NoSuchCLSIdeaException, SystemException {
		CLSIdea clsIdea = findByPrimaryKey(ideaID);

		Session session = null;

		try {
			session = openSession();

			CLSIdea[] array = new CLSIdeaImpl[3];

			array[0] = getByIdeaChallengeId_PrevAndNext(session, clsIdea,
					challengeId, orderByComparator, true);

			array[1] = clsIdea;

			array[2] = getByIdeaChallengeId_PrevAndNext(session, clsIdea,
					challengeId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSIdea getByIdeaChallengeId_PrevAndNext(Session session,
		CLSIdea clsIdea, long challengeId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSIDEA_WHERE);

		query.append(_FINDER_COLUMN_IDEACHALLENGEID_CHALLENGEID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSIdeaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(challengeId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsIdea);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSIdea> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s ideas where challengeId = &#63; from the database.
	 *
	 * @param challengeId the challenge ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByIdeaChallengeId(long challengeId)
		throws SystemException {
		for (CLSIdea clsIdea : findByIdeaChallengeId(challengeId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(clsIdea);
		}
	}

	/**
	 * Returns the number of c l s ideas where challengeId = &#63;.
	 *
	 * @param challengeId the challenge ID
	 * @return the number of matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByIdeaChallengeId(long challengeId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_IDEACHALLENGEID;

		Object[] finderArgs = new Object[] { challengeId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLSIDEA_WHERE);

			query.append(_FINDER_COLUMN_IDEACHALLENGEID_CHALLENGEID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(challengeId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_IDEACHALLENGEID_CHALLENGEID_2 = "clsIdea.challengeId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEANEEDID =
		new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, CLSIdeaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByIdeaNeedId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEANEEDID =
		new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, CLSIdeaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByIdeaNeedId",
			new String[] { Long.class.getName() },
			CLSIdeaModelImpl.NEEDID_COLUMN_BITMASK |
			CLSIdeaModelImpl.DATEADDED_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_IDEANEEDID = new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByIdeaNeedId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the c l s ideas where needId = &#63;.
	 *
	 * @param needId the need ID
	 * @return the matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdea> findByIdeaNeedId(long needId)
		throws SystemException {
		return findByIdeaNeedId(needId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the c l s ideas where needId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param needId the need ID
	 * @param start the lower bound of the range of c l s ideas
	 * @param end the upper bound of the range of c l s ideas (not inclusive)
	 * @return the range of matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdea> findByIdeaNeedId(long needId, int start, int end)
		throws SystemException {
		return findByIdeaNeedId(needId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s ideas where needId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param needId the need ID
	 * @param start the lower bound of the range of c l s ideas
	 * @param end the upper bound of the range of c l s ideas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdea> findByIdeaNeedId(long needId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEANEEDID;
			finderArgs = new Object[] { needId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEANEEDID;
			finderArgs = new Object[] { needId, start, end, orderByComparator };
		}

		List<CLSIdea> list = (List<CLSIdea>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSIdea clsIdea : list) {
				if ((needId != clsIdea.getNeedId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CLSIDEA_WHERE);

			query.append(_FINDER_COLUMN_IDEANEEDID_NEEDID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSIdeaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(needId);

				if (!pagination) {
					list = (List<CLSIdea>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSIdea>(list);
				}
				else {
					list = (List<CLSIdea>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s idea in the ordered set where needId = &#63;.
	 *
	 * @param needId the need ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s idea
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea findByIdeaNeedId_First(long needId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSIdeaException, SystemException {
		CLSIdea clsIdea = fetchByIdeaNeedId_First(needId, orderByComparator);

		if (clsIdea != null) {
			return clsIdea;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("needId=");
		msg.append(needId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSIdeaException(msg.toString());
	}

	/**
	 * Returns the first c l s idea in the ordered set where needId = &#63;.
	 *
	 * @param needId the need ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea fetchByIdeaNeedId_First(long needId,
		OrderByComparator orderByComparator) throws SystemException {
		List<CLSIdea> list = findByIdeaNeedId(needId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s idea in the ordered set where needId = &#63;.
	 *
	 * @param needId the need ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s idea
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea findByIdeaNeedId_Last(long needId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSIdeaException, SystemException {
		CLSIdea clsIdea = fetchByIdeaNeedId_Last(needId, orderByComparator);

		if (clsIdea != null) {
			return clsIdea;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("needId=");
		msg.append(needId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSIdeaException(msg.toString());
	}

	/**
	 * Returns the last c l s idea in the ordered set where needId = &#63;.
	 *
	 * @param needId the need ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea fetchByIdeaNeedId_Last(long needId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByIdeaNeedId(needId);

		if (count == 0) {
			return null;
		}

		List<CLSIdea> list = findByIdeaNeedId(needId, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s ideas before and after the current c l s idea in the ordered set where needId = &#63;.
	 *
	 * @param ideaID the primary key of the current c l s idea
	 * @param needId the need ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s idea
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a c l s idea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea[] findByIdeaNeedId_PrevAndNext(long ideaID, long needId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSIdeaException, SystemException {
		CLSIdea clsIdea = findByPrimaryKey(ideaID);

		Session session = null;

		try {
			session = openSession();

			CLSIdea[] array = new CLSIdeaImpl[3];

			array[0] = getByIdeaNeedId_PrevAndNext(session, clsIdea, needId,
					orderByComparator, true);

			array[1] = clsIdea;

			array[2] = getByIdeaNeedId_PrevAndNext(session, clsIdea, needId,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSIdea getByIdeaNeedId_PrevAndNext(Session session,
		CLSIdea clsIdea, long needId, OrderByComparator orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSIDEA_WHERE);

		query.append(_FINDER_COLUMN_IDEANEEDID_NEEDID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSIdeaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(needId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsIdea);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSIdea> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s ideas where needId = &#63; from the database.
	 *
	 * @param needId the need ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByIdeaNeedId(long needId) throws SystemException {
		for (CLSIdea clsIdea : findByIdeaNeedId(needId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(clsIdea);
		}
	}

	/**
	 * Returns the number of c l s ideas where needId = &#63;.
	 *
	 * @param needId the need ID
	 * @return the number of matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByIdeaNeedId(long needId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_IDEANEEDID;

		Object[] finderArgs = new Object[] { needId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLSIDEA_WHERE);

			query.append(_FINDER_COLUMN_IDEANEEDID_NEEDID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(needId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_IDEANEEDID_NEEDID_2 = "clsIdea.needId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEAMUNICIPALITYID =
		new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, CLSIdeaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByIdeaMunicipalityId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAMUNICIPALITYID =
		new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, CLSIdeaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByIdeaMunicipalityId", new String[] { Long.class.getName() },
			CLSIdeaModelImpl.MUNICIPALITYID_COLUMN_BITMASK |
			CLSIdeaModelImpl.DATEADDED_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_IDEAMUNICIPALITYID = new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByIdeaMunicipalityId", new String[] { Long.class.getName() });

	/**
	 * Returns all the c l s ideas where municipalityId = &#63;.
	 *
	 * @param municipalityId the municipality ID
	 * @return the matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdea> findByIdeaMunicipalityId(long municipalityId)
		throws SystemException {
		return findByIdeaMunicipalityId(municipalityId, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s ideas where municipalityId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param municipalityId the municipality ID
	 * @param start the lower bound of the range of c l s ideas
	 * @param end the upper bound of the range of c l s ideas (not inclusive)
	 * @return the range of matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdea> findByIdeaMunicipalityId(long municipalityId,
		int start, int end) throws SystemException {
		return findByIdeaMunicipalityId(municipalityId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s ideas where municipalityId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param municipalityId the municipality ID
	 * @param start the lower bound of the range of c l s ideas
	 * @param end the upper bound of the range of c l s ideas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdea> findByIdeaMunicipalityId(long municipalityId,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAMUNICIPALITYID;
			finderArgs = new Object[] { municipalityId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEAMUNICIPALITYID;
			finderArgs = new Object[] {
					municipalityId,
					
					start, end, orderByComparator
				};
		}

		List<CLSIdea> list = (List<CLSIdea>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSIdea clsIdea : list) {
				if ((municipalityId != clsIdea.getMunicipalityId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CLSIDEA_WHERE);

			query.append(_FINDER_COLUMN_IDEAMUNICIPALITYID_MUNICIPALITYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSIdeaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(municipalityId);

				if (!pagination) {
					list = (List<CLSIdea>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSIdea>(list);
				}
				else {
					list = (List<CLSIdea>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s idea in the ordered set where municipalityId = &#63;.
	 *
	 * @param municipalityId the municipality ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s idea
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea findByIdeaMunicipalityId_First(long municipalityId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSIdeaException, SystemException {
		CLSIdea clsIdea = fetchByIdeaMunicipalityId_First(municipalityId,
				orderByComparator);

		if (clsIdea != null) {
			return clsIdea;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("municipalityId=");
		msg.append(municipalityId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSIdeaException(msg.toString());
	}

	/**
	 * Returns the first c l s idea in the ordered set where municipalityId = &#63;.
	 *
	 * @param municipalityId the municipality ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea fetchByIdeaMunicipalityId_First(long municipalityId,
		OrderByComparator orderByComparator) throws SystemException {
		List<CLSIdea> list = findByIdeaMunicipalityId(municipalityId, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s idea in the ordered set where municipalityId = &#63;.
	 *
	 * @param municipalityId the municipality ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s idea
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea findByIdeaMunicipalityId_Last(long municipalityId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSIdeaException, SystemException {
		CLSIdea clsIdea = fetchByIdeaMunicipalityId_Last(municipalityId,
				orderByComparator);

		if (clsIdea != null) {
			return clsIdea;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("municipalityId=");
		msg.append(municipalityId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSIdeaException(msg.toString());
	}

	/**
	 * Returns the last c l s idea in the ordered set where municipalityId = &#63;.
	 *
	 * @param municipalityId the municipality ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea fetchByIdeaMunicipalityId_Last(long municipalityId,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByIdeaMunicipalityId(municipalityId);

		if (count == 0) {
			return null;
		}

		List<CLSIdea> list = findByIdeaMunicipalityId(municipalityId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s ideas before and after the current c l s idea in the ordered set where municipalityId = &#63;.
	 *
	 * @param ideaID the primary key of the current c l s idea
	 * @param municipalityId the municipality ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s idea
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a c l s idea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea[] findByIdeaMunicipalityId_PrevAndNext(long ideaID,
		long municipalityId, OrderByComparator orderByComparator)
		throws NoSuchCLSIdeaException, SystemException {
		CLSIdea clsIdea = findByPrimaryKey(ideaID);

		Session session = null;

		try {
			session = openSession();

			CLSIdea[] array = new CLSIdeaImpl[3];

			array[0] = getByIdeaMunicipalityId_PrevAndNext(session, clsIdea,
					municipalityId, orderByComparator, true);

			array[1] = clsIdea;

			array[2] = getByIdeaMunicipalityId_PrevAndNext(session, clsIdea,
					municipalityId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSIdea getByIdeaMunicipalityId_PrevAndNext(Session session,
		CLSIdea clsIdea, long municipalityId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSIDEA_WHERE);

		query.append(_FINDER_COLUMN_IDEAMUNICIPALITYID_MUNICIPALITYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSIdeaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(municipalityId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsIdea);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSIdea> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s ideas where municipalityId = &#63; from the database.
	 *
	 * @param municipalityId the municipality ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByIdeaMunicipalityId(long municipalityId)
		throws SystemException {
		for (CLSIdea clsIdea : findByIdeaMunicipalityId(municipalityId,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(clsIdea);
		}
	}

	/**
	 * Returns the number of c l s ideas where municipalityId = &#63;.
	 *
	 * @param municipalityId the municipality ID
	 * @return the number of matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByIdeaMunicipalityId(long municipalityId)
		throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_IDEAMUNICIPALITYID;

		Object[] finderArgs = new Object[] { municipalityId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLSIDEA_WHERE);

			query.append(_FINDER_COLUMN_IDEAMUNICIPALITYID_MUNICIPALITYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(municipalityId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_IDEAMUNICIPALITYID_MUNICIPALITYID_2 =
		"clsIdea.municipalityId = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEAMUNICIPALITYORGANIZATIONID =
		new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, CLSIdeaImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByIdeaMunicipalityOrganizationId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAMUNICIPALITYORGANIZATIONID =
		new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, CLSIdeaImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByIdeaMunicipalityOrganizationId",
			new String[] { Long.class.getName() },
			CLSIdeaModelImpl.MUNICIPALITYORGANIZATIONID_COLUMN_BITMASK |
			CLSIdeaModelImpl.DATEADDED_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_IDEAMUNICIPALITYORGANIZATIONID =
		new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByIdeaMunicipalityOrganizationId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the c l s ideas where municipalityOrganizationId = &#63;.
	 *
	 * @param municipalityOrganizationId the municipality organization ID
	 * @return the matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdea> findByIdeaMunicipalityOrganizationId(
		long municipalityOrganizationId) throws SystemException {
		return findByIdeaMunicipalityOrganizationId(municipalityOrganizationId,
			QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s ideas where municipalityOrganizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param municipalityOrganizationId the municipality organization ID
	 * @param start the lower bound of the range of c l s ideas
	 * @param end the upper bound of the range of c l s ideas (not inclusive)
	 * @return the range of matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdea> findByIdeaMunicipalityOrganizationId(
		long municipalityOrganizationId, int start, int end)
		throws SystemException {
		return findByIdeaMunicipalityOrganizationId(municipalityOrganizationId,
			start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s ideas where municipalityOrganizationId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param municipalityOrganizationId the municipality organization ID
	 * @param start the lower bound of the range of c l s ideas
	 * @param end the upper bound of the range of c l s ideas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdea> findByIdeaMunicipalityOrganizationId(
		long municipalityOrganizationId, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAMUNICIPALITYORGANIZATIONID;
			finderArgs = new Object[] { municipalityOrganizationId };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_IDEAMUNICIPALITYORGANIZATIONID;
			finderArgs = new Object[] {
					municipalityOrganizationId,
					
					start, end, orderByComparator
				};
		}

		List<CLSIdea> list = (List<CLSIdea>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (CLSIdea clsIdea : list) {
				if ((municipalityOrganizationId != clsIdea.getMunicipalityOrganizationId())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CLSIDEA_WHERE);

			query.append(_FINDER_COLUMN_IDEAMUNICIPALITYORGANIZATIONID_MUNICIPALITYORGANIZATIONID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(CLSIdeaModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(municipalityOrganizationId);

				if (!pagination) {
					list = (List<CLSIdea>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSIdea>(list);
				}
				else {
					list = (List<CLSIdea>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first c l s idea in the ordered set where municipalityOrganizationId = &#63;.
	 *
	 * @param municipalityOrganizationId the municipality organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s idea
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea findByIdeaMunicipalityOrganizationId_First(
		long municipalityOrganizationId, OrderByComparator orderByComparator)
		throws NoSuchCLSIdeaException, SystemException {
		CLSIdea clsIdea = fetchByIdeaMunicipalityOrganizationId_First(municipalityOrganizationId,
				orderByComparator);

		if (clsIdea != null) {
			return clsIdea;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("municipalityOrganizationId=");
		msg.append(municipalityOrganizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSIdeaException(msg.toString());
	}

	/**
	 * Returns the first c l s idea in the ordered set where municipalityOrganizationId = &#63;.
	 *
	 * @param municipalityOrganizationId the municipality organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea fetchByIdeaMunicipalityOrganizationId_First(
		long municipalityOrganizationId, OrderByComparator orderByComparator)
		throws SystemException {
		List<CLSIdea> list = findByIdeaMunicipalityOrganizationId(municipalityOrganizationId,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last c l s idea in the ordered set where municipalityOrganizationId = &#63;.
	 *
	 * @param municipalityOrganizationId the municipality organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s idea
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea findByIdeaMunicipalityOrganizationId_Last(
		long municipalityOrganizationId, OrderByComparator orderByComparator)
		throws NoSuchCLSIdeaException, SystemException {
		CLSIdea clsIdea = fetchByIdeaMunicipalityOrganizationId_Last(municipalityOrganizationId,
				orderByComparator);

		if (clsIdea != null) {
			return clsIdea;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("municipalityOrganizationId=");
		msg.append(municipalityOrganizationId);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCLSIdeaException(msg.toString());
	}

	/**
	 * Returns the last c l s idea in the ordered set where municipalityOrganizationId = &#63;.
	 *
	 * @param municipalityOrganizationId the municipality organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea fetchByIdeaMunicipalityOrganizationId_Last(
		long municipalityOrganizationId, OrderByComparator orderByComparator)
		throws SystemException {
		int count = countByIdeaMunicipalityOrganizationId(municipalityOrganizationId);

		if (count == 0) {
			return null;
		}

		List<CLSIdea> list = findByIdeaMunicipalityOrganizationId(municipalityOrganizationId,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the c l s ideas before and after the current c l s idea in the ordered set where municipalityOrganizationId = &#63;.
	 *
	 * @param ideaID the primary key of the current c l s idea
	 * @param municipalityOrganizationId the municipality organization ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next c l s idea
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a c l s idea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea[] findByIdeaMunicipalityOrganizationId_PrevAndNext(
		long ideaID, long municipalityOrganizationId,
		OrderByComparator orderByComparator)
		throws NoSuchCLSIdeaException, SystemException {
		CLSIdea clsIdea = findByPrimaryKey(ideaID);

		Session session = null;

		try {
			session = openSession();

			CLSIdea[] array = new CLSIdeaImpl[3];

			array[0] = getByIdeaMunicipalityOrganizationId_PrevAndNext(session,
					clsIdea, municipalityOrganizationId, orderByComparator, true);

			array[1] = clsIdea;

			array[2] = getByIdeaMunicipalityOrganizationId_PrevAndNext(session,
					clsIdea, municipalityOrganizationId, orderByComparator,
					false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected CLSIdea getByIdeaMunicipalityOrganizationId_PrevAndNext(
		Session session, CLSIdea clsIdea, long municipalityOrganizationId,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CLSIDEA_WHERE);

		query.append(_FINDER_COLUMN_IDEAMUNICIPALITYORGANIZATIONID_MUNICIPALITYORGANIZATIONID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(CLSIdeaModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(municipalityOrganizationId);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(clsIdea);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<CLSIdea> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the c l s ideas where municipalityOrganizationId = &#63; from the database.
	 *
	 * @param municipalityOrganizationId the municipality organization ID
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeByIdeaMunicipalityOrganizationId(
		long municipalityOrganizationId) throws SystemException {
		for (CLSIdea clsIdea : findByIdeaMunicipalityOrganizationId(
				municipalityOrganizationId, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(clsIdea);
		}
	}

	/**
	 * Returns the number of c l s ideas where municipalityOrganizationId = &#63;.
	 *
	 * @param municipalityOrganizationId the municipality organization ID
	 * @return the number of matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByIdeaMunicipalityOrganizationId(
		long municipalityOrganizationId) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_IDEAMUNICIPALITYORGANIZATIONID;

		Object[] finderArgs = new Object[] { municipalityOrganizationId };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLSIDEA_WHERE);

			query.append(_FINDER_COLUMN_IDEAMUNICIPALITYORGANIZATIONID_MUNICIPALITYORGANIZATIONID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(municipalityOrganizationId);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_IDEAMUNICIPALITYORGANIZATIONID_MUNICIPALITYORGANIZATIONID_2 =
		"clsIdea.municipalityOrganizationId = ?";
	public static final FinderPath FINDER_PATH_FETCH_BY_IDEAID = new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, CLSIdeaImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByIdeaId",
			new String[] { Long.class.getName() },
			CLSIdeaModelImpl.IDEAID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_IDEAID = new FinderPath(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByIdeaId",
			new String[] { Long.class.getName() });

	/**
	 * Returns the c l s idea where ideaID = &#63; or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException} if it could not be found.
	 *
	 * @param ideaID the idea i d
	 * @return the matching c l s idea
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea findByIdeaId(long ideaID)
		throws NoSuchCLSIdeaException, SystemException {
		CLSIdea clsIdea = fetchByIdeaId(ideaID);

		if (clsIdea == null) {
			StringBundler msg = new StringBundler(4);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("ideaID=");
			msg.append(ideaID);

			msg.append(StringPool.CLOSE_CURLY_BRACE);

			if (_log.isWarnEnabled()) {
				_log.warn(msg.toString());
			}

			throw new NoSuchCLSIdeaException(msg.toString());
		}

		return clsIdea;
	}

	/**
	 * Returns the c l s idea where ideaID = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param ideaID the idea i d
	 * @return the matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea fetchByIdeaId(long ideaID) throws SystemException {
		return fetchByIdeaId(ideaID, true);
	}

	/**
	 * Returns the c l s idea where ideaID = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param ideaID the idea i d
	 * @param retrieveFromCache whether to use the finder cache
	 * @return the matching c l s idea, or <code>null</code> if a matching c l s idea could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea fetchByIdeaId(long ideaID, boolean retrieveFromCache)
		throws SystemException {
		Object[] finderArgs = new Object[] { ideaID };

		Object result = null;

		if (retrieveFromCache) {
			result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_IDEAID,
					finderArgs, this);
		}

		if (result instanceof CLSIdea) {
			CLSIdea clsIdea = (CLSIdea)result;

			if ((ideaID != clsIdea.getIdeaID())) {
				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_SELECT_CLSIDEA_WHERE);

			query.append(_FINDER_COLUMN_IDEAID_IDEAID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaID);

				List<CLSIdea> list = q.list();

				if (list.isEmpty()) {
					FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDEAID,
						finderArgs, list);
				}
				else {
					CLSIdea clsIdea = list.get(0);

					result = clsIdea;

					cacheResult(clsIdea);

					if ((clsIdea.getIdeaID() != ideaID)) {
						FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDEAID,
							finderArgs, clsIdea);
					}
				}
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_IDEAID,
					finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (CLSIdea)result;
		}
	}

	/**
	 * Removes the c l s idea where ideaID = &#63; from the database.
	 *
	 * @param ideaID the idea i d
	 * @return the c l s idea that was removed
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea removeByIdeaId(long ideaID)
		throws NoSuchCLSIdeaException, SystemException {
		CLSIdea clsIdea = findByIdeaId(ideaID);

		return remove(clsIdea);
	}

	/**
	 * Returns the number of c l s ideas where ideaID = &#63;.
	 *
	 * @param ideaID the idea i d
	 * @return the number of matching c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countByIdeaId(long ideaID) throws SystemException {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_IDEAID;

		Object[] finderArgs = new Object[] { ideaID };

		Long count = (Long)FinderCacheUtil.getResult(finderPath, finderArgs,
				this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CLSIDEA_WHERE);

			query.append(_FINDER_COLUMN_IDEAID_IDEAID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(ideaID);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_IDEAID_IDEAID_2 = "clsIdea.ideaID = ?";

	public CLSIdeaPersistenceImpl() {
		setModelClass(CLSIdea.class);
	}

	/**
	 * Caches the c l s idea in the entity cache if it is enabled.
	 *
	 * @param clsIdea the c l s idea
	 */
	@Override
	public void cacheResult(CLSIdea clsIdea) {
		EntityCacheUtil.putResult(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaImpl.class, clsIdea.getPrimaryKey(), clsIdea);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G,
			new Object[] { clsIdea.getUuid(), clsIdea.getGroupId() }, clsIdea);

		FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDEAID,
			new Object[] { clsIdea.getIdeaID() }, clsIdea);

		clsIdea.resetOriginalValues();
	}

	/**
	 * Caches the c l s ideas in the entity cache if it is enabled.
	 *
	 * @param clsIdeas the c l s ideas
	 */
	@Override
	public void cacheResult(List<CLSIdea> clsIdeas) {
		for (CLSIdea clsIdea : clsIdeas) {
			if (EntityCacheUtil.getResult(
						CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
						CLSIdeaImpl.class, clsIdea.getPrimaryKey()) == null) {
				cacheResult(clsIdea);
			}
			else {
				clsIdea.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all c l s ideas.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CLSIdeaImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CLSIdeaImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the c l s idea.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CLSIdea clsIdea) {
		EntityCacheUtil.removeResult(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaImpl.class, clsIdea.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(clsIdea);
	}

	@Override
	public void clearCache(List<CLSIdea> clsIdeas) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CLSIdea clsIdea : clsIdeas) {
			EntityCacheUtil.removeResult(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
				CLSIdeaImpl.class, clsIdea.getPrimaryKey());

			clearUniqueFindersCache(clsIdea);
		}
	}

	protected void cacheUniqueFindersCache(CLSIdea clsIdea) {
		if (clsIdea.isNew()) {
			Object[] args = new Object[] { clsIdea.getUuid(), clsIdea.getGroupId() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_UUID_G, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G, args, clsIdea);

			args = new Object[] { clsIdea.getIdeaID() };

			FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_IDEAID, args,
				Long.valueOf(1));
			FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDEAID, args, clsIdea);
		}
		else {
			CLSIdeaModelImpl clsIdeaModelImpl = (CLSIdeaModelImpl)clsIdea;

			if ((clsIdeaModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsIdea.getUuid(), clsIdea.getGroupId()
					};

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_UUID_G, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_UUID_G, args,
					clsIdea);
			}

			if ((clsIdeaModelImpl.getColumnBitmask() &
					FINDER_PATH_FETCH_BY_IDEAID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { clsIdea.getIdeaID() };

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_IDEAID, args,
					Long.valueOf(1));
				FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDEAID, args,
					clsIdea);
			}
		}
	}

	protected void clearUniqueFindersCache(CLSIdea clsIdea) {
		CLSIdeaModelImpl clsIdeaModelImpl = (CLSIdeaModelImpl)clsIdea;

		Object[] args = new Object[] { clsIdea.getUuid(), clsIdea.getGroupId() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);

		if ((clsIdeaModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_UUID_G.getColumnBitmask()) != 0) {
			args = new Object[] {
					clsIdeaModelImpl.getOriginalUuid(),
					clsIdeaModelImpl.getOriginalGroupId()
				};

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID_G, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_UUID_G, args);
		}

		args = new Object[] { clsIdea.getIdeaID() };

		FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEAID, args);
		FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_IDEAID, args);

		if ((clsIdeaModelImpl.getColumnBitmask() &
				FINDER_PATH_FETCH_BY_IDEAID.getColumnBitmask()) != 0) {
			args = new Object[] { clsIdeaModelImpl.getOriginalIdeaID() };

			FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEAID, args);
			FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_IDEAID, args);
		}
	}

	/**
	 * Creates a new c l s idea with the primary key. Does not add the c l s idea to the database.
	 *
	 * @param ideaID the primary key for the new c l s idea
	 * @return the new c l s idea
	 */
	@Override
	public CLSIdea create(long ideaID) {
		CLSIdea clsIdea = new CLSIdeaImpl();

		clsIdea.setNew(true);
		clsIdea.setPrimaryKey(ideaID);

		String uuid = PortalUUIDUtil.generate();

		clsIdea.setUuid(uuid);

		return clsIdea;
	}

	/**
	 * Removes the c l s idea with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param ideaID the primary key of the c l s idea
	 * @return the c l s idea that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a c l s idea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea remove(long ideaID)
		throws NoSuchCLSIdeaException, SystemException {
		return remove((Serializable)ideaID);
	}

	/**
	 * Removes the c l s idea with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the c l s idea
	 * @return the c l s idea that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a c l s idea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea remove(Serializable primaryKey)
		throws NoSuchCLSIdeaException, SystemException {
		Session session = null;

		try {
			session = openSession();

			CLSIdea clsIdea = (CLSIdea)session.get(CLSIdeaImpl.class, primaryKey);

			if (clsIdea == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCLSIdeaException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(clsIdea);
		}
		catch (NoSuchCLSIdeaException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CLSIdea removeImpl(CLSIdea clsIdea) throws SystemException {
		clsIdea = toUnwrappedModel(clsIdea);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(clsIdea)) {
				clsIdea = (CLSIdea)session.get(CLSIdeaImpl.class,
						clsIdea.getPrimaryKeyObj());
			}

			if (clsIdea != null) {
				session.delete(clsIdea);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (clsIdea != null) {
			clearCache(clsIdea);
		}

		return clsIdea;
	}

	@Override
	public CLSIdea updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea clsIdea)
		throws SystemException {
		clsIdea = toUnwrappedModel(clsIdea);

		boolean isNew = clsIdea.isNew();

		CLSIdeaModelImpl clsIdeaModelImpl = (CLSIdeaModelImpl)clsIdea;

		if (Validator.isNull(clsIdea.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			clsIdea.setUuid(uuid);
		}

		Session session = null;

		try {
			session = openSession();

			if (clsIdea.isNew()) {
				session.save(clsIdea);

				clsIdea.setNew(false);
			}
			else {
				session.merge(clsIdea);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !CLSIdeaModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((clsIdeaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] { clsIdeaModelImpl.getOriginalUuid() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);

				args = new Object[] { clsIdeaModelImpl.getUuid() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID,
					args);
			}

			if ((clsIdeaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsIdeaModelImpl.getOriginalUuid(),
						clsIdeaModelImpl.getOriginalCompanyId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);

				args = new Object[] {
						clsIdeaModelImpl.getUuid(),
						clsIdeaModelImpl.getCompanyId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_UUID_C, args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_UUID_C,
					args);
			}

			if ((clsIdeaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEATITLE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsIdeaModelImpl.getOriginalIdeaTitle()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEATITLE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEATITLE,
					args);

				args = new Object[] { clsIdeaModelImpl.getIdeaTitle() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEATITLE,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEATITLE,
					args);
			}

			if ((clsIdeaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAUSERID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsIdeaModelImpl.getOriginalUserId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEAUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAUSERID,
					args);

				args = new Object[] { clsIdeaModelImpl.getUserId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEAUSERID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAUSERID,
					args);
			}

			if ((clsIdeaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEACHALLENGEID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsIdeaModelImpl.getOriginalChallengeId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEACHALLENGEID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEACHALLENGEID,
					args);

				args = new Object[] { clsIdeaModelImpl.getChallengeId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEACHALLENGEID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEACHALLENGEID,
					args);
			}

			if ((clsIdeaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEANEEDID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsIdeaModelImpl.getOriginalNeedId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEANEEDID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEANEEDID,
					args);

				args = new Object[] { clsIdeaModelImpl.getNeedId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEANEEDID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEANEEDID,
					args);
			}

			if ((clsIdeaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAMUNICIPALITYID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsIdeaModelImpl.getOriginalMunicipalityId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEAMUNICIPALITYID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAMUNICIPALITYID,
					args);

				args = new Object[] { clsIdeaModelImpl.getMunicipalityId() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEAMUNICIPALITYID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAMUNICIPALITYID,
					args);
			}

			if ((clsIdeaModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAMUNICIPALITYORGANIZATIONID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						clsIdeaModelImpl.getOriginalMunicipalityOrganizationId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEAMUNICIPALITYORGANIZATIONID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAMUNICIPALITYORGANIZATIONID,
					args);

				args = new Object[] {
						clsIdeaModelImpl.getMunicipalityOrganizationId()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDEAMUNICIPALITYORGANIZATIONID,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_IDEAMUNICIPALITYORGANIZATIONID,
					args);
			}
		}

		EntityCacheUtil.putResult(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
			CLSIdeaImpl.class, clsIdea.getPrimaryKey(), clsIdea);

		clearUniqueFindersCache(clsIdea);
		cacheUniqueFindersCache(clsIdea);

		return clsIdea;
	}

	protected CLSIdea toUnwrappedModel(CLSIdea clsIdea) {
		if (clsIdea instanceof CLSIdeaImpl) {
			return clsIdea;
		}

		CLSIdeaImpl clsIdeaImpl = new CLSIdeaImpl();

		clsIdeaImpl.setNew(clsIdea.isNew());
		clsIdeaImpl.setPrimaryKey(clsIdea.getPrimaryKey());

		clsIdeaImpl.setUuid(clsIdea.getUuid());
		clsIdeaImpl.setIdeaID(clsIdea.getIdeaID());
		clsIdeaImpl.setIdeaTitle(clsIdea.getIdeaTitle());
		clsIdeaImpl.setIdeaDescription(clsIdea.getIdeaDescription());
		clsIdeaImpl.setDateAdded(clsIdea.getDateAdded());
		clsIdeaImpl.setCompanyId(clsIdea.getCompanyId());
		clsIdeaImpl.setGroupId(clsIdea.getGroupId());
		clsIdeaImpl.setUserId(clsIdea.getUserId());
		clsIdeaImpl.setMunicipalityId(clsIdea.getMunicipalityId());
		clsIdeaImpl.setMunicipalityOrganizationId(clsIdea.getMunicipalityOrganizationId());
		clsIdeaImpl.setChallengeId(clsIdea.getChallengeId());
		clsIdeaImpl.setTakenUp(clsIdea.isTakenUp());
		clsIdeaImpl.setNeedId(clsIdea.getNeedId());
		clsIdeaImpl.setDmFolderName(clsIdea.getDmFolderName());
		clsIdeaImpl.setIdFolder(clsIdea.getIdFolder());
		clsIdeaImpl.setRepresentativeImgUrl(clsIdea.getRepresentativeImgUrl());
		clsIdeaImpl.setIsNeed(clsIdea.isIsNeed());
		clsIdeaImpl.setStatus(clsIdea.getStatus());
		clsIdeaImpl.setStatusByUserId(clsIdea.getStatusByUserId());
		clsIdeaImpl.setStatusByUserName(clsIdea.getStatusByUserName());
		clsIdeaImpl.setStatusDate(clsIdea.getStatusDate());
		clsIdeaImpl.setChatGroup(clsIdea.getChatGroup());
		clsIdeaImpl.setIdeaHashTag(clsIdea.getIdeaHashTag());
		clsIdeaImpl.setIdeaStatus(clsIdea.getIdeaStatus());
		clsIdeaImpl.setLanguage(clsIdea.getLanguage());
		clsIdeaImpl.setFinalMotivation(clsIdea.getFinalMotivation());
		clsIdeaImpl.setEvaluationPassed(clsIdea.isEvaluationPassed());
		clsIdeaImpl.setCityName(clsIdea.getCityName());

		return clsIdeaImpl;
	}

	/**
	 * Returns the c l s idea with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the c l s idea
	 * @return the c l s idea
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a c l s idea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCLSIdeaException, SystemException {
		CLSIdea clsIdea = fetchByPrimaryKey(primaryKey);

		if (clsIdea == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCLSIdeaException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return clsIdea;
	}

	/**
	 * Returns the c l s idea with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException} if it could not be found.
	 *
	 * @param ideaID the primary key of the c l s idea
	 * @return the c l s idea
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException if a c l s idea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea findByPrimaryKey(long ideaID)
		throws NoSuchCLSIdeaException, SystemException {
		return findByPrimaryKey((Serializable)ideaID);
	}

	/**
	 * Returns the c l s idea with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the c l s idea
	 * @return the c l s idea, or <code>null</code> if a c l s idea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		CLSIdea clsIdea = (CLSIdea)EntityCacheUtil.getResult(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
				CLSIdeaImpl.class, primaryKey);

		if (clsIdea == _nullCLSIdea) {
			return null;
		}

		if (clsIdea == null) {
			Session session = null;

			try {
				session = openSession();

				clsIdea = (CLSIdea)session.get(CLSIdeaImpl.class, primaryKey);

				if (clsIdea != null) {
					cacheResult(clsIdea);
				}
				else {
					EntityCacheUtil.putResult(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
						CLSIdeaImpl.class, primaryKey, _nullCLSIdea);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(CLSIdeaModelImpl.ENTITY_CACHE_ENABLED,
					CLSIdeaImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return clsIdea;
	}

	/**
	 * Returns the c l s idea with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param ideaID the primary key of the c l s idea
	 * @return the c l s idea, or <code>null</code> if a c l s idea with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSIdea fetchByPrimaryKey(long ideaID) throws SystemException {
		return fetchByPrimaryKey((Serializable)ideaID);
	}

	/**
	 * Returns all the c l s ideas.
	 *
	 * @return the c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdea> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s ideas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of c l s ideas
	 * @param end the upper bound of the range of c l s ideas (not inclusive)
	 * @return the range of c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdea> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s ideas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSIdeaModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of c l s ideas
	 * @param end the upper bound of the range of c l s ideas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSIdea> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CLSIdea> list = (List<CLSIdea>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CLSIDEA);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CLSIDEA;

				if (pagination) {
					sql = sql.concat(CLSIdeaModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<CLSIdea>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSIdea>(list);
				}
				else {
					list = (List<CLSIdea>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the c l s ideas from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (CLSIdea clsIdea : findAll()) {
			remove(clsIdea);
		}
	}

	/**
	 * Returns the number of c l s ideas.
	 *
	 * @return the number of c l s ideas
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CLSIDEA);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the c l s idea persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<CLSIdea>> listenersList = new ArrayList<ModelListener<CLSIdea>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<CLSIdea>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CLSIdeaImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_CLSIDEA = "SELECT clsIdea FROM CLSIdea clsIdea";
	private static final String _SQL_SELECT_CLSIDEA_WHERE = "SELECT clsIdea FROM CLSIdea clsIdea WHERE ";
	private static final String _SQL_COUNT_CLSIDEA = "SELECT COUNT(clsIdea) FROM CLSIdea clsIdea";
	private static final String _SQL_COUNT_CLSIDEA_WHERE = "SELECT COUNT(clsIdea) FROM CLSIdea clsIdea WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "clsIdea.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CLSIdea exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No CLSIdea exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CLSIdeaPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"uuid"
			});
	private static CLSIdea _nullCLSIdea = new CLSIdeaImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<CLSIdea> toCacheModel() {
				return _nullCLSIdeaCacheModel;
			}
		};

	private static CacheModel<CLSIdea> _nullCLSIdeaCacheModel = new CacheModel<CLSIdea>() {
			@Override
			public CLSIdea toEntityModel() {
				return _nullCLSIdea;
			}
		};
}