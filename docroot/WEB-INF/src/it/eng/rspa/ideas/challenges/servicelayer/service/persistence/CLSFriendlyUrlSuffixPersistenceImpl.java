/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFriendlyUrlSuffixException;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSFriendlyUrlSuffix;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFriendlyUrlSuffixImpl;
import it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFriendlyUrlSuffixModelImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the c l s friendly url suffix service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSFriendlyUrlSuffixPersistence
 * @see CLSFriendlyUrlSuffixUtil
 * @generated
 */
public class CLSFriendlyUrlSuffixPersistenceImpl extends BasePersistenceImpl<CLSFriendlyUrlSuffix>
	implements CLSFriendlyUrlSuffixPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CLSFriendlyUrlSuffixUtil} to access the c l s friendly url suffix persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CLSFriendlyUrlSuffixImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CLSFriendlyUrlSuffixModelImpl.ENTITY_CACHE_ENABLED,
			CLSFriendlyUrlSuffixModelImpl.FINDER_CACHE_ENABLED,
			CLSFriendlyUrlSuffixImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CLSFriendlyUrlSuffixModelImpl.ENTITY_CACHE_ENABLED,
			CLSFriendlyUrlSuffixModelImpl.FINDER_CACHE_ENABLED,
			CLSFriendlyUrlSuffixImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CLSFriendlyUrlSuffixModelImpl.ENTITY_CACHE_ENABLED,
			CLSFriendlyUrlSuffixModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public CLSFriendlyUrlSuffixPersistenceImpl() {
		setModelClass(CLSFriendlyUrlSuffix.class);
	}

	/**
	 * Caches the c l s friendly url suffix in the entity cache if it is enabled.
	 *
	 * @param clsFriendlyUrlSuffix the c l s friendly url suffix
	 */
	@Override
	public void cacheResult(CLSFriendlyUrlSuffix clsFriendlyUrlSuffix) {
		EntityCacheUtil.putResult(CLSFriendlyUrlSuffixModelImpl.ENTITY_CACHE_ENABLED,
			CLSFriendlyUrlSuffixImpl.class,
			clsFriendlyUrlSuffix.getPrimaryKey(), clsFriendlyUrlSuffix);

		clsFriendlyUrlSuffix.resetOriginalValues();
	}

	/**
	 * Caches the c l s friendly url suffixs in the entity cache if it is enabled.
	 *
	 * @param clsFriendlyUrlSuffixs the c l s friendly url suffixs
	 */
	@Override
	public void cacheResult(List<CLSFriendlyUrlSuffix> clsFriendlyUrlSuffixs) {
		for (CLSFriendlyUrlSuffix clsFriendlyUrlSuffix : clsFriendlyUrlSuffixs) {
			if (EntityCacheUtil.getResult(
						CLSFriendlyUrlSuffixModelImpl.ENTITY_CACHE_ENABLED,
						CLSFriendlyUrlSuffixImpl.class,
						clsFriendlyUrlSuffix.getPrimaryKey()) == null) {
				cacheResult(clsFriendlyUrlSuffix);
			}
			else {
				clsFriendlyUrlSuffix.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all c l s friendly url suffixs.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CLSFriendlyUrlSuffixImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CLSFriendlyUrlSuffixImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the c l s friendly url suffix.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(CLSFriendlyUrlSuffix clsFriendlyUrlSuffix) {
		EntityCacheUtil.removeResult(CLSFriendlyUrlSuffixModelImpl.ENTITY_CACHE_ENABLED,
			CLSFriendlyUrlSuffixImpl.class, clsFriendlyUrlSuffix.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<CLSFriendlyUrlSuffix> clsFriendlyUrlSuffixs) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (CLSFriendlyUrlSuffix clsFriendlyUrlSuffix : clsFriendlyUrlSuffixs) {
			EntityCacheUtil.removeResult(CLSFriendlyUrlSuffixModelImpl.ENTITY_CACHE_ENABLED,
				CLSFriendlyUrlSuffixImpl.class,
				clsFriendlyUrlSuffix.getPrimaryKey());
		}
	}

	/**
	 * Creates a new c l s friendly url suffix with the primary key. Does not add the c l s friendly url suffix to the database.
	 *
	 * @param friendlyUrlSuffixID the primary key for the new c l s friendly url suffix
	 * @return the new c l s friendly url suffix
	 */
	@Override
	public CLSFriendlyUrlSuffix create(long friendlyUrlSuffixID) {
		CLSFriendlyUrlSuffix clsFriendlyUrlSuffix = new CLSFriendlyUrlSuffixImpl();

		clsFriendlyUrlSuffix.setNew(true);
		clsFriendlyUrlSuffix.setPrimaryKey(friendlyUrlSuffixID);

		return clsFriendlyUrlSuffix;
	}

	/**
	 * Removes the c l s friendly url suffix with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param friendlyUrlSuffixID the primary key of the c l s friendly url suffix
	 * @return the c l s friendly url suffix that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFriendlyUrlSuffixException if a c l s friendly url suffix with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFriendlyUrlSuffix remove(long friendlyUrlSuffixID)
		throws NoSuchCLSFriendlyUrlSuffixException, SystemException {
		return remove((Serializable)friendlyUrlSuffixID);
	}

	/**
	 * Removes the c l s friendly url suffix with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the c l s friendly url suffix
	 * @return the c l s friendly url suffix that was removed
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFriendlyUrlSuffixException if a c l s friendly url suffix with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFriendlyUrlSuffix remove(Serializable primaryKey)
		throws NoSuchCLSFriendlyUrlSuffixException, SystemException {
		Session session = null;

		try {
			session = openSession();

			CLSFriendlyUrlSuffix clsFriendlyUrlSuffix = (CLSFriendlyUrlSuffix)session.get(CLSFriendlyUrlSuffixImpl.class,
					primaryKey);

			if (clsFriendlyUrlSuffix == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCLSFriendlyUrlSuffixException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(clsFriendlyUrlSuffix);
		}
		catch (NoSuchCLSFriendlyUrlSuffixException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected CLSFriendlyUrlSuffix removeImpl(
		CLSFriendlyUrlSuffix clsFriendlyUrlSuffix) throws SystemException {
		clsFriendlyUrlSuffix = toUnwrappedModel(clsFriendlyUrlSuffix);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(clsFriendlyUrlSuffix)) {
				clsFriendlyUrlSuffix = (CLSFriendlyUrlSuffix)session.get(CLSFriendlyUrlSuffixImpl.class,
						clsFriendlyUrlSuffix.getPrimaryKeyObj());
			}

			if (clsFriendlyUrlSuffix != null) {
				session.delete(clsFriendlyUrlSuffix);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (clsFriendlyUrlSuffix != null) {
			clearCache(clsFriendlyUrlSuffix);
		}

		return clsFriendlyUrlSuffix;
	}

	@Override
	public CLSFriendlyUrlSuffix updateImpl(
		it.eng.rspa.ideas.challenges.servicelayer.model.CLSFriendlyUrlSuffix clsFriendlyUrlSuffix)
		throws SystemException {
		clsFriendlyUrlSuffix = toUnwrappedModel(clsFriendlyUrlSuffix);

		boolean isNew = clsFriendlyUrlSuffix.isNew();

		Session session = null;

		try {
			session = openSession();

			if (clsFriendlyUrlSuffix.isNew()) {
				session.save(clsFriendlyUrlSuffix);

				clsFriendlyUrlSuffix.setNew(false);
			}
			else {
				session.merge(clsFriendlyUrlSuffix);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(CLSFriendlyUrlSuffixModelImpl.ENTITY_CACHE_ENABLED,
			CLSFriendlyUrlSuffixImpl.class,
			clsFriendlyUrlSuffix.getPrimaryKey(), clsFriendlyUrlSuffix);

		return clsFriendlyUrlSuffix;
	}

	protected CLSFriendlyUrlSuffix toUnwrappedModel(
		CLSFriendlyUrlSuffix clsFriendlyUrlSuffix) {
		if (clsFriendlyUrlSuffix instanceof CLSFriendlyUrlSuffixImpl) {
			return clsFriendlyUrlSuffix;
		}

		CLSFriendlyUrlSuffixImpl clsFriendlyUrlSuffixImpl = new CLSFriendlyUrlSuffixImpl();

		clsFriendlyUrlSuffixImpl.setNew(clsFriendlyUrlSuffix.isNew());
		clsFriendlyUrlSuffixImpl.setPrimaryKey(clsFriendlyUrlSuffix.getPrimaryKey());

		clsFriendlyUrlSuffixImpl.setFriendlyUrlSuffixID(clsFriendlyUrlSuffix.getFriendlyUrlSuffixID());
		clsFriendlyUrlSuffixImpl.setUrlSuffixImsHome(clsFriendlyUrlSuffix.getUrlSuffixImsHome());
		clsFriendlyUrlSuffixImpl.setUrlSuffixIdea(clsFriendlyUrlSuffix.getUrlSuffixIdea());
		clsFriendlyUrlSuffixImpl.setUrlSuffixNeed(clsFriendlyUrlSuffix.getUrlSuffixNeed());
		clsFriendlyUrlSuffixImpl.setUrlSuffixChallenge(clsFriendlyUrlSuffix.getUrlSuffixChallenge());
		clsFriendlyUrlSuffixImpl.setSenderNotificheMailIdeario(clsFriendlyUrlSuffix.getSenderNotificheMailIdeario());
		clsFriendlyUrlSuffixImpl.setOggettoNotificheMailIdeario(clsFriendlyUrlSuffix.getOggettoNotificheMailIdeario());
		clsFriendlyUrlSuffixImpl.setFirmaNotificheMailIdeario(clsFriendlyUrlSuffix.getFirmaNotificheMailIdeario());
		clsFriendlyUrlSuffixImpl.setUtenzaMail(clsFriendlyUrlSuffix.getUtenzaMail());
		clsFriendlyUrlSuffixImpl.setCdvEnabled(clsFriendlyUrlSuffix.isCdvEnabled());
		clsFriendlyUrlSuffixImpl.setCdvAddress(clsFriendlyUrlSuffix.getCdvAddress());
		clsFriendlyUrlSuffixImpl.setVcEnabled(clsFriendlyUrlSuffix.isVcEnabled());
		clsFriendlyUrlSuffixImpl.setVcAddress(clsFriendlyUrlSuffix.getVcAddress());
		clsFriendlyUrlSuffixImpl.setVcWSAddress(clsFriendlyUrlSuffix.getVcWSAddress());
		clsFriendlyUrlSuffixImpl.setDeEnabled(clsFriendlyUrlSuffix.isDeEnabled());
		clsFriendlyUrlSuffixImpl.setDeAddress(clsFriendlyUrlSuffix.getDeAddress());
		clsFriendlyUrlSuffixImpl.setLbbEnabled(clsFriendlyUrlSuffix.isLbbEnabled());
		clsFriendlyUrlSuffixImpl.setLbbAddress(clsFriendlyUrlSuffix.getLbbAddress());
		clsFriendlyUrlSuffixImpl.setOiaAppId4lbb(clsFriendlyUrlSuffix.getOiaAppId4lbb());
		clsFriendlyUrlSuffixImpl.setTweetingEnabled(clsFriendlyUrlSuffix.isTweetingEnabled());
		clsFriendlyUrlSuffixImpl.setBasicAuthUser(clsFriendlyUrlSuffix.getBasicAuthUser());
		clsFriendlyUrlSuffixImpl.setBasicAuthPwd(clsFriendlyUrlSuffix.getBasicAuthPwd());
		clsFriendlyUrlSuffixImpl.setVerboseEnabled(clsFriendlyUrlSuffix.isVerboseEnabled());
		clsFriendlyUrlSuffixImpl.setMktEnabled(clsFriendlyUrlSuffix.isMktEnabled());
		clsFriendlyUrlSuffixImpl.setEmailNotificationsEnabled(clsFriendlyUrlSuffix.isEmailNotificationsEnabled());
		clsFriendlyUrlSuffixImpl.setDockbarNotificationsEnabled(clsFriendlyUrlSuffix.isDockbarNotificationsEnabled());
		clsFriendlyUrlSuffixImpl.setJmsEnabled(clsFriendlyUrlSuffix.isJmsEnabled());
		clsFriendlyUrlSuffixImpl.setBrokerJMSusername(clsFriendlyUrlSuffix.getBrokerJMSusername());
		clsFriendlyUrlSuffixImpl.setBrokerJMSpassword(clsFriendlyUrlSuffix.getBrokerJMSpassword());
		clsFriendlyUrlSuffixImpl.setBrokerJMSurl(clsFriendlyUrlSuffix.getBrokerJMSurl());
		clsFriendlyUrlSuffixImpl.setJmsTopic(clsFriendlyUrlSuffix.getJmsTopic());
		clsFriendlyUrlSuffixImpl.setPilotingEnabled(clsFriendlyUrlSuffix.isPilotingEnabled());
		clsFriendlyUrlSuffixImpl.setMockEnabled(clsFriendlyUrlSuffix.isMockEnabled());
		clsFriendlyUrlSuffixImpl.setFiwareEnabled(clsFriendlyUrlSuffix.isFiwareEnabled());
		clsFriendlyUrlSuffixImpl.setFiwareRemoteCatalogueEnabled(clsFriendlyUrlSuffix.isFiwareRemoteCatalogueEnabled());
		clsFriendlyUrlSuffixImpl.setFiwareCatalogueAddress(clsFriendlyUrlSuffix.getFiwareCatalogueAddress());
		clsFriendlyUrlSuffixImpl.setNeedEnabled(clsFriendlyUrlSuffix.isNeedEnabled());
		clsFriendlyUrlSuffixImpl.setPublicIdeasEnabled(clsFriendlyUrlSuffix.isPublicIdeasEnabled());
		clsFriendlyUrlSuffixImpl.setReducedLifecycle(clsFriendlyUrlSuffix.isReducedLifecycle());
		clsFriendlyUrlSuffixImpl.setFundingBoxEnabled(clsFriendlyUrlSuffix.isFundingBoxEnabled());
		clsFriendlyUrlSuffixImpl.setFundingBoxAddress(clsFriendlyUrlSuffix.getFundingBoxAddress());
		clsFriendlyUrlSuffixImpl.setFundingBoxAPIAddress(clsFriendlyUrlSuffix.getFundingBoxAPIAddress());
		clsFriendlyUrlSuffixImpl.setGoogleMapsAPIKeyEnabled(clsFriendlyUrlSuffix.isGoogleMapsAPIKeyEnabled());
		clsFriendlyUrlSuffixImpl.setGoogleMapsAPIKey(clsFriendlyUrlSuffix.getGoogleMapsAPIKey());
		clsFriendlyUrlSuffixImpl.setGraylogEnabled(clsFriendlyUrlSuffix.isGraylogEnabled());
		clsFriendlyUrlSuffixImpl.setGraylogAddress(clsFriendlyUrlSuffix.getGraylogAddress());
		clsFriendlyUrlSuffixImpl.setVirtuosityPointsEnabled(clsFriendlyUrlSuffix.isVirtuosityPointsEnabled());
		clsFriendlyUrlSuffixImpl.setEmailOnNewChallengeEnabled(clsFriendlyUrlSuffix.isEmailOnNewChallengeEnabled());
		clsFriendlyUrlSuffixImpl.setOrionUrl(clsFriendlyUrlSuffix.getOrionUrl());

		return clsFriendlyUrlSuffixImpl;
	}

	/**
	 * Returns the c l s friendly url suffix with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the c l s friendly url suffix
	 * @return the c l s friendly url suffix
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFriendlyUrlSuffixException if a c l s friendly url suffix with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFriendlyUrlSuffix findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCLSFriendlyUrlSuffixException, SystemException {
		CLSFriendlyUrlSuffix clsFriendlyUrlSuffix = fetchByPrimaryKey(primaryKey);

		if (clsFriendlyUrlSuffix == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCLSFriendlyUrlSuffixException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return clsFriendlyUrlSuffix;
	}

	/**
	 * Returns the c l s friendly url suffix with the primary key or throws a {@link it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFriendlyUrlSuffixException} if it could not be found.
	 *
	 * @param friendlyUrlSuffixID the primary key of the c l s friendly url suffix
	 * @return the c l s friendly url suffix
	 * @throws it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSFriendlyUrlSuffixException if a c l s friendly url suffix with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFriendlyUrlSuffix findByPrimaryKey(long friendlyUrlSuffixID)
		throws NoSuchCLSFriendlyUrlSuffixException, SystemException {
		return findByPrimaryKey((Serializable)friendlyUrlSuffixID);
	}

	/**
	 * Returns the c l s friendly url suffix with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the c l s friendly url suffix
	 * @return the c l s friendly url suffix, or <code>null</code> if a c l s friendly url suffix with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFriendlyUrlSuffix fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		CLSFriendlyUrlSuffix clsFriendlyUrlSuffix = (CLSFriendlyUrlSuffix)EntityCacheUtil.getResult(CLSFriendlyUrlSuffixModelImpl.ENTITY_CACHE_ENABLED,
				CLSFriendlyUrlSuffixImpl.class, primaryKey);

		if (clsFriendlyUrlSuffix == _nullCLSFriendlyUrlSuffix) {
			return null;
		}

		if (clsFriendlyUrlSuffix == null) {
			Session session = null;

			try {
				session = openSession();

				clsFriendlyUrlSuffix = (CLSFriendlyUrlSuffix)session.get(CLSFriendlyUrlSuffixImpl.class,
						primaryKey);

				if (clsFriendlyUrlSuffix != null) {
					cacheResult(clsFriendlyUrlSuffix);
				}
				else {
					EntityCacheUtil.putResult(CLSFriendlyUrlSuffixModelImpl.ENTITY_CACHE_ENABLED,
						CLSFriendlyUrlSuffixImpl.class, primaryKey,
						_nullCLSFriendlyUrlSuffix);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(CLSFriendlyUrlSuffixModelImpl.ENTITY_CACHE_ENABLED,
					CLSFriendlyUrlSuffixImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return clsFriendlyUrlSuffix;
	}

	/**
	 * Returns the c l s friendly url suffix with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param friendlyUrlSuffixID the primary key of the c l s friendly url suffix
	 * @return the c l s friendly url suffix, or <code>null</code> if a c l s friendly url suffix with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public CLSFriendlyUrlSuffix fetchByPrimaryKey(long friendlyUrlSuffixID)
		throws SystemException {
		return fetchByPrimaryKey((Serializable)friendlyUrlSuffixID);
	}

	/**
	 * Returns all the c l s friendly url suffixs.
	 *
	 * @return the c l s friendly url suffixs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSFriendlyUrlSuffix> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the c l s friendly url suffixs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFriendlyUrlSuffixModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of c l s friendly url suffixs
	 * @param end the upper bound of the range of c l s friendly url suffixs (not inclusive)
	 * @return the range of c l s friendly url suffixs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSFriendlyUrlSuffix> findAll(int start, int end)
		throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the c l s friendly url suffixs.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link it.eng.rspa.ideas.challenges.servicelayer.model.impl.CLSFriendlyUrlSuffixModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of c l s friendly url suffixs
	 * @param end the upper bound of the range of c l s friendly url suffixs (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of c l s friendly url suffixs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<CLSFriendlyUrlSuffix> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<CLSFriendlyUrlSuffix> list = (List<CLSFriendlyUrlSuffix>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CLSFRIENDLYURLSUFFIX);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CLSFRIENDLYURLSUFFIX;

				if (pagination) {
					sql = sql.concat(CLSFriendlyUrlSuffixModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<CLSFriendlyUrlSuffix>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<CLSFriendlyUrlSuffix>(list);
				}
				else {
					list = (List<CLSFriendlyUrlSuffix>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the c l s friendly url suffixs from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (CLSFriendlyUrlSuffix clsFriendlyUrlSuffix : findAll()) {
			remove(clsFriendlyUrlSuffix);
		}
	}

	/**
	 * Returns the number of c l s friendly url suffixs.
	 *
	 * @return the number of c l s friendly url suffixs
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CLSFRIENDLYURLSUFFIX);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the c l s friendly url suffix persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.it.eng.rspa.ideas.challenges.servicelayer.model.CLSFriendlyUrlSuffix")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<CLSFriendlyUrlSuffix>> listenersList = new ArrayList<ModelListener<CLSFriendlyUrlSuffix>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<CLSFriendlyUrlSuffix>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CLSFriendlyUrlSuffixImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_CLSFRIENDLYURLSUFFIX = "SELECT clsFriendlyUrlSuffix FROM CLSFriendlyUrlSuffix clsFriendlyUrlSuffix";
	private static final String _SQL_COUNT_CLSFRIENDLYURLSUFFIX = "SELECT COUNT(clsFriendlyUrlSuffix) FROM CLSFriendlyUrlSuffix clsFriendlyUrlSuffix";
	private static final String _ORDER_BY_ENTITY_ALIAS = "clsFriendlyUrlSuffix.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CLSFriendlyUrlSuffix exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CLSFriendlyUrlSuffixPersistenceImpl.class);
	private static CLSFriendlyUrlSuffix _nullCLSFriendlyUrlSuffix = new CLSFriendlyUrlSuffixImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<CLSFriendlyUrlSuffix> toCacheModel() {
				return _nullCLSFriendlyUrlSuffixCacheModel;
			}
		};

	private static CacheModel<CLSFriendlyUrlSuffix> _nullCLSFriendlyUrlSuffixCacheModel =
		new CacheModel<CLSFriendlyUrlSuffix>() {
			@Override
			public CLSFriendlyUrlSuffix toEntityModel() {
				return _nullCLSFriendlyUrlSuffix;
			}
		};
}