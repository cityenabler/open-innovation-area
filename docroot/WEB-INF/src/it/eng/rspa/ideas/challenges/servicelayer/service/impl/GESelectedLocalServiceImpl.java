/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

import it.eng.rspa.ideas.challenges.servicelayer.NoSuchGESelectedException;
import it.eng.rspa.ideas.challenges.servicelayer.model.GESelected;
import it.eng.rspa.ideas.challenges.servicelayer.service.base.GESelectedLocalServiceBaseImpl;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.EvaluationCriteriaUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.persistence.GESelectedUtil;

/**
 * The implementation of the g e selected local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.rspa.ideas.challenges.servicelayer.service.GESelectedLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.base.GESelectedLocalServiceBaseImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.GESelectedLocalServiceUtil
 */
public class GESelectedLocalServiceImpl extends GESelectedLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.eng.rspa.ideas.challenges.servicelayer.service.GESelectedLocalServiceUtil} to access the g e selected local service.
	 */
	
	/**
	 * @param ideaId
	 * @return
	 */
	public List<GESelected> getGeSelectedsByIdeaId(long ideaId){
		
		List<GESelected> ret = new ArrayList<GESelected>();
		
		try {
			return GESelectedUtil.findBygesByIdeaId(ideaId);
		} catch (SystemException e) {
			
			e.printStackTrace();
			return ret;
		}
		
	}
	
	/**
	 * @param nid
	 * @return
	 */
	public List<GESelected> getGeSelectedsByNid(String nid){
		
		List<GESelected> ret = new ArrayList<GESelected>();
		
		try {
			return GESelectedUtil.findBygesByNid(nid);
		} catch (SystemException e) {
			
			e.printStackTrace();
			return ret;
		}
		
	}
	
	
	/**
	 * @param ideaId
	 * @param nid
	 * @return
	 */
	public GESelected getGeSelectedsByIdeaIdAndNid(long ideaId,String nid){
		
			try {
				return GESelectedUtil.findByideaIdAndNid(ideaId, nid);
			} catch (NoSuchGESelectedException | SystemException e) {
//				e.printStackTrace();
				return null;
			}
		
		
	}
	
	
	
	
	
}