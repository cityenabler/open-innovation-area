/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.ideas.challenges.servicelayer.model.GESelected;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing GESelected in entity cache.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see GESelected
 * @generated
 */
public class GESelectedCacheModel implements CacheModel<GESelected>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{ideaId=");
		sb.append(ideaId);
		sb.append(", nid=");
		sb.append(nid);
		sb.append(", url=");
		sb.append(url);
		sb.append(", icon=");
		sb.append(icon);
		sb.append(", title=");
		sb.append(title);
		sb.append(", shortDescription=");
		sb.append(shortDescription);
		sb.append(", label=");
		sb.append(label);
		sb.append(", rank=");
		sb.append(rank);
		sb.append(", chapter=");
		sb.append(chapter);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public GESelected toEntityModel() {
		GESelectedImpl geSelectedImpl = new GESelectedImpl();

		geSelectedImpl.setIdeaId(ideaId);

		if (nid == null) {
			geSelectedImpl.setNid(StringPool.BLANK);
		}
		else {
			geSelectedImpl.setNid(nid);
		}

		if (url == null) {
			geSelectedImpl.setUrl(StringPool.BLANK);
		}
		else {
			geSelectedImpl.setUrl(url);
		}

		if (icon == null) {
			geSelectedImpl.setIcon(StringPool.BLANK);
		}
		else {
			geSelectedImpl.setIcon(icon);
		}

		if (title == null) {
			geSelectedImpl.setTitle(StringPool.BLANK);
		}
		else {
			geSelectedImpl.setTitle(title);
		}

		if (shortDescription == null) {
			geSelectedImpl.setShortDescription(StringPool.BLANK);
		}
		else {
			geSelectedImpl.setShortDescription(shortDescription);
		}

		if (label == null) {
			geSelectedImpl.setLabel(StringPool.BLANK);
		}
		else {
			geSelectedImpl.setLabel(label);
		}

		if (rank == null) {
			geSelectedImpl.setRank(StringPool.BLANK);
		}
		else {
			geSelectedImpl.setRank(rank);
		}

		if (chapter == null) {
			geSelectedImpl.setChapter(StringPool.BLANK);
		}
		else {
			geSelectedImpl.setChapter(chapter);
		}

		geSelectedImpl.resetOriginalValues();

		return geSelectedImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		ideaId = objectInput.readLong();
		nid = objectInput.readUTF();
		url = objectInput.readUTF();
		icon = objectInput.readUTF();
		title = objectInput.readUTF();
		shortDescription = objectInput.readUTF();
		label = objectInput.readUTF();
		rank = objectInput.readUTF();
		chapter = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(ideaId);

		if (nid == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(nid);
		}

		if (url == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(url);
		}

		if (icon == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(icon);
		}

		if (title == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(title);
		}

		if (shortDescription == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(shortDescription);
		}

		if (label == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(label);
		}

		if (rank == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(rank);
		}

		if (chapter == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(chapter);
		}
	}

	public long ideaId;
	public String nid;
	public String url;
	public String icon;
	public String title;
	public String shortDescription;
	public String label;
	public String rank;
	public String chapter;
}