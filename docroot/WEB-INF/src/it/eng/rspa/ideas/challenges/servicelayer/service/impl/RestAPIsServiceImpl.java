/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.impl;

import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.logging.Logger;

import com.ibm.icu.text.SimpleDateFormat;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.jsonwebservice.JSONWebService;
import com.liferay.portal.security.ac.AccessControlled;

import it.eng.rspa.ideas.challenges.servicelayer.NoSuchCLSIdeaException;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea;
import it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdeaPoi;
import it.eng.rspa.ideas.challenges.servicelayer.model.NeedLinkedChallenge;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSChallengeLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.CLSIdeaPoiLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.NeedLinkedChallengeLocalServiceUtil;
import it.eng.rspa.ideas.challenges.servicelayer.service.base.RestAPIsServiceBaseImpl;
import it.eng.rspa.ideas.utils.ApiRestUtils;

/**
 * The implementation of the rest a p is remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.rspa.ideas.challenges.servicelayer.service.RestAPIsService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.base.RestAPIsServiceBaseImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.RestAPIsServiceUtil
 */
public class RestAPIsServiceImpl extends RestAPIsServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.eng.rspa.ideas.challenges.servicelayer.service.RestAPIsServiceUtil} to access the rest a p is remote service.
	 */
	
	
	private static Logger _log = Logger.getLogger(RestAPIsServiceImpl.class.getName());

	
	@JSONWebService(value="/apis/challenges", method="GET")
	@AccessControlled(guestAccessEnabled = true)
	public JSONArray challenges() {
		
		JSONArray response = JSONFactoryUtil.createJSONArray();
		List<CLSChallenge> challenges; 
		try{ challenges = CLSChallengeLocalServiceUtil.getCLSChallenges(QueryUtil.ALL_POS, QueryUtil.ALL_POS); }
		catch(Exception e){
			_log.warning(e.toString());
			challenges = new ArrayList<CLSChallenge>(); 
		}
		
		for(CLSChallenge c : challenges){
			try{
				JSONObject jChallenge = ApiRestUtils.challenge2JsonObject(c);
				response.put(jChallenge);
			}
			catch(Exception e){
				_log.warning(e.toString());
			}
		}
		
		return response;
	}
	
	
	
	@JSONWebService(value="/apis/challenge", method="GET")
	@AccessControlled(guestAccessEnabled = true)
	public JSONObject onechallenge(long id)  throws PortalException, SystemException {
		JSONObject jChallenge;
		CLSChallenge c = CLSChallengeLocalServiceUtil.getCLSChallenge(id); 
		jChallenge = ApiRestUtils.challenge2JsonObject(c);
		return jChallenge;
	}
	
	
	
	@JSONWebService(value="/apis/ideas", method="GET")
	@AccessControlled(guestAccessEnabled = true)
	public JSONArray ideas() {
		
		JSONArray response = JSONFactoryUtil.createJSONArray();
		List<CLSIdea> ideas; 
		try{ ideas = CLSIdeaLocalServiceUtil.getOnlyIdeas(); }
		catch(Exception e){
			_log.warning(e.toString());
			ideas = new ArrayList<CLSIdea>(); 
		}
		
		for(CLSIdea i : ideas){
			try{
				JSONObject jIdea = ApiRestUtils.idea2JsonObject(i, false);
				response.put(jIdea);
			}
			catch(Exception e){
				_log.warning(e.toString());
			}
		}
		
		return response;
	}
	
	
	
	@JSONWebService(value="/apis/idea", method="GET")
	@AccessControlled(guestAccessEnabled = true)
	public JSONObject oneidea(long id) throws PortalException, SystemException {
		JSONObject jChallenge;
		CLSIdea c = CLSIdeaLocalServiceUtil.getCLSIdea(id); 
		if(c.isIsNeed()){
			throw new NoSuchCLSIdeaException("No idea exists with id "+id);
		}
		jChallenge = ApiRestUtils.idea2JsonObject(c, false);
		return jChallenge;
	}
	
	
	
	@JSONWebService(value="/apis/needs", method="GET")
	@AccessControlled(guestAccessEnabled = true)
	public JSONArray allNeeds() {
		JSONArray response = JSONFactoryUtil.createJSONArray();
		List<CLSIdea> ideas = new ArrayList<CLSIdea>();
		try{ ideas.addAll(CLSIdeaLocalServiceUtil.getNeeds()); }
		catch(Exception e){
			_log.warning(e.toString());
		}
		
		for(CLSIdea i : ideas){
			try{
				JSONObject jIdea = ApiRestUtils.idea2JsonObject(i, true);
				response.put(jIdea);
			}
			catch(Exception e){
				_log.warning(e.toString());
			}
		}
		
		return response;
	}
	
	
	
	@JSONWebService(value="/apis/need", method="GET")
	@AccessControlled(guestAccessEnabled = true)
	public JSONObject oneneed(long id) throws PortalException, SystemException {
		JSONObject jNeed;
		CLSIdea c = CLSIdeaLocalServiceUtil.getCLSIdea(id);
		if(!c.isIsNeed()){
			throw new NoSuchCLSIdeaException("No need exists with id "+id);
		}
		jNeed = ApiRestUtils.idea2JsonObject(c, true);
		return jNeed;
	}
	
	
	@JSONWebService(value="/open311/needs", method="GET")
	@AccessControlled(guestAccessEnabled = true)
	public JSONArray allNeedsOpen311() {
		JSONArray response = JSONFactoryUtil.createJSONArray();
		List<CLSIdea> ideas = new ArrayList<CLSIdea>();
		try{ ideas.addAll(CLSIdeaLocalServiceUtil.getNeeds()); }
		catch(Exception e){
			_log.warning(e.toString());
		}
		
		for(CLSIdea i : ideas){
			try{
				JSONObject jIdea = ApiRestUtils.need2TypeJsonObject(i);
				response.put(jIdea);
			}
			catch(Exception e){
				_log.warning(e.toString());
			}
		}
		
		return response;
	}
	
	
	@JSONWebService(value="/open311/need", method="GET")
	@AccessControlled(guestAccessEnabled = true)
	public JSONObject oneneedOpen311(long id) throws PortalException, SystemException {
		JSONObject jNeed;
		CLSIdea c = CLSIdeaLocalServiceUtil.getCLSIdea(id);
		if(!c.isIsNeed()){
			throw new NoSuchCLSIdeaException("No need exists with id "+id);
		}
		jNeed = ApiRestUtils.need2TypeJsonObject(c);
		return jNeed;
	}
	
	@JSONWebService(value="/open311/pois", method="GET")
	@AccessControlled(guestAccessEnabled = true)
	public JSONArray allPoisOpen311() {
		JSONArray response = JSONFactoryUtil.createJSONArray();
		List<CLSIdeaPoi> pois = new ArrayList<CLSIdeaPoi>();
		try{ pois.addAll(CLSIdeaPoiLocalServiceUtil.getCLSIdeaPois(0, CLSIdeaPoiLocalServiceUtil.getCLSIdeaPoisCount())); }
		catch(Exception e){
			_log.warning(e.toString());
		}
		
		for(CLSIdeaPoi poi : pois){
			try{
				JSONObject jpoi = ApiRestUtils.poiToRequestJsonObject(poi, 1);
				response.put(jpoi);
			}
			catch(Exception e){
				_log.warning(e.toString());
			}
		}
		
		return response;
	}
	
	@JSONWebService(value="/open311/poi", method="GET")
	@AccessControlled(guestAccessEnabled = true)
	public JSONObject onepoiOpen311(long id) throws PortalException, SystemException {
		JSONObject jPoi;
		CLSIdeaPoi poi = CLSIdeaPoiLocalServiceUtil.getCLSIdeaPoi(id);
	
		jPoi = ApiRestUtils.poiToRequestJsonObject(poi, 1);
		return jPoi;
	}
	
	@JSONWebService(value="/open311_v2/pois", method="GET")
	@AccessControlled(guestAccessEnabled = true)
	public JSONArray allPoisOpen311_V2() {
		JSONArray response = JSONFactoryUtil.createJSONArray();
		List<CLSIdeaPoi> pois = new ArrayList<CLSIdeaPoi>();
		try{ pois.addAll(CLSIdeaPoiLocalServiceUtil.getCLSIdeaPois(0, CLSIdeaPoiLocalServiceUtil.getCLSIdeaPoisCount())); }
		catch(Exception e){
			_log.warning(e.toString());
		}
		
		for(CLSIdeaPoi poi : pois){
			try{
				JSONObject jpoi = ApiRestUtils.poiToRequestJsonObject(poi, 2);
				response.put(jpoi);
			}
			catch(Exception e){
				_log.warning(e.toString());
			}
		}
		
		return response;
	}
	
	@JSONWebService(value="/open311_v2/poi", method="GET")
	@AccessControlled(guestAccessEnabled = true)
	public JSONObject onepoiOpen311_V2(long id) throws PortalException, SystemException {
		JSONObject jPoi;
		CLSIdeaPoi poi = CLSIdeaPoiLocalServiceUtil.getCLSIdeaPoi(id);
	
		jPoi = ApiRestUtils.poiToRequestJsonObject(poi, 2);
		return jPoi;
	}
	
}