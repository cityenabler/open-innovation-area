/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSFriendlyUrlSuffix;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing CLSFriendlyUrlSuffix in entity cache.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see CLSFriendlyUrlSuffix
 * @generated
 */
public class CLSFriendlyUrlSuffixCacheModel implements CacheModel<CLSFriendlyUrlSuffix>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(99);

		sb.append("{friendlyUrlSuffixID=");
		sb.append(friendlyUrlSuffixID);
		sb.append(", UrlSuffixImsHome=");
		sb.append(UrlSuffixImsHome);
		sb.append(", UrlSuffixIdea=");
		sb.append(UrlSuffixIdea);
		sb.append(", UrlSuffixNeed=");
		sb.append(UrlSuffixNeed);
		sb.append(", UrlSuffixChallenge=");
		sb.append(UrlSuffixChallenge);
		sb.append(", senderNotificheMailIdeario=");
		sb.append(senderNotificheMailIdeario);
		sb.append(", oggettoNotificheMailIdeario=");
		sb.append(oggettoNotificheMailIdeario);
		sb.append(", firmaNotificheMailIdeario=");
		sb.append(firmaNotificheMailIdeario);
		sb.append(", utenzaMail=");
		sb.append(utenzaMail);
		sb.append(", cdvEnabled=");
		sb.append(cdvEnabled);
		sb.append(", cdvAddress=");
		sb.append(cdvAddress);
		sb.append(", vcEnabled=");
		sb.append(vcEnabled);
		sb.append(", vcAddress=");
		sb.append(vcAddress);
		sb.append(", vcWSAddress=");
		sb.append(vcWSAddress);
		sb.append(", deEnabled=");
		sb.append(deEnabled);
		sb.append(", deAddress=");
		sb.append(deAddress);
		sb.append(", lbbEnabled=");
		sb.append(lbbEnabled);
		sb.append(", lbbAddress=");
		sb.append(lbbAddress);
		sb.append(", oiaAppId4lbb=");
		sb.append(oiaAppId4lbb);
		sb.append(", tweetingEnabled=");
		sb.append(tweetingEnabled);
		sb.append(", basicAuthUser=");
		sb.append(basicAuthUser);
		sb.append(", basicAuthPwd=");
		sb.append(basicAuthPwd);
		sb.append(", verboseEnabled=");
		sb.append(verboseEnabled);
		sb.append(", mktEnabled=");
		sb.append(mktEnabled);
		sb.append(", emailNotificationsEnabled=");
		sb.append(emailNotificationsEnabled);
		sb.append(", dockbarNotificationsEnabled=");
		sb.append(dockbarNotificationsEnabled);
		sb.append(", jmsEnabled=");
		sb.append(jmsEnabled);
		sb.append(", brokerJMSusername=");
		sb.append(brokerJMSusername);
		sb.append(", brokerJMSpassword=");
		sb.append(brokerJMSpassword);
		sb.append(", brokerJMSurl=");
		sb.append(brokerJMSurl);
		sb.append(", jmsTopic=");
		sb.append(jmsTopic);
		sb.append(", pilotingEnabled=");
		sb.append(pilotingEnabled);
		sb.append(", mockEnabled=");
		sb.append(mockEnabled);
		sb.append(", fiwareEnabled=");
		sb.append(fiwareEnabled);
		sb.append(", fiwareRemoteCatalogueEnabled=");
		sb.append(fiwareRemoteCatalogueEnabled);
		sb.append(", fiwareCatalogueAddress=");
		sb.append(fiwareCatalogueAddress);
		sb.append(", needEnabled=");
		sb.append(needEnabled);
		sb.append(", publicIdeasEnabled=");
		sb.append(publicIdeasEnabled);
		sb.append(", reducedLifecycle=");
		sb.append(reducedLifecycle);
		sb.append(", fundingBoxEnabled=");
		sb.append(fundingBoxEnabled);
		sb.append(", fundingBoxAddress=");
		sb.append(fundingBoxAddress);
		sb.append(", fundingBoxAPIAddress=");
		sb.append(fundingBoxAPIAddress);
		sb.append(", googleMapsAPIKeyEnabled=");
		sb.append(googleMapsAPIKeyEnabled);
		sb.append(", googleMapsAPIKey=");
		sb.append(googleMapsAPIKey);
		sb.append(", graylogEnabled=");
		sb.append(graylogEnabled);
		sb.append(", graylogAddress=");
		sb.append(graylogAddress);
		sb.append(", virtuosityPointsEnabled=");
		sb.append(virtuosityPointsEnabled);
		sb.append(", emailOnNewChallengeEnabled=");
		sb.append(emailOnNewChallengeEnabled);
		sb.append(", orionUrl=");
		sb.append(orionUrl);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public CLSFriendlyUrlSuffix toEntityModel() {
		CLSFriendlyUrlSuffixImpl clsFriendlyUrlSuffixImpl = new CLSFriendlyUrlSuffixImpl();

		clsFriendlyUrlSuffixImpl.setFriendlyUrlSuffixID(friendlyUrlSuffixID);

		if (UrlSuffixImsHome == null) {
			clsFriendlyUrlSuffixImpl.setUrlSuffixImsHome(StringPool.BLANK);
		}
		else {
			clsFriendlyUrlSuffixImpl.setUrlSuffixImsHome(UrlSuffixImsHome);
		}

		if (UrlSuffixIdea == null) {
			clsFriendlyUrlSuffixImpl.setUrlSuffixIdea(StringPool.BLANK);
		}
		else {
			clsFriendlyUrlSuffixImpl.setUrlSuffixIdea(UrlSuffixIdea);
		}

		if (UrlSuffixNeed == null) {
			clsFriendlyUrlSuffixImpl.setUrlSuffixNeed(StringPool.BLANK);
		}
		else {
			clsFriendlyUrlSuffixImpl.setUrlSuffixNeed(UrlSuffixNeed);
		}

		if (UrlSuffixChallenge == null) {
			clsFriendlyUrlSuffixImpl.setUrlSuffixChallenge(StringPool.BLANK);
		}
		else {
			clsFriendlyUrlSuffixImpl.setUrlSuffixChallenge(UrlSuffixChallenge);
		}

		if (senderNotificheMailIdeario == null) {
			clsFriendlyUrlSuffixImpl.setSenderNotificheMailIdeario(StringPool.BLANK);
		}
		else {
			clsFriendlyUrlSuffixImpl.setSenderNotificheMailIdeario(senderNotificheMailIdeario);
		}

		if (oggettoNotificheMailIdeario == null) {
			clsFriendlyUrlSuffixImpl.setOggettoNotificheMailIdeario(StringPool.BLANK);
		}
		else {
			clsFriendlyUrlSuffixImpl.setOggettoNotificheMailIdeario(oggettoNotificheMailIdeario);
		}

		if (firmaNotificheMailIdeario == null) {
			clsFriendlyUrlSuffixImpl.setFirmaNotificheMailIdeario(StringPool.BLANK);
		}
		else {
			clsFriendlyUrlSuffixImpl.setFirmaNotificheMailIdeario(firmaNotificheMailIdeario);
		}

		if (utenzaMail == null) {
			clsFriendlyUrlSuffixImpl.setUtenzaMail(StringPool.BLANK);
		}
		else {
			clsFriendlyUrlSuffixImpl.setUtenzaMail(utenzaMail);
		}

		clsFriendlyUrlSuffixImpl.setCdvEnabled(cdvEnabled);

		if (cdvAddress == null) {
			clsFriendlyUrlSuffixImpl.setCdvAddress(StringPool.BLANK);
		}
		else {
			clsFriendlyUrlSuffixImpl.setCdvAddress(cdvAddress);
		}

		clsFriendlyUrlSuffixImpl.setVcEnabled(vcEnabled);

		if (vcAddress == null) {
			clsFriendlyUrlSuffixImpl.setVcAddress(StringPool.BLANK);
		}
		else {
			clsFriendlyUrlSuffixImpl.setVcAddress(vcAddress);
		}

		if (vcWSAddress == null) {
			clsFriendlyUrlSuffixImpl.setVcWSAddress(StringPool.BLANK);
		}
		else {
			clsFriendlyUrlSuffixImpl.setVcWSAddress(vcWSAddress);
		}

		clsFriendlyUrlSuffixImpl.setDeEnabled(deEnabled);

		if (deAddress == null) {
			clsFriendlyUrlSuffixImpl.setDeAddress(StringPool.BLANK);
		}
		else {
			clsFriendlyUrlSuffixImpl.setDeAddress(deAddress);
		}

		clsFriendlyUrlSuffixImpl.setLbbEnabled(lbbEnabled);

		if (lbbAddress == null) {
			clsFriendlyUrlSuffixImpl.setLbbAddress(StringPool.BLANK);
		}
		else {
			clsFriendlyUrlSuffixImpl.setLbbAddress(lbbAddress);
		}

		if (oiaAppId4lbb == null) {
			clsFriendlyUrlSuffixImpl.setOiaAppId4lbb(StringPool.BLANK);
		}
		else {
			clsFriendlyUrlSuffixImpl.setOiaAppId4lbb(oiaAppId4lbb);
		}

		clsFriendlyUrlSuffixImpl.setTweetingEnabled(tweetingEnabled);

		if (basicAuthUser == null) {
			clsFriendlyUrlSuffixImpl.setBasicAuthUser(StringPool.BLANK);
		}
		else {
			clsFriendlyUrlSuffixImpl.setBasicAuthUser(basicAuthUser);
		}

		if (basicAuthPwd == null) {
			clsFriendlyUrlSuffixImpl.setBasicAuthPwd(StringPool.BLANK);
		}
		else {
			clsFriendlyUrlSuffixImpl.setBasicAuthPwd(basicAuthPwd);
		}

		clsFriendlyUrlSuffixImpl.setVerboseEnabled(verboseEnabled);
		clsFriendlyUrlSuffixImpl.setMktEnabled(mktEnabled);
		clsFriendlyUrlSuffixImpl.setEmailNotificationsEnabled(emailNotificationsEnabled);
		clsFriendlyUrlSuffixImpl.setDockbarNotificationsEnabled(dockbarNotificationsEnabled);
		clsFriendlyUrlSuffixImpl.setJmsEnabled(jmsEnabled);

		if (brokerJMSusername == null) {
			clsFriendlyUrlSuffixImpl.setBrokerJMSusername(StringPool.BLANK);
		}
		else {
			clsFriendlyUrlSuffixImpl.setBrokerJMSusername(brokerJMSusername);
		}

		if (brokerJMSpassword == null) {
			clsFriendlyUrlSuffixImpl.setBrokerJMSpassword(StringPool.BLANK);
		}
		else {
			clsFriendlyUrlSuffixImpl.setBrokerJMSpassword(brokerJMSpassword);
		}

		if (brokerJMSurl == null) {
			clsFriendlyUrlSuffixImpl.setBrokerJMSurl(StringPool.BLANK);
		}
		else {
			clsFriendlyUrlSuffixImpl.setBrokerJMSurl(brokerJMSurl);
		}

		if (jmsTopic == null) {
			clsFriendlyUrlSuffixImpl.setJmsTopic(StringPool.BLANK);
		}
		else {
			clsFriendlyUrlSuffixImpl.setJmsTopic(jmsTopic);
		}

		clsFriendlyUrlSuffixImpl.setPilotingEnabled(pilotingEnabled);
		clsFriendlyUrlSuffixImpl.setMockEnabled(mockEnabled);
		clsFriendlyUrlSuffixImpl.setFiwareEnabled(fiwareEnabled);
		clsFriendlyUrlSuffixImpl.setFiwareRemoteCatalogueEnabled(fiwareRemoteCatalogueEnabled);

		if (fiwareCatalogueAddress == null) {
			clsFriendlyUrlSuffixImpl.setFiwareCatalogueAddress(StringPool.BLANK);
		}
		else {
			clsFriendlyUrlSuffixImpl.setFiwareCatalogueAddress(fiwareCatalogueAddress);
		}

		clsFriendlyUrlSuffixImpl.setNeedEnabled(needEnabled);
		clsFriendlyUrlSuffixImpl.setPublicIdeasEnabled(publicIdeasEnabled);
		clsFriendlyUrlSuffixImpl.setReducedLifecycle(reducedLifecycle);
		clsFriendlyUrlSuffixImpl.setFundingBoxEnabled(fundingBoxEnabled);

		if (fundingBoxAddress == null) {
			clsFriendlyUrlSuffixImpl.setFundingBoxAddress(StringPool.BLANK);
		}
		else {
			clsFriendlyUrlSuffixImpl.setFundingBoxAddress(fundingBoxAddress);
		}

		if (fundingBoxAPIAddress == null) {
			clsFriendlyUrlSuffixImpl.setFundingBoxAPIAddress(StringPool.BLANK);
		}
		else {
			clsFriendlyUrlSuffixImpl.setFundingBoxAPIAddress(fundingBoxAPIAddress);
		}

		clsFriendlyUrlSuffixImpl.setGoogleMapsAPIKeyEnabled(googleMapsAPIKeyEnabled);

		if (googleMapsAPIKey == null) {
			clsFriendlyUrlSuffixImpl.setGoogleMapsAPIKey(StringPool.BLANK);
		}
		else {
			clsFriendlyUrlSuffixImpl.setGoogleMapsAPIKey(googleMapsAPIKey);
		}

		clsFriendlyUrlSuffixImpl.setGraylogEnabled(graylogEnabled);

		if (graylogAddress == null) {
			clsFriendlyUrlSuffixImpl.setGraylogAddress(StringPool.BLANK);
		}
		else {
			clsFriendlyUrlSuffixImpl.setGraylogAddress(graylogAddress);
		}

		clsFriendlyUrlSuffixImpl.setVirtuosityPointsEnabled(virtuosityPointsEnabled);
		clsFriendlyUrlSuffixImpl.setEmailOnNewChallengeEnabled(emailOnNewChallengeEnabled);

		if (orionUrl == null) {
			clsFriendlyUrlSuffixImpl.setOrionUrl(StringPool.BLANK);
		}
		else {
			clsFriendlyUrlSuffixImpl.setOrionUrl(orionUrl);
		}

		clsFriendlyUrlSuffixImpl.resetOriginalValues();

		return clsFriendlyUrlSuffixImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		friendlyUrlSuffixID = objectInput.readLong();
		UrlSuffixImsHome = objectInput.readUTF();
		UrlSuffixIdea = objectInput.readUTF();
		UrlSuffixNeed = objectInput.readUTF();
		UrlSuffixChallenge = objectInput.readUTF();
		senderNotificheMailIdeario = objectInput.readUTF();
		oggettoNotificheMailIdeario = objectInput.readUTF();
		firmaNotificheMailIdeario = objectInput.readUTF();
		utenzaMail = objectInput.readUTF();
		cdvEnabled = objectInput.readBoolean();
		cdvAddress = objectInput.readUTF();
		vcEnabled = objectInput.readBoolean();
		vcAddress = objectInput.readUTF();
		vcWSAddress = objectInput.readUTF();
		deEnabled = objectInput.readBoolean();
		deAddress = objectInput.readUTF();
		lbbEnabled = objectInput.readBoolean();
		lbbAddress = objectInput.readUTF();
		oiaAppId4lbb = objectInput.readUTF();
		tweetingEnabled = objectInput.readBoolean();
		basicAuthUser = objectInput.readUTF();
		basicAuthPwd = objectInput.readUTF();
		verboseEnabled = objectInput.readBoolean();
		mktEnabled = objectInput.readBoolean();
		emailNotificationsEnabled = objectInput.readBoolean();
		dockbarNotificationsEnabled = objectInput.readBoolean();
		jmsEnabled = objectInput.readBoolean();
		brokerJMSusername = objectInput.readUTF();
		brokerJMSpassword = objectInput.readUTF();
		brokerJMSurl = objectInput.readUTF();
		jmsTopic = objectInput.readUTF();
		pilotingEnabled = objectInput.readBoolean();
		mockEnabled = objectInput.readBoolean();
		fiwareEnabled = objectInput.readBoolean();
		fiwareRemoteCatalogueEnabled = objectInput.readBoolean();
		fiwareCatalogueAddress = objectInput.readUTF();
		needEnabled = objectInput.readBoolean();
		publicIdeasEnabled = objectInput.readBoolean();
		reducedLifecycle = objectInput.readBoolean();
		fundingBoxEnabled = objectInput.readBoolean();
		fundingBoxAddress = objectInput.readUTF();
		fundingBoxAPIAddress = objectInput.readUTF();
		googleMapsAPIKeyEnabled = objectInput.readBoolean();
		googleMapsAPIKey = objectInput.readUTF();
		graylogEnabled = objectInput.readBoolean();
		graylogAddress = objectInput.readUTF();
		virtuosityPointsEnabled = objectInput.readBoolean();
		emailOnNewChallengeEnabled = objectInput.readBoolean();
		orionUrl = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(friendlyUrlSuffixID);

		if (UrlSuffixImsHome == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(UrlSuffixImsHome);
		}

		if (UrlSuffixIdea == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(UrlSuffixIdea);
		}

		if (UrlSuffixNeed == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(UrlSuffixNeed);
		}

		if (UrlSuffixChallenge == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(UrlSuffixChallenge);
		}

		if (senderNotificheMailIdeario == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(senderNotificheMailIdeario);
		}

		if (oggettoNotificheMailIdeario == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(oggettoNotificheMailIdeario);
		}

		if (firmaNotificheMailIdeario == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(firmaNotificheMailIdeario);
		}

		if (utenzaMail == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(utenzaMail);
		}

		objectOutput.writeBoolean(cdvEnabled);

		if (cdvAddress == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(cdvAddress);
		}

		objectOutput.writeBoolean(vcEnabled);

		if (vcAddress == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(vcAddress);
		}

		if (vcWSAddress == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(vcWSAddress);
		}

		objectOutput.writeBoolean(deEnabled);

		if (deAddress == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(deAddress);
		}

		objectOutput.writeBoolean(lbbEnabled);

		if (lbbAddress == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(lbbAddress);
		}

		if (oiaAppId4lbb == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(oiaAppId4lbb);
		}

		objectOutput.writeBoolean(tweetingEnabled);

		if (basicAuthUser == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(basicAuthUser);
		}

		if (basicAuthPwd == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(basicAuthPwd);
		}

		objectOutput.writeBoolean(verboseEnabled);
		objectOutput.writeBoolean(mktEnabled);
		objectOutput.writeBoolean(emailNotificationsEnabled);
		objectOutput.writeBoolean(dockbarNotificationsEnabled);
		objectOutput.writeBoolean(jmsEnabled);

		if (brokerJMSusername == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(brokerJMSusername);
		}

		if (brokerJMSpassword == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(brokerJMSpassword);
		}

		if (brokerJMSurl == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(brokerJMSurl);
		}

		if (jmsTopic == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(jmsTopic);
		}

		objectOutput.writeBoolean(pilotingEnabled);
		objectOutput.writeBoolean(mockEnabled);
		objectOutput.writeBoolean(fiwareEnabled);
		objectOutput.writeBoolean(fiwareRemoteCatalogueEnabled);

		if (fiwareCatalogueAddress == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(fiwareCatalogueAddress);
		}

		objectOutput.writeBoolean(needEnabled);
		objectOutput.writeBoolean(publicIdeasEnabled);
		objectOutput.writeBoolean(reducedLifecycle);
		objectOutput.writeBoolean(fundingBoxEnabled);

		if (fundingBoxAddress == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(fundingBoxAddress);
		}

		if (fundingBoxAPIAddress == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(fundingBoxAPIAddress);
		}

		objectOutput.writeBoolean(googleMapsAPIKeyEnabled);

		if (googleMapsAPIKey == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(googleMapsAPIKey);
		}

		objectOutput.writeBoolean(graylogEnabled);

		if (graylogAddress == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(graylogAddress);
		}

		objectOutput.writeBoolean(virtuosityPointsEnabled);
		objectOutput.writeBoolean(emailOnNewChallengeEnabled);

		if (orionUrl == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(orionUrl);
		}
	}

	public long friendlyUrlSuffixID;
	public String UrlSuffixImsHome;
	public String UrlSuffixIdea;
	public String UrlSuffixNeed;
	public String UrlSuffixChallenge;
	public String senderNotificheMailIdeario;
	public String oggettoNotificheMailIdeario;
	public String firmaNotificheMailIdeario;
	public String utenzaMail;
	public boolean cdvEnabled;
	public String cdvAddress;
	public boolean vcEnabled;
	public String vcAddress;
	public String vcWSAddress;
	public boolean deEnabled;
	public String deAddress;
	public boolean lbbEnabled;
	public String lbbAddress;
	public String oiaAppId4lbb;
	public boolean tweetingEnabled;
	public String basicAuthUser;
	public String basicAuthPwd;
	public boolean verboseEnabled;
	public boolean mktEnabled;
	public boolean emailNotificationsEnabled;
	public boolean dockbarNotificationsEnabled;
	public boolean jmsEnabled;
	public String brokerJMSusername;
	public String brokerJMSpassword;
	public String brokerJMSurl;
	public String jmsTopic;
	public boolean pilotingEnabled;
	public boolean mockEnabled;
	public boolean fiwareEnabled;
	public boolean fiwareRemoteCatalogueEnabled;
	public String fiwareCatalogueAddress;
	public boolean needEnabled;
	public boolean publicIdeasEnabled;
	public boolean reducedLifecycle;
	public boolean fundingBoxEnabled;
	public String fundingBoxAddress;
	public String fundingBoxAPIAddress;
	public boolean googleMapsAPIKeyEnabled;
	public String googleMapsAPIKey;
	public boolean graylogEnabled;
	public String graylogAddress;
	public boolean virtuosityPointsEnabled;
	public boolean emailOnNewChallengeEnabled;
	public String orionUrl;
}