/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.service.impl;

import it.eng.rspa.ideas.challenges.servicelayer.model.CLSArtifacts;
import it.eng.rspa.ideas.challenges.servicelayer.service.base.CLSArtifactsLocalServiceBaseImpl;
import it.eng.rspa.ideas.utils.DeleteIdeaUtils;

import java.util.List;

import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the c l s artifacts local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.eng.rspa.ideas.challenges.servicelayer.service.CLSArtifactsLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.base.CLSArtifactsLocalServiceBaseImpl
 * @see it.eng.rspa.ideas.challenges.servicelayer.service.CLSArtifactsLocalServiceUtil
 */
public class CLSArtifactsLocalServiceImpl
	extends CLSArtifactsLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.eng.rspa.ideas.challenges.servicelayer.service.CLSArtifactsLocalServiceUtil} to access the c l s artifacts local service.
	 */
	
	
	public List<CLSArtifacts> getArtifactsByIdeaId(Long ideaId) throws SystemException {		 	   
		return clsArtifactsPersistence.findByartifactsByIdeaId(ideaId);
	}
	
	public List<CLSArtifacts> getArtifactsByArtifactId(Long artifactId) throws SystemException {		
		return clsArtifactsPersistence.findByartifactsByArtifactId(artifactId);
	}
	public List<CLSArtifacts> getArtifactsByIdeaIdEArtifactId(Long ideaId,Long artifactId) throws SystemException {		 	   
		return clsArtifactsPersistence.findByartifactsByIdeaIdEArtifactId(ideaId, artifactId);
	}
	 
	@Override
	public List<CLSArtifacts> getFavouriteIdeas(Long ideaId)
				throws SystemException {
			return null;
	}
	 
	 
	 /**
	 * @param artifactId
	 */
	public void unlinkArtifactAndIdeas(long artifactId ){
		 
		 DeleteIdeaUtils.deletIdeasAssociationByArtifactId(artifactId);	 
		 
		 
	 }
	 
	 
}


	