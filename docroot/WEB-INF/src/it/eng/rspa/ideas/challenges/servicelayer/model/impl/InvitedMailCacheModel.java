/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.eng.rspa.ideas.challenges.servicelayer.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.eng.rspa.ideas.challenges.servicelayer.model.InvitedMail;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing InvitedMail in entity cache.
 *
 * @author Engineering Ingegneria Informatica S.p.A.
 * @see InvitedMail
 * @generated
 */
public class InvitedMailCacheModel implements CacheModel<InvitedMail>,
	Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(5);

		sb.append("{ideaId=");
		sb.append(ideaId);
		sb.append(", mail=");
		sb.append(mail);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public InvitedMail toEntityModel() {
		InvitedMailImpl invitedMailImpl = new InvitedMailImpl();

		invitedMailImpl.setIdeaId(ideaId);

		if (mail == null) {
			invitedMailImpl.setMail(StringPool.BLANK);
		}
		else {
			invitedMailImpl.setMail(mail);
		}

		invitedMailImpl.resetOriginalValues();

		return invitedMailImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		ideaId = objectInput.readLong();
		mail = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(ideaId);

		if (mail == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(mail);
		}
	}

	public long ideaId;
	public String mail;
}