// Need or Idea ?
var elemToCreate = "";

var reducedLifecycle = false;
var challengeIdReducedLifecycle = "";
var challengeNameReducedLifecycle = "";

// Function to animate an element [es.: $('#yourElement').animateCss('bounce'); ]
$.fn.extend({
    animateCss: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        $(this).addClass('animated ' + animationName).one(animationEnd, function() {
            $(this).removeClass('animated ' + animationName);
        });
    }
});	


var slideNumber = 0;

// Manage fullpage scroller

		$(document).ready(function() {
			//Disable Loader and enable Scroller to view.
			$(".preloader-wrapper").removeClass('active').css("display","none");
			$("#scroller").addClass("animated fadeIn");
			
			$('#fullpage').fullpage({
				anchors: ['sectionOne', 'sectionTwo', 'sectionThree', 'sectionFour', 'sectionFive', 'sectionSix'],
				lockAnchors: true,
				sectionSelector: '.section-fsp',
				controlArrows: false,
				navigation: true,
				navigationPosition: 'right',
				navigationTooltips: [imsTopLevel, imsDescription, imsTitle, imsCategories, imsKeywords, imsEnding],
				keyboardScrolling: false,
				loopHorizontal: false,
				fadingEffect: true,
// 				verticalCentered: false,
// 				fitToSection: false,
				
				onLeave: function(index, nextIndex, direction){
		            var leavingSection = $(this);

		            //after leaving 
		            if(direction =='down'){
		            	if(index == 1) { 
		            		toggleTabs();
		            	}else
		            	if(index == 5){
		            		$("#scrollBtnToolNext").css("display", "none");
		            		$("#question_last-"+elemToCreate+" p").css("opacity","0");
		            		$("#response_last-"+elemToCreate).css("opacity","0");
		            		//disabling scrolling up
		            		$.fn.fullpage.setAllowScrolling(true);
		            	}
		            }else if(direction =='up'){
		            	if(index >= 2) { 
		            		toggleTabs();
		            	}
		            	if(index == 3){
		            		$.fn.fullpage.setAllowScrolling(false);
		            	}else if(index == 6){
		            		$("#scrollBtnToolNext").css("display", "block");
		            		$("#sectionContent-"+elemToCreate).removeClass("lastSection");
		            	}
		            }
		            
		            $(".responses").css({'padding':'0px'});
		            $("[id^=question]").css("display","none");
            		$("[id^=response]").css({"display":"none"});
            		$('#rememberAssociatedItem').css("display","none");
//            		$("#scrollBtnToolNext").animateCss("fadeOut");
            		
		        },
		        
		        afterLoad: function(anchorLink, index){
		            var loadedSection = $(this);
		           
		            slideNumber = index;
		            
		            //using index
		            if(index == 1){
		            	$("#fp-nav").css({"z-index":-1, 'display':'none' }); //Hide Navigation
		            	$("#scrollBtnToolPrev").css("display", "none");
		            	$("#scrollBtnToolNext").css("display", "none");
		            	$("#subSection"+index).animateCss("fadeIn");
		            	$("[id^=subSection]").removeAttr('class');
	        			$("[id^=subSection]").attr('class', 'subSection');
	        			$.fn.fullpage.setAllowScrolling(false);
		            }else if(index == 2){
		            	$("#fp-nav").css({"z-index":1000, 'display':'block' }); //Show Navigation
		            	$("[id^=subSection"+index+"]").addClass("animated fadeIn");
		            	$("#scrollBtnToolPrev").css("display", "block");
		            	$("#scrollBtnToolNext").css("display", "block");
		            }else if(index == 3){
		            	$("#subSection"+index+"b").css('margin-top', "-"+$("#section1").height()+"px");;
		            	$("#subSection"+index+"a").addClass("animated fadeInLeftBig");
//		            	$("#subSection"+index+"b").addClass("animated fadeInUpBig");
		            }else if(index == 4){
		            	$("#subSection"+index+"a").css("opacity","1");
		            	$("#subSection"+index+"b").css("opacity","1");
		            	$("#subSection"+index+"b").css('margin-top', "-"+$("#section1").height()+"px");
		            	$("#subSection"+index+"a").addClass("animated slideInLeft");
		            	$("#subSection"+index+"b").addClass("animated slideInRight");
		            }else if(index == 5){
		            	$("#subSection"+index+"b").css('margin-top', "-"+$("#section1").height()+"px");
		            	$("#subSection"+index+"a").addClass("animated bounceIn");
		            	$("#subSection"+index+"b").addClass("animated bounceIn");
		            }else if(index == 6){
		            	$("#subSection"+index+"a").addClass("animated fadeIn");
		            }
		            
	            	if(index == 2 || index == 3 || index == 4 || index == 5 ){
	            		
	            		$('#rememberAssociatedItem').css("display","block");
	            		$(".responses").css({'padding':'10px 0px 20px 0px'});
	            		$("#question"+index+"-"+elemToCreate).css("display","block");
	            		$("#response"+index+"-"+elemToCreate).css({"display":"block"});
		            	$("#sectionContent-"+elemToCreate+" .questions .animatedObj").addClass("animated fadeInDown");
		            	$("#sectionContent-"+elemToCreate+" .responses .animatedObj").addClass("animated fadeInUp");
		            	
		            	//Positioning of the sectionContent question/response
	            		var contentH = $("#sectionContent-"+elemToCreate).height();
	            		if($(window).width() > 600){
	            			$("#sectionContent-"+elemToCreate).css("bottom", "50%");
	            			$("#sectionContent-"+elemToCreate).css("margin-bottom", -contentH/2);
	            			$("#fp-nav").css({"z-index":1000, 'display':'block' }); //Show Navigation
	            		}else{
	            			$("#sectionContent-"+elemToCreate).css("top", "47%");
	            			$("#sectionContent-"+elemToCreate).css("margin-bottom", "0");
	            			$("#fp-nav").css({"z-index":-1, 'display':'none' }); //Hide Navigation
	            		}
		            	
	            	}else if(index == 6){
	            		if($(window).width() < 600){
	            			$("#sectionContent-"+elemToCreate).css("top", "50%");
	            		}
		                $("#sectionContent-"+elemToCreate).addClass("lastSection");
	            		$("#question_last-"+elemToCreate).css("display","block");
	            		$("#response_last-"+elemToCreate).css("display","block");
	            		
	            		$("#question_last-"+elemToCreate+" p.subInfo").addClass("animated lightSpeedIn").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
	            			$("#response_last-"+elemToCreate).addClass("animated fadeInUpBig");
						}).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
							$("#question_last-"+elemToCreate+" p.subText").addClass("animated fadeInUp");
						});
	            		$("#sectionContent-"+elemToCreate).css("bottom", "0")
	            		$("#sectionContent-"+elemToCreate).css("margin-bottom", "0")
		            }
	            	
	            	$("#scrollBtnToolNext").animateCss("fadeIn");
	            	
		        },
		        
		        onSlideLeave: function( anchorLink, index, slideIndex, direction, nextSlideIndex){
		            var leavingSlide = $(this);
		            //leaving the second slide of the 1st Section to the previus or the next one
		            if(index == 1 && slideIndex == 1 && (nextSlideIndex == 0 || nextSlideIndex == 2)){
//		            	$("#scrollBtnToolNext").css("display", "block");
		            }else{
//		            	$("#scrollBtnToolNext").css("display", "none");
		            }

		        },
		        
		        afterResize: function(){
//		        	$(".rightHalf, .centerHalf, .leftHalf").width("33%");
		        }
		        
		        
			});
			//Adds or remove the possibility of scrolling through sections by using the mouse wheel/trackpad or touch gestures
			$.fn.fullpage.setAllowScrolling(false);
			
//			resizeHomeOIA();
		});
		
		$(document).on('click', '#btnPrevArrow', function(){
			if (isDoubleClicked($(this))) return;
			$.fn.fullpage.moveSectionUp();
		});
		$(document).on('click', '#btnNextArrow', function(){
//			  $("#scrollBtnToolNext").animateCss("fadeOut");
			
			var errorOnSlide = checkValueOnSlide(slideNumber, elemToCreate);
				
				if (errorOnSlide)
					return;
				
				  // Prevent a DoubleClick	
				  if (isDoubleClicked($(this))) return;
				  //$.fn.fullpage.setAllowScrolling(true); //da cancellare per evitare lo scrollin verso l'alto
				  $.fn.fullpage.moveSectionDown();
		});
	    $(document).on('click', '.btnNext_start', function(){
	        elemToCreate = $(this).data("tipo");
//	        $("#"+elemToCreate+"_page").load( contextPath+"/html/home/fullscrollpage/view_"+elemToCreate+".jsp" , function() {
	          insertTipo();
	          $.fn.fullpage.moveSectionDown();
//	        });
	    });
	    $(document).on('click', '.btnNext_left, .btnNext_right', function(){
    	
	    	
	        elemToCreate = $(this).data("tipo");
	                
	        if (reducedLifecycle){
	        	
	        	preStarted=true;
	    		
	        	$("#challenge"+challengeIdReducedLifecycle).addClass("linked");
	    		setOnChosenChallengeOrNeed("challenge",challengeIdReducedLifecycle, challengeNameReducedLifecycle );
	    		
	    		checkSelectedCard();

	    		elemToCreate = "idea";
	    		insertTipo();
	    		$.fn.fullpage.moveTo("sectionTwo");
	        }	        	        
	        
	        if(elemToCreate!="homeOIA") {
	       // 	$("#"+elemToCreate+"_page").load( contextPath+"/html/home/fullscrollpage/view_"+elemToCreate+".jsp" , function() {
	        		manageBtnSliding(elemToCreate);
	        //	});
	        }else{
	        	manageBtnSliding(elemToCreate);
	        }
	    });
		
		$(document).on('click', '[id^=reloadBtn]', function(){
			$("[id^=subSection]").removeAttr('class');
			$("[id^=subSection]").attr('class', 'subSection');
			$.fn.fullpage.moveTo(2);
		});
		
		$(document).on('click', '[id^=deleteBtn]', function(){
			
			var rAlertCancel=confirm(alertCancel);	
			
			if (rAlertCancel==true){
				$("[id^=subSection]").removeAttr('class');
				$("[id^=subSection]").attr('class', 'subSection');
				$.fn.fullpage.moveTo('sectionOne', 1);
//				toggleTabs();
				//Cancel all input form in the wizard
				$('#'+portletNamespace+'formIdea').trigger("reset");
				$('#'+portletNamespace+'formNeed').trigger("reset");
				//Un-select idea/need in the wizard
				$(".linked").removeClass("linked");
				checkSelectedCard();
				//Cancel all Tags created in the wizard
				var allTags="";
				$('#'+portletNamespace+'formIdea #'+portletNamespace+'assetTagNames').attr("value",allTags);
			    $('#'+portletNamespace+'formNeed #'+portletNamespace+'assetTagNames').attr("value",allTags);
			    $('[id$=addedTags] .chip').remove();
		    
			}
		    
		    
		});
		
	
		function manageBtnSliding(item){

			if(item=="idea"){
				$(".section-fsp").removeClass("sec_need");
				$.fn.fullpage.moveSlideRight();
			}else if(item=="need"){
				$(".section-fsp").removeClass("sec_idea");
				$.fn.fullpage.moveSlideLeft();
			}else{
//				$(".section-fsp").removeClass("sec_need sec_idea");
				$.fn.fullpage.moveTo('sectionOne', 1);
			}
			$(".section-fsp").addClass("sec_"+item);
		}
		
		function insertTipo(){
			if(elemToCreate=="idea"){
				$("#need_page").css("display","none");
				$(".section-fsp").removeClass("sec_need");
			}else{
				$("#idea_page").css("display","none");
				$(".section-fsp").removeClass("sec_idea");
			}
			$("#"+elemToCreate+"_page").css("display","block")
			$(".section-fsp").addClass("sec_"+elemToCreate);
		}
		
		function toggleTabs(){
			$(".tabs li").each(function( index ) {
				  if(index!=0) $( this ).toggleClass("disabled");
			});
		}
		
		//Prevent accidental DoubleClick before 1 second
		function isDoubleClicked(element) {
		    //if already clicked return TRUE to indicate this click is not allowed
		    if (element.data("isclicked")) return true;

		    //mark as clicked for 1 second
		    element.data("isclicked", true);
		    setTimeout(function () {
		        element.removeData("isclicked");
		    }, 1000);

		    //return FALSE to indicate this click was allowed
		    return false;
		}
		
		
		
		
		/* check input value when the user would go on by arrow button */
		function checkValueOnSlide (numSlide, needOrIdea){
			
			
			  if(numSlide == 2){ /*description*/
				  
				  
					  if(needOrIdea=="idea"){
						  
						  var description= document.getElementById(portletNamespace+'ideaDescription').value;
						  
					  }else{
						  
						  var description= document.getElementById(portletNamespace+'needDescription').value;
					  }
					  
					  
					  if (!description){
						  alert(alertInsertDescription);
						  return true;
					  }
					  
				  
			  }else if(numSlide == 3){ /*title*/
				  
				  
					  if(needOrIdea=="idea"){
						  
						  var title = document.getElementById(portletNamespace+'ideaTitle').value;
						  
					  }else{
						  
						  var title = document.getElementById(portletNamespace+'needTitle').value;
					  }
					  
					  if (!title){
						  alert(alertInsertTitle);
						  return true;
					  }
					  
					  var titleExists = checkTitleExist(title);
						 
						  if (titleExists == true){
							  alert(alertExistingTitle);
							  return true;
				  		}
				  
			  }else if(numSlide == 4){ /*categories*/
				  
				  
				  if(needOrIdea=="idea"){
					  
					  var ckCat = checkCategories();
					  if( ckCat == false ){
						  alert(alertSelectACategory);
						  return true; 
					  }
					  
					  
				  }else{
					  
					  var ckCat = checkMaterialCategories();
					  
					  if( ckCat ){
						  alert(alertSelectACategory);
						  return true;
					  }
					  
				  }
				  
				  
			  }
			
			  return false;
		}
		
		