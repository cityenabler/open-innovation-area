# README #

This project is a Liferay 6.2 portlet

### What is this repository for? ###

Open Innovation Area (OIA)

## Requirements  
| Framework                                                                        |Version  |License             |Note 																	|
|--------------------------------------------------------------------------------------------|---------|--------------------|-------------------------------------------------------------------------|
|[Java SE Development Kit](https://www.java.com/it/download/help/index_installing.xml?j=7) |7.0 |[Oracle Binary Code License](http://www.oracle.com/technetwork/java/javase/terms/license/index.html)         |Also v.8|
|[Liferay Portal CE bundled with Tomcat](https://sourceforge.net/projects/lportal/files/Liferay%20Portal/6.2.5%20GA6/liferay-portal-tomcat-6.2-ce-ga6-20160112152609836.zip/download) |6.2 GA6 | [GNU LGPL](https://www.liferay.com/it/downloads/ce-license)      ||
|[postgreSQL](https://www.postgresql.org/download/)| 9.3  |[PostgreSQL License](https://www.postgresql.org/about/licence/) ||
|[Social Office CE](https://web.liferay.com/it/community/liferay-projects/liferay-social-office/overview) | 3.0 | GNU LGPL |


## Libraries

OIA is based on the following software libraries and frameworks.

|Framework                           |Version       |Licence                                      |
|------------------------------------|--------------|---------------------------------------------|
|[Apache ActiveMq](http://activemq.apache.org/) | 5.10.0 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Apache Commons Lang](https://commons.apache.org/proper/commons-lang/) | 3.1 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Apache Commons Logging](http://commons.apache.org/proper/commons-logging/) | 1.2 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Apache Commons Math](http://commons.apache.org/proper/commons-math/) | 3.2 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Apache Log4j](http://logging.apache.org/log4j/1.2/) | 1.2 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Apache Lucene](http://lucene.apache.org/core/) | 2.3.1 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Apache Xerces](https://xerces.apache.org/) | 2.7.1 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[D3](https://d3js.org/) | 3 |[BSD](https://opensource.org/licenses/BSD-3-Clause) |
|[Java EE JMS API](http://jcp.org/en/jsr/detail?id=914) |3.1.2.2 |[CDDL+GPL](https://glassfish.dev.java.net/public/CDDL+GPL.html) |
|[JSTL API](http://jcp.org/en/jsr/detail?id=52) |1.2 |[CDDL+GPL](https://glassfish.dev.java.net/public/CDDL+GPL.html) |
|[JDOM](http://www.jdom.org/) |1.1.3 |[BSD/Apache style](http://www.jdom.org/docs/faq.html#a0030) |
|[Jena IRI](http://jena.sf.net/iri) |0.8 |[BSD-style license](http://jena.sourceforge.net/iri/license.html) |
|[jersey-client](https://jersey.java.net) |1.8 |[CDDL+GPL 1.1](http://glassfish.java.net/public/CDDL+GPL_1_1.html)|
|[jersey-core](https://jersey.java.net) |1.8 |[CDDL+GPL 1.1](http://glassfish.java.net/public/CDDL+GPL_1_1.html)|
|[Jersey core common](https://jersey.java.net) |2.1 |[CDDL+GPL 1.1](http://glassfish.java.net/public/CDDL+GPL_1_1.html)|
|[jQuery](https://jquery.org/) | 2.1.1 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[jQuery DataTables](http://datatables.net) |1.9.4 |[gpl2](http://datatables.net/license_gpl2) - [BSD](http://datatables.net/license_bsd)  |
|[jQuery dotdotdot](http://dotdotdot.frebsite.nl/) | 1.6.10 |Dual licensed under the MIT and GPL licenses |
|[jQuery Multiple File Upload Plugin ](http://www.fyneworks.com/jquery/multiple-file-upload/) | 1.48 | [MIT License](https://raw.githubusercontent.com/Dogfalo/materialize/master/LICENSE) |
|[jQuery prettySocial](https://github.com/sonnyt/prettySocial) |  | |
|[jQuery Swipebox](http://github.com/brutaldesign/swipebox) | 1.2.9 | [MIT License](https://raw.githubusercontent.com/Dogfalo/materialize/master/LICENSE) |
|[jQuery TableSorter](https://github.com/christianbach/tablesorterx) |  | Dual licensed under MIT or GPL licenses|
|[jQuery Upload File Plugin](https://github.com/hayageek/jquery-upload-file) | 2.0 | [MIT License](https://raw.githubusercontent.com/Dogfalo/materialize/master/LICENSE) |
|[Google Maps APIs](https://developers.google.com/maps/documentation/javascript/)| 3 |Creative Commons |
|[gson](https://github.com/google/gson)| 2.2.4 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[ICU4J](http://icu-project.org/apiref/icu4j/)| 3.4.4|[Unicode license](http://www.unicode.org/copyright.html#License) |
|[iText](https://sourceforge.net/projects/itext/)| 5.5.12|[Affero GNU Public License](https://opensource.org/licenses/AGPL-3.0) |
|[Jersey](https://jersey.java.net)| 1.8|[GNU LGPL](https://jersey.java.net/license.html) |
|[JUnit](http://junit.org/junit4/)| 4.5|BSD |
|[Materialize](http://materializecss.com)| 0.97.6 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Material Design Lite](https://github.com/google/material-design-lite)| 1.0.4|[MIT License](https://raw.githubusercontent.com/Dogfalo/materialize/master/LICENSE) |
|[OpenJena](http://www.openjena.org/) |2.6.4 |[BSD-style license](http://openjena.org/license.html) |
|[org.json](http://mvnrepository.com/artifact/org.json/json) | |[JSON License](http://www.json.org/license.html) |
|[ROME](https://rometools.github.io/rome/) | 1.0|[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[SLF4J API](http://www.slf4j.org) | 1.5.8|[MIT License](https://www.slf4j.org/license.html) |
|[SLF4J LOG4J-12 Binding](http://www.slf4j.org) | 1.2 - 1.5.8|[MIT License](https://www.slf4j.org/license.html) |
|[StAX](http://ebr.springsource.com/repository/app/bundle/version/detail?name=com.springsource.javax.xml.stream&version=1.0.1) | 1.0.1|[CDDL 1.0](http://ebr.springsource.com/repository/app/bundle/version/download;jsessionid=37FD3E441BF581B352960E5A8941B9C4.jvm1?name=com.springsource.javax.xml.stream&version=1.0.1&type=license) |
|[Twitter4j](http://twitter4j.org/) | 4.0.4 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |
|[Woodstox](https://github.com/FasterXML/woodstox) | 3.2.9 |[Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0) |

### Who do I talk to? ###

ENG team, Filippo Giuffrida, Antonino Sirchia

### Copying and License

This code is licensed under [AGPL-3.0](https://opensource.org/licenses/AGPL-3.0)