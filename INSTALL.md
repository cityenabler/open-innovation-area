# Installing Open Innovation Area


The installation of OIA is based on Liferay, a Java free and open source enterprise portal software product, distributed under the GNU Lesser General Public License.

## Preliminary configuration

The server that hosts the Open Innovation Area (OIA), first of all, must be Java enabled. For the details about Java 7 installation, follow the [Oracle Official Documentation](http://docs.oracle.com/javase/7/docs/webnotes/install/).The persistence layer is delegated to PostgreSQL. For the details about PostgreSQL installation, follow the [PostgreSQL Official Documentation](https://wiki.postgresql.org/wiki/Detailed_installation_guides). Once Java and PostgreSQL are installed, install Liferay. The release to installing is &quot;Liferay 6.2 CE Bundled with Tomcat&quot;. For the details about Liferay installation, follow the [Liferay Official Documentation](https://www.liferay.com/it/documentation/liferay-portal/6.2/user-guide/-/ai/installation-and-setup-liferay-portal-6-2-user-guide-15-en).
 
### Bundled Tomcat configuration and startup

In order to increase the duration of the working sessions, modify the file web.xml, in tomcat/conf, inside the bundled Liferay. 

    <!-- =========== Default Session Configuration ================= -->
      <!-- You can set the default session timeout (in minutes) for all newly   -->
      <!-- created sessions by modifying the value below.                       -->
        <session-config>
            <session-timeout>30</session-timeout>
        </session-config>
        
 Replacing the value 30, with the value 90.

In order to increase the allocation of RAM memory, modify the file setenv.sh, in tomcat/bin. The file should contain the following lines:

    CATALINA_OPTS="$CATALINA_OPTS -Dfile.encoding=UTF8 -Djava.net.preferIPv4Stack=true  -Dorg.apache.catalina.loader.WebappClassLoader.ENABLE_CLEAR_REFERENCES=false -Duser.timezone=Europe/Rome -Xms2048m -Xmx2048m -XX:PermSize=512m - 
    
    
    
At this point, startup the bundled Tomcat as described in [Liferay Official Documentation](https://www.liferay.com/it/documentation/liferay-portal/6.2/user-guide/-/ai/installing-a-bundle-liferay-portal-6-2-user-guide-15-en). Following the console, the message:


    INFO: Server startup in XXXXX ms 

This message will give confirm about the end of Tomcat startup.
Now, make the first access to the portal, accessing the address and the port where the Tomcat is listing, i.e. _http://localhost:8080/_

### Liferay Basic configuration

----------  

---------- 

In the panel &quot;Basic configuration&quot; set:

- Section Portal
  - Portal name: **OIA**
- Section Administrator User
  - First name: **OIA**
  - Last name: **Administrator**

- Section Database
  - Select _Change_
  - Database Type: **PostgreSQL**
  - JDBC URL: **jdbc:postgresql://localhost:5432/dbname** , where _dbname_  is the database name chosen in the phase of Postgres installation
  - Username: the username chosen in the phase of PostgreSQL installation, i.e. postgres
  - Password: the password chosen in the phase of PostgreSQL installation, i.e. postgres

----------

--------

Follow the wizard to choose the administrator password and complete the installation. 

### Liferay Portal Settings

Once finished the installation procedure, proceed as follows:
- Login as administrator (user OIA Administrator)
- Go to _Control Panel_
- Go to _Portal Settings_
- in _General panel_ set:
  - Mail Domain: mail domain chosen, i.e. oia.eu
  - Virtual Host: address domain chosen, i.e. oia.eu

--------- 
 
- in _User panel_ add:
  -  Citizen
  - Social Office User
to Roles field, as shown in the following Figure.

 ---------- 

- in _Email Notifications_ panel set:
  - Name: **OIA Administrator**
  - Address: the mail address chosen, i.e. admin@oia.eu

- In _Display Settings panel_ set:

  - **English (United Kingdom)**
  - **Finnish (Finland)** *
  - **Italian (Italy)**
  - **Serbian (Serbia)** *
  - **Serbian (Serbia, latin)** *
  - **Spanish (Spain)** *
  as _Current Available Languages_ (See following figure).

  *Finnish, Serbian and Spanish partially translated.
 ----------  

### Liferay Server Administration - Mail

Go to _Server Administration_
- in _Mail panel_ set:
  - Outgoing SMTP Server: the IP or address of SMTP service chosen
  - Outgoing Port: the port of SMTP service chosen
  - Check &quot;Use a Secure Network Connection&quot; if necessary
  - User Name, Password if necessary


### Organization 
Go to _Custom field_ &gt; Organization &gt; Add custom field
- in _Add custom field_ panel enter:
  - Key -->isAuthority
  - Type -->True/False
-  in _custom field_ panel click on Action &gt; on the _isAuthority_ row
   - Give the _view_ permission to _guest_ role
   
- in _Add custom field_ panel enter:
  - Key -->isCompany
  - Type -->True/False
-  in _custom field_ panel click on Action &gt; on the _isCompany_ row
   - Give the _view_ permission to _guest_ role
   
 ----------  

 Go to _Users and Organizations_ &gt; Add &gt; Regular Organization
- enter the name of the "Authority" or "Municipality" that will manage the Challenges
- Repeat this step for each Organization that you need 
 
### Portal-ext properties and restart

The file named [portal-ext.properties](/deploy/portal-ext.properties) must be placed in the same directory where is located the self-generated file _portal-setup-wizard.properties,_ that is at first level of Liferay directory installation.

Restart the Tomcat server.

### Liferay – App Manager

Now access to Liferay as Administrator and move to Apps, App Manager and Install

here, install all LPKGs of [deploy directory](/deploy). 

When finished, create all WAR packages (as described [here] (CREATEWAR.md)) for the Liferay projects published and proceed to install them.

Now, inactivate the unnecessary portlets, listed in the following table.

| Portlet | Portlet |
| --- | --- |
| Amazon Rankings | Hello Velocity |
| Hello World | Loan Calculator |
|  My Submissions (My Workflow Instances)  |  My Workflow Tasks  |
| Network Utilities | Password Generator |
| Recent Downloads | Shopping |
| Web Proxy | XSL Content |

---------- 
 
### Roles

- Go to _Control Panel_
- Go to _Roles_
- Add _Regular Role_
  - **Academy**
  - **Authority**
  - **Business**
  - **Citizen**
  - **CompanyLeader**  
  - **Developer**
- Give to _Authority_ role the permissions.

- Give to _Business_ role the permissions.

- Give to _Citizen_ and _CompanyLeader_  roles the permissions.

- Add to Power User Role the permission Site Administration &gt; Content &gt; Documents and Media &gt;  Document Folder  &gt; View

- Remove to Power User Role the permission Site &gt; Resource Permissions &gt; Site &gt;   _Go to Site Administration_ and  _Manage Pages_

- Add to Guest Role the permissions.


### Users

Thanks to previous configurations, all users who sign up to the Portal have roles of _User, Power User_, _Social Office User_ and _Citizen._

The roles Authority (Public Administration, Municipalities, etc.) and Business (SME, freelance professional, startupper, etc.) must be assigned by the Portal Administrator, when register these users on Liferay.

For each Authority users, add the user to its Organization

 Go to _Users and Organizations_ &gt; Add &gt; Click on the Organization that you created previous
- click on Assign Users
- select the users and save


## OIA configurations

In order to define the categories for Challenges and Ideas, proceed as follows:

- Login as _administrator_ (user OIA Administrator)
- Go to Site _Administration_
- Go to _Content_
- Go to _Categories_
- Define Vocabularies and Categories of Site

- Create the vocabulary that you need

- Create at least one category for each vocabulary

- Go to _Open Innovation Area configuration and enable Vocabularies for Open Innovation Area_, inserting the icon and the color to associate with them.

[Here](http://www.codeyouneed.com/alloyui-icons/) the list of eligible icons

[Here](http://materializecss.com/color.html) the list of eligible colors



- In the _suffix_ area, enter:
  - OIA Home suffix : **innovation-area**
  - Ideas suffix: **ideas\_explorer**
  - Need Reports suffix: **needs\_explorer**
  - Challenges suffix: **challenges\_explorer**

- In the _User notification_ area, check both fields
  - Email notifications Enabled 
  - Dockbar Notifications Enabled  
  
- In the _Default Email Notification_ area, enter:
  - Sender: **OIA**
  - Email object: **Open Innovation Area**
  - Signature: **Admin**
  - Suffix: **openinnovationarea**
  
- In the _Map parameters_ area, enter:
  - Latitude: **40.353237**
  - Longitude: **18.172545**

Or the coordinates where to centre the maps
- In the _Challenges calendar_, chose: **OIA** (Site Calendar)

- Verbosity
  - Check in development environment 
  - Uncheck in production environment

- Piloting
  - Uncheck 

- Need
  - Check if it useful   
  
- Public ideas
  - Check
  
- Public ideas
  - Uncheck  
  


### External Services
In this section you can configure the OIA interoperability demand of other tools

-  Basic Authentican User e.g. opsi@opsi.eu
-  Basic Authentican Password e.g. 01h2h2h3o3 
-  Marketplace Enabled 
-  Tweeting Enabled   
-  Citizen Data Vault Enabled 
-  Citizen Data Vault Address e.g. https://dev.opsi.eu 
-  Visual Composer Enabled 
-  Visual Composer Address e.g. https://dev.opsi.eu/visualcomposer/ 
-  JMS Enabled 
-  broker JMS username e.g. tesb 
-  broker JMS password e.g. tesb 
-  broker JMS URL e.g. tcp://oia.eng.it:8080 
-  JMS Topic e.g. IdeaManagement
-  FIWARE Enabled 
-  FIWARE Remote Catalogue Enabled 
-  FIWARE Remote Catalogue Address e.g. https://catalogue.fiware.org/getallges/views/getallges/
-  FundingBox Enabled 
-  Fundingbox Address e.g. http://fiware-gctc.fundingbox.com/
-  Fundingbox API Address e.g. http://api.fundingbox.com/v2/applications


## Pages
- Login as _administrator_ (user OIA Administrator)
- Go to _Control Panel_
- Go to _Sites_ and choose _your site name_
- Add the pages described in Table 4

| Page name | Page address| Parent| Page type | Note |
| --- | --- | --- |--- | --- |
| Open Innovation Area | /innovation-area | Public Pages | 2 Rows |   |
|Challenges  | /challenges\_explorer | Open Innovation Area| 2 Rows | Hide from Navigation Menu |
|Ideas | /ideas\_explorer | Open Innovation Area| 2 Rows | Hide from Navigation Menu |
|Needs | /needs\_explorer | Open Innovation Area| 2 Rows | Hide from Navigation Menu |

## Portlets
Continuing as Admin.
- Go to &quot;_Open Innovation Area&quot;_ page and install &quot; Open Innovation Area&quot; portlet
- Go to &quot;_ideas\_explorer&quot;_ page and install &quot;Ideas Explorer &quot; portlet
- Go to &quot;_needs\_explorer&quot;_ page and install &quot;Needs Explorer &quot; portlet
- Go to &quot;_challenges\_explorer&quot;_ page and install &quot;Challenges Explorer &quot; portlet
- For each of these 4 portlets, give the &quot;view&quot; permission to &quot;guest&quot; role by portlet configuration, as shown in example of the following figures
 
  ---------- 
 
### Search

- Login as _administrator_ (user OIA Administrator)
- Go to _Control_ Panel
- Go to _Sites_ and choose OIA
  - For each site pages install the _Search_ portlet at the bottom of the page

- Go to Site Templates and choose _Social Office User Home_
  - Uncheck _Allow Site Administrators to Modify the Pages Associated with This Site Template_ 
  - For each site templates pages (Figure 25) install the _Search_ portlet at the bottom of the page
- Go to Site Templates and choose _Social Office User Profile_
	- Uncheck _Allow Site Administrators to Modify the Pages Associated with This Site Template_ 

	
  - For each site templates pages install the _Search_ portlet at the bottom of the page

- For each _Search_ portlet just installed, access to _Configuration_.

- Give the view permission to &quot;guest&quot; role 

- Go in _Advanced configuration_ and modify the JSON:
  - Remove all values under &quot;values&quot; element and add:
    - _&quot;com.liferay.portal.model.User&quot;,_
    - _&quot;it.eng.rspa.ideas.challenges.servicelayer.model.CLSChallenge&quot;,_
    - _&quot;it.eng.rspa.ideas.challenges.servicelayer.model.CLSIdea&quot;,_
<img src="/images/liferay_search_model_19.png" alt="Liferay portlet configuration" height="300"/>


This step is necessary to configure the portal search at the top of each page, 

in next step we will install the Theme, the search portlet disappear from the bottom of each page.
 
### Theme

- Login as _administrator_ (user OIA Administrator)
- Go to _Control Panel_
- Go to _Sites_ and choose _your site name_
  - Set **EssentialCoreMaterial_977** with a Colour Schemes e.g. **Core 2**
   - Check _Dockbar Vertical_ and _Simple Footer_
  - Insert in footer-text field **&quot;by Opsi - Eng&quot;**
- Go to &quot;Logo&quot; section and upload **your logo** Go to _Site Templates_  and choose _Social Office User Home_
  - Set **EssentialCoreMaterialSocial_977** with a Colour Schemes e.g. **Core 2**
  - Check _Dockbar Vertical_ and _Simple Footer_
  - Insert in footer-text field **&quot;by Opsi - Eng&quot;**
  - Go to &quot;Logo&quot; section and upload **your logo**
- Repeat the last step, choosing in _Site Templates_, the site _Social Office User Profile_

